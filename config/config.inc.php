<?php
//database server
define('DB_SERVER', "localhost");
define('DB_USER', "bodhiinf_voyagad");
define('DB_PASS', "@]fNKOe}g[_.");

//database name
define('DB_DATABASE', "bodhiinf_voyag");

//smart to define your table names also
define('TABLE_ACCOUNTS', "accounts");
define('TABLE_ACCOUNT_GROUP', "account_group");
define('TABLE_ADVANCE_RECEIPT', "advance_receipt");
define('TABLE_AIRLINE_FLIGHT', "airline_flight");
define('TABLE_AIRLINE_PASSENGER', "airline_passenger");
define('TABLE_CASH_PAYMENT', "cash_payment");
define('TABLE_CASH_RECEIPT', "cash_receipt");
define('TABLE_BANK_PAYMENT', "bank_payment");
define('TABLE_BANK_RECEIPT', "bank_receipt");
define('TABLE_COMPANY', "company");
define('TABLE_FLIGHT', "flight");
define('TABLE_HOTEL', "hotel");
define('TABLE_HOTEL_CUSTOMER', "hotel_customer");

define('TABLE_INVOICE_AIRLINE', "invoice_airline");
define('TABLE_INVOICE_HOTEL', "invoice_hotel");
define('TABLE_INVOICE_MISC', "invoice_misc");

define('TABLE_INVOICE_TRANSPORT', "invoice_transport");
define('TABLE_INVOICE_VISA', "invoice_visa");

define('TABLE_LOGIN', "login");

define('TABLE_MISC_NARRATION', "misc_narration");
define('TABLE_OTHER_PURCHASE', "other_purchase");
define('TABLE_PASSENGER', "passenger");
define('TABLE_PURCHASE_AIRLINE', "purchase_airline");
define('TABLE_PURCHASE_HOTEL', "purchase_hotel");
define('TABLE_PURCHASE_MISC', "purchase_misc");
define('TABLE_PURCHASE_TRANSPORT', "purchase_transport");
define('TABLE_PURCHASE_VISA', "purchase_visa");

define('TABLE_ROOM_TYPE', "room_type");
define('TABLE_STAFF', "staff");
define('TABLE_TRANSACTION', "transaction");
define('TABLE_TRANSPORT_PROVIDER', "transport_provider");
define('TABLE_VEHICLE_TYPES', "vehicle_types");
define('TABLE_VISA_PASSENGER', "visa_passenger");

define('TABLE_PACKAGE', "package");
define('TABLE_PACKAGE_AIRLINE', "package_airline");
define('TABLE_PACKAGE_AIRLINE_FLIGHTS', "package_airline_flights");
define('TABLE_PACKAGE_HOTEL', "package_hotel");
define('TABLE_PACKAGE_HOTEL_ROOM_TYPE', "package_room_type");
define('TABLE_PACKAGE_TRANSPORTATION', "package_transportation");
define('TABLE_PACKAGE_VISA', "package_visa");

define('TABLE_INVOICE_PACKAGE', "invoice_package");
define('TABLE_INVOICE_PACKAGE_PASSENGER', "invoice_package_passenger");
define('TABLE_INVOICE_PACKAGE_AIRLINE', "invoice_package_airline");
define('TABLE_INVOICE_PACKAGE_HOTEL', "invoice_package_hotel");
define('TABLE_INVOICE_PACKAGE_TRANSPORTATION', "invoice_package_transportation");
define('TABLE_INVOICE_PACKAGE_VISA', "invoice_package_visa");

define('TABLE_PURCHASE_PACKAGE_AIRLINE', "purchase_package_airline");
define('TABLE_PURCHASE_PACKAGE_HOTEL', "purchase_package_hotel");
define('TABLE_PURCHASE_PACKAGE_TRANSPORT', "purchase_package_transport");
define('TABLE_PURCHASE_PACKAGE_VISA', "purchase_package_visa");

define('TABLE_PURCHASE_PAC_AIRLINE', "purchase_pac_airline");

define('TABLE_PERMISSION',"user_permission");
define('TABLE_VISAFOR',"visafor");
define('TABLE_ACADEMICYEAR',"academicyear");
define('TABLE_RELATIONSHIP',"relationship");
define('TABLE_LOG',"log");

define('TABLE_MISC_PASSENGER',"misc_passenger");

define('ROWS_PER_PAGE',"15");
?>