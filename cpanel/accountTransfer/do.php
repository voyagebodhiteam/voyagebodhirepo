<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear=$_SESSION['finYear'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
switch($optype)
{
		
	case 'index':		
		if(!$_POST['from_ledger'] || !$_POST['to_ledger'] || !$_POST['date'])
			{				
				$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            	header("location:index.php");
			}
		else
			{			
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=$success1=0;
				
					/* ------------------------------------------ */
					$voucherQuery ="SELECT MAX(voucherNo) AS vn FROM `".TABLE_TRANSACTION."` WHERE `voucherType`='Account Transfer' and proId=$proId";
					$voucherRecord = $db->query_first($voucherQuery);
					/* ------------------------------------------ */
					
					$Details = 'Account Tranfer';
					
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$App->convert($_POST['from_ledger']);
					$tfdata['toLedger'] 		=  	$App->convert($_POST['to_ledger']);
					$tfdata['voucherNo']		= 	$voucherRecord['vn']+1;
					$tfdata['voucherType']		=  	'Account Transfer';
					$tfdata['debit']		=       0;
					$tfdata['credit']		= 	$App->convert($_POST['paidAmount']);
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	"NOW()";
					$tfdata['remark']			=  	$App->convert($_POST['remark']);
					$tfdata['details']			=  	$Details;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$success=$db->query_insert(TABLE_TRANSACTION, $tfdata);
					//print_r($tfdata);die;
					/* ------------------------------------------ */
				
				
					/* ------------------------------------------ */
					$tsdata['fromLedger'] 		=  	$App->convert($_POST['to_ledger']);
					$tsdata['toLedger'] 		=  	$App->convert($_POST['from_ledger']);
					$tsdata['voucherNo']		= 	 $voucherRecord['vn']+1;
					$tsdata['voucherType']		=  	'Account Transfer';
					$tsdata['debit']		=  $App->convert($_POST['paidAmount']);
					$tsdata['credit']		=  0;
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	"NOW()";
					$tsdata['remark']			=  	$App->convert($_POST['remark']);
					$tsdata['details']			=  	$Details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					$success1=$db->query_insert(TABLE_TRANSACTION, $tsdata);
						
						
					if($success && $success1)
					{				
					$_SESSION['msg'] = $App->sessionMsgCreate('success', "Account Tranfered successfully.");																			
					}
					else
					{
					$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed");									
					}	
					header("location:index.php");	
					
			}
		
		break;
	
	
	// EDIT SECTION
	//-
	case 'edit':
		
		$editId	=	$_REQUEST['editId'];
		
		if(!$_POST['from_ledger'] || !$_POST['to_ledger'] || !$_POST['paidAmount'] || !$_POST['date'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=$success1=0;
				/* ------------------------------------------ */
					
					
					$vou_sql	=	"SELECT * FROM `".TABLE_TRANSACTION."` WHERE `ID`='$editId' AND `voucherType`='Account Transfer'";
				$vou_record = 	$db->query_first($vou_sql);
				$voucher_no = 	$vou_record['voucherNo']; //echo $voucher_no;die;
				
				$del_vou_sql = "DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucherType`='Account Transfer' AND `voucherNo`='$voucher_no'";
				$db->query($del_vou_sql);
				
				/* ------------------------------------------ */
				
				$Details = 'Account Tranfer';
					
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$App->convert($_POST['from_ledger']);
					$tfdata['toLedger'] 		=  	$App->convert($_POST['to_ledger']);
					$tfdata['voucherNo']		= 	$voucherRecord['vn']+1;
					$tfdata['voucherType']		=  	'Account Transfer';
					$tfdata['debit']			=   0;
					$tfdata['credit']			= 	$App->convert($_POST['paidAmount']);
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	"NOW()";
					$tfdata['remark']			=  	$App->convert($_POST['remark']);
					$tfdata['details']			=  	$Details;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$success=$db->query_insert(TABLE_TRANSACTION, $tfdata);
					//print_r($tfdata);die;
					/* ------------------------------------------ */
				
				
					/* ------------------------------------------ */
					$tsdata['fromLedger'] 		=  	$App->convert($_POST['to_ledger']);
					$tsdata['toLedger'] 		=  	$App->convert($_POST['from_ledger']);
					$tsdata['voucherNo']		= 	 $voucherRecord['vn']+1;
					$tsdata['voucherType']		=  	'Account Transfer';
					$tsdata['debit']			= 	$App->convert($_POST['paidAmount']);
					$tsdata['credit']			=  0;
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	"NOW()";
					$tsdata['remark']			=  	$App->convert($_POST['remark']);
					$tsdata['details']			=  	$Details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					$success1=$db->query_insert(TABLE_TRANSACTION, $tsdata);
						
				$db->close();
								
				if($success && $success1)
					{
					$_SESSION['msg'] = $App->sessionMsgCreate('success', "Account Tranfer Updated successfully.");														
					}
					else
					{
					$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed");									
					}	
					header("location:index.php");	
					
				
		}
		break;
	
	// DELETE SECTION
	//-
	case 'delete':

		
				$deleteId	=	$_REQUEST['id'];
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$vou_sql	=	"SELECT * FROM `".TABLE_TRANSACTION."` WHERE `ID`='$deleteId' AND `voucherType`='Account Transfer'  ";
				$vou_record = 	$db->query_first($vou_sql);
				$voucher_no = 	$vou_record['voucherNo'];
				
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucherType`='Account Transfer' AND `voucherNo`='$voucher_no'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg'] = $App->sessionMsgCreate('error', "You can't edit. Because this data is used some where else");	
					    
				}	
							
				$db->close(); 				
												
				if($success)
					{
						$_SESSION['msg'] = $App->sessionMsgCreate('success', "Account Transfer deleted successfully");	
																		
					}
					else
					{
						$_SESSION['msg'] = $App->sessionMsgCreate('error', "You can't edit. Because this data is used some where else");
											
					}	
					header("location:index.php");	
		break;
}
?>