<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
if($_SESSION['travelType']=='Admin')
{
	$finYear=$_SESSION['finYear'];
	$check	=	"".TABLE_TRANSACTION.".finYear='$finYear'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"".TABLE_TRANSACTION.".proId='$proId' and ".TABLE_TRANSACTION.".finYear='$finYear'";
}

$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ". TABLE_TRANSACTION." where ID='$editId' AND `voucherType`='Account Transfer' AND `debit`!='0'";
//echo $editSelect;
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>
<script>
function valid()
{	
flag=false;	
	jPin=document.getElementById('pin').value;
	jPin2=document.getElementById('pin2').value;
	jEmail=document.getElementById('email').value;	
	regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;	//email	
	
		if(jPin!="" &&(jPin.length!=6 || isNaN(jPin)) )
		{																			
		document.getElementById('p').innerHTML="Enter 6 digit number.";
		flag=true;
		}
		
		if(jPin2!="" &&(jPin2.length!=6 || isNaN(jPin2)))
		{																			
		document.getElementById('p2').innerHTML="Enter 6 digit number.";
		flag=true;
		}
		
		if(jEmail!="" &&(!jEmail.match(regEmail)))
		{
		document.getElementById('e').innerHTML="Invalid Email address";
		flag=true;
		}
		
	if(flag==true)
	{
	return false;
	}																		
}
//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit" class="default_form" onsubmit="return valid()">
            <input type="hidden" name="editId" value="<?php echo $editId; ?>">
                <div class="bd_panel_head">
                    <h3>ACCOUNT TRANSFER</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-md-3 form_block_row">
                            <div class="form_block">
                                <label>Date<span class="valid">*</span></label>
                                <input class="user_date" type="text" name="date" readonly="" value="<?php echo date("d/m/Y"); ?>">
                            </div>
                        </div>
                   	</div>
                   	<div class="row">
                        <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form_block">
                                <label>From<span class="valid">*</span></label>
                                <?php  
								  $from_ledger_qry= $db->query("SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID<'4' || ID>'40'");  
								  ?>
                                
                                <select name="from_ledger" class="form-control2" id="from_ledger" required title="From" >
						   		
								  <?php     
								  while($from_ledger_row=mysql_fetch_array($from_ledger_qry))					
								  {					 
								  ?>  
								 <option value="<?php echo $from_ledger_row['ID']; ?>" <?php if($from_ledger_row['ID']==$editRow['fromLedger']) { ?> selected="selected" <?php } ?>><?php echo $from_ledger_row['accountName']; ?></option>
								 </option>
							  <?php  
							  }  
							  ?>
	                  			</select>	
	                  
                            </div>
                        </div>
                         <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form_block">
                                 <label>To<span class="valid">*</span></label>
                                <?php  
								  $to_ledger_qry= $db->query("SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID<'4' || ID>'40'");  
								  ?>
                                
                                <select name="to_ledger" class="form-control2" id="to_ledger" required title="From" >
								  <?php     
								  while($to_ledger_row=mysql_fetch_array($to_ledger_qry))					
								  {					 
								  ?>  
								 <option value="<?php echo $to_ledger_row['ID']; ?>" <?php if($to_ledger_row['ID']==$editRow['toLedger']) { ?> selected="selected" <?php } ?>  ><?php echo $to_ledger_row['accountName']; ?></option>
								 </option>
							  <?php  
							  }  
							  ?>
	                  			</select>
                            </div>
                      	  </div>
                   </div>
                   <div class="row">
                         <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form_block">
                                <label>Paid Amount<span class="valid">*</span></label>
                                <input type="text" name="paidAmount" required="" value="<?php echo $editRow['debit']; ?>">
                            </div>
                      	  </div>
                         <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form_block">
                                <label>Remark<span class="valid">*</span></label>
                                <input type="text" name="remark" required="" value="<?php echo $editRow['remark']; ?>">
                            </div>
                      	  </div>
                   		</div> 	
                   </div>	
                  <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="UPDATE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
