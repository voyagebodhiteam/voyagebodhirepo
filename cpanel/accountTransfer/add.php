<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" onsubmit="return check()">
                <div class="bd_panel_head">
                    <h3>ACCOUNT TRANSFER</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-md-3 form_block_row">
                            <div class="form_block">
                                <label>Date<span class="valid">*</span></label>
                                <input class="user_date" type="text" name="date" required="" value="<?php echo date("d/m/Y"); ?>">
                            </div>
                        </div>
                   </div>
                   <div class="row">
                        <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form_block">
                                <label>From<span class="valid">*</span></label>
                                <?php  
								  $from_ledger_qry= $db->query("SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID<'4' || ID>'40'");  
								  ?>
                                
                                <select name="from_ledger" class="form-control2" id="from_ledger" required title="From" >
						   		<option value="">Select</option>
								  <?php     
								  while($from_ledger_row=mysql_fetch_array($from_ledger_qry))					
								  {					 
								  ?>  
								 <option value="<?php echo $from_ledger_row['ID']; ?>" ><?php echo $from_ledger_row['accountName']; ?>
								 </option>
							  <?php  
							  }  
							  ?>
	                  			</select>	
	                  
                            </div>
                        </div>
                         <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form_block">
                                 <label>To<span class="valid">*</span></label>
                                <?php  
								  $to_ledger_qry= $db->query("SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID<'4' || ID>'40'");  
								  ?>
                                
                                <select name="to_ledger" class="form-control2" id="to_ledger" required title="From" >
						   		<option value="">Select</option>
								  <?php     
								  while($to_ledger_row=mysql_fetch_array($to_ledger_qry))					
								  {					 
								  ?>  
								 <option value="<?php echo $to_ledger_row['ID']; ?>" ><?php echo $to_ledger_row['accountName']; ?>
								 </option>
							  <?php  
							  }  
							  ?>
	                  			</select>
                            </div>
                      	  </div>
                   </div>
                   <div class="row">
                         <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form_block">
                                <label>Paid Amount<span class="valid">*</span></label>
                                <input type="text" name="paidAmount" required="" value="0">
                            </div>
                      	  </div>
                         <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form_block">
                                <label>Remark<span class="valid">*</span></label>
                                <input type="text" name="remark" required="" >
                            </div>
                      	  </div>
                   </div>    
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
