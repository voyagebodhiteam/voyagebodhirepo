<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ". TABLE_COMPANY." where ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);

$select1="select ".TABLE_ACADEMICYEAR.".ID from ".TABLE_ACADEMICYEAR.",". TABLE_COMPANY."
		  where ".TABLE_ACADEMICYEAR.".ID = ". TABLE_COMPANY.".finYear
		  AND ". TABLE_COMPANY.".ID='$editId'";
		  //echo $select1;die;
$res=mysql_query($select1);
$row=mysql_fetch_array($res);
$finYear = $row['ID'];//echo $finYear;die; 
?>
<script>
	function valid()
	{
		flag=false;

		jPin=document.getElementById('pin').value;	
			
			if((isNaN(jPin)||jPin.length!=6) && jPin!=0)
			{		
				document.getElementById('pindiv').innerHTML="Enter valid pin number.";
				flag=true;
			}
		
		if(flag==true)
		{
		return false;
		}
	}
	//clear the validation msg
	function clearbox(Element_id)
	{
	document.getElementById(Element_id).innerHTML="";
	}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit" class="default_form" onsubmit="return valid()">
             <input type="hidden" name="editId" value="<?php echo $editId; ?>">
             <input type="hidden" name="financialYear" value="<?php echo $finYear; ?>">
                <div class="bd_panel_head">
                    <h3>COMPANY MASTER</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Company Name <span class="valid">*</span></label>
                                <input type="text" name="companyName" id="companyName" required="" value="<?php echo $editRow['companyName']; ?>">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                             
                                <label>Financial Year<span class="valid">*</span></label>
                                <select name="financialYear" id="financialYear" required="" disabled="true">  
                               	<option>Select</option>                  	
									<?php 
									$select1="select * from ".TABLE_ACADEMICYEAR."";
									$res=mysql_query($select1);
									while($row=mysql_fetch_array($res))
									{
									?>
									
			                    	<option value="<?php echo $row['ID']?>" <?php if($row['status']=='YES'){?> selected="selected" <?php }?>><?php echo $row['fromYear']."-".$row['toYear']?></option>
									<?php 
									}
									?>
                    			</select>
                            </div>
                        </div>
                         
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Pin</label>
                                <input type="text" name="pin" id="pin" value="<?php echo $editRow['pin']; ?>" onfocus="clearbox('pindiv')">
                                <div id="pindiv" class="valid" style="color:#FF6600;"></div>
                            </div>
                        </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>City</label>
                                <input type="text" name="city" id="city" value="<?php echo $editRow['city']; ?>" >
                            </div>
                          </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">                  
                            <div class="form_block">
                                <label>Phone(off.)</label>
                                <input type="text" name="phone" id="phone"  value="<?php echo $editRow['phone']; ?>">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Mobile <span class="valid">*</span></label>
                                <input type="text" name="mobile" id="mobile" value="<?php echo $editRow['mobile']; ?>" required="">
                            </div>
                       </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Email</label>
                                <input type="text" name="email" id="email"  value="<?php echo $editRow['email']; ?>">
                            </div>
                        </div>                     
                        <div class="col-lg-2 col-md-2 col-sm-2">    
                            <div class="form_block">
                                <label>Website</label>
                                <input type="text" name="website" id="website" value="<?php echo $editRow['website']; ?>">
                            </div>
                        </div>                     
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                         	<div class="form_block">
                                <label>TDS Number</label>
                                <input type="text" name="tds" id="tds"  value="<?php echo $editRow['tds']; ?>">
                            </div>
                       </div>                                      
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                            <div class="form_block">
                                <label>Circle/Ward</label>
                                <input type="text" name="circle" id="circle" value="<?php echo $editRow['circle']; ?>">
                            </div>
                        </div>                     
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                            <div class="form_block">
                                <label>Responsible Person</label>
                                <input type="text" name="resPerson" id="resPerson"  value="<?php echo $editRow['resPerson']; ?>">
                            </div>
                        </div>                     
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                            <div class="form_block">
                                <label>Designation</label>
                                <input type="text" name="designation" id="designation"  value="<?php echo $editRow['designation']; ?>">
                            </div>
                        </div>
                      </div>
                     <div class="row">
                     
                     	<div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Address</label>
                                <textarea name="address" id="address" ><?php echo $editRow['address']; ?></textarea>
                            </div>
                        </div>
                                        
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                            <div class="form_block">
                                <label>Remark</label>
                                <textarea name="remark" id="remark" ><?php echo $editRow['remark']; ?></textarea>
                            </div>
                        </div>
                        
                      </div>
                   
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="UPDATE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
?>
<?php
require('../admin_footer2.php');
?>
