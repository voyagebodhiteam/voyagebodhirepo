<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}


if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
	function valid()
	{
		flag=false;

		jPin=document.getElementById('pin').value;	
			
			if((isNaN(jPin)||jPin.length!=6) && jPin!=0)
			{		
				document.getElementById('pindiv').innerHTML="Enter valid pin number.";
				flag=true;
			}
		jPassword	=	document.getElementById('password').value;
		jConfirm	=	document.getElementById('cPassword').value;
			if(jPassword!=jConfirm)
			{
				document.getElementById('passdiv').innerHTML="Password is Mismatches with Confirm Password";
				flag=true;
			}
		if(flag==true)
		{
		return false;
		}
	}
	//clear the validation msg
	function clearbox(Element_id)
	{
	document.getElementById(Element_id).innerHTML="";
	}
</script>

<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" onsubmit="return valid()">
                <div class="bd_panel_head">
                    <h3>COMPANY MASTER</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Company Name <span class="valid">*</span></label>
                                <input type="text" name="companyName" id="companyName" required="">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Financial Year<span class="valid">*</span></label>
                                <select name="financialYear" id="financialYear" required="">  
			                    	<option value="">Select</option>                  	
									<?php 
									$select1="select * from ".TABLE_ACADEMICYEAR."";
									$res=mysql_query($select1);
									while($row=mysql_fetch_array($res))
									{
									?>
									
			                    	<option value="<?php echo $row['ID']?>"><?php echo $row['fromYear']."-".$row['toYear']?></option>
									<?php 
									}
									?>
                    			</select>
                            </div>
                        </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Pin</label>
                                <input type="text" name="pin" id="pin" onfocus="clearbox('pindiv')">
                                <div id="pindiv" class="valid" style="color:#FF6600;"></div>
                            </div>
                        </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>City</label>
                                <input type="text" name="city" id="city" >
                            </div>
                          </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">                  
                            <div class="form_block">
                                <label>Phone(off.)</label>
                                <input type="text" name="phone" id="phone" >
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Mobile <span class="valid">*</span></label>
                                <input type="text" name="mobile" id="mobile" required="">
                            </div>
                       </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Email</label>
                                <input type="email" name="email" id="email">
                            </div>
                        </div>                     
                        <div class="col-lg-2 col-md-2 col-sm-2">   
                            <div class="form_block">
                                <label>Website</label>
                                <input type="text" name="website" id="website">
                            </div>
                        </div>                     
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                         	<div class="form_block">
                                <label>TDS Number</label>
                                <input type="text" name="tds" id="tds" value="0">
                            </div>
                       </div>                                       
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                            <div class="form_block">
                                <label>Circle/Ward</label>
                                <input type="text" name="circle" id="circle">
                            </div>
                        </div>                     
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                            <div class="form_block">
                                <label>Responsible Person</label>
                                <input type="text" name="resPerson" id="resPerson" >
                            </div>
                        </div>                     
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                            <div class="form_block">
                                <label>Designation</label>
                                <input type="text" name="designation" id="designation" >
                            </div>
                        </div>
                      </div>
                     <div class="row">                     	
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                            <div class="form_block">
                                <label>Username <span class="valid">*</span></label>
                                <input type="text" name="userName" id="userName" required="">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                            <div class="form_block">
                                <label>Password <span class="valid">*</span></label>
                                <input type="password" name="password" id="password" required="" >
                            </div>
                        </div>
                                          
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                            <div class="form_block">
                                <label>Confirm Password <span class="valid">*</span></label>
                                <input type="password" name="cPassword" id="cPassword" required="" onfocus="clearbox('passdiv')" >
                                <div id="passdiv" class="valid" style="color:#FF6600;"></div>	
                                
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Address</label>
                                <textarea name="address" id="address" ></textarea>
                            </div>
                        </div>
                                        
                        <div class="col-lg-2 col-md-2 col-sm-2">  
                            <div class="form_block">
                                <label>Remark</label>
                                <textarea name="remark" id="remark" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
