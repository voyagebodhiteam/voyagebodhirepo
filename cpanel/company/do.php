<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear=$_REQUEST['financialYear'];

//echo $finYear;die;

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype){
	// NEW SECTION
	case 'index':

	if(!$_REQUEST['companyName']){
		$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
		header("location:index.php");
	} 
	else{
		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		$db->connect();
		$success = 0;

		$companyName		= $App->convert($_REQUEST['companyName']);           
		$companyName 		= ucwords(strtolower($companyName));
		$mobile				= $App->convert($_REQUEST['mobile']);

		$existId = $db->existValuesId(TABLE_COMPANY, "companyName='$companyName' and mobile='$mobile'");
		if($existId > 0){
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
			header("location:index.php");
		}
		else{
			$data['companyName']	=	$companyName;
			$data['address']		=	$App->convert($_REQUEST['address']); 
			$data['pin']			=	$App->convert($_REQUEST['pin']);
			$data['city']			=	$App->convert($_REQUEST['city']); 
			$data['phone']			=	$App->convert($_REQUEST['phone']);
			$data['mobile']			=	$App->convert($_REQUEST['mobile']); 
			$data['email']			=	$App->convert($_REQUEST['email']); 
			$data['website']		=	$App->convert($_REQUEST['website']);
			$data['tds']			=	$App->convert($_REQUEST['tds']); 
			$data['circle']			=	$App->convert($_REQUEST['circle']);
			$data['resPerson']		=	$App->convert($_REQUEST['resPerson']); 
			$data['designation']	=	$App->convert($_REQUEST['designation']);
			$data['remark']			=	$App->convert($_REQUEST['remark']);  
			$data['finYear']		=	$finYear;
                

			
			/* ************************************************************/
			$userName		=	$App->convert($_REQUEST['userName']);
			$password		=	$App->convert($_REQUEST['password']);
			$cPassword		=	$App->convert($_REQUEST['cPassword']);
									
					
			if($password==$cPassword){	
						
				$username1			=	mysql_real_escape_string(htmlentities($userName));
				$password1			=	mysql_real_escape_string(htmlentities($password));
						
				$pstring1	 		=	md5($password1); // Encrypted Password
				$pstring2	 		=	mysql_real_escape_string(htmlentities($pstring1));  // Password to store the database
					
						
				$selectRes=mysql_query("SELECT * FROM ".TABLE_LOGIN." WHERE userName='$username1' and finYear=$finYear");
                                         
				$selectRow=mysql_fetch_array($selectRes);//echo $selectRes;
				$Result = mysql_num_rows($selectRes);//echo $Result;die;
				if($Result==0){
					$success = $db->query_insert(TABLE_COMPANY, $data);
											
						$data2['userId']	=	$success;							
						$data2['userName']	=	$username1;
						$data2['password']	=	$pstring2;
						$data2['type']		=	'Branch';
                        $data2['status']	=	'Active';
						$data2['finYear']		= $finYear;
						$success1 = $db->query_insert(TABLE_LOGIN, $data2);														
						if($success1){	
						
							require_once('../../PHPMailer/class.phpmailer.php');														
							$emailTo	=	$App->convert($_REQUEST['email']);
							$userName	=	$App->convert($userName);
							$password	=	$App->convert($password1);
							$subject	=	"MARKAZ";
							$message	=	"UserName=".$userName."\r\n"."Password=".$password;
							//mail($email,$subject,$message);
							
							$email = new PHPMailer();
							//$email->From      = "career@bodhiinfo.com";
							$email->FromName  = "MARKAZ";
							$email->Subject   = $subject;
							$email->Body      = $message;
							$email->AddAddress($emailTo);
							
							if($email->Send())
							{
								$_SESSION['msg']=$App->sessionMsgCreate("success", "Added Successfully..");
							}
							else{
								$_SESSION['msg']=$App->sessionMsgCreate("success", "Added Successfully.Mail sending failed");
							}
							
					
						}
						else{
							$_SESSION['msg']=$App->sessionMsgCreate("error", "Failed ");															
						}																			
				}//if($Result==0)
				else{								
					$_SESSION['msg']=$App->sessionMsgCreate("error","Entered UserName Is Already Exist");		
				}
						
			}//if($pwd==$cPwd)
			else{					
				$_SESSION['msg']=$App->sessionMsgCreate("error","Password is Mismatches with Confirm Password");						
			}		
			header("location:index.php");
		}	//else	
                
                
		/* ************************************************************/
		
	}
	break;
	// EDIT SECTION
	case 'edit':
	$editId = $_REQUEST['editId'];
	$finYear=$_REQUEST['financialYear'];
	
	if(!$_REQUEST['companyName']){
		$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
		header("location:edit.php?id=$editId");
	} 
	else{
		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		$db->connect();
		$success = 0;

		$companyName		= $App->convert($_REQUEST['companyName']);           
		$companyName 		= ucwords(strtolower($companyName));
		$mobile				= $App->convert($_REQUEST['mobile']);


		$existId = $db->existValuesId(TABLE_COMPANY, "companyName='$companyName' and mobile='$mobile' and ID!='$editId'");
		if($existId > 0){
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
			header("location:index.php");
		}
		else{
			$data['companyName']	=	$companyName;
			$data['address']		=	$App->convert($_REQUEST['address']); 
			$data['pin']			=	$App->convert($_REQUEST['pin']);
			$data['city']			=	$App->convert($_REQUEST['city']); 
			$data['phone']			=	$App->convert($_REQUEST['phone']);
			$data['mobile']			=	$App->convert($_REQUEST['mobile']); 
			$data['email']			=	$App->convert($_REQUEST['email']); 
			$data['website']		=	$App->convert($_REQUEST['website']);
			$data['tds']			=	$App->convert($_REQUEST['tds']); 
			$data['circle']			=	$App->convert($_REQUEST['circle']);
			$data['resPerson']		=	$App->convert($_REQUEST['resPerson']); 
			$data['designation']	=	$App->convert($_REQUEST['designation']);
			$data['remark']			=	$App->convert($_REQUEST['remark']);  
			$data['finYear']		=	$finYear;  
               
			$success = $db->query_update(TABLE_COMPANY, $data, " ID='{$editId}'");
			$db->close();

			if($success){
				//$_SESSION['msg'] = " Details Added Successfully";
				$_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");
			} 
			else{
				$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to update details. Please try again.");                   
			}
			header("location:index.php");
		}
	}       
	break;
	// DELETE SECTION
	case 'delete':
	$deleteId = $_REQUEST['id'];//echo $deleteId;die;
	$success = $success2 = 0;

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
	$db->connect();

		$success2 = "DELETE FROM `".TABLE_LOGIN."` WHERE userId='{$deleteId}'";//echo $success2;die;
		$db->query($success2);
		$success = @mysql_query("DELETE FROM `" . TABLE_COMPANY . "` WHERE ID='{$deleteId}'");
	
	$db->close();
	if($success){
		$_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
	} else{
		$_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
	}
	header("location:index.php");
	break;
}
?>