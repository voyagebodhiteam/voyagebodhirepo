<?php
require('../admin_header.php');

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
$editId		=	$_REQUEST['id'];
$editSelect = 	"SELECT A.ID,
 					  A.voucherNo,
 					  A.voucherType,
 					  A.credit,
 					  A.remark,
 					  A.transactionDate,
 					  A.fromLedger as fromId,
 					  A.toLedger as toId,
 					  B.accountName as toLedger,
 					  C.accountName as fromLedger  
 				FROM `".TABLE_TRANSACTION."` A,`".TABLE_ACCOUNTS."` B,`".TABLE_ACCOUNTS."` C 
 			   WHERE (A.voucherType='Cash Payments' AND A.credit!='0' AND A.fromLedger=C.ID AND A.toLedger=B.ID) 
 			     AND  A.ID = $editId ";
	//echo $editSelect;
$editResult 	= 	$db->query($editSelect);
$editRow 		= 	mysql_fetch_array($editResult);
$voucherType	=	$editRow['voucherType'];
$voucherNo		=	$editRow['voucherNo'];
?>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit" id="cash_payment" class="default_form" onsubmit="return check()">
             <input type="hidden" name="editId" value="<?php echo $editId; ?>">
                <div class="bd_panel_head">
                    <h3>CASH PAYMENT</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Date <span class="valid">*</span></label>
                                <input type="text" name="transactionDate" id="transactionDate" class="user_date" required=""  value="<?php echo $App->dbformat_date($editRow['transactionDate']);?>">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">                            
                                <label>Voucher No <span class="valid">*</span></label>
                                <input type="text" name="voucherNo" required="" value="<?php echo 'CP'. $editRow['voucherNo']?>"  readonly="">
                            </div>
                        </div>
                        
                        <!--<div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account From*</label>
                                <select name="accountFrom" required="">
                                    <option value="">Select</option>
                                    <?php
                                    $subId	=	$db->subAccount(8);
                                    $select = "SELECT ID,accountName FROM ".TABLE_ACCOUNTS." WHERE accountGroupId IN($subId)";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        ?>
                                        <option value="<?php echo $row['ID']; ?>" <?php if($editRow['fromId']==$row['ID']){ echo "selected"; }?>><?php echo $row['accountName']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>-->
                        
                        <input type="hidden" name="accountFrom" value="2">
                        
                        <?php
								$cId=$editRow['toId']; 
								$editSelect2 = 	"select  accountName                          			
								from ".TABLE_ACCOUNTS." 
								where ID='$cId'";
								$editResult2 = 	$db->query($editSelect2);
								$editRow2	 = 	mysql_fetch_array($editResult2);
								?>
                        <!--<div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account To*</label>
                                <div class="aj_rel_box aj_account_to">
                                    <input name="accountToName" autocomplete="off" required="" id="accountToName" type="text" value="<?php echo $editRow2['accountName']; ?>">
                                    <input type="hidden" name="accountTo" value="<?php echo $editRow['toId']; ?>" />
                                    <ul class="aj_live_items"></ul>
                                    <p class="validation_mes">Please fill this!</p>
                                </div>
                            </div>
                        </div>-->
                        
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account To Name <span class="valid">*</span></label>
                                <div class="aj_rel_box aj_account_to">
                                    <input name="accountToName" autocomplete="off" required="" id="accountToName" type="text" value="<?php echo $editRow2['accountName']; ?>" readonly="">
                                    <ul class="aj_live_items"></ul>
                                    <p class="validation_mes">Please fill this!</p>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-2 col-md-2 col-sm-2">
                            <label>Account to ID <span class="valid">*</span></label>
                            <div class="aj_rel_box aj_account_to">
                                <input name="accountTo" autocomplete="off" required="" id="accountTo" type="text" value="<?php echo $editRow['toId']; ?>" readonly="">
                                <ul class="aj_live_items"></ul>
                                <p class="validation_mes">Please fill this!</p>
                            </div>
                        </div>
                        
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label style="visibility: hidden;">Get Unpaid Bills</label>
                                <button class="bd_btn bd_btn_blue" id="get_bill_details" data-target="#bill_list_pop">Get Bill Details</button>
                            </div>                            
                        </div>
                    </div>
                    <div class="cash_payment_table_wrap">
                        <table class="table table-bordered bd_table" id="tbl_cash_payment">
                            <thead>
                                <tr class="table_head">
                                    <th colspan="10">Cash Payment</th>
                                    <!--<th colspan="2" class="text-right">
                                        <button class="bd_btn bd_btn_blue" id="add_cash_payment_row" type="button">
                                            <i class="ion ion-plus"></i>Add New
                                        </button>
                                    </th>-->
                                </tr>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Against our invoice no*</th>
                                    <th>Date</th>
                                    <th>Purchase Type</th>
                                    <th>Total</th>
                                     <th>Balance</th>
                                    <th>Discount</th>
                                    <th>Paying Amt</th>
                                    <th>Remarks</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                        $paymentQry 	= mysql_query("SELECT  ".TABLE_CASH_PAYMENT.".voucherType,
                        							 ".TABLE_CASH_PAYMENT.".invoiceNo,
                        							 ".TABLE_CASH_PAYMENT.".remark,
                        							 ".TABLE_CASH_PAYMENT.".payingAmount,
													 ".TABLE_CASH_PAYMENT.".discount
                        						FROM ".TABLE_CASH_PAYMENT." 
                        					   WHERE voucherNo = '".$voucherNo."'
                        					   AND ".TABLE_CASH_PAYMENT.".paymentType='CASH'
                        					   AND ".TABLE_CASH_PAYMENT.".proId='$proId'
                        					GROUP BY ".TABLE_CASH_PAYMENT.".invoiceNo,
                        					".TABLE_CASH_PAYMENT.".voucherType");
                             $i	=	$net	=	$dis = 0;
                             $rowcount=mysql_num_rows($paymentQry);
                        	 while($paymentRow	=	mysql_fetch_array($paymentQry))
                        	 {
                        	 	$invoiceNo	=	$paymentRow['invoiceNo'];
                        	 	$voucherType	=	$paymentRow['voucherType'];
                                        //for getting the net amount if discount is there
                        	 	$payingAmount	=	$paymentRow['payingAmount'];
                        	 	$discount		=	$paymentRow['discount'];
                        	 	$netAmount		=	$payingAmount-$discount;
                        	 	//for getting total paid
                        	 	$totPaidQry	=	mysql_query("SELECT SUM(payingAmount) as paidAmount 
                        	 									FROM `".TABLE_CASH_PAYMENT."` 
                        	 									WHERE voucherType = '".$voucherType."' 
                        	 									AND invoiceNo = ".$invoiceNo." 
                        	 									AND proId='$proId'
                        	 									GROUP BY invoiceNo"); 
                        	 	$totPaidRow	=	mysql_fetch_array($totPaidQry);
                        	 	
                        	 	if($paymentRow['voucherType']=='airline')
                        	 	{
                        	 		$prefix="AP";
									$commonQry 	= mysql_query("SELECT invoiceDate,netAmount 
																FROM ".TABLE_PURCHASE_AIRLINE."
																WHERE invoiceNo=$invoiceNo
																AND proId='$proId'");
									$connonRow		=	mysql_fetch_array($commonQry);
								}
								if($paymentRow['voucherType']=='hotel')
                        	 	{
                        	 		$prefix="HP";
									$commonQry 	= mysql_query("SELECT invoiceDate,netAmount 
																FROM ".TABLE_PURCHASE_HOTEL."
																WHERE invoiceNo=$invoiceNo
																AND proId='$proId'");
									$connonRow		=	mysql_fetch_array($commonQry);
								}
								if($paymentRow['voucherType']=='miscellaneous')
                        	 	{
                        	 		$prefix="MP";
									$commonQry 	= mysql_query("SELECT invoiceDate,netAmount 
																FROM ".TABLE_PURCHASE_MISC."
																WHERE invoiceNo=$invoiceNo
																AND proId='$proId'");
									$connonRow		=	mysql_fetch_array($commonQry);
								}
								if($paymentRow['voucherType']=='transportation')
                        	 	{
                        	 		$prefix="TP";
									$commonQry 	= mysql_query("SELECT invoiceDate,netAmount 
																FROM ".TABLE_PURCHASE_TRANSPORT."
																WHERE invoiceNo=$invoiceNo
																AND proId='$proId'");
									$connonRow		=	mysql_fetch_array($commonQry);
								}
								if($paymentRow['voucherType']=='visa')
                        	 	{
                        	 		$prefix="VP";
									$commonQry 	= mysql_query("SELECT invoiceDate,netAmount 
																FROM ".TABLE_PURCHASE_VISA."
																WHERE invoiceNo=$invoiceNo
																AND proId='$proId'");
									$connonRow		=	mysql_fetch_array($commonQry);
								}							
								if($paymentRow['voucherType']=='Package Airline')
                        	 	{
                        	 		$prefix="PPA";
									$commonQry 	= mysql_query("SELECT purchaseDate as invoiceDate,netAmount 
																FROM ".TABLE_PURCHASE_PACKAGE_AIRLINE."
																WHERE voucherNo=$invoiceNo
																AND proId='$proId'");
									$connonRow		=	mysql_fetch_array($commonQry);
								}
								if($paymentRow['voucherType']=='Package Hotel')
                        	 	{
                        	 		$prefix="PPH";
									$commonQry 	= mysql_query("SELECT purchaseDate as invoiceDate,netAmount 
																FROM ".TABLE_PURCHASE_PACKAGE_HOTEL."
																WHERE voucherNo=$invoiceNo
																AND proId='$proId'");
									$connonRow		=	mysql_fetch_array($commonQry);
								}
								if($paymentRow['voucherType']=='Package Transport')
                        	 	{
                        	 		$prefix="PPT";
									$commonQry 	= mysql_query("SELECT purchaseDate as invoiceDate,netAmount 
																FROM ".TABLE_PURCHASE_PACKAGE_TRANSPORT."
																WHERE voucherNo=$invoiceNo
																AND proId='$proId'");
									$connonRow		=	mysql_fetch_array($commonQry);
								}
								if($paymentRow['voucherType']=='Package Visa')
                        	 	{
                        	 		$prefix="PPV";
									$commonQry 	= mysql_query("SELECT purchaseDate as invoiceDate,netAmount 
																FROM ".TABLE_PURCHASE_PACKAGE_VISA."
																WHERE voucherNo=$invoiceNo
																AND proId='$proId'");
									$connonRow		=	mysql_fetch_array($commonQry);
								}
                            ?>
                             <tr>
	                                    <td><?php echo ++$i;?></td>
	                                    
	                                    <td>
	                                        <div class="aj_rel_box">
	                                            <input type="text" name="invoiceNo_<?php echo $i;?>" data-rel="bill_live" value="<?php echo $prefix.$paymentRow['invoiceNo'];?>" readonly>
	                                            <ul class="aj_live_items">

	                                            </ul>
	                                        </div>
	                                    </td>
	                                    <td>
	                                        <input type="text" class="user_date" name="invoiceDate_<?php echo $i;?>" value="<?php if($connonRow['invoiceDate']){echo $App->dbformat_date($connonRow['invoiceDate']);}?>" readonly>
	                                    </td>
	                                    <td>
	                                        <input type="text" name="invoiceType_<?php echo $i;?>" value="<?php echo $paymentRow['voucherType'];?>" readonly>
	                                    </td>
	                                    <td>
	                                        <input type="text" name="total_<?php echo $i;?>" value="<?php echo $connonRow['netAmount'];?>" readonly>
	                                    </td>
	                                    <td>
	                                        <input type="text" name="totalPaid_<?php echo $i;?>" value="<?php echo $connonRow['netAmount']-$totPaidRow['paidAmount']; ?>" readonly>
	                                    </td>
	                                     <td>
											<input type="text" data-action="trigger_cash_calculation" name="discount_<?php echo $i;?>" value="<?php echo $paymentRow['discount']?>">
										</td>
	                                    <td>
	                                        <input type="text" data-action="trigger_cash_calculation" name="payingAmount_<?php echo $i;?>" value="<?php echo $paymentRow['payingAmount']; ?>">
	                                    </td>
	                                   
	                                    <td>
	                                        <textarea name="remark_<?php echo $i;?>"><?php echo $paymentRow['remark'];?></textarea>
	                                    </td>
	                                    <td>
	                                        <button class="bd_btn bd_btn_red" type="button" data-action="del_cash_payment_row">Delete</button>
	                                    </td>
	                                </tr>
                                <?php
                                $net=$net+$paymentRow['payingAmount'];
                                $dis=$dis+$paymentRow['discount'];
                                 } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                            <div class="form_block">
                                <label>Net Amount</label>
                                <input type="text" name="amount" id="amount" value="<?php echo $net-$dis; ?>">
                            </div>
                                            
                            <div class="form_block">
                                <label>Remark</label>
                                 <textarea name="remark" id="remark" ><?php echo $editRow['remark']; ?></textarea>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                        <input type="hidden" name="rowCount" id="cash_payment_row_count" value="<?php echo $rowcount;?>">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
?>

<!-- Pop up for bill list -->
<div id="bill_list_pop" class="modal fade bd_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Pending Bills</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" id="bill_list">
                        <thead>
                            <tr>
                                <th>Check</th>
                                <th>Voucher No</th>
                                <th>Date</th>
                                <th>Voucher Type</th>
                                <th>Total</th>
                                <th>Paid</th>
                                <th>Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="7">No bills found unpaid.</td>
                            </tr>
                        </tbody>
                    </table>    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="trigger_cash_payment">Pay Now</button>
                </div>
            </div>
        </div>
    </div>
<!-- /Pop up for bill list -->

<!-- div for select box for airline details -->
<div id="cash_payment_common_row" style="display: none">
    <table class="table table-bordered bd_table" id="tbl_cash_payment">
        <tbody>
        <tr>
            <td>1</td>
            <td>
                <input type="text" name="invoiceNo" data-rel="bill_live">
            </td>
            <td>
                <input type="text" data-dp="datepick" name="invoiceDate" readonly>
            </td>
            <td>
                <input type="text" name="total" value="0.00" readonly>
            </td>
            <td>
                <input type="text" name="totalPaid" value="0.00" readonly>
            </td>
            <td>
                <input type="text" data-action="trigger_cash_calculation" name="payingAmount" value="0.00">
            </td>
            <td>
                <textarea name="remark"></textarea>
            </td>
            <td>
                <button class="bd_btn bd_btn_red" type="button" data-action="del_cash_payment_row">Delete</button>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<?php
require('../admin_footer2.php');
?>
