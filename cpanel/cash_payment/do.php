<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}


$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear	=	$_SESSION['finYear'];
//for adding details
$loginType	=	$_SESSION['travelType'];
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
         
if($loginType=='Branch')
	{
		$loginName1		=	"SELECT ".TABLE_COMPANY.".companyName 
							   FROM ".TABLE_LOGIN." ,".TABLE_COMPANY." 						  
							  WHERE ".TABLE_COMPANY.".ID=".TABLE_LOGIN.".userId
							  	AND ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	$loginNameRow['companyName'];			  
	}
if($loginType=='Staff')
	{
		$loginName1		=	"SELECT ".TABLE_STAFF.".staffName, 
									".TABLE_COMPANY.".companyName
							   FROM ".TABLE_LOGIN." ,".TABLE_STAFF.",".TABLE_COMPANY." 						  
							  WHERE ".TABLE_STAFF.".ID=".TABLE_LOGIN.".userId
							  	AND ".TABLE_COMPANY.".ID=".TABLE_STAFF.".proId
							  	AND ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	/*$loginNameRow['companyName']."-".*/$loginNameRow['staffName'];			  
	}
	//echo $loginName;die;
 $db->close();


$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':
			
        if (!$_POST['accountFrom'] || !$_POST['accountTo'] || !$_POST['amount'] || !$_POST['transactionDate']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success1 = $success2 = 0;

           /* ------------------------------------------ */			
				 $newVoucherNo		=	$db->nextVoucher(TABLE_TRANSACTION,'voucherNo',"voucherType='Cash Payments' AND proId='$proId'" );
				 //echo $newVoucherNo;die;
				/* ------------------------------------------ */
				$dis=$totalDis=0;
				$cashDetails ="VouNo#CP$newVoucherNo,INV#";
				for($k=1;$k<=$_POST['rowCount'];$k++)
				{												 				   
				   $invoiceNo		=	"invoiceNo_".$k;
				   $invoiceType		=	"invoiceType_".$k;					   			   
				   $payingAmount	=	"payingAmount_".$k;
				   $discount		=	"discount_".$k;
				   $remark			=	"remark_".$k;
				   
				   $invoiceNo		=	$App->convert($_REQUEST[$invoiceNo]);//echo $invoiceNo;
				   $invoiceNo 		= 	substr($invoiceNo, 2);
				   if(!is_numeric(substr($invoiceNo, 0, 1)))
				   {
				   	 $invoiceNo 		= 	substr($invoiceNo, 1);//echo $invoiceNo;
				   }
	             
            	   $data2['voucherNo']		=	$newVoucherNo; 
            	   //echo $newVoucherNo;die;               
	               $data2['voucherType']	=	$App->convert($_REQUEST[$invoiceType]);
	               //for getting voucherType
				   $voucherType			=	$App->convert($_REQUEST[$invoiceType]);
	               $data2['invoiceNo']		=	$invoiceNo; 
	               
	               //adding prefix for getting corresponding details
	               				if($voucherType=='airline')
	               				{
										$prefix		=	'AP';
										$transType  = 'Airline Receivable';
										$query = "SELECT * FROM ".TABLE_PURCHASE_AIRLINE."
										WHERE ".TABLE_PURCHASE_AIRLINE.".invoiceNo = '$invoiceNo'";
										$purchaseRecord	=	$db->query_first($query);
										
										$aOurInNo = $purchaseRecord['ourInvoiceNo'];
								}
								else if($voucherType=='hotel')
								{
										$prefix		=	'HP';
										$transType  = 'Hotel Receivable';
											$query = "SELECT * FROM ".TABLE_PURCHASE_HOTEL."
										WHERE ".TABLE_PURCHASE_HOTEL.".invoiceNo = '$invoiceNo'";
										$purchaseRecord	=	$db->query_first($query);
										
										$aOurInNo = $purchaseRecord['ourInvoiceNo'];
								}
								else if($voucherType=='visa')
								{
										$prefix		=	'VP';
										$transType  = 'Visa Receivable';
											$query = "SELECT * FROM ".TABLE_PURCHASE_VISA."
										WHERE ".TABLE_PURCHASE_VISA.".invoiceNo = '$invoiceNo'";
										$purchaseRecord	=	$db->query_first($query);
										
										$aOurInNo = $purchaseRecord['ourInvoiceNo'];
								}
								else if($voucherType=='miscellaneous')
								{
										$prefix		=	'MP';
										$transType  = 'Misc Receivable';
											$query = "SELECT * FROM ".TABLE_PURCHASE_MISC."
										WHERE ".TABLE_PURCHASE_MISC.".invoiceNo = '$invoiceNo'";
										$purchaseRecord	=	$db->query_first($query);
										
										$aOurInNo = $purchaseRecord['ourInvoiceNo'];
								}
								else if($voucherType=='transportation')
								{
										$prefix		=	'TP';
										$transType  = 'Transport Receivable';
											$query = "SELECT * FROM ".TABLE_PURCHASE_TRANSPORT."
										WHERE ".TABLE_PURCHASE_TRANSPORT.".invoiceNo = '$invoiceNo'";
										$purchaseRecord	=	$db->query_first($query);
										
										$aOurInNo = $purchaseRecord['ourInvoiceNo'];
								}
								else if($voucherType=='package')
								{
										$prefix		=	'PP';
								}
								
				   $cashDetails.=  $prefix.$invoiceNo.",";
				   		                
	               $data2['payingAmount']	=	$App->convert($_REQUEST[$payingAmount]); 
	               $data2['discount']		=	$dis =$App->convert($_REQUEST[$discount]); 
	               $data2['remark']			=	$App->convert($_REQUEST[$remark]);  
	               $data2['cheque']			=	0;
	               $data2['paymentType']	=	'CASH';
	               $data2['proId']			=	$proId;
	               $data2['loginId']		=	$loginId;
	               $data2['finYear']		=	$finYear;
	             
	              if($invoiceNo>0)
	               {
				   	$success3 = $db->query_insert(TABLE_CASH_PAYMENT, $data2);
				   }
				   else{
				   	continue;
				   }
				   $totalDis+=$dis;
	              
	             }
	             
	             $toQry	=	mysql_query("SELECT accountName FROM ".TABLE_ACCOUNTS." WHERE ID=".$_POST['accountTo']);
				$toRow	=	mysql_fetch_array($toQry);
				$To		=	$toRow['accountName'];	
	            
	            
	            //for getting corresponding invoice/ purchase details from transaction table
				 
				 $transQuery = "SELECT details FROM ".TABLE_TRANSACTION." WHERE 
				 				               ".TABLE_TRANSACTION.".voucherNo = '$aOurInNo'
				 				               AND ".TABLE_TRANSACTION.".voucherType ='$transType'";
				 $resultQuery	=	$db->query_first($transQuery);
				 $transDetails	=	$resultQuery['details'];
	            
	             
				//$cashDetails	=	"Cash Payments(CP), INV#CP$newvocherNo, By:$loginName ";
				//for getting voucherno and invoice details in details field.
				 $cashDetails2 		= 	chop($cashDetails,',');
		         $cashDetails3		=	$cashDetails2." Paid By :$loginName "."To : $To,Details:".$transDetails;	
					//echo $cashDetails3;die;																					
				$tsdata['fromLedger'] 		=  	$App->convert($_POST['accountTo']);
				$tsdata['toLedger'] 		=  	$App->convert($_POST['accountFrom']);
				$tsdata['voucherNo']		= 	$newVoucherNo;
				$tsdata['voucherType']		=  	'Cash Payments';
				$tsdata['credit']			= 	0;
				$tsdata['debit']			=  	$App->convert($_POST['amount'])+$totalDis;
				$tsdata['addedDate']		=	"NOW()";
				$tsdata['transactionDate']	=	$App->dbformat_date($_POST['transactionDate']);
				$tsdata['remark']			=  	$App->convert($_POST['remark']);
				$tsdata['details']			=  	$cashDetails3;
				$tsdata['loginId']			=	$loginId;
				$tsdata['proId']			=	$proId;
				$tsdata['finYear']			=	$finYear;

				$success1=$db->query_insert(TABLE_TRANSACTION, $tsdata);				
				/* ------------------------------------------ */
				
				/* ------------------------------------------ */
				$tfdata['fromLedger'] 		=  	$App->convert($_POST['accountFrom']);
				$tfdata['toLedger'] 		=  	$App->convert($_POST['accountTo']);
				$tfdata['voucherNo']		= 	 $newVoucherNo;
				$tfdata['voucherType']		=  	'Cash Payments';
				$tfdata['credit']			=   $App->convert($_POST['amount'])+$totalDis;
				$tfdata['debit']			= 	0;
				$tfdata['addedDate']		=	"NOW()";
				$tfdata['transactionDate']	=	$App->dbformat_date($_POST['transactionDate']);
				$tfdata['remark']			=  	$App->convert($_POST['remark']);
				$tfdata['details']			=  	$cashDetails3;
				$tfdata['loginId']			=	$loginId;
				$tfdata['proId']			=	$proId;
				$tfdata['finYear']			=	$finYear;
			
				$success2=$db->query_insert(TABLE_TRANSACTION, $tfdata);
			
				/* ------------------------------------------ */
				 if($totalDis>0)
		          {
		          	
		          	 $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Received' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$App->convert($_POST['accountTo']);
					$tsdata['toLedger'] 		=   $discountId;
					$tsdata['voucherNo']		= 	$newVoucherNo;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($totalDis));
					//$tsdata['referenceNo']		=   $newVoucherNo;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['transactionDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['transactionDate']);
					$tsdata['remark']			=  	"Discount Received:inv#".$newVoucherNo;
					$tsdata['details']			=	$cashDetails3;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $discountId;	
					$tfdata['toLedger'] 		=  	$App->convert($_POST['accountTo']);
					$tfdata['voucherNo']		= 	$newVoucherNo;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   abs($App->convert($totalDis));
					$tfdata['debit']			= 	0;
					//$tfdata['referenceNo']		=   $newVoucherNo;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['transactionDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['transactionDate']);
					$tfdata['remark']			=  	"Discount Received:inv#".$newVoucherNo; 
					$tfdata['details']			=	$cashDetails3;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }				   
			
                $db->close();

             if ($success1 && $success2) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
            }
        break;
    // EDIT SECTION
    case 'edit':
        $editId = $_REQUEST['editId'];
        if (!$_POST['accountFrom'] || !$_POST['accountTo'] || !$_POST['amount'] || !$_POST['transactionDate'])  
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:edit.php?id=$editId");
        } 
        else 
        {
           
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success1 = $success2 = 0;

          		/* ------------------------------------------ */
				$vou_sql	=	"SELECT * FROM `".TABLE_TRANSACTION."` WHERE `ID`='$editId' AND `voucherType`='Cash Payments' AND proId='$proId'";
				$vou_record = 	$db->query_first($vou_sql);
				$existVouNo = 	$vou_record['voucherNo'];
				//echo $existVouNo;die;
				$del_cash = "DELETE FROM `".TABLE_CASH_PAYMENT."` WHERE `voucherNo`='$existVouNo' AND paymentType='CASH' AND proId='$proId' and finYear=$finYear";
				$success = $db->query($del_cash);
				
				$del_trans = "DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucherType`='Cash Payments' AND `voucherNo`='$existVouNo' AND proId='$proId' and finYear=$finYear";
				$success = $db->query($del_trans);
				/* ------------------------------------------ */
				
				/* ------------------------------------------ */	
				$dis=$totalDis=0;
				$cashDetails ="VouNo#CP$existVouNo,INV#";
				for($k=1;$k<=$_POST['rowCount'];$k++)
				{												 				   
				   $invoiceNo		=	"invoiceNo_".$k;
				   $invoiceType		=	"invoiceType_".$k;					   			   
				   $payingAmount	=	"payingAmount_".$k;
				   $discount		=	"discount_".$k;
				   $remark			=	"remark_".$k;
				   
				   $invoiceNo		=	$App->convert($_REQUEST[$invoiceNo]);//echo $invoiceNo;
				   $invoiceNo 		= 	substr($invoiceNo, 2);
				   if(!is_numeric(substr($invoiceNo, 0, 1)))
				   {
				   	 $invoiceNo 		= 	substr($invoiceNo, 1);//echo $invoiceNo;
				   }
	             
            	   $data2['voucherNo']		=	$existVouNo; 
            	   //echo $existVouNo;die;               
	               $data2['voucherType']	=	$App->convert($_REQUEST[$invoiceType]);
	               //for getting voucherType
				   $voucherType			=	$App->convert($_REQUEST[$invoiceType]);
	               $data2['invoiceNo']		=	$invoiceNo; 
	               
	               //adding prefix for getting corresponding details
	               				if($voucherType=='airline')
	               				{
										$prefix		=	'AP';
								}
								else if($voucherType=='hotel')
								{
										$prefix		=	'HP';
								}
								else if($voucherType=='visa')
								{
										$prefix		=	'VP';
								}
								else if($voucherType=='miscellaneous')
								{
										$prefix		=	'MP';
								}
								else if($voucherType=='transportation')
								{
										$prefix		=	'TP';
								}
								else if($voucherType=='package')
								{
										$prefix		=	'PP';
								}
								
				   $cashDetails.=  $prefix.$invoiceNo.",";
				   		                
	               $data2['payingAmount']	=	$App->convert($_REQUEST[$payingAmount]); 
	               $data2['discount']		=	$dis =$App->convert($_REQUEST[$discount]); 
	               $data2['remark']			=	$App->convert($_REQUEST[$remark]);  
	               $data2['cheque']			=	0;
	               $data2['paymentType']	=	'CASH';
	               $data2['proId']			=	$proId;
	               $data2['loginId']		=	$loginId;
	             
	              if($invoiceNo>0)
	               {
				   	$success3 = $db->query_insert(TABLE_CASH_PAYMENT, $data2);
				   }
				   else{
				   	continue;
				   }
				   $totalDis+=$dis;
	              
	             }
				//$cashDetails	=	"Cash Payments(CP), INV#CP$newvocherNo, By:$loginName ";
				//for getting voucherno and invoice details in details field.
				 $cashDetails2 		= 	chop($cashDetails,',');
		         $cashDetails3		=	$cashDetails2." By :$loginName "."To : $To";	
																										
				$tsdata['fromLedger'] 		=  	$App->convert($_POST['accountTo']);
				$tsdata['toLedger'] 		=  	$App->convert($_POST['accountFrom']);
				$tsdata['voucherNo']		= 	$existVouNo;
				$tsdata['voucherType']		=  	'Cash Payments';
				$tsdata['credit']			= 	0;
				$tsdata['debit']			=  	$App->convert($_POST['amount'])+$totalDis;
				$tsdata['addedDate']		=	"NOW()";
				$tsdata['transactionDate']	=	$App->dbformat_date($_POST['transactionDate']);
				$tsdata['remark']			=  	$App->convert($_POST['remark']);
				$tsdata['details']			=  	$cashDetails3;
				$tsdata['loginId']			=	$loginId;
				$tsdata['proId']			=	$proId;
				$tsdata['finYear']			=	$finYear;

				$success1=$db->query_insert(TABLE_TRANSACTION, $tsdata);				
				/* ------------------------------------------ */
				
				/* ------------------------------------------ */
				$tfdata['fromLedger'] 		=  	$App->convert($_POST['accountFrom']);
				$tfdata['toLedger'] 		=  	$App->convert($_POST['accountTo']);
				$tfdata['voucherNo']		= 	 $existVouNo;
				$tfdata['voucherType']		=  	'Cash Payments';
				$tfdata['credit']			=   $App->convert($_POST['amount'])+$totalDis;
				$tfdata['debit']			= 	0;
				$tfdata['addedDate']		=	"NOW()";
				$tfdata['transactionDate']	=	$App->dbformat_date($_POST['transactionDate']);
				$tfdata['remark']			=  	$App->convert($_POST['remark']);
				$tfdata['details']			=  	$cashDetails3;
				$tfdata['loginId']			=	$loginId;
				$tfdata['proId']			=	$proId;
				$tfdata['finYear']			=	$finYear;
			
				$success2=$db->query_insert(TABLE_TRANSACTION, $tfdata);
			
				/* ------------------------------------------ */
				 if($totalDis>0)
		          {
		          	
		          	 $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Received' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$App->convert($_POST['accountTo']);
					$tsdata['toLedger'] 		=   $discountId;
					$tsdata['voucherNo']		= 	$existVouNo;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($totalDis));
					//$tsdata['referenceNo']		=   $existVouNo;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['transactionDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['transactionDate']);
					$tsdata['remark']			=  	"Discount Received:inv#".$existVouNo;
					$tsdata['details']			=	$cashDetails3;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $discountId;	
					$tfdata['toLedger'] 		=  	$App->convert($_POST['accountTo']);
					$tfdata['voucherNo']		= 	$existVouNo;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   abs($App->convert($totalDis));
					$tfdata['debit']			= 	0;
					//$tfdata['referenceNo']		=   $existVouNo;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['transactionDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['transactionDate']);
					$tfdata['remark']			=  	"Discount Received:inv#".$existVouNo; 
					$tfdata['details']			=	$cashDetails3;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }				   
			
                $db->close();

             if ($success1 && $success2) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to update details. Please try again.");                   
                }
                 header("location:index.php");
            }
        break;
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
		
		/* ------------------------------------------ */
		$vou_sql	=	"SELECT * FROM `".TABLE_TRANSACTION."` WHERE `ID`='$deleteId' AND `voucherType`='Cash Payments' AND proId='$proId'";
		$vou_record = 	$db->query_first($vou_sql);
		$voucher_no = 	$vou_record['voucherNo'];
		
		$del_cash = "DELETE FROM `".TABLE_CASH_PAYMENT."` WHERE `voucherNo`='$voucher_no' AND paymentType='CASH'  AND proId='$proId' and finYear=$finYear";
		$success = $db->query($del_cash);
		
		$del_trans = "DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucherType`='Cash Payments' AND `voucherNo`='$voucher_no'  AND proId='$proId' and finYear='$finYear'";
		$success = $db->query($del_trans);
        $db->close();
        
        if ($del_trans) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete this.");
        }
        header("location:index.php");
        break;
        
        
        // Printing  ========================================================================
case 'print':
		
				$printID	=	$_REQUEST['id'];
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
					
				$userId		=	$_SESSION['proId'];
				$sql2 		= 	"SELECT * FROM `".TABLE_COMPANY."` where ID=$userId";
				$vou_record1 = 	$db->query_first($sql2);
				$company 	= 	$vou_record1['companyName'];
				$caddress 	= 	$vou_record1['address'];
				$cphone		=	$vou_record1['phone'];
				$cmobile	=	$vou_record1['mobile'];	
				$cemail		=	$vou_record1['email'];
				$cpin		=	$vou_record1['pin'];
				
				// set default header data
				/*$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));*/

				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
				$sql2 = "SELECT A.ID,A.voucherNo,A.credit,A.transactionDate,A.remark,A.details,
         					  	B.accountName as toLedger,
         					  	C.accountName as fromLedger 
         				 FROM `".TABLE_TRANSACTION."` A,`".TABLE_ACCOUNTS."` B,
         				 	  `".TABLE_ACCOUNTS."` C
         			     WHERE A.ID=$printID AND A.fromLedger=C.ID AND 
         			     	   A.toLedger=B.ID 
         			     	   ";
				$vou_record 	= 	$db->query_first($sql2);
					
                //$details		=	$vou_record['details'];			
				$invoiceNo 		= 	$vou_record['voucherNo'];
				$invoiceDate 	= 	$vou_record['transactionDate'];
				$credit			= 	$vou_record['credit'];
				$toLedger		= 	$vou_record['toLedger'];
				//echo $toLedger;die;
				$fromLedger 	= 	$vou_record['fromLedger'];	
				$toLedger		=	$vou_record['toLedger'];			
				
				//$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
				$defRow	=	8;
				//$mainProcCharge	=	$proCharge+$otherCharges;
				
				//basic info
				$tbl_header1 = '<div><table style="width: 638px;" cellspacing="0" border="0" >';
				$tbl_footer1 = '</table></div>';
				$tbl1 = '<tr>
							<td colspan="6" align="left" ><h2>'.strtoupper($company).'</h2></td>
						 </tr>
						 <tr>
							<td colspan="6"></td>
						 </tr>
						 <tr>
						 	<td colspan="3" align="left">'.$caddress.'<br>'.$cpin.'</td>
						 	<td colspan="3" align="left">'.$cphone.'<br>'.$cmobile.'<br>'.$cemail.'</td>
						 </tr>
						 <tr>
							<td colspan="6" ></td>
						 </tr>
						 <tr>
						 	<td colspan="3" style=" width:300px; border-top: 0.1px solid black; border-bottom: 0.1px solid black;"><strong>Staff Name : </strong>'.$loginName.'</td>
						 	<td colspan="3" style=" width:220px; border-top: 0.1px solid black;border-bottom: 0.1px solid black;background-color:#F2F2F2; border-left: 0.1px solid black;" align="center"><strong>Cash Payment</strong></td>
						 	
						 </tr>
						 <tr>
						 	<td colspan="3">Received with thanks</td>
						 	<td colspan="3" style="border-left: 0.1px solid black;">
							 	<table>
								 	<tr>
								 		<td style="width:60px;">Receipt No.</td><td style="width: 60px;" align="left"><strong>CP'.$invoiceNo.'</strong></td>
								 	</tr>
								 	
							 	</table>
						 	</td>
						 </tr>
						
						 <tr>
						 	<td colspan="3" style="border-bottom: 0.1px solid black;">To,'.$fromLedger.'</td>
						 	<td colspan="3" style="border-left: 0.1px solid black; border-bottom: 0.1px solid black;">
							 	<table>
								 	<tr>
								 		<td style="width:60px;">Date </td><td style="width:60px;" align="left"><strong>'.$App->dbformat_date($invoiceDate).'</strong></td>
								 	</tr>
								 	
							 	</table>
						 	</td>
						 </tr>
							';
							
				$pdf->SetFontSize(9);
				$pdf->writeHTML($tbl_header1 . $tbl1 . $tbl_footer1, true, true, false, false, '');
				
				//passenger TABLE
			
				$tbl_header = '<div><table style="width: 638px;" cellspacing="0" border="0" >';
				$tbl_footer = '</table></div>';
				$tbl = '';

				$tbl .= '<tr style="background-color:#F2F2F2">
					        <td colspan="2" style="width: 70px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black"><strong>Sr</strong></td>
					        <td colspan="3" style="width: 200px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black"><strong>Details.</strong></td>
					        <td colspan="3" style="width: 140px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black"><strong>Paid</strong></td>
					        <td colspan="2" style="width: 110px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black"><strong>Discount</strong></td>
				    	</tr>
				    	<tr>
				    		<td colspan="10"></td>
				    	</tr>';
				    	$passQry	=	"SELECT D.ID,D.voucherType,D.invoiceNo,D.payingAmount,D.discount
										   FROM ".TABLE_CASH_PAYMENT." D
										  WHERE	D.voucherNo=$invoiceNo
										  AND paymentType='CASH'
										   AND proId='$proId'
									   
										";
										//echo $passQry;die;
						$pass		=	mysql_query($passQry);
						$passNum	=	mysql_num_rows($pass);
						$defRow		=	8;	//for showing constant height for the table
						$i			=	0;
						$sum		=	0;
						$dis		=   0;
				if($passNum>0)
				{
					while($passRow=mysql_fetch_array($pass))
					{
						$invNo	=	$passRow['invoiceNo'];//echo $invNo;
						$vType = $passRow['voucherType'];
						$sum	=	$sum+$passRow['payingAmount'];
						$dis=$dis+$passRow['discount'];
						
						switch ($vType) {
							case "airline" :
								$voucherType = "Airline Receivable";
								break;
							case "hotel" :
								$voucherType = "Hotel Receivable";
								break;
							case "miscellaneous" :
								$voucherType = "Misc Receivable";
								break;
							case "transportation" :
								$voucherType = "Transport Receivable";
								break;
							case "visa" :
								$voucherType = "Visa Receivable";
								break;
							case "package" :
								$voucherType = "Package Receivable";
								break;
							}
						$passQry2	=	"SELECT D.details
										 FROM ".TABLE_TRANSACTION." D
										 WHERE D.voucherNo=$invNo
										 AND D.voucherType='$voucherType'
										 AND proId='$proId'";
										//echo $passQry;
						$pass2		=	mysql_query($passQry2);
						$passRow2	=	mysql_fetch_array($pass2);
						$details	=	$passRow2['details'];//echo $details;
						
						$tbl .=	'<tr>
							        <td colspan="2">'.++$i.'</td>
							        <td colspan="3">'.$details.'</td>
							        <td colspan="3">'.sprintf("%0.2f",round($passRow['payingAmount'],2)).'</td>
							        <td colspan="2">'.sprintf("%0.2f",round($passRow['discount'],2)).'</td>
							    </tr>
			    				';
					}
				}//die;
				$totalAmount = $sum - $dis;
				if($passNum<$defRow)
				{
					$staticRow	=	$defRow-$passNum;
					for($i=0;$i<$staticRow;$i++)
					{
						$tbl.='<tr>
						       		<td colspan="10"></td>
						       </tr>';
					}
				}
				$tbl 	.= 	'<tr><td colspan="10" style="border-bottom: 0.1px solid black;" ></td></tr>
					    	<tr>
					    		<td colspan="2">Narration :</td>
					    		<td colspan = "3">'.$vou_record['remark'].'</td>
					    		<td colspan="3" align="right">Amount</td>
					    		<td colspan="2" align="left">'.sprintf("%0.2f",round($totalAmount,2)).'</td>
					    	</tr>
					    	<tr><td colspan="10">'.ucfirst($App->amountToWord(round($totalAmount,2))).' </td></tr>
					    	<tr><td colspan="10" align="left">By Cash</td></tr>';
				
		
				$pdf->SetFontSize(9);
				$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, true, false, false, '');
				
				//terms and conditions
				$tbl_header2 = '<div><table style="width: 530px;" cellspacing="0" border="0" >';
				$tbl_footer2 = '</table></div>';
				$tbl2 	= 	'';
				
				$tbl2	.=	'<tr>
								<td  colspan="3" style="border-top: 0.1px solid black" >Terms :</td>
								<td colspan="2" style="border-top: 0.1px solid black" align="right"></td>
								<td style="border-top: 0.1px solid black"></td>
								<td colspan="1" style="border-top: 0.1px solid black"></td>
								<td colspan="3" style="border-top: 0.1px solid black" align="right" >for '.strtoupper($company).'</td>
							</tr>
							<tr>
								<td colspan="6" >
								# Subject to KOZHIKODE jurisdiction.<br>
								# Without original Receipt no refund is permissible.<br>
								# Kindly check all details carefully to avoid un-necessary complications.<br>
								
								</td>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="6">Receivers Signature</td>
								
								<td colspan="4"  align="right">Authorized Signatory</td>
							</tr>
							<tr>
								<td colspan="6" style=" border-bottom: 0.1px solid black">
								</td>
								<td colspan="4" style=" border-bottom: 0.1px solid black"></td>
							</tr>
							
							<tr>
								<td colspan="10" align="center">This is a Computer generated document and does not require any signature.
								</td>
							</tr>
							';
				$pdf->SetFontSize(7);
				$pdf->writeHTML($tbl_header2 . $tbl2 . $tbl_footer2, true, true, false, false, '');
				
				
				// ---------------------------------------------------------
				
				// Close and output PDF document
				$pdf->Output("'cash reciept'.$invoiceNo.pdf", 'I');		
		break;
}
?>