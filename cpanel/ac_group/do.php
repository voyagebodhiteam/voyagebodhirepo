<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="" || $_SESSION['travelType']!='Admin'){
	header("location:../../logout.php");
}


$proId = $_SESSION['proId'];
$loginId = $_SESSION['travelId'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype){
	// NEW SECTION
	case 'index':

	if(!$_REQUEST['acGroup']){
		$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
		header("location:index.php");
	} else{
		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		$db->connect();
		$success = 0;

		$parent = $App->convert($_REQUEST['parent']);
		$acGroup = $App->convert($_REQUEST['acGroup']);
		$acGroup = ucwords(strtolower($acGroup));


		$existId = $db->existValuesId(TABLE_ACCOUNT_GROUP, "acGroup='$acGroup' and parent='$parent'");
		if($existId > 0){
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
			header("location:index.php");
		} else{

			$data['acGroup'] = $acGroup;
			$data['parent'] = $parent;
			$data['proId'] = $proId;
			$data['loginId'] = $loginId;

			$success = $db->query_insert(TABLE_ACCOUNT_GROUP, $data);
			$db->close();

			if($success){				
				$_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");																			
			}
			else{
				$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");								
			}	
			header("location:index.php");	
		}									
	}
       
	break;
	// EDIT SECTION
	/*case 'edit':
	$editId = $_REQUEST['id'];
	if (!$_REQUEST['districtName']) {
	$_SESSION['msg'] = "Error, Invalid Details!";
	header("location:edit.php?id=$editId");
	} else {
	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
	$db->connect();
	$success = 0;

	$districtName = $App->convert($_REQUEST['districtName']);
	$districtName = ucwords(strtolower($districtName));

	$existId = $db->existValuesId(TABLE_DISTRICT, "districtName='$districtName' and ID!=$editId");
	if ($existId > 0) {
	$_SESSION['msg'] = "District Name Is Already Exist";
	header("location:new.php");
	} else {

	$data['districtName'] = $name;
	$data['districtName'] = $districtName;

	$success = $db->query_update(TABLE_DISTRICT, $data, " ID='{$editId}'");
	$db->close();

	if ($success) {
	$_SESSION['msg'] = "District Details updated Successfully";
	header("location:new.php");
	} else {
	$_SESSION['msg'] = "Failed";
	header("location:new.php");
	}
	}
	}


	break;*/
	// DELETE SECTION
	case 'delete':
	$deleteId = $_REQUEST['id'];//echo $deleteId;die;
	$success = 0;

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
	$db->connect();

	try{
		$success = @mysql_query("DELETE FROM `" . TABLE_ACCOUNT_GROUP . "` WHERE ID='{$deleteId}'");
	} 
	catch(Exception $e){
		$_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");	
	}
	$db->close();
	if($success){
		$_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
	} else{
		$_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
	}
	header("location:index.php");
	break;
}
?>