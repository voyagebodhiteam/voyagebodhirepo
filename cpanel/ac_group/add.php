<?php
require('../admin_header.php');

if($_SESSION['travelId']=="" || $_SESSION['travelType']!='Admin'){
	header("location:../../logout.php");
}

if(@isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form method="post" action="do.php?op=index" class="default_form" onsubmit="return check()">
				<div class="bd_panel_head">
					<h3>ACCOUNT GROUP</h3>
				</div>
				<div class="bd_panel_body">
					<div class="panel_row">
						<div class="panel_col">
							<label>master group <span class="valid">*</span></label>
						</div>
						<div class="panel_col">
							<select name="parent" required="">
								<option value="">Select</option>
								<?php
								$select = "select * from " . TABLE_ACCOUNT_GROUP . " order by acGroup";
								$res = $db->query($select);
								while($row = mysql_fetch_array($res)){
									?>
									<option value="<?php echo $row['ID']; ?>"><?php echo $row['acGroup']; ?></option>
									<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="panel_row">
						<div class="panel_col">
							<label>account group name <span class="valid">*</span></label>
						</div>
						<div class="panel_col">
							<input type="text" name="acGroup" required="">
						</div>
					</div>
				</div>
				<div class="bd_panel_footer">
					<div class="panel_row">
						<div class="panel_col_full">
							<input type="submit" name="form" value="SAVE">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
