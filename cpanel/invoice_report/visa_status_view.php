<?php
require("../admin_header.php");


if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}


if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"a.proId='$proId'";
}
$logId	=	$_SESSION['travelId'];
//echo $logId;
if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';

$visaId			=	$_REQUEST['id'];
$editSelect		=	"SELECT * FROM ".TABLE_INVOICE_VISA.",".TABLE_ACCOUNTS." 
					 WHERE ".TABLE_INVOICE_VISA.".ID='$visaId' and ".TABLE_INVOICE_VISA.".customerId=".TABLE_ACCOUNTS.".ID";
$editResult 	= 	$db->query($editSelect);
$editRow 		= 	mysql_fetch_array($editResult);
$visaInvoice	= 	$editRow['invoiceNo'];
$cusName	= 	$editRow['accountName'];
$tabLoginID	= $editRow['loginId'];
//echo $tabLoginID;
?>
<script>
	function fnExcelReport()
    {
    	var visa	= document.getElementById('visaExcel').value;
		window.location.href = 'visaExcel.php?id='+visa;
	}
</script>

<div class="bd_panel bd_panel_default bd_panel_shadow">
        <div class="bd_panel_head">
            <h3><?php echo "VI".$visaInvoice; ?></h3>
        </div>
        <div class="bd_panel_head">
            <h5><?php echo "Customer Name : ".$cusName; ?></h5>
        </div>
        
        <div class="bd_panel_head">
			 <input type="hidden" value="<?php echo $visaId; ?>" id="visaExcel"/>
             <button class="export" onclick="fnExcelReport();" title="Export To Excel" type="button"></button>
            
        	</div>
        
        <form class="default_form"  id="package_reg_form" method="post" action="visa_status_do.php?op=edit">
        <input type="hidden" name="visaId"  value="<?php echo $visaId;?>">
        
        <div class="bd_panel_body">
            <div id="package_dynamic">

				<?php
					$passengerQry	=	"SELECT a.ID,a.passengerName,a.dob,a.passportNo,a.loginId,
												a.submissionDate,a.collectionDate,a.dispatchDate,
												a.travelDate,a.visaMessage,a.status
			                               FROM ".TABLE_VISA_PASSENGER." a
				                          WHERE a.visaId=$visaId AND $check
				                          ORDER BY a.ID DESC";
					$passengerResult=	mysql_query($passengerQry);
					$passengerNum	=	mysql_num_rows($passengerResult);
					// echo $passengerNum;die;
					?>
					<div class="package_dynamic_box pack_dynamic_transportation">
			        <h3 class="pack_dynamic_head">Passengers</h3>

			        <div class="pack_transportation_table_wrap">
			        
			        <input type="hidden" name="number" id="number" value="<?php echo @$passengerNum; ?>" /> 
			            <table class="table table-bordered bd_table show_table" id="package_status_table">
			                <thead>
			                <tr>
			                    <th>Sl No</th>
			                    <th>Invoice No</th>
			                    <th>Passenger</th>
			                    <th>Passport No</th>
			                    <th>Customer Name</th>
			                    <th>Submission Date</th>
			                    <th>Collection Date</th>
			                    <th>Dispatch Date</th>
			                    <th>Travel Date</th>
			                    <th>Status</th>
			                    <th>Visa Message</th>
			                    <th>Action</th>
			                </tr>
			                </thead>
			                <tbody>
			                <?php
			                if($passengerNum==0)
			                {
			                	?>
			                	<tr><td colspan="11" align="center">There is no data in list. </td></tr>
			                	<?php
			                }
			                else
			                {
			                $i	=	0;
			                while($passengerRow = mysql_fetch_array($passengerResult)){
			                	$tabLoginID=$passengerRow['loginId'];
			                	//echo $tabLoginID;
			                	?>
			                <tr>
			                    <td><?php echo ++$i;?>
			                    	
			                    	<input type="hidden" name="passengerId_<?php echo $i;?>" id="passengerId_<?php echo $i;?>"  data-field="passenger_id" value="<?php echo $passengerRow['ID'];?>">
			                    </td>
			                    
			                    <td>
			                        <input type="text" name="invoiceNo_<?php echo $i;?>" id="invoiceNo_<?php echo $i;?>" value="<?php echo "VI".$visaInvoice; ?>" readonly="">
			                    </td>
			                    <td>
			                        <input type="text" name="passanger_<?php echo $i;?>" id="passanger_<?php echo $i;?>" value="<?php echo $passengerRow['passengerName']; ?>" readonly="">
			                    </td>
			                    <td>
			                        <input type="text" name="passNo_<?php echo $i;?>" id="passNo_<?php echo $i;?>" value="<?php echo $passengerRow['passportNo']; ?>" readonly="">
			                    </td>
			                    <td>
			                        <input type="text" name="cusName_<?php echo $i;?>" id="cusName_<?php echo $i;?>" value="<?php echo $cusName; ?>" readonly="" >
			                    </td>
			                    <td>
			                        <input type="text" data-field="subDate" class="user_date" name="subDate_<?php echo $i;?>" id="subDate_<?php echo $i;?>" value="<?php if($passengerRow['submissionDate']){echo $App->dbFormat_date($passengerRow['submissionDate']);} ?>" >
			                    </td>
			                    
			                    <td>
			                        <input type="text" data-field="collDate" class="user_date" name="collDate_<?php echo $i;?>" id="collDate_<?php echo $i;?>" value="<?php if($passengerRow['collectionDate']){echo $App->dbFormat_date($passengerRow['collectionDate']); }?>" >
			                    </td>
			                    <td>
			                        <input type="text" data-field="disDate" class="user_date" name="disDate_<?php echo $i;?>" id="disDate_<?php echo $i;?>" value="<?php if($passengerRow['dispatchDate']){echo $App->dbFormat_date($passengerRow['dispatchDate']);} ?>">
			                    </td>
			                    <td>
			                    	 <input  type="text" class="user_date" name="travelDate_<?php echo $i;?>" id="travelDate_<?php echo $i;?>" value="<?php if($passengerRow['travelDate']){ echo $App->dbFormat_date($passengerRow['travelDate']);}?>"/>
			                    </td>
			                    <td>
			                        <select name="status_<?php echo $i;?>" id="status_<?php echo $i;?>" >
			                      	<option <?php if($passengerRow['status']=='Under Process'){ echo "selected"; }?>>Under Process</option>
			                      	<option <?php if($passengerRow['status']=='Approved'){ echo "selected"; }?> >Approved</option>
			                      	<option <?php if($passengerRow['status']=='Declined'){ echo "selected";}?> >Declined</option>
			                      </select >
			                    </td>
			                    <td>
			                    
			                      <select name="message_<?php echo $i;?>" id="message_<?php echo $i;?>" <?php if($passengerRow['visaMessage']=='Yes'){ echo 'disabled';} ?> >
			                       	<option <?php if($passengerRow['visaMessage']=='No'){ echo "selected";}?> value="No">No</option>
			                       	<option <?php if($passengerRow['visaMessage']=='Yes'){ echo "selected";}?> value="Yes">Yes</option>
			                       </select>
			                     
			                    </td>
			                    <td>
			                        <a class="show_table_lnk show_table_lnk_view" href="../invoice_visa/view.php?id=<?php echo $visaId; ?>">View</a>
			                    </td>
			                    
			                </tr>
			                <?php } }?>
			                </tbody>
			            </table>
			        </div>
			    </div>
				
		   <!---------------------------------------- End --------------------------------------->

            </div>
            
            <div class="airline_submit">
                <input type="hidden" name="pack_passenger_count" id="pack_reg_air_row_count" value="<?php echo $passengerNum; ?>">
                <?php  if($tabLoginID==$logId){?>
                <button type="submit" class="bd_btn bd_btn_green" >Update All</button>
                
                <?php } ?>
            </div>
        </div>
    </form>
</div>

<!-- Popups -->
    <!--Popup for account View-->
    <div id="acs_view_pop" class="modal fade bd_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Passenger Details</h4></div>
                <div class="modal-body">
                    <div class="acs_view_table_wrap table-responsive">
                        <table class="table table-bordered table-striped acs_view_table">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');

require('../admin_footer2.php');
?>


