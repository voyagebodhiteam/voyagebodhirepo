<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}
$loginType 	=	$_SESSION['travelType'];
if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"a.proId='$proId'";
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
	function validDate()
	{
		var date = document.getElementById('date').value;
		
		if((document.getElementById('merge').checked) && (date))
		{
			document.getElementById("searchForm").setAttribute("action", "package_merge.php");
		}
		else if((document.getElementById('merge').checked) && (!date) )
		{
			document.getElementById('dateDiv').innerHTML = "Choose one date";
			return false;
		}
	}
	
	//clear the validation msg
	function clearbox(Element_id)
	{
	document.getElementById(Element_id).innerHTML="";
	}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Package Status</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post"  id="searchForm" onsubmit="return validDate()">
                        	<?php if($loginType	=='Admin'){?>
							<div class="page_search">
								<select name="company" required="" >
									<option value="">Select Company</option>
									<?php $cmpQry	=	mysql_query("SELECT * FROM ".TABLE_COMPANY);
									while($cmpRow		=	mysql_fetch_array($cmpQry))
									{
									?>
									<option value="<?php echo $cmpRow['ID'];?>"  <?php if(@$_REQUEST['company']==$cmpRow['ID']){ echo "selected";}?>><?php echo $cmpRow['companyName'];?></option>
									<?php 	
									}?>
								</select>
							</div>
							<?php }?>
							<div class="page_search">
                        		<input type="text" name="packageName" id="" placeholder="Package Name" value="<?php echo @$_REQUEST['packageName']; ?>">
                        	</div>
                        	<div class="page_search">
                            	<input type="search" name="date" placeholder="Date" id="date" class="user_date" value="<?php echo @$_REQUEST['date']; ?>" onfocus="clearbox('dateDiv')">
                            	<div id="dateDiv" class="valid" style="color:#FF6600;"></div>
                            </div>
                            <div class="page_search" style="min-width: 80px;">
                            	<input  type="checkbox" value="" name="merge" id="merge"/> <br /> By date 
                            </div>
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                 </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    
<?php	
$cond="1";
if(@$_REQUEST['packageName'])
{
	$cond	 =	$cond." and a.packageName LIKE '%".$_REQUEST['packageName']."%' ";
}
if(@$_REQUEST['date'])
{
	$cond	=	$cond." and a.date like '%".$App->dbFormat_date($_REQUEST['date'])."%'";
}
/*if(isset($_REQUEST['merge']))
{
	$mergeDate	=	$_REQUEST['merge'];
	header("location:package_merge.php");
	exit;
}*/

if($loginType=='Admin')
{
	if(@$_REQUEST['company'])
	{
		$proId	=	@$_REQUEST['company'];
	}
	else
	{
		$proId	=	0;
	}
	$check 	= 	"a.proId='$proId'";
}
?>
    
    
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Package Name</th>
                            <th>Ameer</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Passenger Count</th>
                            <th>Room Count</th>
                            <th>Total Passengers</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                     $i=0;
                        $selectAll = "select a.ID,
                        					 a.packageName,
                        					 a.leader,
                        					 a.date,
                        					 a.adultTotal,
                        					 COUNT(b.ID) as passCount
			                          from ".TABLE_PACKAGE." a
			                     LEFT JOIN ".TABLE_INVOICE_PACKAGE_PASSENGER." b ON b.packageId=a.ID
			                          where $cond
			                          and $check
			                          GROUP BY a.ID
			                          order by a.ID desc";
			                          
						//echo $selectAll;
                        $result = $db->query($selectAll);
                        $selectCount = mysql_num_rows($result);
                        
                        if($selectCount==0)
						{
						?>
							 <tr>
								<td align="center" colspan="9">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
                        /*********************** for pagination ******************************/
						$rowsPerPage = ROWS_PER_PAGE;
						if(isset($_GET['page']))
						{
							$pageNum = $_GET['page'];
						}
						else
						{
							$pageNum =1;
						}
						$offset = ($pageNum - 1) * $rowsPerPage;
						$select1=$db->query($selectAll." limit $offset, $rowsPerPage");
						$i=$offset+1;
						//use '$select1' for fetching
						/*************************** for pagination **************************/
						function numPass($cond,$packId)
						{
							$coundQry	=	mysql_query("SELECT COUNT(ID) AS countPass FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." WHERE $cond AND packageId=$packId ");
        					$countRow	=	mysql_fetch_array($coundQry);
        					$count		=	$countRow['countPass'];
        					return($count);
						}
						function numRooms($type,$packId)
						{
							$roomQry	=	mysql_query("SELECT COUNT(".TABLE_INVOICE_PACKAGE_HOTEL.".ID) AS roomCount 
													FROM ".TABLE_INVOICE_PACKAGE_HOTEL." 
													LEFT JOIN ".TABLE_INVOICE_PACKAGE." 
													ON ".TABLE_INVOICE_PACKAGE.".ID=".TABLE_INVOICE_PACKAGE_HOTEL.".in_packageId 
													WHERE ".TABLE_INVOICE_PACKAGE.".packageId= $packId  
													AND ".TABLE_INVOICE_PACKAGE_HOTEL.".roomType=$type 
													");
						
		        			$roomRow	=	mysql_fetch_array($roomQry);
		        			$roomNum	=	$roomRow['roomCount'];
		        			return $roomNum;
						}
                        while ($row = mysql_fetch_array($select1)) {
                        	$packageId	=	$row['ID'];
                        	
                            ?>
                         <tr>                            
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $row['packageName']; ?></td>
                            <td><?php echo $row['leader']; ?></td>
                            <td><?php echo $App->dbformat_date($row['date']); ?></td>
                            <td><?php echo $row['adultTotal']; ?></td>
                            <td>
                            Adult  : <?php echo numPass('age>=12',$packageId); ?><br />
                            Child  : <?php echo numPass('(age>=2 AND age <12)',$packageId); ?><br />
                            Infant : <?php echo numPass('age<2',$packageId); ?><br />
                            Male   : <?php echo numPass("gender='M'",$packageId); ?><br />
                            Female : <?php echo numPass("gender='F'",$packageId); ?><br />
                            
                            </td>
                            <td>
                            Sharing  			: <?php echo numRooms('1',$packageId); ?><br />
                            Twin  				: <?php echo numRooms('2',$packageId); ?><br />
                            Triple 				: <?php echo numRooms('3',$packageId); ?><br />
                            Four Bed   			: <?php echo numRooms('4',$packageId); ?><br />
                            Five Bed 			: <?php echo numRooms('5',$packageId); ?><br />
                            Six Bed				: <?php echo numRooms('6',$packageId); ?><br />	
                            Child With Bed		: <?php echo numRooms('7',$packageId); ?><br />
                            Child Without Bed	: <?php echo numRooms('8',$packageId); ?><br />
                            </td>
                            
                            <td><?php echo $row['passCount']; ?></td>
                            <td><a class="show_table_lnk show_table_lnk_view" href="package_view.php?id=<?php echo $packageId; ?>&proId=<?php echo $proId;?>">View</a>
                            <a class="show_table_lnk show_table_lnk_view" href="package_invoice.php?id=<?php echo $packageId; ?>&proId=<?php echo $proId;?>">Invoice</a>
                            </td>
                            
                        </tr>
                            <?php
                        }
                        }
                        ?>

                    </tbody>
                </table>
            </div>
            
            <!-- paging -->		
            <!--<div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div> -->       
            <!-- paging end-->
            
            <!--*****************************************************************-->
            
            	 <?php 
                  if($selectCount>@$rowsPerPage)
					{
					?>	
					 <br />	
					  <div class="pagerSC" align="center">
					<?php
					
					$query   =  $db->query($selectAll);
					$numrows = mysql_num_rows($query);
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
					   if ($page == $pageNum)
					   {
						  $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
					   }
					   else
					   {
						 	if(@$proId)
					   		 {
							 	$nav .= " <a href=\"$self?page=$page&company=$proId\">$page</a> ";
							 }
							 else
							 {
							 	$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							 }
					   }
					}
					?>
					 <?php
					if ($pageNum > 1)
					{
					   $page  = $pageNum - 1;
					   if(@$proId)
					   {
						   $prev  = " <a href=\"$self?page=$page&company=$proId\">Prev</a> ";
						   $first = " <a href=\"$self?page=1&company=$proId\">First Page</a> ";
					   }
					   else
					   {
						   $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
						   $first = " <a href=\"$self?page=1\">First Page</a> ";
					   }
					}
					else
					{
					   $prev  = '&nbsp;';
					   $first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
					   $page = $pageNum + 1;
					   if(@$proId)
					   {
						   	 $next = " <a href=\"$self?page=$page&company=$proId\">Next</a> ";
						     $last = " <a href=\"$self?page=$maxPage&company=$proId\">Last Page</a> ";
					   }
					   else
					   {
						   	 $next = " <a href=\"$self?page=$page\">Next</a> ";
						   	 $last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
					   }
					}
					else
					{
					   $next = '&nbsp;';
					   $last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>