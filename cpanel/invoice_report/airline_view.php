<?php
require("../admin_header.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if(isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';

$loginType 	=	$_SESSION['travelType'];
if($loginType == 'Admin')
{
	$proId	=	$_REQUEST['proId'];
}
$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ".TABLE_INVOICE_AIRLINE." where ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>

<div class="row">
	<div class="col-lg-12">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form class="default_form" id="invoice_airline" method="post" action="do.php?op=edit">
				<input type="hidden" name="editId" value="<?php  echo $editId;?>">
				<div class="bd_panel_head">
					<h3>Airline - Invoice</h3>
				</div>
				<div class="bd_panel_body">
					<div class="row">
						 <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Invoice No</label>                                     
								<input type="text" name="invoiceNo" value="<?php echo "AI".$editRow['invoiceNo']; ?>" readonly="" disabled="">
							</div>
						</div>
						 <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer Name</label> <!--select from account where accGpId=37--> 
                                    
								<?php
								$cId=$editRow['customerId']; 
								$editSelect2 = 	"select  accountName                          			
								from ".TABLE_ACCOUNTS." 
								where ID='$cId'";
								$editResult2 = 	$db->query($editSelect2);
								$editRow2	 = 	mysql_fetch_array($editResult2);
								?>
                                    
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerName" autocomplete="off" value="<?php echo $editRow2['accountName'];?>" disabled="">
									<input type="hidden" name="customerId" value="<?php echo $editRow['customerId'];?>">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
						 <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Date</label>
								<input type="text" name="invoiceDate" class="user_date" value="<?php echo $App->dbFormat_date($editRow['invoiceDate']); ?>" required="" disabled="">
							</div>
						</div>
					</div>
					
					<div class="form_divider"></div>
					<div class="multi_det">
						<div class="multi_det_body">
							<div class="multi_det_head">
								<h3>Passenger List</h3>
								
								<div class="bd_clear"></div>
							</div>
							<div class="multi_det_table_wrap table-responsive">
								<table class="table table-bordered multi_det_table" id="passenger_table" data-tbl_for="edit">
									<thead>
										<tr>
											<th>Sl No</th>
											<th>Passenger</th>
											<th>Ticket No</th>
											<th>From</th>
											<th>To</th>                                            
											<th>FLT No</th>
											<th>Date</th>
											<th>Fare</th>
											<th>Tax(If percentage,<br>then '%' sign)</th>
											<th>YQ</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php				                        
										$editSelect2 = 	"select  * from ".TABLE_AIRLINE_PASSENGER." where airlineId='$editId'";
										$editResult2 = 	$db->query($editSelect2);
										$rowCountEdit	=	mysql_num_rows($editResult2);
										$i=$p=0;
										while($editRow2	= 	mysql_fetch_array($editResult2)){
											++$p;
											$flightEditId=$editRow2['ID'];
											?>
											<tr>
												<td><span class="sl_no"><?php echo ++$i; ?></span>
													<input type="hidden" name="delID" value="<?= $editRow2['ID']; ?>">
													<input type="hidden" data-field_name="passengerName" name="pass<?php echo $i;?>_passengerName" value="<?php echo $editRow2['passengerName'];?>">
													<input type="hidden" data-field_name="dob" name="pass<?php echo $i;?>_dob" value="<?php echo $App->dbformat_date($editRow2['dob']);?>">	
													<input type="hidden" data-field_name="age" name="pass<?php echo $i;?>_age" value="<?php echo $editRow2['age'];?>">
													<input type="hidden" data-field_name="gender" name="pass<?php echo $i;?>_gender" value="<?php echo $editRow2['gender'];?>">
													<input type="hidden" data-field_name="placeOfBirth" name="pass<?php echo $i;?>_placeOfBirth" value="<?php echo $editRow2['placeOfBirth'];?>">
													<input type="hidden" data-field_name="relationship" name="pass<?php echo $i;?>_relationship" value="<?php echo $editRow2['relationship'];?>">				
													<input type="hidden" data-field_name="nationality" name="pass<?php echo $i;?>_nationality" value="<?php echo $editRow2['nationality'];?>">
													<input type="hidden" data-field_name="mobile" name="pass<?php echo $i;?>_mobile" value="<?php echo $editRow2['mobile'];?>">
													<input type="hidden" data-field_name="passportNo" name="pass<?php echo $i;?>_passportNo" value="<?php echo $editRow2['passportNo'];?>">
													<input type="hidden" data-field_name="dateOfIssue" name="pass<?php echo $i;?>_dateOfIssue" value="<?php echo $App->dbformat_date($editRow2['dateOfIssue']);?>">
													<input type="hidden" data-field_name="validUpTo" name="pass<?php echo $i;?>_validUpTo" value="<?php echo $App->dbformat_date($editRow2['validUpTo']);?>">
													<input type="hidden" data-field_name="issuePlace" name="pass<?php echo $i;?>_issuePlace" value="<?php echo $editRow2['issuePlace'];?>">
												
												
												</td>
												<td><?php echo $editRow2['passengerName'];?>
													<?php				                        
													$editSelect3 = 	"select  * from ".TABLE_AIRLINE_FLIGHT." where airlinePassId='$flightEditId'";
													$editResult3 = 	$db->query($editSelect3);
													$rowCountEdit3	=	mysql_num_rows($editResult3);
													//$flag	=	$rowCountEdit3;
													$j=0;
													while($editRow3	= 	mysql_fetch_array($editResult3)){
														++$j;
													?>												
													<input type="hidden" data-field_name="airlineName_<?php echo $j;?>" name="pass<?php echo $p;?>_airlineName_<?php echo $j;?>" value="<?php echo $editRow3['airlineName'];?>">
													<?php
													}
													$editResult4 = 	$db->query($editSelect3);	
													$j=0;
													while($editRow4	= 	mysql_fetch_array($editResult4)){
														++$j;
													?>
													<input type="hidden" data-field_name="flightNo_<?php echo $j;?>" name="pass<?php echo $p;?>_flightNo_<?php echo $j;?>" value="<?php echo $editRow4['flightNo'];?>">
													<?php
													}
													$editResult5 = 	$db->query($editSelect3);	
													$j=0;
													while($editRow5	= 	mysql_fetch_array($editResult5)){
														++$j;
													?>
													<input type="hidden" data-field_name="ticketNo_<?php echo $j;?>" name="pass<?php echo $p;?>_ticketNo_<?php echo $j;?>" value="<?php echo $editRow5['ticketNo'];?>">
													<?php
													}
													$editResult6 = 	$db->query($editSelect3);	
													$j=0;
													while($editRow6	= 	mysql_fetch_array($editResult6)){
														++$j;
													?>
													<input type="hidden" data-field_name="journeyDate_<?php echo $j;?>" name="pass<?php echo $p;?>_journeyDate_<?php echo $j;?>" value="<?php echo $App->dbformat_date($editRow6['journeyDate']);?>">
													<?php
													}
													$editResult7 = 	$db->query($editSelect3);	
													$j=0;
													while($editRow7	= 	mysql_fetch_array($editResult7)){
														++$j;
													?>
													<input type="hidden" data-field_name="sectorFrom_<?php echo $j;?>" name="pass<?php echo $p;?>_sectorFrom_<?php echo $j;?>" value="<?php echo $editRow7['sectorFrom'];?>">
													<?php
													}
													$editResult8 = 	$db->query($editSelect3);	
													$j=0;
													while($editRow8	= 	mysql_fetch_array($editResult8)){
														++$j;
													?>
													<input type="hidden" data-field_name="sectorTo_<?php echo $j;?>" name="pass<?php echo $p;?>_sectorTo_<?php echo $j;?>" value="<?php echo $editRow8['sectorTo'];?>">
													<?php
													}
													$editResult9 = 	$db->query($editSelect3);	
													$j=0;
													while($editRow9	= 	mysql_fetch_array($editResult9)){
														++$j;
													?>
													<input type="hidden" data-field_name="classType_<?php echo $j;?>" name="pass<?php echo $p;?>_classType_<?php echo $j;?>" value="<?php echo $editRow9['classType'];?>">
													<?php
													}
													$editResult10 = 	$db->query($editSelect3);	
													$j=0;
													while($editRow10	= 	mysql_fetch_array($editResult10)){
														++$j;
													?>
													<input type="hidden" data-field_name="fare_<?php echo $j;?>" name="pass<?php echo $p;?>_fare_<?php echo $j;?>" value="<?php echo $editRow10['fare'];?>">
													<?php
													}
													$editResult11 = 	$db->query($editSelect3);	
													$j=0;
													while($editRow11	= 	mysql_fetch_array($editResult11)){
														++$j;
													?>
													<input type="hidden" data-field_name="tax_<?php echo $j;?>" name="pass<?php echo $p;?>_tax_<?php echo $j;?>" value="<?php echo $editRow11['tax'];?>">
													<?php
													}
													$editResult12 = 	$db->query($editSelect3);	
													$j=0;
													while($editRow12	= 	mysql_fetch_array($editResult12)){
														++$j;
													?>
													<input type="hidden" data-field_name="yq_<?php echo $j;?>" name="pass<?php echo $p;?>_yq_<?php echo $j;?>" value="<?php echo $editRow12['yq'];?>">
													<?php
													}
													$editResult13 = 	$db->query($editSelect3);	
													$j=0;
													while($editRow13	= 	mysql_fetch_array($editResult13)){
														++$j;
													?>
													<input type="hidden" data-field_name="procCharge_<?php echo $j;?>" name="pass<?php echo $p;?>_procCharge_<?php echo $j;?>" value="<?php echo $editRow13['procCharge'];?>">
												<?php
													}
													
													?>
													<input type="hidden" name="<?php echo "pass".$p."_airlineRowCount" ?>" value="<?php echo $rowCountEdit3;?>">
												</td>
												<?php
												
												?>
												<td><?php $editResult14 = 	$db->query($editSelect3);
													while($editRow14	= 	mysql_fetch_array($editResult14))   															{?>
													<span class="air_block"><?php echo $editRow14['ticketNo'];?></span><?php }?></td>
												<td><?php $editResult15 = 	$db->query($editSelect3);
													while($editRow15	= 	mysql_fetch_array($editResult15))															{ ?>
													<span class="air_block"><?php echo $editRow15['sectorFrom'];?></span><?php }?></td>
												<td><?php $editResult16 = 	$db->query($editSelect3);
													while($editRow16	= 	mysql_fetch_array($editResult16))															{  ?>
													<span class="air_block"><?php echo $editRow16['sectorTo'];?></span><?php }?></td>
											
												<td><?php $editResult17 = 	$db->query($editSelect3);
													while($editRow17	= 	mysql_fetch_array($editResult17))															{  ?>
													<span class="air_block"><?php echo $editRow17['flightNo'];?></span><?php }?></td>
												<td><?php $editResult18 = 	$db->query($editSelect3);
													while($editRow18	= 	mysql_fetch_array($editResult18))															{ ?>
													<span class="air_block"><?php  echo $App->dbformat_date($editRow18['journeyDate']);?></span><?php }?></td>
												<td><?php $editResult19 = 	$db->query($editSelect3);
													while($editRow19	= 	mysql_fetch_array($editResult19))															{  ?>
													<span class="air_block"><?php echo $editRow19['fare'];?></span><?php }?></td>
												<td><?php $editResult20 = 	$db->query($editSelect3);
													while($editRow20	= 	mysql_fetch_array($editResult20))															{  ?>
													<span class="air_block"><?php echo $editRow20['tax'];?></span><?php }?></td>
												<td><?php $editResult21 = 	$db->query($editSelect3);
													while($editRow21	= 	mysql_fetch_array($editResult21))															{ ?>
													<span class="air_block"><?php  echo $editRow21['yq'];?></span><?php }?></td>
												<td>
													<a class="show_table_lnk show_table_lnk_view" href="#">View</a>
													
												</td>
												<?php	
												//}
												?>
											</tr>
											<?php											
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
						 <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>TDS</label>
								<input type="text" name="tds" value="<?php echo $editRow['tds']; ?>" disabled="">
							</div>
						 </div>
						 <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Processing Charge</label>
								<input type="text" name="mainProcCharge" value="<?php echo $editRow['mainProcCharge']; ?>" disabled="">
							</div>
						  </div>
						  <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Other Charge</label>
								<input type="text" name="mainOtherCharge" value="<?php echo $editRow['mainOtherCharge']; ?>" disabled="">
							</div>
						  </div>
						 <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Tax ('%' for percentage)</label>
								<input type="text" name="mainTax" value="<?php echo $editRow['mainTax']; ?>" disabled="">
							</div>
						 </div>
						 <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Discount</label>
								<input type="text" name="discount" value="<?php echo $editRow['discount']; ?>" disabled="">
							</div>
						 </div>
						 <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Net Amount</label>
								<input type="text" name="netAmount" value="<?php echo $editRow['netAmount']; ?>" disabled="">
							</div>
						</div>
						
						 <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Description</label>
								<textarea name="description" disabled=""><?php echo $editRow['description']; ?></textarea>
							</div>
						  </div>
						  <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Remark</label>
								<textarea name="remark" disabled=""><?php echo $editRow['remark']; ?></textarea>
							</div>
						  </div>
					</div>
					<div class="row">
						
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
require('../admin_footer1.php');
?>
<!-- Popups -->
<div id="multi_det_add" class="modal fade bd_modal passenger_pop" role="dialog">
	<div class="modal-dialog modal-lg full_modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Passenger Details</h4>
			</div>
			<div class="modal-body">
				<form class="default_form">
					<div class="pop_section">
						<h3 class="pop_section_head">Basic Details</h3>
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Passenger</label>
									<div class="aj_rel_box  aj_passenger">
										<input class="pass_to_table" type="text" name="passengerName" autocomplete="off" disabled="">
										<ul class="aj_live_items">

										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Date of Birth</label>
									<input class="pass_to_table user_date" type="text" name="dob" disabled="">
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Age</label>
									<input class="pass_to_table" type="text" name="age" disabled="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Gender</label>
									<select class="pass_to_table" name="gender">
										<option value="male" selected disabled="">Male</option>
										<option value="female">Female</option>
										<option value="other">Other</option>
									</select>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Mobile</label>
									<input class="pass_to_table" type="text" name="mobile" disabled="">
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Place of Birth</label>
									<input class="pass_to_table" type="text" name="placeOfBirth" disabled="">
								</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Relationship</label>
									<input class="pass_to_table" type="text" name="relationship" disabled="">
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Nationality</label>
									<input class="pass_to_table" type="text" name="nationality" disabled="">
								</div>
							</div>
						</div>
					</div>
					<div class="pop_section">
						<h3 class="pop_section_head">Passport Details</h3>
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Passport No</label>
									<input class="pass_to_table" type="text" name="passportNo" disabled="">
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Date of Issue</label>
									<input class="pass_to_table user_date" type="text" name="dateOfIssue" disabled="">
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Valid Up To</label>
									<input class="pass_to_table user_date" type="text" name="validUpTo" disabled="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="form_block">
									<label>Issue Place</label>
									<input class="pass_to_table" type="text" name="issuePlace" disabled="">
								</div>
							</div>
						</div>
					</div>
					<div class="multi_det_table_wrap table-responsive airline_table">
						<table class="table table-bordered" id="inv_airline_multiple">
							<thead>
								<tr>
									<th colspan="12" valign="middle">Airline Details</th>
									
								</tr>
								<tr>
									<th>Sl No</th>
									<th>Name</th>
									<th>Flight No</th>
									<th>Ticket No</th>
									<th>Journey Date</th>
									<th>Sector From</th>
									<th>Sector TO</th>
									<th>Class</th>
									<th>Fare</th>
									<th>Tax</th>
									<th>YQ</th>
									<th>Proc Charge</th>
									
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>
										<select name="airlineName_1" class="pass_to_table_air" disabled="">
											<option value="">Select</option>
											<?php
											$select = "select * from " . TABLE_FLIGHT . " where proId=$proId";
											$res = $db->query($select);
											while($row = mysql_fetch_array($res)){
												?>
												<option value="<?php echo $row['ID']; ?>"><?php echo $row['flightName']; ?></option>
												<?php
											}
											?>
										</select>
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="flightNo_1" disabled="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="ticketNo_1" disabled="">
									</td>
									<td>
										<input class="pass_to_table_air user_date" type="text" name="journeyDate_1" disabled="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="sectorFrom_1" disabled="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="sectorTo_1" disabled="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="classType_1" disabled="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="fare_1" disabled="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="tax_1" disabled="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="yq_1" disabled="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="procCharge_1" disabled="">
									</td>
									
								</tr>
							</tbody>
						</table>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="trigger_pass_table">OK</button>
			</div>
		</div>
	</div>
</div>

<!-- div for select box for airline details -->
<div id="airline_select_options" style="display: none">
	<table>
		<tbody>
			<tr>
				<td></td>
				<td>
					<select name="airlineName" class="pass_to_table_air">
						<option value="">Select</option>
						<?php
						$select = "select * from " . TABLE_FLIGHT . " where proId=$proId";
						$res = $db->query($select);
						while($row = mysql_fetch_array($res)){
							?>
							<option value="<?php echo $row['ID']; ?>"><?php echo $row['flightName']; ?></option>
							<?php
						}
						?>
					</select>
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="flightNo">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="ticketNo">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" data-dp="datepick" name="journeyDate">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="sectorFrom">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="sectorTo">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="classType">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="fare">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="tax">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="yq">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="procCharge">
				</td>
				
			</tr>
		</tbody>
	</table>
</div>
<!-- div for select box for airline details -->
<?php
require('../admin_footer2.php');
?>