<?php
require("../admin_header.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';
$i = 0;
$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ".TABLE_INVOICE_TRANSPORT." where ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);

$loginType 	=	$_SESSION['travelType'];
if($loginType == 'Admin')
{
	$proId	=	$_REQUEST['proId'];
}

?>
    <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form class="default_form" id="invoice_hotel" method="post" action="do.php?op=edit">
                 <input type="hidden" name="editId" value="<?php  echo $editId;?>">
                    <div class="bd_panel_head">
                        <h3>Transportation - Invoice</h3>
                    </div>
                    <div class="bd_panel_body">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Invoice No</label>                                    
                                    <input type="text" name="invoiceNo" value="<?php echo "TI" . $editRow['invoiceNo']; ?>"  required="" readonly="" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Date</label>
                                    <input class="user_date" type="text" name="invoiceDate" value="<?php echo $App->dbFormat_date($editRow['invoiceDate']); ?>" required="" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">                                    
                                    <label>Customer Name</label> <!--select from account where accGpId=37--> 
                                    <?php
                                    $cId=$editRow['customerId']; 
                                    $editSelect2 = 	"select  accountName                                    				
                                    				 from ".TABLE_ACCOUNTS." 
                                    				 where ID='$cId'";
									$editResult2 = 	$db->query($editSelect2);
									$editRow2	 = 	mysql_fetch_array($editResult2);
                                    ?>
                                    <input type="text" name="customerName" value="<?php echo $editRow2['accountName'];?>" disabled="">
                                    <input type="hidden" name="customerId" value="<?php echo $editRow['customerId'];?>">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer ID</label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerId" autocomplete="off" required="" value="<?php echo $editRow['customerId'];?>" disabled="">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Service Provider</label>
                                    <select name="serviceProviderId" class="pass_to_table" disabled="">
                                        <option value="">Select</option>
                                        <?php
                                        $existId=$editRow['serviceProviderId'];
                                        $select = "select * from " . TABLE_TRANSPORT_PROVIDER." where ID='$existId'";
                                        $res = $db->query($select);
                                        while ($row = mysql_fetch_array($res)) {
                                            ?>
                                            <option value="<?php echo $row['ID']; ?>" <?php if($row['ID']==$editRow['serviceProviderId']){ echo 'selected'; }?>><?php echo $row['providerName']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            
                           
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form_section_head"><h3>No of persons</h3></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Adults</label>
                                    <input type="text" name="adults" required="" value="<?php echo $editRow['adults'];?>"  disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Childrens</label>
                                    <input type="text" name="childrens" required="" value="<?php echo $editRow['childrens'];?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Total persons</label>
                                    <input type="text" name="totalPersons" required="" value="<?php echo $editRow['totalPersons'];?>" disabled="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form_section_head"><h3>No of days</h3></div>
                            </div>
                        </div>
                       <div class="row">
                       
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>From</label>
                                    <input type="text" name="fromDate" required=""  class="user_date" value="<?php echo $App->dbFormat_date($editRow['fromDate']);?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>To</label>
                                    <input type="text" name="toDate" required=""  class="user_date" value="<?php echo $App->dbFormat_date($editRow['toDate']);?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Total Days</label>
                                    <input type="text" name="totalDays" required="" value="<?php echo $editRow['totalDays'];?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Hire vehicle</label>
                                    <select name="vehicleType" disabled="">
                                    	<option value="">Select</option>
                                    	
                                    	 <?php
                                    	$existId=$editRow['vehicleType'];
                                        $select2 = "select * from " . TABLE_VEHICLE_TYPES ." where ID='$existId'";
                                        $res2 = $db->query($select2);
                                        while ($row2 = mysql_fetch_array($res2)) {
                                            ?>
                                        <option value="<?php echo $row2['ID']; ?>" <?php if($editRow['vehicleType']==$row2['ID']){ echo 'selected'; }?>><?php echo $row2['vehicleType']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                      
                       <div class="row">
                         <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Fare</label>
                                    <input type="text" name="fare" id="fare" onfocus="clearbox('farediv')" value="<?php echo $editRow['fare'];?>" disabled="" >
                                    <div id="farediv" class="valid" style="color:#FF6600;"></div>
                                </div>
                            </div>
                          <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Processing charges</label>
                                    <input type="text" name="proCharge" id="proCharge" value="<?php echo $editRow['proCharge'];?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charges</label>
                                    <input type="text" name="otherCharges" id="otherCharges" value="<?php echo $editRow['otherCharges'];?>" disabled="">
                                </div>
                            </div>
                         	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Net Amount</label>
                                    <input type="text" name="netAmount" required=""  value="<?php echo $editRow['netAmount'];?>" disabled="">
                                </div>
                            </div>
                           
                            
                        </div>
                        <div class="row">
                            
                            <div class="col-lg-2 col-md-2 col-sm-2" style="display: none">
                                <div class="form_block">
                                    <label>KM Allowed</label>
                                    <input type="text" name="kmAllowed" value="<?php echo $editRow['kmAllowed'];?>" disabled="">
                                </div>
                            </div>
                        </div>
                        <div class="row">                            
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Arrival Details</label>
                                    <textarea name="arrivalDetails"  disabled=""><?php echo $editRow['arrivalDetails'];?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Departure Details</label>
                                    <textarea name="DepartureDetails" disabled=""><?php echo $editRow['DepartureDetails'];?></textarea>
                                </div>
                            </div>
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Route</label>
                                    <textarea name="route"  disabled=""><?php echo $editRow['route'];?></textarea>
                                </div>
                            </div>
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Inclusive</label>
                                    <textarea name="inclusive"  disabled=""> <?php echo $editRow['inclusive'];?></textarea>
                                </div>
                            </div>
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                 <div class="form_block">
                                    <label>Remark</label>
                                    <textarea name="remark"  disabled=""><?php echo $editRow['remark'];?></textarea>
                                </div>
                            </div>
                        </div>
                        
                         <div class="row">
                           
                         </div>
                        
                       
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
    <script>
        $(function () {
            $('.user_date').each(function () {
                var curDatePicker = $(this);
                //alterTarget = curDatePicker.nextAll('.date_hidden');
                curDatePicker.datepicker({
                    dateFormat: "dd-mm-yy"
                    /*altFormat: "yy-mm-dd",
                     altField: alterTarget*/
                });
            });
        });
    </script>
<?php
require('../admin_footer2.php');
?>