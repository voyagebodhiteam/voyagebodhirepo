<?php
require("../admin_header.php");


if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$loginType 	=	$_SESSION['travelType'];
if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"a.proId='$proId'";
}

if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';

$packId			=	$_REQUEST['id'];
$editSelect		=	"SELECT * FROM ".TABLE_PACKAGE." WHERE ID='$packId'";
$editResult 	= 	$db->query($editSelect);
$editRow 		= 	mysql_fetch_array($editResult);
$services 		= 	explode(",",$editRow['services']);

$loginType 	=	$_SESSION['travelType'];
if($loginType == 'Admin')
{
	$proId	=	$_REQUEST['proId'];
}
?>
<script >
	function fnExcelReport()
    {
    	var package	= document.getElementById('pacExcel').value;
		window.location.href = 'excel.php?id='+package;
	}
</script>
<div class="bd_panel bd_panel_default bd_panel_shadow">
        <div class="bd_panel_head">
            <h3><?php echo $editRow['packageName']; ?></h3>
            
<!--            <button class="export" onclick="fnExcelReport();" title="Export To Excel" type="button"></button>
-->
			 <div class="page_controls">
			 <input type="hidden" value="<?php echo $packId; ?>" id="pacExcel"/>
             <button class="export" onclick="fnExcelReport();" title="Export To Excel" type="button"></button>
            
        	</div>
        	<div class="bd_clear"></div>
        </div>
       
        <!--<div class="page_controls">
        	<div class="page_search">
				<button class="export" onclick="fnExcelReport();" title="Export To Excel" type="button"></button>
			</div>
        </div>-->
        <form class="default_form"  id="package_reg_form" method="post" action="package_do.php?op=edit">
        <input type="hidden" name="packageId" data-field="package_id" value="<?php echo $packId;?>">
        <div class="bd_panel_body">
        	<?php 
        	$adultQry	=	mysql_query("SELECT COUNT(ID) AS adult FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." WHERE age>=12 AND packageId IN($packId)");
        	$adultRow	=	mysql_fetch_array($adultQry);
        	
        	$childQry	=	mysql_query("SELECT COUNT(ID) AS child FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." WHERE (age>=2 AND age <12) AND packageId IN($packId)");
        	$childRow	=	mysql_fetch_array($childQry);
        	
        	$infantQry	=	mysql_query("SELECT COUNT(ID) AS infant FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." WHERE age<2 AND packageId IN($packId)");
        	$infantRow	=	mysql_fetch_array($infantQry);
        	
        	$maleQry	=	mysql_query("SELECT COUNT(ID) AS male FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." WHERE gender='M' AND packageId IN($packId)");
        	$maleRow	=	mysql_fetch_array($maleQry);
        	
        	$femaleQry	=	mysql_query("SELECT COUNT(ID) AS female FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." WHERE gender='F' AND packageId IN($packId)");
        	$femaleRow	=	mysql_fetch_array($femaleQry);

			if(in_array('hotel',$services))
			{
				function numRooms($type,$packId)
				{
					$roomQry	=	mysql_query("SELECT COUNT(".TABLE_INVOICE_PACKAGE_HOTEL.".ID) AS roomCount 
											FROM ".TABLE_INVOICE_PACKAGE_HOTEL." 
											LEFT JOIN ".TABLE_INVOICE_PACKAGE." 
											ON ".TABLE_INVOICE_PACKAGE.".ID=".TABLE_INVOICE_PACKAGE_HOTEL.".in_packageId 
											WHERE ".TABLE_INVOICE_PACKAGE.".packageId IN($packId)  
											AND ".TABLE_INVOICE_PACKAGE_HOTEL.".roomType=$type 
											");
				
        			$roomRow	=	mysql_fetch_array($roomQry);
        			$roomNum	=	$roomRow['roomCount'];
        			return $roomNum;
				}
				
			}
        	?>
        	<div class="row" style="background-color: #f4f4f4" >
                <div class="col-lg-2 col-md-2 col-sm-2" >
                    <div class="form_block" style="margin: 0; line-height: 3;" >
                        <label style="margin: 0">Adult:&nbsp;<span style=" font-weight: bold"><?php echo $adultRow['adult'];?></span></label>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2" >
                    <div class="form_block" style="margin: 0; line-height: 3;">
                        <label style="margin: 0">Child:&nbsp;<span style=" font-weight: bold"><?php echo $childRow['child'];?></span></label>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2" >
                    <div class="form_block" style="margin: 0; line-height: 3;">
                        <label style="margin: 0">Infant:&nbsp;<span style=" font-weight: bold"><?php echo $infantRow['infant'];?></span></label>
                    </div>
                </div>
            </div>
            <div class="row"   >
                <div class="col-lg-2 col-md-2 col-sm-2" >
                    <div class="form_block" style="margin: 0; line-height: 3;">
                        <label style="margin: 0">Male:&nbsp;<span style=" font-weight: bold"><?php echo $maleRow['male'];?></span></label>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2" >
                    <div class="form_block" style="margin: 0; line-height: 3;">
                        <label style="margin: 0">Female:&nbsp;<span style=" font-weight: bold"><?php echo $femaleRow['female'];?></span></label>
                    </div>
                </div>
            </div>
            <?php if(in_array('hotel',$services)){?>
            	<div class="row" style="background-color: #f4f4f4">
            		<div class="col-lg-2 col-md-2 col-sm-2">
	                    <div class="form_block" style="margin: 0; line-height: 3;">
	                        <label style="margin: 0">Sharing:&nbsp;<span style=" font-weight: bold"><?php echo numRooms('1',$packId); ?></span></label>
	                    </div>
                	</div>
                	<div class="col-lg-2 col-md-2 col-sm-2">
	                    <div class="form_block" style="margin: 0; line-height: 3;">
	                        <label style="margin: 0">Twin:&nbsp;<span style=" font-weight: bold"><?php echo numRooms('2',$packId); ?></span></label>
	                    </div>
                	</div>
                	<div class="col-lg-2 col-md-2 col-sm-2">
	                    <div class="form_block" style="margin: 0; line-height: 3;">
	                        <label style="margin: 0">Triple:&nbsp;<span style=" font-weight: bold"><?php echo numRooms('3',$packId); ?></span></label>
	                    </div>
                	</div>
                	<div class="col-lg-2 col-md-2 col-sm-2">
	                    <div class="form_block" style="margin: 0; line-height: 3;">
	                        <label style="margin: 0">Four Bed:&nbsp;<span style=" font-weight: bold"><?php echo numRooms('4',$packId); ?></span></label>
	                    </div>
                	</div>
                	<div class="col-lg-2 col-md-2 col-sm-2">
	                    <div class="form_block" style="margin: 0; line-height: 3;">
	                        <label style="margin: 0">Five Bed:&nbsp;<span style=" font-weight: bold"><?php echo numRooms('5',$packId); ?></span></label>
	                    </div>
                	</div>
                </div>
               	<div class="row" style="background-color: #f4f4f4">
                	
                	<div class="col-lg-2 col-md-2 col-sm-2">
	                    <div class="form_block" style="margin: 0; line-height: 3;">
	                        <label style="margin: 0">Six Bed:&nbsp;<span style=" font-weight: bold"><?php echo numRooms('6',$packId); ?></span></label>
	                    </div>
                	</div>
                	<div class="col-lg-2 col-md-2 col-sm-2">
	                    <div class="form_block"  style="margin: 0; line-height: 3;">
	                        <label style="margin: 0">Child With Bed:&nbsp;<span style=" font-weight: bold"><?php echo numRooms('7',$packId); ?></span></label>
	                    </div>
                	</div>
                	<div class="col-lg-2 col-md-2 col-sm-2">
	                    <div class="form_block"  style="margin: 0; line-height: 3;">
	                        <label style="margin: 0">Child Without Bed:&nbsp;<span style=" font-weight: bold"><?php echo numRooms('8',$packId); ?></span></label>
	                    </div>
                	</div>
            	</div>
            <?php } ?>
        	
            <div id="package_dynamic">

				<?php
					$passengerQry	=	"SELECT a.ID,a.in_packageId,a.passengerName,a.dob,a.age,a.nationality,
												a.birthPlace,a.relation,a.mobile,a.passportNo,a.dateOfIssue,
												a.validUpTo,a.issuePlace,a.loginId,a.total,a.mofa,a.emofa,
                        					 	b.ticketNo,e.submissionDate,e.collectionDate,e.dispatchDate
			                               FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." a 
			                          LEFT JOIN ".TABLE_INVOICE_PACKAGE_AIRLINE." b ON b.passengerId=a.ID 
			                          LEFT JOIN ".TABLE_INVOICE_PACKAGE_VISA." e ON e.passengerId=a.ID 
				                          WHERE a.packageId IN($packId) AND $check
				                       ORDER BY a.ID DESC";

					$passengerResult=	mysql_query($passengerQry);
					$passengerNum	=	mysql_num_rows($passengerResult);
					?>
					<input type="hidden" name="number" id="number" value="<?php echo @$passengerNum; ?>" />
					<div class="package_dynamic_box pack_dynamic_transportation">
			        <h3 class="pack_dynamic_head">Passengers</h3>

			        <div class="pack_transportation_table_wrap">
			            <table class="table table-bordered bd_table show_table" id="package_status_table">
			                <thead>
			                <tr>
			                    <th>Sl No</th>
			                    <th>Invoice No</th>
			                    <th>Passenger</th>
			                    <th>Age</th>
			                    <th>Nationality</th>
			                    <th>Passport No</th>
			                    <th>Mofa</th>
			                    <th>Emofa</th>
			                    <?php if(in_array('airline',$services)){?>
			                    <th>Ticket</th>
			                    <?php }
			                    if(in_array('visa',$services)){?>
			                    <th>Submission Date</th>
			                    <th>Collection Date</th>
			                    <th>Dispatch Date</th>
			                    <?php }?>
			                    <th>Action</th>
			                </tr>
			                </thead>
			                <tbody>
			                <?php
			                if($passengerNum==0 && $_SESSION['travelType']=='Staff')
			                {
			                	?>
			                	<tr><td colspan="13" align="center">There is no data in list. </td></tr>
			                	<?php
			                }
			                else
			                {
			                $i	=	0;
			                while($passengerRow = mysql_fetch_array($passengerResult)){
			                	
			                	?>
			                <tr>
			                    <td><?php echo ++$i;?>
			                       <input type="hidden" name="passengerId_<?php echo $i;?>"  data-field="passenger_id" value="<?php echo $passengerRow['ID'];?>">
			                    </td>
			                    <td>
			                       <input type="text" name="invoice_<?php echo $i;?>" value="<?php echo 'PI'.$passengerRow['in_packageId']; ?>" disabled="" >
			                    </td>
			                    <td>
			                        <input type="text" name="passanger_<?php echo $i;?>" value="<?php echo $passengerRow['passengerName']; ?>" disabled="" >
			                    </td>
			                    <td>
			                       <input type="text" name="dob_<?php echo $i;?>" value="<?php  if($passengerRow['dob']){echo $passengerRow['age'];} ?>" disabled="" >
			                    </td>
			                    <td>
			                       <input type="text" name="nationality_<?php echo $i;?>" value="<?php echo $passengerRow['nationality'];?>" disabled="" >
			                    </td>
			                    
			                    <td>
			                        <input type="text" name="passNo_<?php echo $i;?>" value="<?php echo $passengerRow['passportNo']; ?>" disabled="" >
			                    </td>
			                     <td>
			                       <input type="text" name="mofa_<?php echo $i;?>" value="<?php echo $passengerRow['mofa']; ?>" <?php if(($passengerRow['loginId']!=@$logId) || ($_SESSION['travelType']=='Admin')){ echo "readonly";}?> >
			                    </td>
			                    <td>
			                       <input type="text" name="emofa_<?php echo $i;?>" value="<?php echo $passengerRow['emofa'];?>" <?php if(($passengerRow['loginId']!=@$logId) || ($_SESSION['travelType']=='Admin')){ echo "readonly";}?> >
			                    </td>
			                   
			                    <?php if(in_array('airline',$services)){?>
			                     <td>
			                        <input type="text" data-field="ticketNo" name="ticketNo_<?php echo $i;?>" value="<?php echo $passengerRow['ticketNo']; ?>" <?php if(($passengerRow['loginId']!=@$logId) || ($_SESSION['travelType']=='Admin')){ echo "readonly";}?> >
			                    </td>
			                    <?php }
			                    if(in_array('visa',$services)){?>
			                    <td>
			                        <input type="text" data-field="subDate" class="user_date" name="subDate_<?php echo $i;?>" value="<?php if($passengerRow['submissionDate']){echo $App->dbFormat_date($passengerRow['submissionDate']);} ?>" <?php if(($passengerRow['loginId']!=@$logId) || ($_SESSION['travelType']=='Admin')){ echo "readonly";}?> >
			                    </td>
			                    
			                    <td>
			                        <input type="text" data-field="collDate" class="user_date" name="collDate_<?php echo $i;?>" value="<?php if($passengerRow['collectionDate']){echo $App->dbFormat_date($passengerRow['collectionDate']); }?>" <?php if(($passengerRow['loginId']!=@$logId) || ($_SESSION['travelType']=='Admin')){ echo "readonly";}?> >
			                    </td>
			                    <td>
			                        <input type="text" data-field="disDate" class="user_date" name="disDate_<?php echo $i;?>" value="<?php if($passengerRow['dispatchDate']){echo $App->dbFormat_date($passengerRow['dispatchDate']);} ?>" <?php if(($passengerRow['loginId']!=@$logId) || ($_SESSION['travelType']=='Admin')){ echo "readonly";}?> >
			                    </td>
			                    <?php }?>
			                    <td>
			                        <a class="show_table_lnk show_table_lnk_view" data-view_node="get_com_serv" data-com_serv_type="pack" data-view_id="<?= $passengerRow['ID']; ?>" href="#" data-modal_target="#acs_view_pop">View</a>
			                    </td>
			                    
			                </tr>
			                <?php } }?>
			                </tbody>
			            </table>
			        </div>
			    </div>
				
		   <!---------------------------------------- End --------------------------------------->

            </div>
            <div class="airline_submit">
                <input type="hidden" name="pack_passenger_count" id="pack_reg_air_row_count" value="<?php echo $passengerNum; ?>">
                <?php  if($passengerNum!=0 && $loginType!='Admin'){?>
                <button type="submit" class="bd_btn bd_btn_green"  >Update All</button>
                <?php } ?>
            </div>
        </div>
    </form>
</div>

<!-- Popups -->
    <!--Popup for account View-->
    <div id="acs_view_pop" class="modal fade bd_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Passenger Details</h4></div>
                <div class="modal-body">
                    <div class="acs_view_table_wrap table-responsive">
                        <table class="table table-bordered table-striped acs_view_table">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');

require('../admin_footer2.php');
?>


