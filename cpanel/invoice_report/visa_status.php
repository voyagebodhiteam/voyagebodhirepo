<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"a.proId='$proId'";
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Visa Status</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                        
                        	 <select name="supplierId" >
	                                <option value="">Select</option>
	                                <?php
	                                $select 	= 	"select  ID,accountName from ". TABLE_ACCOUNTS." where accountGroupId='40' and proId=$proId";
									$result 	= 	$db->query($select);							 	
									while($row=mysql_fetch_array($result))
									{
									?>
										<option value="<?php echo $row['ID'] ?>" <?php if(@$_REQUEST['supplierId']==$row['ID']){ echo "selected";} ?>><?php echo $row['accountName'] ?></option>
									<?php	
									}
									?>
                               </select>
                            <input type="search" name="date" placeholder="Date"  class="user_date" value="<?php echo @$_REQUEST['date']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                        
                    </div>
                    
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    
<?php	
$cond="1";
	if(@$_REQUEST['supplierId'])
	{
		$cond	 =	$cond." and c.supplierId =".$_REQUEST['supplierId']." ";
	}
	if(@$_REQUEST['date'])
	{
		$cond=$cond." and a.invoiceDate like '%".$App->dbFormat_date($_REQUEST['date'])."%'";
	}

?>
    
    
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Invoice Number</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Total Passengers</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                     $i=0;
                        $selectAll = "select a.ID,
                        					 a.invoiceNo,
                        					 a.invoiceDate,
                        					 a.netAmount,
                        					 COUNT(b.ID) as passCount
			                          from ".TABLE_INVOICE_VISA." a
			                     LEFT JOIN ".TABLE_VISA_PASSENGER." b ON b.visaId=a.ID
			                     LEFT JOIN ".TABLE_PURCHASE_VISA." c ON c.ourInvoiceNo=a.ID
			                          where $cond
			                          and $check
			                          GROUP BY a.ID
			                          order by a.ID desc";
			                          
						//echo $selectAll;
                        $result 	 = $db->query($selectAll);
                        $selectCount = mysql_num_rows($result);
                        
                        if($selectCount==0)
						{
						?>
							 <tr>
								<td align="center" colspan="7">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
                        /*********************** for pagination ******************************/
						$rowsPerPage = ROWS_PER_PAGE;
						if(isset($_GET['page']))
						{
							$pageNum = $_GET['page'];
						}
						else
						{
							$pageNum =1;
						}
						$offset = ($pageNum - 1) * $rowsPerPage;
						$select1=$db->query($selectAll." limit $offset, $rowsPerPage");
						$i=$offset+1;
						//use '$select1' for fetching
						/*************************** for pagination **************************/
                        while ($row = mysql_fetch_array($select1)) {
                            ?>
                         <tr>                            
                            <td><?php echo $i++; ?></td>
                            <td><?php echo "VI".$row['invoiceNo']; ?></td>
                            <td><?php echo $App->dbformat_date($row['invoiceDate']); ?></td>
                            <td><?php echo $row['netAmount']; ?></td>
                            <td><?php echo $row['passCount']; ?></td>
                            <td><a class="show_table_lnk show_table_lnk_view" href="visa_status_view.php?id=<?php echo $row['ID']; ?>">View</a>
                            </td>
                        </tr>
                            <?php
                        }
                        }
                        ?>

                    </tbody>
                </table>
            </div>
            
            <!-- paging -->		
            <!--<div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div> -->       
            <!-- paging end-->
            
            <!--*****************************************************************-->
            
            	 <?php 
                  if($selectCount>@$rowsPerPage)
					{
					?>	
					 <br />	
					  <div class="pagerSC" align="center">
					<?php
					
					$query   =  $db->query($selectAll);
					$numrows = mysql_num_rows($query);
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
					   if ($page == $pageNum)
					   {
						  $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
					   }
					   else
					   {
						 	if(@$search)
					   		 {
							 	$nav .= " <a href=\"$self?page=$page&sname=$search\">$page</a> ";
							 }
							 else
							 {
							 	$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							 }
					   }
					}
					?>
					 <?php
					if ($pageNum > 1)
					{
					   $page  = $pageNum - 1;
					   if(@$search)
					   {
						   $prev  = " <a href=\"$self?page=$page&sname=$search\">Prev</a> ";
						   $first = " <a href=\"$self?page=1&sname=$search\">First Page</a> ";
					   }
					   else
					   {
						   $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
						   $first = " <a href=\"$self?page=1\">First Page</a> ";
					   }
					}
					else
					{
					   $prev  = '&nbsp;';
					   $first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
					   $page = $pageNum + 1;
					   if(@$search)
					   {
						   	 $next = " <a href=\"$self?page=$page&sname=$search\">Next</a> ";
						     $last = " <a href=\"$self?page=$maxPage&sname=$search\">Last Page</a> ";
					   }
					   else
					   {
						   	 $next = " <a href=\"$self?page=$page\">Next</a> ";
						   	 $last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
					   }
					}
					else
					{
					   $next = '&nbsp;';
					   $last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
            
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>