<?php
require("../admin_header.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';
$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ".TABLE_INVOICE_VISA." where ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);

$loginType 	=	$_SESSION['travelType'];
if($loginType == 'Admin')
{
	$proId	=	$_REQUEST['proId'];
}
?>
<script>
function valid()
{	
	flag=false;
	
	total=document.getElementById('total').value;
	discount=document.getElementById('discount').value;	
	netAmount=document.getElementById('netAmount').value;
		
	if(isNaN(total) || total=="")
	{																			
	document.getElementById('totalDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(discount) || discount=="")
	{																			
	document.getElementById('discountDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(netAmount) || netAmount=="" || netAmount==0)
	{																			
	document.getElementById('netAmountDiv').innerHTML="Enter valid amount";
	flag=true;
	}
	if(flag==true)
	{
	return false;
	}
	
}

//clear the validation msg

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

</script>
<script>
	function total()
	{
	var visaFee=document.getElementById('visaFee').value;
	if(visaFee==''){visaFee=0;}
	
	var vfs=document.getElementById('vfs').value;
	if(vfs==''){vfs=0;}
	
	var ddCharge=document.getElementById('ddCharge').value;
	if(ddCharge==''){ddCharge=0;}
	
	var serviceCharge=document.getElementById('serviceCharge').value;
	if(serviceCharge==''){serviceCharge=0;}
	
	var otherCharge=document.getElementById('otherCharge').value;
	if(otherCharge==''){otherCharge=0;}	
	
	var courierCharge=document.getElementById('courierCharge').value;
	if(courierCharge==''){courierCharge=0;}	
	
	var subTotal=document.getElementById('subTotal').value;
	if(subTotal==''){subTotal=0;}
	
	var mainOtherCharge=document.getElementById('mainOtherCharge').value;
	if(mainOtherCharge==''){mainOtherCharge=0;}	
	
	var discount=document.getElementById('discount').value;
	if(discount==''){discount=0;}
	//alert(vfs);
	//alert(visaFee+vfs+ddCharge+serviceCharge+otherCharge+courierCharge+subTotal+mainOtherCharge);
	var subTot=parseInt(visaFee)+parseInt(vfs)+parseInt(ddCharge)+parseInt(serviceCharge)+parseInt(otherCharge)+parseInt(courierCharge);	
	var tot=subTot+parseInt(mainOtherCharge)-parseInt(discount);
	
	document.getElementById('subTotal').value=subTot;	
	document.getElementById('netAmount').value=tot;
	}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form class="default_form" id="invoice_visa" method="post" action="do.php?op=edit"  onsubmit="return valid()">
                <input type="hidden" name="editId" value="<?php  echo $editId;?>">
                    <div class="bd_panel_head">
                        <h3>Visa - Invoice</h3>
                    </div>
                    <div class="bd_panel_body">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher No</label>                                    
                                    <input type="text" name="invoiceNo" value="<?php echo "VI". $editRow['invoiceNo'];?>"  required="" readonly="" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher Date</label>
                                    <input type="text" name="invoiceDate" class="user_date" value="<?php echo $App->dbFormat_date($editRow['invoiceDate']); ?>" required=""  disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer Name</label> <!--select from account where accGpId=37-->
								<?php
								$cId=$editRow['customerId']; 
								$editSelect2 = 	"select  accountName                          			
								from ".TABLE_ACCOUNTS." 
								where ID='$cId'";
								$editResult2 = 	$db->query($editSelect2);
								$editRow2	 = 	mysql_fetch_array($editResult2);
								?>
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerName" autocomplete="off" required="" value="<?php echo $editRow2['accountName'];?>"  disabled="">
									<input type="hidden" name="customerId">
									<ul class="aj_live_items">

									</ul>
							
								</div>
							</div>
						</div>					
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer ID</label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerId" id="customerId" autocomplete="off" required="" value="<?php echo $editRow['customerId'];?>"  disabled="">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
                            
                        </div>

                        <div class="multi_det">
                            <div class="multi_det_body">
                                <div class="multi_det_head">
                                    <h3>Passenger List</h3>
                                    <!--<div class="multi_det_control">
                                        <ul class="page_nav">
                                            <li>
                                                <a href="#" id="get_visa_passengers" data-modal_target="#visa_passenger_list">
                                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                                    Add New
                                                </a>
                                            </li>
                                        </ul>
                                    </div>-->
                                    <div class="bd_clear"></div>
                                </div>
                                <div class="multi_det_table_wrap table-responsive">
                                    <table class="table table-bordered multi_det_table" id="visa_passenger_table">
                                        <thead>
                                        <tr>
                                            <th>Sl No</th>
											<th>Passenger Name</th>
											<th>Dob</th>
											<th>gender</th>
											<th>Passport No</th>
                                            <!--<th>Actions</th>-->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php				                        
										$i=0;			                        
										$editSelect2 	= 	"select  * from ".TABLE_VISA_PASSENGER." where visaId='$editId' ";
										$editResult2 	= 	$db->query($editSelect2);
										$rowCountEdit	=	mysql_num_rows($editResult2);
									
										while($editRow2	= 	mysql_fetch_array($editResult2)){
											?>
										<tr>
										   <td><?php echo ++$i; ?> </td>
                                           <td><input type="text" name="pass2_passengerName" readonly="" value="<?php echo $editRow2['passengerName'];?>"  disabled=""></td>
										   <td><input type="text" name="pass2_dob" readonly="" value="<?php echo $editRow2['dob'];?>" disabled=""></td>
												
										   <td><input type="text" name="pass2_gender" readonly="" value="<?php echo $editRow2['gender'];?>" disabled=""></td>
										   <td><input type="text" name="pass2_passportNo" readonly=""  value="<?php echo $editRow2['passportNo'];?>" disabled=""></td>
										   <!--<td><a class="hotel_row_del bd_lnk_del" href="#">Delete</a></td>-->
											</tr>
												 <?php
										   $visaFor			=	$editRow2['visaFor'];				   
										   $submissionDate  =	$editRow2['submissionDate'];
										   $collectionDate	=	$editRow2['collectionDate'];
										   $dispatchDate	=	$editRow2['dispatchDate'];
										   $tokenNo			=	$editRow2['tokenNo'];
										   $visaFee			=	$editRow2['visaFee'];
										   $vfs				=	$editRow2['vfs'];
										   $ddCharge		=	$editRow2['ddCharge'];
					                
					                	   $serviceCharge	=	$editRow2['serviceCharge'];
										   $otherCharge		=	$editRow2['otherCharge'];
										   $courierCharge	=	$editRow2['courierCharge'];
										   $subTotal		=	$editRow2['subTotal'];		
													}
													?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                         <fieldset>
                         	<legend>Individual Details</legend>
                        <div class="row">                                               	
                        	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Visa for</label>
                                   
                                    <select name="visaFor" class="pass_to_table" disabled="">                                   
									<option  <?php if($editRow2['visaFor']=='Afghanistan'){ ?> selected="" <?php }?>>Afghanistan</option>
									<option  <?php if($editRow2['visaFor']=='Albania'){ ?> selected="" <?php }?>>Albania</option>
									<option  <?php if($editRow2['visaFor']=='Algeria'){ ?> selected="" <?php }?>>Algeria</option>
									<option  <?php if($editRow2['visaFor']=='American Samoa'){ ?> selected="" <?php }?>>American Samoa</option>
									<option  <?php if($editRow2['visaFor']=='Andorra'){ ?> selected="" <?php }?>>Andorra</option>
									<option  <?php if($editRow2['visaFor']=='Angola'){ ?> selected="" <?php }?>>Angola</option>
									<option  <?php if($editRow2['visaFor']=='Anguilla'){ ?> selected="" <?php }?>>Anguilla</option>
									<option  <?php if($editRow2['visaFor']=='Antarctica'){ ?> selected="" <?php }?>>Antarctica</option>
									<option  <?php if($editRow2['visaFor']=='Antigua and Barbuda'){ ?> selected="" <?php }?>> Antigua and Barbuda</option>
									<option  <?php if($editRow2['visaFor']=='Argentina'){ ?> selected="" <?php }?>> Argentina</option>
									<option  <?php if($editRow2['visaFor']=='Armenia'){ ?> selected="" <?php }?>> Armenia</option>
									<option  <?php if($editRow2['visaFor']=='Aruba'){ ?> selected="" <?php }?>> Aruba</option>
									<option  <?php if($editRow2['visaFor']=='Australia'){ ?> selected="" <?php }?>> Australia</option>
									<option  <?php if($editRow2['visaFor']=='Austria'){ ?> selected="" <?php }?>> Austria</option>
									<option  <?php if($editRow2['visaFor']=='Azerbaijan'){ ?> selected="" <?php }?>> Azerbaijan</option>
									<option  <?php if($editRow2['visaFor']=='Bahamas'){ ?> selected="" <?php }?>> Bahamas</option>
									<option  <?php if($editRow2['visaFor']=='Bahrain'){ ?> selected="" <?php }?>> Bahrain</option>
									<option  <?php if($editRow2['visaFor']=='Bangladesh'){ ?> selected="" <?php }?>> Bangladesh</option>
									<option  <?php if($editRow2['visaFor']=='Barbados'){ ?> selected="" <?php }?>> Barbados</option>
									<option  <?php if($editRow2['visaFor']=='Belarus'){ ?> selected="" <?php }?>> Belarus</option>
									<option  <?php if($editRow2['visaFor']=='Belgium'){ ?> selected="" <?php }?>> Belgium</option>
									<option  <?php if($editRow2['visaFor']=='Belize'){ ?> selected="" <?php }?>> Belize</option>
									<option  <?php if($editRow2['visaFor']=='Benin'){ ?> selected="" <?php }?>> Benin</option>
									<option  <?php if($editRow2['visaFor']=='Bermuda'){ ?> selected="" <?php }?>> Bermuda</option>
									<option  <?php if($editRow2['visaFor']=='Bhutan'){ ?> selected="" <?php }?>> Bhutan</option>
									<option  <?php if($editRow2['visaFor']=='Bolivia'){ ?> selected="" <?php }?>> Bolivia</option>
									<option  <?php if($editRow2['visaFor']=='Bosnia and Herzegovina'){ ?> selected="" <?php }?>> Bosnia and Herzegovina</option>
									<option  <?php if($editRow2['visaFor']=='Botswana'){ ?> selected="" <?php }?>> Botswana</option>
									<option  <?php if($editRow2['visaFor']=='Bouvet Island'){ ?> selected="" <?php }?>> Bouvet Island</option>
									<option  <?php if($editRow2['visaFor']=='Brazil'){ ?> selected="" <?php }?>> Brazil</option>
									<option  <?php if($editRow2['visaFor']=='British Indian Ocean Territory'){ ?> selected="" <?php }?>> British Indian Ocean Territory</option>
									<option  <?php if($editRow2['visaFor']=='Brunei Darussalam'){ ?> selected="" <?php }?>> Brunei Darussalam</option>
									<option  <?php if($editRow2['visaFor']=='Bulgaria'){ ?> selected="" <?php }?>> Bulgaria</option>
									<option  <?php if($editRow2['visaFor']=='Burkina Faso'){ ?> selected="" <?php }?>> Burkina Faso</option>
									<option  <?php if($editRow2['visaFor']=='Burundi'){ ?> selected="" <?php }?>> Burundi</option>
									<option  <?php if($editRow2['visaFor']=='Cambodia'){ ?> selected="" <?php }?>> Cambodia</option>
									<option  <?php if($editRow2['visaFor']=='Cameroon'){ ?> selected="" <?php }?>> Cameroon</option>
									<option  <?php if($editRow2['visaFor']=='Canada'){ ?> selected="" <?php }?>> Canada</option>
									<option  <?php if($editRow2['visaFor']=='Cape Verde'){ ?> selected="" <?php }?>> Cape Verde</option>
									<option  <?php if($editRow2['visaFor']=='Cayman Islands'){ ?> selected="" <?php }?>> Cayman Islands</option>
									<option  <?php if($editRow2['visaFor']=='Central African Republic'){ ?> selected="" <?php }?>> Central African Republic</option>
									<option  <?php if($editRow2['visaFor']=='Afghanistan'){ ?> selected="" <?php }?>> Chad</option>
									<option  <?php if($editRow2['visaFor']=='Chad'){ ?> selected="" <?php }?>> Chile</option>
									<option  <?php if($editRow2['visaFor']=='China'){ ?> selected="" <?php }?>> China</option>
									<option  <?php if($editRow2['visaFor']=='Christmas Island'){ ?> selected="" <?php }?>> Christmas Island</option>
									<option  <?php if($editRow2['visaFor']=='Cocos Islands'){ ?> selected="" <?php }?>> Cocos Islands</option>
									<option  <?php if($editRow2['visaFor']=='Colombia'){ ?> selected="" <?php }?>> Colombia</option>
									<option  <?php if($editRow2['visaFor']=='Comoros'){ ?> selected="" <?php }?>> Comoros</option>
									<option  <?php if($editRow2['visaFor']=='Congo'){ ?> selected="" <?php }?>> Congo</option>
									<option  <?php if($editRow2['visaFor']=='Congo, Democratic Republic of the'){ ?> selected="" <?php }?>> Congo, Democratic Republic of the</option>
									<option  <?php if($editRow2['visaFor']=='Cook Islands'){ ?> selected="" <?php }?>> Cook Islands</option>
									<option  <?php if($editRow2['visaFor']=='Costa Rica'){ ?> selected="" <?php }?>> Costa Rica</option>
									<option  <?php if($editRow2['visaFor']=='Cote d Ivoire'){ ?> selected="" <?php }?>> Cote d'Ivoire</option>
									<option  <?php if($editRow2['visaFor']=='Afghanistan'){ ?> selected="" <?php }?>> Croatia</option>
									<option  <?php if($editRow2['visaFor']=='Croatia'){ ?> selected="" <?php }?>> Cuba</option>
									<option  <?php if($editRow2['visaFor']=='Cyprus'){ ?> selected="" <?php }?>> Cyprus</option>
									<option  <?php if($editRow2['visaFor']=='Czech Republic'){ ?> selected="" <?php }?>> Czech Republic</option>
									<option  <?php if($editRow2['visaFor']=='Denmark'){ ?> selected="" <?php }?>> Denmark</option>
									<option  <?php if($editRow2['visaFor']=='Djibouti'){ ?> selected="" <?php }?>> Djibouti</option>
									<option  <?php if($editRow2['visaFor']=='Dominica'){ ?> selected="" <?php }?>> Dominica</option>
									<option  <?php if($editRow2['visaFor']=='Dominican Republic'){ ?> selected="" <?php }?>> Dominican Republic</option>
									<option  <?php if($editRow2['visaFor']=='Ecuador'){ ?> selected="" <?php }?>> Ecuador</option>
									<option  <?php if($editRow2['visaFor']=='Egypt'){ ?> selected="" <?php }?>> Egypt</option>
									<option  <?php if($editRow2['visaFor']=='El Salvador'){ ?> selected="" <?php }?>> El Salvador</option>
									<option  <?php if($editRow2['visaFor']=='Equatorial Guinea'){ ?> selected="" <?php }?>> Equatorial Guinea</option>
									<option  <?php if($editRow2['visaFor']=='Eritrea'){ ?> selected="" <?php }?>> Eritrea</option>
									<option  <?php if($editRow2['visaFor']=='Estonia'){ ?> selected="" <?php }?>> Estonia</option>
									<option  <?php if($editRow2['visaFor']=='Ethiopia'){ ?> selected="" <?php }?>> Ethiopia</option>
									<option  <?php if($editRow2['visaFor']=='Falkland Islands'){ ?> selected="" <?php }?>> Falkland Islands</option>
									<option  <?php if($editRow2['visaFor']=='Faroe Islands'){ ?> selected="" <?php }?>> Faroe Islands</option>
									<option  <?php if($editRow2['visaFor']=='Fiji'){ ?> selected="" <?php }?>> Fiji</option>
									<option  <?php if($editRow2['visaFor']=='Finland'){ ?> selected="" <?php }?>> Finland</option>
									<option  <?php if($editRow2['visaFor']=='France'){ ?> selected="" <?php }?>> France</option>
									<option  <?php if($editRow2['visaFor']=='French Guiana'){ ?> selected="" <?php }?>> French Guiana</option>
									<option  <?php if($editRow2['visaFor']=='French Polynesia'){ ?> selected="" <?php }?>> French Polynesia</option>
									<option  <?php if($editRow2['visaFor']=='Gabon'){ ?> selected="" <?php }?>> Gabon</option>
									<option  <?php if($editRow2['visaFor']=='Gambia'){ ?> selected="" <?php }?>> Gambia</option>
									<option  <?php if($editRow2['visaFor']=='Georgia'){ ?> selected="" <?php }?>> Georgia</option>
									<option  <?php if($editRow2['visaFor']=='Germany'){ ?> selected="" <?php }?>> Germany</option>
									<option  <?php if($editRow2['visaFor']=='Ghana'){ ?> selected="" <?php }?>> Ghana</option>
									<option  <?php if($editRow2['visaFor']=='Gibraltar'){ ?> selected="" <?php }?>> Gibraltar</option>
									<option  <?php if($editRow2['visaFor']=='Greece'){ ?> selected="" <?php }?>> Greece</option>
									<option  <?php if($editRow2['visaFor']=='Greenland'){ ?> selected="" <?php }?>> Greenland</option>
									<option  <?php if($editRow2['visaFor']=='Grenada'){ ?> selected="" <?php }?>> Grenada</option>
									<option  <?php if($editRow2['visaFor']=='Guadeloupe'){ ?> selected="" <?php }?>> Guadeloupe</option>
									<option  <?php if($editRow2['visaFor']=='Guam'){ ?> selected="" <?php }?>> Guam</option>
									<option  <?php if($editRow2['visaFor']=='Guatemala'){ ?> selected="" <?php }?>> Guatemala</option>
									<option  <?php if($editRow2['visaFor']=='Guinea'){ ?> selected="" <?php }?>> Guinea</option>
									<option  <?php if($editRow2['visaFor']=='Guinea-Bissau'){ ?> selected="" <?php }?>> Guinea-Bissau</option>
									<option  <?php if($editRow2['visaFor']=='Guyana'){ ?> selected="" <?php }?>> Guyana</option>
									<option  <?php if($editRow2['visaFor']=='Haiti'){ ?> selected="" <?php }?>> Haiti</option>
									<option  <?php if($editRow2['visaFor']=='Heard Island and McDonald Islands'){ ?> selected="" <?php }?>> Heard Island and McDonald Islands</option>
									<option  <?php if($editRow2['visaFor']=='Afghanistan'){ ?> selected="" <?php }?>> Honduras</option>
									<option  <?php if($editRow2['visaFor']=='Honduras'){ ?> selected="" <?php }?>> Hong Kong</option>
									<option  <?php if($editRow2['visaFor']=='Hungary'){ ?> selected="" <?php }?>> Hungary</option>
									<option  <?php if($editRow2['visaFor']=='Iceland'){ ?> selected="" <?php }?>> Iceland</option>
									<option  <?php if($editRow2['visaFor']=='India'){ ?> selected="" <?php }?>> India</option>
									<option  <?php if($editRow2['visaFor']=='Indonesia'){ ?> selected="" <?php }?>> Indonesia</option>
									<option  <?php if($editRow2['visaFor']=='Iran'){ ?> selected="" <?php }?>> Iran</option>
									<option  <?php if($editRow2['visaFor']=='Iraq'){ ?> selected="" <?php }?>> Iraq</option>
									<option  <?php if($editRow2['visaFor']=='Ireland'){ ?> selected="" <?php }?>> Ireland</option>
									<option  <?php if($editRow2['visaFor']=='Israel'){ ?> selected="" <?php }?>> Israel</option>
									<option  <?php if($editRow2['visaFor']=='Italy'){ ?> selected="" <?php }?>> Italy</option>
									<option  <?php if($editRow2['visaFor']=='Jamaica'){ ?> selected="" <?php }?>> Jamaica</option>
									<option  <?php if($editRow2['visaFor']=='Japan'){ ?> selected="" <?php }?>> Japan</option>
									<option  <?php if($editRow2['visaFor']=='Jordan'){ ?> selected="" <?php }?>> Jordan</option>
									<option  <?php if($editRow2['visaFor']=='Kazakhstan'){ ?> selected="" <?php }?>> Kazakhstan</option>
									<option  <?php if($editRow2['visaFor']=='Kenya'){ ?> selected="" <?php }?>> Kenya</option>
									<option  <?php if($editRow2['visaFor']=='Kiribati'){ ?> selected="" <?php }?>> Kiribati</option>
									<option  <?php if($editRow2['visaFor']=='Kuwait'){ ?> selected="" <?php }?>> Kuwait</option>
									<option  <?php if($editRow2['visaFor']=='Kyrgyzstan'){ ?> selected="" <?php }?>> Kyrgyzstan</option>
									<option  <?php if($editRow2['visaFor']=='Laos'){ ?> selected="" <?php }?>> Laos</option>
									<option  <?php if($editRow2['visaFor']=='Latvia'){ ?> selected="" <?php }?>> Latvia</option>
									<option  <?php if($editRow2['visaFor']=='Lebanon'){ ?> selected="" <?php }?>> Lebanon</option>
									<option  <?php if($editRow2['visaFor']=='Lesotho'){ ?> selected="" <?php }?>> Lesotho</option>
									<option  <?php if($editRow2['visaFor']=='Liberia'){ ?> selected="" <?php }?>> Liberia</option>
									<option  <?php if($editRow2['visaFor']=='Libya'){ ?> selected="" <?php }?>> Libya</option>
									<option  <?php if($editRow2['visaFor']=='Liechtenstein'){ ?> selected="" <?php }?>> Liechtenstein</option>
									<option  <?php if($editRow2['visaFor']=='Lithuania'){ ?> selected="" <?php }?>> Lithuania</option>
									<option  <?php if($editRow2['visaFor']=='Luxembourg'){ ?> selected="" <?php }?>> Luxembourg</option>
									<option  <?php if($editRow2['visaFor']=='Macao'){ ?> selected="" <?php }?>> Macao</option>
									<option  <?php if($editRow2['visaFor']=='Madagascar'){ ?> selected="" <?php }?>> Madagascar</option>
									<option  <?php if($editRow2['visaFor']=='Malawi'){ ?> selected="" <?php }?>> Malawi</option>
									<option  <?php if($editRow2['visaFor']=='Malaysia'){ ?> selected="" <?php }?>> Malaysia</option>
									<option  <?php if($editRow2['visaFor']=='Maldives'){ ?> selected="" <?php }?>> Maldives</option>
									<option  <?php if($editRow2['visaFor']=='Mali'){ ?> selected="" <?php }?>> Mali</option>
									<option  <?php if($editRow2['visaFor']=='Malta'){ ?> selected="" <?php }?>> Malta</option>
									<option  <?php if($editRow2['visaFor']=='Marshall Islands'){ ?> selected="" <?php }?>> Marshall Islands</option>
									<option  <?php if($editRow2['visaFor']=='Martinique'){ ?> selected="" <?php }?>> Martinique</option>
									<option  <?php if($editRow2['visaFor']=='Mauritania'){ ?> selected="" <?php }?>> Mauritania</option>
									<option  <?php if($editRow2['visaFor']=='Mauritius'){ ?> selected="" <?php }?>> Mauritius</option>
									<option  <?php if($editRow2['visaFor']=='Mayotte'){ ?> selected="" <?php }?>> Mayotte</option>
									<option  <?php if($editRow2['visaFor']=='Mexico'){ ?> selected="" <?php }?>> Mexico</option>
									<option  <?php if($editRow2['visaFor']=='Micronesia'){ ?> selected="" <?php }?>> Micronesia</option>
									<option  <?php if($editRow2['visaFor']=='Moldova'){ ?> selected="" <?php }?>> Moldova</option>
									<option  <?php if($editRow2['visaFor']=='Monaco'){ ?> selected="" <?php }?>> Monaco</option>
									<option  <?php if($editRow2['visaFor']=='Mongolia'){ ?> selected="" <?php }?>> Mongolia</option>
									<option  <?php if($editRow2['visaFor']=='Montenegro'){ ?> selected="" <?php }?>> Montenegro</option>
									<option  <?php if($editRow2['visaFor']=='Montserrat'){ ?> selected="" <?php }?>> Montserrat</option>
									<option  <?php if($editRow2['visaFor']=='Morocco'){ ?> selected="" <?php }?>> Morocco</option>
									<option  <?php if($editRow2['visaFor']=='Mozambique'){ ?> selected="" <?php }?>> Mozambique</option>
									<option  <?php if($editRow2['visaFor']=='Myanmar'){ ?> selected="" <?php }?>> Myanmar</option>
									<option  <?php if($editRow2['visaFor']=='Namibia'){ ?> selected="" <?php }?>> Namibia</option>
									<option  <?php if($editRow2['visaFor']=='Nauru'){ ?> selected="" <?php }?>> Nauru</option>
									<option  <?php if($editRow2['visaFor']=='Nepal'){ ?> selected="" <?php }?>> Nepal</option>
									<option  <?php if($editRow2['visaFor']=='Netherlands'){ ?> selected="" <?php }?>> Netherlands</option>
									<option  <?php if($editRow2['visaFor']=='Netherlands Antilles'){ ?> selected="" <?php }?>> Netherlands Antilles</option>
									<option  <?php if($editRow2['visaFor']==' New Caledonia'){ ?> selected="" <?php }?>> New Caledonia</option>
									<option  <?php if($editRow2['visaFor']=='New Zealand'){ ?> selected="" <?php }?>> New Zealand</option>
									<option  <?php if($editRow2['visaFor']=='Nicaragua'){ ?> selected="" <?php }?>> Nicaragua</option>
									<option  <?php if($editRow2['visaFor']=='Niger'){ ?> selected="" <?php }?>> Niger</option>
									<option  <?php if($editRow2['visaFor']=='Nigeria'){ ?> selected="" <?php }?>> Nigeria</option>
									<option  <?php if($editRow2['visaFor']=='Norfolk Island'){ ?> selected="" <?php }?>> Norfolk Island</option>
									<option  <?php if($editRow2['visaFor']=='North Korea'){ ?> selected="" <?php }?>> North Korea</option>
									<option  <?php if($editRow2['visaFor']=='Norway'){ ?> selected="" <?php }?>> Norway</option>
									<option  <?php if($editRow2['visaFor']=='Oman'){ ?> selected="" <?php }?>> Oman</option>
									<option  <?php if($editRow2['visaFor']=='Pakistan'){ ?> selected="" <?php }?>> Pakistan</option>
									<option  <?php if($editRow2['visaFor']=='Palau'){ ?> selected="" <?php }?>> Palau</option>
									<option  <?php if($editRow2['visaFor']=='Palestinian Territory'){ ?> selected="" <?php }?>> Palestinian Territory</option>
									<option  <?php if($editRow2['visaFor']=='Panama'){ ?> selected="" <?php }?>> Panama</option>
									<option  <?php if($editRow2['visaFor']=='Papua New Guinea'){ ?> selected="" <?php }?>> Papua New Guinea</option>
									<option  <?php if($editRow2['visaFor']=='Paraguay'){ ?> selected="" <?php }?>> Paraguay</option>
									<option  <?php if($editRow2['visaFor']=='Peru'){ ?> selected="" <?php }?>> Peru</option>
									<option  <?php if($editRow2['visaFor']=='Philippines'){ ?> selected="" <?php }?>> Philippines</option>
									<option  <?php if($editRow2['visaFor']=='Pitcairn'){ ?> selected="" <?php }?>> Pitcairn</option>
									<option  <?php if($editRow2['visaFor']=='Poland'){ ?> selected="" <?php }?>> Poland</option>
									<option  <?php if($editRow2['visaFor']=='Portugal'){ ?> selected="" <?php }?>> Portugal</option>
									<option  <?php if($editRow2['visaFor']=='Puerto Rico'){ ?> selected="" <?php }?>> Puerto Rico</option>
									<option  <?php if($editRow2['visaFor']=='Qatar'){ ?> selected="" <?php }?>> Qatar</option>
									<option  <?php if($editRow2['visaFor']=='Romania'){ ?> selected="" <?php }?>> Romania</option>
									<option  <?php if($editRow2['visaFor']=='Russian Federation'){ ?> selected="" <?php }?>> Russian Federation</option>
									<option  <?php if($editRow2['visaFor']=='Rwanda'){ ?> selected="" <?php }?>> Rwanda</option>
									<option  <?php if($editRow2['visaFor']=='Saint Helena'){ ?> selected="" <?php }?>> Saint Helena</option>
									<option  <?php if($editRow2['visaFor']=='Saint Kitts and Nevis'){ ?> selected="" <?php }?>> Saint Kitts and Nevis</option>
									<option  <?php if($editRow2['visaFor']=='Saint Lucia'){ ?> selected="" <?php }?>> Saint Lucia</option>
									<option  <?php if($editRow2['visaFor']=='Saint Pierre and Miquelon'){ ?> selected="" <?php }?>> Saint Pierre and Miquelon</option>
									<option  <?php if($editRow2['visaFor']=='Saint Vincent and the Grenadines'){ ?> selected="" <?php }?>> Saint Vincent and the Grenadines</option>
									<option  <?php if($editRow2['visaFor']=='Samoa'){ ?> selected="" <?php }?>> Samoa</option>
									<option  <?php if($editRow2['visaFor']=='San Marino'){ ?> selected="" <?php }?>> San Marino</option>
									<option  <?php if($editRow2['visaFor']=='Sao Tome and Principe'){ ?> selected="" <?php }?>> Sao Tome and Principe</option>
									<option  <?php if($editRow2['visaFor']=='Saudi Arabia'){ ?> selected="" <?php }?>> Saudi Arabia</option>
									<option  <?php if($editRow2['visaFor']=='Senegal'){ ?> selected="" <?php }?>> Senegal</option>
									<option  <?php if($editRow2['visaFor']=='Serbia'){ ?> selected="" <?php }?>> Serbia</option>
									<option  <?php if($editRow2['visaFor']=='Seychelles'){ ?> selected="" <?php }?>> Seychelles</option>
									<option  <?php if($editRow2['visaFor']=='Sierra Leone'){ ?> selected="" <?php }?>> Sierra Leone</option>
									<option  <?php if($editRow2['visaFor']=='Singapore'){ ?> selected="" <?php }?>> Singapore</option>
									<option  <?php if($editRow2['visaFor']=='Slovakia'){ ?> selected="" <?php }?>> Slovakia</option>
									<option  <?php if($editRow2['visaFor']=='Slovenia'){ ?> selected="" <?php }?>> Slovenia</option>
									<option  <?php if($editRow2['visaFor']=='Solomon Islands'){ ?> selected="" <?php }?>> Solomon Islands</option>
									<option  <?php if($editRow2['visaFor']=='Somalia'){ ?> selected="" <?php }?>> Somalia</option>
									<option  <?php if($editRow2['visaFor']=='South Africa'){ ?> selected="" <?php }?>> South Africa</option>
									<option  <?php if($editRow2['visaFor']=='South Georgia'){ ?> selected="" <?php }?>> South Georgia</option>
									<option  <?php if($editRow2['visaFor']=='South Korea'){ ?> selected="" <?php }?>> South Korea</option>
									<option  <?php if($editRow2['visaFor']=='Spain'){ ?> selected="" <?php }?>> Spain</option>
									<option  <?php if($editRow2['visaFor']=='Sri Lanka'){ ?> selected="" <?php }?>> Sri Lanka</option>
									<option  <?php if($editRow2['visaFor']=='Sudan'){ ?> selected="" <?php }?>> Sudan</option>
									<option  <?php if($editRow2['visaFor']=='Suriname'){ ?> selected="" <?php }?>> Suriname</option>
									<option  <?php if($editRow2['visaFor']=='Svalbard and Jan Mayen'){ ?> selected="" <?php }?>> Svalbard and Jan Mayen</option>
									<option  <?php if($editRow2['visaFor']=='Swaziland'){ ?> selected="" <?php }?>> Swaziland</option>
									<option  <?php if($editRow2['visaFor']=='Sweden'){ ?> selected="" <?php }?>> Sweden</option>
									<option  <?php if($editRow2['visaFor']=='Switzerland'){ ?> selected="" <?php }?>> Switzerland</option>
									<option  <?php if($editRow2['visaFor']=='Syrian Arab Republic'){ ?> selected="" <?php }?>> Syrian Arab Republic</option>
									<option  <?php if($editRow2['visaFor']=='Taiwan'){ ?> selected="" <?php }?>> Taiwan</option>
									<option  <?php if($editRow2['visaFor']=='Tajikistan'){ ?> selected="" <?php }?>> Tajikistan</option>
									<option  <?php if($editRow2['visaFor']=='Tanzania'){ ?> selected="" <?php }?>> Tanzania</option>
									<option  <?php if($editRow2['visaFor']=='Thailand'){ ?> selected="" <?php }?>> Thailand</option>
									<option  <?php if($editRow2['visaFor']=='The Former Yugoslav Republic of Macedonia'){ ?> selected="" <?php }?>> The Former Yugoslav Republic of Macedonia</option>
									<option  <?php if($editRow2['visaFor']=='Timor-Leste'){ ?> selected="" <?php }?>> Timor-Leste</option>
									<option  <?php if($editRow2['visaFor']=='Togo'){ ?> selected="" <?php }?>> Togo</option>
									<option  <?php if($editRow2['visaFor']=='Tokelau'){ ?> selected="" <?php }?>> Tokelau</option>
									<option  <?php if($editRow2['visaFor']=='Tonga'){ ?> selected="" <?php }?>> Tonga</option>
									<option  <?php if($editRow2['visaFor']=='Trinidad and Tobago'){ ?> selected="" <?php }?>> Trinidad and Tobago</option>
									<option  <?php if($editRow2['visaFor']=='Tunisia'){ ?> selected="" <?php }?>> Tunisia</option>
									<option  <?php if($editRow2['visaFor']=='Turkey'){ ?> selected="" <?php }?>> Turkey</option>
									<option  <?php if($editRow2['visaFor']=='Turkmenistan'){ ?> selected="" <?php }?>> Turkmenistan</option>
									<option  <?php if($editRow2['visaFor']=='Tuvalu'){ ?> selected="" <?php }?>> Tuvalu</option>
									<option  <?php if($editRow2['visaFor']=='Uganda'){ ?> selected="" <?php }?>> Uganda</option>
									<option  <?php if($editRow2['visaFor']=='Ukraine'){ ?> selected="" <?php }?>> Ukraine</option>
									<option  <?php if($editRow2['visaFor']=='United Arab Emirates'){ ?> selected="" <?php }?>> United Arab Emirates</option>
									<option  <?php if($editRow2['visaFor']=='United Kingdom'){ ?> selected="" <?php }?>> United Kingdom</option>
									<option  <?php if($editRow2['visaFor']=='United States'){ ?> selected="" <?php }?>> United States</option>
									<option  <?php if($editRow2['visaFor']=='United States Minor Outlying Islands'){ ?> selected="" <?php }?>> United States Minor Outlying Islands</option>
									<option  <?php if($editRow2['visaFor']=='Uruguay'){ ?> selected="" <?php }?>> Uruguay</option>
									<option  <?php if($editRow2['visaFor']=='Uzbekistan'){ ?> selected="" <?php }?>> Uzbekistan</option>
									<option  <?php if($editRow2['visaFor']=='Vanuatu'){ ?> selected="" <?php }?>> Vanuatu</option>
									<option  <?php if($editRow2['visaFor']=='Vatican City'){ ?> selected="" <?php }?>> Vatican City</option>
									<option  <?php if($editRow2['visaFor']=='Venezuela'){ ?> selected="" <?php }?>> Venezuela</option>
									<option  <?php if($editRow2['visaFor']=='Vietnam'){ ?> selected="" <?php }?>> Vietnam</option>
									<option  <?php if($editRow2['visaFor']=='Virgin Islands, British'){ ?> selected="" <?php }?>> Virgin Islands, British</option>
									<option  <?php if($editRow2['visaFor']=='Virgin Islands, U.S.'){ ?> selected="" <?php }?>> Virgin Islands, U.S.</option>
									<option  <?php if($editRow2['visaFor']=='Wallis and Futuna'){ ?> selected="" <?php }?>> Wallis and Futuna</option>
									<option  <?php if($editRow2['visaFor']=='Western Sahara'){ ?> selected="" <?php }?>> Western Sahara</option>
									<option  <?php if($editRow2['visaFor']=='Yemen'){ ?> selected="" <?php }?>> Yemen</option>
									<option  <?php if($editRow2['visaFor']=='Zambia'){ ?> selected="" <?php }?>> Zambia</option>
									<option  <?php if($editRow2['visaFor']=='Zimbabwe'){ ?> selected="" <?php }?>> Zimbabwe</option>
									</select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Submission Date</label>
                                    <input class="pass_to_table user_date" type="text" name="submissionDate" required="" readonly="" value="<?php echo $App->dbformat_date($submissionDate); ?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Collection Date</label>
                                    <input class="pass_to_table user_date" type="text" name="collectionDate" required="" readonly="" value="<?php echo $App->dbformat_date($collectionDate); ?>"  disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Dispatch Date</label>
                                    <input class="pass_to_table user_date" type="text" name="dispatchDate" required="" readonly="" value="<?php echo $App->dbformat_date($dispatchDate); ?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>TokenNo</label>
                                    <input class="pass_to_table" type="text" name="tokenNo" value="<?php echo $tokenNo; ?>" onfocus="clearbox('tokenNoDiv')" disabled="">
                                </div>
                            </div>
                        	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Visa Fee</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="visaFee" value="<?php echo $visaFee; ?>" onfocus="clearbox('visaFeeDiv')" disabled="">
                                    <div class="valid" id="visaFeeDiv"></div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">                      	
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>VFS</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="vfs" value="<?php echo $vfs; ?>" onfocus="clearbox('vfsDiv')" disabled="">
                                    <div class="valid" id="vfsDiv"></div>
                                </div>
                            </div>
                            
                           <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>DD Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="ddCharge" value="<?php echo $ddCharge; ?>" onfocus="clearbox('ddChargeDiv')" disabled="">
                                    <div class="valid" id="ddChargeDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Service Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="serviceCharge" value="<?php echo $serviceCharge; ?>" onfocus="clearbox('totalDiv')" disabled="">
                                    <div class="valid" id="serviceChargeDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="otherCharge" value="<?php echo $otherCharge; ?>" onfocus="clearbox('otherChargeDiv')" disabled="">
                                    <div class="valid" id="otherChargeDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Courier Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="courierCharge" value="<?php echo $courierCharge; ?>" onfocus="clearbox('courierChargeDiv')" disabled="">
                                    <div class="valid" id="courierChargeDiv"></div>
                                </div>
                        	</div>
                        	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Sub Total</label>
                                    <input class="pass_to_table"  type="text" name="subTotal" required="" value="<?php echo $subTotal; ?>" onfocus="clearbox('subTotalDiv')" disabled="">
                                </div>
                                <div class="valid" id="subTotalDiv"></div>
                            </div>                        
                        </div>
                       </fieldset>
                       <fieldset>
                       <legend>Total</legend>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charges</label>
                                    <input type="text" data-visa_net="visa_net_trigger" name="mainOtherCharge" id="mainOtherCharge" value="<?php echo $editRow['mainOtherCharge']; ?>" onfocus="clearbox('mainOtherChargeDiv')" disabled=""> 
								<div class="valid" id="mainOtherChargeDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Discount</label>
                                    <input type="text" data-visa_net="visa_net_trigger" name="discount" id="discount" value="<?php echo $editRow['discount']; ?>" onfocus="clearbox('discountDiv')" disabled="">
								<div class="valid" id="discountDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Net Amount</label>
                                    <input type="text"  name="netAmount" id="netAmount" required="" onfocus="clearbox('netAmountDiv')" value="<?php echo $editRow['netAmount']; ?>" disabled="">
								<div class="valid" id="netAmountDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Description</label>
                                    <textarea class="pass_to_table" name="description" disabled="" ><?php echo $editRow['description']; ?></textarea>
                                </div>
                            </div>
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Remark</label>
                                    <textarea class="pass_to_table" name="remark" disabled=""><?php echo $editRow['remark']; ?></textarea>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <!--<div class="row">
                            <div class="col-lg-12">
                                <div class="airline_submit">
                                    <input type="hidden" name="rowCount" id="visa_passenger_row_count" value="<?php echo $rowCountEdit?>">
                                     <input type="submit" name="savePurchase" value="SAVE & PURCHASE">
									 <input type="submit" name="save" value="SAVE">
                                </div>
                            </div>
                        </div>-->
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
    <!-- Popups -->
    <div id="visa_passenger_list" class="modal fade bd_modal passenger_pop" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Passenger Details</h4>
                </div>
                <div class="modal-body">
					<table class="table table-bordered" id="visa_passenger_check_list">
						<thead>
						<tr>
							<th>Check</th>
							<th>Passenger Name</th>
							<th>Dob</th>
							<th>gender</th>
							<th>Passport No</th>
						</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="trigger_visa_passenger">OK</button>
                </div>
            </div>
        </div>
    </div>
<?php
require('../admin_footer2.php');
?>