<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId		=	$_SESSION['proId'];
$loginId	=	$_SESSION['travelId'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
	 case 'edit':

    		$visaId			=	$_REQUEST['visaId'];
    		
    		if(!$visaId)
    		{
				$_SESSION['msg'] = $App->sessionMsgCreate('error', "Faild to add Update. Invalid details."); 			 header("location:visa_status.php");
			}
			else
			{
	    		$passengerCount	=	$_REQUEST['pack_passenger_count'];
	    		for($i=1;$i<=$passengerCount;$i++)
	    		{
					$passId1	=	'passengerId_'.$i;
					$passId 	=	$_REQUEST[$passId1];
					$ticketNo	=	'ticketNo_'.$i;
					$subDate	=	'subDate_'.$i;
					$collDate	=	'collDate_'.$i;
					$disDate	=	'disDate_'.$i;
					$travelDate	=	'travelDate_'.$i;
					$message	=	'message_'.$i;
					$status		=	'status_'.$i;

					$data2['submissionDate'] 	= 	'';
					$data2['collectionDate'] 	= 	'';
					$data2['dispatchDate'] 		= 	'';
					$data2['travelDate']		=	'';
					$data2['status']			=	'';
					
					if($_REQUEST[$subDate])
					{
						$data2['submissionDate'] 	= 	@$App->dbformat_date($_REQUEST[$subDate]);
					}
					if($_REQUEST[$collDate])
					{
						$data2['collectionDate'] 	= 	$App->dbformat_date($_REQUEST[$collDate]);
					}
					if($_REQUEST[$disDate])
					{
						$data2['dispatchDate'] 		= 	$App->dbformat_date($_REQUEST[$disDate]);
					}
					if($_REQUEST[$travelDate])
					{
						$data2['travelDate']		=	$App->dbformat_date($_REQUEST[$travelDate]);
					}
					if($_REQUEST[$message])
					{
						$data2['visaMessage']		=	$_REQUEST[$message];
						$data2['msgChngBy']			=	$loginId;
					}
					if($_REQUEST[$status])
					{
						$data2['status']			=	$_REQUEST[$status];
					}
					
					$success2	=	$db->query_update(TABLE_VISA_PASSENGER, $data2 ," ID=$passId ");

				}
				$db->close();
				if ($success2) 
	            {
	                $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details Updated successfully");                
	            } 
	            else 
	            {
	                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Faild to Update details. Please try again.");     
	            }
	    		 header("location:visa_status_view.php?id=$visaId");
    		}

	 break;

}	        	
?>