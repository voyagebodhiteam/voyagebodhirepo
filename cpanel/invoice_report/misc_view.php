<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ".TABLE_INVOICE_MISC." where ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit" class="default_form" id="misc_form">
            <input type="hidden" name="editId" value="<?php  echo $editId;?>">
                <div class="bd_panel_head">
                    <h3>Invoice-Miscellaneous Services</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Invoice No*</label>
                                <input type="text" name="invoiceNo" value="<?php echo "MI". $editRow['invoiceNo']; ?>" required="" readonly="" disabled="">
                            </div>
                        </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Invoice Date</label>
                                <input type="text" name="invoiceDate" value="<?php echo $App->dbFormat_date($editRow['invoiceDate']); ?>" class="user_date" disabled="">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2" style="display: none">
                            <div class="form_block">
                                <label>Entry Date</label>
                                <input type="text" name="entryDate" value="<?php echo $App->dbFormat_date($editRow['entryDate']); ?>" class="user_date" disabled="">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                 <label>Customer Name</label> <!--select from account where accGpId=37--> 
                                    <?php
                                    $cId=$editRow['customerId']; 
                                    $editSelect2 = 	"select  accountName                                    				
                                    				 from ".TABLE_ACCOUNTS." 
                                    				 where ID='$cId'";
									$editResult2 = 	$db->query($editSelect2);
									$editRow2	 = 	mysql_fetch_array($editResult2);
                                    ?>
                                    <input type="text" name="customerName" value="<?php echo $editRow2['accountName'];?>" disabled="">
                                    <input type="hidden" name="customerId" value="<?php echo $editRow['customerId'];?>">
                                </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer ID</label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerId" autocomplete="off" required="" value="<?php echo $editRow['customerId'];?>" disabled="">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
						<!--<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
                            <div class="form_block">
                                <label>Agent Name <span class="valid">*</span></label>
                                <select name="agentId" required="" disabled="">
                                    <option value="">Select</option>
                                    <?php
                                    $select = "select ID,accountName 
                                    			from " . TABLE_ACCOUNTS . " 
			                                    where accountGroupId=48 
			                                    AND finYear='$finYear'
			                                    AND proId='$proId'	
			                                    Order by accountName";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        ?>
                                        <option value="<?php echo $row['ID']; ?>" <?php if($editRow['agentId']==$row['ID']){ echo "selected"; } ?>><?php echo $row['accountName']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>-->
					</div>
					<div class="form_divider"></div>
					<div class="multi_det">
						<div class="multi_det_body">
							<div class="multi_det_head">
								<h3>Passenger List</h3>
								<div class="bd_clear"></div>
							</div>
							<div class="multi_det_table_wrap">
								<table class="table table-bordered multi_det_table" id="misc_passenger_table">
									<thead>
									<tr>
										<th>Sl No</th>
										<th>Passenger Name</th>
										<th>Narration</th>
										<th>Basic Amount</th>
										<th>Processing Charge</th>
									</tr>
									</thead>
									<tbody>
									 <?php				                        
										$i=0;			                        
										$editSelect2 	= 	"select  * from ".TABLE_MISC_PASSENGER." where miscId='$editId' and proId=$proId";
										//echo $editSelect2;
										$editResult2 	= 	$db->query($editSelect2);
										$rowCountEdit	=	mysql_num_rows($editResult2);
										//echo 
										while($editRow2	= 	mysql_fetch_array($editResult2)){
											$miscNaraId = $editRow2['narration'];
											?>
									<tr>
										<td><?php echo ++$i; ?> </td>
										<td>
											<div style="position: relative" class="aj_wrap">
												<div class="aj_rel_box">
													<input class="pass_to_table" data-action="live_passenger" data-field_name="passengerName" type="text" name="passengerName_<?php echo $i;?>"  autocomplete="off" required="" value="<?php echo $editRow2['passengerName'];?>" disabled="">
													<ul class="aj_live_items">

													</ul>
												</div>
											</div>
										</td>
										<td>
											<select name="narration_<?php echo $i; ?>" data-field_name="narration" disabled="">
												<option value="">Select</option>
												<?php
												$nrrQry	=	mysql_query("SELECT * FROM ".TABLE_MISC_NARRATION." where (proId=$proId and finYear=$finYear) or proId='0' and ID='$miscNaraId'");
												if(mysql_num_rows($nrrQry))
												{
													while($nrrRow	=	mysql_fetch_array($nrrQry))
													{
														?>
														<!--<option value="<?php echo $nrrRow['ID']; ?>"><?php echo $nrrRow['narration']; ?></option>-->
														<option value="<?php echo $nrrRow['ID'];?>" <?php if($nrrRow['ID']==$editRow2['narration']){ echo 'selected';}?> ><?php echo $nrrRow['narration'];?></option>
														<?php
													}
												}
												?>
											</select>
										</td>
										<td><input class="visa_input" type="text" data-net="net_trigger" data-field_name="basicAmount" name="basicAmount_<?php echo $i; ?>" value="<?php echo $editRow2['basicAmount'];?>" disabled=""></td>
										<td><input class="visa_input" type="text" data-net="net_trigger" data-field_name="procCharge" value="<?php echo $editRow2['procCharge'];?>" name="procCharge_<?php echo $i; ?>" disabled=""></td>
										
									</tr>
									<?php	
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="form_divider"></div>
					<div class="row">
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>TDS</label>
                                <input type="text" data-net="net_trigger" name="tds" value="<?php echo $editRow['tds'];?>" disabled="">
                            </div>
                        </div>
                        
                     	<div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Other Charges</label>
                                <input type="text" data-net="net_trigger" name="otherCharge" value="<?php echo $editRow['otherCharge'];?>" disabled="">
                            </div>
                        </div>
                       <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Tax ('%' for percentage)</label>
                                <input type="text" data-net="net_trigger" name="tax" value="<?php echo $editRow['tax'];?>" disabled="">
                            </div>
                        </div> 
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Discount</label>
                                <input type="text" data-net="net_trigger" name="discount" value="<?php echo $editRow['discount'];?>" disabled="">
                            </div>
                        </div>
                 <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Net amount</label>
                                <input type="text" data-net="net_trigger" name="netAmount" value="<?php echo $editRow['netAmount'];?>" disabled="">
                            </div>
                        </div>
                   <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Description</label>
                                <textarea name="description" disabled=""> <?php echo $editRow['description'];?></textarea>
                            </div>
                        </div>
                       
                    </div>
               </div>
                
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
