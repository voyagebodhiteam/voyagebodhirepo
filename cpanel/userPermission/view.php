<?php
require('../admin_header.php');
if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$proId	=	$_SESSION['proId'];
$staffId	=	$_GET['id'];
?>

<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post"  class="default_form">
                <div class="bd_panel_head">
                    <h3>STAFF PERMISSION</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                         <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form_block">
                                <label>Staff Name</label>
                                <select id="staffId" name="staffId" required  title="Name"  disabled="">
				                    <option>Select</option>
				                    <?php				                     
				                    $selectAll = "select  * from ".TABLE_STAFF." where proId='$proId'";
								    $result = $db->query($selectAll);
								    while ($row = mysql_fetch_array($result)) {
									    ?>
									    <option value="<?php echo $row['ID'] ?>" <?php if($staffId==$row['ID']){ echo "selected";} ?>><?php echo $row['staffName']; ?></option>
										<?php
				                     }
				                     ?>
				                     </select>
                            </div>
                        </div>
                   </div>
                 <div class="row">
                     <div class="col-lg-4 col-md-4 col-sm-4">
                     	<div id="user_permission">
                     	<?php
                     		$select2=mysql_query("SELECT * FROM ".TABLE_PERMISSION." WHERE staffId='$staffId'");
							$rowcount=mysql_num_rows($select2);
							$row2=mysql_fetch_array($select2);
							

							?>		  				
							<input type="hidden" id="staffTableId" name="staffTableId" value="<?php echo $staffId; ?>">


							<table border="1" class="table" align="center">
								<tr>
								
									<td colspan="8" style="text-align:left;"><b>Staff ID:</b><?php echo $row2['staffId'];?></td>			
								</tr>
								<tr>
									<th align="center">Menu</th>
									<th align="center">Add/Print</th>
									<th align="center">Edit</th>
									<th align="center">Delete</th>
									<th align="center" style="padding-left:50px;">Menu</th>
									<th align="center">Add/Print</th>
									<th align="center">Edit</th>		
									<th align="center">Delete</th>		
								</tr>
								<tr>
									<td align="center">Basic Setting</td>
									<td align="center"><input type="checkbox" name="basic1" id="basic1" <?php if($row2['basicSetting']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="basic2" id="basic2" <?php if($row2['basicSettingEdit']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="basic3" id="basic3" <?php if($row2['basicSettingDelete']==1){?> checked="checked"<?php }?> disabled=""></td>		
											
									<!--<td align="center">Package</td>
									<td align="center"><input type="checkbox" name="package1" id="package1" <?php if($row2['package']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="package2" id="package2" <?php if($row2['packageEdit']==1){?> checked="checked"<?php }?> disabled=""></td>-->										<td align="center">Cash Bank</td>
									<td align="center"><input type="checkbox" name="cashbank1" id="cashbank1" <?php if($row2['cashBank']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="cashbank2" id="cashbank2" <?php if($row2['cashBankEdit']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="cashbank3" id="cashbank3" <?php if($row2['cashBankDelete']==1){?> checked="checked"<?php }?> disabled=""></td>
										
								</tr>
								<tr>
									<td align="center">Account</td>
									<td align="center"><input type="checkbox" name="account1" id="account1" <?php if($row2['account']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="account2" id="account2" <?php if($row2['accountEdit']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="account3" id="account3" <?php if($row2['accountDelete']==1){?> checked="checked"<?php }?> disabled=""></td>
												
									<td align="center">Invoice Hotel</td>
									<td align="center"><input type="checkbox" name="invoicehotel1" id="invoicehotel1" <?php if($row2['invoiceHotel']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoicehotel2" id="invoicehotel12" <?php if($row2['invoiceHotelEdit']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoicehotel3" id="invoicehotel3" <?php if($row2['invoiceHotelDelete']==1){?> checked="checked"<?php }?> disabled=""></td>		
								</tr>
								
								<tr>
									<td align="center">Invoice Airline</td>
									<td align="center"><input type="checkbox" name="invoiceairline1" id="invoiceairline1" <?php if($row2['invoiceAirline']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoiceairline2" id="invoiceairline2" <?php if($row2['invoiceAirlineEdit']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoiceairline3" id="invoiceairline3" <?php if($row2['invoiceAirlineDelete']==1){?> checked="checked"<?php }?> disabled=""></td>	
									
									<td align="center">Invoice Miscellenious</td>
									<td align="center"><input type="checkbox" name="invoicemisc1" id="invoicemisc1" <?php if($row2['invoiceMisc']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoicemisc2" id="invoicemisc2" <?php if($row2['invoiceMiscEdit']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoicemisc3" id="invoicemisc3" <?php if($row2['invoiceMiscDelete']==1){?> checked="checked"<?php }?> disabled=""></td>			
									
								</tr>
								
								<tr>
									<td align="center">Invoice Visa</td>
									<td align="center"><input type="checkbox" name="invoicevisa1" id="invoicevisa1" <?php if($row2['invoiceVisa']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoicevisa2" id="invoicevisa2" <?php if($row2['invoiceVisaEdit']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoicevisa3" id="invoicevisa3" <?php if($row2['invoiceVisaDelete']==1){?> checked="checked"<?php }?> disabled=""></td>
												
									<td align="center">Invoice Package</td>
									<td align="center"><input type="checkbox" name="invoicepackage1" id="invoicepackage1" <?php if($row2['invoicePackage']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoicepackage2" id="invoicepackage2" <?php if($row2['invoicePackageEdit']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoicepackage3" id="invoicepackage3" <?php if($row2['invoicePackageDelete']==1){?> checked="checked"<?php }?> disabled=""></td>						
											
								</tr>
								
								<tr>
								
								<td align="center">Invoice Transport</td>
									<td align="center"><input type="checkbox" name="invoicetransport1" id="invoicetransport1" <?php if($row2['invoiceTransport']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoicetransport2" id="invoicetransport2" <?php if($row2['invoiceTransportEdit']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="invoicetransport3" id="invoicetransport3" <?php if($row2['invoiceTransportDelete']==1){?> checked="checked"<?php }?> disabled=""></td>
									
									<td align="center">Other Report</td>
									<td align="center"><input type="checkbox" name="otherReport" id="otherReport" <?php if($row2['otherReport']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="otherReport" id="otherReport" <?php if($row2['otherReport']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="otherReport" id="otherReport" <?php if($row2['otherReport']==1){?> checked="checked"<?php }?> disabled=""></td>			
								
								<!--<td align="center">Purchase Hotel</td>
									<td align="center"><input type="checkbox" name="purchasehotel1" id="purchasehotel1" <?php if($row2['purchaseHotel']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="purchasehotel2" id="purchasehotel2" <?php if($row2['purchaseHotelEdit']==1){?> checked="checked"<?php }?> disabled=""></td>	
								-->
											
											
								</tr>
								

								
								
							<!--<tr>
									<td align="center">Purchase Airline</td>
									<td align="center"><input type="checkbox" name="purchaseairline1" id="purchaseairline1" <?php if($row2['purchaseAirline']==1){?> checked="checked"<?php }?> disabled=""></td>
								
									<td align="center"><input type="checkbox" name="purchaseairline2" id="purchaseairline2" <?php if($row2['purchaseAirlineEdit']==1){?> checked="checked"<?php }?> disabled=""></td>	
									
							<td align="center">Purchase Miscellenious</td>
									<td align="center"><input type="checkbox" name="purchasemisc1" id="purchasemisc1" <?php if($row2['purchaseMisc']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="purchasemisc2" id="purchasemisc2" <?php if($row2['purchaseMiscEdit']==1){?> checked="checked"<?php }?> disabled=""></td>					
							</tr>-->
								
								<!--<tr>
									<td align="center">Purchase Visa</td>
									<td align="center"><input type="checkbox" name="purchasevisa1" id="purchasevisa1" <?php if($row2['purchaseVisa']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="purchasevisa2" id="purchasevisa2" <?php if($row2['purchaseVisaEdit']==1){?> checked="checked"<?php }?> disabled=""></td>			
									<td align="center">Purchase Package</td>
									<td align="center"><input type="checkbox" name="purchasepackage1" id="purchasepackage1" <?php if($row2['purchasePackage']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="purchasepackage2" id="purchasepackage2" <?php if($row2['purchasePackageEdit']==1){?> checked="checked"<?php }?> disabled=""></td>			
											
								</tr>-->
								
								<tr>
									<!--<td align="center">Purchase Transport</td>
									<td align="center"><input type="checkbox" name="purchasetransport1" id="purchasetransport1" <?php if($row2['purchaseTransport']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="purchasetransport2" id="purchasetransport2" <?php if($row2['purchaseTransportEdit']==1){?> checked="checked"<?php }?> disabled=""></td>			
									-->
									
											
								</tr>
								

							<tr>			
									<td align="center">Account Report</td>
									<td align="center"><input type="checkbox" name="accountReport" id="accountReport" <?php if($row2['accountReport']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="accountReport" id="accountReport" <?php if($row2['accountReport']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="accountReport" id="accountReport" <?php if($row2['accountReport']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td></td>
									<td></td>
									<td></td>		
									<td></td>		
								</tr>
								
							<!--<tr>
									<td align="center">User </td>
									<td align="center"><input type="checkbox" name="user1" id="user1" <?php if($row2['user']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td align="center"><input type="checkbox" name="user2" id="user2" <?php if($row2['userEdit']==1){?> checked="checked"<?php }?> disabled=""></td>
									<td></td>
									<td></td>
									<td></td>	
							</tr>-->

								
								<tr>
									<td colspan="8">
										
									</td>
								</tr>
							</table>
                     	</div>
                     </div>  
                </div> 
             </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>

