<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear=$_SESSION['finYear'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype){
	// add SECTION
	case 'add':
	
	if(!$_REQUEST['staffTableId']){	
		$_SESSION['msg']="Error, Invalid Details!";					
		header("location:add.php");		
	}
	else{		
		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
		$db->connect();
		$success=$success2=0;	
		$staffTableId		=	$_POST['staffTableId'];

				
		//$basic1 = 0;
		@$basic1				=	$_POST['basic1'];
		@$basic2				=	$_POST['basic2'];
		@$basic3				=	$_POST['basic3'];
		@$package1			=	$_POST['package1'];
		@$package2			=	$_POST['package2'];
		@$package3			=	$_POST['package3'];
		@$account1			=	$_POST['account1'];
		@$account2			=	$_POST['account2'];
		@$account3			=	$_POST['account3'];
		@$cashbank1			=	$_POST['cashbank1'];
		@$cashbank2			=	$_POST['cashbank2'];
		@$cashbank3			=	$_POST['cashbank3'];
		@$invoiceairline1	=	$_POST['invoiceairline1'];
		@$invoiceairline2	=	$_POST['invoiceairline2'];
		@$invoiceairline3	=	$_POST['invoiceairline3'];
		@$invoicehotel1		=	$_POST['invoicehotel1'];
		@$invoicehotel2		=	$_POST['invoicehotel2'];
		@$invoicehotel3		=	$_POST['invoicehotel3'];
		@$invoicevisa1		=	$_POST['invoicevisa1'];
		@$invoicevisa2		=	$_POST['invoicevisa2'];
		@$invoicevisa3		=	$_POST['invoicevisa3'];
		@$invoicemisc1		=	$_POST['invoicemisc1'];
		@$invoicemisc2		=	$_POST['invoicemisc2'];
		@$invoicemisc3		=	$_POST['invoicemisc3'];
		@$invoicetransport1	=	$_POST['invoicetransport1'];
		@$invoicetransport2	=	$_POST['invoicetransport2'];
		@$invoicetransport3	=	$_POST['invoicetransport3'];
		@$invoicepackage1	=	$_POST['invoicepackage1'];
		@$invoicepackage2	=	$_POST['invoicepackage2'];
		@$invoicepackage3	=	$_POST['invoicepackage3'];
		@$purchaseairline1	=	$_POST['purchaseairline1'];
		@$purchaseairline2	=	$_POST['purchaseairline2'];
				
		@$purchasehotel1		=	$_POST['purchasehotel1'];
		@$purchasehotel2		=	$_POST['purchasehotel2'];
		@$purchasevisa1		=	$_POST['purchasevisa1'];
		@$purchasevisa2		=	$_POST['purchasevisa2'];
		@$purchasemisc1		=	$_POST['purchasemisc1'];
		@$purchasemisc2		=	$_POST['purchasemisc2'];
		@$purchasetransport1	=	$_POST['purchasetransport1'];
		@$purchasetransport2	=	$_POST['purchasetransport2'];
		@$purchasepackage1	=	$_POST['purchasepackage1'];
		@$purchasepackage2	=	$_POST['purchasepackage2'];

		@$accountReport		=	$_POST['accountReport'];
		@$otherReport		=	$_POST['otherReport'];
		@$user1				=	$_POST['user1'];
		@$user2				=	$_POST['user2'];
		@$user3				=	$_POST['user3'];

	
			
		$data['staffId']				=	$App->convert($staffTableId);
				
		if($basic1=="")				
		$data['basicSetting']			=	0;
		else
		$data['basicSetting']			=	1;
		if($basic2=="")				
		$data['basicSettingEdit']		=	0;
		else
		$data['basicSettingEdit']		=	1;
		if($basic3=="")				
		$data['basicSettingDelete']		=	0;
		else
		$data['basicSettingDelete']		=	1;
		/*if($dataResetting=="")				
		$data['dataResetting']			=	0;
		else
		$data['dataResetting']			=	1;*/
		if($package1=="")				
		$data['package']			=	0;
		else
		$data['package']			=	1;
					
		if($package2=="")				
		$data['packageEdit']			=	0;
		else
		$data['packageEdit']			=	1;
		if($package3=="")				
		$data['packageDelete']			=	0;
		else
		$data['packageDelete']			=	1;
					
		if($account1=="")				
		$data['account']				=	0;
		else
		$data['account']				=	1;
		if($account2=="")				
		$data['accountEdit']			=	0;
		else
		$data['accountEdit']			=	1;
		if($account3=="")				
		$data['accountDelete']			=	0;
		else
		$data['accountDelete']			=	1;
					
		if($cashbank1=="")				
		$data['cashBank']			=	0;
		else
		$data['cashBank']			=	1;
		if($cashbank2=="")				
		$data['cashBankEdit']			=	0;
		else
		$data['cashBankEdit']			=	1;
		if($cashbank3=="")				
		$data['cashBankDelete']			=	0;
		else
		$data['cashBankDelete']			=	1;
				
		if($invoiceairline1==""){
		$data['invoiceAirline']				=	0;
		$data['purchaseAirline']			=	0;
		}				
		
		else {
			$data['invoiceAirline']				=	1;
			$data['purchaseAirline']			=	1;
		}
		
		if($invoiceairline2=="") {
			$data['invoiceAirlineEdit']			=	0;
			$data['purchaseAirlineEdit']			=	0;
		}				
		
		else {
			$data['invoiceAirlineEdit']			=	1;
			$data['purchaseAirlineEdit']		=	1;
		}
		if($invoiceairline3=="") {
			$data['invoiceAirlineDelete']			=	0;
			$data['purchaseAirlineDelete']			=	0;
		}				
		
		else {
			$data['invoiceAirlineDelete']			=	1;
			$data['purchaseAirlineDelete']			=	1;
		}
		
		
		
		if($invoicehotel1=="") {
			$data['invoiceHotel']			=	0;
			$data['purchaseHotel']			=	0;
		}				
		
		else {
			$data['invoiceHotel']			=	1;
			$data['purchaseHotel']			=	1;
		}
		
		if($invoicehotel2=="")	{
			$data['invoiceHotelEdit']		=	0;
			$data['purchaseHotelEdit']	=	0;
		}
		else {
			$data['invoiceHotelEdit']		=	1;
			$data['purchaseHotelEdit']	=	1;
		}
		if($invoicehotel3=="")	{
			$data['invoiceHotelDelete']		=	0;
			$data['purchaseHotelDelete']	=	0;
		}
		else {
			$data['invoiceHotelDelete']		=	1;
			$data['purchaseHotelDelete']	=	1;
		}
		
		
		
					
		if($invoicevisa1==""){
			$data['invoiceVisa']	=	0;
			$data['purchaseVisa']	=	0;
		}				
		
		else {
			$data['invoiceVisa']	=	1;
			$data['purchaseVisa']	=	1;
		}
		
		if($invoicevisa2=="") {
			$data['invoiceVisaEdit']	=	0;
			$data['purchaseVisaEdit']	=	0;
		}				
		
		else {
			$data['invoiceVisaEdit']	=	1;
			$data['purchaseVisaEdit']	=	1;
		}
		if($invoicevisa3=="") {
			$data['invoiceVisaDelete']	=	0;
			$data['purchaseVisaDelete']	=	0;
		}				
		
		else {
			$data['invoiceVisaDelete']	=	1;
			$data['purchaseVisaDelete']	=	1;
		}
		
	
		if($invoicemisc1=="") {
			$data['invoiceMisc']		=	0;
			$data['purchaseMisc']		=	0;
		}				
		
		else {
			$data['invoiceMisc']		=	1;
			$data['purchaseMisc']		=	1;
		}
		
		if($invoicemisc2=="") {
			$data['invoiceMiscEdit']	=	0;
			$data['purchaseMiscEdit']	=	0;
		}				
		
		else {
			$data['invoiceMiscEdit']	=	1;
			$data['purchaseMiscEdit']	=	1;
		}
		if($invoicemisc3=="") {
			$data['invoiceMiscDelete']	=	0;
			$data['purchaseMiscDelete']	=	0;
		}				
		
		else {
			$data['invoiceMiscDelete']	=	1;
			$data['purchaseMiscDelete']	=	1;
		}
		
					
		if($invoicetransport1=="") {
			$data['invoiceTransport']	=	0;
			$data['purchaseTransport']	=	0;
		}				
		
		else {
			$data['invoiceTransport']	=	1;
			$data['purchaseTransport']	=	1;
		}
		
		if($invoicetransport2=="") {
			$data['invoiceTransportEdit']	=	0;
			$data['purchaseTransportEdit']	=	0;
		}				
		
		else {
			$data['invoiceTransportEdit']	=	1;
			$data['purchaseTransportEdit']	=	1;
		}
		if($invoicetransport3=="") {
			$data['invoiceTransportDelete']	=	0;
			$data['purchaseTransportDelete']	=	0;
		}				
		
		else {
			$data['invoiceTransportDelete']	=	1;
			$data['purchaseTransportDelete']	=	1;
		}
		
					
		if($invoicepackage1==""){
			$data['invoicePackage']			=	0;
			$data['purchasePackage']		=	0;
		}				
		
		else {
			$data['invoicePackage']			=	1;
			$data['purchasePackage']		=	1;
		}
		
		if($invoicepackage2==""){
			$data['invoicePackageEdit']		=	0;
			$data['purchasePackageEdit']	=	0;
		}				
		
		else {
			$data['invoicePackageEdit']		=	1;
			$data['purchasePackageEdit']	=	1;
		}
		if($invoicepackage3==""){
			$data['invoicePackageDelete']		=	0;
			$data['purchasePackageDelete']	=	0;
		}				
		
		else {
			$data['invoicePackageDelete']		=	1;
			$data['purchasePackageDelete']		=	1;
		}
		
				
				
		/*if($purchaseairline1=="")				
		$data['purchaseAirline']				=	0;
		else
		$data['purchaseAirline']				=	1;
		if($purchaseairline2=="")				
		$data['purchaseAirlineEdit']			=	0;
		else
		$data['purchaseAirlineEdit']			=	1;
		if($purchasehotel1=="")				
		$data['purchaseHotel']			=	0;
		else
		$data['purchaseHotel']			=	1;
		if($purchasehotel2=="")				
		$data['purchaseHotelEdit']	=	0;
		else
		$data['purchaseHotelEdit']	=	1;
					
		if($purchasevisa1=="")				
		$data['purchaseVisa']	=	0;
		else
		$data['purchaseVisa']	=	1;
		if($purchasevisa2=="")				
		$data['purchaseVisaEdit']	=	0;
		else
		$data['purchaseVisaEdit']	=	1;
	
		if($purchasemisc1=="")				
		$data['purchaseMisc']				=	0;
		else
		$data['purchaseMisc']				=	1;
		if($purchasemisc2=="")				
		$data['purchaseMiscEdit']			=	0;
		else
		$data['purchaseMiscEdit']			=	1;
					
		if($purchasetransport1=="")				
		$data['purchaseTransport']				=	0;
		else
		$data['purchaseTransport']				=	1;
		if($purchasetransport2=="")				
		$data['purchaseTransportEdit']			=	0;
		else
		$data['purchaseTransportEdit']			=	1;
					
		if($purchasepackage1=="")				
		$data['purchasePackage']				=	0;
		else
		$data['purchasePackage']				=	1;
		if($purchasepackage2=="")				
		$data['purchasePackageEdit']			=	0;
		else
		$data['purchasePackageEdit']			=	1;*/
		
		if($accountReport=="")				
		$data['accountReport']		=	0;
		else
		$data['accountReport']		=	1;
		if($otherReport=="")				
		$data['otherReport']			=	0;
		else
		$data['otherReport']			=	1;
						
		if($user1=="")				
		$data['user']				=	0;
		else
		$data['user']				=	1;
		if($user2=="")				
		$data['userEdit']				=	0;
		else
		$data['userEdit']				=	1;
		if($user3=="")				
		$data['userDelete']				=	0;
		else
		$data['userDelete']				=	1;
		
		//print_r($data);die;						
				
		$rowCount				=	$App->convert($_REQUEST['rowCount']);
			//cheking atleast one is checked
		
		if($data['userEdit']!=0 || $data['userDelete']!=0 || $data['user']!=0 || $data['otherReport']!=0 || $data['accountReport']!=0 || $data['purchasePackageEdit']!=0 || $data['purchasePackageDelete']!=0 || $data['purchasePackage']!=0 || $data['purchaseTransportEdit']!=0 || $data['purchaseTransportDelete']!=0 || $data['purchaseTransport']!=0 || $data['purchaseMiscEdit']!=0 || $data['purchaseMiscDelete']!=0 || $data['purchaseMisc']!=0 || $data['purchaseVisaEdit']!=0 || $data['purchaseVisaDelete']!=0 || $data['purchaseVisa']!=0 || $data['purchaseHotelEdit']!=0 || $data['purchaseHotelDelete']!=0 || $data['purchaseHotel']!=0 || $data['purchaseAirlineEdit']!=0 || $data['purchaseAirlineDelete']!=0 || $data['purchaseAirline']!=0 || $data['invoicePackageEdit']!=0 || $data['invoicePackageDelete']!=0 || $data['invoicePackage']!=0 || $data['invoiceTransportEdit']!=0 || $data['invoiceTransportDelete']!=0 || $data['invoiceTransport']!=0 || $data['invoiceMiscEdit']!=0 || $data['invoiceMiscDelete']!=0 || $data['invoiceMisc']!=0 || $data['invoiceVisaEdit']!=0 || $data['invoiceVisaDelete']!=0 || $data['invoiceVisa']!=0 || $data['invoiceHotelEdit']!=0 || $data['invoiceHotelDelete']!=0 || $data['invoiceHotel']!=0 || $data['invoiceAirlineEdit']!=0 || $data['invoiceAirlineDelete']!=0 || $data['invoiceAirline']!=0 || $data['cashBankEdit']!=0 || $data['cashBankDelete']!=0 || $data['cashBank']!=0 || $data['accountEdit']!=0 || $data['accountDelete']!=0 || $data['account']!=0 || $data['packageEdit']!=0 || $data['packageDelete']!=0 || $data['package']!=0 || $data['basicSettingEdit']!=0 || $data['basicSettingDelete']!=0 || $data['basicSetting']!=0  )									{
			if($rowCount == 0) // new staff permission inserting
			{
				$success=$db->query_insert(TABLE_PERMISSION,$data,"staffId='{$staffTableId}'");
				//echo $success;die;
			}
			else	// existing staff permission updating
			{
				$success2=$db->query_update(TABLE_PERMISSION,$data,"staffId='{$staffTableId}'");
			}
		}	
		else
		{
			if($rowCount== 0)// no any permission selected for the new user
			{
				$success3 = 1; 
			} 
			else // existing staff without any permission  deleting
			{
				$success3 = @mysql_query("DELETE FROM `" . TABLE_PERMISSION . "` WHERE staffId='{$staffTableId}'");
			}
			
		}	
					
	if($success){
		$_SESSION['msg'] =  $App->sessionMsgCreate('success', "User Permission Updated Successfully");		
						
		
	}
	else if($success2){
		$_SESSION['msg'] = $App->sessionMsgCreate('success', "User Permission Updated Successfully");
							
		
	}
	else if($success3){
		$_SESSION['msg'] = $App->sessionMsgCreate('error', "Didn't set any permission!");
							
		
	}
	else{
		$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed");
						
	}
	header("location:index.php");										
		}
	break;
		
	// DELETE SECTION
	case 'delete':
	$deleteId = $_REQUEST['id'];//echo $deleteId;die;
	$success = $success2 = 0;

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
	$db->connect();

		$success2 = "DELETE FROM `".TABLE_LOGIN."` WHERE userId='{$deleteId}' and type='Staff'";
		$db->query($success2);
		$success = @mysql_query("DELETE FROM `" . TABLE_PERMISSION . "` WHERE ID='{$deleteId}'");
	$db->close();
	if($success){
		$_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
	} else{
		$_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
	}
	header("location:index.php");
	break;

					
}
?>