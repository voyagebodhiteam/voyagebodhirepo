<?php
require('../admin_header.php');
if(@isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>

		
<div class="row">
	<div class="col-lg-12">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form method="post" action="do.php?op=up" class="default_form" enctype="multipart/form-data">
				
				<div class="bd_panel_head">
					<h3>FILE UPLOAD</h3>
				</div>
				<div class="bd_panel_body">
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer Name</label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerName" autocomplete="off" required="">								
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer ID</label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerId" autocomplete="off" required="" >
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Choose file(.csv)</label>
								<input type="file" name="sel_file" id="sel_file" required=""></td> 
							</div>
						</div>
						
					</div>                    
					<div class="row">
						<div class="col-lg-12">
							<div class="airline_submit">                             
								<input type="submit" value="SAVE">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>                
	</div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
