<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}
$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear=$_SESSION['finYear'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['passengerName']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;

            $passengerName		= $App->capitalize($_REQUEST['passengerName']);                      
			$mobile				= $App->convert($_REQUEST['mobile']);

            /*$existId = $db->existValuesId(TABLE_PASSENGER, "passengerName='$passengerName' and mobile='$mobile' AND proId=$proId");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {*/
            	$data['customerId']		=	$App->convert($_REQUEST['customerId']);
                $data['passengerName']	=	$passengerName;
                if($_REQUEST['dob'])
                {
					$data['dob']			=	$App->dbformat_date($_REQUEST['dob']);
				}
                $data['prefix']			=	$App->convert($_REQUEST['prefix']);
                $data['age']			=	$App->convert($_REQUEST['age']);
                $data['gender']			=	$App->convert($_REQUEST['gender']);
                $data['placeOfBirth']	=	$App->convert($_REQUEST['placeOfBirth']); 
                $data['relationshipId']	=	$App->convert($_REQUEST['relationship']);
                $data['mobile']			=	$App->convert($_REQUEST['mobile']);
                $data['nationality']	=	$App->convert($_REQUEST['nationality']); 
                $data['passportNo']		=	$App->convert($_REQUEST['passportNo']);
                if($_REQUEST['dateOfIssue'])
                {
                $data['dateOfIssue']	=	$App->dbformat_date($_REQUEST['dateOfIssue']); 
                }
                if($_REQUEST['validUpTo'])
                {
                $data['validUpTo']		=	$App->dbformat_date($_REQUEST['validUpTo']); 
				}
                $data['issuePlace']		=	$App->convert($_REQUEST['issuePlace']);               
                $data['mofa']			=	$App->convert($_REQUEST['mofa']);               
                $data['emofa']			=	$App->convert($_REQUEST['emofa']);               
				$data['proId']			=	$proId;
                $data['loginId']		=	$loginId;
                $data['finYear']		=	$finYear;
                
                if($_FILES['photo'])
                {
                	if($App->imageValidation($_FILES['photo']['name']))
                	{
						$file 	=	$_FILES['photo']['tmp_name'];
						$date	= 	date("YmdHis");

						$path_info 	= 	pathinfo($_FILES["photo"]["name"]);
						$ext	 	=	$path_info["extension"];
						
						$newfilename = $_REQUEST['passportNo'].'.' . $ext;
						
						if(move_uploaded_file($file, "../../uploads/passenger/" . $newfilename))
		                {
							$img = "uploads/passenger/". $newfilename;		
						}

						$data['passPhoto']	=	$App->convert($img);
					}
					
				}
                
                $success = $db->query_insert(TABLE_PASSENGER,$data);
                $db->close();

                if ($success) 
                {                  
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
           // }
        }
        break;
    // EDIT SECTION
    case 'edit':
        $editId = $_REQUEST['editId'];
        if (!$_REQUEST['passengerName']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:edit.php?id=$editId");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;

            $passengerName		= $App->capitalize($_REQUEST['passengerName']);                      
			$mobile				= $App->convert($_REQUEST['mobile']);

            /*$existId = $db->existValuesId(TABLE_PASSENGER, "passengerName='$passengerName' and mobile='$mobile' and ID!='$editId' AND proId=$proId");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {*/
               
                $data['passengerName']	=	$passengerName;
                $data['customerId']		=	$App->convert($_REQUEST['customerId']);
				if($_REQUEST['dob'])
                {
					$data['dob']			=	$App->dbformat_date($_REQUEST['dob']);
				}                
				$data['age']			=	$App->convert($_REQUEST['age']);
                $data['prefix']			=	$App->convert($_REQUEST['prefix']);
                $data['gender']			=	$App->convert($_REQUEST['gender']);
                $data['placeOfBirth']	=	$App->convert($_REQUEST['placeOfBirth']); 
                $data['relationshipId']	=	$App->convert($_REQUEST['relationship']);
                $data['mobile']			=	$App->convert($_REQUEST['mobile']);
                $data['nationality']	=	$App->convert($_REQUEST['nationality']); 
                $data['passportNo']		=	$App->convert($_REQUEST['passportNo']);
				if($_REQUEST['dateOfIssue'])
                {
               		$data['dateOfIssue']	=	$App->dbformat_date($_REQUEST['dateOfIssue']); 
                }
                if($_REQUEST['validUpTo'])
                {
                	$data['validUpTo']		=	$App->dbformat_date($_REQUEST['validUpTo']); 
				}
                $data['issuePlace']		=	$App->convert($_REQUEST['issuePlace']);  
                $data['mofa']			=	$App->convert($_REQUEST['mofa']);               
                $data['emofa']			=	$App->convert($_REQUEST['emofa']);     
                $data['proId']			=	$proId;
                $data['loginId']		=	$loginId;
                $data['finYear']		=	$finYear;
                
                if($_FILES['photo'])
                {
                	if($App->imageValidation($_FILES['photo']['name']))
                	{
						$file 	=	$_FILES['photo']['tmp_name'];
						$date	= 	date("YmdHis");

						$path_info 	= 	pathinfo($_FILES["photo"]["name"]);
						$ext	 	=	$path_info["extension"];
						
						$newfilename = $_REQUEST['passportNo'].'.' . $ext;
						
						if(move_uploaded_file($file, "../../uploads/passenger/" . $newfilename))
		                {
		                	$res	=	mysql_query("select passPhoto from ".TABLE_PASSENGER." where ID='{$editId}'");
							$row	=	mysql_fetch_array($res);
							$imageold	=	"../../".$row['passPhoto'];
							if (file_exists($imageold)) 	
							{
								unlink($imageold);					
							}
							
							$img="uploads/passenger/". $newfilename;
						}

						$data['passPhoto']	=	$App->convert($img);
					}
					
				}
                $success = $db->query_update(TABLE_PASSENGER, $data, " ID='{$editId}'");
                $db->close();

                if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to update details. Please try again.");                   
                }
                 header("location:index.php");
           // }
        }
        break;
       
  case 'up': 						
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				    $file = $_FILES['sel_file']['name'];/*echo $file;die;*/
					$chk_exit=explode('.',$file);
					if(strtolower(end($chk_exit))=="csv")
					{
						$filename=$_FILES['sel_file']['tmp_name'];
						//echo $filename;
						$handle = fopen($filename, "r");
						fgetcsv($handle, 1000, ",");
						while(($filesop = fgetcsv($handle, 1000, ",")) !== FALSE)
						{
							
							/* $passengerName	= $filesop[0];
							 $dob 	= $filesop[1];
							 $age	= $filesop[2];
							 $pan 	= $filesop[3];
							 $man	= $filesop[4];
							 $dist 	= $filesop[5];
							 $res 	= $filesop[6];*/
							if($filesop[1] && $filesop[8] && $filesop[10] && $filesop[4]){
								
							$data['customerId']		=	$App->convert($_REQUEST['customerId']);
							$data['passengerName']	=	$App->convert($filesop[1]);//echo $var;die;
							$data['gender']			=	strtoupper($App->convert($filesop[2]));
							$data['nationality']	=	$App->convert($filesop[3]); 
							$data['passportNo']		=	$App->convert($filesop[4]);
							if($filesop[5])
			                {
			                	$data['dateOfIssue']	=	$App->dbformat_date($filesop[5]); 
							}
							if($filesop[6])
							{
			               		$data['validUpTo']		=	$App->dbformat_date($filesop[6]); 
							}
							$data['issuePlace']		=	$App->convert($filesop[7]); 
							if($filesop[8])
							{
			                	$data['dob']			=	$App->dbformat_date($filesop[8]); 
							}
							$data['placeOfBirth']	=	$App->convert($filesop[9]);
			                
							$relShip	=	$App->convert($filesop[10]);//echo $relShip;die;
							
							$query = "SELECT ".TABLE_RELATIONSHIP.".ID,
									 ".TABLE_RELATIONSHIP.".relationship 
							         from ".TABLE_RELATIONSHIP." where ".TABLE_RELATIONSHIP.".relationship='$relShip'";
							$result = $db->query($query);
                        	$fetch = mysql_fetch_array($result);
                        	$rela=$fetch['ID'];//echo $rela;die;
                        	$relationship	=	$fetch['relationship'];
                        	
                        	if($relShip!=$relationship)
                        	{
                        		$data3['relationship']  =   $App->convert($filesop[10]);
                        		$data3['proId']			=	$proId;
			                	$data3['loginId']		=	$loginId;
								$data3['finYear']		=	$finYear;
								$reQuery	=	 $db->query_insert(TABLE_RELATIONSHIP, $data3);
								$dample	=	$reQuery;
							}
                        	else {
								 $dample	=	$App->convert($rela);//echo $dample;die;
							}
                        	
                        	
                        	//$dample	=	$App->convert($filesop[6]);echo $dample;die;
                        	
						    $data['relationshipId'] = $dample;
							
                        	
			                $data['mofa']			=	$App->convert($filesop[11]);       
			                $data['emofa']			=	$App->convert($filesop[12]);      
			                $data['proId']			=	$proId;
			                $data['loginId']		=	$loginId;
							$data['finYear']		=	$finYear;
						
						  
							$sql = $db->query_insert(TABLE_PASSENGER, $data);
							}
						}
						fclose($handle);
						if($sql || $filesop[1]!='' && $filesop[8]!='' && $filesop[4]!='' && $filesop[10]!='')
						{	
						 $_SESSION['msg'] = $App->sessionMsgCreate('success', "Result imported successfully"); 	
						}
						else if($filesop[1]==NULL && $filesop[4]==NULL && $filesop[8]==NULL && $filesop[10]==NULL) {
						$_SESSION['msg'] = $App->sessionMsgCreate('error', "Please provide every data");
							}		
					}
					else{
						 $_SESSION['msg'] = $App->sessionMsgCreate('error', "Invalid file"); 
					}
					
					header("location:index.php");												
									
		break;     
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
		
		$res	=	mysql_query("select passPhoto from ".TABLE_PASSENGER." where ID='{$deleteId}' and finYear=$finYear");
		$row	=	mysql_fetch_array($res);
		$image	=	"../../".$row['passPhoto'];
		
        try 
        {
            $success = @mysql_query("DELETE FROM `" . TABLE_PASSENGER . "` WHERE ID='{$deleteId}' and finYear=$finYear");
            if (file_exists($image)) 	
			{
			unlink($image);					
			}
        } 
        catch (Exception $e) 
        {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");	
        }
        $db->close();
        if ($success) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
}
?>