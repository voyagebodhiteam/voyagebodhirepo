<?php
require('../admin_header.php');
if(@isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>

	//calc age
	function getAge()
	{
		var dob = document.getElementById('dob').value;
		//alert(dob);
		//today
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		
		/*if(today.getFullYear()- dob.getFullYear()<1)
		{
			alert ("false");
		}*/
		
		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 

		today = dd+'-'+mm+'-'+yyyy;
		//today end
		var dobSplit= dob.split('-');
		var todaySplit= today.split('-');

		var dobLast = new Date(dobSplit[2], +dobSplit[1]-1, dobSplit[0]);
		var todayLast = new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
		var dateDiff=(todayLast.getTime() - dobLast.getTime()) / (1000*60*60*24);
		//alert(dateDiff);
		var age=Math.round(dateDiff/365);
		if(dateDiff<=0 || age > 150)
		{
			alert("Invalid Date of Birth");
			dateDiff="";
			document.getElementById('dobdiv').value	=1;
			return false;
		}
		else
		{
			var age=Math.round(dateDiff/365);
			document.getElementById('age').value=age;
			document.getElementById('dobdiv').value	='';
			return true;
		}
		
	}
	
	//function for validating date of issue
	function validateDateIssue()
	{
		var dob = document.getElementById('dob').value;
		var issueDate	=	document.getElementById('dateOfIssue').value;
		//alert(issueDate);
		var today 		= 	new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		
		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		}
		today = dd+'-'+mm+'-'+yyyy;
		
		var issueDateSplit	= 	issueDate.split('-');
		var dobSplit		=	dob.split('-');
		//alert(dobSplit);
		var todaySplit		= 	today.split('-');
		
		var issueDateLast 	= 	new Date(issueDateSplit[2], +issueDateSplit[1]-1, issueDateSplit[0]);
		var dobLast			=	new Date(dobSplit[2], +dobSplit[1]-1, dobSplit[0]);
		//alert(dobLast);
		var todayLast 		= 	new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
		var dateDiff		=	(todayLast.getTime() - issueDateLast.getTime()) / (1000*60*60*24);
		var dobDiff			=	(dobLast.getTime() - issueDateLast.getTime()) / (1000*60*60*24);
		
		//alert(dobDiff);
		
		var year = Math.round(dateDiff/365);
		if(dateDiff<=0 || year > 21 || dobDiff>=0)
		{
			alert("Invalid Issue Date");
			document.getElementById('issuediv').value	=1;
			return false;
		}
		else
		{
			document.getElementById('issuediv').value	='';
		}
		
	}
	
	//function for validating date of expiry
	function validateDateExpiry()
	{
		var expiry	=	document.getElementById('validUpTo').value;
		var today 	= 	new Date();
		var dd 		= 	today.getDate();
		var mm 		= 	today.getMonth()+1; //January is 0!
		var yyyy 	= 	today.getFullYear();
		
		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		}
		today = dd+'-'+mm+'-'+yyyy;
		
		var expirySplit	= 	expiry.split('-');
		var todaySplit	= 	today.split('-');
		
		var expiryLast 	= 	new Date(expirySplit[2], +expirySplit[1]-1, expirySplit[0]);
		var todayLast 	= 	new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
		var dateDiff	=	(expiryLast.getTime()-todayLast.getTime()) / (1000*60*60*24);
		
		var year = Math.round(dateDiff/365);
		if(dateDiff<=0 || year > 21)
		{
			alert("Invalid Expiry Date");
			document.getElementById('expirydiv').value	=1;
			return false;
		}
		else
		{
			document.getElementById('expirydiv').value	='';
		}
		
	}

	function validateForm()
	{
		flag=false;		
		
		
	    var customerName	=	document.getElementById('customerName').value;
		var customerId		=	document.getElementById('customerId').value;
		var getResponse=0;
		
		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
           // document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
          	getResponse= xmlhttp.responseText;//alert(getResponse);
          	
          	 
          	
          }	
          
	    };
	   
	   	xmlhttp.open("GET", "../../services/customer_check_ajax.php?name="+customerName+"&id="+customerId, false);
	    xmlhttp.send();
	    
	    
	    
		
		 if(getResponse==0)
	          	{
	          		flag=true;
				}
				
				var dob		=	document.getElementById('dobdiv').value;
				var issue	=	document.getElementById('issuediv').value;
				var expiry	=	document.getElementById('expirydiv').value;
						
				if(dob || issue || expiry)
				{
					document.getElementById('validDiv').innerHTML='Check the dates';
					flag=true;
				}
				
		
		
				
				
				if(flag==true)
				{
					return false;
				}
	}

</script>


		
<div class="row">
	<div class="col-lg-12">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form method="post" action="do.php?op=index" class="default_form" onsubmit="return validateForm()" enctype="multipart/form-data">
				<div class="bd_panel_head">
					<h3>Passenger Registration</h3>
				</div>
				<div class="bd_panel_body">
					<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-2">
						<div class="form_block">
							<label>Customer Name <span class="valid">*</span></label> <!--select from account where accGpId=37-->
							<div class="aj_rel_box aj_customer">
								<input type="text" name="customerName" id="customerName" autocomplete="off" required="">							
								<ul class="aj_live_items">

								</ul>
							</div>
						</div>
					</div>					
					<div class="col-lg-2 col-md-2 col-sm-2">
						<div class="form_block">
							<label>Customer ID </label> <!--select from account where accGpId=37-->
							<div class="aj_rel_box aj_customer">
								<input type="text" name="customerId"  id="customerId" autocomplete="off" required="" >
								<ul class="aj_live_items">

								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
						<div class="form_block">
							<label>Honorifics </label>
							<select name="prefix" id="prefix">
								<option value="">Select</option>
								<option value="Mr">Mr</option>
								<option value="Mrs">Mrs</option>
								<option value="Ms">Ms</option>
							</select>
						</div>
					</div>
					<div class="col-lg-4 col-sm-4 col-md-4 form_block_row">
						<div class="form_block">
							<label>Passenger Name <span class="valid">*</span></label>
							<input type="text" name="passengerName" id="passengerName" required=""></td>
						</div>
					</div>
					<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
						<div class="form_block">
							<label>Gender</label>
							<select class="pass_to_table" name="gender">
								<option value="M" selected>Male</option>
								<option value="F">Female</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
				

				
				


						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">     
							<div class="form_block">
								<label>DOB <span class="valid">*</span></label>
								<input type="text" name="dob" id="dob" class="user_date" required=""  onchange="getAge()">
								<input type="hidden" id="dobdiv" value="" >
							</div>
							
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Age</label>
								<input type="text" name="age" id="age" readonly="">                           
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Place of Birth</label>
								<input type="text" name="placeOfBirth" id="placeOfBirth">
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Nationality</label>
								<input type="text" name="nationality" id="nationality" value="Indian">
							</div>
						</div>  
						<div class="col-lg-2 col-sm-2 col-md-2">
							<div class="form_block">
								<label>Passport No <span class="valid">*</span></label>
								<input class="pass_to_table" type="text" name="passportNo" required="" >
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2">
							<div class="form_block">
								<label>Date of Issue</label>
								<input class="pass_to_table user_date" type="text" name="dateOfIssue" onchange="validateDateIssue()" id="dateOfIssue">
								<input type="hidden" id="issuediv" value="" >
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2">
							<div class="form_block">
								<label>Date of Expiry</label>
								<input class="pass_to_table user_date" type="text" name="validUpTo" onchange="validateDateExpiry()" id="validUpTo" >
								<input type="hidden" id="expirydiv" value="" >
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2">
							<div class="form_block">
								<label>Place of Issue</label>
								<input class="pass_to_table" type="text" name="issuePlace" >
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Relationship <span class="valid">*</span></label>
                                    <select name="relationship" class="pass_to_table" required="">
	                                    <option value="">Select</option>
	                                    <?php 
	                                    $relQry	=	mysql_query("SELECT * FROM ".TABLE_RELATIONSHIP." where proId=$proId and finYear=$finYear");
	                                    while($relRow	=	mysql_fetch_array($relQry))
	                                    {
	                                    ?>
										<option value="<?php echo $relRow['ID']; ?>"><?php echo $relRow['relationship'];?></option>
										<?php }?>
									</select>
								</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
							<div class="form_block">
								<label>Mahram</label>
								<input type="text" name="mobile" id="mobile" >
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2">
							<div class="form_block">
								<label>MOFA</label>
								<input class="pass_to_table" type="text" name="mofa" >
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2">
							<div class="form_block">
								<label>EMOFA</label>
								<input class="pass_to_table" type="text" name="emofa" >
							</div>
						</div>
					</div> 
					<div class="row"> 
						<div class="col-lg-2 col-sm-2 col-md-2">
							<div class="form_block">
								<label>Upload Photo</label>
								<input type="file" name="photo" />
							</div>
						</div>
					</div>   
					<div name="validDiv" id="validDiv" style="color:#FF6600;"></div>               
					<div class="row">
						<div class="col-lg-12">
							<div class="airline_submit">                             
								<input type="submit" value="SAVE">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>                
	</div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
