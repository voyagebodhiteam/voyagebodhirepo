<?php
require('../admin_header.php');

if($_SESSION['travelType']=='Admin')
{
	$check	=	"".TABLE_PASSENGER.".finYear='$finYear'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"".TABLE_PASSENGER.".proId='$proId' and ".TABLE_PASSENGER.".finYear='$finYear'";
}

$type = $_SESSION['travelType'];

//for edit and delete permission
$logID = $_SESSION['travelId'];

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
//delete row in index page

function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Passenger Registration</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                        
                        	<input type="text" name="searchName" id="" placeholder="Passenger Name" value="<?php echo @$_REQUEST['searchName']; ?>">
                            <input type="search" name="searchPass" placeholder="Passport No" value="<?php echo @$_REQUEST['searchPass']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="add.php">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                                <a href="file_upload.php">
                                    <span class="page_nav_ico"></span>
                                    Import file
                                </a>
                                
                                
                                <a href="passenger.csv"  >
                                    <span class="page_nav_ico"></span>
                                    Download format
                                </a>

                            </li>
                        </ul>
                    </div>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    
<?php	
$cond="1";
if(@$_REQUEST['searchName'])
{
	$cond=$cond." and ".TABLE_PASSENGER.".passengerName like'%".$_POST['searchName']."%'";
}
if(@$_REQUEST['searchPass'])
{
$cond=$cond." and ".TABLE_PASSENGER.".passportNo like'%".$_POST['searchPass']."%'";
}

?>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Staff Name</th>
                            <th>Customer Name</th>
                            <th>Customer ID</th>
                            <th>Customer Mobile</th>
                            <th>Passenger Name</th>
                            <th>Passport No</th>
                            <th>Age</th>   
                            <th>Photo</th>              
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                     	$i=0;
                        $selectAll = "select ".TABLE_PASSENGER.".ID,
                                                                 ".TABLE_PASSENGER.".prefix,
                        					 ".TABLE_PASSENGER.".passengerName,
                        					 ".TABLE_PASSENGER.".passportNo,
                        					 ".TABLE_PASSENGER.".dob,
                        					 ".TABLE_PASSENGER.".age,
                        					 ".TABLE_PASSENGER.".passPhoto,
                        					 ".TABLE_PASSENGER.".loginId,
                        					 ".TABLE_ACCOUNTS.".ID as accId,
                        					 ".TABLE_ACCOUNTS.".accountName,
                        					 ".TABLE_ACCOUNTS.".mobile,
                        					 ".TABLE_STAFF.".staffName
                        					 
                        					 FROM ".TABLE_PASSENGER.",".TABLE_ACCOUNTS.",".TABLE_STAFF.",".TABLE_LOGIN." 
                        					 WHERE ".TABLE_PASSENGER.".customerId = ".TABLE_ACCOUNTS.".ID
                        					 AND ".TABLE_PASSENGER.".loginId = ".TABLE_LOGIN.".ID
                        					 AND ".TABLE_LOGIN.".userId		=	".TABLE_STAFF.".ID
                        					 AND $cond and $check ORDER BY ID desc";
                        					 //echo $selectAll;
                        
                        $result = $db->query($selectAll);
                        $selectCount = mysql_num_rows($result);
                        
                        if($selectCount==0)
						{
						?>
							 <tr>
								<td align="center" colspan="10">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
                        /*********************** for pagination ******************************/
						$rowsPerPage = ROWS_PER_PAGE;
						if(isset($_GET['page']))
						{
							$pageNum = $_GET['page'];
						}
						else
						{
							$pageNum =1;
						}
						$offset = ($pageNum - 1) * $rowsPerPage;
						$select1=$db->query($selectAll." limit $offset, $rowsPerPage");
						$i=$offset+1;
						//use '$select1' for fetching
						/*************************** for pagination **************************/
                        while ($row = mysql_fetch_array($select1)) {
                        	$loginIDs=$row['loginId'];
                        	//echo $loginIDs;
                                $prefix = $row['prefix'];
				                            ?>
				                         <tr>                            
				                            <td><?php echo $i++; ?></td>
				                            <td><?php echo $row['staffName']; ?></td>
				                            <td><?php echo $row['accountName']; ?></td>
				                            <td><?php echo $row['accId']; ?></td>
				                            <td><?php echo $row['mobile']; ?></td>
				                            <?php if($prefix!='') { ?>
				                            <td><?php echo $prefix.'.'.$row['passengerName']; ?></td>
				                  <?php } else {  ?>
								  	        <td><?php echo $row['passengerName']; ?></td>
								  <?php }    ?> 
				                            <td><?php echo $row['passportNo']; ?></td>
				                            <td><?php echo $row['age']; ?></td>
				                            <td><img src="<?php if($row['passPhoto']){ echo '../../'.$row['passPhoto'];} ?>" alt="No Image" width="60"/></td>
				                            
				                            <td>
				                                <a class="show_table_lnk show_table_lnk_view" data-view_node="get_com_serv" data-com_serv_type="pass" data-view_id="<?= $row['ID']; ?>" href="#" data-modal_target="#acs_view_pop">View</a>
				                                <?php if($basicSettingEdit && $logID==$loginIDs){?>
				                                <a class="show_table_lnk show_table_lnk_edit" href="edit.php?id=<?php echo $row['ID'];?>">Edit</a>
				                                <a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?id=<?php echo $row['ID'];?>&op=delete">Delete</a>
				                                <?php } //end of if condition?>
				                            </td>
				                        </tr>
				                            <?php
				                        }
				                        }
				                        ?>
				                   
                       
                    </tbody>
                </table>
            </div>
            
            <!-- paging -->		
            <!--<div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div> -->       
            <!-- paging end-->
            
            <!--*****************************************************************-->
            
            	 <?php 
                  if($selectCount>@$rowsPerPage)
					{
					?>	
					 <br />	
					  <div class="pagerSC" align="center">
					<?php
					
					$query   =  $db->query($selectAll);
					$numrows = mysql_num_rows($query);
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
					   if ($page == $pageNum)
					   {
						  $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
					   }
					   else
					   {
						 	if(@$search)
					   		 {
							 	$nav .= " <a href=\"$self?page=$page&sname=$search\">$page</a> ";
							 }
							 else
							 {
							 	$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							 }
					   }
					}
					?>
					 <?php
					if ($pageNum > 1)
					{
					   $page  = $pageNum - 1;
					   if(@$search)
					   {
						   $prev  = " <a href=\"$self?page=$page&sname=$search\">Prev</a> ";
						   $first = " <a href=\"$self?page=1&sname=$search\">First Page</a> ";
					   }
					   else
					   {
						   $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
						   $first = " <a href=\"$self?page=1\">First Page</a> ";
					   }
					}
					else
					{
					   $prev  = '&nbsp;';
					   $first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
					   $page = $pageNum + 1;
					   if(@$search)
					   {
						   	 $next = " <a href=\"$self?page=$page&sname=$search\">Next</a> ";
						     $last = " <a href=\"$self?page=$maxPage&sname=$search\">Last Page</a> ";
					   }
					   else
					   {
						   	 $next = " <a href=\"$self?page=$page\">Next</a> ";
						   	 $last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
					   }
					}
					else
					{
					   $next = '&nbsp;';
					   $last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
        </div>
    </div>

<!-- Popups -->
    <!--Popup for account View-->
    <div id="acs_view_pop" class="modal fade bd_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Passenger Details</h4></div>
                <div class="modal-body">
                    <div class="acs_view_table_wrap table-responsive">
                        <table class="table table-bordered table-striped acs_view_table">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>