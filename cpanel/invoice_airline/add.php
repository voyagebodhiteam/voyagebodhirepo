<?php
require("../admin_header.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if(isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';
?>
<script>

	//calc age
	function getAge()
	{
		var dob=document.getElementById('dob').value;
		//alert(dob);
		//today
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 

		today = dd+'-'+mm+'-'+yyyy;
		//today end
		var dobSplit= dob.split('-');
		var todaySplit= today.split('-');

		var dobLast = new Date(dobSplit[2], +dobSplit[1]-1, dobSplit[0]);
		var todayLast = new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
		var dateDiff=(todayLast.getTime() - dobLast.getTime()) / (1000*60*60*24);
		var age=Math.round(dateDiff/365);
		
		if(age<0)
		{
			alert("Invalid Date of birth");
			age="";
		}
		document.getElementById('age').value=age;
	}

</script>
<script>
function valid()
{
	
	flag=false;
	tds=document.getElementById('tds').value;
	mainProcCharge=document.getElementById('mainProcCharge').value;
	discount=document.getElementById('discount').value;
	mainTax=document.getElementById('mainTax');
	mainOtherCharge=document.getElementById('mainOtherCharge').value;
	netAmount=document.getElementById('netAmount').value;
	
	 letterNumber = /^[0-9]*\.?[0-9%]+$/;
	
        rowCount=document.getElementById('passenger_row_count').value;
if(rowCount=="" || rowCount=="0")
	{																			
	document.getElementById('rowCountDiv').innerHTML="Enter passenger details";
	flag=true;
	}
	
	if(isNaN(tds) || tds=="")
	{																			
	document.getElementById('tdsDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(mainProcCharge) || mainProcCharge=="")
	{																			
	document.getElementById('mainProcChargeDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(discount) || discount=="")
	{																			
	document.getElementById('discountDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(!mainTax.value.match(letterNumber))
	{																			
	document.getElementById('mainTaxDiv').innerHTML="Enter valid Tax number.";
	flag=true;
	}
	if(isNaN(mainOtherCharge) || mainOtherCharge=="")
	{																			
	document.getElementById('mainOtherChargeDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(netAmount) || netAmount=="" || netAmount==0 || netAmount <0)
	{																			
	document.getElementById('netAmountDiv').innerHTML="Enter valid amount";
	flag=true;
	}

	//for customer name and customer id validation	
		var customerName	=	document.getElementById('customerName').value;
		var customerId		=	document.getElementById('customerId').value;
		var getResponse=0;
		
		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
           // document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
          	getResponse= xmlhttp.responseText;//alert(getResponse);
          	
          	 
          	
          }	
          
	    };
	   
	   	xmlhttp.open("GET", "../../services/customer_check_ajax.php?name="+customerName+"&id="+customerId, false);
	    xmlhttp.send();
	  
		 		if(getResponse==0)
	          	{
	          		flag=true;
alert("Invalid Customer Name and ID.!");
				}
	
	if(flag==true)
	{
	return false;
	}
	
}

//clear the validation msg

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

</script>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
<div class="row">
	<div class="col-lg-12">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form class="default_form" id="invoice_airline" method="post" action="do.php?op=index" onsubmit="return valid()">
				<div class="bd_panel_head">
					<h3>Airline - Invoice</h3>
				</div>
				<div class="bd_panel_body">
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Voucher No <span class="valid">*</span></label>
								<input type="text" name="invoiceNo"
							value="<?php echo "AI" . $db->nextVoucher(TABLE_INVOICE_AIRLINE, 'invoiceNo',"proId='$proId'"); ?>"
								required="" readonly="" >
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Voucher Date </label>
								<input type="text" name="invoiceDate" class="user_date"
								value="<?php echo date('d-m-Y') ?>" required="" >
							</div>
						</div>
										
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer Name <span class="valid">*</span></label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerName" autocomplete="off" required="" id="customerName" onchange="return customerValid()">								
									<ul class="aj_live_items">

									</ul>
									<div id="nameDiv"></div>
								</div>
							</div>
						</div>					
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer ID </label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerId" autocomplete="off" required="" id="customerId" onchange="return customerValid()">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
                                                <div class="col-lg-2 col-md-2 col-sm-2">
						<div class="form_block">
                                <label>Agent Name <span class="valid">*</span></label>
                                <select name="agentId" required="" class="pass_to_table">
                                    <option value="">Select</option>
                                    <?php
                                    $select = "select ID,accountName 
                                    			from " . TABLE_ACCOUNTS . " 
			                                    where accountGroupId=48
			                                    AND proId='$proId'
			                                    AND finYear='$finYear'
			                                    Order by accountName";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        ?>
                                        <option
                                            value="<?php echo $row['ID']; ?>"><?php echo $row['accountName']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
					</div>
					<div class="form_divider"></div>
					<div class="multi_det">
						<div class="multi_det_body">
<div id="rowCountDiv" class="valid"></div>
							<div class="multi_det_head">
								<h3>Passenger List</h3>
<div id="rowCountDiv" class="valid"></div>
								<div class="multi_det_control">
									<ul class="page_nav">
										<li>
											<a href="#" class="passenger_modal_trigger"
                                                   data-modal_target="#multi_det_add"   onclick="clearbox('rowCountDiv')">
												<span class="page_nav_ico"><i class="ion ion-plus"></i></span>
												Add New
											</a>
										</li>
									</ul>
								</div>
								<div class="bd_clear"></div>
							</div>
							<div class="multi_det_table_wrap table-responsive">
								<table class="table table-bordered multi_det_table" id="passenger_table">
									<thead>
										<tr>
											<th>Sl No</th>
											<th>Passenger</th>
											<th>Ticket No</th>
											<th>From</th>
											<th>To</th>
											<th>FLT No</th>
											<th>Date</th>
											<th>Fare</th>
											<th>Tax</th>
											<th>YQ</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-2 col-sm-2 col-md-2 ">
							<div class="form_block">
								<label>TDS</label>
								<input type="text" name="tds" id="tds" value="0"  onfocus="clearbox('tdsDiv')">
								<div class="valid" id="tdsDiv"></div>
							</div>
						</div>
					
						<div class="col-lg-2 col-sm-2 col-md-2 ">
							<div class="form_block">
								<label>Processing Charge</label>
								<input type="text" name="mainProcCharge" id="mainProcCharge" value="0" onfocus="clearbox('mainProcChargeDiv')">
								<div  class="valid" id="mainProcChargeDiv"></div>
							</div>
						</div>				
						<div class="col-lg-2 col-sm-2 col-md-2 ">
							<div class="form_block">
								<label>Other Charge</label>
								<input type="text" name="mainOtherCharge" id="mainOtherCharge" value="0" onfocus="clearbox('mainOtherChargeDiv')">
								<div  class="valid" id="mainOtherChargeDiv"></div>
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 ">
							<div class="form_block">
								<label>Tax('%' for percentage)</label>
								<input type="text" name="mainTax" id="mainTax" value="0"  onfocus="clearbox('mainTaxDiv')">
								<div  class="valid" id="mainTaxDiv"></div>
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 ">
							<div class="form_block">
								<label>Discount</label>
								<input type="text" name="discount" id="discount"  value="0" onfocus="clearbox('discountDiv')">
								<div  class="valid" id="discountDiv"></div>
							</div>
						</div>
						
						<div class="col-lg-2 col-sm-2 col-md-2 ">
							<div class="form_block">
								<label>PNR</label>
								<input type="text" name="pnr" id="pnr"  value="0" onfocus="clearbox('pnrDiv')">
								<div  class="valid" id="pnrDiv"></div>
							</div>
						</div>
						
						<div class="col-lg-2 col-sm-2 col-md-2 ">
							<div class="form_block">
								<label>Net Amount <span class="valid">*</span></label>
								<input type="text" name="netAmount" required=""  id="netAmount" onfocus="clearbox('netAmountDiv')" readonly="">
								<div  class="valid" id="netAmountDiv"></div>
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2">
							<div class="form_block">
								<label>Description</label>
								<textarea name="description" rows="3"></textarea>
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 ">
							<div class="form_block">
								<label>Remark</label>
								<textarea name="remark" ></textarea>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-12">
							<div class="airline_submit">
								<input type="hidden" name="rowCount" id="passenger_row_count">
								<input type="submit" name="savePurchase" value="SAVE & PURCHASE" onclick="return check()">
								<!--<input type="submit" name="save" value="SAVE">-->
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
require('../admin_footer1.php');
?>
<!-- Popups -->
<div id="multi_det_add" class="modal fade bd_modal passenger_pop" role="dialog">
	<div class="modal-dialog modal-lg full_modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Passenger Details</h4>
			</div>
			<div class="modal-body">
				<form class="default_form">
					<div class="pop_section">
						<h3 class="pop_section_head">Basic Details</h3>
						<div class="row">
						
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Honorifics</label>
									<select class="pass_to_table" name="prefix" id="prefix">
										<option value="">Select</option>
										<option value="Mr">Mr</option>
										<option value="Mrs">Mrs</option>
										<option value="Ms">Ms</option>
									</select>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Passenger <span class="valid">*</span></label>
									<div class="aj_rel_box  aj_passenger">
										<input class="pass_to_table" type="text" name="passengerName" autocomplete="off"  required="">
										<ul class="aj_live_items">

										</ul>
                                                                                <input type="hidden" name="ID" class="pass_to_table"/>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Date of Birth</label>
									<input class="pass_to_table user_date" type="text" name="dob" id="dob" onchange="getAge()">
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Age</label>
									<input class="pass_to_table" type="text" name="age" id="age" >
								</div>
							</div>
						
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Gender</label>
									<select class="pass_to_table" name="gender" >
										<option value="M" selected="selected">Male</option>
										<option value="F">Female</option>										
									</select>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Place of Birth</label>
									<input class="pass_to_table" type="text" name="placeOfBirth" >
								</div>
							</div>					
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Nationality</label>
									<input class="pass_to_table" type="text" name="nationality" >
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label></label>
									<input class="pass_to_table" type="hidden" name="mobile" value="none">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<!--<label>Relationship</label>-->
									<input class="pass_to_table" type="hidden" name="relationship" value="none">
								</div>
							</div>
						</div>
					</div>
					<div class="pop_section">
						<h3 class="pop_section_head">Passport Details</h3>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Passport No</label>
									<input class="pass_to_table" type="text" name="passportNo" >
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Date of Issue</label>
									<input class="pass_to_table user_date" type="text" name="dateOfIssue" >
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Expiry Date</label>
									<input class="pass_to_table user_date" type="text" name="validUpTo">
								</div>
							</div>
						
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Place of issue</label>
									<input class="pass_to_table" type="text" name="issuePlace">
								</div>
							</div>
						</div>
					</div>
					<div class="multi_det_table_wrap table-responsive airline_table">
						<table class="table table-bordered" id="inv_airline_multiple">
							<thead>
								<tr>
									<th colspan="11" valign="middle">Airline Details</th>
									<th colspan="2" class="text-right">
										<button class="bd_btn bd_btn_blue" id="add_airline_row" type="button">
											<i class="ion ion-plus"></i>Add New
										</button>
									</th>
								</tr>
								<tr>
									<th>Sl No</th>
									<th>Name</th>
									<th>Flight No</th>
									<th>Ticket No</th>
									<th>Journey Date</th>
									<th>Sector From</th>
									<th>Sector TO</th>
									<th>Class</th>
									<th>Fare</th>
									<th>Tax</th>
									<th>YQ</th>
									<th>Proc Charge</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>
										<select name="airlineName_1" class="pass_to_table_air">
											<option value="">Select</option>
											<?php
											$select = "select * from " . TABLE_FLIGHT . " WHERE proId=$proId and finYear=$finYear";
											$res = $db->query($select);
											while($row = mysql_fetch_array($res)){
												?>
												<option value="<?php echo $row['ID']; ?>"><?php echo $row['flightName']; ?></option>
												<?php
											}
											?>
										</select>
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="flightNo_1" required="" required="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="ticketNo_1" required="">
									</td>
									<td>
										<input class="pass_to_table_air user_date" type="text" name="journeyDate_1" required="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="sectorFrom_1" required="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="sectorTo_1" required="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="classType_1" required="">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="fare_1" required="" value="0">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="tax_1" required="" value="0">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="yq_1" required="" value="0">
									</td>
									<td>
										<input class="pass_to_table_air" type="text" name="procCharge_1" required=""  value="0">
									</td>
									<td>
										<button class="bd_btn bd_btn_red" data-action="del_airline_row">Delete</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="trigger_pass_table">OK</button>
			</div>
		</div>
	</div>
</div>

<!-- div for select box for airline details -->
<div id="airline_select_options" style="display: none">
	<table>
		<tbody>
			<tr>
				<td></td>
				<td>
					<select name="airlineName" class="pass_to_table_air" required="">
						<option value="">Select</option>
						<?php
						$select = "select * from " . TABLE_FLIGHT . " where proId=$proId and finYear=$finYear";
						$res = $db->query($select);
						while($row = mysql_fetch_array($res)){
							?>
							<option value="<?php echo $row['ID']; ?>"><?php echo $row['flightName']; ?></option>
							<?php
						}
						?>
					</select>
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="flightNo" required="">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="ticketNo" required="">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" data-dp="datepick" name="journeyDate" required="">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="sectorFrom" required="">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="sectorTo" required="">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="classType" required="">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="fare" required="">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="tax" required="">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="yq" required="">
				</td>
				<td>
					<input class="pass_to_table_air" type="text" name="procCharge" required="">
				</td>
				<td>
					<button class="bd_btn bd_btn_red" data-action="del_airline_row">Delete</button>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<!-- div for select box for airline details -->
<?php
require('../admin_footer2.php');
?>