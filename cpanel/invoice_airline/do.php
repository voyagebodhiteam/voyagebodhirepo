<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']==""){
	header("location:../../logout.php");
}

$proId		=	$_SESSION['proId'];
$loginId	=	$_SESSION['travelId'];
$loginType	=	$_SESSION['travelType'];
$finYear=$_SESSION['finYear'];
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
         
if($loginType=='Branch'){
	$loginName1		=	"SELECT ".TABLE_COMPANY.".companyName 
	FROM ".TABLE_LOGIN." ,".TABLE_COMPANY." 						  
	WHERE ".TABLE_COMPANY.".ID=".TABLE_LOGIN.".userId
	AND ".TABLE_LOGIN.".ID=$loginId";
	$loginNameQry	=	mysql_query($loginName1);
	$loginNameRow	=	mysql_fetch_array($loginNameQry);		
	$loginName		=	$loginNameRow['companyName'];			  
}
if($loginType=='Staff'){
	$loginName1		=	"SELECT ".TABLE_STAFF.".staffName, 
	".TABLE_COMPANY.".companyName
	FROM ".TABLE_LOGIN." ,".TABLE_STAFF.",".TABLE_COMPANY." 						  
	WHERE ".TABLE_STAFF.".ID=".TABLE_LOGIN.".userId
	AND ".TABLE_COMPANY.".ID=".TABLE_STAFF.".proId
	AND ".TABLE_LOGIN.".ID=$loginId";
	$loginNameQry	=	mysql_query($loginName1);
	$loginNameRow	=	mysql_fetch_array($loginNameQry);		
	$loginName		=	/*$loginNameRow['companyName']."-".*/$loginNameRow['staffName'];			  
}
$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype){
	// NEW SECTION
	case 'index':

	if(!$_REQUEST['customerId']){
		$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
		header("location:index.php");
	} 
	else{
		$success =  $success2 = $success3 = 0;
		$eachPassTotal 	=	$eachTotal	=	0;
		$newInvId	=	$db->nextVoucher(TABLE_INVOICE_AIRLINE,'invoiceNo',"proId='$proId'"); 	
		$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Airline Receivable'";
		$acc_record = 	$db->query_first($accId);
		$accountId	=	$acc_record['ID'];
					
		$customerId				= 	$App->convert($_REQUEST['customerId']);           
		$data['customerId']		=	$customerId;
                $agentId			= 	$App->convert($_REQUEST['agentId']);           
		$data['agentId']		=	$agentId;
		$data['invoiceNo']		=	$newInvId;
                
		if($_REQUEST['invoiceDate']){
			$data['invoiceDate']	=	$App->dbformat_date($_REQUEST['invoiceDate']);
		} 
		$data['tds']			=	$tds2	=	$App->convert($_REQUEST['tds']); 
		$data['mainProcCharge']	=	$mainProcCharge2 =	$App->convert($_REQUEST['mainProcCharge']); 
		$data['mainOtherCharge']=	$mainOtherCharge2 =	$App->convert($_REQUEST['mainOtherCharge']);
		$data['remark']			=	$App->convert($_REQUEST['remark']); 
		$data['description']	=	$App->convert($_REQUEST['description']);
		$mainTax				=	$App->convert($_REQUEST['mainTax']);
		$pos 					= 	strpos($mainTax, '%');
		if($pos==''){					 
			$data['mainTax']	=	$mainTax;               
		}				
		$data['pnr']			=	$App->convert($_REQUEST['pnr']);
		$pnr 					= 	$App->convert($_REQUEST['pnr']);
		$data['discount']		=	$App->convert($_REQUEST['discount']);
		$data['netAmount']		=	$App->convert($_REQUEST['netAmount']);
		//$data['prefix']			=	$App->convert($_REQUEST['prefix']);  
		
		$data['status']			=	'Pending';
		$data['proId']			=	$proId;
		$data['loginId']		=	$loginId;
		$data['finYear']		=	$finYear;
		$success = $db->query_insert(TABLE_INVOICE_AIRLINE, $data); 
		
		//$customerKey = $success."AI#".$newInvId.$customerId;
		//$datakey['customerKey']	=	$customerKey;
		//$db->query_update(TABLE_INVOICE_AIRLINE,$datakey,"  ID=$success");
		
		         
		$k=1;	
		$passengerArray			=	array();
		$ticketNoArray			=	array();
		$journeyDateArray		=	array();				
		for($k=1;$k<=$_POST['rowCount'];$k++){
			$prefix			=	"pass".$k."_prefix";								
			$passengerName	=	"pass".$k."_passengerName";
			$dob			=	"pass".$k."_dob";
			$age			=	"pass".$k."_age";
			$gender			=	"pass".$k."_gender";
			$placeOfBirth	=	"pass".$k."_placeOfBirth";
			$relationship	=	"pass".$k."_relationship";
			$nationality	=	"pass".$k."_nationality";
			$mobile			=	"pass".$k."_mobile";
			$passportNo		=	"pass".$k."_passportNo";
			$dateOfIssue	=	"pass".$k."_dateOfIssue";					   
			$validUpTo		=	"pass".$k."_validUpTo";
			$issuePlace		=	"pass".$k."_issuePlace";					   
                
			$data2['airlineId']		=	$success;	 
			$data2['passengerName']	=	$App->capitalize($_REQUEST[$passengerName]);
			array_push($passengerArray,$_REQUEST[$passengerName]);		                
			if($_REQUEST[$dob]){
				$data2['dob']		=	$App->dbformat_date($_REQUEST[$dob]);
			}
			$data2['age']			=	$App->convert($_REQUEST[$age]);
			$data2['prefix']		=	$App->convert($_REQUEST[$prefix]);
			$data2['gender']		=	$App->convert($_REQUEST[$gender]);
			@$data2['placeOfBirth']	=	$App->convert($_REQUEST[$placeOfBirth]);
			@$data2['relationship']	=	$App->convert($_REQUEST[$relationship]);
			$data2['nationality']	=	$App->convert($_REQUEST[$nationality]); 
			@$data2['mobile']		=	$App->convert($_REQUEST[$mobile]);		                 
			$data2['passportNo']	=	$App->convert($_REQUEST[$passportNo]);
			$passNo 				=   $App->convert($_REQUEST[$passportNo]);		             
			if($_REQUEST[$dateOfIssue]){
				$data2['dateOfIssue']	=	$App->dbformat_date($_REQUEST[$dateOfIssue]);
			}
			if($_REQUEST[$validUpTo]){
				$data2['validUpTo']		=	$App->dbformat_date($_REQUEST[$validUpTo]);
			}
			$data2['issuePlace']		=	$App->convert($_REQUEST[$issuePlace]);
			$data2['proId']				=	$proId;
			$data2['loginId']			=	$loginId;
			$data2['finYear']			=	$finYear;
		                               

			$success2 = $db->query_insert(TABLE_AIRLINE_PASSENGER, $data2);
		        //new passenger
			$passId				=	"pass".$k."_ID";
			$existPassId 		=   $App->convert($_REQUEST[$passId]);
			if($existPassId=='')
			{
				$passdata['customerId'] 	= 	$customerId;
				$passdata['passengerName'] 	= 	$_REQUEST[$passengerName];
				$passdata['prefix'] 		= 	$_REQUEST[$prefix];
				if($_REQUEST[$dob])
				{
					$passdata['dob'] 		= 	@$App->dbformat_date($_REQUEST[$dob]);
				}
				$passdata['gender'] 		= 	$_REQUEST[$gender];
				$passdata['age'] 			= 	$_REQUEST[$age];
				$passdata['nationality'] 	= 	$_REQUEST[$nationality];
				$passdata['placeOfBirth'] 	= 	$_REQUEST[$placeOfBirth];
				$passdata['relationshipId'] = 	$_REQUEST[$relationship];
				$passdata['mobile'] 		= 	$_REQUEST[$mobile];
				$passdata['passportNo'] 	= 	$_REQUEST[$passportNo];
				
				if($_REQUEST[$dateOfIssue])
				{
					$passdata['dateOfIssue'] 	= 	$App->dbformat_date($_REQUEST[$dateOfIssue]);
				}
				if($_REQUEST[$validUpTo])
				{
					$passdata['validUpTo'] 	= 	$App->dbformat_date($_REQUEST[$validUpTo]);
				}
				$passdata['issuePlace'] 	= 	$_REQUEST[$issuePlace];
				$passdata['loginId']		=	$loginId;
				$passdata['proId']			=	$proId;
				$passdata['finYear']		=	$finYear;
				
				$psuccess	=	$db->query_insert(TABLE_PASSENGER, $passdata);
			}
		      //new passenger end 
                                               
			$p=1;	
			$airlineRowCount="pass".$k."_airlineRowCount";	
			$jDate='';	
			for($p=1;$p<=$_POST[$airlineRowCount];$p++){												 					   
				$airlineName	=	"pass".$k."_airlineName_$p";
				$flightNo		=	"pass".$k."_flightNo_$p";
				$ticketNo		=	"pass".$k."_ticketNo_$p";
				$journeyDate	=	"pass".$k."_journeyDate_$p";					   
				$sectorFrom 	=	"pass".$k."_sectorFrom_$p";
				$sectorTo		=	"pass".$k."_sectorTo_$p";
				$classType		=	"pass".$k."_classType_$p";
				$fare			=	"pass".$k."_fare_$p";
				$tax			=	"pass".$k."_tax_$p";
				$yq				=	"pass".$k."_yq_$p";
				$procCharge		=	"pass".$k."_procCharge_$p";
                	 
				$data3['airlinePassId']	=	$success2;		                
				@$data3['airlineName']	=	$App->convert($_REQUEST[$airlineName]); 
				$data3['flightNo']		=	$App->convert($_REQUEST[$flightNo]);
				$data3['ticketNo']		=	$App->convert($_REQUEST[$ticketNo]); 
				if($_REQUEST[$journeyDate]){
					$data3['journeyDate']	=	$App->dbformat_date($_REQUEST[$journeyDate]);	
				}		           		   
				$data3['sectorFrom']	=	$App->convert($_REQUEST[$sectorFrom]);
				$sectorFrom 			= 	$App->convert($_REQUEST[$sectorFrom]);  
				$data3['sectorTo']		=	$App->convert($_REQUEST[$sectorTo]);
				$sectorTo				=	$App->convert($_REQUEST[$sectorTo]);
				$data3['classType']		=	$App->convert($_REQUEST[$classType]);  
				$fare2		 =	$data3['fare']			=	$App->convert($_REQUEST[$fare]);
				$tax2		 =	$data3['tax']			=	$App->convert($_REQUEST[$tax]);  
				$yq2		 =	$data3['yq']			=	$App->convert($_REQUEST[$yq]); 
				$procCharge2 =	$data3['procCharge']	=	$App->convert($_REQUEST[$procCharge]);
		                
				$eachTotal=$fare2+$tax2+$yq2+$procCharge2;
				$eachPassTotal+=$eachTotal;
		               
				$data3['proId']			=	$proId;
				$data3['loginId']		=	$loginId; 
				$data3['finYear']		=	$finYear; 
				array_push($ticketNoArray,$_REQUEST[$ticketNo]);
				array_push($journeyDateArray,$_REQUEST[$journeyDate]);               

				$success3 = $db->query_insert(TABLE_AIRLINE_FLIGHT, $data3);
		                
			} 
		}//for
		/*-------------------------------------------------------------------------*/
		//tax calculation()		              
		$totAmt			=	$eachPassTotal+$tds2+$mainProcCharge2+$mainOtherCharge2;
		if(strpos ($mainTax,'%')){
			$taxTrim			=	rtrim($mainTax, "%");							
			$mainTax			=	$totAmt*($taxTrim/100);
			$data4['mainTax'] 	=  	$mainTax;
			$db->query_update(TABLE_INVOICE_AIRLINE, $data4 , "ID=$success" );
		}
		/*-------------------------------------------------------------------------*/
		//To transaction table		           
		$passengers		=	implode("/",$passengerArray);
		$ticketNos		=	implode("/",$ticketNoArray);
		$journeyDates	=	implode("/",$journeyDateArray);	
				   		
		//	echo $passengers;
				   							
		$invoiceDetails	=	"INV#AI$newInvId, Pax:$passengers, Passport No:$passNo, Sector:$sectorFrom/$sectorTo, Date:$journeyDates, Tkt:$ticketNos, PNR:$pnr, By:$loginName";
		//echo $invoiceDetails;die;									
		/* ------------------------------------------ */
		$tsdata['fromLedger'] 		=  	$customerId;
		$tsdata['toLedger'] 		=  	$accountId;
		$tsdata['voucherNo']		= 	$newInvId;
		$tsdata['voucherType']		=  	'Airline Receivable';
		$tsdata['credit']			= 	0;
		$tsdata['debit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);					
		$tsdata['addedDate']		=	"NOW()";
		$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
		$tsdata['remark']			=  	'Airline Receivable: '.$newInvId;
		$tsdata['details']			=  	$invoiceDetails; 
		$tsdata['loginId']			=	$loginId;
		$tsdata['proId']			=	$proId;
		$tsdata['finYear']			=	$finYear;
					
		$db->query_insert(TABLE_TRANSACTION, $tsdata);						
		/* ------------------------------------------ */
		/* ------------------------------------------ */
		$tfdata['fromLedger'] 		=  	$accountId;
		$tfdata['toLedger'] 		=  	$customerId;
		$tfdata['voucherNo']		= 	$newInvId;
		$tfdata['voucherType']		=  	'Airline Receivable';
		$tfdata['credit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);					
		$tfdata['debit']			= 	0;
		$tfdata['addedDate']		=	"NOW()";
		$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
		$tfdata['remark']			=  	'Airline Receivable: '.$newInvId;
		$tfdata['details']			=  	$invoiceDetails;
		$tfdata['loginId']			=	$loginId;
		$tfdata['proId']			=	$proId;
		$tfdata['finYear']			=	$finYear;		
		$db->query_insert(TABLE_TRANSACTION, $tfdata);						
		/*----------------------------------------------------------------------------*/
		//discount		          	
		$discount		=	$App->convert($_REQUEST['discount']);
		$tds			=	$App->convert($_REQUEST['tds']);				          		     
		if($discount>0){		          
			$disId 			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given' ";
			$dis_record 	= 	$db->query_first($disId);
			$discountId		=	$dis_record['ID'];
		                	
			$tsdata['fromLedger'] 		=  	$discountId;
			$tsdata['toLedger'] 		=  	$customerId;
			$tsdata['voucherNo']		= 	$newInvId;
			$tsdata['voucherType']		=  	'Airline Receivable';
			$tsdata['credit']			= 	0;
			$tsdata['debit']			=   abs($App->convert($_POST['discount']));
			$tsdata['addedDate']		=	"NOW()";
			$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tsdata['remark']			=  	'Airline Receivable:Dis# '.$newInvId;
			$tsdata['details']			=  	$invoiceDetails;
			$tsdata['loginId']			=	$loginId;
			$tsdata['proId']			=	$proId;
			$tsdata['finYear']			=	$finYear;
					
			$db->query_insert(TABLE_TRANSACTION, $tsdata);							
			/* ------------------------------------------ */
			/* ------------------------------------------ */
			$tfdata['fromLedger'] 		=  	$customerId;
			$tfdata['toLedger'] 		=  	$discountId;
			$tfdata['voucherNo']		= 	$newInvId;
			$tfdata['voucherType']		=  	'Airline Receivable';
			$tfdata['credit']			=   abs($App->convert($_POST['discount']));
			$tfdata['debit']			= 	0;
			$tfdata['addedDate']		=	"NOW()";
			$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tfdata['remark']			=  	'Airline Receivable:Dis#'.$newInvId;
			$tfdata['details']			=  	$invoiceDetails;
			$tfdata['loginId']			=	$loginId;
			$tfdata['proId']			=	$proId;
			$tfdata['finYear']			=	$finYear;
			$db->query_insert(TABLE_TRANSACTION, $tfdata);			
		}
		//tds
		if($tds>0){     
			$tdsQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tds' ";
			$tds_record 		= 	$db->query_first($tdsQuery);
			$tdsId				=	$tds_record['ID'];		                	
			$tsdata['fromLedger'] 		=   $customerId;
			$tsdata['toLedger'] 		=  	$tdsId;
			$tsdata['voucherNo']		= 	$newInvId;
			$tsdata['voucherType']		=  	'Airline Receivable';
			$tsdata['credit']			= 	0;
			$tsdata['debit']			=   abs($App->convert($_POST['tds']));
			$tsdata['addedDate']		=	"NOW()";
			$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tsdata['remark']			=  	'Airline Receivable:Tds#'.$newInvId;
			$tsdata['details']			=  	$invoiceDetails;
			$tsdata['loginId']			=	$loginId;
			$tsdata['proId']			=	$proId;
			$tsdata['finYear']			=	$finYear;
					
			$db->query_insert(TABLE_TRANSACTION, $tsdata);							
			/* ------------------------------------------ */
			/* ------------------------------------------ */
			$tfdata['fromLedger'] 		=  	$tdsId;
			$tfdata['toLedger'] 		=  	$customerId;
			$tfdata['voucherNo']		= 	$newInvId;
			$tfdata['voucherType']		=  	'Airline Receivable';
			$tfdata['credit']			=   abs($App->convert($_POST['tds']));
			$tfdata['debit']			= 	0;
			$tfdata['addedDate']		=	"NOW()";
			$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tfdata['remark']			=  	'Airline Receivable:Tds#'.$newInvId;
			$tfdata['details']			=  	$invoiceDetails;
			$tfdata['loginId']			=	$loginId;
			$tfdata['proId']			=	$proId;
			$tfdata['finYear']			=	$finYear;
				
			$db->query_insert(TABLE_TRANSACTION, $tfdata);				
		}
		//tax
		if($mainTax>0){
			$taxQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tax' ";
			$tax_record 		= 	$db->query_first($taxQuery);
			$taxId				=	$tax_record['ID'];    
			$tsdata['fromLedger'] 		=   $customerId;
			$tsdata['toLedger'] 		=  	$taxId;
			$tsdata['voucherNo']		= 	$newInvId;
			$tsdata['voucherType']		=  	'Airline Receivable';
			$tsdata['credit']			= 	0;
			$tsdata['debit']			=   abs($mainTax);
			$tsdata['addedDate']		=	"NOW()";
			$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tsdata['remark']			=  	'Airline Receivable:Tax#'.$newInvId;
			$tsdata['details']			=  	$invoiceDetails;
			$tsdata['loginId']			=	$loginId;
			$tsdata['proId']			=	$proId;
			$tsdata['finYear']			=	$finYear;
			
			$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
			/* ------------------------------------------ */
			/* ------------------------------------------ */
			$tfdata['fromLedger'] 		=  	$taxId;
			$tfdata['toLedger'] 		=  	$customerId;
			$tfdata['voucherNo']		= 	$newInvId;
			$tfdata['voucherType']		=  	'Airline Receivable';
			$tfdata['credit']			=   abs($mainTax);
			$tfdata['debit']			= 	0;
			$tfdata['addedDate']		=	"NOW()";
			$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tfdata['remark']			=  	'Airline Receivable:Tax#'.$newInvId;
			$tfdata['details']			=  	$invoiceDetails;
			$tfdata['loginId']			=	$loginId;
			$tfdata['proId']			=	$proId;
			$tfdata['finYear']			=	$finYear;
				
			$db->query_insert(TABLE_TRANSACTION, $tfdata);									  
		}
		          
		/*----------------------------------------------------------------------------*/   
		$db->close();
			
		if($success){
			$_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");  
		} 
		else{
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
		}      
		@$sp	=	$_REQUEST['savePurchase'];
		@$s		=	$_REQUEST['save'];
			
		if($sp){
			header("location:../purchase/add.php?type=a&pId=$success");
		}
		else if($s){
			header("location:index.php");
		}

	}
	break;
	// EDIT SECTION
	case 'edit':
	$editId = $_REQUEST['editId'];
	if(!$_REQUEST['customerId']){
		$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
		header("location:index.php");
	} 
	else{
		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		$db->connect();
		$success =$success1=0;
                $eachPassTotal 	=	$eachTotal	=	0;
   
		$existInvId			=	$db->existVoucherNew(TABLE_INVOICE_AIRLINE,'invoiceNo'," ID='$editId' and proId=$proId");		
		$select=@mysql_query("SELECT ID FROM " . TABLE_AIRLINE_PASSENGER . " WHERE airlineId='{$editId}' and proId=$proId and finYear=$finYear");
			
		$i=0;
		$passId = array();
		while($res=mysql_fetch_array($select)){
			$passId[$i]=$res['ID'];
			$i++;
		}
		$count=count($passId);
		for($i=0;$i<$count;$i++){
			$eachId=$passId[$i];
			@mysql_query("DELETE FROM `" . TABLE_AIRLINE_FLIGHT . "` WHERE airlinePassId='{$eachId}' and proId=$proId and finYear=$finYear");	
		}	
		@mysql_query("DELETE FROM `" . TABLE_AIRLINE_PASSENGER . "` WHERE airlineId='{$editId}' and proId=$proId and finYear=$finYear");
		
		@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$existInvId}' and voucherType='Airline Receivable' and proId=$proId and finYear=$finYear");
		
		//for get the current amount in invoice airline for inserting to log
		$oldAmount				=	$db->query_first("SELECT netAmount FROM ".TABLE_INVOICE_AIRLINE." WHERE ID='$editId' and proId=$proId"); 
					
		$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Airline Receivable'";
		$newInvId	=	$existInvId;
		$acc_record = 	$db->query_first($accId);
		$accountId	=	$acc_record['ID'];			
		$customerId				= 	$App->convert($_REQUEST['customerId']);           
		$data['customerId']		=	$customerId;
                $agentId			= 	$App->convert($_REQUEST['agentId']);           
		$data['agentId']		=	$agentId;
		$data['invoiceNo']		=	$newInvId;
                
		if($_REQUEST['invoiceDate']){
			$data['invoiceDate']	=	$App->dbformat_date($_REQUEST['invoiceDate']);
		} 
		$data['tds']			=	$tds2	=	$App->convert($_REQUEST['tds']); 
		$data['mainProcCharge']	=	$mainProcCharge2 =	$App->convert($_REQUEST['mainProcCharge']); 
		$data['mainOtherCharge']=	$mainOtherCharge2 =	$App->convert($_REQUEST['mainOtherCharge']);
		$data['remark']			=	$App->convert($_REQUEST['remark']); 
		$data['description']	=	$App->convert($_REQUEST['description']);
		$mainTax				=	$App->convert($_REQUEST['mainTax']);
		$pos 					= 	strpos($mainTax, '%');
		if($pos==''){					 
			$data['mainTax']	=	$mainTax;               
		}				
		$data['pnr']			=	$App->convert($_REQUEST['pnr']);
		$pnr 					= 	$App->convert($_REQUEST['pnr']);
		$data['discount']		=	$App->convert($_REQUEST['discount']);
		$data['netAmount']		=	$App->convert($_REQUEST['netAmount']);  
		$data['status']			=	'Pending';
		$data['proId']			=	$proId;
		$data['loginId']		=	$loginId;
		$data['finYear']		=	$finYear;
		$success = $db->query_update(TABLE_INVOICE_AIRLINE, $data,"ID=$editId");               
		$k=1;	
		$passengerArray			=	array();
		$ticketNoArray			=	array();
		$journeyDateArray		=	array();				
		for($k=1;$k<=$_POST['rowCount'];$k++){
			$prefix			=	"pass".$k."_prefix";										
			$passengerName	=	"pass".$k."_passengerName";
			$dob			=	"pass".$k."_dob";
			$age			=	"pass".$k."_age";
			$gender			=	"pass".$k."_gender";
			$placeOfBirth	=	"pass".$k."_placeOfBirth";
			$relationship	=	"pass".$k."_relationship";
			$nationality	=	"pass".$k."_nationality";
			$mobile			=	"pass".$k."_mobile";
			$passportNo		=	"pass".$k."_passportNo";
			$dateOfIssue	=	"pass".$k."_dateOfIssue";					   
			$validUpTo		=	"pass".$k."_validUpTo";
			$issuePlace		=	"pass".$k."_issuePlace";					   
                
			$data2['airlineId']		=	$editId;	 
			$data2['passengerName']	=	$App->capitalize($_REQUEST[$passengerName]);
			array_push($passengerArray,$_REQUEST[$passengerName]);		                
			if($_REQUEST[$dob]){
				$data2['dob']		=	$App->dbformat_date($_REQUEST[$dob]);
			}
			$data2['age']			=	$App->convert($_REQUEST[$age]);
			$data2['prefix']		=	$App->convert($_REQUEST[$prefix]);
			$data2['gender']		=	$App->convert($_REQUEST[$gender]);
			@$data2['placeOfBirth']	=	$App->convert($_REQUEST[$placeOfBirth]);
			@$data2['relationship']	=	$App->convert($_REQUEST[$relationship]);
			$data2['nationality']	=	$App->convert($_REQUEST[$nationality]); 
			@$data2['mobile']		=	$App->convert($_REQUEST[$mobile]);		                 
			$data2['passportNo']	=	$App->convert($_REQUEST[$passportNo]);
			$passNo 				=   $App->convert($_REQUEST[$passportNo]);		             
			if($_REQUEST[$dateOfIssue]){
				$data2['dateOfIssue']	=	$App->dbformat_date($_REQUEST[$dateOfIssue]);
			}
			if($_REQUEST[$validUpTo]){
				$data2['validUpTo']		=	$App->dbformat_date($_REQUEST[$validUpTo]);
			}
			$data2['issuePlace']		=	$App->convert($_REQUEST[$issuePlace]);
			$data2['proId']				=	$proId;
			$data2['loginId']			=	$loginId;
			$data2['finYear']			=	$finYear;
		                               

			$success2 = $db->query_insert(TABLE_AIRLINE_PASSENGER, $data2);
                        //new passenger
			$passId				=	"pass".$k."_ID";
			$existPassId 		=   $App->convert($_REQUEST[$passId]);
			if($existPassId=='')
			{
				$passdata['customerId'] 	= 	$customerId;
				$passdata['passengerName'] 	= 	$_REQUEST[$passengerName];
				$passdata['prefix'] 		= 	$_REQUEST[$prefix];
				if($_REQUEST[$dob])
				{
					$passdata['dob'] 		= 	@$App->dbformat_date($_REQUEST[$dob]);
				}
				$passdata['gender'] 		= 	$_REQUEST[$gender];
				$passdata['age'] 			= 	$_REQUEST[$age];
				$passdata['nationality'] 	= 	$_REQUEST[$nationality];
				$passdata['placeOfBirth'] 	= 	$_REQUEST[$placeOfBirth];
				$passdata['relationshipId'] = 	$_REQUEST[$relationship];
				$passdata['mobile'] 		= 	$_REQUEST[$mobile];
				$passdata['passportNo'] 	= 	$_REQUEST[$passportNo];
				
				if($_REQUEST[$dateOfIssue])
				{
					$passdata['dateOfIssue'] 	= 	$App->dbformat_date($_REQUEST[$dateOfIssue]);
				}
				if($_REQUEST[$validUpTo])
				{
					$passdata['validUpTo'] 	= 	$App->dbformat_date($_REQUEST[$validUpTo]);
				}
				$passdata['issuePlace'] 	= 	$_REQUEST[$issuePlace];
				$passdata['loginId']		=	$loginId;
				$passdata['proId']			=	$proId;
				$passdata['finYear']		=	$finYear;
				
				$psuccess	=	$db->query_insert(TABLE_PASSENGER, $passdata);
			}  
			   //new passenger end 
		                
			$p=1;	
			$airlineRowCount="pass".$k."_airlineRowCount";	
			$jDate='';	
			for($p=1;$p<=$_POST[$airlineRowCount];$p++){												 					   
				$airlineName	=	"pass".$k."_airlineName_$p";
				$flightNo		=	"pass".$k."_flightNo_$p";
				$ticketNo		=	"pass".$k."_ticketNo_$p";
				$journeyDate	=	"pass".$k."_journeyDate_$p";					   
				$sectorFrom 	=	"pass".$k."_sectorFrom_$p";
				$sectorTo		=	"pass".$k."_sectorTo_$p";
				$classType		=	"pass".$k."_classType_$p";
				$fare			=	"pass".$k."_fare_$p";
				$tax			=	"pass".$k."_tax_$p";
				$yq				=	"pass".$k."_yq_$p";
				$procCharge		=	"pass".$k."_procCharge_$p";
                	 
				$data3['airlinePassId']	=	$success2;		                
				@$data3['airlineName']	=	$App->convert($_REQUEST[$airlineName]); 
				$data3['flightNo']		=	$App->convert($_REQUEST[$flightNo]);
				$data3['ticketNo']		=	$App->convert($_REQUEST[$ticketNo]); 
				if($_REQUEST[$journeyDate]){
					$data3['journeyDate']	=	$App->dbformat_date($_REQUEST[$journeyDate]);	
				}		           		   
				$data3['sectorFrom']	=	$App->convert($_REQUEST[$sectorFrom]);
				$sectorFrom 			= 	$App->convert($_REQUEST[$sectorFrom]);  
				$data3['sectorTo']		=	$App->convert($_REQUEST[$sectorTo]);
				$sectorTo				=	$App->convert($_REQUEST[$sectorTo]);
				$data3['classType']		=	$App->convert($_REQUEST[$classType]);  
				$fare2		 =	$data3['fare']			=	$App->convert($_REQUEST[$fare]);
				$tax2		 =	$data3['tax']			=	$App->convert($_REQUEST[$tax]);  
				$yq2		 =	$data3['yq']			=	$App->convert($_REQUEST[$yq]); 
				$procCharge2 =	$data3['procCharge']	=	$App->convert($_REQUEST[$procCharge]);
		                
				$eachTotal=$fare2+$tax2+$yq2+$procCharge2;
				$eachPassTotal+=$eachTotal;
		               
				$data3['proId']			=	$proId;
				$data3['loginId']		=	$loginId; 
				$data3['finYear']		=	$finYear; 
				array_push($ticketNoArray,$_REQUEST[$ticketNo]);
				array_push($journeyDateArray,$_REQUEST[$journeyDate]);               

				$success3 = $db->query_insert(TABLE_AIRLINE_FLIGHT, $data3);
		                
			} 
		}//for
		/*-------------------------------------------------------------------------*/
		//tax calculation()		              
		$totAmt			=	$eachPassTotal+$tds2+$mainProcCharge2+$mainOtherCharge2;
		if(strpos ($mainTax,'%')){
			$taxTrim			=	rtrim($mainTax, "%");							
			$mainTax			=	$totAmt*($taxTrim/100);
			$data4['mainTax'] 	=  	$mainTax;
			$db->query_update(TABLE_INVOICE_AIRLINE, $data4 , "ID=$editId" );
		}
		/*-------------------------------------------------------------------------*/
		//To transaction table		           
		$passengers		=	implode("/",$passengerArray);
		$ticketNos		=	implode("/",$ticketNoArray);
		$journeyDates	=	implode("/",$journeyDateArray);	
				   		
		//	echo $passengers;
				   							
		$invoiceDetails	=	"INV#AI$newInvId, Pax:$passengers, Passport No:$passNo, Sector:$sectorFrom/$sectorTo, Date:$journeyDates, Tkt:$ticketNos, PNR:$pnr, By:$loginName";
		//echo $invoiceDetails;die;									
		/* ------------------------------------------ */
		$tsdata['fromLedger'] 		=  	$customerId;
		$tsdata['toLedger'] 		=  	$accountId;
		$tsdata['voucherNo']		= 	$newInvId;
		$tsdata['voucherType']		=  	'Airline Receivable';
		$tsdata['credit']			= 	0;
		$tsdata['debit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);					
		$tsdata['addedDate']		=	"NOW()";
		$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
		$tsdata['remark']			=  	'Airline Receivable: '.$newInvId;
		$tsdata['details']			=  	$invoiceDetails; 
		$tsdata['loginId']			=	$loginId;
		$tsdata['proId']			=	$proId;
		$tsdata['finYear']			=	$finYear;
					
		$db->query_insert(TABLE_TRANSACTION, $tsdata);						
		/* ------------------------------------------ */
		/* ------------------------------------------ */
		$tfdata['fromLedger'] 		=  	$accountId;
		$tfdata['toLedger'] 		=  	$customerId;
		$tfdata['voucherNo']		= 	$newInvId;
		$tfdata['voucherType']		=  	'Airline Receivable';
		$tfdata['credit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);					
		$tfdata['debit']			= 	0;
		$tfdata['addedDate']		=	"NOW()";
		$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
		$tfdata['remark']			=  	'Airline Receivable: '.$newInvId;
		$tfdata['details']			=  	$invoiceDetails;
		$tfdata['loginId']			=	$loginId;
		$tfdata['proId']			=	$proId;
		$tfdata['finYear']			=	$finYear;		
		$db->query_insert(TABLE_TRANSACTION, $tfdata);						
		/*----------------------------------------------------------------------------*/
		//discount		          	
		$discount		=	$App->convert($_REQUEST['discount']);
		$tds			=	$App->convert($_REQUEST['tds']);				          		     
		if($discount>0){		          
			$disId 			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given' ";
			$dis_record 	= 	$db->query_first($disId);
			$discountId		=	$dis_record['ID'];
		                	
			$tsdata['fromLedger'] 		=  	$discountId;
			$tsdata['toLedger'] 		=  	$customerId;
			$tsdata['voucherNo']		= 	$newInvId;
			$tsdata['voucherType']		=  	'Airline Receivable';
			$tsdata['credit']			= 	0;
			$tsdata['debit']			=   abs($App->convert($_POST['discount']));
			$tsdata['addedDate']		=	"NOW()";
			$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tsdata['remark']			=  	'Airline Receivable:Dis# '.$newInvId;
			$tsdata['details']			=  	$invoiceDetails;
			$tsdata['loginId']			=	$loginId;
			$tsdata['proId']			=	$proId;
			$tsdata['finYear']			=	$finYear;
					
			$db->query_insert(TABLE_TRANSACTION, $tsdata);							
			/* ------------------------------------------ */
			/* ------------------------------------------ */
			$tfdata['fromLedger'] 		=  	$customerId;
			$tfdata['toLedger'] 		=  	$discountId;
			$tfdata['voucherNo']		= 	$newInvId;
			$tfdata['voucherType']		=  	'Airline Receivable';
			$tfdata['credit']			=   abs($App->convert($_POST['discount']));
			$tfdata['debit']			= 	0;
			$tfdata['addedDate']		=	"NOW()";
			$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tfdata['remark']			=  	'Airline Receivable:Dis#'.$newInvId;
			$tfdata['details']			=  	$invoiceDetails;
			$tfdata['loginId']			=	$loginId;
			$tfdata['proId']			=	$proId;
			$tfdata['finYear']			=	$finYear;
			$db->query_insert(TABLE_TRANSACTION, $tfdata);			
		}
		//tds
		if($tds>0){     
			$tdsQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tds' ";
			$tds_record 		= 	$db->query_first($tdsQuery);
			$tdsId				=	$tds_record['ID'];		                	
			$tsdata['fromLedger'] 		=   $customerId;
			$tsdata['toLedger'] 		=  	$tdsId;
			$tsdata['voucherNo']		= 	$newInvId;
			$tsdata['voucherType']		=  	'Airline Receivable';
			$tsdata['credit']			= 	0;
			$tsdata['debit']			=   abs($App->convert($_POST['tds']));
			$tsdata['addedDate']		=	"NOW()";
			$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tsdata['remark']			=  	'Airline Receivable:Tds#'.$newInvId;
			$tsdata['details']			=  	$invoiceDetails;
			$tsdata['loginId']			=	$loginId;
			$tsdata['proId']			=	$proId;
			$tsdata['finYear']			=	$finYear;
					
			$db->query_insert(TABLE_TRANSACTION, $tsdata);							
			/* ------------------------------------------ */
			/* ------------------------------------------ */
			$tfdata['fromLedger'] 		=  	$tdsId;
			$tfdata['toLedger'] 		=  	$customerId;
			$tfdata['voucherNo']		= 	$newInvId;
			$tfdata['voucherType']		=  	'Airline Receivable';
			$tfdata['credit']			=   abs($App->convert($_POST['tds']));
			$tfdata['debit']			= 	0;
			$tfdata['addedDate']		=	"NOW()";
			$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tfdata['remark']			=  	'Airline Receivable:Tds#'.$newInvId;
			$tfdata['details']			=  	$invoiceDetails;
			$tfdata['loginId']			=	$loginId;
			$tfdata['proId']			=	$proId;
			$tfdata['finYear']			=	$finYear;
				
			$db->query_insert(TABLE_TRANSACTION, $tfdata);				
		}
		//tax
		if($mainTax>0){
			$taxQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tax' ";
			$tax_record 		= 	$db->query_first($taxQuery);
			$taxId				=	$tax_record['ID'];    
			$tsdata['fromLedger'] 		=   $customerId;
			$tsdata['toLedger'] 		=  	$taxId;
			$tsdata['voucherNo']		= 	$newInvId;
			$tsdata['voucherType']		=  	'Airline Receivable';
			$tsdata['credit']			= 	0;
			$tsdata['debit']			=   abs($mainTax);
			$tsdata['addedDate']		=	"NOW()";
			$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tsdata['remark']			=  	'Airline Receivable:Tax#'.$newInvId;
			$tsdata['details']			=  	$invoiceDetails;
			$tsdata['loginId']			=	$loginId;
			$tsdata['proId']			=	$proId;
			$tsdata['finYear']			=	$finYear;
			
			$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
			/* ------------------------------------------ */
			/* ------------------------------------------ */
			$tfdata['fromLedger'] 		=  	$taxId;
			$tfdata['toLedger'] 		=  	$customerId;
			$tfdata['voucherNo']		= 	$newInvId;
			$tfdata['voucherType']		=  	'Airline Receivable';
			$tfdata['credit']			=   abs($mainTax);
			$tfdata['debit']			= 	0;
			$tfdata['addedDate']		=	"NOW()";
			$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
			$tfdata['remark']			=  	'Airline Receivable:Tax#'.$newInvId;
			$tfdata['details']			=  	$invoiceDetails;
			$tfdata['loginId']			=	$loginId;
			$tfdata['proId']			=	$proId;
			$tfdata['finYear']			=	$finYear;
				
			$db->query_insert(TABLE_TRANSACTION, $tfdata);									  
		}
		          
		/*----------------------------------------------------------------------------*/   
		
		/*-------------------------------- Inserting to Log --------------------------*/ 
		
		$description 			=	"NetAmt:".$App->convert($_POST['netAmount']).",Discount:".$App->convert($_POST['discount']).",Tds:".$App->convert($_POST['tds']).",MainTax:".$mainTax;
		$datalog['module']		=	'Invoice Airline';
		$datalog['tableId']		=	$editId;
		$datalog['invoiceNo']	=	$newInvId;
		$datalog['changeDate']	=	date("Y-m-d");
		$datalog['Status']		=	'Edit';
		$datalog['oldAmt']		=	$oldAmount['netAmount'];
		$datalog['newAmt']		=	$_REQUEST['netAmount'];			
		$datalog['description']	=	$description;
		$datalog['loginId']		=	$loginId;
		$datalog['proId']		=	$proId;
		$datalog['finYear']		=	$finYear;
		
		$db->query_insert(TABLE_LOG, $datalog);
		/*-------------------------------- Inserting to Log Ends --------------------------*/ 
		$db->close();
			
		if($success){
			$_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");  
		} 
		else{
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to updated details. Please try again.");                   
		}
		@$sp	=	$_REQUEST['savePurchase'];
		@$s		=	$_REQUEST['save'];			
		if($sp){
			header("location:../purchase/edit.php?type=a&pId=$editId");
		}
		else if($s){
			header("location:index.php");
		}
	}        
	break;
	// DELETE SECTION
	case 'delete':
	$deleteId = $_REQUEST['id'];//echo $deleteId;die;
	$success1 =0;

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
	$db->connect();
		  
	$existInvId			=	$db->existVoucherNew(TABLE_INVOICE_AIRLINE,'invoiceNo'," ID='$deleteId' and proId=$proId");
    
    //for get the current amount in invoice airline for inserting to log
	$oldAmount	=	$db->query_first("SELECT netAmount,invoiceNo FROM ".TABLE_INVOICE_AIRLINE." WHERE ID='$deleteId' and proId=$proId");   	   
	$description = "InvoiceAmt:".$oldAmount['netAmount'];
	$select=@mysql_query("SELECT ID FROM " . TABLE_AIRLINE_PASSENGER . " WHERE airlineId='{$deleteId}' and proId=$proId and finYear=$finYear");
			
	$i=0;
	$passId = array();
	while($res=mysql_fetch_array($select)){
		$passId[$i]=$res['ID'];
		$i++;
	}
	$count=count($passId);
	for($i=0;$i<$count;$i++){
		$eachId=$passId[$i];
		@mysql_query("DELETE FROM `" . TABLE_AIRLINE_FLIGHT . "` WHERE airlinePassId='{$eachId}' and proId=$proId and finYear=$finYear");	
	}
			
	@mysql_query("DELETE FROM `" . TABLE_AIRLINE_PASSENGER . "` WHERE airlineId='{$deleteId}' and proId=$proId and finYear=$finYear");
	$success1=mysql_query("DELETE FROM `" . TABLE_INVOICE_AIRLINE . "` WHERE ID='{$deleteId}' and proId=$proId and finYear=$finYear");
	@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$existInvId}' and voucherType='Airline Receivable' and proId=$proId and finYear=$finYear");
			
	//purchase
	@$purExe	=	@mysql_query("SELECT invoiceNo,netAmount FROM `" . TABLE_PURCHASE_AIRLINE . "` WHERE ourInvoiceNo='{$existInvId}' and proId=$proId and finYear=$finYear");
	@$purNum		=	mysql_num_rows($purExe);
	if($purNum>0){
		$purRes			=	mysql_fetch_array($purExe);
		$purInvoiceNo	=	$purRes['invoiceNo'];
		
		$description = $description.",PurchaseVoucherNo:".$purRes['invoiceNo'].",PurchaseAmt:".$purRes['netAmount'];
			
		@mysql_query("DELETE FROM `" . TABLE_PURCHASE_AIRLINE . "` WHERE ourInvoiceNo='{$existInvId}' and proId=$proId and finYear=$finYear");
		@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$purInvoiceNo}' and voucherType='Airline Payable' and proId=$proId and finYear=$finYear");
	}
		
			
	//payment
	$payAmt = 0;
	@$payExe	=	@mysql_query("SELECT ID,voucherNo,payingAmount FROM `" . TABLE_CASH_PAYMENT . "` WHERE invoiceNo='{$purInvoiceNo}' and voucherType='airline' and proId=$proId and finYear=$finYear");
	@$payNum		=	mysql_num_rows($payExe);
	if($payNum>0){
		while($payRes	=	mysql_fetch_array($payExe)){
			$payId	=	$payRes['ID'];
			$payVoucherNo	=	$payRes['voucherNo'];
			$payAmt = $payAmt+$payRes['payingAmount'];
			@mysql_query("DELETE FROM `" . TABLE_CASH_PAYMENT . "` WHERE ID='{$payId}' and proId=$proId and finYear=$finYear");
			@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$payVoucherNo}' and (voucherType='Cash Payments' || voucherType='Bank Payments') and proId=$proId and finYear=$finYear");
		}
	}
	$description = $description.",PaidAmt:".$payAmt;	
	//receipt
	
	$recAmt	=	0;
	@$resExe	=	@mysql_query("SELECT ID,voucherNo,payingAmount FROM `" . TABLE_CASH_RECEIPT . "` WHERE invoiceNo='{$existInvId}' and voucherType='airline' and proId=$proId and finYear=$finYear");
	@$resNum		=	mysql_num_rows($resExe);
	if($resNum>0){
		while($ReceiptRes	=	mysql_fetch_array($resExe)){
			$resId			=	$ReceiptRes['ID'];
			$resVoucherNo	=	$ReceiptRes['voucherNo'];
			$recAmt = $recAmt+$ReceiptRes['payingAmount'];
			@mysql_query("DELETE FROM `" . TABLE_CASH_RECEIPT . "` WHERE ID='{$resId}' and proId=$proId and finYear=$finYear");
			@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$resVoucherNo}' and (voucherType='Cash Receipts' || voucherType='Bank Receipts') and proId=$proId and finYear=$finYear");
		}
	}
	$description = $description.",ReceivedAmt:".$recAmt;
	/*-------------------------------- Inserting to Log --------------------------*/ 
	
	$datalog['module']		=	'Invoice Airline';
	$datalog['tableId']		=	$deleteId;
	$datalog['invoiceNo']	=	$oldAmount['invoiceNo'];
	$datalog['changeDate']	=	date("Y-m-d");
	$datalog['Status']		=	'Delete';
	$datalog['oldAmt']		=	$oldAmount['netAmount'];
	$datalog['newAmt']		=	0;			
	$datalog['description']	=	$description;
	$datalog['loginId']		=	$loginId;
	$datalog['proId']		=	$proId;
	$datalog['finYear']		=	$finYear;
	
	$db->query_insert(TABLE_LOG, $datalog);	
	/*-------------------------------- Inserting to Log -------------------------*/ 
			
	$db->close();
	if($success1){
		$_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
	} else{
		$_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
	}
	header("location:index.php");
	break;
        
	// Printing  ========================================================================
	case 'print':
		
	$printID	=	$_REQUEST['id'];
	require_once('../../printing/tcpdf_include.php');
	require_once('../../printing/tcpdf_config_alt.php');
	require_once('../../printing/tcpdf.php');

	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
				
	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
					
	$userId		=	$_SESSION['proId'];
	$sql2 		= 	"SELECT * FROM `".TABLE_COMPANY."` where ID=$userId";
	$vou_record1 = 	$db->query_first($sql2);
	$company 	= 	$vou_record1['companyName'];
	$caddress 	= 	$vou_record1['address'];
	$cphone		=	$vou_record1['phone'];
	$cmobile	=	$vou_record1['mobile'];	
	$cemail		=	$vou_record1['email'];
	$cpin		=	$vou_record1['pin'];
					
	// set default header data
	$pdf->SetHeaderData(0, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING, array(0,0,0), array(0,0,0));
				
	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
	// set default font subsetting mode
	$pdf->setFontSubsetting(true);
				
	// Set font
	// dejavusans is a UTF-8 Unicode font, if you only need to
	// print standard ASCII chars, you can use core fonts like
	// helvetica or times to reduce file size.
	$pdf->SetFont('courier', '', 11, '', true);
				
	// Add a page
	$pdf->AddPage();
				
	// Set some content to print
				
																
	$sql2 = "SELECT a.ID,a.invoiceNo,a.invoiceDate,a.mainProcCharge,a.netAmount,
	a.tds,a.mainOtherCharge,a.mainTax,a.discount,
	b.accountName,b.address,b.mobile,b.phone,b.pin
	FROM ".TABLE_INVOICE_AIRLINE." as a
	LEFT JOIN	".TABLE_ACCOUNTS." as b ON b.ID = a.customerId
	WHERE	a.ID = $printID  and a.proId=$proId and a.finYear=$finYear
	GROUP BY	a.invoiceNo
	";
				
	$vou_record 	= 	$db->query_first($sql2);
				
	$airlineID		=	$vou_record['ID'];	
							
	$invoiceNo 		= 	$vou_record['invoiceNo'];
	$invoiceDate 	= 	$vou_record['invoiceDate'];
	$mainProcCharge	= 	$vou_record['mainProcCharge'];
	$netAmount 		= 	$vou_record['netAmount'];
	$accountName 	= 	$vou_record['accountName'];
	$address	    = 	$vou_record['address'];
	$pin	    	= 	$vou_record['pin'];
	$phone			=	$vou_record['phone'];
	$mobile 		= 	$vou_record['mobile'];
				
	$proCharge		=	$vou_record['mainProcCharge']+$vou_record['tds']+$vou_record['mainOtherCharge'];
				
	$mainTax		=	$vou_record['mainTax'];
	if(strpos ($mainTax,'%')){
		$tax1		=	substr($mainTax,'%');
		$tax2		=	$tax1[0];
		$mainTax	=	$proCharge*($mainTax/100);
	}
				
	if($mainTax){					
		$mainProcCharge	=	($mainTax+$proCharge)-$vou_record['discount'];
	}
	else{
		$mainProcCharge	=	$proCharge-$vou_record['discount'];
	}
				
	//amount to words
	//$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);

	//basic info
	$tbl_header1 = '<div><table style="width: 530px;" cellspacing="0" border="0" >';
	$tbl_footer1 = '</table></div>';
	$tbl1 = '';
				
	$tbl1	=	'<tr>
	<td colspan="6" align="left" ><h2>'.strtoupper($company).'</h2></td>
	</tr>
	<tr>
	<td colspan="6"></td>
	</tr>
	<tr>
	<td colspan="3" align="left">'.$caddress.'<br>'.$cpin.'</td>
	<td colspan="3" align="left">'.$cphone.'<br>'.$cmobile.'<br>'.$cemail.'</td>
	</tr>
	<tr>
	<td colspan="6" ></td>
	</tr>
	<tr>
	<td colspan="3" style="border-top: 0.1px solid black; border-bottom: 0.1px solid black;">To , <strong>'.$accountName.'</strong></td>
							 	
	<td colspan="3" style="border-top: 0.1px solid black;border-bottom: 0.1px solid black;background-color:#F2F2F2; border-left: 0.1px solid black;" align="center">Air Invoice (AI)</td>
							 	
	</tr>
							 
	<tr>
	<td colspan="3"  style="border-bottom: 0.1px solid black;" align="left">'.$address.'<br>'.$pin.'</td>
	<td colspan="3" style="border-left: 0.1px solid black; border-bottom: 0.1px solid black;" align="left">
	<table >
	<tr>
	<td style="width:60px;" >Invoice :</td>
	<td style="width: 60px;" ><strong>'.$invoiceNo.'</strong></td><td style="width:60px;"><strong>'.$App->dbFormat_date($invoiceDate).'</strong></td>
	</tr>
	<tr>
	<td >Booked By :</td><td colspan="2"></td>
	</tr>
	<tr>
	<td >PNR :</td><td colspan="2">0</td>
	</tr>
	</table>
	</td>
							 	
	</tr>
	';
				
	$pdf->SetFontSize(9);
	$pdf->writeHTML($tbl_header1 . $tbl1 . $tbl_footer1, true, true, false, false, '');

	//passenger TABLE
			
	$tbl_header = '<div><table style="width: 638px; " cellspacing="0"    >';
	$tbl_footer = '</table></div>';
	$tbl = '';

	$tbl .= '<tr style="background-color:#F2F2F2">
	<td style="width: 20px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Sr</strong></td>
	<td style="width: 70px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Passenger</strong></td>
	<td style="width: 50px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Sector</strong></td>
	<td style="width: 50px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Cls</strong></td>
	<td style="width: 50px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Flight</strong></td>
	<td style="width: 70px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Travel Date</strong></td>
	<td style="width: 70px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>TicketNo</strong></td>
	<td style="width: 50px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Basic</strong></td>
	<td style="width: 50px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Tax</strong></td>
	<td style="width: 50px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Total</strong></td>
	</tr>
	<tr>
	<td colspan="10"></td>
	</tr>
	';

	$passQry	=	"SELECT c.ID,c.passengerName
	FROM ".TABLE_AIRLINE_PASSENGER." as c
	WHERE	c.airlineId=$airlineID and proId=$proId
	";
	$pass		=	mysql_query($passQry);
	$passNum	=	mysql_num_rows($pass);
	$defRow		=	8;	//for showing constant height for the table
	$i=0;
	$sumTotal2=0;
			
	if($passNum>0){
		while($passRow=mysql_fetch_array($pass)){
			$passengerName 	= 	$passRow['passengerName'];
			$passID			=	$passRow['ID'];
			$flightQry		=	"SELECT d.sectorFrom,d.sectorTo,d.classType,
			d.flightNo,d.journeyDate,d.ticketNo,
			d.fare,d.tax,d.yq,d.procCharge
			FROM ".TABLE_AIRLINE_FLIGHT." as d 
			WHERE d.airlinePassId=$passID and proId=$proId
			";
			$flight			=	mysql_query($flightQry);
					
			$tbl .= '<tr>
			<td style="width: 20px;">'.++$i.'</td>
			<td style="width: 70px;">'.$passengerName.'</td>
						        
			<td colspan = "7">';
			$passTot	=	0;
			if(mysql_num_rows($flight)){
				$tbl.='<div ><table style="width: 638px; " cellspacing="0" border="0">';
				while($flightRow = mysql_fetch_array($flight)){
					$sectorFrom 	= 	$flightRow['sectorFrom'];
					$sectorTo 		= 	$flightRow['sectorTo'];
					$classType 		= 	$flightRow['classType'];
					$flightNo 		= 	$flightRow['flightNo'];
					$journeyDate 	= 	$flightRow['journeyDate'];
					$ticketNo 		= 	$flightRow['ticketNo'];
					$fare 		    = 	$flightRow['fare']+$flightRow['yq']+$flightRow['procCharge'];
					$tax 		    = 	$flightRow['tax'];								
					$passTot		=	$passTot+$fare+$tax;

                                        
					$tbl.='<tr>
					<td style="width: 50px;">'.$sectorFrom.'-'.$sectorTo.'</td>
					<td style="width: 50px;">'.$classType.'</td>
					<td style="width: 50px;">'.$flightNo.'</td>
					<td style="width: 70px;">'.$App->dbformat_date($journeyDate).'</td>
					<td style="width: 70px;">'.$ticketNo.'</td>
					<td style="width: 50px;">'.sprintf("%0.2f",$fare).'</td>
					<td style="width: 50px;">'.sprintf("%0.2f",$tax).'</td>
										
					</tr>';
				}
				$tbl.='</table></div>';
			}
					        	
			$tbl.='    </td>
			<td style="width: 50px;">'.sprintf("%0.2f",round($passTot,2)).'</td>
			</tr>';
           
                        //for getting total +processing charge
			$sumTotal2=$sumTotal2+$passTot;
			
			//Total charge with procesing charge
				
			$totalCharge=$mainProcCharge+$sumTotal2;
		}
	}
	if($passNum<$defRow){
		$staticRow	=	$defRow-$passNum;
		for($i=0;$i<$staticRow;$i++){
			$tbl.='<tr>
			<td colspan="10"></td>
			</tr>';
		}
	}

        $tbl.= '<tr >
							<td colspan="10" ></td>
						</tr>
						
				    		<tr>
				    			<td colspan = "6"  style=" width: 175px; border-top: 0.1px solid black;" ></td>
				    		<td colspan="3" style=" width: 250px; border-top: 0.1px solid black;">Total Charge</td>
				    		<td colspan="1" style=" width:100px; border-top: 0.1px solid black;" align="right">'.sprintf("%0.2f",round($totalCharge,2)).'</td>
				    		
				    		</tr>';
 
	$tbl.= '
	<tr >
	<td colspan = "6" style="border-top: 0.1px solid black; border-right: 0.1px solid black" ></td>
	<td colspan="3" style="border-top: 0.1px solid black;" align="left"></td>
	<td  align="right" style="border-top: 0.1px solid black; " ></td>
	</tr>
				    	
	<tr>
	<td colspan = "6" style="border-right: 0.1px solid black;" ></td>
	<td colspan="3" align="left">Net Invoice Amount</td>
	<td align="right" >'.sprintf("%0.2f",round($netAmount,2)).'</td>
	</tr>
	<tr><td colspan="10" style=" font:courier; border-top: 0.1px solid black" >'.ucfirst($App->amountToWord(round($netAmount,2))).' </td></tr>
				    	
	';
		
	$pdf->SetFontSize(9);
	$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, true, false, false, '');
				
	//terms and conditions
	$tbl_header2 = '<div><table style="width: 530px;" cellspacing="0" border="0" >';
	$tbl_footer2 = '</table></div>';
	$tbl2 = '';
				
	$tbl2	=	'<tr>
	<td  colspan="3" style="border-top: 0.1px solid black" >Terms :</td>
	<td colspan="2" style="border-top: 0.1px solid black" align="right">E.  & O.  E.</td>
	<td style="border-right: 0.1px solid black;border-top: 0.1px solid black"></td>
	<td colspan="1" style="border-top: 0.1px solid black"></td>
	<td colspan="3" style="border-top: 0.1px solid black" align="right" >for '.strtoupper($company).'</td>
	</tr>
	<tr>
	<td colspan="6" style="border-right: 0.1px solid black">
	# Subject to KOZHIKODE jurisdiction.<br>
	# Without original invoice no refund is permissible.<br>
	# Interest @ 24% will be charged on delayed payment<br>
	# Cheque to be drawn in our company name on presentation of invoice.<br>
	# Kindly check all details carefully to avoid un-necessary complications.<br>
									
	</td>
	<td colspan="4"></td>
	</tr>
	<tr>
	<td colspan="4">Ticket Received in Order.</td>
	<td colspan="2" style="border-right: 0.1px solid black" align="right">Receivers Signature</td>
								
	<td colspan="4" align="right">Authorized Signatory</td>
	</tr>
	<tr>
	<td colspan="6" style="border-right: 0.1px solid black; border-bottom: 0.1px solid black">
	</td>
	<td colspan="4" style=" border-bottom: 0.1px solid black"></td>
	</tr>
							
	<tr>
	<td colspan="10" align="center">This is a Computer generated document and does not require any signature.
	</td>
	</tr>
	';
	$pdf->SetFontSize(7);
	$pdf->writeHTML($tbl_header2 . $tbl2 . $tbl_footer2, true, true, false, false, '');
				

	//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
				
	// ---------------------------------------------------------
				
	// Close and output PDF document
	$pdf->Output("'Invoice airline'.$invoiceNo.pdf", 'I');		
	break;
	//Upload doc
	case 'upload':				
			    	
	if(!$_REQUEST['invoiceDocId']){				
		$_SESSION['msg'] = $App->sessionMsgCreate('error', "Error, Invalid Details!"); 
		header("location:index.php");	
	}
	else{				
		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
		$db->connect();
		$success=$document=0;
					
		$invoiceDocId	=	$App->convert($_REQUEST['invoiceDocId']);	
								
				
		// upload file
		$res=mysql_query("select document from ".TABLE_INVOICE_AIRLINE." where ID='{$invoiceDocId}'");
		$row=mysql_fetch_array($res);
		$document=$row['document'];					
								
		$date		= 	date("YmdHis");
		$path_info  = 	pathinfo($_FILES["invDoc"]["name"]);
		$ext	 	=	$path_info["extension"];	
		//$allowed 	=  	array('gif','png','jpg');		// allowed image extensions					
		$newName 	= 	$date.".".$ext;
		/*
		if(in_array($ext,$allowed) ) 	
		{*/
		$img='';	
		if(move_uploaded_file($_FILES["invDoc"]["tmp_name"],"document/".basename($newName))){
			$doc="document/" . $newName;		
		}
		$data['document']			=	$doc;									
		$success1=$db->query_update(TABLE_INVOICE_AIRLINE,$data," ID='{$invoiceDocId}'");					if($document){										
			if(file_exists($document)){
				unlink($document);					
			} 						
		}					
		$db->close();								
		// }
				
		if($success1){	
			$_SESSION['msg'] = $App->sessionMsgCreate('success', "Document uploaded Successfully"); 						
		}
		else{
			/*if(!in_array($ext,$allowed) ) 
			{
			$_SESSION['msg']="You have uploaded an invalid image file";
			}
			else
			{*/	
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed"); 
			// }
					
		}
		header("location:index.php");
	}
																					
	break;


	case 'ticket':				
	if(!$_REQUEST['tDetails']){				
		$_SESSION['msg'] = $App->sessionMsgCreate('error', "You cannot leave the field as empty!"); 
		header("location:index.php");	
	}
	else{				
		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
		$db->connect();
		$success=$document=0;
					
		$invoiceDocId	=	$App->convert($_REQUEST['ticketId']);	
					
		$data['ticketDetails']			=	$App->convert($_REQUEST['tDetails']);								
		$success1=$db->query_update(TABLE_INVOICE_AIRLINE,$data," ID='{$invoiceDocId}'");													
				
				
		if($success1){	
			$_SESSION['msg'] = $App->sessionMsgCreate('success', "Document uploaded Successfully"); 						
		}
		else{
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed"); 
			
					
		}
		header("location:index.php");
	}
																					
	break;       
}
?>