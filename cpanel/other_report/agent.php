<?php
require('../admin_header.php');

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$loginType 	=	$_SESSION['travelType'];
if($_SESSION['travelType']=='Admin')
{
	$check	=	" ".TABLE_ACCOUNTS.".finYear='$finYear'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	" ".TABLE_ACCOUNTS.".proId='$proId' and ".TABLE_ACCOUNTS.".finYear='$finYear'";
}
?>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Agent Report</h3>
                </div>
                <div class="page_controls">
                <form method="post">
                	<?php if($loginType	=='Admin'){?>
					<div class="page_search">
						<select name="company" required="" >
							<option value="">Select Company</option>
							<?php $cmpQry	=	mysql_query("SELECT * FROM ".TABLE_COMPANY);
							while($cmpRow		=	mysql_fetch_array($cmpQry))
							{
							
							?>
								<option value="<?php echo $cmpRow['ID'];?>"  <?php if(@$_REQUEST['company']==$cmpRow['ID']){ echo "selected";}?>><?php echo $cmpRow['companyName'];?></option>
							<?php 	
							}?>
						</select>
					</div>
					<?php }?>
                    <div class="page_search">
                        
                            <input type="text" name="searchName" id="" placeholder="Agent Name" value="<?php echo @$_REQUEST['searchName']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        
                    </div>
                    </form>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php	
$cond="1";
if(@$_REQUEST['searchName'])
{
	$cond=$cond." and ".TABLE_ACCOUNTS.".accountName like'%".$_POST['searchName']."%'";
}
if($loginType=='Admin')
{
	if(@$_POST['company'])
	{
		$proId	=	@$_POST['company'];
	}
	else
	{
		$proId	=	0;
	}
	$check 	= 	" ".TABLE_ACCOUNTS.".proId='$proId'";
}
?>     
    
<?php if(isset($_POST['submit'])) {
	
?>   
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table" id="account_view_table">
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Staff Name</th>
                        <th>Agent ID</th>
                        <th>Agent Name</th>
                        <th>Opening Balance</th>
                        <th>Credit/Debit</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    $selectAll = "select  " . TABLE_ACCOUNTS . ".ID,
                        					  " . TABLE_ACCOUNTS . ".accountName,
                        					  " . TABLE_ACCOUNTS . ".openingBal,
                        					  " . TABLE_ACCOUNTS . ".payType,
                        					  " . TABLE_ACCOUNTS . ".masterLedger,
                        					  " . TABLE_ACCOUNTS . ".addDate,
                        					  " . TABLE_ACCOUNTS . ".description,
                        					  " . TABLE_ACCOUNTS . ".contactPerson,
                        					  " . TABLE_ACCOUNTS . ".address,
                        					  " . TABLE_ACCOUNTS . ".pin,
                        					  " . TABLE_ACCOUNTS . ".city,
                        					  " . TABLE_ACCOUNTS . ".phone,
                        					  " . TABLE_ACCOUNTS . ".mobile,
                        					  " . TABLE_ACCOUNTS . ".email,
                        					  " . TABLE_ACCOUNTS . ".address2,
                        					  " . TABLE_ACCOUNTS . ".pin2,
                        					  " . TABLE_ACCOUNTS . ".city2,
                        					  " . TABLE_STAFF . ".staffName
                        					  
				                        from " . TABLE_ACCOUNTS . "," . TABLE_STAFF . "," . TABLE_LOGIN. "
				                        where  " . TABLE_ACCOUNTS . ".accountGroupId=48
				                        AND " . TABLE_ACCOUNTS . ".loginId = " . TABLE_LOGIN. ".ID
				                        AND " . TABLE_LOGIN. ".userId = " . TABLE_STAFF . ".ID
				                        and $cond	
				                        and $check			                 
				                        order by ID desc";
				                        //echo $proId;
                    $result = $db->query($selectAll);
                    $number	=	mysql_num_rows($result);
                    if($number==0  or $proId==0)
						{
							?>
							<tr>
								<td colspan="7" align="center">
									No data found
								</td>
							</tr>
							<?php
						}
						else
						{
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
                            <td><?php echo ++$i; ?></td>
                            <td><?php echo $row['staffName']; ?></td>
                            <td><?php echo $row['ID']; ?></td>
                            <td><?php echo $row['accountName']; ?></td>
                            <td><?php echo $row['openingBal']; ?></td>
                            <td><?php echo $row['payType']; ?></td>
                            <td>
                                <a class="show_table_lnk show_table_lnk_view" data-view_node="get_com_serv" data-com_serv_type="acs" data-view_id="<?= $row['ID']; ?>" href="#" data-modal_target="#acs_view_pop">View</a>   
                            </td>
                        </tr>
                        <?php
                    }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Popups -->
    <!--Popup for account View-->
    <div id="acs_view_pop" class="modal fade bd_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agent Details</h4></div>
                <div class="modal-body">
                    <div class="acs_view_table_wrap table-responsive">
                        <table class="table table-bordered table-striped acs_view_table">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> 
<?php } ?>     
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>