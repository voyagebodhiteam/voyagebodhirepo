<?php
require('../admin_header.php');

$loginType 	=	$_SESSION['travelType'];
if($_SESSION['travelType']=='Admin')
{
	$check	= "".TABLE_FLIGHT.".finYear='$finYear'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"  ".TABLE_FLIGHT.".proId='$proId' and ".TABLE_FLIGHT.".finYear='$finYear'";
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Flights</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                        	<?php if($loginType	=='Admin'){?>
							<div class="page_search">
								<select name="company" required="" >
									<option value="">Select Company</option>
									<?php $cmpQry	=	mysql_query("SELECT * FROM ".TABLE_COMPANY);
									while($cmpRow		=	mysql_fetch_array($cmpQry))
									{
									
									?>
										<option value="<?php echo $cmpRow['ID'];?>"  <?php if(@$_REQUEST['company']==$cmpRow['ID']){ echo "selected";}?>><?php echo $cmpRow['companyName'];?></option>
									<?php 	
									}?>
								</select>
							</div>
							<?php }?>
                           <input type="text" name="flightName" id="" placeholder="flightName" value="<?php echo @$_REQUEST['flightName']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                    
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php	
$cond="1";
if(@$_REQUEST['flightName'])
{
	$cond=$cond." and ".TABLE_FLIGHT.".flightName like'%".$_POST['flightName']."%'";
}
if($loginType=='Admin')
{
	if(@$_POST['company'])
	{
		$proId	=	@$_POST['company'];
	}
	else
	{
		$proId	=	0;
	}
	$check 	= 	" ".TABLE_FLIGHT.".proId='$proId'";
}
?>   
<?php if(isset($_POST['submit'])) {
	
?>    
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Staff Name</th> 
                            <th>Flight Name</th>
                            <th>Image</th>                            
                           
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    //$selectAll = "select  * from ".TABLE_FLIGHT." where $cond
                    //AND $check order by ID desc";
                    $selectAll = "SELECT ".TABLE_FLIGHT.".ID,
                    					 ".TABLE_FLIGHT.".flightName,
                    					 ".TABLE_FLIGHT.".image,
                    					 ".TABLE_STAFF.".staffName
                    					 FROM ".TABLE_FLIGHT.",".TABLE_STAFF.",".TABLE_LOGIN."
                    					 WHERE $cond
                    					 AND $check
                    					 AND ".TABLE_FLIGHT.".loginId = ".TABLE_LOGIN.".ID
                    					 AND ".TABLE_LOGIN.".userId = ".TABLE_STAFF.".ID
                    					     order by ID desc";
			                        $result = $db->query($selectAll);
			                        if(mysql_num_rows($result)>0 and $proId!=0){	
			                        while ($row = mysql_fetch_array($result)) {
			                            ?>
			                         <tr>                            
			                            <td><?php echo ++$i; ?></td>
			                            <td><?php echo $row['staffName']; ?></td>
			                            <!--<td><?php echo $row['flightNo']; ?></td>-->
			                            <td><?php echo $row['flightName']; ?></td>
			                            <td><img src="<?php if($row['image']){ echo $row['image'];}else { echo "../../uploads/flight/flight.jpg";} ?>" width="50" height="50"></td>   
			                             
			                        </tr>
			                           <?php
                   		 }
                   	}
                   	else{
						?>
						<tr>
							<td colspan="7" align="center">No data found</td>
						</tr>
						<?php
					}
                    ?>
				                   
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
<!-- Popups -->
    <!--Popup for account View-->
    <div id="acs_view_pop" class="modal fade bd_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Flight Details</h4></div>
                <div class="modal-body">
                    <div class="acs_view_table_wrap table-responsive">
                        <table class="table table-bordered table-striped acs_view_table">
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>    
<?php } ?>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>