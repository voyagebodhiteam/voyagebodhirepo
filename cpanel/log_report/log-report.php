<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}
$finYear = $_SESSION['finYear'];
if($_SESSION['travelType']=='Admin')
{
	$check	=	"a.finYear='$finYear'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"a.proId='$proId' and a.finYear='$finYear'";
}
$loginType 	=	$_SESSION['travelType'];

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>

//for incurrect invoiceNo search
function count()
{
var invoiceNo = document.getElementById('invoiceNo').value;
//alert(invoiceNo);
if((invoiceNo.length < 3) && (invoiceNo!='') ){
	document.getElementById('check').innerHTML= "Enter Current Invoice No";
	return false;
		}
}

// warning if the invoice is going to delete
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>ACTIVITY REPORT</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                       <form method="post" onsubmit="return count()"><!-- count()-> for alert message -->
                        	<?php if($loginType	=='Admin'){?>
							<div class="page_search">
								<select name="company" required="" >
									<option value="">Select Company</option>
									<?php $cmpQry	=	mysql_query("SELECT * FROM ".TABLE_COMPANY." where finYear=$finYear");
									while($cmpRow		=	mysql_fetch_array($cmpQry))
									{
									
									?>
										<option value="<?php echo $cmpRow['ID'];?>"  <?php if(@$_REQUEST['company']==$cmpRow['ID']){ echo "selected";}?>><?php echo $cmpRow['companyName'];?></option>
									<?php 	
									}?>
								</select>
							</div>
							<?php }?>
                        	<div class="page_search" >
                        		<select name="module" required="">
                        			<option value=" ">Select</option>
                        			<option <?php if(@$_REQUEST['module']=='Invoice Airline'){ echo "selected";}?>>
                        			Invoice Airline</option>
                        			<option <?php if(@$_REQUEST['module']=='Invoice Hotel'){ echo "selected" ;}?>>
                        			Invoice Hotel</option>
                        			<option <?php if(@$_REQUEST['module']=='Invoice Visa'){ echo "selected" ;}?>>
                        			Invoice Visa</option>
                        			<option <?php if(@$_REQUEST['module']=='Invoice Transportation'){ echo "selected" ;}?>>
                        			Invoice Transportation</option>
                        			<option <?php if(@$_REQUEST['module']=='Invoice Miscellaneous'){ echo "selected" ;}?>>
                        			Invoice Miscellaneous</option>
                        		</select>
                        	</div>
                            <div class="page_search">
								<input  type="search" name="from" placeholder="From" class="user_date" value="<?php echo @$_REQUEST['from'];?>" required="" >
							</div>
							<div class="page_search" >                      
								<input type="search" name="to" placeholder="To" class="user_date" value="<?php echo @$_REQUEST['to'];?>" required="">                        
							</div>
							
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                            <div class="valid" id="check"></div><!-- id for message  -->
                        </form>
                    </div>
                    
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    <?php	
$cond="1";

$fromdate	= 	date('01-m-Y');
$todate		=	date('d-m-Y');						

if(@$_REQUEST['from'])
{
	$fromdate	=	$_REQUEST['from'];
}
if(@$_REQUEST['to'])
{
	$todate		=	$_REQUEST['to'];
}
$cond = $cond." and a.changeDate BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."'";

if(@$_REQUEST['module'])
{
	$cond = $cond." and a.module LIKE '".$_REQUEST['module']."'";
}
if($loginType=='Admin')
{
	if(@$_REQUEST['company'])
	{
		$proId	=	@$_REQUEST['company'];
	}
	else
	{
		$proId	=	0;
	}
	$check 	= 	"a.proId='$proId' and a.finYear=$finYear";
}

?>
<?php if(isset($_POST['submit'])) {
	
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Module</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th>Invoice No</th>
                            <th>Old Amount</th>
                            <th>New Amount</th>
                            <th>Description</th>
                            <th>Staff</th>
                            <th>Company</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                     
                        $selectAll = "select a.ID,
                        					 a.module,
                        					 a.tableId,
                        					 a.invoiceNo,
                        					 a.changeDate,
                        					 a.status,
                        					 a.oldAmt,
                        					 a.newAmt,
                        					 a.description,
                        					 b.companyName,
                        					 c.staffName
			                          from ".TABLE_LOG." a  
			                     LEFT JOIN ".TABLE_COMPANY." b on a.proId=b.ID
			                     LEFT JOIN ".TABLE_LOGIN." d on a.loginId=d.ID
			                     LEFT JOIN ".TABLE_STAFF." c on d.userId=c.ID			                     
			                          where $cond
			                          	and $check
			                          order by a.ID desc";//echo $selectAll;
                        $result = $db->query($selectAll);
                        $selectCount = mysql_num_rows($result);		//echo $selectCount;		                        
                        if($selectCount==0)
						{
						?>
							 <tr>
								<td align="center" colspan="11">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
	                        /*********************** for pagination ******************************/
							$rowsPerPage = ROWS_PER_PAGE;
							if(isset($_GET['page']))
							{
								$pageNum = $_GET['page'];
							}
							else
							{
								$pageNum =1;
							}
							$offset = ($pageNum - 1) * $rowsPerPage;
							$select1=$db->query($selectAll." limit $offset, $rowsPerPage");
							$i=$offset+1;
							//use '$select1' for fetching
							/*************************** for pagination **************************/
                        
                    
                        while ($row = mysql_fetch_array($select1)) {
                            ?>
                         <tr>                            
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $row['module']; ?></td>
                            <td><?php echo $row['status']; ?></td>
                            <td><?php echo $App->dbformat_date($row['changeDate']); ?></td>
                            <td><?php echo $row['invoiceNo']; ?></td>
                            <td><?php echo $row['oldAmt']; ?></td>
                            <td><?php echo $row['newAmt']; ?></td>
                            <td><?php echo $row['description']; ?></td>
                            <td><?php echo $row['staffName']; ?></td>
                            <td><?php echo $row['companyName']; ?></td>
                            <td>
                     <a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?id=<?php echo $row['ID'];?>&op=delete">Delete</a>
                                
                            </td>
                        </tr>
                            <?php
                            }
                        }
                        ?>				                                         
                    </tbody>
                </table>
            </div>
             <!-- paging -->		
            <!--<div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div> -->       
            <!-- paging end-->
            
            <!--*****************************************************************-->
            
            	 <?php 
                  if(@$selectCount>@$rowsPerPage)
					{
					?>	
					 <br />	
					  <div class="pagerSC" align="center">
					<?php
					
					$query   =  $db->query($selectAll);
					$numrows = mysql_num_rows($query);
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
					   if ($page == $pageNum)
					   {
						  $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
					   }
					   else
					   {
						 	if(@$proId)
					   		 {
							 	$nav .= " <a href=\"$self?page=$page&company=$proId\">$page</a> ";
							 }
							 else
							 {
							 	$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							 }
					   }
					}
					?>
					 <?php
					if ($pageNum > 1)
					{
					   $page  = $pageNum - 1;
					   if(@$proId)
					   {
						   $prev  = " <a href=\"$self?page=$page&company=$proId\">Prev</a> ";
						   $first = " <a href=\"$self?page=1&company=$proId\">First Page</a> ";
					   }
					   else
					   {
						   $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
						   $first = " <a href=\"$self?page=1\">First Page</a> ";
					   }
					}
					else
					{
					   $prev  = '&nbsp;';
					   $first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
					   $page = $pageNum + 1;
					   if(@$proId)
					   {
						   	 $next = " <a href=\"$self?page=$page&company=$proId\">Next</a> ";
						     $last = " <a href=\"$self?page=$maxPage&company=$proId\">Last Page</a> ";
					   }
					   else
					   {
						   	 $next = " <a href=\"$self?page=$page\">Next</a> ";
						   	 $last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
					   }
					}
					else
					{
					   $next = '&nbsp;';
					   $last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
            
        </div>
    </div>
<?php } ?>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>