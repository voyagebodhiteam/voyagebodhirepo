<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear=$_SESSION['finYear'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['relationship']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            			
            $relationship		= $App->capitalize($_REQUEST['relationship']);                      
                
                $data['relationship']		= 	$App->convert($_REQUEST['relationship']);
                $data['proId']			=	$proId;
                $data['loginId']		=	$loginId;
                $data['finYear']		=	$finYear;
                $success = $db->query_insert(TABLE_RELATIONSHIP, $data);
                $db->close();

                if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
            }
       
        break;
        /*// EDIT SECTION
    case 'edit':
    	$editId = $_REQUEST['editId'];
    	if (!$_REQUEST['providerName']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            $providerName		= $App->capitalize($_REQUEST['providerName']);                      
			$mobile				= $App->convert($_REQUEST['mobile']); 

            $existId = $db->existValuesId(TABLE_TRANSPORT_PROVIDER, "providerName='$providerName' and mobile='$mobile' and ID!='$editId' and proId=$proId ");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
                $data['providerName']	=	$providerName;
                $data['mobile']			=	$mobile;
                $data['address']		= 	$App->convert($_REQUEST['address']);
                $data['proId']			=	$proId;
                $data['loginId']		=	$loginId;
                $success = $db->query_update(TABLE_TRANSPORT_PROVIDER, $data , "ID=$editId");
                $db->close();
            

                if ($success) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
    	
    break;*/
    
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
  
			$success1= @mysql_query("DELETE FROM `".TABLE_RELATIONSHIP."` WHERE ID='{$deleteId}' and proId=$proId and finYear=$finYear");
			
        $db->close();
        if ($success1) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
}
?>