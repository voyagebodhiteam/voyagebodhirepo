<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId	=	$_SESSION['travelId'];
$loginType	=	$_SESSION['travelType'];
$finYear	=	$_SESSION['finYear'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
         
if($loginType=='Branch')
	{
		$loginName1		=	"SELECT ".TABLE_COMPANY.".companyName 
							   FROM ".TABLE_LOGIN." ,".TABLE_COMPANY." 						  
							  WHERE ".TABLE_COMPANY.".ID=".TABLE_LOGIN.".userId
							  	AND ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	$loginNameRow['companyName'];			  
	}
if($loginType=='Staff')
	{
		$loginName1		=	"SELECT ".TABLE_STAFF.".staffName, 
									".TABLE_COMPANY.".companyName
							   FROM ".TABLE_LOGIN." ,".TABLE_STAFF.",".TABLE_COMPANY." 						  
							  WHERE ".TABLE_STAFF.".ID=".TABLE_LOGIN.".userId
							  	AND ".TABLE_COMPANY.".ID=".TABLE_STAFF.".proId
							  	AND ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	/*$loginNameRow['companyName']."-".*/$loginNameRow['staffName'];		  
	}
	//echo $loginName;die;


$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['customerId']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
         
            $success = 0;
			$newInvId		=	$db->nextVoucher(TABLE_INVOICE_VISA,'invoiceNo'," proId=$proId ");
			$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Visa Receivable'";
			 $acc_record 	= 	$db->query_first($accId);
			 $accountId		=	$acc_record['ID'];
                  	
                $customerId				= 	$App->convert($_REQUEST['customerId']);           
                $data['customerId']		=	$customerId; 
                $data['invoiceNo']		=	$newInvId;
                //for getting the customer name
                $cusName				= 	$App->convert($_REQUEST['customerName']);
                if($_REQUEST['invoiceDate'])                
                {
                $data['invoiceDate']	=	$App->dbformat_date($_REQUEST['invoiceDate']);
                }
                $data['mainOtherCharge']=	$App->convert($_REQUEST['mainOtherCharge']); 
                $data['discount']		=	$App->convert($_REQUEST['discount']);  
                $data['netAmount']		=	$App->convert($_REQUEST['netAmount']);
                $data['description']	=	$App->convert($_REQUEST['description']); 
                $data['remark']			=	$App->convert($_REQUEST['remark']);                
                $data['status']			=	'Pending';
                $data['proId']			=	$proId;
                $data['loginId']		=	$loginId;
                $data['finYear']		=	$finYear;

                $success = $db->query_insert(TABLE_INVOICE_VISA, $data);
               	//$customerKey = $success."VI#".$newInvId.$customerId;
		//$datakey['customerKey']	=	$customerKey;
	        //$db->query_update(TABLE_INVOICE_VISA,$datakey,"  ID=$success");
				
                $k=1;
                //for getting passenger name and passport no	
                $passName2 = '';	
                $passNo2 = '';			
					for($k=1;$k<=$_POST['rowCount'];$k++)
					{	
						//echo $_POST['rowCount'];die;											 
					   $passengerName	=	"pass".$k."_passengerName";
					   $dob				=	"pass".$k."_dob";
					   $gender			=	"pass".$k."_gender";
					   $passportNo		=	"pass".$k."_passportNo";
					   
					   $data2['visaId']			=	$success;
					   $data2['passengerName']	=	$App->convert(@$_REQUEST[$passengerName]);
					   
					   if($_REQUEST[$dob])
                                           {     
					   $data2['dob']			=	$App->dbformat_date(@$_REQUEST[$dob]);
                                           } 
					   $data2['gender']			=	$App->convert(@$_REQUEST[$gender]);
					   $data2['passportNo']		=	$App->convert(@$_REQUEST[$passportNo]);
					   
		               $data2['visaFor']		=	$App->convert($_REQUEST['visaFor']);
		               //for getting visaFor ID
		               $vFor					=	$App->convert($_REQUEST['visaFor']);
		               if($_REQUEST['submissionDate'])
		               {
						   $data2['submissionDate'] =	$App->dbformat_date($_REQUEST['submissionDate']);
						   $sudmissionDate = $App->convert($_REQUEST['submissionDate']);
					   }				   
					   if($_REQUEST['collectionDate'])
					   {
					   	   $data2['collectionDate']	=	$App->dbformat_date($_REQUEST['collectionDate']);
					   }
					   if($_REQUEST['dispatchDate'])
					   {
					   	   $data2['dispatchDate']	=	$App->dbformat_date($_REQUEST['dispatchDate']);
					   }
					   $data2['tokenNo']		=	$App->convert($_REQUEST['tokenNo']);
					   $data2['visaFee']		=	$App->convert($_REQUEST['visaFee']);
					   $data2['vfs']			=	$App->convert($_REQUEST['vfs']);
					   $data2['ddCharge']		=	$App->convert($_REQUEST['ddCharge']);
                
                	   $data2['serviceCharge']	=	$App->convert($_REQUEST['serviceCharge']);
					   $data2['otherCharge']	=	$App->convert($_REQUEST['otherCharge']);
					   $data2['courierCharge']	=	$App->convert($_REQUEST['courierCharge']);
					   $data2['subTotal']		=	$App->convert($_REQUEST['subTotal']);			
		               $data2['proId']			=	$proId;
                	   $data2['loginId']		=	$loginId;	                               
                	   $data2['finYear']		=	$finYear;	                               
		               $success2 = $db->query_insert(TABLE_VISA_PASSENGER, $data2);
		               
		               $passName2 = $passName2.@$_REQUEST[$passengerName].",";
		               $passNo2		=	$passNo2.@$_REQUEST[$passportNo].",";

                               //new passenger adding
		               
		               if(!$_REQUEST['pass'.$k.'_ID'])
						{
						$passdata['passengerName'] 	= 	$_REQUEST[$passengerName];
						//$passdata['prefix'] 		= 	$_REQUEST[$prefix];
						if($_REQUEST[$dob])
						{
							$passdata['dob'] 		= 	@$App->dbformat_date($_REQUEST[$dob]);
						}
                                                $passdata['customerId']		=	$customerId;
						$passdata['gender'] 		= 	$_REQUEST[$gender];
						$passdata['passportNo'] 	= 	$_REQUEST[$passportNo];
						$passdata['loginId']		=	$loginId;
						$passdata['proId']			=	$proId;
						$passdata['finYear']		=	$finYear;
				
						$psuccess	=	$db->query_insert(TABLE_PASSENGER, $passdata);
						}
		      			//new passenger end           

		        
		             }
		             //choping the passenger name and passport no if more than  one 
		             $passName3 	= chop($passName2,",");
		             $passNo3		= chop($passNo2,",");
		             
		           /*-------------------------------------------------------------------------*/
		           //To transaction table
		         															
					/* ------------------------------------------ */
					
					//for getting service provider name 
					$select = "	select ". TABLE_VISAFOR.".visafor 
								from ". TABLE_VISAFOR.",".TABLE_INVOICE_VISA." 
								where ". TABLE_VISAFOR.".ID = $vFor";
					//echo $select;die;
                    $res = $db->query($select);
               		$Row = mysql_fetch_array($res);
					$visaFor = $Row['visafor'];
					
					//explode passenger name and passport no 
					
					$nameParts = explode(',',$passName3);
					//print_r($parts);die;
					$noPass		=	explode(',',$passNo3);
					//print_r($noPass);die; 
					
					//from that array its assigning to a variable
					$passString='';
					for($j=0;$j<$_POST['rowCount'];$j++)
					{
						$passString = $passString."Pax :".$nameParts[$j]
.", PassNo :".$noPass[$j]."/";					
					}

				
					
					$invoiceDetails	=	"INV#VI$newInvId,$passString visaFor:$visaFor,By:$loginName";
					//echo $invoiceDetails;die;
					
					$tsdata['fromLedger'] 		=  	$customerId;
					$tsdata['toLedger'] 		=  	$accountId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Visa Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   $App->convert($_POST['netAmount'])+$App->convert($_REQUEST['discount']);
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Visa Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$accountId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Visa Receivable';
					$tfdata['credit']			=   $App->convert($_POST['netAmount'])+$App->convert($_REQUEST['discount']);
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Visa Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  /*----------------------------------------------------------------------------*/
		             /*----------------------------------------------------------------------------*/
		          //discount
		          			$discount		=	$App->convert($_REQUEST['discount']);         
		          if($discount>0)
		          {
		          	
		          
		          
		             $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          	
				  	$tsdata['fromLedger'] 		=  	$discountId;
					$tsdata['toLedger'] 		=  	$customerId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Visa Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['discount']));
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Visa Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$customerId;
					$tfdata['toLedger'] 		=  	$discountId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Visa Receivable';
					$tfdata['credit']			=   abs($App->convert($_REQUEST['discount']));
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Visa Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
		           $db->close();
		           if($success){
			$_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");  
		} 
		else{
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
		}      
		@$sp	=	$_REQUEST['savePurchase'];
		@$s		=	$_REQUEST['save'];
			
		if($sp){
			header("location:../purchase/add.php?type=v&pId=$success");
		}
		else if($s){
			header("location:index.php");
		}

	}
	break;
	       
    // EDIT SECTION
    case 'edit':
        $editId = $_REQUEST['editId'];
         if (!$_REQUEST['customerId']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success =$success1=0;
            
            $existInvId	=	$db->existVoucherNew(TABLE_INVOICE_VISA,'invoiceNo'," ID='$editId' and proId=$proId");
            
            //for get the current amount in invoice airline for inserting to log
			$oldAmount				=	$db->query_first("SELECT netAmount FROM ".TABLE_INVOICE_VISA." WHERE ID='$editId' and proId=$proId");
						
			@mysql_query("DELETE FROM `" . TABLE_VISA_PASSENGER . "` WHERE visaId='{$editId}' and proId=$proId and finYear=$finYear");
			@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$existInvId}' and voucherType='Visa Receivable' and proId=$proId and finYear=$finYear");	
						
			$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Visa Receivable'";
			 $acc_record 	= 	$db->query_first($accId);
			 $accountId		=	$acc_record['ID'];
                  	
                $customerId				= 	$App->convert($_REQUEST['customerId']);           
                $data['customerId']		=	$customerId; 
                $newInvId					=	$existInvId;
                $data['invoiceNo']		=	$newInvId;
                //for getting the customer name
                $cusName				= 	$App->convert($_REQUEST['customerName']);
                if($_REQUEST['invoiceDate'])                
                {
                $data['invoiceDate']	=	$App->dbformat_date($_REQUEST['invoiceDate']);
                }
                $data['mainOtherCharge']=	$App->convert($_REQUEST['mainOtherCharge']); 
                $data['discount']		=	$App->convert($_REQUEST['discount']);  
                $data['netAmount']		=	$App->convert($_REQUEST['netAmount']);
                $data['description']	=	$App->convert($_REQUEST['description']); 
                $data['remark']			=	$App->convert($_REQUEST['remark']);                
                $data['status']			=	'Pending';
                $data['proId']			=	$proId;
                $data['loginId']		=	$loginId;

                $success = $db->query_update(TABLE_INVOICE_VISA, $data,"ID=$editId");             
                $k=1;
                //for getting passenger name and passport no	
                $passName2 = '';	
                $passNo2 = '';			
					for($k=1;$k<=$_POST['rowCount'];$k++)
					{	
						//echo $_POST['rowCount'];die;											 
					   $passengerName	=	"pass".$k."_passengerName";
					   $dob				=	"pass".$k."_dob";
					   $gender			=	"pass".$k."_gender";
					   $passportNo		=	"pass".$k."_passportNo";
					   
					   $data2['visaId']			=	$editId;
					   $data2['passengerName']	=	$App->convert(@$_REQUEST[$passengerName]);
					   
                                           if($_REQUEST[$dob])
                                           {     
					   $data2['dob']			=	$App->dbformat_date(@$_REQUEST[$dob]);
                                           } 
					   $data2['gender']			=	$App->convert(@$_REQUEST[$gender]);
					   $data2['passportNo']		=	$App->convert(@$_REQUEST[$passportNo]);
					   
		               $data2['visaFor']		=	$App->convert($_REQUEST['visaFor']);
		               //for getting visaFor ID
		               $vFor					=	$App->convert($_REQUEST['visaFor']);
		               if($_REQUEST['submissionDate'])
		               {
						   $data2['submissionDate'] =	$App->dbformat_date($_REQUEST['submissionDate']);
						   $sudmissionDate = $App->convert($_REQUEST['submissionDate']);
					   }				   
					   if($_REQUEST['collectionDate'])
					   {
					   	   $data2['collectionDate']	=	$App->dbformat_date($_REQUEST['collectionDate']);
					   }
					   if($_REQUEST['dispatchDate'])
					   {
					   	   $data2['dispatchDate']	=	$App->dbformat_date($_REQUEST['dispatchDate']);
					   }
					   $data2['tokenNo']		=	$App->convert($_REQUEST['tokenNo']);
					   $data2['visaFee']		=	$App->convert($_REQUEST['visaFee']);
					   $data2['vfs']			=	$App->convert($_REQUEST['vfs']);
					   $data2['ddCharge']		=	$App->convert($_REQUEST['ddCharge']);
                
                	   $data2['serviceCharge']	=	$App->convert($_REQUEST['serviceCharge']);
					   $data2['otherCharge']	=	$App->convert($_REQUEST['otherCharge']);
					   $data2['courierCharge']	=	$App->convert($_REQUEST['courierCharge']);
					   $data2['subTotal']		=	$App->convert($_REQUEST['subTotal']);			
		               $data2['proId']			=	$proId;
                	   $data2['loginId']		=	$loginId;	                               
                	   $data2['finYear']		=	$finYear;	                               
		               $success2 = $db->query_insert(TABLE_VISA_PASSENGER, $data2);
		               
		               $passName2 = $passName2.@$_REQUEST[$passengerName].",";
		               $passNo2		=	$passNo2.@$_REQUEST[$passportNo].",";


                               //new passenger adding
		               
		               if(!$_REQUEST['pass'.$k.'_ID'])
						{
						$passdata['passengerName'] 	= 	$_REQUEST[$passengerName];
						//$passdata['prefix'] 		= 	$_REQUEST[$prefix];
						if($_REQUEST[$dob])
						{
							$passdata['dob'] 		= 	@$App->dbformat_date($_REQUEST[$dob]);
						}
						$passdata['customerId']		=	$customerId; 
						$passdata['gender'] 		= 	$_REQUEST[$gender];
						$passdata['passportNo'] 	= 	$_REQUEST[$passportNo];
						$passdata['loginId']		=	$loginId;
						$passdata['proId']			=	$proId;
						$passdata['finYear']		=	$finYear;
				
						$psuccess	=	$db->query_insert(TABLE_PASSENGER, $passdata);
						}
		      			//new passenger end 
		        
		             }
		             //choping the passenger name and passport no if more than  one 
		             $passName3 	= chop($passName2,",");
		             $passNo3		= chop($passNo2,",");
		             
		           /*-------------------------------------------------------------------------*/
		           //To transaction table
		         															
					/* ------------------------------------------ */
					
					//for getting service provider name 
					$select = "	select ". TABLE_VISAFOR.".visafor 
								from ". TABLE_VISAFOR.",".TABLE_INVOICE_VISA." 
								where ". TABLE_VISAFOR.".ID = $vFor";
					//echo $select;die;
                    $res = $db->query($select);
               		$Row = mysql_fetch_array($res);
					$visaFor = $Row['visafor'];
					
					//explode passenger name and passport no 
					
					$nameParts = explode(',',$passName3);
					//print_r($parts);die;
					$noPass		=	explode(',',$passNo3);
					//print_r($noPass);die; 
					
					//from that array its assigning to a variable
					$passString='';
					for($j=0;$j<$_POST['rowCount'];$j++)
					{
						$passString = $passString."Pax :".$nameParts[$j]
.", PassNo :".$noPass[$j]."/";					
					}

				
					
					$invoiceDetails	=	"INV#VI$newInvId,$passString visaFor:$visaFor, By:$loginName";
					//echo $invoiceDetails;die;
					
					$tsdata['fromLedger'] 		=  	$customerId;
					$tsdata['toLedger'] 		=  	$accountId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Visa Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   $App->convert($_POST['netAmount'])+$App->convert($_REQUEST['discount']);
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Visa Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$accountId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Visa Receivable';
					$tfdata['credit']			=   $App->convert($_POST['netAmount'])+$App->convert($_REQUEST['discount']);
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Visa Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  /*----------------------------------------------------------------------------*/
		             /*----------------------------------------------------------------------------*/
		          //discount
		          $discount			=	$App->convert($_REQUEST['discount']);         
		          if($discount>0)
		          {		          	
		             $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          	
				  	$tsdata['fromLedger'] 		=  	$discountId;
					$tsdata['toLedger'] 		=  	$customerId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Visa Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['discount']));
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Visa Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$customerId;
					$tfdata['toLedger'] 		=  	$discountId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Visa Receivable';
					$tfdata['credit']			=   abs($App->convert($_REQUEST['discount']));
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Visa Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  
				  /*-------------------------------- Inserting to Log --------------------------*/ 
				$description 			=	"NetAmt:".$App->convert($_POST['netAmount']).",Discount:".$App->convert($_REQUEST['discount']);
				
				$datalog['module']		=	'Invoice Visa';
				$datalog['tableId']		=	$editId;
				$datalog['invoiceNo']	=	$newInvId;
				$datalog['changeDate']	=	date("Y-m-d");
				$datalog['Status']		=	'Edit';
				$datalog['oldAmt']		=	$oldAmount['netAmount'];
				$datalog['newAmt']		=	$_REQUEST['netAmount'];			
				$datalog['description']	=	$description;
				$datalog['loginId']		=	$loginId;
				$datalog['proId']		=	$proId;
				$datalog['finYear']		=	$finYear;
				
				$db->query_insert(TABLE_LOG, $datalog);
				/*-------------------------------- Inserting to Log Ends --------------------------*/
		           $db->close();			
			if($success){
			$_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully"); 
		} 
		else{
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to update details. Please try again.");                   
		}
		@$sp	=	$_REQUEST['savePurchase'];
		@$s		=	$_REQUEST['save'];			
		if($sp){
			header("location:../purchase/edit.php?type=v&pId=$editId");
		}
		else if($s){
			header("location:index.php");
		}
	}        
	break;
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success1 = $success2=$success3=0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
		   $existInvId	=	$db->existVoucherNew(TABLE_INVOICE_VISA,'invoiceNo'," ID='$deleteId' and proId=$proId");
		   
		   //for get the current amount in invoice airline for inserting to log
			$oldAmount	=	$db->query_first("SELECT netAmount,invoiceNo FROM ".TABLE_INVOICE_VISA." WHERE ID='$deleteId' and proId=$proId");
			$description = "InvoiceAmt:".$oldAmount['netAmount'];
		   
           $success1=mysql_query("DELETE FROM `" . TABLE_INVOICE_VISA . "` WHERE ID='{$deleteId}' and proId=$proId and finYear=$finYear");
			@mysql_query("DELETE FROM `" . TABLE_VISA_PASSENGER . "` WHERE visaId='{$deleteId}' and proId=$proId and finYear=$finYear");
			@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$existInvId}' and voucherType='Visa Receivable' and proId=$proId and finYear=$finYear");	
			
			//purchase
			@$purExe	=	@mysql_query("SELECT invoiceNo,netAmount FROM `" . TABLE_PURCHASE_VISA . "` WHERE ourInvoiceNo='{$existInvId}' and proId=$proId and finYear=$finYear");
			@$purNum		=	mysql_num_rows($purExe);
			if($purNum>0)
			{
				$purRes			=	mysql_fetch_array($purExe);
				$purInvoiceNo	=	$purRes['invoiceNo'];
				$description = $description.",PurchaseVoucherNo:".$purRes['invoiceNo'].",PurchaseAmt:".$purRes['netAmount'];
				@mysql_query("DELETE FROM `" . TABLE_PURCHASE_VISA . "` WHERE ourInvoiceNo='{$existInvId}' and proId=$proId and finYear=$finYear");
				@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$purInvoiceNo}' and voucherType='Visa Payable' and proId=$proId and finYear=$finYear");
			}
			
			//payment
			$payAmt = 0;
			@$payExe	=	@mysql_query("SELECT ID,voucherNo,payingAmount FROM `" . TABLE_CASH_PAYMENT . "` WHERE invoiceNo='{$purInvoiceNo}' and voucherType='visa' and proId=$proId and finYear=$finYear");
			@$payNum		=	mysql_num_rows($payExe);
			if($payNum>0)
			{
				while($payRes	=	mysql_fetch_array($payExe))
				{
					$payId	=	$payRes['ID'];
					$payVoucherNo	=	$payRes['voucherNo'];
					$payAmt = $payAmt+$payRes['payingAmount'];
					@mysql_query("DELETE FROM `" . TABLE_CASH_PAYMENT . "` WHERE ID='{$payId}' and proId=$proId and finYear=$finYear");
					@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$payVoucherNo}' and (voucherType='Cash Payments' || voucherType='Bank Payments') and proId=$proId and finYear=$finYear");
				}
			}
			$description = $description.",PaidAmt:".$payAmt;
			//receipt
			$recAmt	=	0;
			@$resExe	=	@mysql_query("SELECT ID,voucherNo,payingAmount FROM `" . TABLE_CASH_RECEIPT . "` WHERE invoiceNo='{$existInvId}' and voucherType='visa' and proId=$proId and finYear=$finYear");
			@$resNum		=	mysql_num_rows($resExe);
			if($resNum>0)
			{
				while($ReceiptRes	=	mysql_fetch_array($resExe))
				{
					$resId			=	$ReceiptRes['ID'];
					$resVoucherNo	=	$ReceiptRes['voucherNo'];
					$recAmt = $recAmt+$ReceiptRes['payingAmount'];
					@mysql_query("DELETE FROM `" . TABLE_CASH_RECEIPT . "` WHERE ID='{$resId}' and proId=$proId and finYear=$finYear");
					@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$resVoucherNo}' and (voucherType='Cash Receipts' || voucherType='Bank Receipts') and proId=$proId and finYear=$finYear");
				}
			}
			$description = $description.",ReceivedAmt:".$recAmt;
			/*-------------------------------- Inserting to Log --------------------------*/ 
			$datalog['module']		=	'Invoice Visa';
			$datalog['tableId']		=	$deleteId;
			$datalog['invoiceNo']	=	$oldAmount['invoiceNo'];
			$datalog['changeDate']	=	date("Y-m-d");
			$datalog['Status']		=	'Delete';
			$datalog['oldAmt']		=	$oldAmount['netAmount'];
			$datalog['newAmt']		=	0;			
			$datalog['description']	=	$description;
			$datalog['loginId']		=	$loginId;
			$datalog['proId']		=	$proId;
			$datalog['finYear']		=	$finYear;
			
			$db->query_insert(TABLE_LOG, $datalog);	
			/*-------------------------------- Inserting to Log -------------------------*/
			
        $db->close();
        if ($success1) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
        
         // Printing  ========================================================================
		case 'print':
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$userId		=	$_SESSION['proId'];
				$sql2 		= 	"SELECT * FROM `".TABLE_COMPANY."` where ID=$userId";
				$vou_record1 = 	$db->query_first($sql2);
				$company 	= 	$vou_record1['companyName'];
				$caddress 	= 	$vou_record1['address'];
				$cphone		=	$vou_record1['phone'];
				$cmobile	=	$vou_record1['mobile'];	
				$cemail		=	$vou_record1['email'];
				$cpin		=	$vou_record1['pin'];
				
				// set default header data
				/*$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));*/

				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print
				
				$fid	=	$_REQUEST['id'];								
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
				$sql2 =  "SELECT a.ID,a.invoiceNo,a.invoiceDate,a.mainOtherCharge,a.discount,a.netAmount,
								b.accountName,b.address,b.mobile,b.phone,b.pin
						   FROM ".TABLE_INVOICE_VISA." as a
					  LEFT JOIN	".TABLE_ACCOUNTS." as b ON b.ID = a.customerId
					      where a.ID =$fid and a.proId=$proId and a.finYear=$finYear
						";
						//echo $sql2;
				$vou_record 	= 	$db->query_first($sql2);
				
				$visaID			=	$vou_record['ID'];	
				$invoiceNo 		= 	$vou_record['invoiceNo'];
				$invoiceDate 	= 	$vou_record['invoiceDate'];
				$total			= 	$vou_record['mainOtherCharge'];
				$netAmount 		= 	$vou_record['netAmount'];
				$discount 		= 	$vou_record['discount'];
				
				$accountName	= 	$vou_record['accountName'];
				$address	    = 	$vou_record['address'];
				$mobile 		= 	$vou_record['mobile'];
				$phone 			= 	$vou_record['phone'];
				$pin 			= 	$vou_record['pin'];
				
				$mainProcCharge	=	$total-$discount;
				
				//amount to words
				//$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
				
				//basic info
				$tbl_header1 = '<div><table style="width: 530px;" cellspacing="0" border="0" >';
				$tbl_footer1 = '</table></div>';
				$tbl1 = '';
				
				$tbl1	=	'<tr>
								<td colspan="6" align="left" ><h2>'.strtoupper($company).'</h2></td>
							 </tr>
							 <tr>
								<td colspan="6"></td>
							 </tr>
							 <tr>
							 	<td colspan="3" align="left">'.$caddress.'<br>'.$cpin.'</td>
							 	<td colspan="3" align="left">'.$cphone.'<br>'.$cmobile.'<br>'.$cemail.'</td>
							 </tr>
							 <tr>
								<td colspan="6" ></td>
							 </tr>
							 <tr>
							 	<td colspan="3" style="border-top: 0.1px solid black; border-bottom: 0.1px solid black;">To , <strong>'.$accountName.'</strong></td>
							 	
							 	<td colspan="3" style="border-top: 0.1px solid black;border-bottom: 0.1px solid black;background-color:#F2F2F2; border-left: 0.1px solid black;" align="center">Visa Invoice (VI)</td>
							 	
							 </tr>
							 
							 <tr>
							 	<td colspan="3"  style="border-bottom: 0.1px solid black;" align="left">'.$address.'<br>'.$pin.'</td>
							 	<td colspan="3" style="border-left: 0.1px solid black; border-bottom: 0.1px solid black;" align="left">
								 	<table >
									 	<tr>
									 		<td style="width:60px;" >Invoice :</td>
									 		<td style="width: 60px;" ><strong>'.$invoiceNo.'</strong></td><td style="width:60px;"><strong>'.$App->dbFormat_date($invoiceDate).'</strong></td>
									 	</tr>
									 	<tr>
									 		<td >Booked By :</td><td colspan="2"></td>
									 	</tr>
									 	<tr>
									 		<td >PNR :</td><td colspan="2">0</td>
									 	</tr>
								 	</table>
							 	</td>
							 	
							 </tr>
							';
				
				$pdf->SetFontSize(9);
				$pdf->writeHTML($tbl_header1 . $tbl1 . $tbl_footer1, true, true, false, false, '');
				
				//passenger TABLE
			
				$tbl_header = '<div><table style="width: 638px;" cellspacing="0" border="0" >';
				$tbl_footer = '</table></div>';
				$tbl = '';

				$tbl .= '<tr style="background-color:#F2F2F2">
					        <td style="width: 20px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Sr</strong></td>
					        <td style="width: 120px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Passenger</strong></td>
					        <td style="width: 90px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>visaFor</strong></td>
					        <td style="width: 100px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Passport No</strong></td>
					        <td style="width: 200px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black " colspan="3"><strong>Total</strong></td>
				    	</tr>
				    	<tr>
				    		<td colspan="7"></td>
				    	</tr>
				    	';
				$passQry	=	"SELECT c.ID,c.passengerName,c.visaFee,c.vfs,c.passportNo,
										c.ddCharge,c.serviceCharge,c.otherCharge,
										c.courierCharge,c.subTotal, d.visafor
								   FROM ".TABLE_VISA_PASSENGER." as c
							  LEFT JOIN ".TABLE_VISAFOR." d ON d.ID = c.visaFor
								  WHERE	c.visaId=$visaID
								    and c.proId=$proId
								";
				$pass		=	mysql_query($passQry);
				$passNum	=	mysql_num_rows($pass);
				$defRow		=	8;	//for showing constant height for the table
				$i=0;
				$sumTotal2=0;

				if($passNum>0)
				{
					while($passRow=mysql_fetch_array($pass))
					{
                                                $sumTotal=$passRow['subTotal'];
						$sumTotal2=$sumTotal2+$sumTotal;
 
						$basic =	$passRow['vfs']+$passRow['ddCharge']+$passRow['serviceCharge']+$passRow['otherCharge']+$passRow['courierCharge'];
						$tbl .= '<tr>
						        <td>'.++$i.'</td>
						        <td>'.$passRow['passengerName'].'</td>
						        <td>'.$passRow['visafor'].'</td>
						        <td>'.$passRow['passportNo'].'</td>
								<td colspan="3">'.sprintf("%0.2f",round($passRow['subTotal'],2)).'</td>
						    	</tr>';
					}
				}
                                //Total charge with procesing charge
				
				$totalCharge=$mainProcCharge+$sumTotal2;

				if($passNum<$defRow)
				{
					$staticRow	=	$defRow-$passNum;
					for($i=0;$i<$staticRow;$i++)
					{
						$tbl.='<tr>
						       		<td colspan="7"></td>
						       </tr>';
					}
				}
                                $tbl.= '<tr >
							<td colspan="7" ></td>
						</tr>
						
				    		<tr>
				    			<td colspan = "3"  style=" width: 175px; border-top: 0.1px solid black;" ></td>
				    		<td colspan="3" style=" width: 250px; border-top: 0.1px solid black;">Total Charge</td>
				    		<td colspan="1" style=" width:100px; border-top: 0.1px solid black;" align="right">'.sprintf("%0.2f",round($totalCharge,2)).'</td>
				    		
				    		</tr>';

				$tbl.= '
						<tr>
				    		<td colspan = "3" style="border-top: 0.1px solid black; border-right: 0.1px solid black"></td>
				    		<td colspan="3" style="border-top: 0.1px solid black;" align="left"></td>
				    		<td  align="right" style="border-top: 0.1px solid black; "></td>
				    	</tr>
				    	
				    	<tr>
				    		<td colspan = "3" style="border-right: 0.1px solid black;"></td>
				    		<td colspan="3" align="left">Net Invoice Amount</td>
				    		<td align="right">'.sprintf("%0.2f",round($netAmount,2)).'</td>
				    	</tr>
				    	<tr><td colspan="7" style=" font:courier; border-top: 0.1px solid black">'.ucfirst($App->amountToWord(round($netAmount,2))).' </td></tr>
				    	';
		
				$pdf->SetFontSize(9);
				$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, true, false, false, '');
				
				//terms and conditions
				$tbl_header2 = '<div><table style="width: 530px;" cellspacing="0" border="0" >';
				$tbl_footer2 = '</table></div>';
				$tbl2 = '';
				
				$tbl2	=	'<tr>
								<td  colspan="3" style="border-top: 0.1px solid black" >Terms :</td>
								<td colspan="2" style="border-top: 0.1px solid black" align="right">E.  & O.  E.</td>
								<td style="border-right: 0.1px solid black;border-top: 0.1px solid black"></td>
								<td colspan="1" style="border-top: 0.1px solid black"></td>
								<td colspan="3" style="border-top: 0.1px solid black" align="right" >for '.strtoupper($company).'</td>
							</tr>
							<tr>
								<td colspan="6" style="border-right: 0.1px solid black">
								# Subject to KOZHIKODE jurisdiction.<br>
								# Without original invoice no refund is permissible.<br>
								# Interest @ 24% will be charged on delayed payment<br>
								# Cheque to be drawn in our company name on presentation of invoice.<br>
								# Kindly check all details carefully to avoid un-necessary complications.<br>
									
								</td>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="4">Ticket Received in Order.</td>
								<td colspan="2" style="border-right: 0.1px solid black" align="right">Receivers Signature</td>
								
								<td colspan="4" align="right">Authorized Signatory</td>
							</tr>
							<tr>
								<td colspan="6" style="border-right: 0.1px solid black; border-bottom: 0.1px solid black">
								</td>
								<td colspan="4" style=" border-bottom: 0.1px solid black"></td>
							</tr>
							
							<tr>
								<td colspan="10" align="center">This is a Computer generated document and does not require any signature.
								</td>
							</tr>
							';
				$pdf->SetFontSize(7);
				$pdf->writeHTML($tbl_header2 . $tbl2 . $tbl_footer2, true, true, false, false, '');
				
				
				// ---------------------------------------------------------
				
				// Close and output PDF document
				$pdf->Output("'Invoice Visa'.$invoiceNo.pdf", 'I');		
		break;
			//Upload doc
		case 'upload':				
			    	
		if(!$_REQUEST['invoiceDocId'])
			{				
				$_SESSION['msg'] = $App->sessionMsgCreate('error', "Error, Invalid Details!"); 
				header("location:index.php");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=$document=0;
					
					$invoiceDocId	=	$App->convert($_REQUEST['invoiceDocId']);	
								
				
				// upload file
				$res=mysql_query("select document from ".TABLE_INVOICE_VISA." where ID='{$invoiceDocId}'");
				$row=mysql_fetch_array($res);
				$document=$row['document'];					
								
				$date		= 	date("YmdHis");
				$path_info  = 	pathinfo($_FILES["invDoc"]["name"]);
				$ext	 	=	$path_info["extension"];	
				//$allowed 	=  	array('gif','png','jpg');		// allowed image extensions					
				$newName 	= 	$date.".".$ext;
/*
				if(in_array($ext,$allowed) ) 	
				{*/
					$img='';	
					if(move_uploaded_file($_FILES["invDoc"]["tmp_name"],"document/".basename($newName)))
					{
						$doc="document/" . $newName;		
					}
					$data['document']			=	$doc;									
					$success1=$db->query_update(TABLE_INVOICE_VISA,$data," ID='{$invoiceDocId}'");					if($document)					
					{										
						if (file_exists($document)) 	
						{
						unlink($document);					
						} 						
					}					
					$db->close();								
				// }
				
				if($success1)
				{	
					$_SESSION['msg'] = $App->sessionMsgCreate('success', "Document uploaded Successfully"); 						
				}
				else
				{
					/*if(!in_array($ext,$allowed) ) 
					{
						$_SESSION['msg']="You have uploaded an invalid image file";
					}
					else
					{*/	
						$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed"); 
					// }
					
				}
				header("location:index.php");
			}
																					
			break;
}

?>