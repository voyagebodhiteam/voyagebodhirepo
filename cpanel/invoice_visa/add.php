<?php
require("../admin_header.php");
if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';
?>
<script>

function valid()
{	
	flag=false;
	total=document.getElementById('subTotal').value;
	discount=document.getElementById('discount').value;
	netAmount=document.getElementById('netAmount').value;
	visaFee=document.getElementById('visaFee').value;
	totalCount=document.getElementById('visa_passenger_row_count').value;
	
	if(isNaN(totalCount) || totalCount=="" || totalCount=="0")
	{																			
	document.getElementById('rowCountDiv').innerHTML="Please enter passenger details.";
	flag=true;
	}
	if(isNaN(visaFee) || visaFee=="" || visaFee=="0")
	{																			
	document.getElementById('visaFeeDiv').innerHTML="Enter valid amount.";
	flag=true;
	}	
	if(isNaN(total) || total=="")
	{																			
	document.getElementById('totalDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(discount) || discount=="")
	{																			
	document.getElementById('discountDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(netAmount) || netAmount=="" || netAmount==0 || netAmount <0)
	{																			
	document.getElementById('netAmountDiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
	// for getting alert if the submission date is not correct
	
	var suDate = document.getElementById('sDate').value;
	
	if(suDate == 1)
			{
				document.getElementById('subDiv').innerHTML = "Invalid Submission Date";
				flag	=	true;
			}
	
	//for getting alert if the collection date is not correct
	
	var colDate = document.getElementById('cDate').value;
	
	if(colDate == 1)
			{
				document.getElementById('colDiv').innerHTML = "Invalid Collection Date";
				flag	=	true;
			}
	
	//for getting alert if the dispatch date is not correct
	
	var disDate = document.getElementById('dDate').value;
	
	if(disDate == 1)
			{
				document.getElementById('disDiv').innerHTML = "Invalid Dispatch Date";
				flag	=	true;
			}
			
		//for customer name and customer id validation	
		var customerName	=	document.getElementById('customerName').value;
		//alert(customerName);
		var customerId		=	document.getElementById('customerId').value;
		var getResponse=0;
		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
           // document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
          	getResponse= xmlhttp.responseText;
          	//alert(getResponse);
          	
          	 
          	
          }	
          
	    };
	   
	   	xmlhttp.open("GET", "../../services/customer_check_ajax.php?name="+customerName+"&id="+customerId, false);
	    xmlhttp.send();
		if(getResponse==0)
      	{
      		flag=true;
                alert("Invalid Customer Name and ID.!");
		}
		
	if(flag==true)
	{
	return false;
	}
	
	
}

//clear the validation msg

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

</script>

<script>
	function total()
	{
	var totalCount=document.getElementById('visa_passenger_row_count').value;

		
	var visaFee=document.getElementById('visaFee').value;
	if(visaFee==''){visaFee=0;}
	
	var vfs=document.getElementById('vfs').value;
	if(vfs==''){vfs=0;}
	
	var ddCharge=document.getElementById('ddCharge').value;
	if(ddCharge==''){ddCharge=0;}
	
	var serviceCharge=document.getElementById('serviceCharge').value;
	if(serviceCharge==''){serviceCharge=0;}
	
	var otherCharge=document.getElementById('otherCharge').value;
	if(otherCharge==''){otherCharge=0;}	
	
	var courierCharge=document.getElementById('courierCharge').value;
	if(courierCharge==''){courierCharge=0;}	
	
	var subTotal=document.getElementById('subTotal').value;
	if(subTotal==''){subTotal=0;}
	
	var mainOtherCharge=document.getElementById('mainOtherCharge').value;
	if(mainOtherCharge==''){mainOtherCharge=0;}	
	
	var discount=document.getElementById('discount').value;
	if(discount==''){discount=0;}
	//alert(vfs);
	//alert(visaFee+vfs+ddCharge+serviceCharge+otherCharge+courierCharge+subTotal+mainOtherCharge);
	var subTot=parseInt(visaFee)+parseInt(vfs)+parseInt(ddCharge)+parseInt(serviceCharge)+parseInt(otherCharge)+parseInt(courierCharge);	
	var tot=(totalCount*subTot)+parseInt(mainOtherCharge)-parseInt(discount);
	
	document.getElementById('subTotal').value=subTot;	
	document.getElementById('netAmount').value=tot;
	}
	
	//collection date validation
	
	function colvalid()
	{
		flag=false;
		var colDate = document.getElementById('colDate').value;
		var subDate = document.getElementById('subDate').value;

		var today = new Date();
        var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 

		today = dd+'-'+mm+'-'+yyyy;
			
			if(colDate !='')
				{
					var colSplit= colDate.split('-');
                                        var subSplit= subDate.split('-');
					var todaySplit= today.split('-');
					
					var colLast 	= 	new Date(colSplit[2], +colSplit[1]-1, colSplit[0]);
                                        var subLast 	= 	new Date(subSplit[2], +subSplit[1]-1, subSplit[0]);

					var todayLast 		= 	new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
					var dateDiff		=	(todayLast.getTime() - colLast.getTime()) / (1000*60*60*24);
                                        var colsubDateDiff	=	(subLast.getTime() - colLast.getTime()) / (1000*60*60*24); 
					//alert(dateDiff);
					
					if(dateDiff>0 || colsubDateDiff>0)
					{
						document.getElementById('cDate').value = 1;
						alert("Invalid Collection Date");
						flag=true;
					}
					else
					{
						document.getElementById('cDate').value = '';
					}
				}
				
				if(flag==true)
				{
				return false;
				}
				
	}//end of collection date validation
	
	//submission date validation
	
	function subvalid()
	{
		flag=false;
		var subDate = document.getElementById('subDate').value;
		
		var today = new Date();
        var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 

		today = dd+'-'+mm+'-'+yyyy;
			
			if(subDate !='')
				{
					var subSplit= subDate.split('-');
					var todaySplit= today.split('-');
					
					var subLast 	= 	new Date(subSplit[2], +subSplit[1]-1, subSplit[0]);
					var todayLast 		= 	new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
					var dateDiff		=	(todayLast.getTime() - subLast.getTime()) / (1000*60*60*24);
					//alert(dateDiff);
					
					if(dateDiff>0)
					{
						document.getElementById('sDate').value = 1;
						alert("Invalid Submission Date");
						flag=true;
					}
					else {
						document.getElementById('sDate').value = '';
					}
					
				}
				
				if(flag==true)
				{
				return false;
				}
				
	}//end of submission date validation
	
	
	//dispatch date validation
	
	function disvalid()
	{
		flag=false;
		var disDate = document.getElementById('disDate').value;
		
		var today = new Date();
        var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 

		today = dd+'-'+mm+'-'+yyyy;
			
			if(disDate !='')
				{
					var disSplit= disDate.split('-');
					var todaySplit= today.split('-');
					
					var disLast 	= 	new Date(disSplit[2], +disSplit[1]-1, disSplit[0]);
					var todayLast 		= 	new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
					var dateDiff		=	(todayLast.getTime() - disLast.getTime()) / (1000*60*60*24);
					//alert(dateDiff);
					
					if(dateDiff>0)
					{
						document.getElementById('dDate').value = 1;
						alert("Invalid Dispatch Date");
						flag=true;
					}
					else {
						document.getElementById('dDate').value = '';
					}
					
				}
				
				if(flag==true)
				{
				return false;
				}
				
	}//end of dispatch date validation

// live calculation while adding new passenger or deleting a passenger
$(function () {
$(document).on('click', '#trigger_visa_passenger', function () {
total();	
});	
$(document).on('click', '#add_manulal_row_visa', function () {
total();	
});	
$(document).on('click', 'button[data-action="del_visa_pass_row"]', function () {
total();	
});	
});
	
</script>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form class="default_form" id="invoice_visa" method="post" action="do.php?op=index"  onsubmit="return valid()">
                    <div class="bd_panel_head">
                        <h3>Visa - Invoice</h3>
                    </div>
                    <div class="bd_panel_body">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher No <span class="valid">*</span></label>                                    
                                    <input type="text" name="invoiceNo" value="<?php echo "VI". $db->nextVoucher(TABLE_INVOICE_VISA,'invoiceNo'," proId=$proId ");?>"  required="" readonly="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher Date </label>
                                    <input type="text" name="invoiceDate" class="user_date" value="<?php echo date('d-m-Y') ?>" required="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer Name <span class="valid">*</span></label> <!--select from account where accGpId=37--> 
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerName" autocomplete="off" required="" id="customerName">								
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>					
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer ID </label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerId"  autocomplete="off" required="" id="customerId">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
                            
                        </div>

                        <div class="multi_det">
                            <div class="multi_det_body">
                                <div class="multi_det_head">
                                    <h3>Passenger List</h3>
                                    <div class="multi_det_control">
                                    
                                    	<div id="rowCountDiv" class="valid"></div>
                                    	
                                        <ul class="page_nav">
                                            <li>
                                                <a href="#" id="get_visa_passengers" data-modal_target="#visa_passenger_list" onclick="clearbox('rowCountDiv')">
                                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                                    Select Passengers
                                                </a>
                                            </li>
                                            <li>
                                                &nbsp;&nbsp;&nbsp;
                                                <a href="#" id="add_manulal_row_visa" onclick="clearbox('rowCountDiv')">
                                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                                    Add New Row
                                                </a>
                                            </li>
                                        </ul>
                                    
                                    </div>
                                    <div class="bd_clear"></div>
                                </div>
                                <div class="multi_det_table_wrap table-responsive">
                                    <table class="table table-bordered multi_det_table" id="visa_passenger_table">
                                        <thead>
                                        <tr>
                                            <th>Sl No</th>
											<th>Passenger Name</th>
											<th>Dob</th>
											<th>gender</th>
											<th>Passport No</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                         <fieldset>
                         	<legend>Individual Details</legend>
                        <div class="row">                                               	
                        	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Visa for <span class="valid">*</span></label>                                   
                                    <select name="visaFor" class="pass_to_table" required="">
	                                    <option value="">Select</option>
	                                    <?php 
	                                    $visaQry	=	mysql_query("SELECT * FROM ".TABLE_VISAFOR." where (proId='$proId' and finYear=$finYear) || (proId='0')");
	                                    while($visaRow	=	mysql_fetch_array($visaQry))
	                                    {
	                                    ?>
										<option value="<?php echo $visaRow['ID']; ?>"><?php echo $visaRow['visafor'];?></option>
										<?php }?>
									</select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Submission Date</label>
                                    <input class="pass_to_table user_date" type="text" name="submissionDate"  id="subDate" onchange="subvalid()" onfocus="clearbox('subDiv')">
                                    <input type="hidden" id="sDate" name="sDate" value="">
                                    <div class="valid" id="subDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Collection Date</label>
                                    <input class="pass_to_table user_date" type="text" name="collectionDate"  id="colDate" onchange="colvalid()" onfocus="clearbox('colDiv')">
                                    <input type="hidden" id="cDate" name="cDate" value="">
                                    <div class="valid" id="colDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Dispatch Date</label>
                                    <input class="pass_to_table user_date" type="text" name="dispatchDate" onchange="disvalid()" id="disDate" onfocus="clearbox('disDiv')">
                                    <input type="hidden" id="dDate" name="dDate" value="">
                                    <div class="valid" id="disDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>TokenNo</label>
                                    <input class="pass_to_table" type="text" name="tokenNo" >
                                </div>
                            </div>
                        	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Visa Fee</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="visaFee" value="0" onkeyup="total()" id="visaFee" required="" onfocus="clearbox('visaFeeDiv')">
                                    <div class="valid" id="visaFeeDiv"></div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">                      	
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>VFS</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="vfs" value="0" onkeyup="total()" id="vfs">
                                    <div class="valid" id="vfsDiv"></div>
                                </div>
                            </div>
                            
                           <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>DD Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="ddCharge" value="0" onkeyup="total()" id="ddCharge">
                                    <div class="valid" id="ddChargeDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Service Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="serviceCharge" value="0" onkeyup="total()" id="serviceCharge">
                                    <div class="valid" id="serviceChargeDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="otherCharge" value="0" onkeyup="total()" id="otherCharge">
                                    <div class="valid" id="otherChargeDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Courier Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="courierCharge" value="0" onkeyup="total()" id="courierCharge">
                                    <div class="valid" id="courierChargeDiv"></div>
                                </div>
                        	</div>
                        	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Sub Total <span class="valid">*</span></label>
                                    <input class="pass_to_table"  type="text" name="subTotal" required="" id="subTotal" onkeyup="total()" id="subTotal">
                                </div>
                                <div class="valid" id="subTotalDiv"></div>
                            </div>                        
                        </div>
                       </fieldset>
                       <fieldset>
                       <legend>Total</legend>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charges</label>
                                    <input type="text" data-visa_net="visa_net_trigger" name="mainOtherCharge" id="mainOtherCharge" value="0"  onfocus="clearbox('mainOtherChargeDiv')"  onkeyup="total()">
								<div class="valid" id="totalDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Discount</label>
                                    <input type="text" data-visa_net="visa_net_trigger" name="discount" id="discount" value="0"  onfocus="clearbox('discountDiv')"  onkeyup="total()">
								<div class="valid" id="discountDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Net Amount <span class="valid">*</span></label>
                                    <input type="text"  name="netAmount" id="netAmount" required="" onfocus="clearbox('netAmountDiv')"  onkeyup="total()" readonly="">
								<div class="valid" id="netAmountDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Description</label>
                                    <textarea class="pass_to_table" name="description" ></textarea>
                                </div>
                            </div>
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Remark</label>
                                    <textarea class="pass_to_table" name="remark"></textarea>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <div class="row">
                            <div class="col-lg-12">
                            
                                <div class="airline_submit">
                                    <input type="hidden" name="rowCount" id="visa_passenger_row_count">
                                     <input type="submit" name="savePurchase" value="SAVE & PURCHASE" onclick="return check()">
									 <!--<input type="submit" name="save" value="SAVE">-->
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
    <!-- Popups -->
    <div id="visa_passenger_list" class="modal fade bd_modal passenger_pop" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Passenger Details</h4>
                </div>
                <div class="modal-body">
					<table class="table table-bordered" id="visa_passenger_check_list">
						<thead>
						<tr>
							<th>Check</th>
							<th>Passenger Name</th>
							<th>Dob</th>
							<th>gender</th>
							<th>Passport No</th>
						</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="trigger_visa_passenger">OK</button>
                </div>
            </div>
        </div>
    </div>
<?php
require('../admin_footer2.php');
?>