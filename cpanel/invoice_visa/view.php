<?php
require("../admin_header.php");
if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';
$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ".TABLE_INVOICE_VISA." where ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>
<script>
function valid()
{	
	flag=false;
	
	total=document.getElementById('total').value;
	discount=document.getElementById('discount').value;	
	netAmount=document.getElementById('netAmount').value;
		
	if(isNaN(total) || total=="")
	{																			
	document.getElementById('totalDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(discount) || discount=="")
	{																			
	document.getElementById('discountDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(netAmount) || netAmount=="" || netAmount==0)
	{																			
	document.getElementById('netAmountDiv').innerHTML="Enter valid amount";
	flag=true;
	}
	if(flag==true)
	{
	return false;
	}
	
}

//clear the validation msg

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

</script>
<script>
	function total()
	{
	var visaFee=document.getElementById('visaFee').value;
	if(visaFee==''){visaFee=0;}
	
	var vfs=document.getElementById('vfs').value;
	if(vfs==''){vfs=0;}
	
	var ddCharge=document.getElementById('ddCharge').value;
	if(ddCharge==''){ddCharge=0;}
	
	var serviceCharge=document.getElementById('serviceCharge').value;
	if(serviceCharge==''){serviceCharge=0;}
	
	var otherCharge=document.getElementById('otherCharge').value;
	if(otherCharge==''){otherCharge=0;}	
	
	var courierCharge=document.getElementById('courierCharge').value;
	if(courierCharge==''){courierCharge=0;}	
	
	var subTotal=document.getElementById('subTotal').value;
	if(subTotal==''){subTotal=0;}
	
	var mainOtherCharge=document.getElementById('mainOtherCharge').value;
	if(mainOtherCharge==''){mainOtherCharge=0;}	
	
	var discount=document.getElementById('discount').value;
	if(discount==''){discount=0;}
	//alert(vfs);
	//alert(visaFee+vfs+ddCharge+serviceCharge+otherCharge+courierCharge+subTotal+mainOtherCharge);
	var subTot=parseInt(visaFee)+parseInt(vfs)+parseInt(ddCharge)+parseInt(serviceCharge)+parseInt(otherCharge)+parseInt(courierCharge);	
	var tot=subTot+parseInt(mainOtherCharge)-parseInt(discount);
	
	document.getElementById('subTotal').value=subTot;	
	document.getElementById('netAmount').value=tot;
	}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form class="default_form" id="invoice_visa" method="post" action="do.php?op=edit"  onsubmit="return valid()">
                <input type="hidden" name="editId" value="<?php  echo $editId;?>">
                    <div class="bd_panel_head">
                        <h3>Visa - Invoice</h3>
                    </div>
                    <div class="bd_panel_body">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher No</label>                                    
                                    <input type="text" name="invoiceNo" value="<?php echo "VI". $editRow['invoiceNo'];?>"  required="" readonly="" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher Date</label>
                                    <input type="text" name="invoiceDate" class="user_date" value="<?php echo $App->dbFormat_date($editRow['invoiceDate']); ?>" required=""  disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer Name</label> <!--select from account where accGpId=37-->
								<?php
								$cId=$editRow['customerId']; 
								$editSelect2 = 	"select  accountName                          			
								from ".TABLE_ACCOUNTS." 
								where ID='$cId'";
								$editResult2 = 	$db->query($editSelect2);
								$editRow2	 = 	mysql_fetch_array($editResult2);
								?>
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerName" autocomplete="off" required="" value="<?php echo $editRow2['accountName'];?>"  disabled="">
									<input type="hidden" name="customerId">
									<ul class="aj_live_items">

									</ul>
							
								</div>
							</div>
						</div>					
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer ID</label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerId" id="customerId" autocomplete="off" required="" value="<?php echo $editRow['customerId'];?>"  disabled="">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
                            
                        </div>

                        <div class="multi_det">
                            <div class="multi_det_body">
                                <div class="multi_det_head">
                                    <h3>Passenger List</h3>
                                    <!--<div class="multi_det_control">
                                        <ul class="page_nav">
                                            <li>
                                                <a href="#" id="get_visa_passengers" data-modal_target="#visa_passenger_list">
                                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                                    Add New
                                                </a>
                                            </li>
                                        </ul>
                                    </div>-->
                                    <div class="bd_clear"></div>
                                </div>
                                <div class="multi_det_table_wrap table-responsive">
                                    <table class="table table-bordered multi_det_table" id="visa_passenger_table">
                                        <thead>
                                        <tr>
                                            <th>Sl No</th>
											<th>Passenger Name</th>
											<th>Dob</th>
											<th>gender</th>
											<th>Passport No</th>
                                            <!--<th>Actions</th>-->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php				                        
										$i=0;			                        
										$editSelect2 	= 	"select  * from ".TABLE_VISA_PASSENGER." where visaId='$editId' and proId=$proId";
										$editResult2 	= 	$db->query($editSelect2);
										$rowCountEdit	=	mysql_num_rows($editResult2);
									
										while($editRow2	= 	mysql_fetch_array($editResult2)){
											?>
										<tr>
										   <td><?php echo ++$i; ?> </td>
                                           <td><input type="text" name="pass2_passengerName" readonly="" value="<?php echo $editRow2['passengerName'];?>"  disabled=""></td>
										   <td><input type="text" name="pass2_dob" readonly="" value="<?php echo $editRow2['dob'];?>" disabled=""></td>
												
										   <td><input type="text" name="pass2_gender" readonly="" value="<?php echo $editRow2['gender'];?>" disabled=""></td>
										   <td><input type="text" name="pass2_passportNo" readonly=""  value="<?php echo $editRow2['passportNo'];?>" disabled=""></td>
										   <!--<td><a class="hotel_row_del bd_lnk_del" href="#">Delete</a></td>-->
											</tr>
												 <?php
										   $visaFor			=	$editRow2['visaFor'];				   
										   $submissionDate  =	$editRow2['submissionDate'];
										   $collectionDate	=	$editRow2['collectionDate'];
										   $dispatchDate	=	$editRow2['dispatchDate'];
										   $tokenNo			=	$editRow2['tokenNo'];
										   $visaFee			=	$editRow2['visaFee'];
										   $vfs				=	$editRow2['vfs'];
										   $ddCharge		=	$editRow2['ddCharge'];
					                
					                	   $serviceCharge	=	$editRow2['serviceCharge'];
										   $otherCharge		=	$editRow2['otherCharge'];
										   $courierCharge	=	$editRow2['courierCharge'];
										   $subTotal		=	$editRow2['subTotal'];		
													}
													?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                         <fieldset>
                         	<legend>Individual Details</legend>
                        <div class="row">                                               	
                        	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Visa for</label>
                                    <select name="visaFor" class="pass_to_table" disabled=""> 
									<?php 
                                    $visaQry	=	mysql_query("SELECT * FROM ".TABLE_VISAFOR." where proId=$proId");
                                    while($visaRow	=	mysql_fetch_array($visaQry))
                                    {
                                    ?>
										<option value="<?php echo $visaRow['ID'];?>"<?php if($visaFor==$visaRow['ID']){ echo "selected";}?> ><?php echo $visaRow['visafor'];?></option>
									<?php }?>
									</select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Submission Date</label>
                                    <input class="pass_to_table user_date" type="text" name="submissionDate" required="" readonly="" value="<?php echo $App->dbformat_date($submissionDate); ?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Collection Date</label>
                                    <input class="pass_to_table user_date" type="text" name="collectionDate" required="" readonly="" value="<?php echo $App->dbformat_date($collectionDate); ?>"  disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Dispatch Date</label>
                                    <input class="pass_to_table user_date" type="text" name="dispatchDate" required="" readonly="" value="<?php echo $App->dbformat_date($dispatchDate); ?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>TokenNo</label>
                                    <input class="pass_to_table" type="text" name="tokenNo" value="<?php echo $tokenNo; ?>" onfocus="clearbox('tokenNoDiv')" disabled="">
                                </div>
                            </div>
                        	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Visa Fee</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="visaFee" value="<?php echo $visaFee; ?>" onfocus="clearbox('visaFeeDiv')" disabled="">
                                    <div class="valid" id="visaFeeDiv"></div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">                      	
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>VFS</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="vfs" value="<?php echo $vfs; ?>" onfocus="clearbox('vfsDiv')" disabled="">
                                    <div class="valid" id="vfsDiv"></div>
                                </div>
                            </div>
                            
                           <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>DD Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="ddCharge" value="<?php echo $ddCharge; ?>" onfocus="clearbox('ddChargeDiv')" disabled="">
                                    <div class="valid" id="ddChargeDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Service Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="serviceCharge" value="<?php echo $serviceCharge; ?>" onfocus="clearbox('totalDiv')" disabled="">
                                    <div class="valid" id="serviceChargeDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="otherCharge" value="<?php echo $otherCharge; ?>" onfocus="clearbox('otherChargeDiv')" disabled="">
                                    <div class="valid" id="otherChargeDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Courier Charges</label>
                                    <input class="pass_to_table" data-visa_net="visa_net_trigger" type="text" name="courierCharge" value="<?php echo $courierCharge; ?>" onfocus="clearbox('courierChargeDiv')" disabled="">
                                    <div class="valid" id="courierChargeDiv"></div>
                                </div>
                        	</div>
                        	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Sub Total</label>
                                    <input class="pass_to_table"  type="text" name="subTotal" required="" value="<?php echo $subTotal; ?>" onfocus="clearbox('subTotalDiv')" disabled="">
                                </div>
                                <div class="valid" id="subTotalDiv"></div>
                            </div>                        
                        </div>
                       </fieldset>
                       <fieldset>
                       <legend>Total</legend>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charges</label>
                                    <input type="text" data-visa_net="visa_net_trigger" name="mainOtherCharge" id="mainOtherCharge" value="<?php echo $editRow['mainOtherCharge']; ?>" onfocus="clearbox('mainOtherChargeDiv')" disabled=""> 
								<div class="valid" id="mainOtherChargeDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Discount</label>
                                    <input type="text" data-visa_net="visa_net_trigger" name="discount" id="discount" value="<?php echo $editRow['discount']; ?>" onfocus="clearbox('discountDiv')" disabled="">
								<div class="valid" id="discountDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Net Amount</label>
                                    <input type="text"  name="netAmount" id="netAmount" required="" onfocus="clearbox('netAmountDiv')" value="<?php echo $editRow['netAmount']; ?>" disabled="">
								<div class="valid" id="netAmountDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Description</label>
                                    <textarea class="pass_to_table" name="description" disabled="" ><?php echo $editRow['description']; ?></textarea>
                                </div>
                            </div>
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Remark</label>
                                    <textarea class="pass_to_table" name="remark" disabled=""><?php echo $editRow['remark']; ?></textarea>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                        <!--<div class="row">
                            <div class="col-lg-12">
                                <div class="airline_submit">
                                    <input type="hidden" name="rowCount" id="visa_passenger_row_count" value="<?php echo $rowCountEdit?>">
                                     <input type="submit" name="savePurchase" value="SAVE & PURCHASE">
									 <input type="submit" name="save" value="SAVE">
                                </div>
                            </div>
                        </div>-->
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
    <!-- Popups -->
    <div id="visa_passenger_list" class="modal fade bd_modal passenger_pop" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Passenger Details</h4>
                </div>
                <div class="modal-body">
					<table class="table table-bordered" id="visa_passenger_check_list">
						<thead>
						<tr>
							<th>Check</th>
							<th>Passenger Name</th>
							<th>Dob</th>
							<th>gender</th>
							<th>Passport No</th>
						</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="trigger_visa_passenger">OK</button>
                </div>
            </div>
        </div>
    </div>
<?php
require('../admin_footer2.php');
?>