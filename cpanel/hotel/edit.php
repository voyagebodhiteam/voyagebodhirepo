<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ". TABLE_HOTEL." where ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>
<script>
	function valid()
	{
		flag=false;

		jPin=document.getElementById('pin').value;	
			
			if((isNaN(jPin)||jPin.length!=6) && jPin!=0)
			{		
				document.getElementById('pindiv').innerHTML="Enter valid pin number.";
				flag=true;
			}
		
		if(flag==true)
		{
		return false;
		}
	}
	//clear the validation msg
	function clearbox(Element_id)
	{
	document.getElementById(Element_id).innerHTML="";
	}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit" class="default_form" onsubmit="return valid()">
             <input type="hidden" name="editId" value="<?php echo $editId; ?>">
                <div class="bd_panel_head">
                    <h3>HOTEL REGISTRATION</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                    	<div class="col-lg-12">
                         <div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
                             <div class="form_block">
                                <label>Hotel Name <span class="valid">*</span></label>
                                <input type="text" name="hotelname" id="hotelname" required="" value="<?php echo $editRow['hotelName'];?>">
                            </div>
                            </div>
                            
                            <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Pin</label>
                                <input type="text" name="pin" id="pin" value="<?php echo $editRow['pin'];?>" onfocus="clearbox('pindiv')">
                                <div id="pindiv" class="valid" style="color:#FF6600;"></div>
                            </div>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>City</label>
                                <input type="text" name="city" id="city" value="<?php echo $editRow['city'];?>">
                            </div>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>State</label>
                                 <input type="text" name="state" id="state" value="<?php echo $editRow['state'];?>">
                            </div>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Country</label>
                                <input type="text" name="country" id="country" value="<?php echo $editRow['country'];?>">
                            </div>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Phone(off.) <span style="color:#FF9900;">*</span></label>
                                <input type="text" name="phone" id="phone"  value="<?php echo $editRow['phone'];?>" required>
                            </div>
                            </div>
                            
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-12">
                          	<div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Mobile</label>
                                <input type="text" name="mobile" id="mobile" value="<?php echo $editRow['mobile'];?>">
                            </div>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Email</label>
                                <input type="email" name="email" id="email" value="<?php echo $editRow['email'];?>">
                            </div>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Website</label>
                                <input type="text" name="website" id="websit" value="<?php echo $editRow['website'];?>">
                            </div>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Address</label>
                                <textarea  name="address" id="address"><?php echo $editRow['address'];?></textarea>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="UPDATE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
