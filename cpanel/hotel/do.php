<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear=$_SESSION['finYear'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['hotelname']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;

            $hotelname		= $App->capitalize($_REQUEST['hotelname']);           
			$phone			= $_REQUEST['phone'];
			
            $existId = $db->existValuesId(TABLE_HOTEL, " hotelName='$hotelname' and phone='$phone' and proId=$proId");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
                $data['hotelName']	=	$hotelname;
                $data['address']	=	$App->convert($_REQUEST['address']);
                $data['pin']		=	$_REQUEST['pin'];
                $data['city']		=	$App->capitalize($_REQUEST['city']);
                $data['state']		=	$App->capitalize($_REQUEST['state']);
                $data['country']	=	$App->capitalize($_REQUEST['country']);
                $data['phone']		=	$phone;
                $data['mobile']		=	$_REQUEST['mobile'];
                $data['email']		=	$_REQUEST['email'];
                $data['website']	=	$_REQUEST['website'];
                
                $data['loginId']	=	$loginId;
				$data['proId']		=	$proId;
				$data['finYear']		=	$finYear; 
				
                $success = $db->query_insert(TABLE_HOTEL, $data);
                $db->close();

                if ($success) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
        break;
    // EDIT SECTION
    case 'edit':
    	$editId = $_REQUEST['editId'];
    	if (!$_REQUEST['hotelname']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;

            $hotelname		= $App->capitalize($_REQUEST['hotelname']);           
			$phone			= $_REQUEST['phone'];
			
            $existId = $db->existValuesId(TABLE_HOTEL, " hotelName='$hotelname' and phone='$phone' and ID!='$editId' and proId=$proId");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
                $data['hotelName']	=	$hotelname;
                $data['address']	=	$App->convert($_REQUEST['address']);
                $data['pin']		=	$_REQUEST['pin'];
                $data['city']		=	$App->capitalize($_REQUEST['city']);
                $data['state']		=	$App->capitalize($_REQUEST['state']);
                $data['country']	=	$App->capitalize($_REQUEST['country']);
                $data['phone']		=	$phone;
                $data['mobile']		=	$_REQUEST['mobile'];
                $data['email']		=	$_REQUEST['email'];
                $data['website']	=	$_REQUEST['website'];
				$data['loginId']			=	$loginId;
				$data['proId']				=	$proId;
				$data['finYear']		=	$finYear;
                $success = $db->query_update(TABLE_HOTEL, $data , "ID=$editId" );
                $db->close();

                if ($success) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
    	
    break;
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
  		$success1= @mysql_query("DELETE FROM `".TABLE_HOTEL."` WHERE ID='{$deleteId}' and finYear=$finYear");
			   
        $db->close();
        if ($success1) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
}
?>