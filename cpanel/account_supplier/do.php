<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear=$_SESSION['finYear'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
switch($optype)
{
		
	case 'index':		
		if(!$_POST['accountGroupId'] || !$_POST['accountName'] || !$_POST['payType'])
			{				
				$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            	header("location:index.php");
			}
		else
			{			
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$existId=0;
				$accountGroupId	=	$App->convert($_POST['accountGroupId']);
				$accountName	=	$App->convert($_POST['accountName']);
				$payType		=	$App->convert($_POST['payType']);
				$mobile			=	$App->convert($_POST['mobile']);
				
				
				$existId1=$db->checkValueExist(TABLE_ACCOUNTS,"accountName='$accountName' and proId='$proId'");
				$existId2=$db->checkValueExist(TABLE_ACCOUNTS,"mobile='$mobile' and proId='$proId'");
				
				if($existId1 >0)
				{						
				$_SESSION['msg']=$App->sessionMsgCreate("error","Account name is already exist");			
				header("location:index.php");
				}
				else if($existId2 >0)
				{						
				$_SESSION['msg']=$App->sessionMsgCreate("error","Mobile number is already exist");			
				header("location:index.php");
				}							
				else
				{
				/* ------------------------------------------ */
					$data['agentId'] 				=	$App->convert($_POST['agentId']);
					$data['accountGroupId'] 		=	$App->convert($_POST['accountGroupId']);
					$data['accountName']			=	$App->convert($_POST['accountName']);
					$data['openingBal']				= 	$App->convert($_POST['openingBal']);
					$data['payType']				=	$App->convert($_POST['payType']);
					$data['masterLedger']		    =   "0";
					$data['addDate']				=	"NOW()";
					$data['loginId']		    	=   $loginId;
					$data['proId']					=	$proId;
					$data['finYear']					=	$finYear;
					
					$data['description']			=	$App->convert($_POST['description']);
					$data['contactPerson'] 			=	$App->convert($_POST['contactPerson']);
					$data['address']				=	$App->convert($_POST['address']);
					$data['pin']					= 	$App->convert($_POST['pin']);
					$data['city']					=	$App->convert($_POST['city']);
					$data['phone'] 					=	$App->convert($_POST['phone']);
					$data['mobile']					=	$App->convert($_POST['mobile']);
					$data['email']					= 	$App->convert($_POST['email']);
					$data['address2']				=	$App->convert($_POST['address2']);
					$data['pin2']					= 	$App->convert($_POST['pin2']);
					$data['city2']					= 	$App->convert($_POST['city2']);
		
					$accountLastInsId = $db->query_insert(TABLE_ACCOUNTS, $data);
					
					/* ------------------------------------------ */
					
					$susQuery 	=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Suspense' ";
					$susRecord  =	$db->query_first($susQuery);
					$suspenseId	=	$susRecord['ID'];
					/* ------------------------------------------ */
					$voucherQuery ="SELECT MAX(voucherNo) AS vn FROM `".TABLE_TRANSACTION."` WHERE `voucherType`='Opening Balance' and proId=$proId";
					$voucherRecord = $db->query_first($voucherQuery);
					/* ------------------------------------------ */
					
					/* ------------------------------------------ */
					if($_POST['openingBal']!=0)
					{
						$detailsVoucher = 	$voucherRecord['vn']+1;
						$transData['fromLedger'] 		=  $accountLastInsId;
						$transData['toLedger'] 			=  $accountLastInsId;
						$transData['voucherNo']			=  $voucherRecord['vn']+1;
						$transData['voucherType']		=  'Opening Balance';
						
						if($_POST['payType']=='Credit')
							{
								$transData['credit'] 	=	$App->convert($_POST['openingBal']);
								$transData['debit']		=	'0';
							}
						else if($_POST['payType']=='Debit')
							{
								$transData['debit'] 	=	$App->convert($_POST['openingBal']);
								$transData['credit']	=	'0';
							}
								
						$transData['remark']				=  'Opening Balance';
						$transData['addedDate']				=	"NOW()";
						$transData['transactionDate']		=	"NOW()";
						$transData['details']				=	"INV#".$detailsVoucher.", acc:".$_POST['accountName'].",Opening:".$_POST['openingBal'];
						$transData['loginId']				=	$loginId;
						$transData['proId']					=	$proId;
						$transData['finYear']					=	$finYear;

						$db->query_insert(TABLE_TRANSACTION, $transData);
						
						$transData2['fromLedger'] 			=  $suspenseId;
						$transData2['toLedger'] 			=  $accountLastInsId;
						$transData2['voucherNo']			=  $voucherRecord['vn']+1;
						$transData2['voucherType']			=  'Opening Balance';
						
						if($_POST['payType']=='Credit')
							{
								$transData2['debit'] = $App->convert($_POST['openingBal']);
								$transData2['credit']	=	'0';
								
							}
						else if($_POST['payType']=='Debit')
							{
								$transData2['credit'] = $App->convert($_POST['openingBal']);
								$transData2['debit']		=	'0';
							}	
						$transData2['remark']				=  'Opening Balance';
						$transData2['addedDate']			=	"NOW()";
						$transData2['transactionDate']		=	"NOW()";
						$transData2['details']				=	"INV#".$detailsVoucher.", acc:".$_POST['accountName'].",Opening:".$_POST['openingBal'];
						$transData2['loginId']				=	$loginId;
						$transData2['proId']				=	$proId;
						$transData2['finYear']				=	$finYear;
						 
						$db->query_insert(TABLE_TRANSACTION, $transData2);
					}
					//die;
				/* ------------------------------------------ */
				if($accountLastInsId)
					{					
					$_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");																			
					}
					else
					{
					$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");								
					}	
					header("location:index.php");	
				}
  header("location:index.php");									
			}
		
		break;
	
	// EDIT SECTION
	//-
	case 'edit':
		
		//$uid	=	$_SESSION['LogID'];
		$editId	=	$_REQUEST['editId'];
		
		if(!$_POST['accountGroupId'] || !$_POST['accountName'] || !$_POST['payType'])
			{				
				$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            	header("location:index.php");
			}
		else
			{
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				/* ------------------------------------------ */
					
					
					/* ---------************---------------if main ledger,cant update name and ac group */
					$select=mysql_query("select masterLedger,accountName from ".TABLE_ACCOUNTS." where ID='$editId'");
					//echo "select masterLedger,accountName from ".TABLE_ACCOUNTS." where ID='$editId'";die;
					$res=mysql_fetch_array($select);
					$mainLedger=0;
					
					if($res['masterLedger']=='1')
					{
					$mainLedger						=	$res['accountName'];				
					}
					/* ---------************--------------- */
					else
					{
					$data['accountGroupId'] 		=	$App->convert($_POST['accountGroupId']);
					$data['accountName']			=	$App->convert($_POST['accountName']);					
					}
				
					$data['agentId'] 				=	$App->convert($_POST['agentId']);
					$data['openingBal']				= 	$App->convert($_POST['openingBal']);
					$data['payType']		    	=   $App->convert($_POST['payType']);
					$data['masterLedger']		    =   "0";
					$data['addDate']				=	"NOW()";
					$data['loginId']		    	=   $loginId;
					$data['proId']					=	$proId;
					$data['finYear']					=	$finYear;
					
					$data['description']			=	$App->convert($_POST['description']);
					$data['contactPerson'] 			=	$App->convert($_POST['contactPerson']);
					$data['address']				=	$App->convert($_POST['address']);
					$data['pin']					= 	$App->convert($_POST['pin']);
					$data['city']					=	$App->convert($_POST['city']);
					$data['phone'] 					=	$App->convert($_POST['phone']);
					$data['mobile']					=	$App->convert($_POST['mobile']);
					$data['email']					= 	$App->convert($_POST['email']);
					$data['address2']				=	$App->convert($_POST['address2']);
					$data['pin2']					= 	$App->convert($_POST['pin2']);
					$data['city2']					= 	$App->convert($_POST['city2']);
		
				
				$success=$db->query_update(TABLE_ACCOUNTS, $data, "ID='{$editId}'");
				/* ------------------------------------------ */
				
				/* ------------------------------------------ */
				$voucherNo=0;
				$vou_sql 	= "SELECT * FROM `".TABLE_TRANSACTION."` WHERE `fromLedger`='$editId' AND `toLedger`='$editId' AND `voucherType`='Opening Balance' and proId=$proId";
				$vou_record = $db->query_first($vou_sql);
				$voucherNo = $vou_record['voucherNo'];
				if(!$voucherNo)
				{					
					$voucherQuery ="SELECT MAX(voucherNo) AS vn FROM `".TABLE_TRANSACTION."` WHERE `voucherType`='Opening Balance' and proId=$proId";
					$voucherRecord = $db->query_first($voucherQuery);
					$voucherNo=$voucherRecord['vn']+1;
				}
				$del_vou_sql = "DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucherNo`='$voucherNo' AND `voucherType`='Opening Balance' and proId=$proId and finYear=$finYear";
				$db->query($del_vou_sql);
				/* ------------------------------------------ */
				$susQuery   = "SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Suspense' ";
				$susRecord  = $db->query_first($susQuery);
				$suspenseId = $susRecord['ID'];
				/* ------------------------------------------ */
				/* ------------------------------------------ */
				if($_POST['openingBal']!=0)
				{
				$detailsVoucher = 	$voucherRecord['vn']+1;
				$transData['fromLedger'] 		=  $editId;
				$transData['toLedger'] 			=  $editId;
				$transData['voucherNo']			=  $voucherNo;
				$transData['voucherType']		=  'Opening Balance';
				
				if($_POST['payType']=='Credit')
					{
						$transData['credit'] = $App->convert($_POST['openingBal']); 
						$transData['debit']	=	'0';
					}
				else if($_POST['payType']=='Debit')
					{
						$transData['debit'] = $App->convert($_POST['openingBal']);
						$transData['credit']	=	'0';
					}	
				$transData['remark']				=  'Opening Balance';
				$transData['addedDate']				=	"NOW()";
				$transData['transactionDate']		=	"NOW()";
				$transData['details']				=	"INV#".$detailsVoucher.", acc:".$_POST['accountName'].",Opening:".$_POST['openingBal'];
				$transData['loginId']				=	$loginId;
				$transData['finYear']				=	$finYear;
				$transData['proId']				=	$proId;

				$db->query_insert(TABLE_TRANSACTION, $transData);
				
				$transData2['fromLedger'] 		=  $suspenseId;
				$transData2['toLedger'] 		=  $editId;;
				$transData2['voucherNo']		=  $voucherNo;
				$transData2['voucherType']		=  'Opening Balance';
				
				if($_POST['payType']=='Credit')
					{
						$transData2['debit'] = $App->convert($_POST['openingBal']);
						$transData2['credit']	=	'0';
					}
				else if($_POST['payType']=='Debit')
					{
						$transData2['credit'] = $App->convert($_POST['openingBal']);
						$transData2['debit']	=	'0';
					}	
				$transData2['remark']				=  'Opening Balance';
				$transData2['addedDate']			=	"NOW()";
				$transData2['transactionDate']		=	"NOW()";
				$transData2['details']				=	"INV#".$detailsVoucher.", acc:".$_POST['accountName'].",Opening:".$_POST['openingBal'];
				$transData2['loginId']				=	$loginId;
				$transData2['proId']				=	$proId;
				$transData2['finYear']				=	$finYear;

				$db->query_insert(TABLE_TRANSACTION, $transData2);
				}
				/* ------------------------------------------ */
				
				$db->close();
				
				
				if($success && $mainLedger=='0')
				{					
					$_SESSION['msg'] = $App->sessionMsgCreate('success', "Ledger updated successfully.");	
				}													
				else if($success && $mainLedger)
				{					
					$_SESSION['msg'] = $App->sessionMsgCreate('error', "Ledger updated successfully. But ledger name can't change.");													
				}
				else
				{
					$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed");
				}
					header("location:index.php");	
			}
		
		break;
	
	// DELETE SECTION
	//-
	case 'delete':
		
				$deleteId	=	$_REQUEST['id'];
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;				
																	
		  	try
			{
					$existId=$db->checkValueExist(TABLE_TRANSACTION,"(fromLedger='$deleteId' || toLedger='$deleteId') AND `voucherType`!='Opening Balance'");
				if($existId)
				{						
				$_SESSION['msg']=$App->sessionMsgCreate("error","You can't delete. Because this data is used some where else");			
				
				}				
				else
				{		
				$success=mysql_query("DELETE FROM `".TABLE_ACCOUNTS."` WHERE ID='{$deleteId}'  AND masterLedger='0' and finYear=$finYear");				
				$success=mysql_affected_rows();	
				mysql_query("DELETE FROM `".TABLE_TRANSACTION."` WHERE `fromLedger`='$deleteId' AND `voucherType`='Opening Balance' and proId=$proId and finYear=$finYear");		
				if($success>0)
					{
					$_SESSION['msg'] = $App->sessionMsgCreate('success', "Account deleted successfully");													
					}
					else
					{
					$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed");
					}			      
				}
			}
				catch (Exception $e) 
				{
					$_SESSION['msg'] = $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
				}		
				$db->close(); 			
								
					header("location:index.php");		

		break;

}
?>