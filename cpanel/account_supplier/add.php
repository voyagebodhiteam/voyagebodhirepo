<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"".TABLE_ACCOUNTS.".proId='$proId'";
}
unset($_SESSION['msg']);
?>
<script>
function valid()
{	
flag=false;	
	jPin=document.getElementById('pin').value;
	jPin2=document.getElementById('pin2').value;
	jEmail=document.getElementById('email').value;	
	regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;	//email	
	
		if(jPin!="" &&(jPin.length!=6 || isNaN(jPin)) )
		{																			
		document.getElementById('p').innerHTML="Enter 6 digit number.";
		flag=true;
		}
		
		if(jPin2!="" &&(jPin2.length!=6 || isNaN(jPin2)))
		{																			
		document.getElementById('p2').innerHTML="Enter 6 digit number.";
		flag=true;
		}
		
		if(jEmail!="" &&(!jEmail.match(regEmail)))
		{
		document.getElementById('e').innerHTML="Invalid Email address";
		flag=true;
		}
		
	if(flag==true)
	{
	return false;
	}																		
}
//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" onsubmit="return valid()">
                <div class="bd_panel_head">
                    <h3>SUPPLIER</h3>
                </div>
                <div class="bd_panel_body">
                	<!-- <div class="row">
                    	<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Agent Name<span class="valid">*</span></label>--> <!--select from account where accGpId=37-->
								<!--<div class="aj_rel_box aj_agent">
									<input type="text" name="agentName" autocomplete="off"  id="agentName" required="">								
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>					
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Agent ID <span class="valid">*</span></label>--> <!--select from account where accGpId=37-->
								<!--<div class="aj_rel_box aj_agent">
									<input type="text" name="agentId" autocomplete="off"  id="agentId" required="">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
					</div>-->
                    <div class="row">
                        <div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
                            <div class="form_block">
                                <label>Supplier Name <span class="valid">*</span></label>
                                <input type="text" name="accountName" required="">
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
                            <div class="form_block">
                                <label>Agent Name <span class="valid">*</span></label>
                                <select name="agentId" required="">
                                    <option value="">Select</option>
                                    <?php
                                    $select = "select ID,accountName 
                                    			from " . TABLE_ACCOUNTS . " 
			                                    where accountGroupId=48 
			                                    	 and $check
			                                    Order by accountName";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        ?>
                                        <option
                                            value="<?php echo $row['ID']; ?>"><?php echo $row['accountName']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <!--<div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Master Group*</label>
                                <select name="accountGroupId" required="">
                                    <option value="">Select</option>
                                    <?php
                                    $select = "select * from " . TABLE_ACCOUNT_GROUP . " Order by acGroup";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        ?>
                                        <option
                                            value="<?php echo $row['ID']; ?>"><?php echo $row['acGroup']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                         </div>-->
                         <input type="hidden" name="accountGroupId" id="accountGroupId" value="40" >
                         <!-- 37 means Supplier/ Sundry Creditors in account_group table-->
                         <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Opening Balance <span class="valid">*</span></label>
                                <input type="text" name="openingBal" required="" value="0">
                            </div>
                         </div>
                         <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Credit/Debit <span class="valid">*</span></label>
                                <select name="payType" required="">
                                    <option value="Credit">Credit</option>
                                    <option value="Debit">Debit</option>
                                </select>
                            </div>
						  </div>
						  <div class="col-lg-2 col-sm-2 col-md-2">		
                            <div class="form_block">
                                <label>Description</label>
                                <textarea name="description" ></textarea>
                            </div>
                          </div>
                          </div>
                          <div class="row">
                            <div class="form_block_section_head">
                                <label>Contact Details</label>
                            </div>
                          </div>
                    <div class="row">
                          <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Contact Person Name</label>
                                <input type="text" name="contactPerson" >
                            </div>
                          </div>
                        <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Address</label>
                                <textarea name="address"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Pin</label>
                                <input type="text" name="pin"  id="pin" onfocus="clearbox('p')">
							<div  id="p" style="height:20px; width:200px;color:#FF0000;"></div>
                            </div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>City</label>
                                <input type="text" name="city">
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Phone</label>
                                <input type="text" name="phone" >
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Mobile <span class="valid">*</span></label>
                                <input type="text" name="mobile" required="" >
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block_section_head" style="margin: 0px; padding: 0px; visibility: hidden;">
                                <label>A</label>
                            </div>
                            <div class="form_block">
                                <label>Email</label>
                                <input type="text" name="email" id="email" onfocus="clearbox('e')">
							<div  id="e" style="height:20px; width:200px;color:#FF0000;"></div>
                            </div>
                        </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form_block_section_head" style="margin: 0px; padding: 0px;">
                                    <label>Correspondance Address</label>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <div class="form_block">
                                            <label>Address2</label>
                                            <textarea name="address2" ></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <div class="form_block">
                                            <label>Pin2</label>
                                            <input type="text" name="pin2" id="pin2" onfocus="clearbox('p2')">
                                            <div  id="p2" style="height:20px; width:200px;color:#FF0000;"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-md-4">
                                        <div class="form_block">
                                            <label>City2</label>
                                            <input type="text" name="city2">
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
