<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}
$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear	=	$_SESSION['finYear'];
//for details
$loginType	=	$_SESSION['travelType'];
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();

//for getting the staff name in print
$id = $_SESSION['travelUserId']; 

$qry = "SELECT * FROM ".TABLE_STAFF." where ID='$id'";
									$rows = $db->query($qry);		
									$res=mysql_fetch_array($rows);
									$name=$res['staffName'];

         
if($loginType=='Branch')
	{
		$loginName1		=	"SELECT ".TABLE_COMPANY.".companyName 
							   FROM ".TABLE_LOGIN." ,".TABLE_COMPANY." 						  
							  WHERE ".TABLE_COMPANY.".ID=".TABLE_LOGIN.".userId
							  	AND ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	$loginNameRow['companyName'];			  
	}
if($loginType=='Staff')
	{
		$loginName1		=	"SELECT ".TABLE_STAFF.".staffName, 
									".TABLE_COMPANY.".companyName
							   FROM ".TABLE_LOGIN." ,".TABLE_STAFF.",".TABLE_COMPANY." 						  
							  WHERE ".TABLE_STAFF.".ID=".TABLE_LOGIN.".userId
							  	AND ".TABLE_COMPANY.".ID=".TABLE_STAFF.".proId
							  	AND ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	/*$loginNameRow['companyName']."-".*/$loginNameRow['staffName'];			  
	}
	//echo $loginName;die;
 $db->close();

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index': 
        if (!$_POST['accountFrom'] || !$_POST['accountTo'] || !$_POST['amount'] || !$_POST['transactionDate']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success1 = $success2 = 0;

           /* ------------------------------------------ */
			 $newVoucherNo		=	$db->nextVoucher(TABLE_TRANSACTION,'voucherNo',"voucherType='Advance Receipt' and proId='$proId'");						 
                $data['voucherNo']		=	$newVoucherNo;
                $data['voucherDate']	=	$App->dbFormat_date($_POST['transactionDate']);
                $voucherDate			=	$App->dbFormat_date($_POST['transactionDate']);
                $data['customerId']		=	$App->convert($_REQUEST['accountFrom']);
                $data['netAmount']		=	$App->convert($_REQUEST['amount']);
                $data['transferType']	=	$App->convert($_REQUEST['transferType']);
                $transType				=	$App->convert($_REQUEST['transferType']);
                $data['remark']			=	$App->convert($_REQUEST['remark']);
                $data['loginId']		=	$loginId;
				$data['proId']			=	$proId;
				$data['finYear']		=	$finYear; 
				
                $success 		= 	$db->query_insert(TABLE_ADVANCE_RECEIPT, $data);
                
				$customerId		= 	$App->convert($_REQUEST['accountFrom']);
				//echo $customerId;die;
				
				$invoiceDetails =  "VouNo#AR$newVoucherNo,TransDate :$voucherDate,TransferType:$transType,Received By :$loginName"; 
		
				$tsdata['fromLedger'] 		=  	$customerId;
				$tsdata['toLedger'] 		=  	$App->convert($_POST['accountTo']);
				$tsdata['voucherNo']		= 	$newVoucherNo;
				$tsdata['voucherType']		=  	'Advance Receipt';
				$tsdata['credit']			= 	$App->convert($_POST['amount']);;
				$tsdata['debit']			=   0;				
				$tsdata['addedDate']		=	"NOW()";
				$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['transactionDate']);
				$tsdata['remark']			=  	'Advance Receipt: '.$newVoucherNo;
				$tsdata['details']			=  	$invoiceDetails; 
				$tsdata['loginId']			=	$loginId;
				$tsdata['proId']			=	$proId;
				$tsdata['finYear']			=	$finYear;
						
				$success1=$db->query_insert(TABLE_TRANSACTION, $tsdata);						
				/* ------------------------------------------ */
				/* ------------------------------------------ */
				$tfdata['fromLedger'] 		=  	$App->convert($_POST['accountTo']);
				$tfdata['toLedger'] 		=  	$customerId;
				$tfdata['voucherNo']		= 	$newVoucherNo;
				$tfdata['voucherType']		=  	'Advance Receipt';
				$tfdata['credit']			=   0;					
				$tfdata['debit']			= 	$App->convert($_POST['amount']);
				$tfdata['addedDate']		=	"NOW()";
				$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['transactionDate']);
				$tfdata['remark']			=  	'Advance Receipt'.$newVoucherNo;
				$tfdata['details']			=  	$invoiceDetails;
				$tfdata['loginId']			=	$loginId;
				$tfdata['proId']			=	$proId;
				$tfdata['finYear']			=	$finYear;		
				$success2=$db->query_insert(TABLE_TRANSACTION, $tfdata);					
			}		
                $db->close();
//$success1 && $success2
                if ($success1 && $success2) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
            
        break;
    // EDIT SECTION
    case 'edit':
        $editId = $_REQUEST['editId'];
        if (!$_REQUEST['amount'])  
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:edit.php?id=$editId");
        } 
        else 
        {
           
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success1 = $success2 = 0;

          		/* ------------------------------------------ */
				$vou_sql	=	"SELECT voucherNo FROM `".TABLE_ADVANCE_RECEIPT."` WHERE `ID`='$editId'and proId='$proId'";
				$vou_record = 	$db->query_first($vou_sql);
				$existVouNo = 	$vou_record['voucherNo'];
				//echo $existVouNo;die;
				
				/*$del_cash = "DELETE FROM `".TABLE_ADVANCE_RECEIPT."` WHERE `voucherNo`='$existVouNo' AND proId='$proId' and finYear='$finYear'";*/
				//echo $del_cash;die;
				//$success = $db->query($del_cash);
				
				$del_trans = "DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucherType`='Advance Receipt' AND `voucherNo`='$existVouNo' AND proId='$proId' and finYear='$finYear'";
				//echo $del_trans;die;
                $success = $db->query($del_trans);
								 
                $data['voucherNo']		=	$existVouNo;
                $data['voucherDate']	=	$App->dbFormat_date($_POST['transactionDate']);
                $voucherDate			=	$App->dbFormat_date($_POST['transactionDate']);
                $data['customerId']		=	$App->convert($_REQUEST['accountFrom']);
                $data['netAmount']		=	$App->convert($_REQUEST['amount']);
                $data['transferType']	=	$App->convert($_REQUEST['transferType']);
                $transferType 			=   $App->convert($_REQUEST['transferType']);
                //echo $transferType;die;
                $data['remark']			=	$App->convert($_REQUEST['remark']);
                
                $data['loginId']		=	$loginId;
				$data['proId']			=	$proId;
				$data['finYear']		=	$finYear; 
		               
		     	$success3 = $db->query_update(TABLE_ADVANCE_RECEIPT, $data, "ID = $editId");
				$cashDetails3	= "VouNo#AR$existVouNo,TransDate :$voucherDate,TransferType:$transferType,Received By :$loginName"; 
						
				$customerId					= 	$App->convert($_REQUEST['accountFrom']); 	
				$tsdata['fromLedger'] 		=  	$customerId;
				$tsdata['toLedger'] 		=  	$App->convert($_POST['accountTo']);
				$tsdata['voucherNo']		= 	$existVouNo;
				$tsdata['voucherType']		=  	'Advance Receipt';
				$tsdata['credit']			= 	$App->convert($_POST['amount']);;
				$tsdata['debit']			=   0;				
				$tsdata['addedDate']		=	"NOW()";
				$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['transactionDate']);
				$tsdata['remark']			=  	'Advance Receipt: '.$existVouNo;
				$tsdata['details']			=  	$invoiceDetails; 
				$tsdata['loginId']			=	$loginId;
				$tsdata['proId']			=	$proId;
				$tsdata['finYear']			=	$finYear;

				$success1=$db->query_insert(TABLE_TRANSACTION, $tsdata);				
				/* ------------------------------------------ */
				
				/* ------------------------------------------ */
				$tfdata['fromLedger'] 		=  	$App->convert($_POST['accountTo']);
				$tfdata['toLedger'] 		=  	$customerId;
				$tfdata['voucherNo']		= 	$existVouNo;
				$tfdata['voucherType']		=  	'Advance Receipt';
				$tfdata['credit']			=   0;					
				$tfdata['debit']			= 	$App->convert($_POST['amount']);
				$tfdata['addedDate']		=	"NOW()";
				$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['transactionDate']);
				$tfdata['remark']			=  	'Advance Receipt'.$existVouNo;
				$tfdata['details']			=  	$invoiceDetails;
				$tfdata['loginId']			=	$loginId;
				$tfdata['proId']			=	$proId;
				$tfdata['finYear']			=	$finYear;
				
				$success2=$db->query_insert(TABLE_TRANSACTION, $tfdata);	
                $db->close();
//$success1 && $success2
                if ($success1 && $success2) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");                
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to update details. Please try again.");                   
                }
                 header("location:index.php");
            }
        break;
    // DELETE SECTION
case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
		
		/* ------------------------------------------ */
		
		
		$vou_sql	=	"SELECT voucherNo FROM `".TABLE_ADVANCE_RECEIPT."` WHERE `ID`='$deleteId'and proId='$proId'";
		$vou_record = 	$db->query_first($vou_sql);
		$existVouNo = 	$vou_record['voucherNo'];
		
		$del_trans = "DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucherType`='Advance Receipt' AND `voucherNo`='$existVouNo' AND proId='$proId' and finYear='$finYear'";
		$success = $db->query($del_trans);
		
	$del_cash = "DELETE FROM `".TABLE_ADVANCE_RECEIPT."` WHERE `ID`='$deleteId'and proId='$proId'";		    $success = $db->query($del_cash);
				
        $db->close();
        
        if ($del_trans) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete this.");
        }
        header("location:index.php");
        break;
        
        //set bill
        case 'bill':
						
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
			$db->connect();
			$success1=0;
			$advanceId 			=	$_REQUEST['advanceId'];
			$billType			=	"billType_".$advanceId;			
			$data['billType']	=	$App->convert($_REQUEST[$billType]);
			$billNo				=	"billNo_".$advanceId;
			$str 				= 	$App->convert($_REQUEST[$billNo]);
			$str2 				= 	substr($str, 2);			
			$data['billNo']		=	$str2;		
			
			$success1=$db->query_update(TABLE_ADVANCE_RECEIPT,$data,"ID='{$advanceId}'");
			
			$db->close();
			
				if($success1)
				{							
					 $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Added Successfully");										
				}
				
				else{
					$_SESSION['msg'] =  $App->sessionMsgCreate('error', "Failed");										
				}
			
						
			header("location:set_bill.php");											

					
		break;
        
         // Printing  ========================================================================
case 'print':
		
				$printID	=	$_REQUEST['id'];
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
					
				$userId		=	$_SESSION['proId'];
				$sql2 		= 	"SELECT * FROM `".TABLE_COMPANY."` where ID=$userId";
				$vou_record1 = 	$db->query_first($sql2);
				$company 	= 	$vou_record1['companyName'];
				$caddress 	= 	$vou_record1['address'];
				$cphone		=	$vou_record1['phone'];
				$cmobile	=	$vou_record1['mobile'];	
				$cemail		=	$vou_record1['email'];
				$cpin		=	$vou_record1['pin'];
				
				// set default header data
				/*$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));*/

				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
				$sql2 = "SELECT A.ID,A.voucherNo,A.credit,A.transactionDate,A.remark,A.details,
         					  	B.accountName as toLedger,
         					  	C.accountName as fromLedger 
         				 FROM `".TABLE_TRANSACTION."` A,`".TABLE_ACCOUNTS."` B,
         				 	  `".TABLE_ACCOUNTS."` C
         			     WHERE A.ID=$printID AND A.fromLedger=C.ID AND 
         			     	   A.toLedger=B.ID 
         			     	   ";
				$vou_record 	= 	$db->query_first($sql2);
				
				$details 		= 	$vou_record['details'];
				$invoiceNo 		= 	$vou_record['voucherNo'];
				$invoiceDate 	= 	$vou_record['transactionDate'];
				$credit			= 	$vou_record['credit'];
				$toLedger		= 	$vou_record['toLedger'];
				//echo $toLedger;die;
				$fromLedger 	= 	$vou_record['fromLedger'];	
				$toLedger		=	$vou_record['toLedger'];			
				
				//$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
				$defRow	=	8;
				//$mainProcCharge	=	$proCharge+$otherCharges;
				
				//basic info
				$tbl_header1 = '<div><table style="width: 638px;" cellspacing="0" border="0" >';
				$tbl_footer1 = '</table></div>';
				$tbl1 = '<tr>
							<td colspan="6" align="left" ><h2>'.strtoupper($company).'</h2></td>
						 </tr>
						 <tr>
							<td colspan="6"></td>
						 </tr>
						 <tr>
						 	<td colspan="3" align="left">'.$caddress.'<br>'.$cpin.'</td>
						 	<td colspan="3" align="left">'.$cphone.'<br>'.$cmobile.'<br>'.$cemail.'</td>
						 </tr>
						 <tr>
							<td colspan="6" ></td>
						 </tr>
						 <tr>
						 	<td colspan="3" style=" width:300px; border-top: 0.1px solid black; border-bottom: 0.1px solid black;"><strong>Staff Name : </strong>'.$name.'</td>
						 	<td colspan="3" style=" width:220px; border-top: 0.1px solid black;border-bottom: 0.1px solid black;background-color:#F2F2F2; border-left: 0.1px solid black;" align="center"><strong>Cash Receipt</strong></td>
						 	
						 </tr>
						 <tr>
						 	<td colspan="3">Received with thanks</td>
						 	<td colspan="3" style="border-left: 0.1px solid black;">
							 	<table>
								 	<tr>
								 		<td style="width:60px;">Receipt No.</td><td style="width: 60px;" align="left"><strong>'.$invoiceNo.'</strong></td>
								 	</tr>
								 	
							 	</table>
						 	</td>
						 </tr>
						
						 <tr>
						 	<td colspan="3" style="border-bottom: 0.1px solid black;">To,'.$fromLedger.'</td>
						 	<td colspan="3" style="border-left: 0.1px solid black; border-bottom: 0.1px solid black;">
							 	<table>
								 	<tr>
								 		<td style="width:60px;">Date </td><td style="width:60px;" align="left"><strong>'.$App->dbformat_date($invoiceDate).'</strong></td>
								 	</tr>
								 	
							 	</table>
						 	</td>
						 </tr>
							';
							
				$pdf->SetFontSize(9);
				$pdf->writeHTML($tbl_header1 . $tbl1 . $tbl_footer1, true, true, false, false, '');
				
				//passenger TABLE
			
				$tbl_header = '<div><table style="width: 638px;" cellspacing="0" border="0" >';
				$tbl_footer = '</table></div>';
				$tbl = '';

				$tbl .= '<tr style="background-color:#F2F2F2">
					        <td colspan="2" style="width: 70px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black"><strong>Sr</strong></td>
					        <td colspan="3" style="width: 270px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black"><strong>Details.</strong></td>
					        <td colspan="3" style="width: 90px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black"><strong>Received</strong></td>
					        <td colspan="2" style="width: 90px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black"><strong>Discount</strong></td>
				    	</tr>
				    	<tr>
				    		<td colspan="10"></td>
				    	</tr>';
				    	$passQry	=	"SELECT D.ID,D.voucherType,D.invoiceNo,D.payingAmount,D.discount
										   FROM ".TABLE_CASH_RECEIPT." D
										  WHERE	D.voucherNo=$invoiceNo
										  AND paymentType='CASH'
										   AND proId='$proId'
									   
										";
										//echo $passQry;
						$pass		=	mysql_query($passQry);
						$passNum	=	mysql_num_rows($pass);
						$defRow		=	8;	//for showing constant height for the table
						$i			=	0;
						$sum		=	0;
						$dis		=   0;
				if($passNum>0)
				{
					while($passRow=mysql_fetch_array($pass))
					{
						$vType = $passRow['voucherType'];
						
						if($vType=='airline') 
						{
							$prefix = 'IA';
						}
						else if($vType=='hotel') 
						{
							$prefix = 'IH';
						}
						else if($vType=='transportation') 
						{
							$prefix = 'IT';
						}
						else if($vType=='miscellaneous') 
						{
							$prefix = 'IM';
						}
						else if($vType=='visa') {
							$prefix = 'IV';
						}
						else {
							$prefix = 'IP';
						}
						
						
						
						$sum	=	$sum+$passRow['payingAmount'];
						$dis=$dis+$passRow['discount'];
						$tbl .=	'<tr>
							        <td colspan="2">'.++$i.'</td>
							        <td colspan="3">'.$details.'</td>
							        <td colspan="3">'.sprintf("%0.2f",round($passRow['payingAmount'],2)).'</td>
							        <td colspan="2">'.sprintf("%0.2f",round($passRow['discount'],2)).'</td>
							    </tr>
			    				';
					}
				}
				$totalAmount = $sum - $dis;
				if($passNum<$defRow)
				{
					$staticRow	=	$defRow-$passNum;
					for($i=0;$i<$staticRow;$i++)
					{
						$tbl.='<tr>
						       		<td colspan="10"></td>
						       </tr>';
					}
				}
				$tbl 	.= 	'<tr><td colspan="10" style="border-bottom: 0.1px solid black;" ></td></tr>
					    	<tr>
					    		<td colspan="2">Narration :</td>
					    		<td colspan = "3">'.$vou_record['remark'].'</td>
					    		<td colspan="3" align="right">Amount</td>
					    		<td colspan="2" align="left">'.sprintf("%0.2f",round($totalAmount,2)).'</td>
					    	</tr>
					    	<tr><td colspan="10">'.ucfirst($App->amountToWord(round($totalAmount,2))).' </td></tr>
					    	<tr><td colspan="10" align="left">By Cash</td></tr>';
				
		
				$pdf->SetFontSize(9);
				$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, true, false, false, '');
				
				//terms and conditions
				$tbl_header2 = '<div><table style="width: 530px;" cellspacing="0" border="0" >';
				$tbl_footer2 = '</table></div>';
				$tbl2 	= 	'';
				
				$tbl2	.=	'<tr>
								<td  colspan="3" style="border-top: 0.1px solid black" >Terms :</td>
								<td colspan="2" style="border-top: 0.1px solid black" align="right"></td>
								<td style="border-top: 0.1px solid black"></td>
								<td colspan="1" style="border-top: 0.1px solid black"></td>
								<td colspan="3" style="border-top: 0.1px solid black" align="right" >for '.strtoupper($company).'</td>
							</tr>
							<tr>
								<td colspan="6" >
								# Subject to KOZHIKODE jurisdiction.<br>
								# Without original Receipt no refund is permissible.<br>
								# Kindly check all details carefully to avoid un-necessary complications.<br>
								
								</td>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="6">Receivers Signature</td>
								
								<td colspan="4"  align="right">Authorized Signatory</td>
							</tr>
							<tr>
								<td colspan="6" style=" border-bottom: 0.1px solid black">
								</td>
								<td colspan="4" style=" border-bottom: 0.1px solid black"></td>
							</tr>
							
							<tr>
								<td colspan="10" align="center">This is a Computer generated document and does not require any signature.
								</td>
							</tr>
							';
				$pdf->SetFontSize(7);
				$pdf->writeHTML($tbl_header2 . $tbl2 . $tbl_footer2, true, true, false, false, '');
				
				
				// ---------------------------------------------------------
				
				// Close and output PDF document
				$pdf->Output("'cash reciept'.$invoiceNo.pdf", 'I');		
		break;
}
?>