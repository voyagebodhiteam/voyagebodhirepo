<?php
require('../admin_header.php');

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" id="cash_receipt" class="default_form">
                <div class="bd_panel_head">
                    <h3>ADVANCE RECEIPT</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Date <span class="valid">*</span></label>
                                <input type="text" name="transactionDate" id="transactionDate" class="user_date" required="" value="<?php echo date("d-m-Y") ?>">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <?php
                                $vouNo		=	$db->nextVoucher(TABLE_TRANSACTION,'voucherNo',"voucherType='Advance Receipt' and proId='$proId'");
                                ?>
                                <label>Voucher No <span class="valid">*</span></label>
                                <input type="text" name="voucherNo" required="" value="<?php echo 'AR' . $vouNo; ?>"  readonly="">
                            </div>
                        </div>
                        
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account from Name <span class="valid">*</span></label>
                                <div class="aj_rel_box aj_account_from">
                                    <input name="accountFromName" autocomplete="off" required="" id="accountFromName" type="text">
                                    <ul class="aj_live_items"></ul>
                                    <p class="validation_mes">Please fill this!</p>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-2 col-md-2 col-sm-3">
                            <label>Account From ID <span class="valid">*</span></label>
                            <div class="aj_rel_box aj_account_from">
                                <input type="text" name="accountFrom" id="accountFrom" autocomplete="off" required />
                                <ul class="aj_live_items"></ul>
                                <p class="validation_mes">Please fill this!</p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                        	<div class="form_block">
                                <label>Transfer Type <span class="valid">*</span></label>
                                <select name="transferType">
                                	<option value="CASH">Cash</option>
                                	<option value="BANK">Bank</option>
                                </select>
                            </div>
                        </div>
                        <!--<div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account To*</label>
                                <select name="accountTo" required="">
                                    <option value="">Select</option>
                                    <?php
                                    $select = "select * from " . TABLE_ACCOUNTS . " where `accountGroupId`='8' OR `accountGroupId`='15'";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        ?>
                                        <option
                                            value="<?php echo $row['ID']; ?>"><?php echo $row['accountName']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>-->
                        <input type="hidden" name="accountTo" value="2">
                    </div>                    
                    <div class="row">                        
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Net Amount <span class="valid">*</span></label>
                                <input type="text" name="amount" id="amount" value="0" required="">
                            </div>	
                       </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">						 
                            <div class="form_block">
                                <label>Remark</label>
                                <textarea name="remark" id="remark"></textarea>
                            </div>						
                     </div>
                  </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
