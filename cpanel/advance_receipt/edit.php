<?php
require('../admin_header.php');

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$editId		=	$_REQUEST['id'];
$editSelect = "SELECT A.ID,
 					  A.voucherNo,
 					  A.voucherDate,
 					  A.customerId,
 					  B.accountName,
 					  A.transferType,
 					  A.netAmount,
 					  A.remark 					 
 				FROM `".TABLE_ADVANCE_RECEIPT."` A,
 					`".TABLE_ACCOUNTS."` B																		WHERE  A.customerId=B.ID  
 					and A.ID='$editId'					
 			   	ORDER BY A.voucherDate DESC";
                     	
$editResult 	= 	$db->query($editSelect);
$editRow 		= 	mysql_fetch_array($editResult);
//$voucherType	=	$editRow['voucherType'];
$voucherNo		=	$editRow['voucherNo'];
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit" id="cash_receipt" class="default_form">
             <input type="hidden" name="editId" value="<?php echo $editId; ?>">
                <div class="bd_panel_head">
                    <h3>ADVANCE RECEIPT</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Date <span class="valid">*</span></label>
                                <input type="text" name="transactionDate" id="transactionDate" class="user_date"
                                       required="" value="<?php echo $App->dbformat_date($editRow['voucherDate']);?>">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">                                
                                <label>Voucher No <span class="valid">*</span></label>
                                <input type="text" name="voucherNo" required="" value="<?php echo 'AR' .$editRow['voucherNo']?>"  readonly="">
                            </div>
                        </div>                        
                         
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account from Name <span class="valid">*</span></label>
                                <div class="aj_rel_box aj_account_from">
                                    <input name="accountFromName" autocomplete="off" required="" id="accountFromName" type="text" value="<?php echo $editRow['accountName']; ?>"  readonly="">
                                    <ul class="aj_live_items"></ul>
                                    <p class="validation_mes">Please fill this!</p>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-2 col-md-2 col-sm-3">
                            <label>Account From ID <span class="valid">*</span></label>
                            <div class="aj_rel_box aj_account_from">
                                <input type="text" name="accountFrom" id="accountFrom" autocomplete="off" required   value="<?php echo $editRow['customerId']; ?>"  readonly=""/>
                                <ul class="aj_live_items"></ul>
                                <p class="validation_mes">Please fill this!</p>
                            </div>
                        </div>
                        <input type="hidden" name="accountTo" value="2">
                        <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                        	<div class="form_block">                        		
                                <label>Transfer Type <span class="valid">*</span></label>
                                <select name="transferType">
                                	<option value="CASH" <?=$editRow['transferType'] == 'CASH' ? ' selected="selected"' : '';?>>Cash</option>
                                	<option value="BANK" <?=$editRow['transferType'] == 'BANK' ? ' selected="selected"' : '';?>>Bank</option>
                                </select>
                            </div>
                        </div>
                        
                    </div>    
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                            <div class="form_block">
                                <label>Net Amount <span class="valid">*</span></label>
                                <input type="text" name="amount" id="amount" value="<?php echo $editRow['netAmount']; ?>"  required="">
                            </div>
						</div>
						 <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                            <div class="form_block">
                                <label>Remark</label>
                                <textarea name="remark" id="remark"><?php echo $editRow['remark'];?></textarea>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
?>

<!-- Pop up for bill list -->
<div id="bill_list_pop_receipt" class="modal fade bd_modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pending Bills</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered" id="bill_list">
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Vouvher No</th>
                        <th>Date</th>
                        <th>Voucher Type</th>
                        <th>Total</th>
                        <th>Paid</th>
                        <th>Balance</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="7">No bills found unpaid.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="trigger_cash_payment">Pay Now</button>
            </div>
        </div>
    </div>
</div>
<?php
require('../admin_footer2.php');
?>
