<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
    header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<div class="row" style="margin: 15px 0px;">
        <div class="clearfix dash_row">
            <div class="dash_box">
                <a class="info-box clearfix" <?php if($accountReport==0 && $travelType=='Staff'){
                    ?> href="#" <?php } else { ?> href="../account_reports/payment_report.php" <?php }?>>
                    <span class="info-box-icon aqua"><i class="ion ion-log-out"></i></span>

                    <div class="info-box-content">
                        <!--<span class="info-box-text">Profit</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-aqua">Payment</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" <?php if($accountReport==0 && $travelType=='Staff'){
                    ?> href="#" <?php } else { ?> href="../account_reports/receipt_report.php" <?php }?>>
                    <span class="info-box-icon bg-green"><i class="ion ion-log-in"></i></span>

                    <div class="info-box-content">
                        <!--<span class="info-box-text">Profit</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-green">Receipt</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <?php
            if($travelType!='Staff') {
            ?>
                <div class="dash_box">
                    <a class="info-box clearfix" <?php if($travelType=='Branch'){
                        ?> href="../user/index.php"<?php } else { ?>  href="#"  <?php }?>>
                        <span class="info-box-icon bg-purple"><i class="ion ion-log-in"></i></span>

                        <div class="info-box-content">
                            <!--<span class="info-box-text">Profit</span>
                            <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                            <span class="info-box-number bg-purple">Staff</span>
                        </div>
                        <!-- /.info-box-content -->
                    </a>
                </div>
            <?php
            }
            ?>
        </div>
</div>

<div class="row">
    <div class="col-lg-8 col-md-6 col-sm-6">
        <!-- BAR CHART -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Sale - Purchase Chart</h3>
            </div>
            <div class="box-body">
                <div class="chart">
                    <canvas id="barChart" style="height:230px"></canvas>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<div style="margin-left: 380px;">
<?php
require('../admin_footer1.php');

$dateTime = new DateTime();
$monthNum =$dateTime->format('m') ; 
$month='';
$barDataSale = $barDataPurchase = '';
for($i=1;$i<=$monthNum;$i++) {
   $dateObj   = DateTime::createFromFormat('!m', $i);
	$monthName = $dateObj->format('F');
	$j = $dateObj->format('t');
	$month=$month.$monthName.',';
	//sale
	$fm="Y-$i-01";
	$lm="Y-$i-$j";
	$monthFirst = date($fm); // hard-coded '01' for first day
	$monthLast = date($lm);
	//echo $monthFirst.",";echo $monthLast.",";
	$aQry = "SELECT sum(netAmount) as aSum FROM `".TABLE_INVOICE_AIRLINE."` WHERE (invoiceDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'";
	$resA=mysql_query($aQry);
	$rowA=mysql_fetch_array($resA);
	
		$hQry = "SELECT sum(netAmount) as hSum FROM `".TABLE_INVOICE_HOTEL."` WHERE (invoiceDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'";
	$resH=mysql_query($hQry);
	$rowH=mysql_fetch_array($resH);
	
		$mQry = "SELECT sum(netAmount) as mSum FROM `".TABLE_INVOICE_MISC."` WHERE (invoiceDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'" ;
	$resM=mysql_query($mQry);
	$rowM=mysql_fetch_array($resM);
	
		$tQry = "SELECT sum(netAmount) as tSum FROM `".TABLE_INVOICE_TRANSPORT."` WHERE (invoiceDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'" ;
	$resT=mysql_query($tQry);
	$rowT=mysql_fetch_array($resT);
	
		$vQry = "SELECT sum(netAmount) as vSum FROM `".TABLE_INVOICE_VISA."` WHERE (invoiceDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'" ;
	$resV=mysql_query($vQry);
	$rowV=mysql_fetch_array($resV);
	
		$pQry = "SELECT sum(netAmount) as pSum FROM `".TABLE_INVOICE_PACKAGE."` WHERE (invoiceDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'" ;
	$resP=mysql_query($pQry);
	$rowP=mysql_fetch_array($resP);
	
		
		$tot= $rowA['aSum']+$rowH['hSum']+$rowM['mSum']+$rowT['tSum']+$rowV['vSum']+$rowP['pSum'];
		$tot=round($tot);
		//echo '<p style="margin-left: 380px;">'.$tot.'</p>';
		//echo $netAmt;
		$barDataSale = $barDataSale.$tot.',';
		
		//PURCHASE
		$aQry2 = "SELECT sum(netAmount) as aSum FROM `".TABLE_PURCHASE_AIRLINE."` WHERE (invoiceDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'";
	$resA2=mysql_query($aQry2);
	$rowA2=mysql_fetch_array($resA2);
	
		$hQry2 = "SELECT sum(netAmount) as hSum FROM `".TABLE_PURCHASE_HOTEL."` WHERE (invoiceDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'";
	$resH2=mysql_query($hQry2);
	$rowH2=mysql_fetch_array($resH2);
	
		$mQry2 = "SELECT sum(netAmount) as mSum FROM `".TABLE_PURCHASE_MISC."` WHERE (invoiceDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'" ;
	$resM2=mysql_query($mQry2);
	$rowM2=mysql_fetch_array($resM2);
	
		$tQry2 = "SELECT sum(netAmount) as tSum FROM `".TABLE_PURCHASE_TRANSPORT."` WHERE (invoiceDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'" ;
	$resT2=mysql_query($tQry2);
	$rowT2=mysql_fetch_array($resT2);
	
		$vQry2 = "SELECT sum(netAmount) as vSum FROM `".TABLE_PURCHASE_VISA."` WHERE (invoiceDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'" ;
	$resV2=mysql_query($vQry2);
	$rowV2=mysql_fetch_array($resV2);
	
		$pQry21 = "SELECT sum(netAmount) as pSumA FROM `".TABLE_PURCHASE_PACKAGE_AIRLINE."` WHERE (purchaseDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'" ;
	$resP21=mysql_query($pQry21);
	$rowP21=mysql_fetch_array($resP21);
	
	$pQry22 = "SELECT sum(netAmount) as pSumH FROM `".TABLE_PURCHASE_PACKAGE_HOTEL."` WHERE (purchaseDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'" ;
	$resP22=mysql_query($pQry22);
	$rowP22=mysql_fetch_array($resP22);
	
	$pQry23 = "SELECT sum(netAmount) as pSumT FROM `".TABLE_PURCHASE_PACKAGE_TRANSPORT."` WHERE (purchaseDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'" ;
	$resP23=mysql_query($pQry23);
	$rowP23=mysql_fetch_array($resP23);
	
	$pQry24 = "SELECT sum(netAmount) as pSumV FROM `".TABLE_PURCHASE_PACKAGE_VISA."` WHERE (purchaseDate between '$monthFirst' and '$monthLast') and proId='$proId' and finYear='$finYear'" ;
	$resP24=mysql_query($pQry24);
	$rowP24=mysql_fetch_array($resP24);
			
		$tot2= $rowA2['aSum']+$rowH2['hSum']+$rowM2['mSum']+$rowT2['tSum']+$rowV2['vSum']+$rowP21['pSumA']+$rowP22['pSumH']+$rowP23['pSumT']+$rowP24['pSumV'];
		
		$tot2=round($tot2);
		
		$barDataPurchase = $barDataPurchase.$tot2.',';				
}
$lastMonth = chop($month, ',');
$barDataSale = chop($barDataSale, ',');
$barDataPurchase = chop($barDataPurchase, ',');
?>
</div>
<script src="../../js/Chart.min.js"></script>
<script>
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var bXAxis = '<?php echo $lastMonth ?>';
    var barDataSale = '<?php echo $barDataSale ?>';
    var barDataPurchase = '<?php echo $barDataPurchase ?>';
    var barChartData = {
        labels: bXAxis.split(','),
        datasets: [
            {
                label: "Sales",
                fillColor: "rgba(210, 214, 222, 1)",
                strokeColor: "rgba(210, 214, 222, 1)",
                pointColor: "rgba(210, 214, 222, 1)",
                pointStrokeColor: "#c1c7d1",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: barDataSale.split(',')
            },
            {
                label: "Purchase",
                fillColor: "rgba(60,141,188,0.9)",
                strokeColor: "rgba(60,141,188,0.8)",
                pointColor: "#3b8bba",
                pointStrokeColor: "rgba(60,141,188,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(60,141,188,1)",
                data: barDataPurchase.split(',')
            }
        ]
    };
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
</script>
<?php
require('../admin_footer2.php');
?>