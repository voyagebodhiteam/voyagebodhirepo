<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear=$_SESSION['finYear'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype){
	// NEW SECTION
	case 'index':

	if(!$_REQUEST['name']){
		$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
		header("location:index.php");
	} 
	else{
		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		$db->connect();
		$success = 0;

		$staffName		= $App->capitalize($_REQUEST['name']);     
		$mobile			= $App->convert($_REQUEST['mobile']);

		$existId = $db->existValuesId(TABLE_STAFF, "staffName='$staffName' and mobile='$mobile' and proId=$proId");
		if($existId > 0){
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
			header("location:index.php");
		}
		else{
			
			$userName		=	$App->convert($_REQUEST['userName']);
			$password		=	$App->convert($_REQUEST['password']);
			$cPassword		=	$App->convert($_REQUEST['cpassword']);
									
			//echo $password."-".$cPassword;	die;	
			if($password==$cPassword){	
						
				$username1			=	mysql_real_escape_string(htmlentities($userName));
				$password1			=	mysql_real_escape_string(htmlentities($password));
						
				$pstring1	 		=	md5($password1); // Encrepted Password
				$pstring2	 		=	mysql_real_escape_string(htmlentities($pstring1));  // Password to store the database
					
						
				$selectRes=mysql_query("SELECT * FROM ".TABLE_LOGIN." WHERE userName='$username1'");
				$selectRow=mysql_fetch_array($selectRes);//echo $selectRes;die;
				$Result = mysql_num_rows($selectRes);//echo $Result;die;
				if($Result==0){					
					/*$existId=$db->existValuesId(TABLE_LOGIN,"userId='$success'");				
					if($existId>0){					
						$_SESSION['msg']=$App->sessionMsgCreate("error", "This User Has been Already Registered"); 					
						header("location:index.php");								
					}
					else{*/
						$data['staffName']	=	$staffName;
						$data['mobile']		=	$App->convert($_REQUEST['mobile']); 
			            $data['email']		=	$App->convert($_REQUEST['email']);                
			            $data['userType']	=	$App->convert($_REQUEST['type']); 
			            $data['proId']		=	$proId;
			            $data['loginId']	=	$loginId;
			            $data['finYear']	=	$finYear;  
			             $success = $db->query_insert(TABLE_STAFF, $data);
			               
						/* ************************************************************/
						$data2['userId']		=	$success;							
						$data2['userName']	=	$username1;
						$data2['password']	=	$pstring2;
						$data2['type']		=	'Staff';
                                                $data2['status']		=	'Active';
						$data2['finYear']		=$finYear;
						$success1 = $db->query_insert(TABLE_LOGIN, $data2);														
						if($success1){	
							require_once('../../PHPMailer/class.phpmailer.php');
							
							$emailTo	=	$App->convert($_REQUEST['email']);
							$userName	=	$App->convert($userName);
							$password	=	$App->convert($password1);
							$subject	=	"MARKAZ";
							$message	=	"UserName=".$userName."\r\n"."Password=".$password;
							//mail($email,$subject,$message);
							
							$email = new PHPMailer();
							$email->FromName  = "MARKAZ";
							$email->Subject   = $subject;
							$email->Body      = $message;
							$email->AddAddress($emailTo);
							
							if($email->Send())
							{
								$_SESSION['msg']=$App->sessionMsgCreate("success", "Added Successfully..");
							}
							else
							{
								$_SESSION['msg']=$App->sessionMsgCreate("success", "Added Successfully.Mail sending failed");
							} 
						}
						else{
							$_SESSION['msg']=$App->sessionMsgCreate("error", "Failed ");															
						}
					//}										
								
				}//if($Result==0)
				else{								
					$_SESSION['msg']=$App->sessionMsgCreate("error","Entered UserName Is Already Exist");		
				}
						
			}//if($pwd==$cPwd)
			else{					
				$_SESSION['msg']=$App->sessionMsgCreate("error","Password is Mismatches with Confirm Password");						
			}
			//}//else
			header("location:index.php");
		}	//else	
                
                
		/* ************************************************************/
		
	}
	break;
	 // EDIT SECTION
    case 'edit':
        $editId = $_REQUEST['editId'];
        if (!$_REQUEST['name']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:edit.php?id=$editId");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;

            $staffName		= $App->convert($_REQUEST['name']);           
            $staffName 		= ucwords(strtolower($staffName));
            $mobile				= $App->convert($_REQUEST['mobile']);
			

            $existId = $db->existValuesId(TABLE_STAFF, "name='$staffName' and mobile='$mobile' and proId=$proId and ID!='$editId'");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
                $data['staffName']	=	$staffName;
                $data['mobile']		=	$App->convert($_REQUEST['mobile']); 
                $data['email']			=	$App->convert($_REQUEST['email']);
               /* $data['userName']			=	$App->convert($_REQUEST['userName']); 
                $data['password']			=	$App->convert($_REQUEST['password']);*/
                $data['userType']			=	$App->convert($_REQUEST['type']);   
               	$data['proId']			=	$proId;
                $data['loginId']		=	$loginId;
                $data['finYear']		=	$finYear;
                $success = $db->query_update(TABLE_STAFF, $data, " ID='{$editId}'");
                $db->close();

                if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to update details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
        break;
	// DELETE SECTION
	case 'delete':
	$deleteId = $_REQUEST['id'];//echo $deleteId;die;
	$success = $success2 = 0;

	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
	$db->connect();

		$success = @mysql_query("DELETE FROM `" . TABLE_PERMISSION . "` WHERE staffId='{$deleteId}'");


		$success2 = "DELETE FROM `".TABLE_LOGIN."` WHERE userId='{$deleteId}' and type='Staff'";
		$db->query($success2);
		$success = @mysql_query("DELETE FROM `" . TABLE_STAFF . "` WHERE ID='{$deleteId}' and finYear=$finYear");
	
	$db->close();
	if($success){
		$_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
	} else{
		$_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
	}
	header("location:index.php");
	break;
}
?>