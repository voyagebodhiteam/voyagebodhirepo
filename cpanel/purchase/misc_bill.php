<?php
require('../admin_header.php');

if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"A.proId='$proId'";
}

//user permission
$type = $_SESSION['travelType'];

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>PURCHASE - MISC.</h3>
                </div>
                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Voucher No</th>
                            <th>Bill Date</th>
                            <th>Entry Date</th>
                            <th>Supplier Name</th>
                            <th>Net Amount</th>
                            <th>Set BillNo</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                     $i=0;
                     $k=1;
                        $selectAll = "select  A.ID,
                        					  A.invoiceNo,
                       						  A.invoiceDate, 
                       						  A.entryDate,                       					 										  A.billNo,
                        					  A.netAmount,
                        					  B.accountName
                        			  from ".TABLE_PURCHASE_MISC." as A,
                        			  	   ".TABLE_ACCOUNTS." as B
                        			  where A.supplierId=B.ID
                        			  and $check
                        			  order by A.ID desc";
                        $result = $db->query($selectAll);
                        $selectCount = mysql_num_rows($result);
                        
                        if($selectCount==0)
						{
						?>
							 <tr>
								<td align="center" colspan="7">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
                        /*********************** for pagination ******************************/
						$rowsPerPage = ROWS_PER_PAGE;
						if(isset($_GET['page']))
						{
							$pageNum = $_GET['page'];
						}
						else
						{
							$pageNum =1;
						}
						$offset = ($pageNum - 1) * $rowsPerPage;
						$select1=$db->query($selectAll." limit $offset, $rowsPerPage");
						$i=$offset+1;
						//use '$select1' for fetching
						/*************************** for pagination **************************/
                        
                        while ($row = mysql_fetch_array($select1)) {
                        	
      						$miscId = $row['ID'];
                            ?>
                        <form method="post" action="do.php?op=miscBill">     
                         <tr>                            
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $row['invoiceNo']; ?></td>
                            <td><?php echo $App->dbformat_date($row['invoiceDate']); ?></td>
                            <td><?php echo $App->dbformat_date($row['entryDate']); ?></td>
                            <td><?php echo $row['accountName']; ?></td>
                            <td><?php echo $row['netAmount']; ?></td>                        
                            <td> 
                            
                            <!--<a class="show_table_lnk show_table_lnk_view" href="view.php?id=<?php echo $row['ID'];?>&type=m">View</a>--> 
                            
                            <input type="hidden" name="miscId_<?php echo $k;?>" value="<?php echo $miscId; ?>">
                            <input type="text" id="miscBill" value="<?php echo $row['billNo']; ?>" placeholder="Set Bill No" name="miscBill_<?php echo $k++;?>">
                            
                            </td>
                        </tr>
                            <?php
                        }
                        }
                        ?>
				         
				         <tr>
                        	<td colspan="7">
                        		
                        		 <input type="hidden" name="misc_count" id="misc_row_count" value="<?php echo $selectCount; ?>">
                        		 
                        		<input name="save"  class="btn btn-success btn-md" style="background-color:#8abe40" type="submit" value="Save">
                        		
                        	</td>
                        </tr>
				        </form>            
                                 
                       
                    </tbody>
                </table>
            </div>
            
            <!-- paging -->		
            <!--<div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div> -->       
            <!-- paging end-->
            
            <!--*****************************************************************-->
            
            	 <?php 
                  if($selectCount>@$rowsPerPage)
					{
					?>	
					 <br />	
					  <div class="pagerSC" align="center">
					<?php
					
					$query   =  $db->query($selectAll);
					$numrows = mysql_num_rows($query);
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
					   if ($page == $pageNum)
					   {
						  $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
					   }
					   else
					   {
						 	if(@$search)
					   		 {
							 	$nav .= " <a href=\"$self?page=$page&sname=$search\">$page</a> ";
							 }
							 else
							 {
							 	$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							 }
					   }
					}
					?>
					 <?php
					if ($pageNum > 1)
					{
					   $page  = $pageNum - 1;
					   if(@$search)
					   {
						   $prev  = " <a href=\"$self?page=$page&sname=$search\">Prev</a> ";
						   $first = " <a href=\"$self?page=1&sname=$search\">First Page</a> ";
					   }
					   else
					   {
						   $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
						   $first = " <a href=\"$self?page=1\">First Page</a> ";
					   }
					}
					else
					{
					   $prev  = '&nbsp;';
					   $first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
					   $page = $pageNum + 1;
					   if(@$search)
					   {
						   	 $next = " <a href=\"$self?page=$page&sname=$search\">Next</a> ";
						     $last = " <a href=\"$self?page=$maxPage&sname=$search\">Last Page</a> ";
					   }
					   else
					   {
						   	 $next = " <a href=\"$self?page=$page\">Next</a> ";
						   	 $last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
					   }
					}
					else
					{
					   $next = '&nbsp;';
					   $last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
            
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>