<?php
require('../admin_header.php');

if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"A.proId='$proId'";
}


//user permission
$type = $_SESSION['travelType'];

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>PURCHASE - TRANSPORT</h3>
                </div>
                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Voucher No</th>
                            <th>Bill Date</th>
                            <th>Entry Date</th>
                            <th>Supplier Name</th>
                            <th>Net Amount</th>
                            <th>Set BillNo</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                     $i=0;
                     $j=1;
                        $selectAll = "select  A.ID,
                        					  A.invoiceNo,
                       						  A.invoiceDate, 
                       						  A.entryDate,                       					 										  A.billNo,	
                        					  A.netAmount,
                        					  B.accountName
                        			  from ".TABLE_PURCHASE_TRANSPORT." as A,
                        			  	   ".TABLE_ACCOUNTS." as B
                        			  where A.supplierId=B.ID
                        			  and $check
                        			  order by A.ID asc";
                        			  
                        $result = $db->query($selectAll);
                        $selectCount = mysql_num_rows($result);
                        while ($row = mysql_fetch_array($result)) {
                        	$transId = $row['ID'];
                            ?>
                         <form method="post" action="do.php?op=transportBill">      
                         <tr>                            
                            <td><?php echo ++$i; ?></td>
                            <td><?php echo $row['invoiceNo']; ?></td>
                            <td><?php echo $App->dbformat_date($row['invoiceDate']); ?></td>
                            <td><?php echo $App->dbformat_date($row['entryDate']); ?></td>
                            <td><?php echo $row['accountName']; ?></td>
                            <td><?php echo $row['netAmount']; ?></td>                        
                            <td>     
                            <!--<a class="show_table_lnk show_table_lnk_view" href="view.php?id=<?php echo $row['ID'];?>&type=t">View</a>-->
                            <input type="hidden" name="transId_<?php echo $j;?>" value="<?php echo $transId; ?>">
                            <input type="text" id="transBill" value="<?php echo $row['billNo']; ?>" placeholder="Set Bill No" name="transBill_<?php echo $j++;?>">
                            
                            </td>
                        </tr>
                            <?php
                        }
                        ?>
                        
                        <tr>
                        	<td colspan="7">
                        		
                        		 <input type="hidden" name="trans_count" id="trans_row_count" value="<?php echo $selectCount; ?>">
                        		 
                        		<input name="save"  class="btn btn-success btn-md" style="background-color:#8abe40" type="submit" value="Save">
                        		
                        	</td>
                        </tr>
				        </form>  
				                   
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>