<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear=$_SESSION['finYear'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            $purchaseType	=	$App->convert($_REQUEST['purchaseType']);
            $supplierId		=	$App->convert($_REQUEST['supplierId']);
            
            if($purchaseType=='Airline'){ 
            	$invoice_sql 	= 	"SELECT MAX(invoiceNo) AS invoiceNo FROM `".TABLE_PURCHASE_AIRLINE."` where proId=$proId and finYear=$finYear";
            	//echo $invoice_sql;die;
             	$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Airline Payable'";
             	$tsdata['voucherType']		=  	'Airline Payable';
             	$tfdata['voucherType']		=  	'Airline Payable';
                $transType			=	'Airline Receivable';
                                        }
		    elseif($purchaseType=='Visa'){ 
		    	$invoice_sql 	= 	"SELECT MAX(invoiceNo) AS invoiceNo FROM `".TABLE_PURCHASE_VISA."` where proId=$proId and finYear=$finYear";
		    	$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Visa Payable'";
		    	$tsdata['voucherType']		=  	'Visa Payable';
             	        $tfdata['voucherType']		=  	'Visa Payable';
                        $transType			=	'Visa Receivable';
                                                 }	
		  //elseif($purchaseType=='Package'){ 
		  		//$invoice_sql 	= 	"SELECT MAX(invoiceNo) AS invoiceNo FROM `".TABLE_PURCHASE_PACKAGE."`";
		  		//$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Package Payable ' ";
		  		//$tsdata['voucherType']		=  	'Package Payable ';
             	//$tfdata['voucherType']		=  	'Package Payable ';}
		    elseif($purchaseType=='Hotel'){ 
		    	$invoice_sql 	= 	"SELECT MAX(invoiceNo) AS invoiceNo FROM `".TABLE_PURCHASE_HOTEL."` where proId=$proId and finYear=$finYear";
		    	$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Hotel Payable'";
		    	$tsdata['voucherType']		=  	'Hotel Payable';
             	        $tfdata['voucherType']		=  	'Hotel Payable';
                        $transType			=	'Hotel Receivable';                          
                                                   }
		    elseif($purchaseType=='Transportation'){ 
		    	$invoice_sql 	= 	"SELECT MAX(invoiceNo) AS invoiceNo FROM `".TABLE_PURCHASE_TRANSPORT."` where proId=$proId and finYear=$finYear";
		    	$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Transport Payable'";
		    	$tsdata['voucherType']		=  	'Transport Payable';
             	        $tfdata['voucherType']		=  	'Transport Payable';
                        $transType			=	'Transport Receivable';
                                                           }
		    elseif($purchaseType=='Misc'){ 
		    	$invoice_sql 	= 	"SELECT MAX(invoiceNo) AS invoiceNo FROM `".TABLE_PURCHASE_MISC."` where proId=$proId and finYear=$finYear";
		    	$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Misc Payable'";
		    	$tsdata['voucherType']		=  	'Misc Payable';
             	        $tfdata['voucherType']		=  	'Misc Payable';
                        $transType			=	'Misc Receivable';
                                                 }
		 	
				$invoice_record	=	$db->query_first($invoice_sql);
				$newinvoiceId		=	$invoice_record['invoiceNo']+1;	
	        	//echo $newinvoiceId;die;
				$acc_record 	= 	$db->query_first($accId);
			 	$accountId		=	$acc_record['ID'];
				
				$str = $_POST['ourInvoiceNo'];
				$str2 = substr($str, 2);				
      
                $data['invoiceNo']			=	$newinvoiceId;    
                $data['invoiceDate']		=	$App->dbformat_date($_REQUEST['invoiceDate']);
                $data['entryDate']			=	$App->dbformat_date($_REQUEST['entryDate']); 
                $data['supplierId']			=	$supplierId; 
                $data['expAccountId']		=	$App->convert($_REQUEST['expAccountId']);
                $data['ourInvoiceNo']		=	$str2; 
                $data['taxRegNo']			=	$App->convert($_REQUEST['taxRegNo']);
                
                $data['basicAmount']		=	$basicAmount2	=	$App->convert($_REQUEST['basicAmount']);
                $data['proCharge']			=	$procCharge2	=	$App->convert($_REQUEST['proCharge']);
                $data['otherCharges']		=	$otherCharge2	=  $App->convert($_REQUEST['otherCharges']);
                $data['tds']				=	$tds2			=	$App->convert($_REQUEST['tds']); 
                $data['refundable']			=	$refundable2	=	$App->convert($_REQUEST['refundable']); 
              
                $totAmt					=	$basicAmount2+$tds2+$procCharge2+$otherCharge2+$refundable2;
                $mainTax				=	$App->convert($_REQUEST['tax']);
				if(strpos ($mainTax,'%'))
				{
					$taxTrim	=	rtrim($mainTax, "%");							
					$mainTax	=	$totAmt*($taxTrim/100);
					$data['taxInPer']		=	$mainTax; 
				}
				else{
					$data['taxInPer']		=	$mainTax;
				}           
                $data['roundAmount']		=	$App->convert($_REQUEST['roundAmount']); 
                $data['netAmount']			=	$App->convert($_REQUEST['netAmount']);
                $data['remark']				=	$App->convert($_REQUEST['remark']); 
                $data['description']		=	$App->convert($_REQUEST['description']);     
                $data['billNo']				=	$App->convert($_REQUEST['billNo']); 
                $data['tac']				=	$App->convert($_REQUEST['tac']);          
                $data['proId']				=	$proId;
                $data['loginId']			=	$loginId;
                $data['finYear']			=	$finYear;
                
  if($purchaseType=='Airline'){  $success = $db->query_insert(TABLE_PURCHASE_AIRLINE, $data);}
  elseif($purchaseType=='Visa'){  $success = $db->query_insert(TABLE_PURCHASE_VISA, $data);}	
 //elseif($purchaseType=='Package'){  $success = $db->query_insert(TABLE_PURCHASE_PACKAGE, $data);}
  elseif($purchaseType=='Hotel'){ $success = $db->query_insert(TABLE_PURCHASE_HOTEL, $data);}
  elseif($purchaseType=='Transportation'){  $success = $db->query_insert(TABLE_PURCHASE_TRANSPORT, $data);}
  elseif($purchaseType=='Misc'){  $success = $db->query_insert(TABLE_PURCHASE_MISC, $data);}
 
		         /*-------------------------------------------------------------------------*/
		           //To transaction table		           
				    
					/* ------------------------------------------ */														
					/* ------------------------------------------ */

                                         $purchaseQuery		=	"SELECT DISTINCT details FROM ".TABLE_TRANSACTION." WHERE voucherNo = '$str2' AND voucherType='$transType' AND proId=$proId and finYear=$finYear";
					//echo $purchaseQuery;die;
					$resultQuery	=	$db->query_first($purchaseQuery);
					$transDetails	=	$resultQuery['details'];
					
					//adding prefix for getting corresponding details
	               				if($transType=='Airline Receivable')
	               				{
										$prefix		=	'AP';
								}
								else if($transType=='Hotel Receivable')
								{
										$prefix		=	'HP';
								}
								else if($transType=='Visa Receivable')
								{
										$prefix		=	'VP';
								}
								else if($transType=='Misc Receivable')
								{
										$prefix		=	'MP';
								}
								else
								{
										$prefix		=	'TP';
								}
					


					$details	=	"Details:".$transDetails;

					$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$accountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	$App->convert($_POST['netAmount'])-$App->convert($_REQUEST['roundAmount'])-$App->convert($_REQUEST['tds'])-$mainTax+$App->convert($_REQUEST['tac']);
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	$App->convert($_REQUEST['remark']); 
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$accountId;
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	$App->convert($_POST['netAmount'])-$App->convert($_REQUEST['roundAmount'])-$App->convert($_REQUEST['tds'])-$mainTax+$App->convert($_REQUEST['tac']);
					
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	$App->convert($_REQUEST['remark']);
					$tfdata['details']			=	$details; 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  /*----------------------------------------------------------------------------*/
		             /*----------------------------------------------------------------------------*/
		          //discount
		          	
		          	$discount		=	$App->convert($_REQUEST['roundAmount']);
		          	$tds			=	$App->convert($_REQUEST['tds']);
		          	$tac			=	$App->convert($_REQUEST['tac']);
		           	         
		          if($discount>0)
		          {
		          	
		          	 $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$discountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	abs($App->convert($_REQUEST['roundAmount']));
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Discount Given:inv#".$newinvoiceId;
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $discountId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($App->convert($_REQUEST['roundAmount']));
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Discount Given:inv#".$newinvoiceId; 
					$tfdata['details']			=	$details;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				   
					 	         
		          if($discount<0)
		          {
		          	
		          	 $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Received' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$discountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['roundAmount']));
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Discount Rcvd:inv#".$newinvoiceId; 
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $discountId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   abs($App->convert($_REQUEST['roundAmount']));
					$tfdata['debit']			= 	0;
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Discount Rcvd:inv#".$newinvoiceId;
					$tfdata['details']			=	$details; 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tds
				  if($tds)
		          {
		          	
		          	 $tdsQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tds' ";
					 $tds_record 		= 	$db->query_first($tdsQuery);
					 $tdsId		=	$tds_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$tdsId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	abs($App->convert($_REQUEST['tds']));
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tds:inv#".$newinvoiceId;
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $tdsId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($App->convert($_REQUEST['tds']));
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tds:inv#".$newinvoiceId; 
					$tfdata['details']			=	$details;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tax
				  if($mainTax)
		          {
		          	
		          	 $taxQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tax' ";
					 $tax_record 		= 	$db->query_first($taxQuery);
					 $taxId				=	$tax_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$taxId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	abs($mainTax);
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tax:inv#".$newinvoiceId;
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $taxId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($mainTax);
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tax:inv#".$newinvoiceId; 
					$tfdata['details']			=	$details;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tac
				  if($tac)
		          {
		          	
		          	 $tacQuery		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tac' ";
					 $tac_record 	= 	$db->query_first($tacQuery);
					 $tacId			=	$tac_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$tacId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['tac']));
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tac:inv#".$newinvoiceId; 
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $tacId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   abs($App->convert($_REQUEST['tac']));
					$tfdata['debit']			= 	0;
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tac:inv#".$newinvoiceId;
					$tfdata['details']			=	$details; 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
		          
		          /*----------------------------------------------------------------------------*/ 
		             $db->close();

		                if ($success) 
		                {		                  
		                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
		                } 
		                else 
		                {
		                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
		                }		               
	                  if($purchaseType=='Airline'){header("location:airline.php");}
					  elseif($purchaseType=='Visa'){header("location:visa.php");}	
					 //elseif($purchaseType=='Package'){header("location:package.php");}
					  elseif($purchaseType=='Hotel'){header("location:hotel.php");}
					  elseif($purchaseType=='Transportation'){header("location:transport.php");}
					  elseif($purchaseType=='Misc'){header("location:misc.php");}
	 
        break;
    // EDIT SECTION
    case 'edit':	
    		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = $totAmt=0;
            $str  = $App->convert($_REQUEST['ourInvoiceNo']);
			$str2 = substr($str, 2);
            $purchaseType	=	$App->convert($_REQUEST['purchaseType']);
            $supplierId		=	$App->convert($_REQUEST['supplierId']);
            
            if($purchaseType=='Airline'){ 
            	$invoice_sql 	= 	"SELECT invoiceNo FROM `".TABLE_PURCHASE_AIRLINE."` where ourInvoiceNo= '$str2' AND proId=$proId and finYear=$finYear";
            	$invoice_record	=	$db->query_first($invoice_sql);
				$newinvoiceId	=	$invoice_record['invoiceNo'];
				@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$newinvoiceId}' and voucherType='Airline Payable' and proId=$proId and finYear=$finYear");
					
            	//echo $invoice_sql;die;
             	$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Airline Payable'";
             	$tsdata['voucherType']		=  	'Airline Payable';
             	$tfdata['voucherType']		=  	'Airline Payable';
                $transType = 'Airline Receivable';
}
		    elseif($purchaseType=='Visa'){ 
		    	$invoice_sql 	= 	"SELECT invoiceNo FROM `".TABLE_PURCHASE_VISA."` where ourInvoiceNo= '$str2' AND proId=$proId and finYear=$finYear";
		    	$invoice_record	=	$db->query_first($invoice_sql);
				$newinvoiceId	=	$invoice_record['invoiceNo'];
				@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$newinvoiceId}' and voucherType='Visa Payable' and proId=$proId and finYear=$finYear");
					
		    	$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Visa Payable'";
		    	$tsdata['voucherType']		=  	'Visa Payable';
             	        $tfdata['voucherType']		=  	'Visa Payable';
                        $transType = 'Visa Receivable';}			  
		    elseif($purchaseType=='Hotel'){ 
		    	$invoice_sql 	= 	"SELECT invoiceNo FROM `".TABLE_PURCHASE_HOTEL."` where ourInvoiceNo= '$str2' AND proId=$proId and finYear=$finYear";
		    	$invoice_record	=	$db->query_first($invoice_sql);
				$newinvoiceId	=	$invoice_record['invoiceNo'];
				@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$newinvoiceId}' and voucherType='Hotel Payable' and proId=$proId and finYear=$finYear");
					
		    	$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Hotel Payable'";
		    	$tsdata['voucherType']		=  	'Hotel Payable';
             	        $tfdata['voucherType']		=  	'Hotel Payable';
                        $transType = 'Hotel Receivable';
}
		    elseif($purchaseType=='Transportation'){ 
		    	$invoice_sql 	= 	"SELECT invoiceNo FROM `".TABLE_PURCHASE_TRANSPORT."` where ourInvoiceNo= '$str2' AND proId=$proId and finYear=$finYear";
		    	$invoice_record	=	$db->query_first($invoice_sql);
				$newinvoiceId	=	$invoice_record['invoiceNo'];
				@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$newinvoiceId}' and voucherType='Transport Payable' and proId=$proId and finYear=$finYear");
					
		    	$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Transport Payable'";
		    	$tsdata['voucherType']		=  	'Transport Payable';
             	        $tfdata['voucherType']		=  	'Transport Payable';
                        $transType = 'Transport Receivable';
}
		    elseif($purchaseType=='Misc'){ 
		    	$invoice_sql 	= 	"SELECT invoiceNo FROM `".TABLE_PURCHASE_MISC."` where ourInvoiceNo= '$str2' AND proId=$proId and finYear=$finYear";
		    	$invoice_record	=	$db->query_first($invoice_sql);
				$newinvoiceId	=	$invoice_record['invoiceNo'];
				@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$newinvoiceId}' and voucherType='Misc Payable' and proId=$proId and finYear=$finYear");
					
		    	$accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Misc Payable'";
		    	$tsdata['voucherType']		=  	'Misc Payable';
             	        $tfdata['voucherType']		=  	'Misc Payable';
                        $transType = 'Misc Receivable';
}
		 	
				
	        	//echo $newinvoiceId;die;
				$acc_record 	= 	$db->query_first($accId);
			 	$accountId		=	$acc_record['ID'];
				
								
      
                $data['invoiceNo']			=	$newinvoiceId;    
                $data['invoiceDate']		=	$App->dbformat_date($_REQUEST['invoiceDate']);
                $data['entryDate']			=	$App->dbformat_date($_REQUEST['entryDate']); 
                $data['supplierId']			=	$supplierId; 
                $data['expAccountId']		=	$App->convert($_REQUEST['expAccountId']);
                $data['ourInvoiceNo']		=	$str2; 
                $data['taxRegNo']			=	$App->convert($_REQUEST['taxRegNo']);
                $data['basicAmount']		=	$basicAmount2	=	$App->convert($_REQUEST['basicAmount']);
                $data['proCharge']			=	$procCharge2	=	$App->convert($_REQUEST['proCharge']);
                $data['otherCharges']		=	$otherCharge2	=  $App->convert($_REQUEST['otherCharges']);
                $data['tds']				=	$tds2			=	$App->convert($_REQUEST['tds']); 
                $data['refundable']			=	$refundable2	=	$App->convert($_REQUEST['refundable']); 
              
                $totAmt					=	$basicAmount2+$tds2+$procCharge2+$otherCharge2+$refundable2;
                $mainTax				=	$App->convert($_REQUEST['tax']);
				if(strpos ($mainTax,'%'))
				{
					$taxTrim	=	rtrim($mainTax, "%");							
					$mainTax	=	$totAmt*($taxTrim/100);
					$data['taxInPer']		=	$mainTax; 
				}
				else{
					$data['taxInPer']		=	$mainTax;
				}  
                
                $data['roundAmount']		=	$App->convert($_REQUEST['roundAmount']); 
                $data['netAmount']			=	$App->convert($_REQUEST['netAmount']);
                $data['remark']				=	$App->convert($_REQUEST['remark']); 
                $data['description']		=	$App->convert($_REQUEST['description']);     
                $data['billNo']				=	$App->convert($_REQUEST['billNo']); 
                $data['tac']				=	$App->convert($_REQUEST['tac']);          
                $data['proId']				=	$proId;
                $data['loginId']			=	$loginId;
                $data['finYear']			=	$finYear;
                
  if($purchaseType=='Airline'){  $success = $db->query_update(TABLE_PURCHASE_AIRLINE, $data,"invoiceNo='{$newinvoiceId}'");}
  elseif($purchaseType=='Visa'){  $success = $db->query_update(TABLE_PURCHASE_VISA, $data,"invoiceNo='{$newinvoiceId}'");}	
  elseif($purchaseType=='Hotel'){ $success = $db->query_update(TABLE_PURCHASE_HOTEL, $data,"invoiceNo='{$newinvoiceId}'");}
  elseif($purchaseType=='Transportation'){  $success = $db->query_update(TABLE_PURCHASE_TRANSPORT, $data,"invoiceNo='{$newinvoiceId}'");}
  elseif($purchaseType=='Misc'){  $success = $db->query_update(TABLE_PURCHASE_MISC, $data,"invoiceNo='{$newinvoiceId}'");}
 
		         /*-------------------------------------------------------------------------*/
		           //To transaction table		           
				    
					/* ------------------------------------------ */	

                                        $purchaseQuery		=	"SELECT DISTINCT details FROM ".TABLE_TRANSACTION." WHERE voucherNo = '$str2' AND voucherType='$transType' and proId=$proId and finYear=$finYear";
					//echo $purchaseQuery;die;
					$resultQuery	=	$db->query_first($purchaseQuery);
					$transDetails	=	$resultQuery['details'];

													
					/* ------------------------------------------ */
		$details	=	"INV#".$newinvoiceId.",sup:".$supplierId.",:against:".$str2.",Details:".$transDetails;

					$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$accountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	$App->convert($_POST['netAmount'])-$App->convert($_REQUEST['roundAmount'])-$App->convert($_REQUEST['tds'])-$mainTax+$App->convert($_REQUEST['tac']);
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	$App->convert($_REQUEST['remark']); 
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$accountId;
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	$App->convert($_POST['netAmount'])-$App->convert($_REQUEST['roundAmount'])-$App->convert($_REQUEST['tds'])-$mainTax+$App->convert($_REQUEST['tac']);
					
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	$App->convert($_REQUEST['remark']);
					$tfdata['details']			=	$details; 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  /*----------------------------------------------------------------------------*/
		             /*----------------------------------------------------------------------------*/
		          //discount
		          	
		          	$discount		=	$App->convert($_REQUEST['roundAmount']);
		          	$tds			=	$App->convert($_REQUEST['tds']);
		          	$tac			=	$App->convert($_REQUEST['tac']);
		           	         
		          if($discount>0)
		          {
		          	
		          	 $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$discountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	abs($App->convert($_REQUEST['roundAmount']));
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Discount Given:inv#".$newinvoiceId;
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $discountId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($App->convert($_REQUEST['roundAmount']));
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Discount Given:inv#".$newinvoiceId; 
					$tfdata['details']			=	$details;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				   
					 	         
		          if($discount<0)
		          {
		          	
		          	 $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Received' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$discountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['roundAmount']));
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Discount Rcvd:inv#".$newinvoiceId; 
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $discountId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   abs($App->convert($_REQUEST['roundAmount']));
					$tfdata['debit']			= 	0;
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Discount Rcvd:inv#".$newinvoiceId;
					$tfdata['details']			=	$details; 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tds
				  if($tds)
		          {
		          	
		          	 $tdsQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tds' ";
					 $tds_record 		= 	$db->query_first($tdsQuery);
					 $tdsId		=	$tds_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$tdsId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	abs($App->convert($_REQUEST['tds']));
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tds:inv#".$newinvoiceId;
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $tdsId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($App->convert($_REQUEST['tds']));
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tds:inv#".$newinvoiceId; 
					$tfdata['details']			=	$details;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tax
				  if($mainTax)
		          {
		          	
		          	 $taxQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tax' ";
					 $tax_record 		= 	$db->query_first($taxQuery);
					 $taxId				=	$tax_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$taxId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	abs($mainTax);
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tax:inv#".$newinvoiceId;
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $taxId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($mainTax);
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tax:inv#".$newinvoiceId; 
					$tfdata['details']			=	$details;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tac
				  if($tac)
		          {
		          	
		          	 $tacQuery		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tac' ";
					 $tac_record 	= 	$db->query_first($tacQuery);
					 $tacId			=	$tac_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$tacId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					//$tsdata['voucherType']		=  	'Airline Receivable'; //above assigned
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['tac']));
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tac:inv#".$newinvoiceId; 
					$tsdata['details']			=	$details;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $tacId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					//$tfdata['voucherType']		=  	'Airline Receivable';//above assigned
					$tfdata['credit']			=   abs($App->convert($_REQUEST['tac']));
					$tfdata['debit']			= 	0;
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['entryDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tac:inv#".$newinvoiceId;
					$tfdata['details']			=	$details; 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
		          
		          /*----------------------------------------------------------------------------*/ 
		             $db->close();

		                if ($success) 
		                {		                  
		                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
		                } 
		                else 
		                {
		                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
		                }		               
	                  if($purchaseType=='Airline'){header("location:airline.php");}
					  elseif($purchaseType=='Visa'){header("location:visa.php");}	
					 //elseif($purchaseType=='Package'){header("location:package.php");}
					  elseif($purchaseType=='Hotel'){header("location:hotel.php");}
					  elseif($purchaseType=='Transportation'){header("location:transport.php");}
					  elseif($purchaseType=='Misc'){header("location:misc.php");}
	 
        break;
   // DELETE SECTION
	//-
	case 'delete':		
				$purchaseId		=	$_REQUEST['id'];
				$purchaseType	=	$_REQUEST['type'];
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();	
				
				
		if($purchaseType=='a'){ $select 	= 	"select  invoiceNo from ". TABLE_PURCHASE_AIRLINE." where ID='$purchaseId' AND proId=$proId and finYear=$finYear";
		$vouType='Airline Payable';}
		
		elseif($purchaseType=='v'){ $select = 	"select  invoiceNo from ". TABLE_PURCHASE_VISA." where ID='$purchaseId' AND proId=$proId and finYear=$finYear";
		$vouType='Visa Payable';}	
		
		//elseif($purchaseType=='p'){ $select 	= 	"select  invoiceNo from ". TABLE_INVOICE_PACKAGE." where ID='$purchaseId'";
		//$vouType='Package Payable ';}
		
		elseif($purchaseType=='h'){ $select = 	"select  invoiceNo from ". TABLE_PURCHASE_HOTEL." where ID='$purchaseId' AND proId=$proId and finYear=$finYear";
		$vouType='Hotel Payable';}
		
		elseif($purchaseType=='t'){ $select = 	"select  invoiceNo from ". TABLE_PURCHASE_TRANSPORT." where ID='$purchaseId' AND proId=$proId and finYear=$finYear";
		$vouType='Transport Payable';}
		
		elseif($purchaseType=='m'){ $select = 	"select  invoiceNo from ". TABLE_PURCHASE_MISC." where ID='$purchaseId' AND proId=$proId and finYear=$finYear";
		$vouType='Misc Payable';}
		
			$invoiceRecord = 	$db->query_first($select);
			$invoiceNo		= 	$invoiceRecord['invoiceNo'];
		// delete from transaction	
		$success1=@mysql_query("DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucherNo`='$invoiceNo' AND `voucherType`='$vouType' AND proId=$proId and finYear=$finYear");	
		// delete from each table	
		if($purchaseType=='a'){ 
		$success2= @mysql_query("DELETE FROM `".TABLE_PURCHASE_AIRLINE."` WHERE ID='$purchaseId' AND proId=$proId and finYear=$finYear");}
		
		elseif($purchaseType=='v'){ 
		$success2= @mysql_query("DELETE FROM `".TABLE_PURCHASE_VISA."` WHERE ID='$purchaseId' AND proId=$proId and finYear=$finYear");}	
		
		//elseif($purchaseType=='p'){ 
		//$success2= @mysql_query("DELETE FROM `".TABLE_INVOICE_PACKAGE."` WHERE ID='$purchaseId'");}
		
		elseif($purchaseType=='h'){ 
		$success2= @mysql_query("DELETE FROM `".TABLE_PURCHASE_HOTEL."` WHERE ID='$purchaseId' AND proId=$proId and finYear=$finYear");}
		
		elseif($purchaseType=='t'){
		$success2= @mysql_query("DELETE FROM `".TABLE_PURCHASE_TRANSPORT."` WHERE ID='$purchaseId' AND proId=$proId and finYear=$finYear");}
		
		elseif($purchaseType=='m'){ 
		$success2= @mysql_query("DELETE FROM `".TABLE_PURCHASE_MISC."` WHERE ID='$purchaseId' AND proId=$proId and finYear=$finYear");}
		
		$db->close(); 									
				
		if($success1 && $success2)
			{
			$_SESSION['msg']=  $App->sessionMsgCreate('success', "Details Deleted Successfully");							
			}
			else
			{
			$_SESSION['msg']="Failed";						
			}
			
		 if($purchaseType=='a'){header("location:airline.php");}
		  elseif($purchaseType=='v'){header("location:visa.php");}	
		 //elseif($purchaseType=='p'){header("location:package.php");}
		  elseif($purchaseType=='h'){header("location:hotel.php");}
		  elseif($purchaseType=='t'){header("location:transport.php");}
		  elseif($purchaseType=='m'){header("location:misc.php");}					
		break;	
		
	
	
	case 'airlineBill':
						
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
			$db->connect();
			$success1=0;
			
						
				$purchaseCount 				=	$_REQUEST['purchase_count'];
				//echo $purchaseCount;die;
				for($i=1;$i<=$purchaseCount; $i++)
				{
					$airlineId1		=	'airlineId_'.$i;
					$airlineId		=	$_REQUEST[$airlineId1];
					$billNo1		=	'setBillNo_'.$i;
					$billNo			=	$_REQUEST[$billNo1];
					//echo $billNo;die;	
				
					$data['billNo']	=	'';
				
				
				$data['billNo']					=	$App->convert($_REQUEST[$billNo1]);		
				
				$success1=$db->query_update(TABLE_PURCHASE_AIRLINE,$data,"ID='{$airlineId}'");
				//echo $tableId;	die;	
				//die;	
				
				}
				$db->close();
				
				
					if($success1)
					{							
						 $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Added Successfully");										
					}
					
					else{
						$_SESSION['msg'] =  $App->sessionMsgCreate('error', "Failed");										
					}
				
							
				header("location:airline.php");											

					
		break;
		
		
		case 'hotelBill':
						
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
			$db->connect();
			$success1=0;
			
						
				$purchaseCount 				=	$_REQUEST['purchase_count'];
				//echo $purchaseCount;die;
				for($i=1;$i<=$purchaseCount; $i++)
				{
					$hotelId1			=	'hotelId_'.$i;
					$hotelId			=	$_REQUEST[$hotelId1];
					$hotelBill1			=	'hotelBill_'.$i;
					$hotelBill			=	$_REQUEST[$hotelBill1];
					
				
				$data['billNo']	=	'';
				
				$data['billNo']					=	$App->convert($_REQUEST[$hotelBill1]);		
				
				$success1=$db->query_update(TABLE_PURCHASE_HOTEL,$data,"ID='{$hotelId}'");
			
				}
				$db->close();
				
				
					if($success1)
					{							
						 $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Added Successfully");										
					}
					
					else{
						$_SESSION['msg'] =  $App->sessionMsgCreate('error', "Failed");										
					}
				
							
				header("location:hotel.php");											

					
		break;
		
		
		case 'miscBill':
						
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
			$db->connect();
			$success1=0;
			
						
				$miscCount 				=	$_REQUEST['misc_count'];
				//echo $purchaseCount;die;
				for($i=1;$i<=$miscCount; $i++)
				{
					$miscId1			=	'miscId_'.$i;
					$miscId				=	$_REQUEST[$miscId1];
					$miscBill1			=	'miscBill_'.$i;
					$miscBill			=	$_REQUEST[$miscBill1];
					
				
				$data['billNo']	=	'';
				
				$data['billNo']					=	$App->convert($_REQUEST[$miscBill1]);		
				
				$success1=$db->query_update(TABLE_PURCHASE_MISC,$data,"ID='{$miscId}'");
			
				}
				$db->close();
				
				
					if($success1)
					{							
						 $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Added Successfully");										
					}
					
					else{
						$_SESSION['msg'] =  $App->sessionMsgCreate('error', "Failed");										
					}
				
							
				header("location:misc.php");											

					
		break;
		
		case 'transportBill':
						
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
			$db->connect();
			$success1=0;
			
						
				$transCount 				=	$_REQUEST['trans_count'];
				//echo $purchaseCount;die;
				for($i=1;$i<=$transCount; $i++)
				{
					$transId1				=	'transId_'.$i;
					$transId				=	$_REQUEST[$transId1];
					$transBill1				=	'transBill_'.$i;
					$transBill				=	$_REQUEST[$transBill1];
					
				
				$data['billNo']	=	'';
				
				$data['billNo']					=	$App->convert($_REQUEST[$transBill1]);		
				
				$success1=$db->query_update(TABLE_PURCHASE_TRANSPORT,$data,"ID='{$transId}'");
			
				}
				$db->close();
				
				
					if($success1)
					{							
						 $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Added Successfully");										
					}
					
					else{
						$_SESSION['msg'] =  $App->sessionMsgCreate('error', "Failed");										
					}
				
							
				header("location:transport.php");											

					
		break;
		
		case 'visaBill':
						
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
			$db->connect();
			$success1=0;
			
						
				$visaCount 				=	$_REQUEST['visa_count'];
				//echo $purchaseCount;die;
				for($i=1;$i<=$visaCount; $i++)
				{
					$visaId1				=	'visaId_'.$i;
					$visaId					=	$_REQUEST[$visaId1];
					$visaBill1				=	'visaBill_'.$i;
					$visaBill				=	$_REQUEST[$visaBill1];
					
				
				$data['billNo']	=	'';
				
				$data['billNo']					=	$App->convert($_REQUEST[$visaBill1]);		
				
				$success1=$db->query_update(TABLE_PURCHASE_VISA,$data,"ID='{$visaId}'");
			
				}
				$db->close();
				
				
					if($success1)
					{							
						 $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Added Successfully");										
					}
					
					else{
						$_SESSION['msg'] =  $App->sessionMsgCreate('error', "Failed");										
					}
				
							
				header("location:visa.php");											

					
		break;
			
		
}//end of switch case

?>