<?php
require('../admin_header.php');
if(@isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$purchaseType		=	$_REQUEST['type'];

if($purchaseType=='a'){ $head="AIRLINE";$prefix2="AI";$prefix="AP";}
elseif($purchaseType=='v'){ $head="VISA";$prefix2="VI";$prefix="VP";}	
elseif($purchaseType=='h'){ $head="HOTEL";$prefix2="HI";$prefix="HP";}
elseif($purchaseType=='t'){ $head="TRANSPORTATION";$prefix2="TI";$prefix="TP";}
elseif($purchaseType=='m'){ $head="MISC";$prefix2="MI";$prefix="MP";}

if(isset($_REQUEST['pId'])){
$purchaseId			=	$_REQUEST['pId'];
if($purchaseType=='a'){ $purchaseSelect = "select  ". TABLE_INVOICE_AIRLINE.".netAmount,
	". TABLE_INVOICE_AIRLINE.".invoiceNo,
	sum(".TABLE_AIRLINE_FLIGHT.".fare) as fSum,
	sum(".TABLE_AIRLINE_FLIGHT.".tax) as tSum,
	sum(".TABLE_AIRLINE_FLIGHT.".yq) as ySum 
	from ". TABLE_INVOICE_AIRLINE.",".TABLE_AIRLINE_PASSENGER.",".TABLE_AIRLINE_FLIGHT." 
	where ". TABLE_INVOICE_AIRLINE.".ID='$purchaseId'
	and ". TABLE_INVOICE_AIRLINE.".ID=". TABLE_AIRLINE_PASSENGER.".airlineId
	and ". TABLE_AIRLINE_PASSENGER.".ID=". TABLE_AIRLINE_FLIGHT.".airlinePassId
	AND ". TABLE_INVOICE_AIRLINE.".proId=$proId
	group by ". TABLE_AIRLINE_PASSENGER.".airlineId";	
													
}
											
elseif($purchaseType=='v'){ $purchaseSelect = "select  ". TABLE_INVOICE_VISA.".netAmount,
	". TABLE_INVOICE_VISA.".invoiceNo,	
	sum(".TABLE_VISA_PASSENGER.".visaFee) as amountSum
	from ". TABLE_INVOICE_VISA.",".TABLE_VISA_PASSENGER."
	where ". TABLE_INVOICE_VISA.".ID='$purchaseId'
	and ". TABLE_INVOICE_VISA.".ID=". TABLE_VISA_PASSENGER.".visaId
	AND ". TABLE_INVOICE_VISA.".proId=$proId				
	group by ". TABLE_VISA_PASSENGER.".visaId";}		

elseif($purchaseType=='h'){ $purchaseSelect = "select  ". TABLE_INVOICE_HOTEL.".netAmount,
	". TABLE_INVOICE_HOTEL.".invoiceNo,
	sum(".TABLE_HOTEL_CUSTOMER.".netAmount) as netAmountSum			
	from ". TABLE_INVOICE_HOTEL.",".TABLE_HOTEL_CUSTOMER."
	where ". TABLE_INVOICE_HOTEL.".ID='$purchaseId'
	and ". TABLE_INVOICE_HOTEL.".ID=". TABLE_HOTEL_CUSTOMER.".hotelInvoiceId
	AND ". TABLE_INVOICE_HOTEL.".proId=$proId	
	group by ". TABLE_HOTEL_CUSTOMER.".hotelInvoiceId";}

elseif($purchaseType=='m'){ $purchaseSelect = "select  ". TABLE_INVOICE_MISC.".netAmount,
	". TABLE_INVOICE_MISC.".invoiceNo,
	sum(". TABLE_MISC_PASSENGER.".basicAmount) as sumBasic			
	from ". TABLE_INVOICE_MISC.",". TABLE_MISC_PASSENGER."
	where ". TABLE_INVOICE_MISC.".ID='$purchaseId' AND ". TABLE_INVOICE_MISC.".proId=$proId AND ". TABLE_INVOICE_MISC.".ID=". TABLE_MISC_PASSENGER.".miscId";}
elseif($purchaseType=='t'){ $purchaseSelect = "select  ". TABLE_INVOICE_TRANSPORT.".netAmount,
	". TABLE_INVOICE_TRANSPORT.".invoiceNo,	
	". TABLE_INVOICE_TRANSPORT.".fare		
	from ". TABLE_INVOICE_TRANSPORT."
	where ". TABLE_INVOICE_TRANSPORT.".ID='$purchaseId' AND ". TABLE_INVOICE_TRANSPORT.".proId=$proId";}
//echo $purchaseSelect;
$purchaseResult 	= 	$db->query($purchaseSelect);
$purchaseRow 		= 	mysql_fetch_array($purchaseResult);
$invInvoiceNo		=	$purchaseRow['invoiceNo'];

$netAmount = $purchaseRow['netAmount'];
}
?>
<script>
	function getBasic(str) {
		//alert(str);
		type=document.getElementById("type").value;
		if (str.length == 0) { 
			document.getElementById("txtHint").innerHTML = "";
			return;
		} else {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					document.getElementById("basicAmount").value = xmlhttp.responseText;
					document.getElementById("netAmount").value = xmlhttp.responseText;
				}
			};
			xmlhttp.open("GET", "basic_ajax.php?q=" +str+"&type="+type, true);
			xmlhttp.send();
		}
	}
</script>

<script>
	function valid()
	{
		flag=false;
		proCharge=document.getElementById('proCharge').value;
		otherCharges= document.getElementById('otherCharges').value;
		tds =document.getElementById('tds').value;
		refundable=document.getElementById('refundable').value;
		taxInPer= document.getElementById('tax').value;
		tac= document.getElementById('tac').value;
		roundAmount=document.getElementById('roundAmount').value;
		netAmount=document.getElementById('netAmount').value;
		
		inNet   = document.getElementById('inNet').value;

                if(parseFloat(inNet)<parseFloat(netAmount))
		{
			var pursave = confirm("Purchase is in loss.! Do you want to continue.?");
			if(pursave==true)
			{
				window.submit();
			}
			else{
				flag=true;
			}
		}   
	
		if(isNaN(proCharge) || proCharge=="")
		{																			///for procharge
			document.getElementById('proChargeDiv').innerHTML="Enter number only.";
			flag=true;
		}
		if(isNaN(otherCharges) || otherCharges=="")
		{																			///for otherCharges
			document.getElementById('otherChargesDiv').innerHTML="Enter number only.";
			flag=true;
		}
		if(isNaN(tds) || tds=="")
		{																			///for tds
			document.getElementById('tdsDiv').innerHTML="Enter number only.";
			flag=true;
		}
		if(isNaN(refundable) || refundable=="")
		{																			///for refundable
			document.getElementById('refundableDiv').innerHTML="Enter number only.";
			flag=true;
		}	
		if(isNaN(taxInPer) || taxInPer=="")
		{																			///for taxInPer
			document.getElementById('taxInPerDiv').innerHTML="Enter number only.";
			flag=true;
		}
		if(isNaN(tac) || tac=="")
		{																			///for taxInPer
			document.getElementById('tacDiv').innerHTML="Enter number only.";
			flag=true;
		}
		if(isNaN(roundAmount) || roundAmount=="")
		{																			///for roundAmount
			document.getElementById('roundAmountDiv').innerHTML="Enter number only.";
			flag=true;
		}
		if(isNaN(netAmount) || netAmount=="")
		{																			///for netAmount
			document.getElementById('netAmountDiv').innerHTML="Enter number only.";
			flag=true;
		}

                var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		flag=true;
		}


		if(flag==true)
		{
			return false;
		}
	
																				
	}
	//clear the validation msg
	function clearbox(Element_id)
	{
		document.getElementById(Element_id).innerHTML="";
	}

</script>
<div class="row">
	<div class="col-lg-12">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form method="post" action="do.php?op=edit" class="default_form" id="purchase_form" onsubmit="return valid()" >
			
			<input type="hidden" value="<?php echo $netAmount; ?>" name="inNet" id="inNet">
			
				<?php
				//get purchase details
				if($purchaseType=='a'){ @$purExe	=	"SELECT * FROM `" . TABLE_PURCHASE_AIRLINE . "` WHERE ourInvoiceNo='$invInvoiceNo' and proId=$proId and finYear=$finYear";?>
					<input type="hidden" name="type" id="type" value="a"><?php }	
		
				elseif($purchaseType=='v'){ @$purExe	=	"SELECT * FROM `" . TABLE_PURCHASE_VISA . "` WHERE ourInvoiceNo='$invInvoiceNo' and proId=$proId and finYear=$finYear"; ?>
					<input type="hidden" name="type" id="type" value="v"><?php }
				
				elseif($purchaseType=='h'){ @$purExe	=	"SELECT * FROM `" . TABLE_PURCHASE_HOTEL . "` WHERE ourInvoiceNo='$invInvoiceNo' and proId=$proId and finYear=$finYear";  ?>
					<input type="hidden" name="type" id="type" value="h"><?php }
			
				elseif($purchaseType=='m'){ @$purExe	=	"SELECT * FROM `" . TABLE_PURCHASE_MISC . "` WHERE ourInvoiceNo='$invInvoiceNo' and proId=$proId and finYear=$finYear"; ?>
					<input type="hidden" name="type" id="type" value="m"><?php }
			
				elseif($purchaseType=='t'){ @$purExe	=	"SELECT * FROM `" . TABLE_PURCHASE_TRANSPORT . "` WHERE ourInvoiceNo='$invInvoiceNo' and proId=$proId and finYear=$finYear";?>
					<input type="hidden" name="type" id="type" value="t"><?php }
				//echo $purExe;
				$purResult 			= 	$db->query($purExe);
				@$purNum			=	mysql_num_rows($purResult);
				if($purNum>0){
					$purRes			=	mysql_fetch_array($purResult);
				}
				?>
				<input type="hidden" name="editId" value="<?php echo($purRes['ID']) ?>">
				<div class="bd_panel_head">
					<h3>PURCHASE - <?php echo @$head;?></h3>
				</div>
				<div class="bd_panel_body">
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
							<div class="form_block">
								<label>Voucher No <span class="valid">*</span></label>            
								<input type="text" name="invoiceNo" value="<?php echo $prefix.$purRes['invoiceNo']; ?>" readonly="" required="">                                
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Voucher Date <span class="valid">*</span></label>
								<input type="text" name="invoiceDate" class="user_date" value="<?php echo $App->dbFormat_date($purRes['invoiceDate']); ?>" required="">
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Bill No</label>
								<input type="text" name="billNo" value="<?php echo $purRes['billNo']; ?>">
							</div>
						</div>
						<input type="hidden" name="entryDate" class="user_date" value="<?php echo date('d-m-Y') ?>" required="">
						<!--<div class="col-lg-2 col-md-2 col-sm-2">
						<div class="form_block">
						<label>Entry Date</label>
						<input type="text" name="entryDate" class="user_date" value="<?php echo date('d-m-Y') ?>" required="">
						</div>
						</div>-->
						<!--<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
						<div class="form_block">
						<label>Purchase Type</label>-->
						<input type="hidden" name="purchaseType" value="<?php if($purchaseType=='a'){ echo('Airline');}
						elseif($purchaseType=='v'){ echo('Visa');}
						elseif($purchaseType=='h'){ echo('Hotel');}
						elseif($purchaseType=='t'){ echo('Transportation');}
						elseif($purchaseType=='m'){ echo('Misc');}	
						?>" readonly="">                                	                                		                               	                              
						<!--</div>
						</div>-->
                                                    
                          
						<!-- <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">                           
						<div class="form_block">
						<label>Expense Account</label> -->                            
						<?php
						if($purchaseType=='a'){ $select 	= 	"select  ID,accountName from ". TABLE_ACCOUNTS." where ID='5'";}
						elseif($purchaseType=='v'){ $select 	= 	"select  ID,accountName from ". TABLE_ACCOUNTS." where ID='7'";}						
						elseif($purchaseType=='h'){ $select 	= 	"select  ID,accountName from ". TABLE_ACCOUNTS." where ID='11'";}
						elseif($purchaseType=='t'){ $select 	= 	"select  ID,accountName from ". TABLE_ACCOUNTS." where ID='13'";}
						elseif($purchaseType=='m'){ $select 	= 	"select  ID,accountName from ". TABLE_ACCOUNTS." where ID='15'";}
                            	
						$result 	= 	$db->query($select);							 	
						$row=mysql_fetch_array($result);
						?>
						<input type="hidden" name="expAccountId" required="" value="<?php echo $row['ID'] ?>">
								
                           
						<!--</div>
						</div>-->
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">                      
							<div class="form_block">
								<label>Against our voucher <span class="valid">*</span></label>
								<input type="text" name="ourInvoiceNo" id="ourInvoiceNo"  required="" <?php if(isset($_REQUEST['pId'])){?>  value="<?php echo $prefix2.$purRes['ourInvoiceNo']; ?>" readonly="" <?php } else {?> onkeyup="getBasic(this.value)" <?php }?>>
								<div id="voucherNoDiv" class="valid"></div>
							</div>
						</div>                         
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">    
							<div class="form_block">
								<label>Basic Amount<span class="valid">*</span></label>
								<input type="text" data-action="live_net" name="basicAmount" id="basicAmount"
								<?php if(isset($_REQUEST['pId'])&& $purchaseType=='a'){  
									$tot=$purchaseRow['fSum']+$purchaseRow['tSum']+$purchaseRow['ySum'];			
									?>										
   																								
									value="<?php echo $tot; ?>"  <?php
								}
								else if(isset($_REQUEST['pId'])&& $purchaseType=='v'){  
									$tot=$purchaseRow['amountSum'];	   																		?>										
   																								
									value="<?php echo $tot; ?>" <?php
								}
								else if(isset($_REQUEST['pId'])&& $purchaseType=='h'){  
									$tot=$purchaseRow['netAmountSum'];	   											
									?>										
   																								
									value="<?php echo $tot; ?>"  <?php 
								}
								else if(isset($_REQUEST['pId'])&& $purchaseType=='m'){  
									$tot=$purchaseRow['sumBasic'];	   											
									?>										
   																								
									value="<?php echo $tot; ?>"  <?php 
								}
								else if(isset($_REQUEST['pId'])&& $purchaseType=='t'){  
									$tot=$purchaseRow['fare'];	   											
									?>										 					
									value="<?php echo $tot; ?>"  <?php 
								} ?> required="" readonly="">
							</div>
						</div> 
                         
                          
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">   
							<div class="form_block">
								<label>Service Tax Reg No</label>
								<input type="text" name="taxRegNo" id="taxRegNo" value="<?php echo $purRes['taxRegNo']; ?>">
							</div>
						</div>
					</div>
					<div class="row">                        
                                                  
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">                           
							<div class="form_block">
								<label>Supplier <span class="valid">*</span></label>
								<select name="supplierId" required="">
									<option value="">Select</option>
									<?php
									$select 	= 	"select  ID,accountName from ". TABLE_ACCOUNTS." where accountGroupId='40' AND proId=$proId ORDER BY accountName";
									$result 	= 	$db->query($select);							 	
									while($row=mysql_fetch_array($result)){
										?>
										<option value="<?php echo $row['ID'] ?>" <?php if($purRes['supplierId']==$row['ID']){ echo("selected"); } ?>><?php echo $row['accountName'] ?></option>
										<?php	
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row"> 
							<div class="form_block">
								<label>Proc Charge</label>
								<input type="text" data-action="live_net" name="proCharge" id="proCharge"  value="<?php echo $purRes['proCharge']; ?>" onfocus="clearbox('proChargeDiv')">
								<div  id="proChargeDiv" class="valid"></div>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">                            
							<div class="form_block">
								<label>Other Charges</label>
								<input type="text" name="otherCharges" data-action="live_net" id="otherCharges"  value="<?php echo $purRes['otherCharges']; ?>" onfocus="clearbox('otherChargesDiv')">
								<div  id="otherChargesDiv" class="valid"></div>                                
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
							<div class="form_block">
								<label>TDS</label>
								<input type="text" name="tds" id="tds" data-action="live_net"  value="<?php echo $purRes['tds']; ?>" onfocus="clearbox('tdsDiv')">
								<div  id="tdsDiv" class="valid"></div>                                
							</div>                            
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
							<div class="form_block">
								<label>Refundable</label>
								<input type="text" name="refundable" id="refundable" data-action="live_net"  value="<?php echo $purRes['refundable']; ?>" onfocus="clearbox('refundableDiv')">
								<div  id="refundableDiv" class="valid"></div>                                
							</div>
						</div>
                        
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
							<div class="form_block">
								<label>Service Tax & Cess Amount</label>
								<input type="text" name="tax" id="tax" data-action="live_net"  value="<?php echo $purRes['taxInPer']; ?>" onfocus="clearbox('taxInPerDiv')">
								<div  id="taxInPerDiv" class="valid"></div>
							</div>
						</div>
					</div>
					<div class="row">
                         
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>TAC</label>
								<input type="text" name="tac" data-action="live_net"   id="tac"  value="<?php echo $purRes['tac']; ?>" required="" onfocus="clearbox('tacDiv')">
								<div  id="tacDiv" class="valid"></div>  
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
							<div class="form_block">
								<label>Rounded off</label>
								<input type="text" name="roundAmount" id="roundAmount" data-action="live_net"  value="<?php echo $purRes['roundAmount']; ?>" onfocus="clearbox('roundAmountDiv')">
								<div  id="roundAmountDiv" class="valid"></div>                               
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
							<div class="form_block">
								<label>Net Amount <span class="valid">*</span></label>
								<input type="text" name="netAmount" id="netAmount" required="" onfocus="clearbox('netAmountDiv')"  value="<?php if(isset($_REQUEST['pId'])){  echo $tot+$purRes['proCharge']+$purRes['otherCharges']+$purRes['tds']+$purRes['refundable']+$purRes['taxInPer']+$purRes['roundAmount']-$purRes['tac'];}?>" readonly="">
								<div  id="netAmountDiv" class="valid"></div>                               
							</div>
						</div>
                                 	
                    	
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
							<div class="form_block">
								<label>Remark</label>
								<textarea name="remark" id="remark"><?php echo $purRes['remark']; ?></textarea>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">   
							<div class="form_block">
								<label>Description</label>
								<textarea name="description" id="description"><?php echo $purRes['description']; ?></textarea>
							</div>
						</div> 
                         
					</div>
				</div>
				<div class="bd_panel_footer">
					<div class="panel_row">
						<div class="form_block_full">
							<input type="submit" name="form" value="OK">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
require('../admin_footer1.php');
?>
<script>
	$(function () {
		$('input').focus(function () {
			$('#netAmountDiv').empty();
		});	
	});
</script>
<?php
require('../admin_footer2.php');
?>
