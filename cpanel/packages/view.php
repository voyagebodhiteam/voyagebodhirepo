<?php
require("../admin_header.php");
if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';

$editId			=	$_REQUEST['id'];

if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"proId='$proId'";
}

$editSelect		=	"SELECT * FROM ".TABLE_PACKAGE." WHERE ID='$editId'";
$editResult 	= 	$db->query($editSelect);
$editRow 		= 	mysql_fetch_array($editResult);
$services 		= 	explode(",",$editRow['services']);
?>
<div class="bd_panel bd_panel_default bd_panel_shadow">
    <form class="default_form"  id="package_reg_form">
        <div class="bd_panel_head">
            <h3><?php echo $editRow['packageName']; ?></h3>
        </div>
        <div class="bd_panel_body">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Package Name</label>
                        <input name="packageName" type="text" value="<?php echo $editRow['packageName']; ?>" disabled="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2" style="display: none">
                    <div class="form_block">
                        <label>Staff in Charge</label>
                        <select name="staff" disabled="">
                            <option value="">Select</option>
                            <?php
                            $staffQry = mysql_query("SELECT * FROM " . TABLE_STAFF . " ");
                            while ($staffRow = mysql_fetch_array($staffQry)) {
                                ?>
                                <option
                                    value="<?php echo $staffRow['ID']; ?>" <?php if($staffRow['ID']==$editRow['staff']){echo "selected";}?>><?php echo $staffRow['staffName']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Leader</label>
                        <input type="text" name="leader" value="<?php echo $editRow['leader']; ?>" disabled="">
                    </div>
                </div>
            
            	<div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Mobile</label>
                        <input type="text" name="mobile" value="<?php echo $editRow['mobile']; ?>" disabled="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Departure Date</label>
                        <input type="text" name="date" class="user_date" value="<?php echo $App->dbformat_date($editRow['date']); ?>" disabled="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Details</label>
                        <input type="text" name="place" value="<?php echo $editRow['place']; ?>" disabled="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Duration</label>
                        <input type="text" name="duration" value="<?php echo $editRow['duration']; ?>" disabled="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Remark</label>
                        <input type="text" name="premark" value="<?php echo $editRow['remark']; ?>" disabled="">
                    </div>
                </div>
               
            </div>
            
            
            
            <div class="form_section_head"><h3>Services</h3></div>
            <div class="package_serv_controls">
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_airline" name="airline" <?php if(in_array('airline',$services)){echo "checked=checked";}?> disabled=""><label
                        class="check_label">Airline</label>
                </div>
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_hotel" name="hotel" <?php if(in_array('hotel',$services)){echo "checked=checked";}?> disabled=""><label
                        class="check_label">Hotel</label>
                </div>
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_visa" name="visa" <?php if(in_array('visa',$services)){echo "checked=checked";}?> disabled=""><label
                        class="check_label">Visa</label>
                </div>
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_transportation" name="transport" <?php if(in_array('transportation',$services)){echo "checked=checked";}?> disabled=""><label
                        class="check_label">Transportation</label>
                </div>
            </div>
            <div id="package_dynamic">
            <!----------------------------------Airline---------------------------------->
				<?php if(in_array('airline',$services))
				{
					$airQry		=	mysql_query("SELECT a1.yq,
														a1.procCharge,
														a1.tax,
														a1.adultCharge,
														a1.childCharge,
														a1.infantCharge,
														a1.adultTotal,
														a1.childTotal,
														a1.infantTotal,
														a1.remark,
														a2.supplierId,
														a2.adultTotal as a2adultTotal,
														a2.childTotal as a2childTotal,
														a2.infantTotal as a2infantTotal
												   FROM ".TABLE_PACKAGE_AIRLINE." as a1 ,".TABLE_PURCHASE_PAC_AIRLINE." as a2
												  WHERE a1.packageId=$editId and a2.packageId=$editId");
					
					$airRow		=	mysql_fetch_array($airQry);
					?>
					<div class="package_dynamic_box pack_dynamic_airline">
				        <h3 class="pack_dynamic_head">Airline</h3>
				        <div class="pack_air_table_wrap">
				       
				            <table class="table table-bordered bd_table ">
				                <thead>
				                <tr class="table_head">
				                    <th colspan="6">Airline Details</th>
				                    <th colspan="4">
				                        <!--<button class="bd_btn bd_btn_blue add_package_airline_reg_row" type="button">
				                            <i class="ion ion-plus"></i>Add New
				                        </button>-->
				                    </th>
				                </tr>
				                <tr>
				                    <th>Sl No</th>
				                    <th>Airline</th>
				                    <th>Flight No</th>
				                    <th>Journey Date</th>
				                    <th>Time</th>
                                                    <th>Arrival Date</th>
                    				<th>Time</th>
				                    <th>Sector From</th>
				                    <th>Sector To</th>
				                    <th>Class</th>
				                  
				                </tr>
				                </thead>
				                <tbody>
				                <?php 
				                $m = 1;
				                $flightQry	= mysql_query("SELECT * FROM ".TABLE_PACKAGE_AIRLINE_FLIGHTS." WHERE packageId=$editId");
				                if(mysql_num_rows($flightQry)>0)
				                {
				                while($flightRows	= mysql_fetch_array($flightQry))
				                {
				                ?>
				                <tr>
				                    <td><?php echo $m;$m++;?></td>
				                    <td>
				                        <select name="airlineName_1" disabled="">
				                            <option value="">Select</option>
				                            <?php
				                            $airQry1 = mysql_query("select ID,flightName FROM " . TABLE_FLIGHT . " ");
				                            while ($airRow1 = mysql_fetch_array($airQry1)) {
				                                ?>
				                                <option
				                                    value="<?php echo $airRow1['ID']; ?>" <?php if($flightRows['airline']==$airRow1['ID']){echo "selected";}?>><?php echo $airRow1['flightName']; ?></option>
				                                <?php
				                            }
				                            ?>
				                        </select>
				                    </td>
				                    <td>
				                        <input type="text" name="flightNo_1" value="<?php echo $flightRows['flightNo'];?>" disabled="">
				                    </td>
				                    <td>
				                        <input type="text" data-dp="date_pick" name="airJDate_1" value="<?php echo $App->dbformat_date($flightRows['jDate']);?>" disabled="">
				                    </td>
				                    <td>
				                        <input type="text" name="airTime_1" value="<?php echo $flightRows['airTime'];?>" disabled="" >
				                    </td>
                                                    <td>
				                        <input type="text" data-dp="date_pick" name="airADate_1" value="<?php echo $App->dbformat_date($flightRows['aDate']);?>" disabled="">
				                    </td>
				                    <td>
				                        <input type="text" name="airATime_1" value="<?php echo $flightRows['aTime'];?>" disabled="" >
				                    </td>
				                    <td>
				                        <input type="text" name="sectorFrom_1" value="<?php echo $flightRows['sectorFrom'];?>" disabled="">
				                    </td>
				                    <td>
				                        <input type="text" name="sectorTo_1" value="<?php echo $flightRows['sectorTo'];?>" disabled="">
				                    </td>
				                    <td>
				                        <input type="text" name="class_1"  value="<?php echo $flightRows['class'];?>" disabled="">
				                    </td>
				                   
				                </tr>
				                <?php }
				                }
				                ?>
				                </tbody>
				            </table>
				            
				        </div>
				        
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Remarks</label>
				                    <input type="text" name="airRemark"  value="<?php echo $airRow['remark']; ?>" disabled="" >
				                </div>
				            </div>
				        </div>
				        <h3>Total</h3>
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Processing Charge</label>
				                    <input type="text" name="airProCharge" value="<?php echo $airRow['procCharge']; ?>" disabled="">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Tax</label>
				                    <input type="text" name="airTax" value="<?php echo $airRow['tax']; ?>" disabled="">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Yq</label>
				                    <input type="text" name="airYq" value="<?php echo $airRow['yq']; ?>" disabled="">
				                </div>
				            </div>
				        </div>
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Adult charge</label>
				                    <input type="text" name="adultChargeAir" value="<?php echo $airRow['adultCharge']; ?>" disabled="">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Child charge</label>
				                    <input type="text" name="childChargeAir" value="<?php echo $airRow['childCharge']; ?>" disabled="">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Infant charge</label>
				                    <input type="text" name="infantChargeAir" value="<?php echo $airRow['infantCharge']; ?>" disabled="">
				                </div>
				            </div>
				        </div>
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Adult Total</label>
				                    <input type="text" name="adultTotalAir" value="<?php echo $airRow['adultTotal']; ?>" disabled="">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Child Total</label>
				                    <input type="text" name="childTotalAir" value="<?php echo $airRow['childTotal']; ?>" disabled="">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Infant Total</label>
				                    <input type="text" name="infantTotalAir" value="<?php echo $airRow['infantTotal']; ?>" disabled="">
				                </div>
				            </div>
				        </div>
				        <h3>Purchase</h3>
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Supplier</label>
				                    <select name="supplierIdAir" disabled="">
				                        <option value="">Select</option>
				                        <?php
				                        $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' ";
				                        $result = $db->query($select);
				                        while ($row = mysql_fetch_array($result)) {
				                            ?>
				                            <option
				                                value="<?php echo $row['ID'] ?>" <?php if($row['ID']==$airRow['supplierId']){echo "selected";}?>><?php echo $row['accountName'] ?></option>
				                            <?php
				                        }
				                        ?>
				                    </select>
				                </div>
				            </div>
				        </div>
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Adult Total</label>
				                    <input type="text" name="prAirAdult" value="<?php echo $airRow['a2adultTotal'];?>" disabled="">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Child Total</label>
				                    <input type="text" name="prAirChild" value="<?php echo $airRow['a2childTotal'];?>" disabled="">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Infant Total</label>
				                    <input type="text" name="prAirInfant" value="<?php echo $airRow['a2infantTotal'];?>" disabled="">
				                </div>
				            </div>
				        </div>
				    </div>
				<?php
				}?>	
			<!----------------------------------Hotel---------------------------------->
				<?php if(in_array('hotel',$services))
				{
					$hotQry		=	mysql_query("SELECT h1.ID,
														h1.hotelId,
														h1.checkinDate,
														h1.checkoutDate,
														h1.noOfDays,
														h1.supplierId
												   FROM ".TABLE_PACKAGE_HOTEL." as h1
												  WHERE h1.packageId=$editId");

					?>
					<div class="package_dynamic_box pack_dynamic_hotel">
				    <h3 class="pack_dynamic_head">Hotel</h3>
				        <div class="pack_hotel_table_wrap">
				            <table class="table table-bordered bd_table hotel_outer_table">
				                <thead>
				                <tr class="table_head">
				                    <th colspan="7">Hotel Details</th>
				                    <th colspan="2">
				                        
				                    </th>
				                </tr>
				                <tr>
				                    <th>Sl No</th>
				                    <th>Hotel</th>
				                    <th>Check In Date</th>
				                    <th>Check Out Date</th>
				                    <th>Number of Days</th>
				                    <th>Supplier</th>
				                    <th colspan="3">Room Details</th>
				                </tr>
				                </thead>
				                <tbody>
				                <?php while($hotRow	=mysql_fetch_array($hotQry)) { 
				                $package_hotelId	=	$hotRow['ID'];
				                ?>
				                    <tr>
				                        <td>1</td>
				                        <td>
				                            <select name="hot1_hotelName" disabled="">
				                                <option value="">Select</option>
				                                <?php
				                                $hotelQry = mysql_query("SELECT ID,hotelName FROM " . TABLE_HOTEL . " ");
				                                while ($hotelRow = mysql_fetch_array($hotelQry)) {
				                                    ?>
				                                    <option
				                                        value="<?php echo $hotelRow['ID'] ?>" <?php if($hotelRow['ID']==$hotRow['hotelId']){echo "selected";}?>><?php echo $hotelRow['hotelName'] ?></option>
				                                    <?php
				                                }
				                                ?>
				                            </select>
				                        </td>
				                        <td>
				                            <input type="text" name="hot1_hotelcheckinDate" data-dp="date_pick" value="<?php echo $App->dbformat_date($hotRow['checkinDate']);?>" disabled="">
				                        </td>
				                        <td>
				                            <input type="text" name="hot1_hotelcheckoutDate" data-dp="date_pick" value="<?php echo  $App->dbformat_date($hotRow['checkoutDate']);?>" disabled="">
				                        </td>
				                        <td>
				                            <input type="text" name="hot1_noOfDays" value="<?php echo  $hotRow['noOfDays'];?>" disabled="">
				                        </td>
				                        <td>
				                             <select name="hot1_supplierId" disabled="">
                                                <option value="">Select</option>
                                                <?php
                                                $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40'";
                                                $result = $db->query($select);
                                                while ($row = mysql_fetch_array($result)) {
                                                    ?>
                                                    <option
                                                        value="<?php echo $row['ID'] ?>" <?php if($row['ID']==$hotRow['supplierId']){echo "selected";}?>><?php echo $row['accountName'] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
				                        </td>
				                        <td colspan="3">
				                            <table class="table table-bordered bd_table hotel_inner_table">
				                                <thead>
				                                <tr>
				                                    <th>Room Type</th>
				                                    <th>Charge</th>
				                                    <th>Other Charges</th>
				                                    <th>Invoice Total</th>
                                   					<th>Purchase Total</th>
				                                </tr>
				                                </thead>
				                                <tbody>
				                                <?php $roomQry = mysql_query("SELECT h2.roomId,
																					h2.rate,
																					h2.otherCharge,
																					h2.total,
																					h2.purchaseAmount,
																					h3.roomType
																			   FROM ".TABLE_PACKAGE_HOTEL_ROOM_TYPE." as h2
																			   LEFT JOIN ".TABLE_ROOM_TYPE." as h3 ON h3.ID=h2.roomId
																			  WHERE h2.package_hotelId=$package_hotelId 
																			  ORDER BY h2.roomId asc ");
												$i=	0;
				                                while ($roomRow = mysql_fetch_array($roomQry)) {
				                                	$i++;
										        $constant	=	1;
												if($roomRow['roomId']==2)
												{
													$constant	=	2;
												}
												if($roomRow['roomId']==3)
												{
													$constant	=	3;
												}
												if($roomRow['roomId']==4)
												{
													$constant	=	4;
												}
												if($roomRow['roomId']==5)
												{
													$constant	=	5;
												}
												if($roomRow['roomId']==6)
												{
													$constant	=	6;
												}
												
												$hotRoomRate				=	$roomRow['rate']*$constant;	
												$hotRoomOtherCharge			=	$roomRow['otherCharge']*$constant;	
												$hotRoomTotal				=	$hotRoomRate+$hotRoomOtherCharge;	
												$hotRoomPurchaseAmount		=	$roomRow['purchaseAmount']*$constant;
				                                    ?>
				                                    <tr>
				                                        <td style="min-width: 85px;">
				                                            <?= $roomRow['roomType']; ?>
				                                            <input type="hidden" name="hot1_roomId_<?= $i; ?>" value="<?= $roomRow['roomId']; ?>" disabled="">
				                                        </td>
				                                        <td style="min-width: 85px; max-width: 85px;">
				                                            <input value="<?= $hotRoomRate; ?>" type="text" name="hot1_roomcharge_<?= $i; ?>" disabled="">
				                                        </td>
				                                        <td style="min-width: 85px; max-width: 85px;">
				                                            <input value="<?= $hotRoomOtherCharge; ?>" type="text" name="hot1_roomOtherCharge_<?= $i; ?>" disabled="">
				                                        </td>
				                                        <td style="min-width: 85px; max-width: 85px;">
				                                            <input value="<?= $hotRoomTotal; ?>" type="text" name="hot1_roomTotal_<?= $i; ?>" disabled="">
				                                        </td>
				                                        
				                                        <td style="min-width: 85px; max-width: 85px;">
				                                            <input value="<?= $hotRoomPurchaseAmount; ?>" type="text" name="hot1_purchaseTotal_<?= $i; ?>" disabled="">
				                                        </td>
				                                    </tr>
				                                    <?php
				                                    $i++;
				                                }
				                                ?>
				                                </tbody>
				                            </table>
				                        </td>
				                        
				                    </tr>
				                    <?php }?>
				                </tbody>
				            </table>
				        </div>
				    </div>
				<?php
				}?>	
			<!----------------------------------Visa---------------------------------->
				<?php if(in_array('visa',$services))
				{
					$visaQry	=	"SELECT v1.visaFee,
											v1.visaFor,
											v1.vfs,
											v1.ddCharge,
											v1.serviceCharge,
											v1.otherCharge,
											v1.courierCharge,
											v1.total,
											v1.supplierId as v2SupplierId,
											v1.purchaseAmount as v2Total
									   FROM ".TABLE_PACKAGE_VISA." AS v1
									  WHERE v1.packageId=$editId
									";
					$visaResult	= 	mysql_query($visaQry);
					?>
					<div class="package_dynamic_box pack_dynamic_visa">
			        <h3 class="pack_dynamic_head">Visa</h3>

			        <div class="pack_transportation_table_wrap">
			            <table class="table table-bordered bd_table ">
			                <thead>
			                <tr class="table_head">
			                    <th colspan="10">Visa Details</th>
			                    <th colspan="1">
			                        
			                    </th>
			                </tr>
			                <tr>
			                    <th>Sl No</th>
			                    <th>Visa For</th>
			                    <th>Visa Fee</th>
			                    <th>VFS</th>
			                    <th>DD Charge</th>
			                    <th>Service Charge</th>
			                    <th>Other Charge</th>
			                    <th>Courier Charge</th>
			                    <th>Total</th>
			                    <th>Supplier</th>
			                    <th>Purchase Amt</th>
			                </tr>
			                </thead>
			                <tbody>
			                <?php 
			                $i	=	0;
			                while($visaRow	=	mysql_fetch_array($visaResult)){?>
			                <tr>
			                    <td><?php echo ++$i;?></td>
			                     <td>
			                        <select name="visaFor_1" disabled="">
			                        	<option value="">Select</option>
			                            <?php 
			                            $visaQry1	=	mysql_query("SELECT * FROM ".TABLE_VISAFOR." ");
			                            while($visaRow1	=	mysql_fetch_array($visaQry1))
			                            {
			                            ?>
										<option value="<?php echo $visaRow1['ID']; ?>" <?php if($visaRow1['ID']==$visaRow['visaFor']){ echo "selected";}?>><?php echo $visaRow1['visafor'];?></option>
										<?php }?>
			                        </select>
			                    </td>
			                    <td>
			                        <input type="text" name="visaFee_1" value="<?php echo $visaRow['visaFee'];?>" disabled="">
			                    </td>
			                    <td>
			                        <input type="text" name="vfs_1" value="<?php echo $visaRow['vfs'];?>" disabled="">
			                    </td>
			                    <td>
			                        <input type="text" name="ddCharge_1" value="<?php echo $visaRow['ddCharge'];?>" disabled="">
			                    </td>
			                    <td>
			                        <input type="text" name="serviceCharge_1" value="<?php echo $visaRow['serviceCharge'];?>" disabled="">
			                    </td>
			                    <td>
			                        <input type="text" name="visaOtherCharge_1" value="<?php echo $visaRow['otherCharge'];?>" disabled="">
			                    </td>
			                    <td>
			                        <input type="text" name="courierCharge_1" value="<?php echo $visaRow['courierCharge'];?>" disabled="">
			                    </td>
			                    <td>
			                        <input type="text" name="visaTotal_1" value="<?php echo $visaRow['total'];?>" disabled="">
			                    </td>
			                    <td>
			                        <select name="supplierIdVisa_1" disabled="">
			                            <option value="">Select</option>
			                            <?php
			                            $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' ";
			                            $result = $db->query($select);
			                            while ($row = mysql_fetch_array($result)) {
			                                ?>
			                                <option
			                                    value="<?php echo $row['ID'] ?>" <?php if($visaRow['v2SupplierId']==$row['ID']){ echo "selected";}?>><?php echo $row['accountName'] ?></option>
			                                <?php
			                            }
			                            ?>
			                        </select>
			                    </td>
			                    <td>
			                        <input type="text" name="prVisaTotal_1" value="<?php echo $visaRow['v2Total'];?>" disabled="">
			                    </td>
			                    
			                </tr>
			                <?php }?>
			                </tbody>
			            </table>
			        </div>
			    </div>
				<?php
				}?>	
			<!----------------------------------Transportation---------------------------------->
				<?php if(in_array('transportation',$services))
				{
					$transportQry	=	"SELECT t1.vehicle,
												t1.seatCharge,
												t1.remark,
												t1.supplierId,
												t1.purchaseAmount
										   FROM ".TABLE_PACKAGE_TRANSPORTATION." AS t1
										  WHERE	t1.packageId=$editId
										";
					$transportResult	=	mysql_query($transportQry);
					
					?>
					<div class="package_dynamic_box pack_dynamic_transportation">
			        <h3 class="pack_dynamic_head">Transportation</h3>

			        <div class="pack_transportation_table_wrap">
			            <table class="table table-bordered bd_table">
			                <thead>
			                <tr class="table_head">
			                    <th colspan="5">Transportation Details</th>
			                    <th colspan="1">
			                      
			                    </th>
			                </tr>
			                <tr>
			                    <th>Sl No</th>
			                    <th>Vehicle</th>
			                    <th>Seat Charge</th>
			                    <th>Remark</th>
			                    <th>Supplier</th>
			                    <th>Purchase Amt</th>
			                </tr>
			                </thead>
			                <tbody>
			                <?php
			                $j=0;
			                 while($transportRow=mysql_fetch_array($transportResult)){?>
			                <tr>
			                    <td><?php echo ++$j;?></td>
			                    <td>
			                        <select name="vehicle_1" disabled="">
			                            <?php
			                            $transQry = mysql_query("SELECT * FROM " . TABLE_VEHICLE_TYPES." ");
			                            while ($transRow = mysql_fetch_array($transQry)) {
			                                ?>
			                                <option
			                                    value="<?php echo $transRow['ID']; ?>" <?php if($transRow['ID']==$transportRow['vehicle']){echo "selected";}?>><?php echo $transRow['vehicleType']; ?></option>
			                            <?php } ?>
			                        </select>
			                    </td>
			                    <td>
			                        <input type="text" name="seatCharge_1" value="<?php echo $transportRow['seatCharge'];?>" disabled="">
			                    </td>
			                    <td>
			                        <input type="text" name="transRemark_1" value="<?php echo $transportRow['remark'];?>" disabled="">
			                    </td>
			                    <td>
			                        <select name="supplierIdTrans_1" disabled="">
			                            <option value="">Select</option>
			                            <?php
			                            $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' ";
			                            $result = $db->query($select);
			                            while ($row = mysql_fetch_array($result)) {
			                                ?>
			                                <option
			                                    value="<?php echo $row['ID'] ?>" <?php if($row['ID']==$transportRow['supplierId']){echo "selected";}?>><?php echo $row['accountName'] ?></option>
			                                <?php
			                            }
			                            ?>
			                        </select>
			                    </td>
			                    <td>
			                        <input type="text" name="prTransTotal_1" value="<?php echo $transportRow['purchaseAmount'];?>" disabled="">
			                    </td>
			                    
			                </tr>
			                <?php }?>
			                </tbody>
			            </table>
			        </div>
			    </div>
			
				<?php
				}?>
		   <!---------------------------------------- End --------------------------------------->

            </div>
            <div class="airline_submit">
                <input type="hidden" name="pack_reg_air_row_count" id="pack_reg_air_row_count" value="1">
                <input type="hidden" name="pack_reg_room_row_count" id="pack_reg_room_row_count" value="1">
				
            </div>
            <h3>Total</h3>
            <div class="row">
            	 <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>TDS</label>
                        <input type="text" name="ptds" value="<?php echo $editRow['tds']; ?>" disabled="">
                    </div>
                </div>
                
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Tax(Amount)</label>
                        <input type="text" name="pTax" value="<?php echo $editRow['tax']; ?>" disabled="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Other Charges</label>
                        <input type="text" name="pOtherCharges" value="<?php echo $editRow['otherCharge']; ?>" disabled="">
                    </div>
                </div>
           </div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Adult</label>
                        <input type="text" name="adultTotal" value="<?php echo $editRow['adultTotal']; ?>" disabled="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Child</label>
                        <input type="text" name="childTotal" value="<?php echo $editRow['childTotal']; ?>" disabled="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Infant</label>
                        <input type="text" name="infantTotal" value="<?php echo $editRow['infantTotal']; ?>" disabled="">
                    </div>
                </div>
                
            </div>
             <div class="row" style="margin-top: 30px;">
            	<div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Discount</label>
                        <input type="text" name="pDiscount" id="pDiscount"  disabled="" value="<?php echo $editRow['discount']; ?>" >
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Extra Charge</label>
                        <input type="text" name="pExtra" id="pExtra"  disabled=""  value="<?php echo $editRow['extra']; ?>" >
                    </div>
                </div>
            </div>
            <div>
            <input type="checkbox" disabled=""   name="confirm" <?php if($editRow['confirm']==1){ echo 'checked=""';}?> /> Confirm
            </div>
        </div>
    </form>
</div>
<?php
require('../admin_footer1.php');
?>
<div class="package_dynamic_source" style="display: none">
    <div class="package_dynamic_box pack_dynamic_airline">
        <h3 class="pack_dynamic_head">Airline</h3>
        <div class="pack_air_table_wrap">
            <table class="table table-bordered bd_table ">
                <thead>
                <tr class="table_head">
                    <th colspan="6">Airline Details</th>
                    <th colspan="2">
                        <button class="bd_btn bd_btn_blue add_package_airline_reg_row" type="button">
                            <i class="ion ion-plus"></i>Add New
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Sl No</th>
                    <th>Airline</th>
                    <th>Flight No</th>
                    <th>Journey Date</th>
                    <th>Time</th>
                    <th>Sector From</th>
                    <th>Sector To</th>
                    <th>Class</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>
                        <select name="airlineName_1">
                            <option value="">Select</option>
                            <?php
                            $airQry = mysql_query("select ID,flightName FROM " . TABLE_FLIGHT . " ");
                            while ($airRow = mysql_fetch_array($airQry)) {
                                ?>
                                <option
                                    value="<?php echo $airRow['ID']; ?>"><?php echo $airRow['flightName']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="flightNo_1">
                    </td>
                    <td>
                        <input type="text" data-dp="date_pick" name="airJDate_1">
                    </td>
                    <td>
                        <input type="text" name="airTime_1">
                    </td>
                    <td>
                        <input type="text" name="sectorFrom_1">
                    </td>
                    <td>
                        <input type="text" name="sectorTo_1">
                    </td>
                    <td>
                        <input type="text" name="class_1">
                    </td>
                    <td>
                        <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_airline_row">Delete</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Remarks</label>
                    <input type="text" name="airRemark">
                </div>
            </div>
        </div>
        <h3>Total</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Processing Charge</label>
                    <input type="text" name="airProCharge">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Tax</label>
                    <input type="text" name="airTax">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Yq</label>
                    <input type="text" name="airYq">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult charge</label>
                    <input type="text" name="adultChargeAir">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child charge</label>
                    <input type="text" name="childChargeAir">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Infant charge</label>
                    <input type="text" name="infantChargeAir">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult Total</label>
                    <input type="text" name="adultTotalAir">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child Total</label>
                    <input type="text" name="childTotalAir">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Infant Total</label>
                    <input type="text" name="infantTotalAir">
                </div>
            </div>
        </div>
        <h3>Purchase</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Supplier</label>
                    <select name="supplierIdAir">
                        <option value="">Select</option>
                        <?php
                        $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40'";
                        $result = $db->query($select);
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <option
                                value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult Total</label>
                    <input type="text" name="prAirAdult">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child Total</label>
                    <input type="text" name="prAirChild">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Infant Total</label>
                    <input type="text" name="prAirInfant">
                </div>
            </div>
        </div>
    </div>
    <div class="package_dynamic_box pack_dynamic_hotel">
        <h3 class="pack_dynamic_head">Hotel</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Hotel</label>
                    <select name="hotelName">
                        <option value="">Select</option>
                        <?php
                        $hotelQry = mysql_query("SELECT ID,hotelName FROM " . TABLE_HOTEL . " ");
                        while ($hotelRow = mysql_fetch_array($hotelQry)) {
                            ?>
                            <option
                                value="<?php echo $hotelRow['ID'] ?>"><?php echo $hotelRow['hotelName'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Check in Date</label>
                    <input type="text" name="hotelcheckinDate" class="user_date">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Check out Date</label>
                    <input type="text" name="hotelcheckoutDate" class="user_date">
                </div>
            </div>
        </div>
        <h3>Room Type</h3>
        <div class="pack_hotel_table_wrap">
            <table class="table table-bordered bd_table">
                <thead>
                <tr class="table_head">
                    <th colspan="2">Room Type</th>
                    <th colspan="2">
                        <button class="bd_btn bd_btn_blue add_package_hotel_reg_row" type="button">
                            <i class="ion ion-plus"></i>Add New
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Sl No</th>
                    <th>Room Type</th>
                    <th>Charge</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>
                        <select name="roomtype_1">
                            <?php 
                            $roomQry = mysql_query("select * FROM  " . TABLE_ROOM_TYPE." ORDER BY roomId ASC");
                            while ($roomRow = mysql_fetch_array($roomQry)) {
                            	
                                ?>
                                <option
                                    value="<?php echo $roomRow['ID']; ?>"><?php echo $roomRow['roomType']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="roomcharge_1">
                    </td>
                    <td>
                        <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_hotel_row">Delete</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="form_section_head"><h3>Total</h3></div>

        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Other Charges</label>
                    <input type="text" name="hotelOtherCharges">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Tax</label>
                    <input type="text" name="hotelTax">
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult Charge</label>
                    <input type="text" name="adultChargeHotel">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child With Bed</label>
                    <input type="text" name="childwbCharge">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child Without Bed</label>
                    <input type="text" name="childwobCharge">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult Total</label>
                    <input type="text" name="adultTotalHotel">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child With Bed Total</label>
                    <input type="text" name="childwbTotal">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child Without Bed Total</label>
                    <input type="text" name="childwobTotal">
                </div>
            </div>
        </div>
        <h3>Purchase</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Supplier</label>
                    <select name="supplierIdHotel">
                        <option value="">Select</option>
                        <?php
                        $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' ";
                        $result = $db->query($select);
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <option
                                value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult Total</label>
                    <input type="text" name="pradultTotal">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child With Bed</label>
                    <input type="text" name="prchildwbTotal">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child Withot Bed</label>
                    <input type="text" name="prchildwobTotal">
                </div>
            </div>
        </div>

    </div>
    <div class="package_dynamic_box pack_dynamic_visa">
        <h3 class="pack_dynamic_head">Visa</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Visa Fee</label>
                    <input type="text" name="visaFee">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>VFS</label>
                    <input type="text" name="vfs">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>DD Charge</label>
                    <input type="text" name="ddCharge">
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Service Charge</label>
                    <input type="text" name="serviceCharge">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Other Charge</label>
                    <input type="text" name="visaotherCharge">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Courier Charge</label>
                    <input type="text" name="courierCharge">
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Total</label>
                    <input type="text" name="visatotal">
                </div>
            </div>
        </div>
        <h3>Purchase</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Supplier</label>
                    <select name="supplierIdVisa">
                        <option value="">Select</option>
                        <?php
                        $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' ";
                        $result = $db->query($select);
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <option
                                value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Total</label>
                        <input type="text" name="prVisaTotal">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="package_dynamic_box pack_dynamic_transportation">
        <h3 class="pack_dynamic_head">Transportation</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Vehicle</label>
                    <select name="vehicle">
                        <?php
                        $transQry = mysql_query("SELECT * FROM " . TABLE_VEHICLE_TYPES." ");
                        while ($transRow = mysql_fetch_array($transQry)) {
                            ?>
                            <option
                                value="<?php echo $transRow['ID']; ?>"><?php echo $transRow['vehicleType']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Seat Charge</label>
                    <input type="text" name="seatCharge">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Remark</label>
                    <input type="text" name="transRemark">
                </div>
            </div>
        </div>
        <h3>Purchase</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Supplier</label>
                    <select name="supplierIdTrans">
                        <option value="">Select</option>
                        <?php
                        $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' ";
                        $result = $db->query($select);
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <option
                                value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Total</label>
                        <input type="text" name="prTransTotal">
                    </div>
                </div>
            </div>

        </div>

        <!-------------------------------- End ------------------------------------->
    </div>
</div>
<?php
require('../admin_footer2.php');
?>


