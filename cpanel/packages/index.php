<?php
require('../admin_header.php');
if($_SESSION['travelType']=='Admin')
{
	$check	=	"a.finYear='$finYear'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"a.proId='$proId' and a.finYear='$finYear'";
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
$rowsPerPage = ROWS_PER_PAGE;
?>
<script>
function delete_type()
{
var del=confirm("Related all data will be lose.Do you really want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>PACKAGE CREATION</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post">
                            <input type="search" name="sname" placeholder="Search here..." value="<?php if(@$_REQUEST['search']){echo @$_REQUEST['sname'];}?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                        </form>
                    </div>
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="add.php">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
            <?php
            $cond = 1;
            if(@$_REQUEST['sname'])
            {
				$search	 =	$_REQUEST['sname'];
				$cond	 =	$cond." and a.packageName LIKE '%".$search."%' ";
			}
            ?>
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Package Name</th>
                            <th>Details</th>
                            <th>Duration</th>
                            <th>Adult Charge</th>
                            <th>Services</th>
                            <th>Departure Date</th>
                            <th>Created by</th>
                            <th>Status</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                     //$i=0;
                        $selectAll = "SELECT a.ID,a.packageName,a.place,a.duration,a.adultTotal,a.services,
                        					 a.date,a.confirm,a.loginId,a.proId
                        				FROM ".TABLE_PACKAGE." a
                        				WHERE $cond
                                                        AND $check  
                        				ORDER BY ID DESC";
                        $result = $db->query($selectAll);
                        $selectCount	=	mysql_num_rows($result);
                        if($selectCount==0)
                        {
                        	?>
						 	<tr>
						 		<td colspan="10" align="center"> There is no data in list. </td>
						 	</tr>
						 	<?php
                        }
                        else
                        {
                        	/*********************** for pagination ******************************/
							
							if(isset($_GET['page']))
							{
								$pageNum = $_GET['page'];
							}
							else
							{
								$pageNum =1;
							}
							$offset = ($pageNum - 1) * $rowsPerPage;
							$select1=$db->query($selectAll." limit $offset, $rowsPerPage");
							$i=$offset+1;
							//use '$select1' for fetching
							/*************************** for pagination **************************/
							
	                        while ($row = mysql_fetch_array($select1)) 
	                        {
	                        	// for getting created by who
	                        	if($row['proId']=='0')
	                        	{
									$createdBy = 'Admin';
								}
								else
								{
									$createSql	= 	mysql_query("SELECT companyName FROM ".TABLE_COMPANY." WHERE ID=".$row['proId']."")	;
									$creatRow	=	mysql_fetch_array($createSql);
									$createdBy	=	$creatRow['companyName'];
								}
	                            ?>
	                         <tr>                            
	                            <td><?php echo $i++; ?></td>
	                            <td><?php echo $row['packageName']; ?></td>
	                            <td><?php echo $row['place']; ?></td>
	                            <td><?php echo $row['duration']; ?></td>
	                            <td><?php echo $row['adultTotal']; ?></td>
	                            <td><?php echo $row['services']; ?></td>
	                            <td><?php echo $App->dbformat_date($row['date']); ?></td>
	                            <td><?php echo $createdBy; ?></td>
	                            <td><?php if($row['confirm']==1){?>
		                            <span style="color: #5cb85c;" > <?php echo 'Confirm'; ?></span>
		                            <?php  } else { ?>
		                            <span style="color: #d9534f;" ><?php echo 'Not Confirm';} ?></span>
	                            </td>
	                            <td>
	                                <a class="show_table_lnk show_table_lnk_view" href="view.php?id=<?php echo $row['ID'];?>">View</a>
	                                
	                                 <?php   
	                                 $today	=	date('Y-m-d'); 
	                                if(($row['date']>$today) && ($row['confirm']==0) && ($row['proId']==$proId || $_SESSION['travelType']=='Admin') )
	                                {
	                                ?>
	                                <a class="show_table_lnk show_table_lnk_edit" href="edit.php?id=<?php echo $row['ID'];?>">Edit</a>
	                                <?php
									}else{?>
									 <a href="javascript:void(0)" class="bd_lnk bd_lnk_disabled">Edit</a>
	                                <?php } 
	                                if($row['proId']==$proId || $_SESSION['travelType']=='Admin')
	                                {
	                                ?>
	                                <a class="show_table_lnk show_table_lnk_del" href="do.php?id=<?php echo $row['ID'];?>&op=delete" onclick="return delete_type();">Delete</a>
	                                <?php } else {?>
	                                <a class="bd_lnk bd_lnk_disabled" href="javascript:void(0)">Delete</a>
	                                <?php } ?>
	                            </td>
	                        </tr>
	                            <?php
	                        }
	                     }
                        ?>
                    </tbody>
                </table>
            </div>
             <!--*****************************************************************-->
            
            	 <?php 
                  if(@$selectCount>@$rowsPerPage)
					{
					?>	
					 <br />	
					  <div class="pagerSC" align="center">
					<?php
					
					$query   =  $db->query($selectAll);
					$numrows = mysql_num_rows($query);
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
					   if ($page == $pageNum)
					   {
						  $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
					   }
					   else
					   {
						 	if(@$search)
					   		 {
							 	$nav .= " <a href=\"$self?page=$page&sname=$search\">$page</a> ";
							 }
							 else
							 {
							 	$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							 }
					   }
					}
					?>
					 <?php
					if ($pageNum > 1)
					{
					   $page  = $pageNum - 1;
					   if(@$search)
					   {
						   $prev  = " <a href=\"$self?page=$page&sname=$search\">Prev</a> ";
						   $first = " <a href=\"$self?page=1&sname=$search\">First Page</a> ";
					   }
					   else
					   {
						   $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
						   $first = " <a href=\"$self?page=1\">First Page</a> ";
					   }
					}
					else
					{
					   $prev  = '&nbsp;';
					   $first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
					   $page = $pageNum + 1;
					   if(@$search)
					   {
						   	 $next = " <a href=\"$self?page=$page&sname=$search\">Next</a> ";
						     $last = " <a href=\"$self?page=$maxPage&sname=$search\">Last Page</a> ";
					   }
					   else
					   {
						   	 $next = " <a href=\"$self?page=$page\">Next</a> ";
						   	 $last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
					   }
					}
					else
					{
					   $next = '&nbsp;';
					   $last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>