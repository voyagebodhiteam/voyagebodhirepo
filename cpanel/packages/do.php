<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId		=	$_SESSION['proId'];
$loginId	=	$_SESSION['travelId'];
$loginType	=	$_SESSION['travelType'];
$finYear	=	$_SESSION['finYear'];

if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$check 	= 	" proId='$proId'";
}

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
	 case 'index':
	 				
    		$returnVar=addData();

			if ($returnVar) 
            {		                  
                $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
            } 
            else 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again."); 
            }	
			header("location:index.php");
	 break;
	 
	 // edit
	 case 'edit':
	 			$editId		=	$_REQUEST['editId'];
	 		
	 			$db			= 	new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        		$db->connect();
        		        			
 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_AIRLINE."` WHERE packageId='$editId' AND $check");
 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_AIRLINE_FLIGHTS."` WHERE packageId='$editId' AND $check ");
 				@mysql_query("DELETE FROM `".TABLE_PURCHASE_PAC_AIRLINE."` WHERE packageId='$editId' AND $check ");
 				
 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_HOTEL."` WHERE packageId='$editId'");
 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_HOTEL_ROOM_TYPE."` WHERE packageId='$editId' AND $check ");
 				
 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_VISA."` WHERE packageId='$editId'  AND $check");
 				
 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_TRANSPORTATION."` WHERE packageId='$editId' AND $check");
 				
 				//@mysql_query("DELETE FROM `".TABLE_PACKAGE."` WHERE ID='$editId' AND proId=$proId ");
 				
 				$db->close();
				//echo $editId;die;
	 			$returnVar	=	addData($editId);
	 			if ($returnVar) 
                {		                  
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully"); 
                } 
                else 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again."); 
                }	
    			header("location:index.php");
	 			
	 break;
	 case 'delete':
	 				$deleteId 	= 	$_REQUEST['id'];
	 				
	 				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        			$db->connect();
        			
    				$invoiceNoQry	=	mysql_query("SELECT invoiceNo,ID FROM ".TABLE_INVOICE_PACKAGE." WHERE packageId= $deleteId AND $check ");
    				$invoiceNo1 	=	array();
    				$in_packageId1	=	array();
				   	while($invoiceNoRow	=	mysql_fetch_array($invoiceNoQry))
				   	{
				   		array_push($invoiceNo1,$invoiceNoRow['invoiceNo']);
				   		array_push($in_packageId1,$invoiceNoRow['ID']);
					}
					
					$invoiceNo	=	implode(",",$invoiceNo1);
					
					$in_packageId	=	implode(",",$in_packageId1);
					/************************ Reciept andpayment tables*****************************/
					@mysql_query("DELETE FROM `".TABLE_CASH_RECEIPT."` WHERE invoiceNo in ($invoiceNo) AND voucherType like '%package%' AND $check");
					
        			/************************ invoice package tables*****************************/
        			@mysql_query("DELETE FROM `".TABLE_TRANSACTION."` WHERE referenceNo in ($invoiceNo) AND voucherType like '%Package%' AND $check");
		   			
					@mysql_query("DELETE FROM `" . TABLE_INVOICE_PACKAGE_AIRLINE. "` WHERE in_packageId in ($in_packageId) AND $check ");
					@mysql_query("DELETE FROM `" . TABLE_PURCHASE_PACKAGE_AIRLINE. "` WHERE in_packageId in ($in_packageId) AND $check ");
					
					@mysql_query("DELETE FROM `" . TABLE_INVOICE_PACKAGE_HOTEL. "` WHERE in_packageId in ($in_packageId) AND $check ");
					@mysql_query("DELETE FROM `" . TABLE_PURCHASE_PACKAGE_HOTEL. "` WHERE in_packageId in ($in_packageId) AND $check ");
					
					@mysql_query("DELETE FROM `" . TABLE_INVOICE_PACKAGE_TRANSPORTATION. "` WHERE in_packageId in ($in_packageId)' AND $check ");
					@mysql_query("DELETE FROM `" . TABLE_PURCHASE_PACKAGE_TRANSPORT. "` WHERE in_packageId in ($in_packageId) AND $check ");
					
					@mysql_query("DELETE FROM `" . TABLE_INVOICE_PACKAGE_VISA. "` WHERE in_packageId in ($in_packageId) AND $check ");
					@mysql_query("DELETE FROM `" . TABLE_PURCHASE_PACKAGE_VISA. "` WHERE in_packageId in ($in_packageId) AND $check");
					
					@mysql_query("DELETE FROM `".TABLE_INVOICE_PACKAGE_PASSENGER."` WHERE packageId='{$deleteId}' AND $check");
					
					@mysql_query("DELETE FROM `" . TABLE_INVOICE_PACKAGE. "` WHERE packageId='{$deleteId}' AND $check");
        			/************************ Package tables *****************************/
        			
	 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_AIRLINE."` WHERE packageId='$deleteId' AND $check ");
	 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_AIRLINE_FLIGHTS."` WHERE packageId='$deleteId' AND $check");
	 				@mysql_query("DELETE FROM `".TABLE_PURCHASE_PAC_AIRLINE."` WHERE packageId='$deleteId' AND $check ");
	 				
	 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_HOTEL."` WHERE packageId='$deleteId' AND $check ");
	 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_HOTEL_ROOM_TYPE."` WHERE packageId='$deleteId' AND $check");
	 				
	 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_VISA."` WHERE packageId='$deleteId' AND $check ");
	 				
	 				@mysql_query("DELETE FROM `".TABLE_PACKAGE_TRANSPORTATION."` WHERE packageId='$deleteId' AND $check");
	 				
	 				$success	= 	mysql_query("DELETE FROM `".TABLE_PACKAGE."` WHERE ID='$deleteId' AND $check ");
	 				
	 				$db->close();
	 				if($success)
					{
						$_SESSION['msg'] = $App->sessionMsgCreate('success', "Details deleted successfully");
					}
					else
					{
						$_SESSION['msg'] = $App->sessionMsgCreate('error', "You can't delete this data.");
					}
					header("location:index.php");
	 break;
}


//add 
function addData($editId=0)
{
	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
    $db->connect();
    
    $App = new Application();
    global $proId;
    global $loginId;
    global $loginType;
    global $finYear;

    if($loginType=='Admin')
	{
		$loginName		=	"Admin";
	}
	/*if($loginType=='Staff')
	{
		$loginName1		=	"SELECT ".TABLE_STAFF.".staffName 
							   FROM ".TABLE_LOGIN." 
						  LEFT JOIN ".TABLE_STAFF." ON ".TABLE_STAFF.".ID=".TABLE_LOGIN.".userId
							  WHERE ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	$loginNameRow['staffName'];			  
	}*/
	if($loginType=='Branch')
	{
		$loginName1		=	"SELECT ".TABLE_COMPANY.".companyName 
							   FROM ".TABLE_LOGIN." 
						  LEFT JOIN ".TABLE_COMPANY." ON ".TABLE_COMPANY.".ID=".TABLE_LOGIN.".userId
							  WHERE ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	$loginNameRow['companyName'];			  
	}
    $packageId		=	$editId;
	$confirmQry 	= 	mysql_query("SELECT confirm,services,packageName,adultTotal,childTotal,infantTotal FROM ".TABLE_PACKAGE." WHERE ID=$packageId");
	$confirmRow		=	mysql_fetch_array($confirmQry);	
	$pacService		=	$confirmRow['services'];
	$pacServiceAry	=	explode(",",$pacService);
	$packageName	=	$confirmRow['packageName'];
	$adultPac		=	$confirmRow['adultTotal'];
	$childPac		=	$confirmRow['childTotal'];
	$infantPac		=	$confirmRow['infantTotal'];
	
	$confirm		=	$confirmRow['confirm'];
    $success = 0;
    if($_REQUEST['packageName'])
	{
		//  package master table
		
		$services	=	array();
		$data['packageName'] 	=	$App->capitalize($_REQUEST['packageName']);
		$data['staff']			=	$App->capitalize($_REQUEST['staff']);
		$data['leader']			=	$App->capitalize($_REQUEST['leader']);
		$data['mobile']			=	$App->convert($_REQUEST['mobile']);
		if($_REQUEST['date'])
		{
			$data['date']		=	$App->dbformat_date($_REQUEST['date']);
		}
		$data['place']			=	$App->convert($_REQUEST['place']);
		$data['duration']		=	$App->convert($_REQUEST['duration']);
		$data['tds']			=	$App->convert($_REQUEST['ptds']);
		$data['tax']			=	$App->convert($_REQUEST['pTax']);
		$data['otherCharge']	=	$App->convert($_REQUEST['pOtherCharges']);
		$data['remark']			=	$App->convert($_REQUEST['premark']);
		
		if(@$_REQUEST['airline'])
		{
			array_push($services,'airline');
		}
		if(@$_REQUEST['hotel'])
		{
			array_push($services,'hotel');
		}
		if(@$_REQUEST['visa'])
		{
			array_push($services,'visa');
		}
		if(@$_REQUEST['transport'])
		{
			array_push($services,'transportation');
		}
			
		$data['services']		=	implode(",",$services);
		$data['adultTotal']		=	$App->convert($_REQUEST['adultTotal']);
		$data['childTotal']		=	$App->convert($_REQUEST['childTotal']);
		$data['infantTotal']	=	$App->convert($_REQUEST['infantTotal']);
		if(isset($_REQUEST['confirm'])|| @$_REQUEST['confirm']=='1')
		{
			$data['confirm']		=	1;
		}
		else
		{
			$data['confirm']		=	0;
		}
		$data['loginId']		=	$loginId;
		$data['proId']			=	$proId;
                $data['finYear']		=	$finYear;
		if($editId!=0)
		{
			$db->query_update(TABLE_PACKAGE,$data , "ID=$editId");
			$success	=	$editId;
		}
		else
		{
			$success	=	$db->query_insert(TABLE_PACKAGE, $data);
		}
		
		if(@$_REQUEST['airline'])
		{
			$data1['packageId']		=	$success;
			$data1['yq']			=	$_REQUEST['airYq'];
			$data1['procCharge']	=	$_REQUEST['airProCharge'];
			$data1['tax']			=	$_REQUEST['airTax'];
			$data1['adultCharge']	=	$_REQUEST['adultChargeAir'];
			$data1['childCharge']	=	$_REQUEST['childChargeAir'];
			$data1['infantCharge']	=	$_REQUEST['infantChargeAir'];
			$data1['adultTotal']	=	$_REQUEST['adultTotalAir'];
			$data1['childTotal']	=	$_REQUEST['childTotalAir'];
			$data1['infantTotal']	=	$_REQUEST['infantTotalAir'];							
			$data1['remark']		=	$_REQUEST['airRemark'];
			$data1['loginId']		=	$loginId;
			$data1['proId']			=	$proId;
			
			$success1	=	$db->query_insert(TABLE_PACKAGE_AIRLINE,$data1);
			
			//for multi airlines to package_airline_flight   
			
			$airlinecount			=	$_REQUEST['pack_reg_air_row_count'];
			for($i=1;$i<=$airlinecount;$i++)
			{
				$airlineName 	=	'airlineName_'.$i;
				$flightNo 		=	'flightNo_'.$i;
				$sectorFrom 	=	'sectorFrom_'.$i;
				$sectorTo 		=	'sectorTo_'.$i;
				$airJDate 		=	'airJDate_'.$i;
				$airTime		=	'airTime_'.$i;
                $airADate		=	'airADate_'.$i;
				$airATime		=	'airATime_'.$i;
				$class 			=	'class_'.$i;
				
				$fdata1['packageId']			=	$success;
				$fdata1['package_airlineId']	=	$success1;
				$fdata1['airline']				=	$_REQUEST[$airlineName];
				$fdata1['flightNo']				=	$_REQUEST[$flightNo];
				$fdata1['sectorFrom']			=	$_REQUEST[$sectorFrom];
				$fdata1['sectorTo']				=	$_REQUEST[$sectorTo];
				if($_REQUEST[$airJDate])
				{
					$fdata1['jDate']			=	$App->dbformat_date($_REQUEST[$airJDate]);
				}
				$fdata1['airTime']			=	$_REQUEST[$airTime];
                                if($_REQUEST[$airADate])
				{
					$fdata1['aDate']			=	$App->dbformat_date($_REQUEST[$airADate]);
				}
				$fdata1['aTime']			=	$_REQUEST[$airATime];
				$fdata1['class']			=	$_REQUEST[$class];
				$fdata1['loginId']			=	$loginId;
				$fdata1['proId']			=	$proId;
				
				$fsuccess1		=	$db->query_insert(TABLE_PACKAGE_AIRLINE_FLIGHTS,$fdata1);
			}
			
			// airline purchase table

			$pdata1['packageId']	=	$success;
			$pdata1['supplierId']	=	$_REQUEST['supplierIdAir'];
			$pdata1['adultTotal']	=	$_REQUEST['prAirAdult'];
			$pdata1['childTotal']	=	$_REQUEST['prAirChild'];
			$pdata1['infantTotal']	=	$_REQUEST['prAirInfant'];
			$pdata1['loginId']		=	$loginId;
			$pdata1['proId']		=	$proId;
			
			$psuccess1	=	$db->query_insert(TABLE_PURCHASE_PAC_AIRLINE,$pdata1);
			
		}
		if(@$_REQUEST['hotel'] )
		{
			$hotelCount				=	$_REQUEST['pack_reg_hotel_row_count'];
			if($hotelCount>0)
			{
				for($h=1;$h<=$hotelCount;$h++)
				{
					$hotelName				=	'hot'.$h.'_hotelName';		
					$hotelcheckinDate		=	'hot'.$h.'_hotelcheckinDate';	
					$hotelcheckoutDate		=	'hot'.$h.'_hotelcheckoutDate';	
					$hotelnoOfDays			=	'hot'.$h.'_noOfDays';	
					$supplier				=	'hot'.$h.'_supplierId';
						
					$data2['packageId']		=	$success;
					$data2['hotelId']		=	$_REQUEST[$hotelName];
					$data2['noOfDays']		=	$_REQUEST[$hotelnoOfDays];
					$data2['supplierId']	=	$_REQUEST[$supplier];
					$data2['loginId']		=	$loginId;
					$data2['proId']			=	$proId;
					if($_REQUEST[$hotelcheckinDate])
					{
						$data2['checkinDate']	=	$App->dbformat_date($_REQUEST[$hotelcheckinDate]);
					}
					if($_REQUEST[$hotelcheckoutDate])
					{
						$data2['checkoutDate']	=	$App->dbformat_date($_REQUEST[$hotelcheckoutDate]);
					}

					$success2	=	$db->query_insert(TABLE_PACKAGE_HOTEL,$data2);	
					

					for($j=1;$j<=9;$j++)
					{
						$roomtype		=	'hot'.$h.'_roomId_'.$j;
						$roomcharge		=	'hot'.$h.'_roomcharge_'.$j;
						$othercharge	=	'hot'.$h.'_roomOtherCharge_'.$j;
						$hotelTotal		=	'hot'.$h.'_roomTotal_'.$j;
						$purchaseTotal	=	'hot'.$h.'_purchaseTotal_'.$j;
						
						$constant	=	1;
						if($_REQUEST[$roomtype]==2)
						{
							$constant	=	2;
						}
						if($_REQUEST[$roomtype]==3)
						{
							$constant	=	3;
						}
						if($_REQUEST[$roomtype]==4)
						{
							$constant	=	4;
						}
						if($_REQUEST[$roomtype]==5)
						{
							$constant	=	5;
						}
						if($_REQUEST[$roomtype]==6)
						{
							$constant	=	6;
						}
						
						$hotRoomRate				=	$_REQUEST[$roomcharge]/$constant;	
						$hotRoomOtherCharge			=	$_REQUEST[$othercharge]/$constant;	
						$hotRoomTotal				=	$hotRoomRate+$hotRoomOtherCharge;	
						$hotRoomPurchaseAmount		=	$_REQUEST[$purchaseTotal]/$constant;	
						
						$rdata2['packageId']		=	$success;
						$rdata2['package_hotelId']	=	$success2;
						$rdata2['roomId']			=	$_REQUEST[$roomtype];
						$rdata2['rate']				=	$hotRoomRate;
						$rdata2['otherCharge']		=	$hotRoomOtherCharge;
						$rdata2['total']			=	$hotRoomTotal;
						$rdata2['purchaseAmount']	=	$hotRoomPurchaseAmount;
						$rdata2['loginId']			=	$loginId;
						$rdata2['proId']			=	$proId;
						
						$rsuccess2	= 	$db->query_insert(TABLE_PACKAGE_HOTEL_ROOM_TYPE,$rdata2);
					}
				}
			}
		}
		if(@$_REQUEST['visa'])
		{
			$visaCount		=	$_REQUEST['pack_reg_visa_row_count'];
			if($visaCount>0)
			{
				for($v=1;$v<=$visaCount;$v++)
				{
					$visaFee			=	'visaFee_'.$v;
					$vfs				=	'vfs_'.$v;
					$ddCharge			=	'ddCharge_'.$v;
					$serviceCharge		=	'serviceCharge_'.$v;
					$visaotherCharge	=	'visaOtherCharge_'.$v;
					$courierCharge		=	'courierCharge_'.$v;
					$visaTotal			=	'visaTotal_'.$v;
					$supplierIdVisa		=	'supplierIdVisa_'.$v;
					$visaFor			=	'visaFor_'.$v;
					$prVisaTotal		=	'prVisaTotal_'.$v;
					
					$data3['packageId']		=	$success;
					$data3['visaFor']		=	$_REQUEST[$visaFor];
					$data3['visaFee']		=	$_REQUEST[$visaFee];
					$data3['vfs']			=	$_REQUEST[$vfs];
					$data3['ddCharge']		=	$_REQUEST[$ddCharge];
					$data3['serviceCharge']	=	$_REQUEST[$serviceCharge];
					$data3['otherCharge']	=	$_REQUEST[$visaotherCharge];
					$data3['courierCharge']	=	$_REQUEST[$courierCharge];
					$data3['total']			=	$_REQUEST[$visaTotal];
					$data3['supplierId']	=	$_REQUEST[$supplierIdVisa];
					$data3['purchaseAmount']=	$_REQUEST[$prVisaTotal];
					$data3['loginId']		=	$loginId;
					$data3['proId']			=	$proId;
					
					$success3				=	$db->query_insert(TABLE_PACKAGE_VISA,$data3);
				}							
			}
		}
		if(@$_REQUEST['transport'])
		{
			$transCount	=	$_REQUEST['pack_reg_transportation_row_count'];
			if($transCount>0)
			for($t=1;$t<=$transCount;$t++)
			{
				$vehicle			=	'vehicle_'.$t;
				$seatCharge			=	'seatCharge_'.$t;
				$transRemark		=	'transRemark_'.$t;
				$supplierIdTrans	=	'supplierIdTrans_'.$t;
				$prTransTotal		=	'prTransTotal_'.$t;
			
				$data4['packageId']			=	$success;
				$data4['vehicle']			=	$_REQUEST[$vehicle];
				$data4['seatCharge']		=	$_REQUEST[$seatCharge];
				$data4['remark']			=	$_REQUEST[$transRemark];
				$data4['supplierId']		=	$_REQUEST[$supplierIdTrans];
				$data4['purchaseAmount']	=	$_REQUEST[$prTransTotal];
				$data4['loginId']			=	$loginId;
				$data4['proId']				=	$proId;
				
				$success4 = $db->query_insert(TABLE_PACKAGE_TRANSPORTATION,$data4);
			}							
		}
		if($editId)
		{
		   /*
			* If editting one package that already did invoice for that edit the curresponding invoices also 
			* edit invoice package table, 4 package service table, package passenger table, transaction table
		   */
		   // first check whether any invoice is done for this package
		   $checkInvoiceQry	=	mysql_query("SELECT ID,tds,tax,proCharge,otherCharge,discount,invoiceNo
		   									   FROM ".TABLE_INVOICE_PACKAGE." WHERE packageId='$success'");
		   if(mysql_num_rows($checkInvoiceQry)>0)
		   {
		   		//for visa and airline
		   		$packCurrentAmountsQry	=	"SELECT PA.adultTotal,PA.childTotal,PA.infantTotal,
		   											COALESCE(SUM(PV.total),0) as visatotal,
		   											P.tds,P.otherCharge,P.tax
		   									   FROM ".TABLE_PACKAGE." P
		   								  LEFT JOIN ".TABLE_PACKAGE_AIRLINE." PA ON PA.packageId=P.ID
		   							      LEFT JOIN ".TABLE_PACKAGE_VISA." PV ON PV.packageId=P.ID
		   									  WHERE P.ID='$success'
		   								   GROUP BY PA.packageId,PV.packageId
		   									";		   									
		   		$packCurrentAmountsRes	=	mysql_query($packCurrentAmountsQry);
		   		$packCurrentAmounts		=	mysql_fetch_array($packCurrentAmountsRes);
		   		
		   		//for trasportation
		   		$transCurrentAmountsQry	=	"SELECT COALESCE(SUM(PT.seatCharge),0) as seatCharge
		   									   FROM ".TABLE_PACKAGE." P
		   								  LEFT JOIN ".TABLE_PACKAGE_TRANSPORTATION." PT ON PT.packageId=P.ID
		   									  WHERE P.ID='$success'
		   								   GROUP BY PT.packageId
		   									";		   									
		   		$transCurrentAmountsRes	=	mysql_query($transCurrentAmountsQry);
		   		$transCurrentAmounts	=	mysql_fetch_array($transCurrentAmountsRes);
		   		
		   		while($checkInvoice	=	mysql_fetch_array($checkInvoiceQry))
		   		{
		   			$iPackTotal		=	0;
					$iPackId		=	$checkInvoice['ID'];
					$packPassQry	=	mysql_query("SELECT PS.ID,PS.in_packageId,PS.packageId,PS.age,PS.total,
					 										IH.roomType
													   FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." PS
												  LEFT JOIN ".TABLE_INVOICE_PACKAGE_HOTEL." IH ON IH.passengerId=PS.ID
													   WHERE PS.in_packageId='$iPackId'");

					while($packPass	=	mysql_fetch_array($packPassQry))
					{
						$passTotal	=	0;
						$passId		=	$packPass['ID'];
						$passAge 	= 	$packPass['age'];
						$passroom 	= 	$packPass['roomType'];
						
						$hotelCurrentAmountsQry	=	mysql_query("SELECT SUM(total) as total
						                                           FROM ".TABLE_PACKAGE_HOTEL_ROOM_TYPE." PH 
						                                          WHERE PH.packageId='$success' AND roomId=$passroom
						                                       GROUP BY PH.packageId
						                                          ");

						$hotelCurrentAmounts	=	mysql_fetch_array($hotelCurrentAmountsQry);  
						if($passAge<2)
						{
							$airlineAmt	=	$packCurrentAmounts['infantTotal'];
						}
						elseif($passAge>=2 AND $passAge <12)
						{
							$airlineAmt	=	$packCurrentAmounts['childTotal'];
						}
						elseif($passAge>=12)
						{
							$airlineAmt	=	$packCurrentAmounts['adultTotal'];
						}
						
						$dataA['total']	=	$airlineAmt;
						$db->query_update(TABLE_INVOICE_PACKAGE_AIRLINE,$dataA , "passengerId='$passId' and in_packageId='$iPackId'");

						$dataH['total']	=	$hotelCurrentAmounts['total'];
						$db->query_update(TABLE_INVOICE_PACKAGE_HOTEL,$dataH , "passengerId='$passId' and in_packageId='$iPackId'");
						$dataV['total']	=	$packCurrentAmounts['visatotal'];
						$db->query_update(TABLE_INVOICE_PACKAGE_VISA,$dataV , "passengerId='$passId' and in_packageId='$iPackId'");
						$dataT['total']	=	$transCurrentAmounts['seatCharge'];
						$db->query_update(TABLE_INVOICE_PACKAGE_TRANSPORTATION,$dataT , "passengerId='$passId' and in_packageId='$iPackId'");
						$taxnew	=	$packCurrentAmounts['tax'];
						$passTotal1	=	$airlineAmt+$hotelCurrentAmounts['total']+$packCurrentAmounts['visatotal']+$transCurrentAmounts['seatCharge']+$packCurrentAmounts['tds']+$packCurrentAmounts['otherCharge'];
						if(strpos ($taxnew,'%'))
						{
							$taxTrim	=	rtrim($taxnew, "%");							
							$taxnew		=	$passTotal1*($taxTrim/100);			
						}
						$passTotal	=	$passTotal1+$taxnew;
						
						$dataP['total']	=	$passTotal;
						$db->query_update(TABLE_INVOICE_PACKAGE_PASSENGER,$dataP , "ID='$passId' and in_packageId='$iPackId'");
						$iPackTotal = 	$iPackTotal+$passTotal;
					}
					
					$itax		=	$checkInvoice['tax'];
					$inPackNet1	=	$iPackTotal+$checkInvoice['tds']+$checkInvoice['proCharge']+$checkInvoice['otherCharge'];
					if(strpos ($itax,'%'))
					{
						$taxTrim			=	rtrim($itax, "%");							
						$itax				=	$inPackNet1*($taxTrim/100);			
					}
					$inPackNet2 = $inPackNet1+$itax;
					$inPackNet	= $inPackNet2-$checkInvoice['discount'];
					$dataIP['netAmount']	=	$inPackNet;
					$db->query_update(TABLE_INVOICE_PACKAGE,$dataIP , "ID='$iPackId'");
					
					$pacInvoiceNo	=	$checkInvoice['invoiceNo'];
					$transAmt		=	$inPackNet-($checkInvoice['tds']+$itax);
					
					$dataIT1['credit']	=	$transAmt;	
					$db->query_update(TABLE_TRANSACTION,$dataIT1 , "referenceNo ='$pacInvoiceNo' AND voucherType like '%Package%' AND fromLedger='8'");
					$dataIT2['debit']	=	$transAmt;
					$db->query_update(TABLE_TRANSACTION,$dataIT2 , "referenceNo ='$pacInvoiceNo' AND voucherType like '%Package%' AND toLedger='8'");
				}
		   }

			if(isset($_REQUEST['confirm']) && $confirm==0)
			{
				 $pacInvoiceQry	=	mysql_query("SELECT ID,invoiceNo,invoiceDate,customerId FROM ".TABLE_INVOICE_PACKAGE." WHERE packageId=$packageId ");

				 $pacInvoiceNum	=	mysql_num_rows($pacInvoiceQry);
				 if($pacInvoiceNum>0)
				 {
				 	while($pacInvoiceRow	=	mysql_fetch_array($pacInvoiceQry))
				 	{
				 		$packageInvoice		=	$pacInvoiceRow['ID'];
				 		$pacInvoiceNo		=	$pacInvoiceRow['invoiceNo'];
				 		$pacInvoiceDate		=	$pacInvoiceRow['invoiceDate'];
				 		
				 		$pacPassengerQry	=	mysql_query("SELECT ID,age,gender,passportNo,passengerName FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." WHERE in_packageId=".$packageInvoice."");
				 		while($pacPassengerRow	=	mysql_fetch_array($pacPassengerQry))
				 		{
							if(in_array("airline",$pacServiceAry))
							{
								// voucher number
								$airVoucherNo	=	$db->transactionVoucher(TABLE_TRANSACTION," voucherType ='Package Airline' and proId=$proId ");
								//fromLedger
								$accPrId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Airline Payable' ";
								$acc_prRecord 	= 	$db->query_first($accPrId);
								$accountPrId	=	$acc_prRecord['ID'];
				
								//to ledger
								$supplyQry		=	mysql_query("SELECT supplierId,adultTotal,childTotal,infantTotal FROM ".TABLE_PURCHASE_PAC_AIRLINE." WHERE packageId=$packageId");
								$supplyRow		=	mysql_fetch_array($supplyQry);
								$supplierId		=	$supplyRow['supplierId'];
								
								if($pacPassengerRow['age']>=12)
								{
									$airPurchase = $supplyRow['adultTotal'];
								}
								else if($pacPassengerRow['age']>=2 AND $pacPassengerRow['age']<12)
								{
									$airPurchase = $supplyRow['childTotal'];
								}
								else if($pacPassengerRow['age']<2)
								{
									$airPurchase = $supplyRow['infantTotal'];
								}
								
								$pdata2['in_packageId']	=	$packageInvoice;	
								$pdata2['passengerId']	=	$pacPassengerRow['ID'];
								$pdata2['purchaseDate']	=	$pacInvoiceDate;
								$pdata2['supplierId']	= 	$supplierId;
								$pdata2['voucherNo']	= 	$airVoucherNo;
								$pdata2['netAmount']	=	$airPurchase;
								$pdata2['loginId']		=	$loginId;
								$pdata2['proId']		=	$proId;
								
								$psuccess1	=	$db->query_insert(TABLE_PURCHASE_PACKAGE_AIRLINE,$pdata2);
								/* -----------------Transaction - purchase airline paid ------------ */
								
								$airDetails	=	"INV#PI$pacInvoiceNo ,Pac:$packageName ,Pax:".$pacPassengerRow['passengerName'].",By:$loginName";					
								$tsdata1['fromLedger'] 		=  	$accountPrId;
								$tsdata1['toLedger'] 		=  	$supplierId;
								$tsdata1['voucherNo']		= 	$airVoucherNo;
								$tsdata1['voucherType']		=  	'Package Airline'; 
								$tsdata1['credit']			= 	0;
								$tsdata1['debit']			=   $airPurchase;
								$tsdata1['referenceNo']		=   $pacInvoiceNo;
								$tsdata1['addedDate']		=	"NOW()";
								$tsdata1['transactionDate']	=	$pacInvoiceDate;
								$tsdata1['remark']			=  	'Purchase Package: '.$pacInvoiceNo; 
								$tsdata1['details']			=  	$airDetails; 
								$tsdata1['loginId']			=	$loginId;
								$tsdata1['proId']			=	$proId;
								
								$db->query_insert(TABLE_TRANSACTION, $tsdata1);						
												
								/* ------------------------------------------ */
								/* ------------------------------------------ */
								$tfdata1['fromLedger'] 		=  	$supplierId;
								$tfdata1['toLedger'] 		=  	$accountPrId;
								$tfdata1['voucherNo']		= 	$airVoucherNo;
								$tfdata1['voucherType']		=  	'Package Airline';
								$tfdata1['credit']			=   $airPurchase;
								$tfdata1['debit']			= 	0;
								$tfdata1['referenceNo']		=   $pacInvoiceNo;
								$tfdata1['addedDate']		=	"NOW()";
								$tfdata1['transactionDate']	=	$pacInvoiceDate;
								$tfdata1['remark']			=  	'Purchase Package: '.$pacInvoiceNo;
								$tfdata1['details']			=  	$airDetails; 
								$tfdata1['loginId']			=	$loginId;
								$tfdata1['proId']			=	$proId;
							
								$db->query_insert(TABLE_TRANSACTION, $tfdata1);		
								
							}
							if(in_array("hotel",$pacServiceAry))
							{
								
								//fromLedger
								$accPrId1 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Hotel Payable' ";
								$acc_prRecord1 	= 	$db->query_first($accPrId1);
								$accountPrId1	=	$acc_prRecord1['ID'];
								// roomtype
								$roomQry		=	mysql_query("SELECT roomType FROM ".TABLE_INVOICE_PACKAGE_HOTEL." WHERE passengerId= ".$pacPassengerRow['ID']." and in_packageId = $packageInvoice");
								$roomRow		=	mysql_fetch_array($roomQry);
								$roomTypeId		=	$roomRow['roomType'];
								//to ledger
								/*$supplyQry1		=	mysql_query("SELECT supplierId,purchaseAmount FROM ".TABLE_PACKAGE_HOTEL_ROOM_TYPE." WHERE packageId=$packageId and roomId=$roomTypeId ");*/
								
								$supplyQry1		=	mysql_query("SELECT PR.purchaseAmount,PH.supplierId
													   FROM ".TABLE_PACKAGE_HOTEL." PH
												 INNER JOIN ".TABLE_PACKAGE_HOTEL_ROOM_TYPE." PR ON PH.ID=PR.package_hotelId
													  WHERE PH.packageId=$packageId and PR.roomId=$roomTypeId ");
								while($supplyRow1		=	mysql_fetch_array($supplyQry1))
								{
									// voucher number
									$hotVoucherNo	=	$db->transactionVoucher(TABLE_TRANSACTION," voucherType ='Package Hotel' and proId=$proId ");
									$supplierId1		=	$supplyRow1['supplierId'];
									$purchaseAmount		=	$supplyRow1['purchaseAmount'];
									
									$pdata3['in_packageId']	=	$packageInvoice;	
									$pdata3['passengerId']	=	$pacPassengerRow['ID'];
									$pdata3['purchaseDate']	=	$pacInvoiceDate;
									$pdata3['supplierId']	= 	$supplierId1;
									$pdata3['voucherNo']	= 	$hotVoucherNo;
									$pdata3['netAmount']	=	$purchaseAmount;
									$pdata3['loginId']		=	$loginId;
									$pdata3['proId']		=	$proId;
									
									$psuccess1	=	$db->query_insert(TABLE_PURCHASE_PACKAGE_HOTEL,$pdata3);
									/* -----------------Transaction - purchase hotel paid ------------ */
									
									$hotelDetails	=	"INV#PI$pacInvoiceNo ,Pac:$packageName ,Pax:".$pacPassengerRow['passengerName']." ,By:$loginName";					
									$tsdata2['fromLedger'] 		=  	$accountPrId1;
									$tsdata2['toLedger'] 		=  	$supplierId1;
									$tsdata2['voucherNo']		= 	$hotVoucherNo;
									$tsdata2['voucherType']		=  	'Package Hotel'; 
									$tsdata2['credit']			= 	0;
									$tsdata2['debit']			=   $purchaseAmount;
									$tsdata2['referenceNo']		=   $pacInvoiceNo;
									$tsdata2['addedDate']		=	"NOW()";
									$tsdata2['transactionDate']	=	$pacInvoiceDate;
									$tsdata2['remark']			=  	'Purchase Package: '.$pacInvoiceNo; 
									$tsdata2['details']			=  	$hotelDetails; 
									$tsdata2['loginId']			=	$loginId;
									$tsdata2['proId']			=	$proId;
									
									$db->query_insert(TABLE_TRANSACTION, $tsdata2);						
													
									/* ------------------------------------------ */
									/* ------------------------------------------ */
									$tfdata2['fromLedger'] 		=  	$supplierId1;
									$tfdata2['toLedger'] 		=  	$accountPrId1;
									$tfdata2['voucherNo']		= 	$hotVoucherNo;
									$tfdata2['voucherType']		=  	'Package Hotel';
									$tfdata2['credit']			=   $purchaseAmount;
									$tfdata2['debit']			= 	0;
									$tfdata2['referenceNo']		=   $pacInvoiceNo;
									$tfdata2['addedDate']		=	"NOW()";
									$tfdata2['transactionDate']	=	$pacInvoiceDate;
									$tfdata2['remark']			=  	'Purchase Package: '.$pacInvoiceNo;
									$tfdata2['details']			=  	$hotelDetails; 
									$tfdata2['loginId']			=	$loginId;
									$tfdata2['proId']			=	$proId;
								
									$db->query_insert(TABLE_TRANSACTION, $tfdata2);
								}
									
							}
							if(in_array("visa",$pacServiceAry))
							{
								
								//fromLedger
								$accPrId2 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Visa Payable' ";
								$acc_prRecord2 	= 	$db->query_first($accPrId2);
								$accountPrId2	=	$acc_prRecord2['ID'];
								
								//to ledger
								$supplyQry2		=	mysql_query("SELECT supplierId,purchaseAmount FROM ".TABLE_PACKAGE_VISA." WHERE packageId=$packageId");
								while($supplyRow2		=	mysql_fetch_array($supplyQry2))
								{
									//visa voucher number
									$visaVoucherNo	=	$db->transactionVoucher(TABLE_TRANSACTION," voucherType ='Package Visa' and proId=$proId ");
						
									$supplierId2		=	$supplyRow2['supplierId'];
									$purchaseAmount		=	$supplyRow2['purchaseAmount'];
									
									$pdata4['in_packageId']	=	$packageInvoice;	
									$pdata4['passengerId']	=	$pacPassengerRow['ID'];
									$pdata4['purchaseDate']	=	$pacInvoiceDate;
									$pdata4['supplierId']	= 	$supplierId2;
									$pdata4['voucherNo']	= 	$visaVoucherNo;
									$pdata4['netAmount']	=	$purchaseAmount;
									$pdata4['loginId']		=	$loginId;
									$pdata4['proId']		=	$proId;
									
									$psuccess1	=	$db->query_insert(TABLE_PURCHASE_PACKAGE_VISA,$pdata4);
									/* -----------------Transaction - purchase visa paid ------------ */
									
									$visaDetails	=	"INV#PI$pacInvoiceNo ,Pac:$packageName ,Pax:".$pacPassengerRow['passengerName']." ,By:$loginName";					
									$tsdata3['fromLedger'] 		=  	$accountPrId2;
									$tsdata3['toLedger'] 		=  	$supplierId2;
									$tsdata3['voucherNo']		= 	$visaVoucherNo;
									$tsdata3['voucherType']		=  	'Package Visa'; 
									$tsdata3['credit']			= 	0;
									$tsdata3['debit']			=   $purchaseAmount;
									$tsdata3['referenceNo']		=   $pacInvoiceNo;
									$tsdata3['addedDate']		=	"NOW()";
									$tsdata3['transactionDate']	=	$pacInvoiceDate;
									$tsdata3['remark']			=  	'Purchase Package: '.$pacInvoiceNo; 
									$tsdata3['details']			=  	$visaDetails; 
									$tsdata3['loginId']			=	$loginId;
									$tsdata3['proId']			=	$proId;
									
									$db->query_insert(TABLE_TRANSACTION, $tsdata3);						
													
									/* ------------------------------------------ */
									/* ------------------------------------------ */
									$tfdata3['fromLedger'] 		=  	$supplierId2;
									$tfdata3['toLedger'] 		=  	$accountPrId2;
									$tfdata3['voucherNo']		= 	$visaVoucherNo;
									$tfdata3['voucherType']		=  	'Package Visa';
									$tfdata3['credit']			=   $purchaseAmount;
									$tfdata3['debit']			= 	0;
									$tfdata3['referenceNo']		=   $pacInvoiceNo;
									$tfdata3['addedDate']		=	"NOW()";
									$tfdata3['transactionDate']	=	$pacInvoiceDate;
									$tfdata3['remark']			=  	'Purchase Package: '.$pacInvoiceNo;
									$tfdata3['details']			=  	$visaDetails; 
									$tfdata3['loginId']			=	$loginId;
									$tfdata3['proId']			=	$proId;
								
									$db->query_insert(TABLE_TRANSACTION, $tfdata3);
								}
							}
							if(in_array("transportation",$pacServiceAry))
							{
																//fromLedger
								$accPrId3 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Transport Payable' ";
								$acc_prRecord3 	= 	$db->query_first($accPrId3);
								$accountPrId3	=	$acc_prRecord3['ID'];
								
								// seatStatus
								$seatQry		=	mysql_query("SELECT seatStatus FROM ".TABLE_INVOICE_PACKAGE_TRANSPORTATION." WHERE passengerId= ".$pacPassengerRow['ID']." and in_packageId = $packageInvoice");
								$seatRow		=	mysql_fetch_array($seatQry);
								$seatStatus		=	$seatRow['seatStatus'];
								if($seatStatus==1)
								{
									//to ledger
									$supplyQry3		=	mysql_query("SELECT supplierId,purchaseAmount FROM ".TABLE_PACKAGE_TRANSPORTATION." WHERE packageId=$packageId ");
									while($supplyRow3		=	mysql_fetch_array($supplyQry3))
									{
										//transaction voucher number
										$transVoucherNo	=	$db->transactionVoucher(TABLE_TRANSACTION," voucherType ='Package Transportation' and proId=$proId ");
										$supplierId3		=	$supplyRow3['supplierId'];
										$purchaseAmount		=	$supplyRow3['purchaseAmount'];
										
										$pdata5['in_packageId']	=	$packageInvoice;	
										$pdata5['passengerId']	=	$pacPassengerRow['ID'];
										$pdata5['purchaseDate']	=	$pacInvoiceDate;
										$pdata5['supplierId']	= 	$supplierId3;
										$pdata5['voucherNo']	= 	$transVoucherNo;
										$pdata5['netAmount']	=	$purchaseAmount;
										$pdata5['loginId']		=	$loginId;
										$pdata5['proId']		=	$proId;
										
										$psuccess1	=	$db->query_insert(TABLE_PURCHASE_PACKAGE_TRANSPORT,$pdata5);
										/* -----------------Transaction - purchase hotel paid ------------ */
										
										$pacDetails	=	"INV#PI$pacInvoiceNo ,Pac:$packageName ,Pax:".$pacPassengerRow['passengerName']." ,By:$loginName";					
										$tsdata3['fromLedger'] 		=  	$accountPrId3;
										$tsdata3['toLedger'] 		=  	$supplierId3;
										$tsdata3['voucherNo']		= 	$transVoucherNo;
										$tsdata3['voucherType']		=  	'Package Transportation'; 
										$tsdata3['credit']			= 	0;
										$tsdata3['debit']			=   $purchaseAmount;
										$tsdata3['referenceNo']		=   $pacInvoiceNo;
										$tsdata3['addedDate']		=	"NOW()";
										$tsdata3['transactionDate']	=	$pacInvoiceDate;
										$tsdata3['remark']			=  	'Purchase Package: '.$pacInvoiceNo; 
										$tsdata3['details']			=  	$pacDetails; 
										$tsdata3['loginId']			=	$loginId;
										$tsdata3['proId']			=	$proId;
										
										$db->query_insert(TABLE_TRANSACTION, $tsdata3);						
														
										/* ------------------------------------------ */
										/* ------------------------------------------ */
										$tsdata3['fromLedger'] 		=  	$supplierId3;
										$tsdata3['toLedger'] 		=  	$accountPrId3;
										$tsdata3['voucherNo']		= 	$transVoucherNo;
										$tsdata3['voucherType']		=  	'Package Transportation';
										$tsdata3['credit']			=   $purchaseAmount;
										$tsdata3['debit']			= 	0;
										$tsdata3['referenceNo']		=   $pacInvoiceNo;
										$tsdata3['addedDate']		=	"NOW()";
										$tsdata3['transactionDate']	=	$pacInvoiceDate;
										$tsdata3['remark']			=  	'Purchase Package: '.$pacInvoiceNo;
										$tsdata3['details']			=  	$pacDetails; 
										$tsdata3['loginId']			=	$loginId;
										$tsdata3['proId']			=	$proId;
									
										$db->query_insert(TABLE_TRANSACTION, $tsdata3);
									}
								}
							}
							
						} // passenger loop
					} //invoice packages loop
				 } // checking this package in invoice package table
				 if($_REQUEST['pDiscount']>0)
				 { 
				 	//echo $_REQUEST['pDiscount'];die;
				 	$pDiscountNw	=	$_REQUEST['pDiscount'];
				 	
				 	$adultPacNw		=	0;
		 			$childPacNw		=	0;
		 			$infantPacNw	=	0;
		 			if($adultPac>0)
		 			{
						$adultPacNw	=	$adultPac-$pDiscountNw;
					}
					if($childPac>0)
		 			{
						$childPacNw	=	$childPac-$pDiscountNw;
					}
					if($infantPac>0)
		 			{
						$infantPacNw	=	$infantPac-$pDiscountNw;
					}
					
					// updating to package table
				    mysql_query("UPDATE ".TABLE_PACKAGE." SET `adultTotal`=$adultPacNw,`childTotal`=$childPacNw,`infantTotal`=$infantPacNw,`discount`=$pDiscountNw WHERE ID=$packageId");

					//discount account
					$disId 			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given' ";
					$dis_record 	= 	$db->query_first($disId);
					$discountId		=	$dis_record['ID'];
					
					//Selecting  invoice packages of this package
					$pacInQry		=	mysql_query("SELECT IP.ID,IP.invoiceNo,IP.invoiceDate,IP.customerId,IP.netAmount,
															count(PP.ID) passNum 
													   FROM ".TABLE_INVOICE_PACKAGE." IP 
												  LEFT JOIN ".TABLE_INVOICE_PACKAGE_PASSENGER." PP ON PP.in_packageId=IP.ID
													  WHERE IP.packageId=$packageId and IP.proId=$proId
													  GROUP BY IP.ID");

					 $pacInNum	=	mysql_num_rows($pacInQry);
					 if($pacInNum>0)
					 {
					 	while($pacInRow	=	mysql_fetch_array($pacInQry))
					 	{
					 		$pacInId		=	$pacInRow['ID'];
					 		$pacInNo		=	$pacInRow['invoiceNo'];
					 		$pacInDate		=	$pacInRow['invoiceDate'];
					 		$customerId		=	$pacInRow['customerId'];
					 		$pacInAmount	=	$pacInRow['netAmount'];
					 		$passCount		=	$pacInRow['passNum'];
				 			
				 			$totalDiscount	=	$passCount*$pDiscountNw;
				 			$totalInAmt		=	$pacInAmount-$totalDiscount;
				 			
				 			
							$customerQry	=	mysql_query("select accountName FROM ".TABLE_ACCOUNTS." WHERE ID='$customerId' ");
							$customerRow	=	mysql_fetch_array($customerQry);
							 
					      	$invoiceDetails	=	"INV#PI$pacInNo,Pac:$packageName ,By:$loginName,To:".$customerRow['accountName'];
					      	// updating to invoice package table
					      	mysql_query("UPDATE ".TABLE_INVOICE_PACKAGE." SET `netAmount`= $totalInAmt WHERE ID=$pacInId");
					      	
					      	// updating to transaction table
						  	$tsdata5['fromLedger'] 		=  	$discountId;
							$tsdata5['toLedger'] 		=  	$customerId;
							$tsdata5['voucherNo']		= 	$pacInNo;
							$tsdata5['voucherType']		=  	'Package Receivable';
							$tsdata5['credit']			= 	0;
							$tsdata5['debit']			=   $totalDiscount;
							$tsdata5['referenceNo']		=   $pacInNo;
							$tsdata5['addedDate']		=	"NOW()";
							$tsdata5['transactionDate']	=	$pacInDate;
							$tsdata5['remark']			=  	'Invoice Package: '.$pacInNo;
							$tsdata5['details']			=  	$invoiceDetails;
							$tsdata5['loginId']			=	$loginId;
							$tsdata5['proId']			=	$proId;
							
							$db->query_insert(TABLE_TRANSACTION, $tsdata5);						
											
							/* ------------------------------------------ */
							/* ------------------------------------------ */
							$tfdata5['fromLedger'] 		=  	$customerId;
							$tfdata5['toLedger'] 		=  	$discountId;
							$tfdata5['voucherNo']		= 	$pacInNo;
							$tfdata5['voucherType']		=  	'Package Receivable';
							$tfdata5['credit']			=   $totalDiscount;
							$tfdata5['debit']			= 	0;
							$tfdata5['referenceNo']		=   $pacInNo;
							$tfdata5['addedDate']		=	"NOW()";
							$tfdata5['transactionDate']	=	$pacInDate;
							$tfdata5['remark']			=  	'Invoice Package: '.$pacInNo;
							$tfdata5['details']			=  	$invoiceDetails;
							$tfdata5['loginId']			=	$loginId;
							$tfdata5['proId']			=	$proId;
						
							$db->query_insert(TABLE_TRANSACTION, $tfdata5);
						}
					}
				 }
				 if($_REQUEST['pExtra']!=0)
				 {
				 	$pExtraNw		= 	$_REQUEST['pExtra'];	
				 	
				 	$totalDiscount	=	0;
		 			$totalInAmt		=	0;
				 	
				 	$adultPacNw		=	0;
		 			$childPacNw		=	0;
		 			$infantPacNw	=	0;
		 			
		 			if($adultPac>0)
		 			{
						$adultPacNw	=	$adultPac+$pExtraNw;
					}
					if($childPac>0)
		 			{
						$childPacNw	=	$childPac+$pExtraNw;
					}
					if($infantPac>0)
		 			{
						$infantPacNw	=	$infantPac+$pExtraNw;
					}
					
					// updating to package date
				    mysql_query("UPDATE ".TABLE_PACKAGE." SET `adultTotal`=$adultPacNw,`childTotal`=$childPacNw,`infantTotal`=$infantPacNw,`extra`=$pExtraNw WHERE ID=$packageId");
				 	
				 	//Selecting  invoice packages of this package
					$pacInQry		=	mysql_query("SELECT IP.ID,IP.invoiceNo,IP.invoiceDate,IP.customerId,IP.netAmount,
															count(PP.ID) passNum 
													   FROM ".TABLE_INVOICE_PACKAGE." IP 
												  LEFT JOIN ".TABLE_INVOICE_PACKAGE_PASSENGER." PP ON PP.in_packageId=IP.ID
													  WHERE IP.packageId=$packageId and IP.proId=$proId
													  GROUP BY IP.ID
													  ");

					 $pacInNum	=	mysql_num_rows($pacInQry);
					 if($pacInNum>0)
					 {
					 	while($pacInRow	=	mysql_fetch_array($pacInQry))
					 	{
					 		$pacInId		=	$pacInRow['ID'];
					 		$pacInNo		=	$pacInRow['invoiceNo'];
					 		$pacInDate		=	$pacInRow['invoiceDate'];
					 		$customerId		=	$pacInRow['customerId'];
					 		$pacInAmount	=	$pacInRow['netAmount'];
					 		$passCount		=	$pacInRow['passNum'];
				 			
				 			$totalExtra		=	$passCount*$pExtraNw;
				 			$totalInAmt		=	$pacInAmount+$totalExtra;
				 	
						 	$invoiceDetails	=	"INV#PI$pacInNo,Pac:$packageName ,By:$loginName,To:Package Extra Chrg";
			  	 
						  	$extraQuery		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Package Extra Chrg' ";
							$extra_record 	= 	$db->query_first($extraQuery);
							$extraId		=	$extra_record['ID'];
							
							// updating to invoice package table
					      	mysql_query("UPDATE ".TABLE_INVOICE_PACKAGE." SET `netAmount`= $totalInAmt WHERE ID=$pacInId");
					      	
						    //updating to transaction table
						  	$tsdata['fromLedger'] 		=   $customerId;
							$tsdata['toLedger'] 		=  	$extraId;
							$tsdata['voucherNo']		= 	$pacInNo;
							$tsdata['voucherType']		=  	'Package Receivable';
							$tsdata['credit']			= 	0;
							$tsdata['debit']			=   $totalExtra;
							$tsdata['addedDate']		=	"NOW()";
							$tsdata['transactionDate']	=	$pacInDate;
							$tsdata['remark']			=  	'Package Receivable: '.$pacInNo;
							$tsdata['details']			=  	$invoiceDetails;
							$tsdata['loginId']			=	$loginId;
							$tsdata['proId']			=	$proId;
							
							$db->query_insert(TABLE_TRANSACTION, $tsdata);						
											
							/* ------------------------------------------ */
							/* ------------------------------------------ */
							$tfdata['fromLedger'] 		=  	$tdsId;
							$tfdata['toLedger'] 		=  	$customerId;
							$tfdata['voucherNo']		= 	$pacInNo;
							$tfdata['voucherType']		=  	'Package Receivable';
							$tfdata['credit']			=   $totalExtra;
							$tfdata['debit']			= 	0;
							$tfdata['addedDate']		=	"NOW()";
							$tfdata['transactionDate']	=	$pacInDate;
							$tfdata['remark']			=  	'Package Receivable: '.$pacInNo;
							$tfdata['details']			=  	$invoiceDetails;
							$tfdata['loginId']			=	$loginId;
							$tfdata['proId']			=	$proId;

							$db->query_insert(TABLE_TRANSACTION, $tfdata);	
						}	
					}
				 }
				 
			} // cheking whether coinfirm checked and ticking first time
		} // if updation is taking place then only
	}
	$db->close();
	return $success;
}	 
?>