<?php
require("../admin_header.php");
if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';
$editId			=	$_REQUEST['id'];

if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"proId='$proId'";
}

$editSelect		=	"SELECT * FROM ".TABLE_PACKAGE." WHERE ID='$editId' AND $check";
$editResult 	= 	$db->query($editSelect);
$editRow 		= 	mysql_fetch_array($editResult);
$services 		= 	explode(",",$editRow['services']);
?>

<script>

	function valid()
	{
		flag=false;

		ptds = document.getElementById('ptds').value;	
			if(isNaN(ptds) && ptds!=0)
			{		
				document.getElementById('ptdsdiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		pTax = document.getElementById('pTax').value;	
			if(isNaN(pTax) && pTax!=0)
			{		
				document.getElementById('pTaxdiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		pOtherCharges = document.getElementById('pOtherCharges').value;	
			if(isNaN(pOtherCharges) && pOtherCharges!=0)
			{		
				document.getElementById('pOtherChargesdiv').innerHTML="Enter valid amount.";
				flag=true;
			}
	    adultTotal = document.getElementById('adultTotal').value;	
			if(isNaN(adultTotal) && adultTotal!=0)
			{		
				document.getElementById('adultTotaldiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		childTotal = document.getElementById('childTotal').value;	
			if(isNaN(childTotal) && childTotal!=0)
			{		
				document.getElementById('childTotaldiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		infantTotal = document.getElementById('infantTotal').value;	
			if(isNaN(infantTotal) && infantTotal!=0)
			{		
				document.getElementById('infantTotaldiv').innerHTML="Enter valid amount.";
				flag=true;
			}
			
		airline		=	document.getElementById('airline').checked;
		visa		=	document.getElementById('visa').checked;
		hotel		=	document.getElementById('hotel').checked;
		transport	=	document.getElementById('transport').checked;
		
			if((!airline)&&(!visa)&&(!hotel)&&(!transport)) 
			{
				document.getElementById('servicesdiv').innerHTML="Select atleast one.";
				flag=true;
			}
		var exdate	=	document.getElementById('expirydiv').value;
			if(exdate == 1)
			{
				document.getElementById('dDiv').innerHTML = "Invalid Date";
				flag	=	true;
			}
			
		var pDiscount	=	document.getElementById('pDiscount').value;
			if(pDiscount<0 || pDiscount>=adultTotal)
			{
				document.getElementById('pDiscountDiv').innerHTML = "Enter valid amount";
				flag	=	true;
			}
			
		var pExtra	=	document.getElementById('pExtra').value;
			if(pExtra<0)
			{
				document.getElementById('pExtraDiv').innerHTML = "Enter valid amount";
				flag	=	true;
			}	
		if(flag==true)
		{
		return false;
		}
	}
	//clear the validation msg
	function clearbox(Element_id)
	{
	document.getElementById(Element_id).innerHTML="";
	}

	function validdate()
	{
		var dDate	=	document.getElementById('dDate').value;
		var today 	= 	new Date();
		var dd 		= 	today.getDate();
		var mm 		= 	today.getMonth()+1; //January is 0!
		var yyyy 	= 	today.getFullYear();
		
		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		}
		today = dd+'-'+mm+'-'+yyyy;
		
		var dDateSplit= dDate.split('-');
		var todaySplit= today.split('-');
		
		var dDateLast 	= 	new Date(dDateSplit[2], +dDateSplit[1]-1, dDateSplit[0]);
		var todayLast 		= 	new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
		var dateDiff		=	(todayLast.getTime() - dDateLast.getTime()) / (1000*60*60*24);
		//alert(dateDiff);
		var year = Math.round(dateDiff/365);
		if(dateDiff>=0)
		{
			alert("Invalid Departure Date");
			document.getElementById('expirydiv').value	=1;
			return false;
		}
		
		else {
			
			document.getElementById('expirydiv').value	='';
		}
		
	}
</script>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
<div class="bd_panel bd_panel_default bd_panel_shadow">
       <form class="default_form" method="post" id="package_reg_form" action="do.php?op=edit" onsubmit="return valid()">
       <input  type="hidden" name="editId" value="<?php echo $editId;?>"/>
        <div class="bd_panel_head">
            <h3>Create Package</h3>
        </div>
        <div class="bd_panel_body">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Package Name <span class="valid">*</span></label>
                        <input name="packageName" type="text" value="<?php echo $editRow['packageName']; ?>" required="" >
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2" style="display: none">
                    <div class="form_block">
                        <label>Staff in Charge</label>
                        <select name="staff">
                            <option value="">Select</option>
                            <?php
                            $staffQry = mysql_query("SELECT * FROM " . TABLE_STAFF . " where $check");
                            while ($staffRow = mysql_fetch_array($staffQry)) {
                                ?>
                                <option value="<?php echo $staffRow['ID']; ?>"  <?php if($staffRow['ID']==$editRow['staff']){echo "selected";}?>><?php echo $staffRow['staffName']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Leader</label>
                        <input type="text" name="leader" value="<?php echo $editRow['leader']; ?>">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Mobile</label>
                        <input type="text" name="mobile" value="<?php echo $editRow['mobile']; ?>">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Departure Date</label>
                        <input type="text" name="date" class="user_date" value="<?php echo $App->dbformat_date($editRow['date']); ?>" onchange="validdate()" id="dDate" onclick="clearbox('dDiv')" >
                        <input type="hidden" name="expirydiv" id="expirydiv" value="" />
                    </div>
                    <div id="dDiv" class="valid" style="color:#FF6600;"></div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Details</label>
                        <input type="text" name="place" value="<?php echo $editRow['place']; ?>">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Duration</label>
                        <input type="text" name="duration" value="<?php echo $editRow['duration']; ?>">
                    </div>
                </div> 
            </div>
            <div class="row"> 
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Remark</label>
                        <input type="text" name="premark" value="<?php echo $editRow['remark']; ?>">
                    </div>
                </div>
            </div>
            
            <div class="form_section_head"><h3>Services <span class="valid">*</span></h3></div>
            
            <div class="package_serv_controls">
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_airline" name="airline" id="airline" onclick="clearbox('servicesdiv')" <?php if(in_array('airline',$services)){echo "checked=checked";}?> ><label
                        class="check_label">Airline</label>
                </div>
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_hotel" name="hotel" id="hotel" onclick="clearbox('servicesdiv')" <?php if(in_array('hotel',$services)){echo "checked=checked";}?> ><label
                        class="check_label">Hotel</label>
                </div>
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_visa" name="visa" id="visa" onclick="clearbox('servicesdiv')" <?php if(in_array('visa',$services)){echo "checked=checked";}?> ><label
                        class="check_label">Visa</label>
                </div>
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_transportation" name="transport" id="transport" onclick="clearbox('servicesdiv')" <?php if(in_array('transportation',$services)){echo "checked=checked";}?> >                        
                    <label class="check_label">Transportation</label>
                </div>
            </div>
             <div id="servicesdiv" class="valid" style="color:#FF6600;"></div>
             
            <div id="package_dynamic">
			<!----------------------------------Airline---------------------------------->
				<?php if(in_array('airline',$services))
				{
					$airQry		=	mysql_query("SELECT a1.yq,
														a1.procCharge,
														a1.tax,
														a1.adultCharge,
														a1.childCharge,
														a1.infantCharge,
														a1.adultTotal,
														a1.childTotal,
														a1.infantTotal,
														a1.remark,
														a2.supplierId,
														a2.adultTotal as a2adultTotal,
														a2.childTotal as a2childTotal,
														a2.infantTotal as a2infantTotal
												   FROM ".TABLE_PACKAGE_AIRLINE." as a1 ,".TABLE_PURCHASE_PAC_AIRLINE." as a2
												  WHERE a1.packageId=$editId and a2.packageId=$editId");
					
					$airRow		=	mysql_fetch_array($airQry);
					?>
					<div class="package_dynamic_box pack_dynamic_airline">
				        <h3 class="pack_dynamic_head">Airline</h3>
				        <div class="pack_air_table_wrap">
				       
				            <table class="table table-bordered bd_table ">
				                <thead>
				                <tr class="table_head">
				                    <th colspan="9">Airline Details</th>
				                    <th colspan="2">
				                        <button class="bd_btn bd_btn_blue add_package_airline_reg_row" type="button">
				                            <i class="ion ion-plus"></i>Add New
				                        </button>
				                    </th>
				                </tr>
				                <tr>
				                    <th>Sl No</th>
				                    <th>Airline</th>
				                    <th>Flight No</th>
				                    <th>Journey Date</th>
				                    <th>Time</th>
				                    <th>Arrival Date</th>
                    				<th>Time</th>
				                    <th>Sector From</th>
				                    <th>Sector To</th>
				                    <th>Class</th>
				                    <th>Action</th>
				                  
				                </tr>
				                </thead>
				                <tbody>
				                <?php 
				                $m = 0;
				                $flightQry	= 	mysql_query("SELECT * FROM ".TABLE_PACKAGE_AIRLINE_FLIGHTS." WHERE packageId=$editId ORDER BY ID ASC ");
				                $flightNum	=	mysql_num_rows($flightQry);		
				                if($flightNum>0)
				                {
				                while($flightRows	= mysql_fetch_array($flightQry))
				                {
				                ?>
				                <tr>
				                    <td><?php echo ++$m;?></td>
				                    <td>
				                        <select name="airlineName_<?php echo $m ;?>"  >
				                            <option value="">Select</option>
				                            <?php
				                            $airQry1 = mysql_query("select ID,flightName FROM " . TABLE_FLIGHT . " WHERE $check");
				                            while ($airRow1 = mysql_fetch_array($airQry1)) {
				                                ?>
				                                <option
				                                    value="<?php echo $airRow1['ID']; ?>" <?php if($flightRows['airline']==$airRow1['ID']){echo "selected";}?>><?php echo $airRow1['flightName']; ?></option>
				                                <?php
				                            }
				                            ?>
				                        </select>
				                    </td>
				                    <td>
				                        <input type="text" name="flightNo_<?php echo $m ;?>" value="<?php echo $flightRows['flightNo'];?>"  >
				                    </td>
				                    <td>
				                        <input type="text" data-dp="date_pick" name="airJDate_<?php echo $m ;?>" value="<?php echo $App->dbformat_date($flightRows['jDate']);?>"  >
				                    </td>
				                    <td>
				                        <input type="text" name="airTime_<?php echo $m ;?>" value="<?php echo $flightRows['airTime'];?>">
				                    </td>
				                    <td>
				                        <input type="text" data-dp="date_pick" name="airADate_<?php echo $m ;?>" value="<?php echo $flightRows['aDate'];?>">
				                    </td>
				                    <td>
				                        <input type="text" name="airATime_<?php echo $m ;?>" value="<?php echo $flightRows['aTime'];?>">
				                    </td>
				                    <td>
				                        <input type="text" name="sectorFrom_<?php echo $m ;?>" value="<?php echo $flightRows['sectorFrom'];?>"  >
				                    </td>
				                    <td>
				                        <input type="text" name="sectorTo_<?php echo $m ;?>" value="<?php echo $flightRows['sectorTo'];?>"  >
				                    </td>
				                    <td>
				                        <input type="text" name="class_<?php echo $m ;?>"  value="<?php echo $flightRows['class'];?>"  >
				                    </td>
				                   	<td> <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_airline_row">Delete</button> </td>
				                </tr>
				                <?php }
				                }
				                ?>
				                </tbody>
				            </table>
				            
				        </div>
				        
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Remarks</label>
				                    <input type="text" name="airRemark"  value="<?php echo $airRow['remark']; ?>"   >
				                </div>
				            </div>
				        </div>
				        <h3>Total</h3>
				         <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Adult fare</label>
				                    <input type="text" name="adultChargeAir" data-cal="air_live_cal" value="<?php echo $airRow['adultCharge']; ?>">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Child fare</label>
				                    <input type="text" name="childChargeAir" data-cal="air_live_cal" value="<?php echo $airRow['childCharge']; ?>">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Infant fare</label>
				                    <input type="text" name="infantChargeAir" data-cal="air_live_cal" value="<?php echo $airRow['infantCharge']; ?>">
				                </div>
				            </div>
				        </div>
				        <div class="row">           
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Tax</label>
				                    <input type="text" name="airTax" data-cal="air_live_cal" value="<?php echo $airRow['tax']; ?>">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Yq</label>
				                    <input type="text" name="airYq" data-cal="air_live_cal" value="<?php echo $airRow['yq']; ?>">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Processing Charge</label>
				                    <input type="text" name="airProCharge" data-cal="air_live_cal" value="<?php echo $airRow['procCharge']; ?>">
				                </div>
				            </div>
				        </div>
				        
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Adult Total</label>
				                    <input type="text" name="adultTotalAir" readonly value="<?php echo $airRow['adultTotal']; ?>">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Child Total</label>
				                    <input type="text" name="childTotalAir" readonly value="<?php echo $airRow['childTotal']; ?>">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Infant Total</label>
				                    <input type="text" name="infantTotalAir" readonly value="<?php echo $airRow['infantTotal']; ?>">
				                </div>
				            </div>
				        </div>
				        <h3>Purchase</h3>
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Supplier</label>
				                    <select name="supplierIdAir">
				                        <option value="">Select</option>
				                        <?php
				                        $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check ";
				                        $result = $db->query($select);
				                        while ($row = mysql_fetch_array($result)) {
				                            ?>
				                            <option
				                                value="<?php echo $row['ID'] ?>" <?php if($row['ID']==$airRow['supplierId']){echo "selected";}?>><?php echo $row['accountName'] ?></option>
				                            <?php
				                        }
				                        ?>
				                    </select>
				                </div>
				            </div>
				        </div>
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Adult Total</label>
				                    <input type="text" name="prAirAdult" value="<?php echo $airRow['a2adultTotal'];?>" >
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Child Total</label>
				                    <input type="text" name="prAirChild" value="<?php echo $airRow['a2childTotal'];?>">
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Infant Total</label>
				                    <input type="text" name="prAirInfant" value="<?php echo $airRow['a2infantTotal'];?>">
				                </div>
				            </div>
				        </div>
				    </div>
				<?php
				}?>	
			<!----------------------------------Hotel---------------------------------->
				<?php if(in_array('hotel',$services))
				{
					$hotQry		=	mysql_query("SELECT h1.ID,
														h1.hotelId,
														h1.checkinDate,
														h1.checkoutDate,
														h1.noOfDays,
														h1.supplierId
												   FROM ".TABLE_PACKAGE_HOTEL." as h1
												  WHERE h1.packageId=$editId");

					?>
					<div class="package_dynamic_box pack_dynamic_hotel">
				    <h3 class="pack_dynamic_head">Hotel</h3>
				        <div class="pack_hotel_table_wrap">
				            <table class="table table-bordered bd_table hotel_outer_table">
				                <thead>
				                <tr class="table_head">
				                    <th colspan="7">Hotel Details</th>
				                    <th colspan="3">
				                         <button class="bd_btn bd_btn_blue add_package_hotel_reg_row" type="button">
				                            <i class="ion ion-plus"></i>Add New
				                        </button>
				                    </th>
				                </tr>
				                <tr>
				                    <th>Sl No</th>
				                    <th>Hotel</th>
				                    <th>Check In Date</th>
				                    <th>Check Out Date</th>
				                    <th>Number of Days</th>
				                    <th>Supplier</th>
				                    <th colspan="3">Room Details</th>
				                    <th>Action</th>
				                </tr>
				                </thead>
				                <tbody>
				                <?php 
				                $h	=	0;
				                $hotelNum	=	mysql_num_rows($hotQry);
				                if($hotelNum>0)
				                {
								
				                while($hotRow	=mysql_fetch_array($hotQry)) 
				                { 
				                $package_hotelId	=	$hotRow['ID'];
				                ?>
				                    <tr>
				                        <td><?php echo ++$h;?></td>
				                        <td>
				                            <select name="hot<?php echo $h;?>_hotelName"  >
				                                <option value="">Select</option>
				                                <?php
				                                $hotelQry = mysql_query("SELECT ID,hotelName FROM " . TABLE_HOTEL . " where $check");
				                                while ($hotelRow = mysql_fetch_array($hotelQry)) {
				                                    ?>
				                                    <option
				                                        value="<?php echo $hotelRow['ID'] ?>" <?php if($hotelRow['ID']==$hotRow['hotelId']){echo "selected";}?>><?php echo $hotelRow['hotelName'] ?></option>
				                                    <?php
				                                }
				                                ?>
				                            </select>
				                        </td>
				                        <td>
				                            <input type="text" name="hot<?php echo $h;?>_hotelcheckinDate" data-dp="date_pick" value="<?php echo $App->dbformat_date($hotRow['checkinDate']);?>"  >
				                        </td>
				                        <td>
				                            <input type="text" name="hot<?php echo $h;?>_hotelcheckoutDate" data-dp="date_pick" value="<?php echo  $App->dbformat_date($hotRow['checkoutDate']);?>"  >
				                        </td>
				                        <td>
				                            <input type="text" name="hot<?php echo $h;?>_noOfDays" value="<?php echo  $hotRow['noOfDays'];?>"  >
				                        </td>
				                        <td>
                                            <select name="hot<?php echo $h;?>_supplierId"  >
                                                <option value="">Select</option>
                                                <?php
                                                $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check";
                                                $result = $db->query($select);
                                                while ($row = mysql_fetch_array($result)) {
                                                    ?>
                                                    <option
                                                        value="<?php echo $row['ID'] ?>" <?php if($row['ID']==$hotRow['supplierId']){echo "selected";}?>><?php echo $row['accountName'] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
										
				                        <td colspan="3">
				                            <table class="table table-bordered bd_table hotel_inner_table">
				                                <thead>
				                                <tr>
				                                    <th>Room Type</th>
				                                    <th>Charge</th>
				                                    <th>Other Charges</th>
				                                    <th>Invoice Total</th>
				                                    <th>Purchase Total</th>
				                                </tr>
				                                </thead>
				                                <tbody>
				                                <?php $roomQry = mysql_query("SELECT h2.roomId,
																					h2.rate,
																					h2.otherCharge,
																					h2.total,
																					h2.purchaseAmount,
																					h3.roomType
																			   FROM ".TABLE_PACKAGE_HOTEL_ROOM_TYPE." as h2
																			   LEFT JOIN ".TABLE_ROOM_TYPE." as h3 ON h3.ID=h2.roomId
																			  WHERE h2.package_hotelId=$package_hotelId
																			  ORDER BY roomId ASC");
												$hi=	0;
												if(mysql_num_rows($roomQry)>0){
				                                while ($roomRow = mysql_fetch_array($roomQry)) {
				                                	$hi++;
				                                	
				                                $constant	=	1;
												if($roomRow['roomId']==2)
												{
													$constant	=	2;
												}
												if($roomRow['roomId']==3)
												{
													$constant	=	3;
												}
												if($roomRow['roomId']==4)
												{
													$constant	=	4;
												}
												if($roomRow['roomId']==5)
												{
													$constant	=	5;
												}
												if($roomRow['roomId']==6)
												{
													$constant	=	6;
												}
												
												$hotRoomRate				=	$roomRow['rate']*$constant;	
												$hotRoomOtherCharge			=	$roomRow['otherCharge']*$constant;	
												$hotRoomTotal				=	$hotRoomRate+$hotRoomOtherCharge;	
												$hotRoomPurchaseAmount		=	$roomRow['purchaseAmount']*$constant;
												
				                                    ?>
				                                    <tr>
				                                        <td style="min-width: 85px;">
				                                            <?= $roomRow['roomType']; ?>
				                                            <input type="hidden" name="hot<?php echo $h;?>_roomId_<?= $hi; ?>" value="<?= $roomRow['roomId']; ?>"  >
				                                        </td>
				                                        <td style="min-width: 85px; max-width: 85px;">
				                                            <input value="<?= $hotRoomRate; ?>" type="text" name="hot<?php echo $h;?>_roomcharge_<?= $hi; ?>" data-cal="room_live_cal" data-cal_field="charge"  >
				                                        </td>
				                                        <td style="min-width: 85px; max-width: 85px;">
				                                            <input value="<?= $hotRoomOtherCharge; ?>" type="text" name="hot<?php echo $h;?>_roomOtherCharge_<?= $hi; ?>" data-cal="room_live_cal" data-cal_field="other_charge" >
				                                        </td>
				                                        <td style="min-width: 85px; max-width: 85px;">
				                                            <input value="<?= $hotRoomTotal; ?>" type="text" name="hot<?php echo $h;?>_roomTotal_<?= $hi; ?>" readonly="" data-cal_field="room_total" >
				                                        </td>
				                                        
				                                        <td style="min-width: 85px; max-width: 85px;">
				                                            <input value="<?= $hotRoomPurchaseAmount; ?>" type="text" name="hot<?php echo $h;?>_purchaseTotal_<?= $hi; ?>"  >
				                                        </td>
				                                    </tr>
				                                    <?php
				                                   }
				                                }
				                                else
				                                {
				                                	$roomQry = mysql_query("select * FROM  " . TABLE_ROOM_TYPE . " WHERE proId='-1' ORDER BY ID ASC ");
                                					$hi=	0;
                                					while ($roomRow = mysql_fetch_array($roomQry)) {
                                						$hi++;
				                                	?>
				                                <tr>
				                                        
		                                            <td style="min-width: 85px;">
			                                            <?= $roomRow['roomType']; ?>
			                                            <input type="hidden" name="hot<?php echo $h;?>_roomId_<?= $hi; ?>" value="<?= $roomRow['ID']; ?>">
			                                        </td>
			                                        <td style="min-width: 85px; max-width: 85px;">
			                                            <input type="text" name="hot<?php echo $h;?>_roomcharge_<?= $hi; ?>" data-cal="room_live_cal" data-cal_field="charge"  >
			                                        </td>
			                                        <td style="min-width: 85px; max-width: 85px;">
			                                            <input type="text" name="hot<?php echo $h;?>_roomOtherCharge_<?= $hi; ?>" data-cal="room_live_cal" data-cal_field="other_charge" >
			                                        </td>
			                                        <td style="min-width: 85px; max-width: 85px;">
			                                            <input type="text" name="hot<?php echo $h;?>_roomTotal_<?= $hi; ?>" readonly="" data-cal_field="room_total" >
			                                        </td>
			                                        
			                                        <td style="min-width: 85px; max-width: 85px;">
			                                            <input  type="text" name="hot<?php echo $h;?>_purchaseTotal_<?= $hi; ?>"  >
			                                        </td>
			                                    </tr>
				                               <?php }} ?>
				                                </tbody>
				                            </table>
				                        </td>
				                        <td>
				                            <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_hotel_row">Delete</button>
				                        </td>
				                    </tr>
				                    <?php }	
								}?>
				                </tbody>
				            </table>
				        </div>
				    </div>
				<?php
				}?>	
			<!----------------------------------Visa---------------------------------->
				<?php if(in_array('visa',$services))
				{
					$visaQry	=	"SELECT v1.visaFee,
											v1.visaFor,
											v1.vfs,
											v1.ddCharge,
											v1.serviceCharge,
											v1.otherCharge,
											v1.courierCharge,
											v1.total,
											v1.supplierId as v2SupplierId,
											v1.purchaseAmount as v2Total
									   FROM ".TABLE_PACKAGE_VISA." AS v1
									  WHERE v1.packageId=$editId
								   ORDER BY v1.ID ASC
									";
					$visaResult	= 	mysql_query($visaQry);
					?>
					<div class="package_dynamic_box pack_dynamic_visa">
			        <h3 class="pack_dynamic_head">Visa</h3>

			        <div class="pack_transportation_table_wrap">
			            <table class="table table-bordered bd_table ">
			                <thead>
			                <tr class="table_head">
			                    <th colspan="10">Visa Details</th>
			                    <th colspan="2">
			                        <button class="bd_btn bd_btn_blue add_package_visa_reg_row" type="button">
			                            <i class="ion ion-plus"></i>Add New
			                        </button>
			                    </th>
			                </tr>
			                <tr>
			                    <th>Sl No</th>
			                    <th>Visa For</th>
			                    <th>Visa Fee</th>
			                    <th>VFS</th>
			                    <th>DD Charge</th>
			                    <th>Service Charge</th>
			                    <th>Other Charge</th>
			                    <th>Courier Charge</th>
			                    <th>Total</th>
			                    <th>Supplier</th>
			                    <th>Purchase Amt</th>
			                    <th>Action</th>
			                </tr>
			                </thead>
			                <tbody>
			                <?php 
			                $v	=	0;
			                $visaNum	=	mysql_num_rows($visaResult);
			                if($visaNum>0)
			                {
			                while($visaRow	=	mysql_fetch_array($visaResult)){?>
			                <tr>
			                    <td><?php echo ++$v;?></td>
			                     <td>
			                        <select name="visaFor_<?php echo $v;?>" data-cal="visa_live_cal" >
			                        	<option value="">Select</option>
			                            <?php 
			                            $visaQry1	=	mysql_query("SELECT * FROM ".TABLE_VISAFOR." where $check");
			                            while($visaRow1	=	mysql_fetch_array($visaQry1))
			                            {
			                            ?>
										<option value="<?php echo $visaRow1['ID']; ?>" <?php if($visaRow1['ID']==$visaRow['visaFor']){ echo "selected";}?>><?php echo $visaRow1['visafor'];?></option>
										<?php }?>
			                        </select>
			                    </td>
			                    <td>
			                        <input type="text" name="visaFee_<?php echo $v;?>" value="<?php echo $visaRow['visaFee'];?>" data-cal="visa_live_cal" >
			                    </td>
			                    <td>
			                        <input type="text" name="vfs_<?php echo $v;?>" value="<?php echo $visaRow['vfs'];?>" data-cal="visa_live_cal" >
			                    </td>
			                    <td>
			                        <input type="text" name="ddCharge_<?php echo $v;?>" value="<?php echo $visaRow['ddCharge'];?>" data-cal="visa_live_cal" >
			                    </td>
			                    <td>
			                        <input type="text" name="serviceCharge_<?php echo $v;?>" value="<?php echo $visaRow['serviceCharge'];?>" data-cal="visa_live_cal" >
			                    </td>
			                    <td>
			                        <input type="text" name="visaOtherCharge_<?php echo $v;?>" value="<?php echo $visaRow['otherCharge'];?>" data-cal="visa_live_cal" >
			                    </td>
			                    <td>
			                        <input type="text" name="courierCharge_<?php echo $v;?>" value="<?php echo $visaRow['courierCharge'];?>" data-cal="visa_live_cal" >
			                    </td>
			                    <td>
			                        <input type="text" name="visaTotal_<?php echo $v;?>" value="<?php echo $visaRow['total'];?>" readonly >
			                    </td>
			                    <td>
			                        <select name="supplierIdVisa_<?php echo $v;?>"  >
			                            <option value="">Select</option>
			                            <?php
			                            $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check ";
			                            $result = $db->query($select);
			                            while ($row = mysql_fetch_array($result)) {
			                                ?>
			                                <option
			                                    value="<?php echo $row['ID'] ?>" <?php if($visaRow['v2SupplierId']==$row['ID']){ echo "selected";}?>><?php echo $row['accountName'] ?></option>
			                                <?php
			                            }
			                            ?>
			                        </select>
			                    </td>
			                    <td>
			                        <input type="text" name="prVisaTotal_<?php echo $v;?>" value="<?php echo $visaRow['v2Total'];?>"  >
			                    </td>
			                    <td>
                        <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_visa_row">Delete</button>
                    </td>
			                    
			                </tr>
			                <?php }
							}?>
			                </tbody>
			            </table>
			        </div>
			    </div>
				<?php
				}?>	
			<!----------------------------------Transportation---------------------------------->
				<?php if(in_array('transportation',$services))
				{
					$transportQry	=	"SELECT t1.vehicle,
												t1.seatCharge,
												t1.remark,
												t1.supplierId,
												t1.purchaseAmount
										   FROM ".TABLE_PACKAGE_TRANSPORTATION." AS t1
										  WHERE	t1.packageId=$editId
									   ORDER BY	t1.ID ASC
										";
					$transportResult	=	mysql_query($transportQry);
					
					?>
					<div class="package_dynamic_box pack_dynamic_transportation">
			        <h3 class="pack_dynamic_head">Transportation</h3>

			        <div class="pack_transportation_table_wrap">
			            <table class="table table-bordered bd_table">
			                <thead>
			                <tr class="table_head">
			                    <th colspan="5">Transportation Details</th>
			                    <th colspan="2">
			                      <button class="bd_btn bd_btn_blue add_package_transportation_reg_row" type="button">
		                            <i class="ion ion-plus"></i>Add New
		                         </button>
			                    </th>
			                </tr>
			                <tr>
			                    <th>Sl No</th>
			                    <th>Vehicle</th>
			                    <th>Seat Charge</th>
			                    <th>Remark</th>
			                    <th>Supplier</th>
			                    <th>Purchase Amt</th>
			                    <th>Action</th>
			                </tr>
			                </thead>
			                <tbody>
			                <?php
			                $t=0;
			                $transNum	=	mysql_num_rows($transportResult);
			                if($transNum>0){
			                while($transportRow=mysql_fetch_array($transportResult)){?>
			                <tr>
			                    <td><?php echo ++$t;?></td>
			                    <td>
			                        <select name="vehicle_<?php echo $t;?>"  >
			                            <?php
			                            $transQry = mysql_query("SELECT * FROM " . TABLE_VEHICLE_TYPES." where $check");
			                            while ($transRow = mysql_fetch_array($transQry)) {
			                                ?>
			                                <option
			                                    value="<?php echo $transRow['ID']; ?>" <?php if($transRow['ID']==$transportRow['vehicle']){echo "selected";}?>><?php echo $transRow['vehicleType']; ?></option>
			                            <?php } ?>
			                        </select>
			                    </td>
			                    <td>
			                        <input type="text" name="seatCharge_<?php echo $t;?>" value="<?php echo $transportRow['seatCharge'];?>" data-cal="package_reg_live_cal" >
			                    </td>
			                    <td>
			                        <input type="text" name="transRemark_<?php echo $t;?>" value="<?php echo $transportRow['remark'];?>"  >
			                    </td>
			                    <td>
			                        <select name="supplierIdTrans_<?php echo $t;?>"  >
			                            <option value="">Select</option>
			                            <?php
			                            $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check";
			                            $result = $db->query($select);
			                            while ($row = mysql_fetch_array($result)) {
			                                ?>
			                                <option
			                                    value="<?php echo $row['ID'] ?>" <?php if($row['ID']==$transportRow['supplierId']){echo "selected";}?>><?php echo $row['accountName'] ?></option>
			                                <?php
			                            }
			                            ?>
			                        </select>
			                    </td>
			                    <td>
			                        <input type="text" name="prTransTotal_<?php echo $t;?>" value="<?php echo $transportRow['purchaseAmount'];?>"  >
			                    </td>
			                    <td>
			                        <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_transportation_row" >Delete</button>
			                    </td>
			                </tr>
			                <?php }
							}?>
			                </tbody>
			            </table>
			        </div>
			    </div>
			
				<?php
				}?>
		   <!---------------------------------------- End --------------------------------------->
		   	</div>
            <!--</div>-->
            <div class="row" style="margin-top: 30px;">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>TDS</label>
                        <input type="text" name="ptds" id="ptds" onfocus="clearbox('ptdsdiv')" data-cal="package_reg_live_cal" value="<?php echo $editRow['tds']; ?>">
                        <div id="ptdsdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
               <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Tax(Amount)</label>
                        <input type="text" name="pTax" id="pTax" onfocus="clearbox('pTaxdiv')" data-cal="package_reg_live_cal" value="<?php echo $editRow['tax']; ?>" >
                        <div id="pTaxdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
                 <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Other Charges</label>
                        <input type="text" name="pOtherCharges" id="pOtherCharges" onfocus="clearbox('pOtherChargesdiv')" data-cal="package_reg_live_cal" value="<?php echo $editRow['otherCharge']; ?>">
                        <div id="pOtherChargesdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
            </div>
           
            <div class="form_section_head"><h3>Total</h3></div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Adult</label>
                        <input type="text" name="adultTotal" id="adultTotal" required="" onfocus="clearbox('adultTotaldiv')" readonly value="<?php echo $editRow['adultTotal']; ?>">
                        <div id="adultTotaldiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Child</label>
                        <input type="text" name="childTotal" id="childTotal" required="" onfocus="clearbox('childTotaldiv')" readonly value="<?php echo $editRow['childTotal']; ?>">
                        <div id="childTotaldiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Infant</label>
                        <input type="text" name="infantTotal" id="infantTotal" required="" onfocus="clearbox('infantTotaldiv')" value="<?php echo $editRow['infantTotal']; ?>" readonly>
                        <div id="infantTotaldiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
            </div>
             <div class="row" style="margin-top: 30px;">
            	<div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Discount</label>
                        <input type="text" name="pDiscount" id="pDiscount" <?php if($editRow['discount']!=0 or $editRow['extra']!=0){?> disabled="" <?php }?> value="<?php echo $editRow['discount']; ?>" onfocus="clearbox('pDiscountDiv')"  >
                        <div id="pDiscountDiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Extra Charge</label>
                        <input type="text" name="pExtra" id="pExtra" <?php if($editRow['discount']!=0 or $editRow['extra']!=0){?> disabled="" <?php }?> value="<?php echo $editRow['extra']; ?>" onfocus="clearbox('pExtraDiv')" >
                        <div id="pExtraDiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
            </div>
            <div class="airline_submit">
                <input type="hidden" name="pack_reg_air_row_count" id="pack_reg_air_row_count"  <?php if(@$flightNum>0){ echo "value='$flightNum'" ; }?> value="1">
                <input type="hidden" name="pack_reg_visa_row_count" id="pack_reg_visa_row_count" <?php if(@$visaNum>0){ echo "value='$visaNum'" ; }?> value="1">
                <input type="hidden" name="pack_reg_transportation_row_count" id="pack_reg_transportation_row_count" <?php if(@$transNum>0){ echo "value='$transNum'" ; }?> value="1">
                <input type="hidden" name="pack_reg_hotel_row_count" id="pack_reg_hotel_row_count" <?php if(@$hotelNum>0){ echo "value='$hotelNum'" ; }?> value="1">
                <input type="submit" value="UPDATE" onclick="return check()">
                
                <?php if($editRow['confirm']==1){?>
                	<input type="checkbox"  disabled=""  name="confirm1" checked="" /> Confirm
                	<input name="confirm" value="1" type="hidden"/>
              <?php } else { ?>
              		<input type="checkbox" name="confirm" /> Confirm
              <?php }?>
            </div>
        </div>
    </form>
    
</div>
<?php
require('../admin_footer1.php');
?>
<div class="package_dynamic_source" style="display: none">
    <div class="package_dynamic_box pack_dynamic_airline">
        <h3 class="pack_dynamic_head">Airline</h3>
        <div class="pack_air_table_wrap">
            <table class="table table-bordered bd_table ">
                <thead>
                <tr class="table_head">
                    <th colspan="6">Airline Details</th>
                    <th colspan="2">
                        <button class="bd_btn bd_btn_blue add_package_airline_reg_row" type="button">
                            <i class="ion ion-plus"></i>Add New
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Sl No</th>
                    <th>Airline</th>
                    <th>Flight No</th>
                    <th>Journey Date</th>
                    <th>Time</th>
                    <th>Sector From</th>
                    <th>Sector To</th>
                    <th>Class</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>
                        <select name="airlineName_1">
                            <option value="">Select</option>
                            <?php
                            $airQry = mysql_query("select ID,flightName FROM " . TABLE_FLIGHT . " WHERE $check");
                            while ($airRow = mysql_fetch_array($airQry)) {
                                ?>
                                <option
                                    value="<?php echo $airRow['ID']; ?>"><?php echo $airRow['flightName']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="flightNo_1">
                    </td>
                    <td>
                        <input type="text" data-dp="date_pick" name="airJDate_1">
                    </td>
                    <td>
                        <input type="text" name="airTime_1">
                    </td>
                    <td>
                        <input type="text" name="sectorFrom_1">
                    </td>
                    <td>
                        <input type="text" name="sectorTo_1">
                    </td>
                    <td>
                        <input type="text" name="class_1">
                    </td>
                    <td>
                        <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_airline_row">Delete</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Remarks</label>
                    <input type="text" name="airRemark">
                </div>
            </div>
        </div>
        <h3>Total</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult fare</label>
                    <input type="text" name="adultChargeAir" data-cal="air_live_cal">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child fare</label>
                    <input type="text" name="childChargeAir" data-cal="air_live_cal">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Infant fare</label>
                    <input type="text" name="infantChargeAir" data-cal="air_live_cal">
                </div>
            </div>
        </div>
        <div class="row">           
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Tax</label>
                    <input type="text" name="airTax" data-cal="air_live_cal">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Yq</label>
                    <input type="text" name="airYq" data-cal="air_live_cal">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Processing Charge</label>
                    <input type="text" name="airProCharge" data-cal="air_live_cal">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult Total</label>
                    <input type="text" name="adultTotalAir" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child Total</label>
                    <input type="text" name="childTotalAir" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Infant Total</label>
                    <input type="text" name="infantTotalAir" readonly>
                </div>
            </div>
        </div>
        <h3>Purchase</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Supplier</label>
                    <select name="supplierIdAir">
                        <option value="">Select</option>
                        <?php
                        $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check";
                        $result = $db->query($select);
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <option
                                value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult Total</label>
                    <input type="text" name="prAirAdult">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child Total</label>
                    <input type="text" name="prAirChild">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Infant Total</label>
                    <input type="text" name="prAirInfant">
                </div>
            </div>
        </div>
    </div>
    <div class="package_dynamic_box pack_dynamic_hotel">
        <h3 class="pack_dynamic_head">Hotel</h3>
        <div class="pack_hotel_table_wrap">
            <table class="table table-bordered bd_table hotel_outer_table">
                <thead>
                <tr class="table_head">
                    <th colspan="7">Hotel Details</th>
                    <th colspan="3">
                        <button class="bd_btn bd_btn_blue add_package_hotel_reg_row" type="button">
                            <i class="ion ion-plus"></i>Add New
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Sl No</th>
                    <th>Hotel</th>
                    <th>Check In Date</th>
                    <th>Check Out Date</th>
                    <th>Number of Days</th>
                    <th>Supplier</th>
                    <th colspan="3">Room Details</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>
                            <select name="hot1_hotelName">
                                <option value="">Select</option>
                                <?php
                                $hotelQry = mysql_query("SELECT ID,hotelName FROM " . TABLE_HOTEL . " where $check");
                                while ($hotelRow = mysql_fetch_array($hotelQry)) {
                                    ?>
                                    <option
                                        value="<?php echo $hotelRow['ID'] ?>"><?php echo $hotelRow['hotelName'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" name="hot1_hotelcheckinDate" data-dp="date_pick">
                        </td>
                        <td>
                            <input type="text" name="hot1_hotelcheckoutDate" data-dp="date_pick">
                        </td>
                        <td>
                            <input type="text" name="hot1_noOfDays" readonly="">
                        </td>
                         <td>
                            <select name="hot1_supplierId">
                                <option value="">Select</option>
                                <?php
                                $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check";
                                $result = $db->query($select);
                                while ($row = mysql_fetch_array($result)) {
                                    ?>
                                    <option
                                        value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td colspan="3">
                            <table class="table table-bordered bd_table hotel_inner_table">
                                <thead>
                                <tr>
                                    <th>Room Type</th>
                                    <th>Charge</th>
                                    <th>Other Charges</th>
                                    <th>Invoice Total</th>
                                    <th>Purchase Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $roomQry = mysql_query("select * FROM  " . TABLE_ROOM_TYPE . " WHERE proId='-1'");
                                $i = 1;
                                while ($roomRow = mysql_fetch_array($roomQry)) {
                                    ?>
                                    <tr>
                                        <td style="min-width: 85px;">
                                            <?= $roomRow['roomType']; ?>
                                            <input type="hidden" name="hot1_roomId_<?= $i; ?>" value="<?= $roomRow['ID']; ?>">
                                        </td>
                                        <td style="min-width: 85px; max-width: 85px;">
                                            <input type="text" data-cal="room_live_cal" data-cal_field="charge" name="hot1_roomcharge_<?= $i; ?>">
                                        </td>
                                        <td style="min-width: 85px; max-width: 85px;">
                                            <input type="text" data-cal="room_live_cal" data-cal_field="other_charge" name="hot1_roomOtherCharge_<?= $i; ?>">
                                        </td>
                                        <td style="min-width: 85px; max-width: 85px;">
                                            <input type="text" readonly="" data-cal_field="room_total" name="hot1_roomTotal_<?= $i; ?>">
                                        </td>
                                       
                                        <td style="min-width: 85px; max-width: 85px;">
                                            <input type="text" name="hot1_purchaseTotal_<?= $i; ?>">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_hotel_row">Delete</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="package_dynamic_box pack_dynamic_visa">
        <h3 class="pack_dynamic_head">Visa</h3>

        <div class="pack_transportation_table_wrap">
            <table class="table table-bordered bd_table ">
                <thead>
                <tr class="table_head">
                    <th colspan="10">Visa Details</th>
                    <th colspan="2">
                        <button class="bd_btn bd_btn_blue add_package_visa_reg_row" type="button">
                            <i class="ion ion-plus"></i>Add New
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Sl No</th>
                    <th>Visa For</th>
                    <th>Visa Fee</th>
                    <th>VFS</th>
                    <th>DD Charge</th>
                    <th>Service Charge</th>
                    <th>Other Charge</th>
                    <th>Courier Charge</th>
                    <th>Total</th>
                    <th>Supplier</th>
                    <th>Purchase Amt</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                     <td>
                        <select name="visaFor_1" data-cal="visa_live_cal">
                            <option value="">Select</option>
                            <?php 
                            $visaQry	=	mysql_query("SELECT * FROM ".TABLE_VISAFOR." where $check");
                            while($visaRow	=	mysql_fetch_array($visaQry))
                            {
                            ?>
							<option value="<?php echo $visaRow['ID']; ?>"><?php echo $visaRow['visafor'];?></option>
							<?php }?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="visaFee_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="vfs_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="ddCharge_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="serviceCharge_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="visaOtherCharge_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="courierCharge_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="visaTotal_1" readonly>
                    </td>
                    <td>
                        <select name="supplierIdVisa_1">
                            <option value="">Select</option>
                            <?php
                            $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check";
                            $result = $db->query($select);
                            while ($row = mysql_fetch_array($result)) {
                            ?>
                            <option
                                value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="prVisaTotal_1">
                    </td>
                    <td>
                        <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_visa_row">Delete</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="package_dynamic_box pack_dynamic_transportation">
        <h3 class="pack_dynamic_head">Transportation</h3>

        <div class="pack_transportation_table_wrap">
            <table class="table table-bordered bd_table">
                <thead>
                <tr class="table_head">
                    <th colspan="5">Transportation Details</th>
                    <th colspan="2">
                        <button class="bd_btn bd_btn_blue add_package_transportation_reg_row" type="button">
                            <i class="ion ion-plus"></i>Add New
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Sl No</th>
                    <th>Vehicle</th>
                    <th>Seat Charge</th>
                    <th>Remark</th>
                    <th>Supplier</th>
                    <th>Purchase Amt</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>
                        <select name="vehicle_1">
                            <?php
                            $transQry = mysql_query("SELECT * FROM " . TABLE_VEHICLE_TYPES." where $check");
                            while ($transRow = mysql_fetch_array($transQry)) {
                                ?>
                                <option
                                    value="<?php echo $transRow['ID']; ?>"><?php echo $transRow['vehicleType']; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="seatCharge_1" data-cal="package_reg_live_cal">
                    </td>
                    <td>
                        <input type="text" name="transRemark_1">
                    </td>
                    <td>
                        <select name="supplierIdTrans_1">
                            <option value="">Select</option>
                            <?php
                            $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check";
                            $result = $db->query($select);
                            while ($row = mysql_fetch_array($result)) {
                                ?>
                                <option
                                    value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="prTransTotal_1">
                    </td>
                    <td>
                        <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_transportation_row">Delete</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
require('../admin_footer2.php');
?>
