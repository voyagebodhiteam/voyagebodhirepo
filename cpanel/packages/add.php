<?php
require("../admin_header.php");
if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';

$userId	=	$_SESSION['travelUserId'];

if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"proId='$proId'";
}

?>

<script>

	function valid()
	{
		flag=false;

		ptds = document.getElementById('ptds').value;	
			if(isNaN(ptds) && ptds!=0)
			{		
				document.getElementById('ptdsdiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		pTax = document.getElementById('pTax').value;	
			if(isNaN(pTax) && pTax!=0)
			{		
				document.getElementById('pTaxdiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		pOtherCharges = document.getElementById('pOtherCharges').value;	
			if(isNaN(pOtherCharges) && pOtherCharges!=0)
			{		
				document.getElementById('pOtherChargesdiv').innerHTML="Enter valid amount.";
				flag=true;
			}
	    adultTotal = document.getElementById('adultTotal').value;	
			if(isNaN(adultTotal) && adultTotal!=0)
			{		
				document.getElementById('adultTotaldiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		childTotal = document.getElementById('childTotal').value;	
			if(isNaN(childTotal) && childTotal!=0)
			{		
				document.getElementById('childTotaldiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		infantTotal = document.getElementById('infantTotal').value;	
			if(isNaN(infantTotal) && infantTotal!=0)
			{		
				document.getElementById('infantTotaldiv').innerHTML="Enter valid amount.";
				flag=true;
			}
			
		airline		=	document.getElementById('airline').checked;
		visa		=	document.getElementById('visa').checked;
		hotel		=	document.getElementById('hotel').checked;
		transport	=	document.getElementById('transport').checked;
		
			if((!airline)&&(!visa)&&(!hotel)&&(!transport)) 
			{
				document.getElementById('servicesdiv').innerHTML="Select atleast one.";
				flag=true;
			}
		var exdate	=	document.getElementById('expirydiv').value;
			if(exdate == 1)
			{
				document.getElementById('dDiv').innerHTML = "Invalid Date";
				flag	=	true;
			}
		if(flag==true)
		{
		return false;
		}
	}
	//clear the validation msg
	function clearbox(Element_id)
	{
	document.getElementById(Element_id).innerHTML="";
	}

	function validdate()
	{
		var dDate	=	document.getElementById('dDate').value;
		var today 	= 	new Date();
		var dd 		= 	today.getDate();
		var mm 		= 	today.getMonth()+1; //January is 0!
		var yyyy 	= 	today.getFullYear();
		
		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		}
		today = dd+'-'+mm+'-'+yyyy;
		
		var dDateSplit= dDate.split('-');
		var todaySplit= today.split('-');
		
		var dDateLast 	= 	new Date(dDateSplit[2], +dDateSplit[1]-1, dDateSplit[0]);
		var todayLast 		= 	new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
		var dateDiff		=	(todayLast.getTime() - dDateLast.getTime()) / (1000*60*60*24);
		//alert(dateDiff);
		var year = Math.round(dateDiff/365);
		if(dateDiff>=0)
		{
			
			document.getElementById('expirydiv').value	=1;
			alert("Invalid Departure Date");
			return false;
		}
		
		else {
			
			document.getElementById('expirydiv').value	='';
		}
		
	}
</script>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
<div class="bd_panel bd_panel_default bd_panel_shadow">
        <form class="default_form" method="post" id="package_reg_form" action="do.php?op=index" onsubmit="return valid()">
        <div class="bd_panel_head">
            <h3>Create Package</h3>
        </div>
        <div class="bd_panel_body">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Package Name <span class="valid">*</span></label>
                        <input name="packageName" type="text" required="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2" style="display: none">
                    <div class="form_block" >
                        <label>Staff in Charge</label>
                        <select name="staff">
                            <option value="">Select</option>
                            <?php
                            $staffQry = mysql_query("SELECT * FROM " . TABLE_STAFF . " where $check");
                            while ($staffRow = mysql_fetch_array($staffQry)) {
                                ?>
                                <option
                                    value="<?php echo $staffRow['ID']; ?>" <?php if($userId==$staffRow['ID']){ echo "selected"; }?>><?php echo $staffRow['staffName']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Leader</label>
                        <input type="text" name="leader">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Mobile</label>
                        <input type="text" name="mobile">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Departure Date<span class="valid">*</span></label>
                        <input type="text" name="date" class="user_date" value="<?php $datetime = new DateTime('tomorrow');
echo $datetime->format('d-m-Y');?>" onchange="validdate()" required="" id="dDate" onclick="clearbox('dDiv')" >
						<input type="hidden" name="expirydiv" id="expirydiv" value="" />
                    </div>
                     <div id="dDiv" class="valid" style="color:#FF6600;"></div>
                </div>
               
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Details</label>
                        <input type="text" name="place">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Duration</label>
                        <input type="text" name="duration">
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Remark</label>
                        <input type="text" name="premark">
                    </div>
                </div>
            </div>
            
            <div class="form_section_head"><h3>Services <span class="valid">*</span></h3></div>
            
            <div class="package_serv_controls">
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_airline" name="airline" id="airline" onclick="clearbox('servicesdiv')" ><label
                        class="check_label">Airline</label>
                </div>
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_hotel" name="hotel" id="hotel" onclick="clearbox('servicesdiv')" ><label
                        class="check_label">Hotel</label>
                </div>
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_visa" name="visa" id="visa" onclick="clearbox('servicesdiv')" ><label
                        class="check_label">Visa</label>
                </div>
                <div class="form_block form_block_check">
                    <input type="checkbox" data-append="pack_dynamic_transportation" name="transport" id="transport" onclick="clearbox('servicesdiv')">                        
                    <label class="check_label">Transportation</label>
                </div>
            </div>
             <div id="servicesdiv" class="valid" style="color:#FF6600;"></div>
             
            <div id="package_dynamic">

            </div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>TDS</label>
                        <input type="text" name="ptds" id="ptds" onfocus="clearbox('ptdsdiv')" data-cal="package_reg_live_cal">
                        <div id="ptdsdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
               <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Tax(Amount)</label>
                        <input type="text" name="pTax" id="pTax" onfocus="clearbox('pTaxdiv')" data-cal="package_reg_live_cal">
                        <div id="pTaxdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
                 <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Other Charges</label>
                        <input type="text" name="pOtherCharges" id="pOtherCharges" onfocus="clearbox('pOtherChargesdiv')" data-cal="package_reg_live_cal">
                        <div id="pOtherChargesdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
            </div>
            <div class="form_section_head"><h3>Total</h3></div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Adult<span class="valid">*</span></label>
                        <input type="text" name="adultTotal" id="adultTotal" required="" onfocus="clearbox('adultTotaldiv')" readonly>
                        <div id="adultTotaldiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Child</label>
                        <input type="text" name="childTotal" id="childTotal" required="" onfocus="clearbox('childTotaldiv')" readonly>
                        <div id="childTotaldiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Infant</label>
                        <input type="text" name="infantTotal" id="infantTotal" required="" onfocus="clearbox('infantTotaldiv')" readonly>
                        <div id="infantTotaldiv" class="valid" style="color:#FF6600;"></div>
                    </div>
                </div>
            </div>
            <div class="airline_submit">
                <input type="hidden" name="pack_reg_air_row_count" id="pack_reg_air_row_count" value="1">
                <input type="hidden" name="pack_reg_visa_row_count" id="pack_reg_visa_row_count" value="1">
                <input type="hidden" name="pack_reg_transportation_row_count" id="pack_reg_transportation_row_count" value="1">
                <input type="hidden" name="pack_reg_hotel_row_count" id="pack_reg_hotel_row_count" value="1">
                
                <input type="submit" value="Save" onclick="return check()">
                <input type="checkbox"  name="confirm" /> Confirm
            </div>
        </div>
    </form>
</div>
<?php
require('../admin_footer1.php');
?>
<div class="package_dynamic_source" style="display: none">
    <div class="package_dynamic_box pack_dynamic_airline">
        <h3 class="pack_dynamic_head">Airline</h3>
        <div class="pack_air_table_wrap">
            <table class="table table-bordered bd_table ">
                <thead>
                <tr class="table_head">
                    <th colspan="7">Airline Details</th>
                    <th colspan="4">
                        <button class="bd_btn bd_btn_blue add_package_airline_reg_row" type="button">
                            <i class="ion ion-plus"></i>Add New
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Sl No</th>
                    <th>Airline <span class="valid">*</span></th>
                    <th>Flight No</th>
                    <th>Journey Date</th>
                    <th>Time</th>
                    <th>Arrival Date</th>
                    <th>Time</th>
                    <th>Sector From</th>
                    <th>Sector To</th>
                    <th>Class</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>
                        <select name="airlineName_1">
                            <option value="">Select</option>
                            <?php
                            $airQry = mysql_query("select ID,flightName FROM " . TABLE_FLIGHT . " WHERE $check ");
                            while ($airRow = mysql_fetch_array($airQry)) {
                                ?>
                                <option
                                    value="<?php echo $airRow['ID']; ?>"><?php echo $airRow['flightName']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="flightNo_1">
                    </td>
                    <td>
                        <input type="text" data-dp="date_pick" name="airJDate_1">
                    </td>
                    <td>
                        <input type="text" name="airTime_1">
                    </td>
                    <td>
                        <input type="text" data-dp="date_pick" name="airADate_1">
                    </td>
                    <td>
                        <input type="text" name="airATime_1">
                    </td>
                    <td>
                        <input type="text" name="sectorFrom_1">
                    </td>
                    <td>
                        <input type="text" name="sectorTo_1">
                    </td>
                    <td>
                        <input type="text" name="class_1">
                    </td>
                    <td>
                        <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_airline_row">Delete</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Remarks</label>
                    <input type="text" name="airRemark">
                </div>
            </div>
        </div>
        <h3>Total</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult fare<span class="valid">*</span></label>
                    <input type="text" name="adultChargeAir" data-cal="air_live_cal">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child fare</label>
                    <input type="text" name="childChargeAir" data-cal="air_live_cal">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Infant fare</label>
                    <input type="text" name="infantChargeAir" data-cal="air_live_cal">
                </div>
            </div>
        </div>
        <div class="row">           
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Tax</label>
                    <input type="text" name="airTax" data-cal="air_live_cal">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Yq</label>
                    <input type="text" name="airYq" data-cal="air_live_cal">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Processing Charge</label>
                    <input type="text" name="airProCharge" data-cal="air_live_cal">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult Total</label>
                    <input type="text" name="adultTotalAir" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child Total</label>
                    <input type="text" name="childTotalAir" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Infant Total</label>
                    <input type="text" name="infantTotalAir" readonly>
                </div>
            </div>
        </div>
        <h3>Purchase</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Supplier</label>
                    <select name="supplierIdAir">
                        <option value="">Select</option>
                        <?php
                        $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check ";
                        $result = $db->query($select);
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <option
                                value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Adult Total</label>
                    <input type="text" name="prAirAdult">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Child Total</label>
                    <input type="text" name="prAirChild">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Infant Total</label>
                    <input type="text" name="prAirInfant">
                </div>
            </div>
        </div>
    </div>
    <div class="package_dynamic_box pack_dynamic_hotel">
        <h3 class="pack_dynamic_head">Hotel</h3>
        <div class="pack_hotel_table_wrap">
            <table class="table table-bordered bd_table hotel_outer_table">
                <thead>
                <tr class="table_head">
                    <th colspan="7">Hotel Details</th>
                    <th colspan="3">
                        <button class="bd_btn bd_btn_blue add_package_hotel_reg_row" type="button">
                            <i class="ion ion-plus"></i>Add New
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Sl No</th>
                    <th>Hotel <span class="valid">*</span></th>
                    <th>Check In Date <span class="valid">*</span></th>
                    <th>Check Out Date <span class="valid">*</span></th>
                    <th>Number of Days <span class="valid">*</span></th>
                    <th>Supplier</th>
                    <th colspan="3">Room Details</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>
                            <select name="hot1_hotelName">
                                <option value="">Select</option>
                                <?php
                                $hotelQry = mysql_query("SELECT ID,hotelName FROM " . TABLE_HOTEL . " where $check ");
                                while ($hotelRow = mysql_fetch_array($hotelQry)) {
                                    ?>
                                    <option
                                        value="<?php echo $hotelRow['ID'] ?>"><?php echo $hotelRow['hotelName'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" name="hot1_hotelcheckinDate" data-dp="date_pick">
                        </td>
                        <td>
                            <input type="text" name="hot1_hotelcheckoutDate" data-dp="date_pick">
                        </td>
                        <td>
                            <input type="text" name="hot1_noOfDays" readonly="">
                        </td>
						<td>
                            <select name="hot1_supplierId">
                                <option value="">Select</option>
                                <?php
                                $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check ";
                                $result = $db->query($select);
                                while ($row = mysql_fetch_array($result)) {
                                    ?>
                                    <option
                                        value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td colspan="3">
                            <table class="table table-bordered bd_table hotel_inner_table">
                                <thead>
                                <tr>
                                    <th>Room Type</th>
                                    <th>Charge</th>
                                    <th>Other Charges</th>
                                    <th>Invoice Total</th>
                                    <th>Purchase Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $roomQry = mysql_query("select * FROM  " . TABLE_ROOM_TYPE . " WHERE proId='-1' ORDER BY ID ASC ");
                                $i = 1;
                                while ($roomRow = mysql_fetch_array($roomQry)) {
                                    ?>
                                    <tr>
                                        <td style="min-width: 85px;">
                                            <?= $roomRow['roomType']; ?>
                                            <input type="hidden" name="hot1_roomId_<?= $i; ?>" value="<?= $roomRow['ID']; ?>">
                                        </td>
                                        <td style="min-width: 85px; max-width: 85px;">
                                            <input type="text" data-cal="room_live_cal" value="0" data-cal_field="charge" name="hot1_roomcharge_<?= $i; ?>">
                                        </td>
                                        <td style="min-width: 85px; max-width: 85px;">
                                            <input type="text" data-cal="room_live_cal" data-cal_field="other_charge" name="hot1_roomOtherCharge_<?= $i; ?>">
                                        </td>
                                        <td style="min-width: 85px; max-width: 85px;">
                                            <input type="text" readonly="" data-cal_field="room_total" name="hot1_roomTotal_<?= $i; ?>">
                                        </td>
                                        
                                        <td style="min-width: 85px; max-width: 85px;">
                                            <input type="text" value="0" name="hot1_purchaseTotal_<?= $i; ?>">
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_hotel_row">Delete</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="package_dynamic_box pack_dynamic_visa">
        <h3 class="pack_dynamic_head">Visa</h3>

        <div class="pack_transportation_table_wrap">
            <table class="table table-bordered bd_table ">
                <thead>
                <tr class="table_head">
                    <th colspan="10">Visa Details</th>
                    <th colspan="2">
                        <button class="bd_btn bd_btn_blue add_package_visa_reg_row" type="button">
                            <i class="ion ion-plus"></i>Add New
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Sl No</th>
                    <th>Visa For<span class="valid">*</span></th>
                    <th>Visa Fee<span class="valid">*</span></th>
                    <th>VFS</th>
                    <th>DD Charge</th>
                    <th>Service Charge</th>
                    <th>Other Charge</th>
                    <th>Courier Charge</th>
                    <th>Total</th>
                    <th>Supplier</th>
                    <th>Purchase Amt</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                     <td>
                        <select name="visaFor_1" data-cal="visa_live_cal">
                            <option value="">Select</option>
                            <?php 
                            $visaQry	=	mysql_query("SELECT * FROM ".TABLE_VISAFOR." where $check ");
                            while($visaRow	=	mysql_fetch_array($visaQry))
                            {
                            ?>
							<option value="<?php echo $visaRow['ID']; ?>"><?php echo $visaRow['visafor'];?></option>
							<?php }?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="visaFee_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="vfs_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="ddCharge_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="serviceCharge_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="visaOtherCharge_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="courierCharge_1" data-cal="visa_live_cal">
                    </td>
                    <td>
                        <input type="text" name="visaTotal_1" readonly>
                    </td>
                    <td>
                        <select name="supplierIdVisa_1">
                            <option value="">Select</option>
                            <?php
                            $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check ";
                            $result = $db->query($select);
                            while ($row = mysql_fetch_array($result)) {
                            ?>
                            <option
                                value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="prVisaTotal_1">
                    </td>
                    <td>
                        <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_visa_row">Delete</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="package_dynamic_box pack_dynamic_transportation">
        <h3 class="pack_dynamic_head">Transportation</h3>

        <div class="pack_transportation_table_wrap">
            <table class="table table-bordered bd_table">
                <thead>
                <tr class="table_head">
                    <th colspan="5">Transportation Details</th>
                    <th colspan="2">
                        <button class="bd_btn bd_btn_blue add_package_transportation_reg_row" type="button">
                            <i class="ion ion-plus"></i>Add New
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Sl No</th>
                    <th>Vehicle <span class="valid">*</span></th>
                    <th>Seat Charge <span class="valid">*</span></th>
                    <th>Remark</th>
                    <th>Supplier</th>
                    <th>Purchase Amt</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>
                        <select name="vehicle_1">
                            <?php
                            $transQry = mysql_query("SELECT * FROM " . TABLE_VEHICLE_TYPES." where $check");
                            while ($transRow = mysql_fetch_array($transQry)) {
                                ?>
                                <option
                                    value="<?php echo $transRow['ID']; ?>"><?php echo $transRow['vehicleType']; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="seatCharge_1" data-cal="package_reg_live_cal">
                    </td>
                    <td>
                        <input type="text" name="transRemark_1">
                    </td>
                    <td>
                        <select name="supplierIdTrans_1">
                            <option value="">Select</option>
                            <?php
                            $select = "select  ID,accountName from " . TABLE_ACCOUNTS . " where accountGroupId='40' AND $check";
                            $result = $db->query($select);
                            while ($row = mysql_fetch_array($result)) {
                                ?>
                                <option
                                    value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="prTransTotal_1">
                    </td>
                    <td>
                        <button class="bd_btn bd_btn_red" type="button" data-action="del_pack_reg_transportation_row">Delete</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
require('../admin_footer2.php');
?>
