<?php
require('../admin_header.php');

if(@isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
$editId		=	$_REQUEST['id'];
$editSelect = 	"SELECT A.ID,
						A.voucherNo,
						A.voucherType,
						A.credit,
						A.remark,
						A.transactionDate,
						A.fromLedger as fromId,
						A.toLedger as toId,
						B.accountName as toLedger,
						C.accountName as fromLedger  
						FROM `".TABLE_TRANSACTION."` A,`".TABLE_ACCOUNTS."` B,`".TABLE_ACCOUNTS."` C 
						WHERE (A.voucherType='Bank Receipts' AND A.credit!='0' AND A.fromLedger=C.ID AND A.toLedger=B.ID) 
						AND  A.ID = $editId ";
						//echo $editSelect;
						$editResult 	= 	$db->query($editSelect);
						$editRow 		= 	mysql_fetch_array($editResult);
						$voucherType	=	$editRow['voucherType'];
						$voucherNo		=	$editRow['voucherNo'];
						?>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
<div class="row">
	<div class="col-lg-12">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form method="post" action="do.php?op=edit" id="cash_receipt" class="default_form" onsubmit="return check()">
				<input type="hidden" name="editId" value="<?php echo $editId; ?>">
				<div class="bd_panel_head">
					<h3>Bank RECEIPT</h3>
				</div>
				<div class="bd_panel_body">
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Date <span class="valid">*</span></label>
								<input type="text" name="transactionDate" id="transactionDate" class="user_date"
								required="" value="<?php echo $App->dbformat_date($editRow['transactionDate']);?>">
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">                                
								<label>Voucher No <span class="valid">*</span></label>
								<input type="text" name="voucherNo" required="" value="<?php echo 'BR' .$editRow['voucherNo']?>"  readonly="">
							</div>
						</div>                        
						<?php
						$cId=$editRow['fromId']; 
						$editSelect2 = 	"select  accountName                          			
						from ".TABLE_ACCOUNTS." 
						where ID='$cId'";
						$editResult2 = 	$db->query($editSelect2);
						$editRow2	 = 	mysql_fetch_array($editResult2);
						?>
						<!--<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Account from*</label>
								<div class="aj_rel_box aj_account_from">
									<input name="accountFromName" autocomplete="off" required="" id="accountFromName" type="text" value="<?php echo $editRow2['accountName']; ?>">
									<input type="hidden" name="accountFrom"  value="<?php echo $editRow['fromId']; ?>"/>
									<ul class="aj_live_items"></ul>
									<p class="validation_mes">Please fill this!</p>
								</div>
							</div>
						</div>-->
						<div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account from <span class="valid">*</span></label>
                                <div class="aj_rel_box aj_account_from">
                                    <input name="accountFromName" autocomplete="off" required="" id="accountFromName" type="text"  value="<?php echo $editRow2['accountName']; ?>" readonly="">
                                    <ul class="aj_live_items"></ul>
                                    <p class="validation_mes">Please fill this!</p>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-2 col-md-2 col-sm-3">
                            <label>Account From ID <span class="valid">*</span></label>
                            <div class="aj_rel_box aj_account_from">
                                <input type="text" name="accountFrom" id="accountFrom" autocomplete="off" required  value="<?php echo $editRow['fromId']; ?>" readonly=""/>
                                <ul class="aj_live_items"></ul>
                                <p class="validation_mes">Please fill this!</p>
                            </div>
                        </div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Account To <span class="valid">*</span></label>
								<select name="accountTo" required="">
									<option value="">Select</option>
									<?php
									$subId	=	$db->subAccount(12);
									$select = "SELECT ID,accountName 
												FROM ".TABLE_ACCOUNTS." 
												WHERE accountGroupId IN($subId)
												AND proId='$proId'";
									$res = $db->query($select);
									while($row = mysql_fetch_array($res)){
										?>
										<option value="<?php echo $row['ID']; ?>" <?php if($editRow['toId']==$row['ID']){ echo "selected"; }?>><?php echo $row['accountName']; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>

						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label style="visibility: hidden;">Get Unpaid Bills</label>
								<button class="bd_btn bd_btn_blue" id="get_bill_details"
                                        data-target="#bill_list_pop_receipt">Get Bill Details
								</button>
							</div>
						</div>
					</div>
					<div class="cash_payment_table_wrap">
						<table class="table table-bordered bd_table" id="tbl_cash_receipt">
							<thead>
								<tr class="table_head">
									<th colspan="10" class="text-center">Bank Receipt</th>
									<!--<th colspan="2" class="text-right">
									<button class="bd_btn bd_btn_blue" id="add_cash_receipt_row" type="button">
									<i class="ion ion-plus"></i>Add New
									</button>
									</th>-->
								</tr>
								<tr>
									<th>Sl No</th>
									<th>Against our invoice no*</th>
									<th>Date</th>
									<th>Voucher Type</th>                               
									<th>Total</th>
									<th>Balance</th>
	                                <th>Discount</th>
	                                <th>Paying Amt</th>
	                                <th>Remarks</th>
	                                <th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								$paymentQry 	= mysql_query("SELECT  ".TABLE_CASH_RECEIPT.".voucherType,
									".TABLE_CASH_RECEIPT.".invoiceNo,
									".TABLE_CASH_RECEIPT.".remark,
									".TABLE_CASH_RECEIPT.".cheque,
									".TABLE_CASH_RECEIPT.".payingAmount,
									".TABLE_CASH_RECEIPT.".discount
									FROM ".TABLE_CASH_RECEIPT." 
									WHERE voucherNo = '".$voucherNo."'
									AND paymentType='BANK'
									AND proId='$proId'
									GROUP BY ".TABLE_CASH_RECEIPT.".invoiceNo,
											 ".TABLE_CASH_RECEIPT.".voucherType");
								$i	=	$net	=	$dis = 0;
								$rowcount=mysql_num_rows($paymentQry);
								
								while($paymentRow	=	mysql_fetch_array($paymentQry)){
									$invoiceNo	=	$paymentRow['invoiceNo'];
									$voucherType	=	$paymentRow['voucherType'];
									$cheque	=	$paymentRow['cheque'];
                                                                        //for getting the net amount if discount is there
	                        	 	                        $payingAmount	=	$paymentRow['payingAmount'];
	                        	 	                        $discount		=	$paymentRow['discount'];
	                        	 	                        $netAmount		=	$payingAmount-$discount;
									//for getting total paid
									
									$totPaidQry	=	mysql_query("SELECT SUM(payingAmount) as paidAmount 
										FROM `".TABLE_CASH_RECEIPT."` 
										WHERE voucherType = '".$voucherType."' 
										AND invoiceNo = ".$invoiceNo." 
										AND proId='$proId'
										GROUP BY invoiceNo");
									$totPaidRow	=	mysql_fetch_array($totPaidQry);
                        	 	
                        	 	
									if($paymentRow['voucherType']=='airline'){
										$prefix="AI";
										$commonQry 	= mysql_query("SELECT invoiceDate,netAmount 
											FROM ".TABLE_INVOICE_AIRLINE."
											WHERE invoiceNo=$invoiceNo
											AND proId='$proId'"
											);
										$connonRow		=	mysql_fetch_array($commonQry);
									}
									if($paymentRow['voucherType']=='hotel'){
										$prefix="HI";
										$commonQry 	= mysql_query("SELECT invoiceDate,netAmount 
											FROM ".TABLE_INVOICE_HOTEL."
											WHERE invoiceNo=$invoiceNo
											AND proId='$proId'");
										$connonRow		=	mysql_fetch_array($commonQry);
									}
									if($paymentRow['voucherType']=='miscellaneous'){
										$prefix="MI";
										$commonQry 	= mysql_query("SELECT invoiceDate,netAmount 
											FROM ".TABLE_INVOICE_MISC."
											WHERE invoiceNo=$invoiceNo
											AND proId='$proId'");
										$connonRow		=	mysql_fetch_array($commonQry);
									}
									if($paymentRow['voucherType']=='transportation'){
										$prefix="TI";
										$commonQry 	= mysql_query("SELECT invoiceDate,netAmount 
											FROM ".TABLE_INVOICE_TRANSPORT."
											WHERE invoiceNo=$invoiceNo
											AND proId='$proId'");
										$connonRow		=	mysql_fetch_array($commonQry);
									}
									if($paymentRow['voucherType']=='visa'){
										$prefix="VI";
										$commonQry 	= mysql_query("SELECT invoiceDate,netAmount 
											FROM ".TABLE_INVOICE_VISA."
											WHERE invoiceNo=$invoiceNo
											AND proId='$proId'");
										$connonRow		=	mysql_fetch_array($commonQry);
									}
									if($paymentRow['voucherType']=='package'){
										$prefix="PI";
										$commonQry 	= mysql_query("SELECT invoiceDate,netAmount 
											FROM ".TABLE_INVOICE_PACKAGE."
											WHERE invoiceNo=$invoiceNo
											AND proId='$proId'");
										$connonRow		=	mysql_fetch_array($commonQry);
									}
									?>
									<tr>
										<td><?php  echo ++$i;?></td>
	                                   
										<td>
											<div class="aj_rel_box">
												<input type="text" name="invoiceNo_<?php echo $i;?>" data-rel="bill_live" value="<?php echo $prefix.$paymentRow['invoiceNo'];?>" readonly>
												<ul class="aj_live_items">

												</ul>
											</div>
										</td>
										<td>
											<input type="text" class="user_date" name="invoiceDate_<?php echo $i;?>" value="<?php if($connonRow['invoiceDate']){echo $App->dbformat_date($connonRow['invoiceDate']);}?>" readonly>
										</td>
										<td>
											<input type="text" name="invoiceType_<?php echo $i;?>" value="<?php echo $paymentRow['voucherType'];?>" readonly>
										</td>
										<td>
											<input type="text" name="total_<?php echo $i;?>" value="<?php echo $connonRow['netAmount'];?>" readonly>
										</td>
										<td>
											<input type="text" name="totalPaid_<?php echo $i;?>" value="<?php echo $connonRow['netAmount']-$totPaidRow['paidAmount']; ?>" readonly>
										</td>
										<td>
											<input type="text" data-action="trigger_cash_calculation" name="discount_<?php echo $i;?>" value="<?php echo $paymentRow['discount']?>">
										</td>
										<td>
											<input type="text" data-action="trigger_cash_calculation" name="payingAmount_<?php echo $i;?>" value="<?php echo $paymentRow['payingAmount']?>">
										</td>
										
										<td>
											<textarea name="remark_<?php echo $i;?>"><?php echo $paymentRow['remark']?></textarea>
										</td>
										<td>
											<button class="bd_btn bd_btn_red" type="button" data-action="del_cash_payment_row">Delete</button>
										</td>
									</tr>
									<?php
                                $net=$net+$paymentRow['payingAmount'];
                                $dis=$dis+$paymentRow['discount'];
                                 } ?>
							</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
							<div class="form_block">
								<label>Cheque No</label>
								<input type="text" name="cheque" id="cheque" value="<?php echo $cheque; ?>">
							</div>
						</div> 
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
							<div class="form_block">
								<label>Net Amount <span class="valid">*</span></label>
								<input type="text" name="amount" id="amount" value="<?php echo $net-$dis; ?>"  required="">
							</div>
						</div> 
						<div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
							<div class="form_block">
								<label>Remark</label>
								<textarea name="remark" id="remark"><?php echo $editRow['remark'];?></textarea>
							</div>

						</div>
					</div>
				</div>
				<div class="bd_panel_footer">
					<div class="panel_row">
						<div class="form_block_full">
							<input type="hidden" name="rowCount" id="cash_receipt_row_count"  value="<?php echo $rowcount ?>">
							<input type="submit" name="form" value="SAVE">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
require('../admin_footer1.php');
?>

<!-- Pop up for bill list -->
<div id="bill_list_pop_receipt" class="modal fade bd_modal" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Pending Bills</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered" id="bill_list">
					<thead>
						<tr>
							<th>Sl No</th>
							<th>Vouvher No</th>
							<th>Date</th>
							<th>Voucher Type</th>
							<th>Total</th>
							<th>Paid</th>
							<th>Balance</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="7">No bills found unpaid.</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="trigger_cash_payment">Pay Now</button>
			</div>
		</div>
	</div>
</div>
<!-- /Pop up for bill list -->


<!-- div for select box for airline details -->
<div id="cash_receipt_common_row" style="display: none">
	<table class="table table-bordered bd_table" id="tbl_cash_reciept">
		<tbody>
			<tr>
				<td>1</td>
				<td>
					<select name="invoiceType">
						<option value="airline" selected>Airline</option>
						<option value="hotel">Hotel</option>
						<option value="miscellaneous">Miscellaneous</option>
						<option value="transportation">Transportation</option>
						<option value="visa">Visa</option>
						<option value="package">Package</option>
					</select>
				</td>
				<td>
					<input type="text" name="invoiceNo" data-rel="bill_live">
				</td>
				<td>
					<input type="text" data-dp="datepick" name="invoiceDate" readonly>
				</td>
				<td>
					<input type="text" name="total" value="0.00" readonly>
				</td>
				<td>
					<input type="text" name="totalPaid" value="0.00" readonly>
				</td>
				<td>
					<input type="text" data-action="trigger_cash_calculation" name="payingAmount" value="0.00">
				</td>
				<td>
					<textarea name="remark"></textarea>
				</td>
				<td>
					<button class="bd_btn bd_btn_red" type="button" data-action="del_cash_receipt_row">Delete</button>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?php
require('../admin_footer2.php');
?>
