<?php
require('../admin_header.php');
$rowsPerPage = ROWS_PER_PAGE;

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}
$mergeDate	=	@$_REQUEST['date'];

if(!$mergeDate) // while refreshing should go back to package.php
{
	@header("location:package.php");
}
$loginType 	=	$_SESSION['travelType'];
if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"a.proId='$proId'";
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Package Status</h3>
                </div>
                <div class="page_controls">
                    <!--<div class="page_search">-->
                       
                    </div>
               <!-- </div>-->

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
	<?php

	$cond	=	1;
	$cond1	=	1;
	if($mergeDate)
	{
		$cond	=	$cond." and a.date like '%".$App->dbFormat_date($_REQUEST['date'])."%'";
	}
	if(@$_REQUEST['company']!='all')
	{
		$cond1	=	$cond1." and a.proId='".$_REQUEST['company']."'";
	}
	?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Package Name</th>
                            <th>Ameer</th>
                            <th>Date</th>
                            <th>Passenger Count</th>
                            <th>Room Count</th>
                            <th>Total Passengers</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                    
                     	$i=0;
                        $selectAll = "select a.ID,
                        					 a.packageName,
                        					 a.leader,
                        					 a.date,
                        					 COUNT(b.ID) as passCount
			                          from ".TABLE_PACKAGE." a
			                     LEFT JOIN ".TABLE_INVOICE_PACKAGE_PASSENGER." b ON b.packageId=a.ID
			                          where $cond  and $cond1
			                          GROUP BY a.ID
			                          order by a.ID desc";
			                          
						//echo $selectAll;
                        $result = $db->query($selectAll);
                        $selectCount = mysql_num_rows($result);
                        
                        if(($selectCount==0) || (!$mergeDate))
						{
						?>
							 <tr>
								<td align="center" colspan="9">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
                        /*********************** for pagination ******************************/
					
						if(isset($_GET['page']))
						{
							$pageNum = $_GET['page'];
						}
						else
						{
							$pageNum =1;
						}
						$offset = ($pageNum - 1) * $rowsPerPage;
						$select1=$db->query($selectAll." limit $offset, $rowsPerPage");
						$i=$offset+1;
						//use '$select1' for fetching
						/*************************** for pagination **************************/
						
						/*************************** Function for passenger count **************************/
						
						function numPass($cond,$packId)
						{
							$coundQry	=	mysql_query("SELECT COUNT(ID) AS countPass FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." WHERE $cond AND packageId IN($packId) ");
        					$countRow	=	mysql_fetch_array($coundQry);
        					$count		=	$countRow['countPass'];
        					return($count);
						}
						function numRooms($type,$packId)
						{
							$roomQry	=	mysql_query("SELECT COUNT(".TABLE_INVOICE_PACKAGE_HOTEL.".ID) AS roomCount 
													FROM ".TABLE_INVOICE_PACKAGE_HOTEL." 
													LEFT JOIN ".TABLE_INVOICE_PACKAGE." 
													ON ".TABLE_INVOICE_PACKAGE.".ID=".TABLE_INVOICE_PACKAGE_HOTEL.".in_packageId 
													WHERE ".TABLE_INVOICE_PACKAGE.".packageId IN($packId)  
													AND ".TABLE_INVOICE_PACKAGE_HOTEL.".roomType=$type 
													");
						
		        			$roomRow	=	mysql_fetch_array($roomQry);
		        			$roomNum	=	$roomRow['roomCount'];
		        			return $roomNum;
						}
						
						/************************************************************************************/
						$passCount = 0;
						$packageId		=	array();
						$packageName	=	array();
						$packageleader	=	array();
                        while ($row = mysql_fetch_array($select1)) 
                        {
                        	array_push($packageId, $row['ID']);
                        	array_push($packageName, $row['packageName']);
                        	array_push($packageleader, $row['leader']);
                        	$date		=	$row['date'];
                        	$passCount	=	$passCount + $row['passCount'];
                        }
						$packageId  = implode(',',$packageId);
                        ?>
                         <tr>                            
                            <td><?php echo $i++; ?></td>
                            <td><?php echo implode(',',$packageName); ?></td>
                            <td><?php echo implode(',',$packageleader); ?></td>
                            <td><?php echo $App->dbformat_date($date); ?></td>
                            <td>
                            Adult  : <?php echo numPass('age>=12',$packageId); ?><br />
                            Child  : <?php echo numPass('(age>=2 AND age <12)',$packageId); ?><br />
                            Infant : <?php echo numPass('age<2',$packageId); ?><br />
                            Male   : <?php echo numPass("gender='M'",$packageId); ?><br />
                            Female : <?php echo numPass("gender='F'",$packageId); ?><br />
                            
                            </td>
                            <td>
                            Sharing  			: <?php echo numRooms('1',$packageId); ?><br />
                            Twin  				: <?php echo numRooms('2',$packageId); ?><br />
                            Triple 				: <?php echo numRooms('3',$packageId); ?><br />
                            Four Bed   			: <?php echo numRooms('4',$packageId); ?><br />
                            Five Bed 			: <?php echo numRooms('5',$packageId); ?><br />
                            Six Bed				: <?php echo numRooms('6',$packageId); ?><br />	
                            Child With Bed		: <?php echo numRooms('7',$packageId); ?><br />
                            Child Without Bed	: <?php echo numRooms('8',$packageId); ?><br />
                            Adult Without Bed	: <?php echo numRooms('9',$packageId); ?><br />
                            </td>
                            
                            <td><?php echo $passCount; ?></td>
                            <td><a class="show_table_lnk show_table_lnk_view" href="package_view.php?id=<?php echo $packageId; ?>&proId=<?php echo $proId;?>">View</a>
                            <a class="show_table_lnk show_table_lnk_view" href="package_invoice.php?id=<?php echo $packageId; ?>&proId=<?php echo $proId;?>">Invoice</a>
                            </td>
                            
                        </tr>
                            <?php
                        }
                        ?>

                    </tbody>
                </table>
            </div>
            
            <!-- paging -->		
            <!--<div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div> -->       
            <!-- paging end-->
            
            <!--*****************************************************************-->
            
            	 <?php 
                  if($selectCount>@$rowsPerPage)
					{
					?>	
					 <br />	
					  <div class="pagerSC" align="center">
					<?php
					
					$query   =  $db->query($selectAll);
					$numrows = mysql_num_rows($query);
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
					   if ($page == $pageNum)
					   {
						  $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
					   }
					   else
					   {
						 	if(@$proId)
					   		 {
							 	$nav .= " <a href=\"$self?page=$page&company=$proId\">$page</a> ";
							 }
							 else
							 {
							 	$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							 }
					   }
					}
					?>
					 <?php
					if ($pageNum > 1)
					{
					   $page  = $pageNum - 1;
					   if(@$proId)
					   {
						   $prev  = " <a href=\"$self?page=$page&company=$proId\">Prev</a> ";
						   $first = " <a href=\"$self?page=1&company=$proId\">First Page</a> ";
					   }
					   else
					   {
						   $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
						   $first = " <a href=\"$self?page=1\">First Page</a> ";
					   }
					}
					else
					{
					   $prev  = '&nbsp;';
					   $first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
					   $page = $pageNum + 1;
					   if(@$proId)
					   {
						   	 $next = " <a href=\"$self?page=$page&company=$proId\">Next</a> ";
						     $last = " <a href=\"$self?page=$maxPage&company=$proId\">Last Page</a> ";
					   }
					   else
					   {
						   	 $next = " <a href=\"$self?page=$page\">Next</a> ";
						   	 $last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
					   }
					}
					else
					{
					   $next = '&nbsp;';
					   $last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>