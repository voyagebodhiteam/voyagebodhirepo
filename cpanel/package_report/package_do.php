<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId		=	$_SESSION['proId'];
$loginId	=	$_SESSION['travelId'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
	 case 'edit':

    		$pacId			=	$_REQUEST['packageId'];
    		$editSelect		=	"SELECT services FROM ".TABLE_PACKAGE." WHERE ID='$pacId'";
			$editResult 	= 	$db->query($editSelect);
			$editRow 		= 	mysql_fetch_array($editResult);
			$services 		= 	explode(",",$editRow['services']);
    		if(!$pacId)
    		{
				$_SESSION['msg'] = $App->sessionMsgCreate('error', "Faild to add Update. Invalid details."); 			
				header("location:package.php");
			}
			else
			{
	    		$passengerCount	=	$_REQUEST['pack_passenger_count'];
	    		for($i=1;$i<=$passengerCount;$i++)
	    		{
					$passId1	=	'passengerId_'.$i;
					$passId 	=	$_REQUEST[$passId1];
					$ticketNo	=	'ticketNo_'.$i;
					$subDate	=	'subDate_'.$i;
					$collDate	=	'collDate_'.$i;
					$disDate	=	'disDate_'.$i;
					$nationality=	'nationality'.$i;
					$mofa		=	'mofa_'.$i;
					$emofa		=	'emofa_'.$i;
					
					$data['mofa']		=	$App->convert($_REQUEST[$mofa]);
					$data['emofa']		=	$App->convert($_REQUEST[$emofa]);
					$success			=	$db->query_update(TABLE_INVOICE_PACKAGE_PASSENGER, $data ," ID='{$passId}' " );
					
					
					if(in_array('airline',$services))
					{
						$data1['ticketNo'] 	= 	$App->convert($_REQUEST[$ticketNo]);
						$success1			=	$db->query_update(TABLE_INVOICE_PACKAGE_AIRLINE, $data1 ," passengerId='{$passId}' " );
					}
					
					if(in_array('visa',$services))
					{
						$data2['submissionDate'] 	= 	'';
						$data2['collectionDate'] 	= 	'';
						$data2['dispatchDate'] 		= 	'';
						
						if($_REQUEST[$subDate])
						{
							$data2['submissionDate'] 	= 	@$App->dbformat_date($_REQUEST[$subDate]);
						}
						if($_REQUEST[$collDate])
						{
							$data2['collectionDate'] 	= 	$App->dbformat_date($_REQUEST[$collDate]);
						}
						if($_REQUEST[$disDate])
						{
							$data2['dispatchDate'] 	= 	$App->dbformat_date($_REQUEST[$disDate]);
						}
						
						
						
						if($_REQUEST[$subDate] || $_REQUEST[$collDate]|| $_REQUEST[$disDate])
						{
							$success2	=	$db->query_update(TABLE_INVOICE_PACKAGE_VISA, $data2 ," passengerId=$passId ");
						}
						
					}
					
				}
				$db->close();
				if ($success || $success1 || $success2 ) 
	            {
	                $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details Updated successfully");                
	            } 
	            else 
	            {
	                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Faild to Update details. Please try again.");     
	            }
	    		 header("location:package.php");
    		}
    		
	        
	        
	 break;

}	        	
?>