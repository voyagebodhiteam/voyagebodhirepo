<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();

if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"a.proId='$proId'";
}
$packId		=	$_REQUEST['id'];
  
$filename 	= "package_passenger_list";         //File Name
$sl = 0;
$sql 		= "SELECT   @sl:=@sl+1 SlNo,a.in_packageId InvoiceNo,a.passengerName Name,a.gender Gender,a.dob DOB ,
						a.age Age,a.nationality Nationality,a.birthPlace PlaceOfBirth,a.relation Relation,
						a.mobile Mahram,a.passportNo PassportNo,a.dateOfIssue DateOfIssue,
						a.validUpTo ValidUpTo,a.issuePlace PlaceOfIssue,a.mofa Mofa,a.emofa Emofa,
				 		b.ticketNo TicketNo,
				 		e.submissionDate DateOfSubmission,e.collectionDate DateOfCollection,
				 		e.dispatchDate DateOfDispatch
			       FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." a 
			  LEFT JOIN ".TABLE_INVOICE_PACKAGE_AIRLINE." b ON b.passengerId=a.ID 
			  LEFT JOIN ".TABLE_INVOICE_PACKAGE_VISA." e ON e.passengerId=a.ID , (SELECT @sl:= 0) AS sl
			      WHERE a.packageId IN($packId) AND $check
			   ORDER BY a.ID ASC";

$result 	= @mysql_query($sql) or die("Couldn't execute query:<br>" . mysql_error(). "<br>" . mysql_errno());    
$file_ending = "xlsx";

//header info for browser

header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=$filename.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");

/*******Start of Formatting for Excel*******/ 
  
//define separator (defines columns in excel & tabs in word)
$sep = "\t"; //tabbed character
//start of printing column names as names of MySQL fields
for ($i = 0; $i < mysql_num_fields($result); $i++) {
echo mysql_field_name($result,$i) . "\t";
}
print("\n");    
//end of printing column names  
//start while loop to get data
    while($row = mysql_fetch_row($result))
    {
        $schema_insert = "";
        for($j=0; $j<mysql_num_fields($result);$j++)
        {
            if(!isset($row[$j]))
                $schema_insert .= "NULL".$sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]".$sep;
            else
                $schema_insert .= "".$sep;
        }
        $schema_insert = str_replace($sep."$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }   
?>