<?php
require('../admin_header.php');

if($_SESSION['travelType']=='Admin')
{
	$check	=	"A.finYear='$finYear'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"A.proId='$proId' and A.finYear='$finYear'";
}

$type = $_SESSION['travelType'];

//for edit and delete permission
$logID = $_SESSION['travelId'];
if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

//for incurrect invoiceNo search
function count()
{
var vno = document.getElementById('vno').value;
//alert(invoiceNo);
if((vno.length < 3) && (vno!='') ){
	document.getElementById('check').innerHTML= "Enter Current Invoice No";
	return false;
		}
}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>BANK PAYMENT</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post" onsubmit="return count()"><!-- count()-> for alert message -->
                             <input type="text" name="vno" id="vno" placeholder="Voucher No" value="<?php echo @$_REQUEST['vno']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                             <div class="valid" id="check"></div><!-- id for message  -->
                        </form>
                    </div>
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="add.php">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
<?php	
$cond="1";
if(@$_REQUEST['vno'])
{
	//deleting the first two character from a string 
	$str = $_POST['vno'];
	$str2 = substr($str, 2);
	$cond=$cond." and A.voucherNo like'%".$str2."%'";
}
?>     
        
    
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Date</th>
                            <th>User Name</th>
                            <th>Voucher No</th>
                            <th>Account From</th>
                            <th>Account To</th>
                            <th>Amount</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                     $i=0;
                     $selectAll = "SELECT A.ID,
                     					  A.voucherNo,
                     					  A.voucherType,
                     					  A.credit,
                     					  A.remark,
                     					  A.transactionDate,
                     					  A.loginId,
                     					  A.proId,
                     					  B.accountName as toLedger,
                     					  C.accountName as fromLedger   
                     				FROM `".TABLE_TRANSACTION."` A,`".TABLE_ACCOUNTS."` B,`".TABLE_ACCOUNTS."` C 
                     				WHERE (A.voucherType='Bank Payments' AND A.credit!='0' AND A.fromLedger=C.ID AND A.toLedger=B.ID)
                     			  and $cond
                     			  and $check
                     			   ORDER BY A.ID DESC";
                     $result = $db->query($selectAll);
                     if(mysql_num_rows($result)==0)
                     {
					 	?>
					 	<tr><td colspan="8" align="center"> There is no data in list </td></tr>
					 	<?php
					 }
					 else
					 {
	                     while ($row = mysql_fetch_array($result)) {
	                     	
	                     					//for permission of edit and delete
                        					$loginIDs=$row['loginId'];
                        					$tabProId=$row['proId'];
                        					
                        					//for getting username in index
			                        	
			                        	$queryOfLog = "SELECT type from ".TABLE_LOGIN."
				                     				WHERE ".TABLE_LOGIN.".ID='$loginIDs'";   									
				                     				
				                     $rowsLog = $db->query($queryOfLog);		
									 $resLog=mysql_fetch_array($rowsLog);
									 
									 $loginType	=$resLog['type'];
											
                        	if($tabProId==0)
                        	{
								$userName ='Admin';
							}
                        	else if($tabProId!=0) {
							if($loginType=="Branch")
							{
							$qry="SELECT companyName FROM ".TABLE_COMPANY.",".TABLE_LOGIN."
								  WHERE ".TABLE_COMPANY.".ID=".TABLE_LOGIN.".userId
								  AND ".TABLE_LOGIN.".ID=$loginIDs";
							$rows = $db->query($qry);		
							$res=mysql_fetch_array($rows);
							$userName=$res['companyName'];	  
							}
							if($loginType=="Staff")
							{
							$qry="SELECT staffName FROM ".TABLE_STAFF.",".TABLE_LOGIN."
								  WHERE ".TABLE_STAFF.".ID=".TABLE_LOGIN.".userId
								  AND ".TABLE_LOGIN.".ID=$loginIDs";
							$rows = $db->query($qry);		
							$res=mysql_fetch_array($rows);
							$userName=$res['staffName'];
							}
							}
			                        	
			                        	//
                        					
		                     ?>
		                     <tr>                            
		                        <td><?php echo ++$i; ?></td>
                                        <td><?php echo $row['transactionDate']; ?></td>
		                        <td><?php echo $userName; ?></td>
		                        <td><?php echo 'BP'.$row['voucherNo']; ?></td>
		                        <td><?php echo $row['fromLedger']; ?></td>
		                        <td><?php echo $row['toLedger']; ?></td>
		                        <td><?php echo $row['credit']; ?></td>	                        
		                        <td>
		                            <a class="show_table_lnk show_table_lnk_view" href="view.php?id=<?php echo $row['ID'];?>">View</a>
		                            
		                            <?php if(($cashBankEdit==1 && $_SESSION['travelType']=="Staff" && $logID==$loginIDs) || ($_SESSION['travelType']=="Branch") ){ ?>
		                            <a class="show_table_lnk show_table_lnk_edit" href="edit.php?id=<?php echo $row['ID'];?>">Edit</a>
		                           <?php } else { ?>
									<a href="javascript:void(0)" class="bd_lnk bd_lnk_disabled">Edit</a>
                     			<?php } ?>
                     			
		                             <?php if(($cashBankDelete==1 && $_SESSION['travelType']=="Staff" && $logID==$loginIDs) || ( $_SESSION['travelType']=="Branch") ){ ?>
		                             <a class="show_table_lnk show_table_lnk_del" href="do.php?id=<?php echo $row['ID'];?>&op=delete" onclick="return delete_type();">Delete</a>
		                             <?php } else {//end of if condition?>
		                    <a href="javascript:void(0)" class="bd_lnk bd_lnk_disabled">Delete</a>
		                    <?php } ?>
		                             
		                             
                                              <a class="show_table_lnk show_table_lnk_edit" href="do.php?id=<?php echo $row['ID'];?>&op=print" target="_blank">Print</a>
		                        </td>
		                    </tr>
		                    <?php
	                    }
	                }
                    ?>				                                         
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>