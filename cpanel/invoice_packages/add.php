<?php
require("../admin_header.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';
?>

<script>

	//calc age
function getAge()
{
	var dob=document.getElementById('dob').value;
	//alert(dob);
	//today
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd='0'+dd
	} 

	if(mm<10) {
	    mm='0'+mm
	} 

	today = dd+'-'+mm+'-'+yyyy;
	var dobSplit= dob.split('-');
	var todaySplit= today.split('-');
	
	var dobLast = new Date(dobSplit[2], +dobSplit[1]-1, dobSplit[0]);
	var todayLast = new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
	var dateDiff=(todayLast.getTime() - dobLast.getTime()) / (1000*60*60*24);
	var age=Math.round(dateDiff/365);
	document.getElementById('age').value=age;
}

</script>
<script>

	function valid()
	{
		flag=false;
		tds = document.getElementById('tds').value;	
			if(isNaN(tds) && tds!=0)
			{		
				document.getElementById('tdsdiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		mainProcCharge = document.getElementById('mainProcCharge').value;	
			if(isNaN(mainProcCharge) && mainProcCharge!=0)
			{		
				document.getElementById('mainProcChargediv').innerHTML="Enter valid amount.";
				flag=true;
			}
		discount = document.getElementById('discount').value;	
			if(isNaN(discount) && discount!=0)
			{		
				document.getElementById('discountdiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		/*mainTax = document.getElementById('mainTax').value;	
			if(isNaN(mainTax) && mainTax!=0)
			{		
				document.getElementById('mainTaxdiv').innerHTML="Enter valid amount.";
				flag=true;
			}*/
			if((!document.getElementsByName('pass1_passengerName').length > 0) || (!document.getElementsByName('pass1_passengerName')[0].value))
			{
				document.getElementById('passCountdiv').innerHTML="Add Passenger.";
				flag=true;
			}
			
		mainOtherCharge = document.getElementById('mainOtherCharge').value;	
			if(isNaN(mainOtherCharge) && mainOtherCharge!=0)
			{		
				document.getElementById('mainOtherChargediv').innerHTML="Enter valid amount.";
				flag=true;
			}
		passCount = document.getElementById('inv_pack_passenger_count').value;	
			if(passCount == 0)
			{		
				document.getElementById('passCountdiv').innerHTML="Add Passenger.";
				flag=true;
			}
		netAmount = document.getElementById('netAmount').value;	
			if(netAmount <= 0)
			{		
				document.getElementById('netAmountdiv').innerHTML="Invalid Amount.";
				flag=true;
			}
			
			//for customer name and customer id validation	
		var customerName	=	document.getElementById('customerName').value;
		var customerId		=	document.getElementById('customerId').value;
		var getResponse=0;
		
		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
           // document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
          	getResponse= xmlhttp.responseText;//alert(getResponse);
          	
          	 
          	
          }	
          
	    };
	   
	   	xmlhttp.open("GET", "../../services/customer_check_ajax.php?name="+customerName+"&id="+customerId, false);
	    xmlhttp.send();
	  
		 		if(getResponse==0)
	          	{
	          		flag=true;
alert("Invalid Customer Name and ID.!");
				}
		if(flag==true)
		{
		return false;
		}
	}
	//clear the validation msg
	function clearbox(Element_id)
	{
	document.getElementById(Element_id).innerHTML="";
	}

	
</script>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form class="default_form" id="invoice_package" method="post" action="do.php?op=index" onsubmit="return valid()">
                    <div class="bd_panel_head">
                        <h3>Package - Invoice</h3>
                    </div>
                    <div class="bd_panel_body">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher No</label>
                                    <input type="text" name="invoiceNo" value="<?php echo "PI" . $db->nextVoucher(TABLE_INVOICE_PACKAGE, 'invoiceNo'," proId=$proId"); ?>" required="" readonly="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Date</label>
                                    <input type="text" name="invoiceDate" class="user_date"
                                           value="<?php echo date('d-m-Y') ?>" >
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Customer Name <span class="valid">*</span></label> <!--select from account where accGpId=37-->    
                                    <div class="aj_rel_box aj_customer">
                                        <input type="text" name="customerName" autocomplete="off" required="" id="customerName">
                                    	<input type="hidden" name="customerId" >
                                        <ul class="aj_live_items">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Customer ID</label> <!--select from account where accGpId=37-->
									<div class="aj_rel_box aj_customer">
										<input type="text" name="customerId" autocomplete="off" required="" id="customerId">
										<ul class="aj_live_items">

										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
	                                <label>Agent Name <span class="valid">*</span></label>
	                                <select name="agentId" required="" class="pass_to_table">
	                                    <option value="">Select</option>
	                                    <?php
	                                    $select = "select ID,accountName 
	                                    			from " . TABLE_ACCOUNTS . " 
				                                    where accountGroupId=48
				                                    AND proId='$proId'
				                                    AND finYear='$finYear'
				                                    Order by accountName";
	                                    $res = $db->query($select);
	                                    while ($row = mysql_fetch_array($res)) {
	                                        ?>
	                                        <option
	                                            value="<?php echo $row['ID']; ?>"><?php echo $row['accountName']; ?></option>
	                                        <?php
	                                    }
	                                    ?>
	                                </select>
                            	</div>
                        	</div>
						</div>
						<div class="row">
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Select Package <span class="valid">*</span></label> 
                                    <select name="package" required="">
                                        <option value="">Select</option>
                                        <?php
                                        $pacQry = mysql_query("SELECT * FROM " . TABLE_PACKAGE . " WHERE date>now() AND proId=$proId or proId ='0' and finYear=$finYear");
                                        
                                        while ($pacRow = mysql_fetch_array($pacQry)) {
                                            ?>
                                            <option value="<?php echo $pacRow['ID']; ?>" data-dynamic_form="<?php echo $pacRow['services']; ?>"><?php echo $pacRow['packageName']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Package Rate</label>
                                    <div id="package_rate_show" style="height: 34px; background-color: #CCC; padding-left7px; padding-top: 7px;"></div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="form_divider"></div>
                        <div class="multi_det">
                            <div class="multi_det_body">
                                <div class="multi_det_head">
                                    <h3>Passenger List</h3>
                                    <div class="multi_det_control">
                                        <ul class="page_nav">
                                            <li>
                                                <a href="#" id="inv_pack_modal_trigger"
                                                   data-modal_target="#inv_package_passenger_pop">
                                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                                    Add New
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="bd_clear"></div>
                                </div>
                                <div class="multi_det_table_wrap table-responsive">
                                    <table class="table table-bordered multi_det_table" id="inv_package_paasenger_table">
                                        <thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Passenger</th>
                                            <th>Age</th>
                                            <th>Passport No</th>
                                            <th>Airline Ticket No</th>
                                            <th>Airline Amt</th>
                                            <th>Hotel Amt</th>
                                            <th>Visa Amt</th>
                                            <th>Transportation Amt</th>
                                            <th>Total</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						<div class="form_divider"></div>
                        <div class="row">
                            <div class="col-lg-2 col-sm-2 col-md-2 ">
                                
                                <div class="form_block">
                                    <label>TDS</label>
                                    <input type="text" name="tds" id="tds" onfocus="clearbox('tdsdiv')">
                                    <div id="tdsdiv" class="valid" style="color:#FF6600;"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Processing Charge</label>
                                    <input type="text" name="mainProcCharge" id="mainProcCharge" onfocus="clearbox('mainProcChargediv')">
                                    <div id="mainProcChargediv" class="valid" style="color:#FF6600;"></div>
                                </div>
                             </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Discount</label>
                                    <input type="text" name="discount" id="discount" onfocus="clearbox('discountdiv')">
                                    <div id="discountdiv" class="valid" style="color:#FF6600;"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Tax ('%' for percentage)</label>
                                    <input type="text" name="mainTax" id="mainTax" onfocus="clearbox('mainTaxdiv')">
                                    <div id="mainTaxdiv" class="valid" style="color:#FF6600;"></div>
                                </div>
                             </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charge</label>
                                    <input type="text" name="mainOtherCharge" id="mainOtherCharge" onfocus="clearbox('mainOtherChargediv')">
                                    <div id="mainOtherChargediv" class="valid" style="color:#FF6600;"></div>
                                </div>
                             </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Net Amount <span class="valid">*</span></label>
                                    <input type="text" name="netAmount" id="netAmount" readonly="" required="" onfocus="clearbox('netAmountdiv')">
                                    <div id="netAmountdiv" class="valid" style="color:#FF6600;"></div>
                                </div>
                            </div>
                           
                        </div>
                        <div class="row">
                        	
                           
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Description</label>
                                    <textarea name="description"></textarea>
                                </div>
                            </div>
                        </div>
                         <div id="passCountdiv" class="valid" style="color:#FF6600;"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="airline_submit">
                                    <input id="inv_pack_passenger_count" type="hidden" name="passengerRowCount" value="0">
                                    <input type="submit" value="Save" onclick="return check()">
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
    <!-- Popups -->
    <div id="inv_package_passenger_pop" class="modal fade bd_modal passenger_pop" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Passenger Details</h4>
                </div>
                <div class="modal-body">
                    <form class="default_form" id="inv_package_passenger_pop_form" data-form_for="age_based">
                        <div id="inv_pack_pop_passenger_details">
                            <div class="row">
                            
                            	<div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Honorifics</label>
		                                <select class="pass_to_table" name="prefix">
											<option value="">Select</option>
											<option value="Mr">Mr</option>
											<option value="Mrs">Mrs</option>
											<option value="Ms">Ms</option>
										</select>
									 </div>
                                </div>
                            
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Passenger Name</label>
                                         <input class="pass_to_table" type="hidden" name="ID"  autocomplete="off">
                                        <div class="aj_rel_box  aj_passenger">
                                            <input class="pass_to_table" type="text" name="passengerName"  autocomplete="off">
                                            <ul class="aj_live_items">

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Gender</label>
		                                <select class="pass_to_table" name="gender">
											<option value="M" selected="">Male</option>
											<option value="F">Female</option>
										</select>
									 </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Date of Birth</label>
                                        <input class="pass_to_table user_date" type="text" name="dob">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Age</label>
                                        <input class="pass_to_table" type="text" name="age" id="age">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Place of Birth</label>
                                        <input class="pass_to_table" type="text" name="placeOfBirth">
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="row">
                            	<div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Relation Ship</label>
                                        <select class="pass_to_table" name="relationship">
                                            <option value="">Select</option>
                                            <?php 
                                            $relQry	= mysql_query("select * FROM ".TABLE_RELATIONSHIP." where proId=$proId");
                                            while($relRow	=	mysql_fetch_array($relQry))
                                            {
                                            ?>
                                            <option value="<?php echo $relRow['ID'];?>"><?php echo $relRow['relationship']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                
                            	<div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Mahram</label>
                                        <select class="pass_to_table" name="mobile">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Nationality</label>
                                        <input class="pass_to_table" type="text" name="nationality">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Passport No</label>
                                        <input class="pass_to_table" type="text" name="passportNo">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Date of Issue</label>
                                        <input class="pass_to_table user_date" type="text" name="dateOfIssue">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Place of Issue</label>
                                        <input class="pass_to_table" type="text" name="issuePlace">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                            	<div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Date of Expire</label>
                                        <input class="pass_to_table user_date" type="text" name="validUpTo">
                                    </div>
                                </div>
                            </div>
                         
                          
                        </div>
                        <!-- Container for dynamic form elements -->
                        <div id="inv_pack_dynamic_form_target">

                        </div>
                        <!-- Container for dynamic form elements/ -->
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="trigger_pass_table">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /Popups -->

<!-- Dynamic form elements for passenger -->
<div id="inv_pack_dynamic_source" style="display: none;">
    <!-- Airline -->
    <div class="package_dynamic_box inv_pack_dynamic_airline" style="display: none;">
        <h3 class="pack_dynamic_head">Airline Details</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Ticket No</label>
                    <input class="pass_to_table" type="text" name="ticketNo" >
                </div>
            </div>
        </div>
        <h3>Total</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="airInvoice" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="airPurchase" readonly>
                </div>
            </div>
        </div>
    </div>
    <!-- /Airline -->
    <!-- Hotel -->
    <div class="inv_pack_dynamic_hotel">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Room Type</label>
                    <select class="hotel_input pass_to_table" name="roomType">
                       
                    </select>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2" style="display: none;">
                <div class="form_block form_block_check">
                    <label style="display: block;">Child</label>
                    <input type="checkbox" name="" />
                     <label class="check_label">With Bed</label>
                </div>
            </div>
        </div>
        <div class="row" style="display: none;">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="hotInvoice" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="hotPurchase" readonly>
                </div>
            </div>
        </div>
    </div>
    <!-- /Hotel -->
    <!-- Visa -->
    <div class="package_dynamic_box inv_pack_dynamic_visa" style="display: none">
        <h3 class="pack_dynamic_head">Visa</h3>
        <div class="inv_pack_visa_basics">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Submission Date</label>
                        <input class="pass_to_table" type="text" name="submissionDate" data-dp="date_pic">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Collection Date</label>
                        <input class="pass_to_table" type="text" name="collectionDate" data-dp="date_pic">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Dispatch Date</label>
                        <input class="pass_to_table" type="text" name="dispatchDate" data-dp="date_pic">
                    </div>
                </div>
            </div>
        </div>
        <h3>Total</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="visaInvoice" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="visaPurchase" readonly>
                </div>
            </div>
        </div>
    </div>
    <!-- /Visa -->
    <!-- Transportation -->
    <div class="inv_pack_dynamic_transportation">
        <!--<h3 class="pack_dynamic_head">Transportation</h3>-->
        <!--<h3>Total</h3>-->
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2" style="display:none;">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="transInvoice"  readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2" style="display:none;">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="transPurchase"  readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block form_block_check">
                	<label style="display: block;">Transportation</label>
                    <input type="checkbox" name="seatStatus"  checked=""/>
                    <label class="check_label">With Seat</label>
                </div>
            </div>
        </div>
    </div>
    <!-- /Transportation -->
</div>
<?php
require('../admin_footer2.php');
?>