<?php
require("../admin_header.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}

$_SESSION['msg'] = '';

if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$check 	= 	" proId='$proId'";
}

$editId		=	$_REQUEST['id'];
$editSelect = 	"select t1.ID,
						t1.invoiceNo,
						t1.invoiceDate,
						t1.packageId,
						t1.customerId,
						t1.agentId,
						t1.tds,
						t1.tax,
						t1.proCharge,
						t1.otherCharge,
						t1.discount,
						t1.netAmount,
						t1.description,
						t2.accountName
                   from ".TABLE_INVOICE_PACKAGE." as t1
              LEFT JOIN ".TABLE_ACCOUNTS." as t2 on t2.ID=t1.customerId 
                  where  t1.ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);

//for getting the services specified in that purticular package
$packageId 		=	$editRow['packageId'];
$pacServiceQry	=	mysql_query("SELECT services,confirm,extra,discount,adultTotal FROM ".TABLE_PACKAGE." WHERE ID='$packageId'");
$pacServiceRow	=	mysql_fetch_array($pacServiceQry);
$pacService		=	$pacServiceRow['services'];
$pacServiceAry	=	explode(",",$pacService);
?>
 <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form class="default_form" id="invoice_package">
                    <div class="bd_panel_head">
                        <h3>Package - Invoice</h3>
                    </div>
                    <div class="bd_panel_body">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher No</label>
                                    <input type="text" name="invoiceNo" value="<?php echo "PI" . $editRow['invoiceNo']; ?>" required="" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Date</label>
                                    <input type="text" name="invoiceDate" class="user_date"
                                           value="<?php echo $App->dbFormat_date($editRow['invoiceDate']); ?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Customer Name</label> <!--select from account where accGpId=37-->    
                                    <div class="aj_rel_box aj_customer">
                                        <input type="text" name="customerName" autocomplete="off" value="<?php echo $editRow['accountName'];?>" disabled="">
                                    	<input type="hidden" name="customerId" >
                                        <ul class="aj_live_items">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
								<div class="form_block">
									<label>Customer ID</label> <!--select from account where accGpId=37-->
									<div class="aj_rel_box aj_customer">
										<input type="text" name="customerId" autocomplete="off" required="" value="<?php echo $editRow['customerId'];?>" disabled="">
										<ul class="aj_live_items">

										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
                            <div class="form_block">
                                <label>Agent Name <span class="valid">*</span></label>
                                <select name="agentId" required="" disabled="">
                                    <option value="">Select</option>
                                    <?php
                                    $select = "select ID,accountName 
                                    			from " . TABLE_ACCOUNTS . " 
			                                    where accountGroupId=48 
			                                    AND finYear='$finYear'
			                                    AND proId='$proId'	
			                                    Order by accountName";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        ?>
                                        <option value="<?php echo $row['ID']; ?>" <?php if($editRow['agentId']==$row['ID']){ echo "selected"; } ?>><?php echo $row['accountName']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
						</div>
						
						<div class="row">
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Select Package</label> <!--select from account where accGpId=37-->
                                    <select name="package" required="" disabled="">
                                        <option value="">Select</option>
                                        <?php
                                        $pacQry = mysql_query("SELECT * FROM " . TABLE_PACKAGE . "" );
                                        while ($pacRow = mysql_fetch_array($pacQry)) {
                                            ?>
                                            <option value="<?php echo $pacRow['ID']; ?>" data-dynamic_form="<?php echo $pacRow['services']; ?>" <?php if($pacRow['ID']==$editRow['packageId']){echo "selected";}?>><?php echo $pacRow['packageName']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                           	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Package Rate</label>
                                    <div id="package_rate_show" style="height: 34px; background-color: #CCC; padding-left7px; padding-top: 7px;"><?php echo $pacServiceRow['adultTotal'];?></div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="form_divider"></div>
                        <div class="multi_det">
                            <div class="multi_det_body">
                                <div class="multi_det_head">
                                    <h3>Passenger List</h3>
                                    <div class="multi_det_control">
                                       <!-- <ul class="page_nav">
                                            <li>
                                                <a href="#" id="inv_pack_modal_trigger"
                                                   data-modal_target="#inv_package_passenger_pop">
                                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                                    Add New
                                                </a>
                                            </li>
                                        </ul>-->
                                    </div>
                                    <div class="bd_clear"></div>
                                </div>
                                <div class="multi_det_table_wrap table-responsive">
                                    <table class="table table-bordered multi_det_table" id="inv_package_paasenger_table">
                                        <thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Passenger</th>
                                            <th>Age</th>
                                            <th>Passport No</th>
                                            <th>Airline Ticket No</th>
                                            <th>Airline Amt</th>
                                            <th>Hotel Amt</th>
                                            <th>Visa Amt</th>
                                            <th>Transportation Amt</th>
                                            <th>Total</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
        $passQry	=	"SELECT	p.ID,p.in_packageId,p.packageId,p.passengerName,p.dob,p.gender,
        						p.age,p.nationality,p.birthPlace,p.relation,p.mobile,
        						p.passportNo,p.dateOfIssue,p.prefix,
        						p.validUpTo,p.issuePlace,p.total as passTotal,
        						ia.ticketNo,ia.total as iaTotal,
        						pa.netAmount as paNetAmount,
        						ih.roomType,ih.bedStatus,
        						ih.total as ihTotal,
        						ph.netAmount as phNetAmount,
        						iv.submissionDate,iv.collectionDate,
        						pv.netAmount as pvNetAmount,
        						iv.dispatchDate,iv.total as ivTotal,
        						it.total as itTotal,it.seatStatus,
        						pt.netAmount as ptNetAmount
        				   FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." as p
        			  LEFT JOIN ".TABLE_INVOICE_PACKAGE_AIRLINE." as ia on ia.passengerId=p.ID
        			  LEFT JOIN ".TABLE_PURCHASE_PACKAGE_AIRLINE." as pa on pa.passengerId=p.ID
        			  LEFT JOIN ".TABLE_INVOICE_PACKAGE_HOTEL." as ih on ih.passengerId=p.ID
        			  LEFT JOIN ".TABLE_PURCHASE_PACKAGE_HOTEL." as ph on ph.passengerId=p.ID
        			  LEFT JOIN ".TABLE_INVOICE_PACKAGE_VISA." as iv on iv.passengerId=p.ID
        			  LEFT JOIN ".TABLE_PURCHASE_PACKAGE_VISA." as pv on pv.passengerId=p.ID
        			  LEFT JOIN ".TABLE_INVOICE_PACKAGE_TRANSPORTATION." as it on it.passengerId=p.ID
        			  LEFT JOIN ".TABLE_PURCHASE_PACKAGE_TRANSPORT." as pt on pt.passengerId=p.ID
        				  WHERE p.in_packageId='$editId'
        			   GROUP BY	p.ID
        				";
                                        $passResult	=	mysql_query($passQry);
                                        $i=0;
                                        while($passRow	=	mysql_fetch_array($passResult))
                                        {

                                        ?>
										<tr <?php  if($passRow['seatStatus']==0){echo "data-seat_status=off";}elseif($passRow['seatStatus']==1){echo "data-seat_status=on";}?>>
										<td><span class="sl_no"><?php echo ++$i;?></span>
											<input data-field_name="prefix" name="pass<?php echo $i; ?>_prefix" value="<?php echo $passRow['prefix'];?>" type="hidden">
											<input data-field_name="passengerName" name="pass<?php echo $i; ?>_passengerName" value="<?php echo $passRow['passengerName'];?>" type="hidden">
											<input data-field_name="gender" name="pass<?php echo $i; ?>_gender" value="<?php if($passRow['gender']){ echo $passRow['gender'];}?>" type="hidden">
											<input data-field_name="dob" name="pass<?php echo $i; ?>_dob" value="<?php if($passRow['dob']){ echo $App->dbFormat_date($passRow['dob']);}?>" type="hidden">
											<input data-field_name="age" name="pass<?php echo $i; ?>_age" value="<?php echo $passRow['age'];?>" type="hidden">
											<input data-field_name="placeOfBirth" name="pass<?php echo $i; ?>_placeOfBirth" value="<?php echo $passRow['birthPlace'];?>" type="hidden">
											<input data-field_name="relationship" name="pass<?php echo $i; ?>_relationship" value="<?php echo $passRow['relation'];?>" type="hidden">
											
											<input data-field_name="nationality" name="pass<?php echo $i; ?>_nationality" value="Indian" value="<?php echo $passRow['nationality'];?>" type="hidden">
											<input data-field_name="mobile" name="pass<?php echo $i; ?>_mobile" value="<?php echo $passRow['mobile'];?>" type="hidden">
											<input data-field_name="passportNo" name="pass<?php echo $i; ?>_passportNo" value="<?php echo $passRow['passportNo'];?>" type="hidden">
											<input data-field_name="dateOfIssue" name="pass<?php echo $i; ?>_dateOfIssue" value="<?php if($passRow['dateOfIssue']){ echo $App->dbFormat_date($passRow['dateOfIssue']);}?>" type="hidden">
											<input data-field_name="issuePlace" name="pass<?php echo $i; ?>_issuePlace"value="<?php echo $passRow['issuePlace'];?>" type="hidden">
											<input data-field_name="validUpTo" name="pass<?php echo $i; ?>_validUpTo" value="<?php if($passRow['validUpTo']){ echo $App->dbFormat_date($passRow['validUpTo']);}?>" type="hidden">
											<input data-field_name="ticketNo" name="pass<?php echo $i; ?>_ticketNo" value="<?php echo $passRow['ticketNo'];?>" type="hidden">
											<input data-field_name="airInvoice" name="pass<?php echo $i; ?>_airInvoice" value="<?php echo $passRow['iaTotal'];?>" type="hidden">
											<input data-field_name="airPurchase" name="pass<?php echo $i; ?>_airPurchase" value="<?php echo $passRow['paNetAmount'];?>" type="hidden">
											<input data-field_name="roomType" name="pass<?php echo $i; ?>_roomType" value="<?php echo $passRow['roomType'];?>" type="hidden">
											<input data-field_name="hotInvoice" name="pass<?php echo $i; ?>_hotInvoice" value="<?php echo $passRow['ihTotal'];?>" type="hidden">
											<input data-field_name="hotPurchase" name="pass<?php echo $i; ?>_hotPurchase" value="<?php echo $passRow['phNetAmount'];?>" type="hidden">
											
											<input data-field_name="submissionDate" name="pass<?php echo $i; ?>_submissionDate" value="<?php if($passRow['submissionDate']){ echo $App->dbFormat_date($passRow['submissionDate']);}?>" type="hidden">
											<input data-field_name="collectionDate" name="pass<?php echo $i; ?>_collectionDate" value="<?php if($passRow['collectionDate']){ echo $App->dbFormat_date($passRow['collectionDate']);}?>" type="hidden">
											<input data-field_name="dispatchDate" name="pass<?php echo $i; ?>_dispatchDate" value="<?php if($passRow['dispatchDate']){ echo $App->dbFormat_date($passRow['dispatchDate']);}?>" type="hidden">
											<input data-field_name="visaInvoice" name="pass<?php echo $i; ?>_visaInvoice" value="<?php echo $passRow['ivTotal'];?>" type="hidden">
											<input data-field_name="visaPurchase" name="pass<?php echo $i; ?>_visaPurchase" value="<?php echo $passRow['pvNetAmount'];?>" type="hidden">
											<input data-field_name="transInvoice" name="pass<?php echo $i; ?>_transInvoice" value="<?php echo $passRow['itTotal'];?>" type="hidden">
											<input data-field_name="transPurchase" name="pass<?php echo $i; ?>_transPurchase" value="<?php echo $passRow['ptNetAmount'];?>" type="hidden">
												
										</td>
										<td><?php echo $passRow['passengerName'];?>
											<input name="pass<?php echo $i; ?>_invoiceTotal" value="<?php echo $passRow['passTotal'];?>" type="hidden">
											<!--<input name="pass<?php echo $i; ?>_purchaseTotal" value="1100" type="hidden">-->
										</td>
										<td><?php echo $passRow['age'];?></td>
										<td><?php echo $passRow['passportNo'];?></td>
										<td><?php echo $passRow['ticketNo'];?></td>
										<td><?php echo $passRow['iaTotal'];?></td>
										<td><?php echo $passRow['ihTotal'];?></td>
										<td><?php echo $passRow['ivTotal'];?></td>
										<td><?php echo $passRow['itTotal'];?></td>
										<td><?php echo $passRow['passTotal'];?></td>
										<td><a class="show_table_lnk show_table_lnk_view" href="#">View</a></td>
										</tr>
										<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						<div class="form_divider"></div>
                        <div class="row">
                            
                            <div class="col-lg-2 col-md-2 col-sm-2 " >
                                <div class="form_block">
                                    <label>TDS</label>
                                    <input type="text" name="tds" value="<?php echo $editRow['tds'];?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Processing Charge</label>
                                    <input type="text" name="mainProcCharge" value="<?php echo $editRow['proCharge'];?>" disabled="">
                                </div>
                             </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Discount</label>
                                    <input type="text" name="discount" value="<?php echo $editRow['discount'];?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Tax </label>
                                    <input type="text" name="mainTax" value="<?php echo $editRow['tax'];?>" disabled="">
                                </div>
                             </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charge</label>
                                    <input type="text" name="mainOtherCharge" value="<?php echo $editRow['otherCharge'];?>" disabled="">
                                </div>
                             </div>
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Net Amount</label>
                                    <input type="text" name="netAmount"  value="<?php echo $editRow['netAmount'];?>" disabled="">
                                </div>
                            </div>
                             </div>
                             <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2 ">
                                <div class="form_block">
                                    <label>Description</label>
                                    <textarea name="description" disabled=""><?php echo $editRow['description'];?></textarea>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
    <!-- Popups -->
    <div id="inv_package_passenger_pop" class="modal fade bd_modal passenger_pop" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Passenger Details</h4>
                </div>
                <div class="modal-body">
                    <form class="default_form" id="inv_package_passenger_pop" data-form_for="age_based">
                        <div id="inv_pack_pop_passenger_details">
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Passenger Name</label>
                                        <div class="aj_rel_box  aj_passenger">
                                            <input class="pass_to_table" type="text" name="passengerName"  autocomplete="off">
                                            <ul class="aj_live_items">

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Date of Birth</label>
                                        <input class="pass_to_table user_date" type="text" name="dob">
                                    </div>
                                </div>
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Gender</label>
                                        <input class="pass_to_table" type="text" name="gender" id="age">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Age</label>
                                        <input class="pass_to_table" type="text" name="age" id="age">
                                    </div>
                                </div>
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Place of Birth</label>
                                        <input class="pass_to_table" type="text" name="placeOfBirth">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Relation Ship</label>
                                        <select class="pass_to_table" name="relationship">
                                            <option value="">Select</option>
                                            <?php 
                                            $relQry	= mysql_query("select * FROM ".TABLE_RELATIONSHIP." where $check");
                                            while($relRow	=	mysql_fetch_array($relQry))
                                            {
                                            ?>
                                            <option value="<?php echo $relRow['ID'];?>"><?php echo $relRow['relationship']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                
                               
                            </div>

                            <div class="row">
                            	 <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Mahram</label>
                                        <select class="pass_to_table" name="mobile">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Nationality</label>
                                        <input class="pass_to_table" type="text" name="nationality">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Passport No</label>
                                        <input class="pass_to_table" type="text" name="passportNo">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Date of Issue</label>
                                        <input class="pass_to_table user_date" type="text" name="dateOfIssue">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Place of Issue</label>
                                        <input class="pass_to_table" type="text" name="issuePlace">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Date of Expire</label>
                                        <input class="pass_to_table user_date" type="text" name="validUpTo">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Container for dynamic form elements -->
                        <div id="inv_pack_dynamic_form_target">
						<?php if(in_array('airline',$pacServiceAry))
						{
						?>
						<div class="package_dynamic_box inv_pack_dynamic_airline" style="display: none;">
					        <h3 class="pack_dynamic_head">Airline Details</h3>
					        <div class="row">
					            <div class="col-lg-2 col-md-2 col-sm-2">
					                <div class="form_block">
					                    <label>Ticket No</label>
					                    <input class="pass_to_table" type="text" name="ticketNo" >
					                </div>
					            </div>
					        </div>
					        <h3>Total</h3>
					        <div class="row">
					            <div class="col-lg-2 col-md-2 col-sm-2">
					                <div class="form_block">
					                    <label>Invoice</label>
					                    <input class="pass_to_table" type="text" name="airInvoice" readonly>
					                </div>
					            </div>
					            <div class="col-lg-2 col-md-2 col-sm-2">
					                <div class="form_block">
					                    <label>Purchase</label>
					                    <input class="pass_to_table" type="text" name="airPurchase" readonly>
					                </div>
					            </div>
					        </div>
					    </div>
						<?php } ?>
						<?php if(in_array('hotel',$pacServiceAry))
						{
						?>
						<div class="inv_pack_dynamic_hotel">
				       <!-- <h3 class="pack_dynamic_head">Hotel</h3>-->
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Room Type</label>
				                    <select class="hotel_input pass_to_table" name="roomType">
				                   <?php $roomtQry = mysql_query("select * from ".TABLE_ROOM_TYPE." where proId='-1'");
		                    while($roomtRow	=	mysql_fetch_array($roomtQry))
		                    {
								?>
								<option value="<?php echo $roomtRow['ID'];?>" > <?php echo $roomtRow['roomType'];?></option>
								<?php
							}
		                    ?>
										
				                    </select>
				                </div>
				            </div>
				             <!--<div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                <label style="display: block;">Child</label>
				                    <label>With Bed</label>
				                    <input type="checkbox" name="" disabled="" />
				                </div>
				            </div>-->
				         </div>
				        <!--<h3>Total</h3>-->
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2"  style="display: none;">
				                <div class="form_block">
				                    <label>Invoice</label>
				                    <input class="pass_to_table" type="text" name="hotInvoice" readonly>
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2" style="display: none;">
				                <div class="form_block">
				                    <label>Purchase</label>
				                    <input class="pass_to_table" type="text" name="hotPurchase" readonly>
				                </div>
				            </div>
				        </div>
				    </div>
						<?php } ?>
						<?php if(in_array('visa',$pacServiceAry))
						{
						?>
						<div class="package_dynamic_box inv_pack_dynamic_visa" style="display: none">
				        <h3 class="pack_dynamic_head">Visa</h3>
				        <div class="inv_pack_visa_basics">
				            <div class="row">
				                <div class="col-lg-2 col-md-2 col-sm-2">
				                    <div class="form_block">
				                        <label>Visa for</label>
				                        <input class="pass_to_table" type="text" name="visaFor">
				                    </div>
				                </div>
				                <div class="col-lg-2 col-md-2 col-sm-2">
				                    <div class="form_block">
				                        <label>Submission Date</label>
				                        <input class="pass_to_table" type="text" name="submissionDate" data-dp="date_pic">
				                    </div>
				                </div>
				                <div class="col-lg-2 col-md-2 col-sm-2">
				                    <div class="form_block">
				                        <label>Collection Date</label>
				                        <input class="pass_to_table" type="text" name="collectionDate" data-dp="date_pic">
				                    </div>
				                </div>
				            </div>
				            <div class="row">
				                <div class="col-lg-2 col-md-2 col-sm-2">
				                    <div class="form_block">
				                        <label>Dispatch Date</label>
				                        <input class="pass_to_table" type="text" name="dispatchDate" data-dp="date_pic">
				                    </div>
				                </div>
				                <div class="col-lg-2 col-md-2 col-sm-2">
				                    <div class="form_block">
				                        <label>TokenNo</label>
				                        <input class="pass_to_table" type="text" name="tokenNo">
				                    </div>
				                </div>
				            </div>
				        </div>
				        <h3>Total</h3>
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Invoice</label>
				                    <input class="pass_to_table" type="text" name="visaInvoice" readonly>
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block">
				                    <label>Purchase</label>
				                    <input class="pass_to_table" type="text" name="visaPurchase" readonly>
				                </div>
				            </div>
				        </div>
				    </div>
						<?php } ?>
						<?php if(in_array('transportation',$pacServiceAry))
						{
						?>
						<div class="inv_pack_dynamic_transportation">
				        <!--<h3 class="pack_dynamic_head">Transportation</h3>-->
				       <!-- <h3>Total</h3>-->
				        <div class="row">
				            <div class="col-lg-2 col-md-2 col-sm-2" style="display:none;">
				                <div class="form_block">
				                    <label>Invoice</label>
				                    <input class="pass_to_table" type="text" name="transInvoice"  readonly>
				                </div>
				            </div>
				            <div class="col-lg-2 col-md-2 col-sm-2" style="display:none;">
				                <div class="form_block">
				                    <label>Purchase</label>
				                    <input class="pass_to_table" type="text" name="transPurchase"  readonly>
				                </div>
				            </div>
				             <div class="col-lg-2 col-md-2 col-sm-2">
				                <div class="form_block form_block_check">
				                	<label style="display: block;">Transportation</label>
				                    <input type="checkbox" name="seatStatus"  checked="" disabled=""/>
				                    <label class="check_label">With Seat</label>
				                </div>
				            </div>
				        </div>
				    </div>
						<?php } ?>
                        </div>
                        <!-- Container for dynamic form elements/ -->
                    </form>
                </div>
                <div class="modal-footer">
                    <!--<button type="button" class="btn btn-success" id="trigger_pass_table">OK</button>-->
                </div>
            </div>
        </div>
    </div>
    <!-- /Popups -->

<!-- Dynamic form elements for passenger -->
<div id="inv_pack_dynamic_source" style="display: none;">
    <!-- Airline -->
    <div class="package_dynamic_box inv_pack_dynamic_airline">
        <h3 class="pack_dynamic_head">Airline Details</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Ticket No</label>
                    <input class="pass_to_table" type="text" name="ticketNo" >
                </div>
            </div>
        </div>
        <h3>Total</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="airInvoice" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="airPurchase" readonly>
                </div>
            </div>
        </div>
    </div>
    <!-- /Airline -->
    <!-- Hotel -->
    <div class="package_dynamic_box inv_pack_dynamic_hotel">
        <h3 class="pack_dynamic_head">Hotel</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Room Type</label>
                    
                    <select class="hotel_input pass_to_table" name="roomType">
					 
                    </select>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                <label style="display: block;">Child</label>
                    <label>With Bed</label>
                    <input type="checkbox" name="" />
                </div>
            </div>
        </div>
        <h3>Total</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="hotInvoice" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="hotPurchase" readonly>
                </div>
            </div>
        </div>
    </div>
    <!-- /Hotel -->
    <!-- Visa -->
    <div class="package_dynamic_box inv_pack_dynamic_visa">
        <h3 class="pack_dynamic_head">Visa</h3>
        <div class="inv_pack_visa_basics">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Visa for</label>
                        <input class="pass_to_table" type="text" name="visaFor">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Submission Date</label>
                        <input class="pass_to_table" type="text" name="submissionDate" data-dp="date_pic">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Collection Date</label>
                        <input class="pass_to_table" type="text" name="collectionDate" data-dp="date_pic">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Dispatch Date</label>
                        <input class="pass_to_table" type="text" name="dispatchDate" data-dp="date_pic">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>TokenNo</label>
                        <input class="pass_to_table" type="text" name="tokenNo">
                    </div>
                </div>
            </div>
        </div>
        <h3>Total</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="visaInvoice" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="visaPurchase" readonly>
                </div>
            </div>
        </div>
    </div>
    <!-- /Visa -->
    <!-- Transportation -->
    <div class="package_dynamic_box inv_pack_dynamic_transportation">
        <h3 class="pack_dynamic_head">Transportation</h3>
        <h3>Total</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2" style="display:none;">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="transInvoice"  readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2" style="display:none;">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="transPurchase"  readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block form_block_check">
                    
                    <input type="checkbox" name="seatStatus"  checked=""/>
                    <label class="check_label">With Seat</label>
                </div>
            </div>
        </div>
    </div>
    <!-- /Transportation -->
</div>
<?php
require('../admin_footer2.php');
?>