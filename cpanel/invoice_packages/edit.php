<?php
require("../admin_header.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';
$editId		=	$_REQUEST['id'];
$editSelect = 	"select t1.ID,
						t1.invoiceNo,
						t1.invoiceDate,
						t1.packageId,
						t1.customerId,
						t1.agentId,
						t1.tds,
						t1.tax,
						t1.proCharge,
						t1.otherCharge,
						t1.discount,
						t1.netAmount,
						t1.description,
						t2.accountName
                   from ".TABLE_INVOICE_PACKAGE." as t1
              LEFT JOIN ".TABLE_ACCOUNTS." as t2 on t2.ID=t1.customerId 
                  where  t1.ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);

//for getting the services specified in that purticular package
$packageId 		=	$editRow['packageId'];
$pacServiceQry	=	mysql_query("SELECT services,confirm,extra,discount,adultTotal FROM ".TABLE_PACKAGE." WHERE ID='$packageId'");
$pacServiceRow	=	mysql_fetch_array($pacServiceQry);
$pacService		=	$pacServiceRow['services'];
$pacServiceAry	=	explode(",",$pacService);
?>

<script>

	//calc age
function getAge()
{
	var dob=document.getElementById('dob').value;
	//alert(dob);
	//today
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd='0'+dd
	} 

	if(mm<10) {
	    mm='0'+mm
	} 

	today = dd+'-'+mm+'-'+yyyy;
	var dobSplit= dob.split('-');
	var todaySplit= today.split('-');
	
	var dobLast = new Date(dobSplit[2], +dobSplit[1]-1, dobSplit[0]);
	var todayLast = new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
	var dateDiff=(todayLast.getTime() - dobLast.getTime()) / (1000*60*60*24);
	var age=Math.round(dateDiff/365);
	document.getElementById('age').value=age;
}

</script>
<script>

	function valid()
	{
		flag=false;
		tds = document.getElementById('tds').value;	
			if(isNaN(tds) && tds!=0)
			{		
				document.getElementById('tdsdiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		mainProcCharge = document.getElementById('mainProcCharge').value;	
			if(isNaN(mainProcCharge) && mainProcCharge!=0)
			{		
				document.getElementById('mainProcChargediv').innerHTML="Enter valid amount.";
				flag=true;
			}
		discount = document.getElementById('discount').value;	
			if(isNaN(discount) && discount!=0)
			{		
				document.getElementById('discountdiv').innerHTML="Enter valid amount.";
				flag=true;
			}
		/*mainTax = document.getElementById('mainTax').value;	
			if(isNaN(mainTax) && mainTax!=0)
			{		
				document.getElementById('mainTaxdiv').innerHTML="Enter valid amount.";
				flag=true;
			}*/
			
		mainOtherCharge = document.getElementById('mainOtherCharge').value;	
			if(isNaN(mainOtherCharge) && mainOtherCharge!=0)
			{		
				document.getElementById('mainOtherChargediv').innerHTML="Enter valid amount.";
				flag=true;
			}
		passCount = document.getElementById('inv_pack_passenger_count').value;	
			if(passCount == 0)
			{		
				document.getElementById('passCountdiv').innerHTML="Add Passenger.";
				flag=true;
			}	
		netAmount = document.getElementById('netAmount').value;	
		//alert(netAmount);
			if(netAmount <= 0)
			{		
				document.getElementById('netAmountdiv').innerHTML="Invalid Amount.";
				flag=true;
			}
			//for customer name and customer id validation	
		var customerName	=	document.getElementById('customerName').value;
		var customerId		=	document.getElementById('customerId').value;
		var getResponse=0;
		
		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
           // document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
          	getResponse= xmlhttp.responseText;//alert(getResponse);
          	
          	 
          	
          }	
          
	    };
	   
	   	xmlhttp.open("GET", "../../services/customer_check_ajax.php?name="+customerName+"&id="+customerId, false);
	    xmlhttp.send();
	  
		 		if(getResponse==0)
	          	{
	          		flag=true;
alert("Invalid Customer Name and ID.!");
				}

		if(flag==true)
		{
		return false;
		}
	}
	//clear the validation msg
	function clearbox(Element_id)
	{
	document.getElementById(Element_id).innerHTML="";
	}

	
</script>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form class="default_form" id="invoice_package" method="post" action="do.php?op=edit" onsubmit="return valid()">
                 <input  type="hidden" name="editId" value="<?php echo $editId;?>"/>
                    <div class="bd_panel_head">
                        <h3>Package - Invoice</h3>
                    </div>
                    <div class="bd_panel_body">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher No</label>
                                    <input type="text" name="invoiceNo" value="<?php echo "PI" . $editRow['invoiceNo']; ?>" required="" readonly="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Date</label>
                                    <input type="text" name="invoiceDate" class="user_date"
                                           value="<?php echo $App->dbFormat_date($editRow['invoiceDate']); ?>" >
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Customer Name <span class="valid">*</span></label> <!--select from account where accGpId=37-->    
                                    <div class="aj_rel_box aj_customer">
                                        <input type="text" name="customerName" autocomplete="off" required="" value="<?php echo $editRow['accountName'];?>" id="customerName">
                                        <ul class="aj_live_items">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer ID</label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerId" autocomplete="off" required="" value="<?php echo $editRow['customerId'];?>" id="customerId">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2 form_block_row">
                            <div class="form_block">
                                <label>Agent Name <span class="valid">*</span></label>
                                <select name="agentId" required="">
                                    <option value="">Select</option>
                                    <?php
                                    $select = "select ID,accountName 
                                    			from " . TABLE_ACCOUNTS . " 
			                                    where accountGroupId=48
			                                    AND finYear='$finYear'
			                                    AND proId='$proId' 
			                                    Order by accountName";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        ?>
                                        <option value="<?php echo $row['ID']; ?>" <?php if($editRow['agentId']==$row['ID']){ echo "selected"; } ?>><?php echo $row['accountName']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
						
						</div>
						<div class="row">
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Select Package <span class="valid">*</span></label> 
                                    <select name="package" required="">
                                        <option value="">Select</option>
                                        <?php
                                        $pacQry = mysql_query("SELECT * FROM " . TABLE_PACKAGE . " WHERE date>now() AND proId = $proId or proId ='0' and finYear=$finYear");
                                        while ($pacRow = mysql_fetch_array($pacQry)) {
                                            ?>
                                            <option value="<?php echo $pacRow['ID']; ?>" data-dynamic_form="<?php echo $pacRow['services']; ?>"  <?php if($pacRow['ID']==$editRow['packageId']){echo "selected";}?> ><?php echo $pacRow['packageName']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Package Rate</label>
                                    <div id="package_rate_show" style="height: 34px; background-color: #CCC; padding-left7px; padding-top: 7px;"><?php echo $pacServiceRow['adultTotal'];?></div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="form_divider"></div>
                        <div class="multi_det">
                            <div class="multi_det_body">
                                <div class="multi_det_head">
                                    <h3>Passenger List</h3>
                                    <div class="multi_det_control">
                                        <ul class="page_nav">
                                            <li>
                                                <a href="#" id="inv_pack_modal_trigger"
                                                   data-modal_target="#inv_package_passenger_pop">
                                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                                    Add New
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="bd_clear"></div>
                                </div>
                                <div class="multi_det_table_wrap table-responsive">
                                    <table class="table table-bordered multi_det_table" id="inv_package_paasenger_table">
                                        <thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Passenger</th>
                                            <th>Age</th>
                                            <th>Passport No</th>
                                            <th>Airline Ticket No</th>
                                            <th>Airline Amt</th>
                                            <th>Hotel Amt</th>
                                            <th>Visa Amt</th>
                                            <th>Transportation Amt</th>
                                            <th>Total</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                         <?php 
        $passQry	=	"SELECT	p.ID,p.in_packageId,p.packageId,p.passengerName,p.dob,p.gender,p.prefix,
        						p.age,p.nationality,p.birthPlace,p.relation,p.mobile,
        						p.passportNo,p.dateOfIssue,
        						p.validUpTo,p.issuePlace,p.total as passTotal,
        						ia.ticketNo,ia.total as iaTotal,
        						pa.netAmount as paNetAmount,
        						ih.roomType,ih.bedStatus,
        						ih.total as ihTotal,
        						ph.netAmount as phNetAmount,
        						iv.submissionDate,iv.collectionDate,
        						pv.netAmount as pvNetAmount,
        						iv.dispatchDate,iv.total as ivTotal,
        						it.total as itTotal,it.seatStatus,
        						pt.netAmount as ptNetAmount
        				   FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." as p
        			  LEFT JOIN ".TABLE_INVOICE_PACKAGE_AIRLINE." as ia on ia.passengerId=p.ID
        			  LEFT JOIN ".TABLE_PURCHASE_PACKAGE_AIRLINE." as pa on pa.passengerId=p.ID
        			  LEFT JOIN ".TABLE_INVOICE_PACKAGE_HOTEL." as ih on ih.passengerId=p.ID
        			  LEFT JOIN ".TABLE_PURCHASE_PACKAGE_HOTEL." as ph on ph.passengerId=p.ID
        			  LEFT JOIN ".TABLE_INVOICE_PACKAGE_VISA." as iv on iv.passengerId=p.ID
        			  LEFT JOIN ".TABLE_PURCHASE_PACKAGE_VISA." as pv on pv.passengerId=p.ID
        			  LEFT JOIN ".TABLE_INVOICE_PACKAGE_TRANSPORTATION." as it on it.passengerId=p.ID
        			  LEFT JOIN ".TABLE_PURCHASE_PACKAGE_TRANSPORT." as pt on pt.passengerId=p.ID
        				  WHERE p.in_packageId='$editId'
        			   GROUP BY	p.ID
        				";
        				//echo $passQry;die;
                                        $passResult	=	mysql_query($passQry);
                                        $passNum	=	mysql_num_rows($passResult);
                                        if($passNum>0)
                                        {
                                        $i=0;
                                        while($passRow	=	mysql_fetch_array($passResult))
                                        {

                                        ?>
                                        <tr <?php  if($passRow['seatStatus']==0){echo "data-seat_status=off";}elseif($passRow['seatStatus']==1){echo "data-seat_status=on";}?>  data-additional_extra="<?php echo $pacServiceRow['extra'];?>" data-additional_discount="<?php echo $pacServiceRow['discount'];?>" data-hotel_room_status="<?php echo $passRow['bedStatus'];?>" >
										<td><span class="sl_no"><?php echo ++$i;?></span>
                                                                                        <input data-field_name="ID" name="pass<?php echo $i; ?>_ID" value="<?php echo $passRow['ID'];?>" type="hidden">
											<input data-field_name="prefix" name="pass<?php echo $i; ?>_prefix" value="<?php echo $passRow['prefix'];?>" type="hidden">
											<input data-field_name="passengerName" name="pass<?php echo $i; ?>_passengerName" value="<?php echo $passRow['passengerName'];?>" type="hidden">
											<input data-field_name="gender" name="pass<?php echo $i; ?>_gender" value="<?php if($passRow['gender']){ echo $passRow['gender'];}?>" type="hidden">
											<input data-field_name="dob" name="pass<?php echo $i; ?>_dob" value="<?php if($passRow['dob']){ echo $App->dbFormat_date($passRow['dob']);}?>" type="hidden">
											<input data-field_name="age" name="pass<?php echo $i; ?>_age" value="<?php echo $passRow['age'];?>" type="hidden">
											<input data-field_name="placeOfBirth" name="pass<?php echo $i; ?>_placeOfBirth" value="<?php echo $passRow['birthPlace'];?>" type="hidden">
											<input data-field_name="relationship" name="pass<?php echo $i; ?>_relationship" value="<?php echo $passRow['relation'];?>" type="hidden">
											
											<input data-field_name="nationality" name="pass<?php echo $i; ?>_nationality" value="Indian" value="<?php echo $passRow['nationality'];?>" type="hidden">
											<input data-field_name="mobile" name="pass<?php echo $i; ?>_mobile" value="<?php echo $passRow['mobile'];?>" type="hidden">
											<input data-field_name="passportNo" name="pass<?php echo $i; ?>_passportNo" value="<?php echo $passRow['passportNo'];?>" type="hidden">
											<input data-field_name="dateOfIssue" name="pass<?php echo $i; ?>_dateOfIssue" value="<?php if($passRow['dateOfIssue']){ echo $App->dbFormat_date($passRow['dateOfIssue']);}?>" type="hidden">
											<input data-field_name="issuePlace" name="pass<?php echo $i; ?>_issuePlace"value="<?php echo $passRow['issuePlace'];?>" type="hidden">
											<input data-field_name="validUpTo" name="pass<?php echo $i; ?>_validUpTo" value="<?php if($passRow['validUpTo']){ echo $App->dbFormat_date($passRow['validUpTo']);}?>" type="hidden">
											<input data-field_name="ticketNo" name="pass<?php echo $i; ?>_ticketNo" value="<?php echo $passRow['ticketNo'];?>" type="hidden">
											<input data-field_name="airInvoice" name="pass<?php echo $i; ?>_airInvoice" value="<?php echo $passRow['iaTotal'];?>" type="hidden">
											<input data-field_name="airPurchase" name="pass<?php echo $i; ?>_airPurchase" value="<?php echo $passRow['paNetAmount'];?>" type="hidden">
											<input data-field_name="roomType" name="pass<?php echo $i; ?>_roomType" value="<?php echo $passRow['roomType'];?>" type="hidden">
											<input data-field_name="hotInvoice" name="pass<?php echo $i; ?>_hotInvoice" value="<?php echo $passRow['ihTotal'];?>" type="hidden">
											<input data-field_name="hotPurchase" name="pass<?php echo $i; ?>_hotPurchase" value="<?php echo $passRow['phNetAmount'];?>" type="hidden">
											
											<input data-field_name="submissionDate" name="pass<?php echo $i; ?>_submissionDate" value="<?php if($passRow['submissionDate']){ echo $App->dbFormat_date($passRow['submissionDate']);}?>" type="hidden">
											<input data-field_name="collectionDate" name="pass<?php echo $i; ?>_collectionDate" value="<?php if($passRow['collectionDate']){ echo $App->dbFormat_date($passRow['collectionDate']);}?>" type="hidden">
											<input data-field_name="dispatchDate" name="pass<?php echo $i; ?>_dispatchDate" value="<?php if($passRow['dispatchDate']){ echo $App->dbFormat_date($passRow['dispatchDate']);}?>" type="hidden">
											<input data-field_name="visaInvoice" name="pass<?php echo $i; ?>_visaInvoice" value="<?php echo $passRow['ivTotal'];?>" type="hidden">
											<input data-field_name="visaPurchase" name="pass<?php echo $i; ?>_visaPurchase" value="<?php echo $passRow['pvNetAmount'];?>" type="hidden">
											<input data-field_name="transInvoice" name="pass<?php echo $i; ?>_transInvoice" value="<?php echo $passRow['itTotal'];?>" type="hidden">
											<input data-field_name="transPurchase" name="pass<?php echo $i; ?>_transPurchase" value="<?php echo $passRow['ptNetAmount'];?>" type="hidden">
											<input  data-field_name="seatStatus" name="pass<?php echo $i; ?>_seatStatus" value="<?php echo $passRow['seatStatus'];?>" type="hidden">
												
										</td>
										<td>
										<?php echo $passRow['prefix'];?>
										<?php echo $passRow['passengerName'];?>
											<input name="pass<?php echo $i; ?>_invoiceTotal" value="<?php echo $passRow['passTotal'];?>" type="hidden">
											
											<input name="pass<?php echo $i; ?>_bedStatus" value="<?php echo $passRow['bedStatus'];?>" type="hidden">
											<input name="pass<?php echo $i; ?>_seatStatus" value="<?php echo $passRow['seatStatus'];?>" type="hidden">
											<input name="pass<?php echo $i; ?>_purchaseTotal" value="0" type="hidden">
										</td>
										<td><?php echo $passRow['age'];?></td>
										<td><?php echo $passRow['passportNo'];?></td>
										<td><?php echo $passRow['ticketNo'];?></td>
										<td><?php echo $passRow['iaTotal'];?></td>
										<td><?php echo $passRow['ihTotal'];?></td>
										<td><?php echo $passRow['ivTotal'];?></td>
										<td><?php echo $passRow['itTotal'];?></td>
										<td><?php echo $passRow['passTotal'];?></td>
										<td>
										<a class="show_table_lnk show_table_lnk_view" href="#">View</a>
										<a class="show_table_lnk show_table_lnk_edit" href="#">Edit</a>
										<a class="show_table_lnk show_table_lnk_del" href="#">Delete</a>
										</td>
										</tr>
										<?php }
										} ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						<div class="form_divider"></div>
                        <div class="row">
                            <div class="col-lg-2 col-sm-2 col-md-2 ">
                                
                                <div class="form_block">
                                    <label>TDS</label>
                                    <input type="text" name="tds" id="tds" onfocus="clearbox('tdsdiv')" value="<?php echo $editRow['tds'];?>">
                                    <div id="tdsdiv" class="valid" style="color:#FF6600;"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Processing Charge</label>
                                    <input type="text" name="mainProcCharge" id="mainProcCharge" onfocus="clearbox('mainProcChargediv')" value="<?php echo $editRow['proCharge'];?>">
                                    <div id="mainProcChargediv" class="valid" style="color:#FF6600;"></div>
                                </div>
                             </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Discount</label>
                                    <input type="text" name="discount" id="discount" onfocus="clearbox('discountdiv')" value="<?php echo $editRow['discount'];?>">
                                    <div id="discountdiv" class="valid" style="color:#FF6600;"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Tax ('%' for percentage)</label>
                                    <input type="text" name="mainTax" id="mainTax" onfocus="clearbox('mainTaxdiv')" value="<?php echo $editRow['tax'];?>">
                                    <div id="mainTaxdiv" class="valid" style="color:#FF6600;"></div>
                                </div>
                             </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charge</label>
                                    <input type="text" name="mainOtherCharge" id="mainOtherCharge" onfocus="clearbox('mainOtherChargediv')" value="<?php echo $editRow['otherCharge'];?>">
                                    <div id="mainOtherChargediv" class="valid" style="color:#FF6600;"></div>
                                </div>
                             </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Net Amount</label>
                                    <input type="text" id="netAmount" name="netAmount" readonly="" required="" value="<?php echo $editRow['netAmount'];?>" onfocus="clearbox('netAmountdiv')" >
                                    <div id="netAmountdiv" class="valid" style="color:#FF6600;"></div>
                                </div>
                            </div>
                           
                        </div>
                        <div class="row">
                        	
                           
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Description</label>
                                    <textarea name="description"><?php echo $editRow['description'];?></textarea>
                                </div>
                            </div>
                        </div>
                        <div id="passCountdiv" class="valid" style="color:#FF6600;"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="airline_submit">
                                    <input id="inv_pack_passenger_count" type="hidden" name="passengerRowCount" <?php if($passNum>0){echo "value='$passNum'" ; }?> value="0">
                                    <input type="submit" value="UPDATE" onclick="return check()">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
    <!-- Popups -->
    <?php $packageTotalQry	=	mysql_query("SELECT tds, otherCharge, tax,extra,discount FROM ".TABLE_PACKAGE." WHERE ID=$packageId");
    $packageTotalRow		=	mysql_fetch_array($packageTotalQry);
    ?>
    <div id="inv_package_passenger_pop" class="modal fade bd_modal passenger_pop" role="dialog" data-general_amt="<?php echo $packageTotalRow['tds']+ $packageTotalRow['otherCharge']+ $packageTotalRow['tax']; ?>" data-additional_extra="<?php echo $pacServiceRow['extra'];?>" data-additional_discount="<?php echo $pacServiceRow['discount'];?>">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Passenger Details</h4>
                </div>
                <div class="modal-body">
                    <form class="default_form" id="inv_package_passenger_pop_form" data-form_for="age_based">
                        <div id="inv_pack_pop_passenger_details">
                            <div class="row">
                            	 <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Honorifics</label>
		                                <select class="pass_to_table" name="prefix">
											<option value="">Select</option>
											<option value="Mr">Mr</option>
											<option value="Mrs">Mrs</option>
											<option value="Ms">Ms</option>
										</select>
									 </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Passenger Name</label>
                                         <input class="pass_to_table" type="hidden" name="ID"  autocomplete="off">
                                        <div class="aj_rel_box  aj_passenger">
                                            <input class="pass_to_table" type="text" name="passengerName"  autocomplete="off">
                                            <ul class="aj_live_items">

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Gender</label>
		                                <select class="pass_to_table" name="gender">
											<option value="M" selected="">Male</option>
											<option value="F">Female</option>
										</select>
									 </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Date of Birth</label>
                                        <input class="pass_to_table user_date" type="text" name="dob">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Age</label>
                                        <input class="pass_to_table" type="text" name="age" id="age">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Place of Birth</label>
                                        <input class="pass_to_table" type="text" name="placeOfBirth">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Relation Ship</label>
                                         <select class="pass_to_table" name="relationship">
                                            <option value="">Select</option>
                                            <?php 
                                            $relQry	= mysql_query("select * FROM ".TABLE_RELATIONSHIP." where proId=$proId");
                                            while($relRow	=	mysql_fetch_array($relQry))
                                            {
                                            ?>
                                            <option value="<?php echo $relRow['ID'];?>"><?php echo $relRow['relationship']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                            	<div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Mahram</label>
                                         <select class="pass_to_table" name="mobile">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Nationality</label>
                                        <input class="pass_to_table" type="text" name="nationality">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Passport No</label>
                                        <input class="pass_to_table" type="text" name="passportNo">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Date of Issue</label>
                                        <input class="pass_to_table user_date" type="text" name="dateOfIssue">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Place of Issue</label>
                                        <input class="pass_to_table" type="text" name="issuePlace">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <div class="form_block">
                                        <label>Date of Expire</label>
                                        <input class="pass_to_table user_date" type="text" name="validUpTo">
                                    </div>
                                </div>
                            </div>
                         
                          
                        </div>
                        <!-- Container for dynamic form elements -->
                        <div id="inv_pack_dynamic_form_target">
							<?php if(in_array('airline',$pacServiceAry))
							{
							?>
							<div class="package_dynamic_box inv_pack_dynamic_airline" style="display: none;">
						        <h3 class="pack_dynamic_head">Airline Details</h3>
						        <div class="row">
						            <div class="col-lg-2 col-md-2 col-sm-2">
						                <div class="form_block">
						                    <label>Ticket No</label>
						                    <input class="pass_to_table" type="text" name="ticketNo" >
						                </div>
						            </div>
						        </div>
						        <h3>Total</h3>
						        <div class="row">
						        <?php
						         $airlineRateQry = mysql_query("SELECT adultTotal, childTotal, infantTotal FROM `".TABLE_PACKAGE_AIRLINE."` WHERE packageId = ".$packageId);
						         $airlineRateRow = mysql_fetch_array($airlineRateQry);
						          $airlinePurQry = mysql_query("SELECT adultTotal, childTotal, infantTotal FROM `".TABLE_PURCHASE_PAC_AIRLINE."` WHERE packageId = ".$packageId);
						          $airlinePurRow	=	mysql_fetch_array($airlinePurQry);
						        ?>
						            <div class="col-lg-2 col-md-2 col-sm-2">
						                <div class="form_block">
						                    <label>Invoice</label>
						                    <input class="pass_to_table" type="text" name="airInvoice" readonly data-adult_total="<?php echo $airlineRateRow['adultTotal'];?>" data-child_total="<?php echo $airlineRateRow['childTotal'];?>" data-infant_total="<?php echo $airlineRateRow['infantTotal'];?>">
						                </div>
						            </div>
						            <div class="col-lg-2 col-md-2 col-sm-2">
						                <div class="form_block">
						                    <label>Purchase</label>
						                    <input class="pass_to_table" type="text" name="airPurchase" readonly data-adult_total="<?php echo $airlinePurRow['adultTotal'];?>" data-child_total="<?php echo $airlinePurRow['childTotal'];?>" data-infant_total="<?php echo $airlinePurRow['infantTotal'];?>">
						                </div>
						            </div>
						        </div>
						    </div>
							<?php }?>
							<?php if(in_array('hotel',$pacServiceAry))
							{
							?>
							<div class="inv_pack_dynamic_hotel">
						        <div class="row">
						        <?php 
						         $hotelRateQry = mysql_query("SELECT p3.roomType, p3.ID, SUM(p2.total) AS roomInvTotal, SUM(p2.purchaseAmount) AS roomPurTotal FROM `".TABLE_PACKAGE_HOTEL."` AS p1 INNER JOIN `".TABLE_PACKAGE_HOTEL_ROOM_TYPE."` AS p2 ON p1.ID = p2.package_hotelId INNER JOIN `".TABLE_ROOM_TYPE."` AS p3 ON p3.ID = p2.roomId WHERE p1.packageId = ".$packageId." GROUP BY p2.roomId");
						        
						        ?>
						            <div class="col-lg-2 col-md-2 col-sm-2">
						                <div class="form_block">
						                    <label>Room Type</label>
						                    <select class="hotel_input pass_to_table" name="roomType">
						                        	
						                        <?php  while($hotelRateRow = mysql_fetch_array($hotelRateQry)){?>
						                        <option data-room_rate="<?php echo $hotelRateRow['roomInvTotal'];?>" data-room_pur_total="<?php echo $hotelRateRow['roomPurTotal'];?>" value="<?php echo $hotelRateRow['ID'] ?>"><?php echo $hotelRateRow['roomType'] ?></option>	
						                        <?php }?>
						                    </select>
						                </div>
						            </div>
						            <div class="col-lg-2 col-md-2 col-sm-2" style="display: none;">
						                <div class="form_block form_block_check">
						                    <label style="display: block;">Child</label>
						                    <input type="checkbox" name="" />
						                     <label class="check_label">With Bed</label>
						                </div>
						            </div>
						        </div>
						        <div class="row" style="display: none;">
						            <div class="col-lg-2 col-md-2 col-sm-2">
						                <div class="form_block">
						                    <label>Invoice</label>
						                    <input class="pass_to_table" type="text" name="hotInvoice" readonly>
						                </div>
						            </div>
						            <div class="col-lg-2 col-md-2 col-sm-2">
						                <div class="form_block">
						                    <label>Purchase</label>
						                    <input class="pass_to_table" type="text" name="hotPurchase" readonly>
						                </div>
						            </div>
						        </div>
						    </div>
							<?php } ?>
							<?php if(in_array('visa',$pacServiceAry))
							{
							?>
							<div class="package_dynamic_box inv_pack_dynamic_visa" style="display: none">
						        <h3 class="pack_dynamic_head">Visa</h3>
						        <div class="inv_pack_visa_basics">
						            <div class="row">
						                <div class="col-lg-2 col-md-2 col-sm-2">
						                    <div class="form_block">
						                        <label>Submission Date</label>
						                        <input class="pass_to_table" type="text" name="submissionDate" data-dp="date_pic">
						                    </div>
						                </div>
						                <div class="col-lg-2 col-md-2 col-sm-2">
						                    <div class="form_block">
						                        <label>Collection Date</label>
						                        <input class="pass_to_table" type="text" name="collectionDate" data-dp="date_pic">
						                    </div>
						                </div>
						            </div>
						            <div class="row">
						                <div class="col-lg-2 col-md-2 col-sm-2">
						                    <div class="form_block">
						                        <label>Dispatch Date</label>
						                        <input class="pass_to_table" type="text" name="dispatchDate" data-dp="date_pic">
						                    </div>
						                </div>
						            </div>
						        </div>
						        <h3>Total</h3>
						        <div class="row">
						        <?php 
						        $visaRateQry = mysql_query("SELECT SUM(total) AS invTotal, SUM(purchaseAmount) AS purTotal FROM `".TABLE_PACKAGE_VISA."` WHERE packageId = ".$packageId." GROUP BY packageId");
						        $visaRateRow = mysql_fetch_array($visaRateQry);
						        
						        ?>
						            <div class="col-lg-2 col-md-2 col-sm-2">
						                <div class="form_block">
						                    <label>Invoice</label>
						                    <input class="pass_to_table" type="text" name="visaInvoice" readonly value="<?php echo $visaRateRow['invTotal']?>">
						                </div>
						            </div>
						            <div class="col-lg-2 col-md-2 col-sm-2">
						                <div class="form_block">
						                    <label>Purchase</label>
						                    <input class="pass_to_table" type="text" name="visaPurchase" readonly value="<?php echo $visaRateRow['purTotal']?>">
						                </div>
						            </div>
						        </div>
						    </div>
							<?php } ?>
							<?php if(in_array('transportation',$pacServiceAry))
							{
							?>
							<div class="inv_pack_dynamic_transportation">
						        <!--<h3 class="pack_dynamic_head">Transportation</h3>-->
						        <!--<h3>Total</h3>-->
						        <div class="row">
						        <?php 
						         $transportationRateQry = mysql_query("SELECT SUM(seatCharge) AS invTotal, SUM(purchaseAmount) AS purTotal FROM `".TABLE_PACKAGE_TRANSPORTATION."` WHERE packageId = ".$packageId." GROUP BY packageId");
						         $transportationRateRow = mysql_fetch_array($transportationRateQry);
						        ?>
						            <div class="col-lg-2 col-md-2 col-sm-2" style="display:none;">
						                <div class="form_block">
						                    <label>Invoice</label>
						                    <input class="pass_to_table" type="text" name="transInvoice" data-inv_total="<?php echo $transportationRateRow['invTotal'];?>" value="<?php echo $transportationRateRow['invTotal'];?>" readonly>
						                </div>
						            </div>
						            <div class="col-lg-2 col-md-2 col-sm-2" style="display:none;">
						                <div class="form_block">
						                    <label>Purchase</label>
						                    <input class="pass_to_table" type="text" name="transPurchase" data-pur_total="<?php echo $transportationRateRow['purTotal'];?>"  value="<?php echo $transportationRateRow['purTotal'];?>" readonly>
						                </div>
						            </div>
						            <div class="col-lg-2 col-md-2 col-sm-2">
						                <div class="form_block form_block_check">
						                	<label style="display: block;">Transportation</label>
						                    <input type="checkbox" name="seatStatus"  checked=""/>
						                    <label class="check_label">With Seat</label>
						                </div>
						            </div>
						        </div>
						    </div>
							<?php } ?>
                        </div>
                        <!-- Container for dynamic form elements/ -->
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="trigger_pass_table">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /Popups -->

<!-- Dynamic form elements for passenger -->
<div id="inv_pack_dynamic_source" style="display: none;">
    <!-- Airline -->
    <div class="package_dynamic_box inv_pack_dynamic_airline" style="display: none;">
        <h3 class="pack_dynamic_head">Airline Details</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Ticket No</label>
                    <input class="pass_to_table" type="text" name="ticketNo" >
                </div>
            </div>
        </div>
        <h3>Total</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="airInvoice" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="airPurchase" readonly>
                </div>
            </div>
        </div>
    </div>
    <!-- /Airline -->
    <!-- Hotel -->
    <div class="inv_pack_dynamic_hotel">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Room Type</label>
                    <select class="hotel_input pass_to_table" name="roomType">
                        <option>Select</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2" style="display: none;">
                <div class="form_block form_block_check">
                    <label style="display: block;">Child</label>
                    <input type="checkbox" name="" />
                     <label class="check_label">With Bed</label>
                </div>
            </div>
        </div>
        <div class="row" style="display: none;">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="hotInvoice" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="hotPurchase" readonly>
                </div>
            </div>
        </div>
    </div>
    <!-- /Hotel -->
    <!-- Visa -->
    <div class="package_dynamic_box inv_pack_dynamic_visa" style="display: none">
        <h3 class="pack_dynamic_head">Visa</h3>
        <div class="inv_pack_visa_basics">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Submission Date</label>
                        <input class="pass_to_table" type="text" name="submissionDate" data-dp="date_pic">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Collection Date</label>
                        <input class="pass_to_table" type="text" name="collectionDate" data-dp="date_pic">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form_block">
                        <label>Dispatch Date</label>
                        <input class="pass_to_table" type="text" name="dispatchDate" data-dp="date_pic" >
                    </div>
                </div>
            </div>
        </div>
        <h3>Total</h3>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="visaInvoice" readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="visaPurchase" readonly>
                </div>
            </div>
        </div>
    </div>
    <!-- /Visa -->
    <!-- Transportation -->
    <div class="inv_pack_dynamic_transportation">
        <!--<h3 class="pack_dynamic_head">Transportation</h3>-->
        <!--<h3>Total</h3>-->
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2" style="display:none;">
                <div class="form_block">
                    <label>Invoice</label>
                    <input class="pass_to_table" type="text" name="transInvoice"  readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2" style="display:none;">
                <div class="form_block">
                    <label>Purchase</label>
                    <input class="pass_to_table" type="text" name="transPurchase"  readonly>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="form_block form_block_check">
                	<label style="display: block;">Transportation</label>
                    <input type="checkbox" name="seatStatus"  checked=""/>
                    <label class="check_label">With Seat</label>
                </div>
            </div>
        </div>
    </div>
    <!-- /Transportation -->
</div>
<?php
require('../admin_footer2.php');
?>