<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if(@isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
function valid()
{	
	flag=false;	
	
	  
	tds=document.getElementById('tds').value;
	tax=document.getElementById('tax');
	letterNumber =  /^[0-9]*\.?[0-9%]+$/;
	basicAmount=document.getElementById('basicAmount').value;
	otherCharge=document.getElementById('otherCharge').value;
	procCharge=document.getElementById('procCharge').value;
	discount=document.getElementById('discount').value;	
	netAmount=document.getElementById('netAmount').value;
	
	if(isNaN(basicAmount) || basicAmount=="" || basicAmount=="0")
	{																			
	document.getElementById('basicAmountDiv').innerHTML="Enter valid amount.";
	flag=true;
	}
	if(isNaN(tds) || tds=="")
	{																			
	document.getElementById('tdsDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(!tax.value.match(letterNumber))
	{																			
	document.getElementById('taxDiv').innerHTML="Enter valid Tax number.";
	flag=true;
	}
	if(isNaN(otherCharge) || otherCharge=="")
	{																			
	document.getElementById('otherChargeDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(procCharge) || procCharge=="")
	{																			
	document.getElementById('procChargeDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(discount) || discount=="")
	{																			
	document.getElementById('discountDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(netAmount) || netAmount=="" || netAmount==0 || netAmount <0)
	{																			
	document.getElementById('netAmountDiv').innerHTML="Enter valid amount";
	flag=true;
	}	
	
	
	//for customer name and customer id validation	
		var customerName	=	document.getElementById('customerName').value;
		var customerId		=	document.getElementById('customerId').value;
		var getResponse=0;
		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
           // document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
          	getResponse= xmlhttp.responseText;//alert(getResponse);
          	
          	 
          	
          }	
          
	    };
	   
	   	xmlhttp.open("GET", "../../services/customer_check_ajax.php?name="+customerName+"&id="+customerId, false);
	    xmlhttp.send();
	  
	
		 		if(getResponse==0)
	          	{
	          		flag=true;
                                alert("Invalid Customer Name and ID.!");
				}
	
	if(flag==true)
	{
	return false;
	}
	
}

//clear the validation msg

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
	
</script>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
<div class="row">
	<div class="col-lg-12">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form method="post" action="do.php?op=index" class="default_form" id="misc_form"  onsubmit="return valid()">
				<div class="bd_panel_head">
					<h3>MISCELLANEOUS - INVOICE</h3>
				</div>
				<div class="bd_panel_body">
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Voucher No <span class="valid">*</span></label>
								<input type="text" name="invoiceNo" value="<?php echo "MI". $db->nextVoucher(TABLE_INVOICE_MISC,'invoiceNo',"proId='$proId'"); ?>" required="" readonly="">
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Voucher Date </label>
								<input type="text" name="invoiceDate" value="<?php echo date("d-m-Y")?>" class="user_date" required="">
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2" style="display: none">
							<div class="form_block">
								<label>Entry Date <span class="valid">*</span></label>
								<input type="text" name="entryDate" value="<?php echo date("d-m-Y")?>" class="user_date" required="">
							</div>
						</div>                         
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer Name <span class="valid">*</span></label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerName" autocomplete="off" required="" id="customerName">								
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>					
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer ID </label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerId" autocomplete="off" required="" id="customerId">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Passenger Name <span class="valid">*</span></label>
								<div class="aj_rel_box  aj_passenger">
									<input class="pass_to_table" type="text" name="passengerName"  autocomplete="off" required="">
									<ul class="aj_live_items">

									</ul>
								</div>                           
							</div>
						</div>  
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Narration</label>
								<select name="narration">
									<option value="">Select</option>
									<?php 
									$nrrQry	=	mysql_query("SELECT * FROM ".TABLE_MISC_NARRATION." where (proId=$proId and finYear=$finYear) or proId='0'");
									if(mysql_num_rows($nrrQry))
									{
										while($nrrRow	=	mysql_fetch_array($nrrQry))
										{
											?>
											<option value="<?php echo $nrrRow['ID']; ?>"><?php echo $nrrRow['narration']; ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div> 
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Basic Amount <span class="valid">*</span></label>
								<input type="text" data-net="net_trigger" name="basicAmount" id="basicAmount" required="" onfocus="clearbox('basicAmountDiv')">
								<div class="valid" id="basicAmountDiv"></div>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>TDS</label>
								<input type="text" data-net="net_trigger" name="tds" value="0" id="tds" onfocus="clearbox('tdsDiv')">
								<div class="valid" id="tdsDiv"></div>
							</div>
						</div>						
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Other Charges</label>
								<input type="text" data-net="net_trigger" name="otherCharge" value="0" id="otherCharge" onfocus="clearbox('otherChargeDiv')">
								<div class="valid" id="otherChargeDiv"></div>

							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Processing Charges</label>
								<input type="text" data-net="net_trigger" name="procCharge" value="0" id="procCharge" onfocus="clearbox('procChargeDiv')">
								<div class="valid" id="procChargeDiv"></div>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Tax ('%' for percentage)</label>
								<input type="text" data-net="net_trigger" name="tax" value="0" id="tax" onfocus="clearbox('taxDiv')">
								<div class="valid" id="taxDiv"></div>
							</div>
						</div> 
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Discount</label>
								<input type="text" data-net="net_trigger" name="discount" value="0" id="discount" onfocus="clearbox('discountDiv')">
								<div class="valid" id="discountDiv"></div>
							</div>
						</div>
						
						
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Net amount <span class="valid">*</span></label>
								<input type="text" data-net="net_trigger" name="netAmount" required="" id="netAmount" onfocus="clearbox('netAmountDiv')" readonly="">
								<div class="valid" id="netAmountDiv"></div>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Description</label>
								<textarea name="description" ></textarea>
							</div>
						</div>       
					</div>
				</div>
				<div class="bd_panel_footer">
					<div class="panel_row">
						<div class="form_block_full">
							<input type="submit" name="savePurchase" value="SAVE & PURCHASE" onclick="return check()">
							<!--<input type="submit" name="save" value="SAVE">-->

						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
