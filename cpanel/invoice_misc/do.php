<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$finYear=$_SESSION['finYear'];
$loginId	=	$_SESSION['travelId'];
$loginType	=	$_SESSION['travelType'];
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
         
if($loginType=='Branch')
	{
		$loginName1		=	"SELECT ".TABLE_COMPANY.".companyName 
							   FROM ".TABLE_LOGIN." ,".TABLE_COMPANY." 						  
							  WHERE ".TABLE_COMPANY.".ID=".TABLE_LOGIN.".userId
							  	AND ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	$loginNameRow['companyName'];			  
	}
if($loginType=='Staff')
	{
		$loginName1		=	"SELECT ".TABLE_STAFF.".staffName, 
									".TABLE_COMPANY.".companyName
							   FROM ".TABLE_LOGIN." ,".TABLE_STAFF.",".TABLE_COMPANY." 						  
							  WHERE ".TABLE_STAFF.".ID=".TABLE_LOGIN.".userId
							  	AND ".TABLE_COMPANY.".ID=".TABLE_STAFF.".proId
							  	AND ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	/*$loginNameRow['companyName']."-".*/$loginNameRow['staffName'];			  
	}
	//echo $loginName;die;


$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['invoiceNo']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {        
            $success =0;            
			$newInvId			=	$db->nextVoucher(TABLE_INVOICE_MISC,'invoiceNo',"proId='$proId'");
			$accId ="SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Misc Receivable' ";
			$acc_record = $db->query_first($accId);
			$accountId=$acc_record['ID'];	
			
            $data['invoiceNo']		=	$newInvId;
            $customerId				=	$App->convert($_REQUEST['customerId']);
            $data['customerId']		=	$customerId;
            //$agentId				= 	$App->convert($_REQUEST['agentId']);           
			//$data['agentId']		=	$agentId;  
            if($_REQUEST['invoiceDate'])  
            {
				$data['invoiceDate']	=	$App->dbFormat_date($_REQUEST['invoiceDate']);
			}
			if($_REQUEST['entryDate']) 
			{
				$data['entryDate']		=	$App->dbFormat_date($_REQUEST['entryDate']);
			}
            $data['description']	=	$App->convert($_REQUEST['description']);
            $data['tds']			=	$tds2			=	$App->convert($_REQUEST['tds']);
            $data['otherCharge']	=	$otherCharge2	=	$App->convert($_REQUEST['otherCharge']);
            $data['discount']		=	$App->convert($_REQUEST['discount']);
            //$totAmt					=	$tds2+$otherCharge2;
            $mainTax				=	$App->convert($_REQUEST['tax']);
			$pos 					= 	strpos($mainTax, '%');
			if($pos==''){					 
				$data['tax']	=	$mainTax;               
			}	
            $data['netAmount']		=	$App->convert($_REQUEST['netAmount']);
            $data['status']			=	'Pending';
            //$data['narration']		=	$App->convert($_REQUEST['narration']);
            //$narration				=	$App->convert($_REQUEST['narration']);
            //$nId = $App->convert($_REQUEST['narration']);
            $data['proId']			=	$proId;
            $data['loginId']		=	$loginId;
            $data['finYear']		=	$finYear;

            $success = $db->query_insert(TABLE_INVOICE_MISC, $data);
            
            $k=1;
            $passName2 = '';
            $naTn = '';	
            
            for($k=1;$k<=$_POST['rowCount'];$k++){
            	//echo $_POST['rowCount'];die;
					   $passengerName	=	"passengerName_".$k;//echo $passengerName;
					   $narration		=	"narration_".$k;
					   $basicAmount		=	"basicAmount_".$k;
					   $procCharge		=	"procCharge_".$k;//echo $procCharge;
					   
					   $data2['miscId']		=	$success;
					   $data2['passengerName']	= $passengerName=	$App->capitalize($_REQUEST[$passengerName]);
					  //echo $passengerName;
					   	$data2['narration']			= $narration=	$App->convert($_REQUEST[ $narration]);
					   	//echo $narration;
					$basicAmount =	$data2['basicAmount']		=	$App->convert($_REQUEST[$basicAmount]);
						$procCharge = $data2['procCharge']		=	$App->convert($_REQUEST[$procCharge]);
						$data2['proId']			=	$proId;
                	    $data2['loginId']		=	$loginId;	                               
                	    $data2['finYear']		=	$finYear; 
                	    //print_r($data2);
					    $success2 = $db->query_insert(TABLE_MISC_PASSENGER, $data2);
					    //print_r($success2);
					    $passName2 = $passName2.$passengerName.",";
					    
					    $narraQuery = "SELECT narration from ".TABLE_MISC_NARRATION." where
					    			  ".TABLE_MISC_NARRATION.".ID='$narration'";
					    $narra_record = $db->query_first($narraQuery);
						$naraName=$narra_record['narration'];
						
					    $naTn	=	$naTn.$naraName.",";
					    
					   $eachTotal=$basicAmount+$procCharge;
					   $eachPassTotal+=$eachTotal;
			}
            //echo $eachPassTotal;die;
            
            //tax calculation()		              
		$totAmt			=	$eachPassTotal+$tds2+$otherCharge2;
		if(strpos ($mainTax,'%')){
			$taxTrim			=	rtrim($mainTax, "%");							
			$mainTax			=	$totAmt*($taxTrim/100);
			$data4['tax'] 	=  	$mainTax;
			$db->query_update(TABLE_INVOICE_MISC, $data4 , "ID=$success" );
		}
            
			  /*-------------------------------------------------------------------------*/
			 //To transaction table
		         															
					/* ------------------------------------------ */
					
					$passName3 	= chop($passName2,",");
					$naTn1		= chop($naTn,",");//echo $naTn1;die;
					
					
					$nameParts = explode(',',$passName3);
					$noNaration		=	explode(',',$naTn1);
					
					$passString='';
					for($j=0;$j<$_POST['rowCount'];$j++)
					{
						$passString = $passString."Pax :".$nameParts[$j]
.", Narration :".$noNaration[$j]."/";					
					}

					//echo $passString;die;
					
					$invoiceDetails	=	"INV#MI$newInvId,$passString By:$loginName ";
					//echo $invoiceDetails;die;
					
					$tsdata['fromLedger'] 		=  	$customerId;
					$tsdata['toLedger'] 		=  	$accountId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Misc Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Misc Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$accountId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Misc Receivable';
					$tfdata['credit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Misc Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  /*----------------------------------------------------------------------------*/
		  
		         
		          	$discount		=	$App->convert($_REQUEST['discount']);
		          	$tds			=	$App->convert($_REQUEST['tds']);
		          	
		           //discount		         
		          if($discount>0)
		          {
		          	
		             $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given'";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$discountId;
					$tsdata['toLedger'] 		=  	$customerId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Misc Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['discount']));
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Misc Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$customerId;
					$tfdata['toLedger'] 		=  	$discountId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Misc Receivable';
					$tfdata['credit']			=   abs($App->convert($_REQUEST['discount']));
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Misc Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				   //tds
				   if($tds>0)
		          {
		          	
		          
		             $tdsQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tds' ";
					 $tds_record 		= 	$db->query_first($tdsQuery);
					 $tdsId				=	$tds_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=   $customerId;
					$tsdata['toLedger'] 		=  	$tdsId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Misc Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_POST['tds']));
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Misc Receivable: '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$tdsId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Misc Receivable';
					$tfdata['credit']			=   abs($App->convert($_POST['tds']));
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Misc Receivable: '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tax
				  if($mainTax>0)
		          {
		          	 $taxQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tax' ";
					 $tax_record 		= 	$db->query_first($taxQuery);
					 $taxId				=	$tax_record['ID'];
		          
				  	$tsdata['fromLedger'] 		=   $customerId;
					$tsdata['toLedger'] 		=  	$taxId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Misc Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($mainTax);
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Misc Receivable: '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$taxId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Misc Receivable';
					$tfdata['credit']			=   abs($mainTax);
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Misc Receivable: '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
									  
				  }
		          
		          /*----------------------------------------------------------------------------*/   
		             $db->close();
		            
	       if($success){
			$_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");  
		} 
		else{
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
		}      
		@$sp	=	$_REQUEST['savePurchase'];
		@$s		=	$_REQUEST['save'];
			
		if($sp){
			header("location:../purchase/add.php?type=m&pId=$success");
		}
		else if($s){
			header("location:index.php");
		}

	}
	break;
    // EDIT SECTION
    case 'edit':
    	$editId = $_REQUEST['editId'];//echo $editId;die;
    	if (!$_REQUEST['invoiceNo']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success =0;
            
            $existInvId	=	$db->existVoucherNew(TABLE_INVOICE_MISC,'invoiceNo'," ID='$editId' and proId=$proId");
			 //echo $existInvId;die;						 
			 //for get the current amount in invoice airline for inserting to log
			$oldAmount				=	$db->query_first("SELECT netAmount FROM ".TABLE_INVOICE_MISC." WHERE ID='$editId' and proId=$proId");
			
			@mysql_query("DELETE FROM `" . TABLE_MISC_PASSENGER . "` WHERE miscId='{$editId}' and proId=$proId and finYear=$finYear"); 
			//echo "DELETE FROM `" . TABLE_MISC_PASSENGER . "` WHERE miscId='{$editId}' and proId=$proId and finYear=$finYear";die;
			 @mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$existInvId}' and voucherType='Misc Receivable' and proId=$proId and finYear=$finYear");	
			$accId ="SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Misc Receivable' ";
			$acc_record = $db->query_first($accId);
			$accountId=$acc_record['ID'];
				
			$newInvId				=	$existInvId; 
            $data['invoiceNo']		=	$newInvId;
            $customerId				=	$App->convert($_REQUEST['customerId']);
            $data['customerId']		=	$customerId;
            //$agentId				= 	$App->convert($_REQUEST['agentId']);           
			//$data['agentId']		=	$agentId;  
            if($_REQUEST['invoiceDate'])  
            {
				$data['invoiceDate']	=	$App->dbFormat_date($_REQUEST['invoiceDate']);
			}
			if($_REQUEST['entryDate']) 
			{
				$data['entryDate']		=	$App->dbFormat_date($_REQUEST['entryDate']);
			}
            $data['description']	=	$App->convert($_REQUEST['description']);
            
            $data['tds']			=	$tds2			=	$App->convert($_REQUEST['tds']);            
            $data['otherCharge']	=	$otherCharge2	=	$App->convert($_REQUEST['otherCharge']);
            $data['discount']		=	$App->convert($_REQUEST['discount']);
            //$totAmt					=	$tds2+$otherCharge2;
            $mainTax				=	$App->convert($_REQUEST['tax']);
			$pos 					= 	strpos($mainTax, '%');
			if($pos==''){					 
				$data['tax']	=	$mainTax;               
			}
            $data['netAmount']		=	$App->convert($_REQUEST['netAmount']);
            $data['status']			=	'Pending';
            //$data['narration']		=	$App->convert($_REQUEST['narration']);
            //$narration				=	$App->convert($_REQUEST['narration']);
            //$nId = $App->convert($_REQUEST['narration']);
            $data['proId']			=	$proId;
            $data['loginId']		=	$loginId;
            $data['finYear']		=	$finYear;
//print_r($data);die;
             $success = $db->query_update(TABLE_INVOICE_MISC, $data,"ID=$editId");
           
             
            $k=1;
            $passName2 = '';
            $naTn = '';	
            
            for($k=1;$k<=$_POST['rowCount'];$k++){
            	//echo $_POST['rowCount'];die;
					   $passengerName	=	"passengerName_".$k;//echo $passengerName;
					   $narration		=	"narration_".$k;//echo $narration;
					   $basicAmount		=	"basicAmount_".$k;
					   $procCharge		=	"procCharge_".$k;//echo $procCharge;
					   
					   $data3['miscId']		=	$editId;
					   $data3['passengerName']	= $passengerName=	$App->capitalize($_REQUEST[$passengerName]);
					  //echo $passengerName;
					   	$data3['narration']			= $narration=	$App->convert(@$_REQUEST[ $narration]);
					   	
						$basicAmount= $data3['basicAmount']		=	$App->convert($_REQUEST[$basicAmount]);
						$procCharge=$data3['procCharge']		=	$App->convert($_REQUEST[$procCharge]);
						$data3['proId']			=	$proId;
                	    $data3['loginId']		=	$loginId;	                               
                	    $data3['finYear']		=	$finYear; 
                	    //print_r($data3);
					    $success3 = $db->query_insert(TABLE_MISC_PASSENGER, $data3);
					    //print_r($success2);
					    $passName2 = $passName2.$passengerName.",";
					    
					    $narraQuery = "SELECT narration from ".TABLE_MISC_NARRATION." where
					    			  ".TABLE_MISC_NARRATION.".ID='$narration'";
					    $narra_record = $db->query_first($narraQuery);
						$naraName=$narra_record['narration'];
						
					    $naTn	=	$naTn.$naraName.",";
					    
					    
					    $eachTotal=$basicAmount+$procCharge;
					   	$eachPassTotal+=$eachTotal;
					   
			}
           
           
           //tax calculation()		              
		$totAmt			=	$eachPassTotal+$tds2+$otherCharge2;
		if(strpos ($mainTax,'%')){
			$taxTrim			=	rtrim($mainTax, "%");							
			$mainTax			=	$totAmt*($taxTrim/100);
			$data4['tax'] 	=  	$mainTax;
			$db->query_update(TABLE_INVOICE_MISC, $data4 , "ID=$success" );
		}
           
           
			  	/*-------------------------------------------------------------------------*/
			 	//To transaction table
		         															
				/* ------------------------------------------ */
					
					$passName3 	= chop($passName2,",");
					$naTn1		= chop($naTn,",");//echo $naTn1;die;
					
					
					$nameParts = explode(',',$passName3);
					$noNaration		=	explode(',',$naTn1);
					
					$passString='';
					for($j=0;$j<$_POST['rowCount'];$j++)
					{
						$passString = $passString."Pax :".$nameParts[$j]
.", Narration :".$noNaration[$j]."/";					
					}

					//echo $passString;die;
					
					$invoiceDetails	=	"INV#MI$newInvId,$passString By:$loginName ";
					//echo $invoiceDetails;die;
					
					$tsdata['fromLedger'] 		=  	$customerId;
					$tsdata['toLedger'] 		=  	$accountId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Misc Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Misc Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$accountId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Misc Receivable';
					$tfdata['credit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Misc Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  /*----------------------------------------------------------------------------*/
		  
		         
		          	$discount		=	$App->convert($_REQUEST['discount']);
		          	$tds			=	$App->convert($_REQUEST['tds']);
		          	
		           //discount		         
		          if($discount>0)
		          {
		          	
		             $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given'";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$discountId;
					$tsdata['toLedger'] 		=  	$customerId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Misc Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['discount']));
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Misc Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$customerId;
					$tfdata['toLedger'] 		=  	$discountId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Misc Receivable';
					$tfdata['credit']			=   abs($App->convert($_REQUEST['discount']));
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Misc Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				   //tds
				   if($tds>0)
		          {
		          	
		          
		             $tdsQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tds' ";
					 $tds_record 		= 	$db->query_first($tdsQuery);
					 $tdsId				=	$tds_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=   $customerId;
					$tsdata['toLedger'] 		=  	$tdsId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Misc Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_POST['tds']));
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Misc Receivable: '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$tdsId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Misc Receivable';
					$tfdata['credit']			=   abs($App->convert($_POST['tds']));
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Misc Receivable: '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tax
				  if($mainTax>0)
		          {
		          	 $taxQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tax' ";
					 $tax_record 		= 	$db->query_first($taxQuery);
					 $taxId				=	$tax_record['ID'];
		          
				  	$tsdata['fromLedger'] 		=   $customerId;
					$tsdata['toLedger'] 		=  	$taxId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Misc Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($mainTax);
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Misc Receivable: '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$taxId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Misc Receivable';
					$tfdata['credit']			=   abs($mainTax);
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Misc Receivable: '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
									  
				  }
		          
		          /*----------------------------------------------------------------------------*/   
		          
		          /*-------------------------------- Inserting to Log --------------------------*/ 
				$description 			=	"NetAmt:".$App->convert($_POST['netAmount']).",Discount:".$App->convert($_POST['discount']).",Tds:".$App->convert($_POST['tds']).",MainTax:".$mainTax;
				
				$datalog['module']		=	'Invoice Miscellaneous';
				$datalog['tableId']		=	$editId;
				$datalog['invoiceNo']	=	$newInvId;
				$datalog['changeDate']	=	date("Y-m-d");
				$datalog['Status']		=	'Edit';
				$datalog['oldAmt']		=	$oldAmount['netAmount'];
				$datalog['newAmt']		=	$_REQUEST['netAmount'];			
				$datalog['description']	=	$description;
				$datalog['loginId']		=	$loginId;
				$datalog['proId']		=	$proId;
				$datalog['finYear']		=	$finYear;
				
				$db->query_insert(TABLE_LOG, $datalog);
				/*-------------------------------- Inserting to Log Ends --------------------------*/ 
				
		    $db->close();
			if($success){
			$_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");  
		} 
		else{
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to updated details. Please try again.");                   
		}
		@$sp	=	$_REQUEST['savePurchase'];
		@$s		=	$_REQUEST['save'];			
		if($sp){
			header("location:../purchase/edit.php?type=m&pId=$editId");
		}
		else if($s){
			header("location:index.php");
		}
	}        
	break;
   // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success1 = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
		   $existInvId	=	$db->existVoucherNew(TABLE_INVOICE_MISC,'invoiceNo'," ID='$deleteId' and proId=$proId");
		   	//for get the current amount in invoice airline for inserting to log
			$oldAmount	=	$db->query_first("SELECT netAmount,invoiceNo FROM ".TABLE_INVOICE_MISC." WHERE ID='$deleteId' and proId=$proId");
			$description = "InvoiceAmt:".$oldAmount['netAmount'];
           $success1=mysql_query("DELETE FROM `" . TABLE_INVOICE_MISC . "` WHERE ID='{$deleteId}' and proId=$proId and finYear=$finYear");
           
           @mysql_query("DELETE FROM `" . TABLE_MISC_PASSENGER . "` WHERE miscId='{$deleteId}' and proId=$proId and finYear=$finYear");
			 
			 @mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$existInvId}' and voucherType='Misc Receivable' and proId=$proId and finYear=$finYear");	
			
			//purchase
			@$purExe	=	@mysql_query("SELECT invoiceNo,netAmount FROM `" . TABLE_PURCHASE_MISC . "` WHERE ourInvoiceNo='{$existInvId}' and proId=$proId and finYear=$finYear");
			@$purNum		=	mysql_num_rows($purExe);
			if($purNum>0)
			{
				$purRes			=	mysql_fetch_array($purExe);
				$purInvoiceNo	=	$purRes['invoiceNo'];
				$description = $description.",PurchaseVoucherNo:".$purRes['invoiceNo'].",PurchaseAmt:".$purRes['netAmount'];
				@mysql_query("DELETE FROM `" . TABLE_PURCHASE_MISC . "` WHERE ourInvoiceNo='{$existInvId}' and proId=$proId and finYear=$finYear");
				@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$purInvoiceNo}' and voucherType='Misc Payable' and proId=$proId and finYear=$finYear");
			}
			
			//payment
			$payAmt = 0;
			@$payExe	=	@mysql_query("SELECT ID,voucherNo,payingAmount FROM `" . TABLE_CASH_PAYMENT . "` WHERE invoiceNo='{$purInvoiceNo}' and voucherType='miscellaneous' and proId=$proId and finYear=$finYear");
			@$payNum		=	mysql_num_rows($payExe);
			if($payNum>0)
			{
				while($payRes	=	mysql_fetch_array($payExe))
				{
					$payId	=	$payRes['ID'];
					$payVoucherNo	=	$payRes['voucherNo'];
					$payAmt = $payAmt+$payRes['payingAmount'];
					@mysql_query("DELETE FROM `" . TABLE_CASH_PAYMENT . "` WHERE ID='{$payId}' and proId=$proId and finYear=$finYear");
					@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$payVoucherNo}' and (voucherType='Cash Payments' || voucherType='Bank Payments') and proId=$proId and finYear=$finYear");
				}
			}
			$description = $description.",PaidAmt:".$payAmt;
			
			//receipt
			$recAmt	=	0;
			@$resExe	=	@mysql_query("SELECT ID,voucherNo,payingAmount FROM `" . TABLE_CASH_RECEIPT . "` WHERE invoiceNo='{$existInvId}' and voucherType='miscellaneous' and proId=$proId and finYear=$finYear");
			@$resNum		=	mysql_num_rows($resExe);
			if($resNum>0)
			{
				while($ReceiptRes	=	mysql_fetch_array($resExe))
				{
					$resId			=	$ReceiptRes['ID'];
					$resVoucherNo	=	$ReceiptRes['voucherNo'];
					$recAmt = $recAmt+$ReceiptRes['payingAmount'];
					@mysql_query("DELETE FROM `" . TABLE_CASH_RECEIPT . "` WHERE ID='{$resId}' and proId=$proId and finYear=$finYear");
					@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$resVoucherNo}' and (voucherType='Cash Receipts' || voucherType='Bank Receipts') and proId=$proId and finYear=$finYear");
				}
			}
			$description = $description.",ReceivedAmt:".$recAmt;
			
			/*-------------------------------- Inserting to Log --------------------------*/ 
			$datalog['module']		=	'Invoice Miscellaneous';
			$datalog['tableId']		=	$deleteId;
			$datalog['invoiceNo']	=	$oldAmount['invoiceNo'];
			$datalog['changeDate']	=	date("Y-m-d");
			$datalog['Status']		=	'Delete';
			$datalog['oldAmt']		=	$oldAmount['netAmount'];
			$datalog['newAmt']		=	0;			
			$datalog['description']	=	$description;
			$datalog['loginId']		=	$loginId;
			$datalog['proId']		=	$proId;
			$datalog['finYear']		=	$finYear;
			
			$db->query_insert(TABLE_LOG, $datalog);	
			/*-------------------------------- Inserting to Log -------------------------*/
        $db->close();
        if ($success1) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
        
         // Printing  ========================================================================
		case 'print':
				$printID	=	$_REQUEST['id'];
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
					
				$userId		=	$_SESSION['proId'];
				$sql2 		= 	"SELECT * FROM `".TABLE_COMPANY."` where ID=$userId";
				$vou_record1 = 	$db->query_first($sql2);
				$company 	= 	$vou_record1['companyName'];
				$caddress 	= 	$vou_record1['address'];
				$cphone		=	$vou_record1['phone'];
				$cmobile	=	$vou_record1['mobile'];	
				$cemail		=	$vou_record1['email'];
				$cpin		=	$vou_record1['pin'];
				
				// set default header data
				/*$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));*/

				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print
				
				$fid	=	$_REQUEST['id'];								
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
				$sql2 = "SELECT a.ID,a.invoiceNo,a.invoiceDate,a.tds,a.tax,
								a.otherCharge,a.discount,a.netAmount,
								b.accountName,b.address,b.mobile,b.phone,b.pin
						   FROM ".TABLE_INVOICE_MISC." as a
					  LEFT JOIN	".TABLE_ACCOUNTS." as b ON b.ID = a.customerId	
					  	  WHERE a.ID=$printID and a.proId=$proId				
						";
						
				$transQry		=	mysql_query($sql2);
				$vou_record 	= 	mysql_fetch_array($transQry);
				$passNum		=	mysql_num_rows($transQry);
				$miscID			=   $vou_record['ID'];				
				$invoiceNo 		= 	$vou_record['invoiceNo'];
				$invoiceDate 	= 	$vou_record['invoiceDate'];
				//$basicAmount	= 	$vou_record['basicAmount'];
				$tds 			= 	$vou_record['tds'];
				$tax 			= 	$vou_record['tax'];
				$otherCharge 	= 	$vou_record['otherCharge'];
                                //$processingCharge	= $vou_record['procCharge'];    
				$discount 		= 	$vou_record['discount'];
				$netAmount 		= 	$vou_record['netAmount'];
				
				$accountName 	= 	$vou_record['accountName'];
				$address	    = 	$vou_record['address'];
				$mobile 		= 	$vou_record['mobile'];
				$phone 			= 	$vou_record['phone'];
				$pin 			= 	$vou_record['pin'];
				
				//$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
				//$defRow	=	8;
				
				$proCharge		=	$vou_record['otherCharge']+$vou_record['tds'];				
				$mainTax		=	$vou_record['tax'];
				if(strpos ($mainTax,'%'))
				{
					$tax1		=	substr($mainTax,'%');
					$tax2		=	$tax1[0];
					$mainTax	=	$proCharge*($mainTax/100);
				}
				
				if($mainTax)
				{
					
					$mainProcCharge	=	($mainTax+$proCharge)-$vou_record['discount'];
				}
				else
				{
					$mainProcCharge	=	$proCharge-$vou_record['discount'];
				}

                               // $totalAmount	=	$mainProcCharge;
    
                                $purchaseQuery		=	"SELECT DISTINCT details FROM ".TABLE_TRANSACTION." WHERE voucherNo = '$invoiceNo' AND voucherType='Misc Receivable' AND proId=$proId  and finYear=$finYear";
					$resultQuery	=	$db->query_first($purchaseQuery);
					$transDetails	=	$resultQuery['details'];

				
				//basic info
				$tbl_header1 = '<div><table style="width: 530px;" cellspacing="0" border="0" >';
				$tbl_footer1 = '</table></div>';
				$tbl1 = '';
				
				$tbl1	=	'<tr>
								<td colspan="6" align="left" ><h2>'.strtoupper($company).'</h2></td>
							 </tr>
							 <tr>
								<td colspan="6"></td>
							 </tr>
							 <tr>
							 	<td colspan="3" align="left">'.$caddress.'<br>'.$cpin.'</td>
							 	<td colspan="3" align="left">'.$cphone.'<br>'.$cmobile.'<br>'.$cemail.'</td>
							 </tr>
							 <tr>
								<td colspan="6" ></td>
							 </tr>
							 <tr>
							 	<td colspan="3" style="border-top: 0.1px solid black; border-bottom: 0.1px solid black;">To , <strong>'.$accountName.'</strong></td>
							 	
							 	<td colspan="3" style="border-top: 0.1px solid black;border-bottom: 0.1px solid black;background-color:#F2F2F2; border-left: 0.1px solid black;" align="center">Miscellaneous Invoice (MI)</td>
							 	
							 </tr>
							 
							 <tr>
							 	<td colspan="3"  style="border-bottom: 0.1px solid black;" align="left">'.$address.'<br>'.$pin.'</td>
							 	<td colspan="3" style="border-left: 0.1px solid black; border-bottom: 0.1px solid black;" align="left">
								 	<table >
									 	<tr>
									 		<td style="width:60px;" >Invoice :</td>
									 		<td style="width: 60px;" ><strong>'.$invoiceNo.'</strong></td><td style="width:60px;"><strong>'.$App->dbFormat_date($invoiceDate).'</strong></td>
									 	</tr>
									 	<tr>
									 		<td >Booked By :</td><td colspan="2"></td>
									 	</tr>
									 	<tr>
									 		<td >PNR :</td><td colspan="2">0</td>
									 	</tr>
								 	</table>
							 	</td>
							 	
							 </tr>
							';
				
				$pdf->SetFontSize(9);
				$pdf->writeHTML($tbl_header1 . $tbl1 . $tbl_footer1, true, true, false, false, '');
				
				//passenger TABLE
			
				$tbl_header = '<div><table style="width: 638px; " cellspacing="0"    >';
				$tbl_footer = '</table></div>';
				$tbl = '';

				$tbl .= '<tr style="background-color:#F2F2F2">
					        <td style="width: 100px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black"><strong>Sr</strong></td>
					        <td style="width: 200px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black"><strong>Details</strong></td>
					        <td style="width: 230px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black"><strong>Net Amount</strong></td>
					        
				    	</tr>
				    	<tr>
				    		<td colspan="3"></td>
				    	</tr>';
				$passQry	=	 " SELECT d.ID,d.basicAmount,d.procCharge,d.miscId
								   FROM ".TABLE_MISC_PASSENGER." as d 
								   WHERE	d.miscId=$miscID and d.proId=$proId and d.finYear=$finYear
								";//echo $passQry;
				$totBamount=0;	
				$totPcharge =0;			
				$pass		=	mysql_query($passQry);
				$passNum	=	mysql_num_rows($pass);
				$defRow		=	8;	//for showing constant height for the table
				$i=0;
				if($passNum>0)
				{
					while($passRow=mysql_fetch_array($pass))
					{
						$basicCharge = $passRow['basicAmount'];
						$procCharge		= $passRow['procCharge'];
						$totBamount=$totBamount+$basicCharge;
						$totPcharge=$totPcharge+ $procCharge;
					}
					
				} 
				  	
				$totalCharge=$mainProcCharge+$totBamount+$totPcharge;
				
				    	$tbl.= '<tr>
					        <td>1</td>
					        <td>'.$transDetails.'</td>
					        <td>'.$totalCharge.'</td>
					        
					    </tr>
				    	';

				if($passNum<$defRow)
				{
					$staticRow	=	$defRow-$passNum;
					for($i=0;$i<$staticRow;$i++)
					{
						$tbl.='<tr>
						       		<td colspan="3"></td>
						       </tr>';
					}
				}
				$tbl.= '<tr><td colspan="3" ></td></tr>
						<tr>
							<td style="border-top: 0.1px solid black; border-right: 0.1px solid black"></td>
				    		<td align="left" style="border-top: 0.1px solid black;" align="left"></td>
				    		<td align="right" style="border-top: 0.1px solid black; "></td>
				    	</tr>
				    	
				    	<tr>
				    		<td style="border-right: 0.1px solid black;">hiii</td>
				    		<td align="left" >Net Invoice Amount</td>
				    		<td align="right">'.sprintf("%0.2f",round($netAmount,2)).'</td>
				    	</tr>
				    	<tr><td colspan="3" style=" font:courier; border-top: 0.1px solid black">'.ucfirst($App->amountToWord(round($netAmount,2))).' </td></tr>
				    	';
		
				$pdf->SetFontSize(9);
				$pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, true, false, false, '');
				
				
				//terms and conditions
				$tbl_header2 = '<div><table style="width: 530px;" cellspacing="0" border="0" >';
				$tbl_footer2 = '</table></div>';
				$tbl2 = '';
				
				$tbl2	=	'<tr>
								<td  colspan="3" style="border-top: 0.1px solid black" >Terms :</td>
								<td colspan="2" style="border-top: 0.1px solid black" align="right">E.  & O.  E.</td>
								<td style="border-right: 0.1px solid black;border-top: 0.1px solid black"></td>
								<td colspan="1" style="border-top: 0.1px solid black"></td>
								<td colspan="3" style="border-top: 0.1px solid black" align="right" >for '.strtoupper($company).'</td>
							</tr>
							<tr>
								<td colspan="6" style="border-right: 0.1px solid black">
								# Subject to KOZHIKODE jurisdiction.<br>
								# Without original invoice no refund is permissible.<br>
								# Interest @ 24% will be charged on delayed payment<br>
								# Cheque to be drawn in our company name on presentation of invoice.<br>
								# Kindly check all details carefully to avoid un-necessary complications.<br>
									
								</td>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="4">Ticket Received in Order.</td>
								<td colspan="2" style="border-right: 0.1px solid black" align="right">Receivers Signature</td>
								
								<td colspan="4" align="right">Authorized Signatory</td>
							</tr>
							<tr>
								<td colspan="6" style="border-right: 0.1px solid black; border-bottom: 0.1px solid black">
								</td>
								<td colspan="4" style=" border-bottom: 0.1px solid black"></td>
							</tr>
							
							<tr>
								<td colspan="10" align="center">This is a Computer generated document and does not require any signature.
								</td>
							</tr>
							';
				$pdf->SetFontSize(7);
				$pdf->writeHTML($tbl_header2 . $tbl2 . $tbl_footer2, true, true, false, false, '');

				//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
				
				// ---------------------------------------------------------
				
				// Close and output PDF document
				$pdf->Output("'Invoice Miscellaneous'.$invoiceNo.pdf", 'I');		
		break;
			//Upload doc
		case 'upload':				
			    	
		if(!$_REQUEST['invoiceDocId'])
			{				
				$_SESSION['msg'] = $App->sessionMsgCreate('error', "Error, Invalid Details!"); 
				header("location:index.php");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=$document=0;
					
					$invoiceDocId	=	$App->convert($_REQUEST['invoiceDocId']);	
								
				
				// upload file
				$res=mysql_query("select document from ".TABLE_INVOICE_MISC." where ID='{$invoiceDocId}'");
				$row=mysql_fetch_array($res);
				$document=$row['document'];					
								
				$date		= 	date("YmdHis");
				$path_info  = 	pathinfo($_FILES["invDoc"]["name"]);
				$ext	 	=	$path_info["extension"];	
				//$allowed 	=  	array('gif','png','jpg');		// allowed image extensions					
				$newName 	= 	$date.".".$ext;
/*
				if(in_array($ext,$allowed) ) 	
				{*/
					$img='';	
					if(move_uploaded_file($_FILES["invDoc"]["tmp_name"],"document/".basename($newName)))
					{
						$doc="document/" . $newName;		
					}
					$data['document']			=	$doc;									
					$success1=$db->query_update(TABLE_INVOICE_MISC,$data," ID='{$invoiceDocId}'");					if($document)					
					{										
						if (file_exists($document)) 	
						{
						unlink($document);					
						} 						
					}					
					$db->close();								
				// }
				
				if($success1)
				{	
					$_SESSION['msg'] = $App->sessionMsgCreate('success', "Document uploaded Successfully"); 						
				}
				else
				{
					/*if(!in_array($ext,$allowed) ) 
					{
						$_SESSION['msg']="You have uploaded an invalid image file";
					}
					else
					{*/	
						$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed"); 
					// }
					
				}
				header("location:index.php");
			}
																					
			break;
}
?>