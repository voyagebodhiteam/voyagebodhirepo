<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

//for edit and delete permission
$logID = $_SESSION['travelId'];

if($_SESSION['travelType']=='Admin')
{
	$check	=	"a.finYear='$finYear'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"a.proId='$proId' and a.finYear='$finYear'";
}

$type = $_SESSION['travelType'];

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
/////**************************** ALL DELETE FUNCTIONS ****************///

// warning if the invoice is going to delete
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
// warning if the invoice purchased then deleting
function purchase_delete(k)
{
if(k) {
		var del=confirm("This invoice is Purchased. Do you want to delete ?");
		
	}
	
	return del;
}
//warning if the invoice's payment is over then going to delete
function payment_delete(l)
{
if(l) {
		var del=alert("Sorry.! This invoice's Payment is over. You cannot delete !");
		
	}
	
	return false;
}
// warning if the invoice's receipt is over then going to delete
function receipt_delete(m)
{
if(m) {
		var del=alert("Sorry .! This invoice's receipt is over. You cannot delete !");
		
	}
	
	return false;
}
/////**************************** ALL EDIT FUNCTIONS ****************///

//for warning if the invoice's payment is over while edit
function pay(x)
{
	if(x) {
		var edit=confirm("This invoice's Payment is over. Do you want to edit ?");		
	}
	return edit;	
}
//for warning edit if the invoice receipt is going to edit
function receipt(z)
{
	if(z) {
		var edit=confirm("This invoice's receipt is over. Do you want to edit ?");		
	}
	return edit;	
}
//for warning edit if the invoice is purchased.
function purchase(y)
{

	if(y) {
		var edit=confirm("This invoice is Purchased. Do you want to edit ?");		
	}	
	return edit;	
}


//for incurrect invoiceNo search
function count()
{
var invoiceNo = document.getElementById('invoiceNo').value;
//alert(invoiceNo);
if((invoiceNo.length < 3) && (invoiceNo!='') ){
	document.getElementById('check').innerHTML= "Enter Current Invoice No";
	return false;
		}
}
</script>
<script>
function fnExcelReport()
    {
	


             var tab_text="<table border='2px'><tr><td colspan='7' style='text-align:center; font-size:11px;'><h3>INVOICE MISCELLANEOUS</h3></td> </tr>";
             var textRange; var j=0;
             tab = document.getElementById('headerTable'); // id of table1	
		   // id of table1	
		   //alert (tab.rows.length);
		   	 var number = document.getElementById('number').value;
			//alert(number);
		 
try
{
		tab_text=tab_text+"<tr><td bgcolor='#87AFC6'>S.No</td><td bgcolor='#87AFC6'>User Name</td><td bgcolor='#87AFC6'>Voucher No</td><td bgcolor='#87AFC6'>Voucher Date</td><td bgcolor='#87AFC6'>Customer Name</td><td bgcolor='#87AFC6'>Amount</td></tr>";

x=0;
 if(number == 0)
	   {
	   		tab_text=tab_text+"<tr><td  colspan='7' align='center'>There are no Reports in list.</td></tr>";
	   }
	else
	  {
           for(j = 1 ; j < tab.rows.length ; j++) 
          {     
			
			 tab_text = tab_text + "<tr>";
			 dd=0;
		  	 for(k = 0; k < tab.rows[j].cells.length ; k++ ) 
			 {			 	
                tab_text=tab_text+"<td>"+tab.rows[j].cells[k].innerHTML+"</td>";
						dd=1;
						
			 }
			 
			
				tab_text = tab_text + "</tr>";
				
			
          }
	  }     

	  
}
catch(err)
{
 alert(err);
}
          tab_text=tab_text+"</table>";
		  tab_text= tab_text.replace("<tr></tr>", "");
		  tab_text= tab_text.replace("<tr>  </tr>", "");
	
          tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
          tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
          tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

               var ua = window.navigator.userAgent;
              var msie = ua.indexOf("MSIE "); 

                 if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                    {
                           txtArea1.document.open("txt/html","replace");
                           txtArea1.document.write(tab_text);						   
                           txtArea1.document.close();
                           txtArea1.focus(); 
                           sa=txtArea1.document.execCommand("SaveAs",true,"");
                     }  
                  else                 //other browser not tested on IE 11
                      sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  


                      return (sa);



	}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>MISCELLANEOUS - INVOICE</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post" onsubmit="return count()"><!-- count()-> for alert message -->
                        <div class="page_search_row clearfix">
                        	<div class="page_search">
	                        	<input type="text" name="invoiceNo" id="invoiceNo" placeholder="Voucher No" value="<?php echo @$_REQUEST['invoiceNo']; ?>">
                        	</div>
                        	<div class="page_search">
	                        	<input type="text" name="cusName" id="cusName" placeholder="Customer Name" value="<?php echo @$_REQUEST['cusName']; ?>">
                        	</div>
                        </div>
                        
                        <div class="page_search_row clearfix" style="margin-top: 10px;">
                            <div class="page_search"> 	
	                            <input type="search" name="fDate" placeholder="From Date"  class="user_date" value="<?php echo @$_REQUEST['fromDate']; ?>">
                            </div>
                            <div class="page_search"> 
	                            <input type="search" name="tDate" placeholder="To Date"  class="user_date" value="<?php echo @$_REQUEST['toDate']; ?>">
                            </div>
                            <div class="page_search"> 
	                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                            </div>
                            <div class="page_nav_menu" style="margin-left:10px;">
		                        <ul class="page_nav">
		                            <li>
		                                <a href="add.php">
		                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
		                                    Add New
		                                </a>
		                            </li>
		                        </ul>
	                    	</div>
	                    	<div class="page_search">
	                    		<button onclick="fnExcelReport();"  class="btn btn-default export" title="Export To Excel"></button>
                      
                    		</div>
                            <div class="valid" id="check"></div><!-- id for message  -->
                        </div>
                        </form>
                    </div>
                    
                    
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    
<?php	
$cond="1";
if(@$_REQUEST['invoiceNo'])
{
	//deleting the first two character from a string
	$str = $_POST['invoiceNo'];
	$str2 = substr($str, 2);
	$cond=$cond." and a.invoiceNo like'".$str2."'";
}
if(@$_REQUEST['fDate'] && @$_REQUEST['tDate'])
{
	$cond=$cond." and a.invoiceDate BETWEEN '".$App->dbFormat_date($_POST['fDate'])."' AND '".$App->dbFormat_date($_REQUEST['tDate'])."'";
}
else if(@$_REQUEST['fDate'])
{
	$cond	 =	$cond." and a.invoiceDate='".$App->dbFormat_date($_REQUEST['fDate'])."'";
}
else if(@$_REQUEST['tDate'])
{
	$cond	 =	$cond." and a.invoiceDate='".$App->dbFormat_date($_REQUEST['tDate'])."'";
}
if(@$_REQUEST['cusName'])
{
	$cond=$cond." and b.accountName like '%".$App->convert($_REQUEST['cusName'])."%'";
}

?>
    
<?php
                     $i=0;
                        $selectAll = "select a.ID,
                        					 a.invoiceNo,
                        					 a.invoiceDate,
                        					 a.loginId,
                        					 a.document,
                        					 b.accountName,
                        					 a.netAmount,
                        					 a.status,
                        					 a.proId
			                          from ".TABLE_INVOICE_MISC." a,
			                          	   ".TABLE_ACCOUNTS." b
			                          where a.customerId=b.ID
			                          and $cond
			                          and $check
			                          order by a.ID desc";
                        $result = $db->query($selectAll);
                        $selectCount = mysql_num_rows($result); ?>    
    
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
            <input type="hidden" name="number" id="number" value="<?php echo @$selectCount; ?>" /> 
                <table class="table table-bordered table-striped show_table" id="headerTable">
                    <thead>
                         <tr>
                            <th>Sl No</th>
                            <th>User Name</th>
                            <th>Voucher No</th>
                            <th>Voucher Date</th>
                            <th>Customer Name</th>
                            <th>Amount</th>                     
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                        
                        if($selectCount==0)
						{
						?>
							 <tr>
								<td align="center" colspan="7">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
                        /*********************** for pagination ******************************/
						$rowsPerPage = ROWS_PER_PAGE;
						if(isset($_GET['page']))
						{
							$pageNum = $_GET['page'];
						}
						else
						{
							$pageNum =1;
						}
						$offset = ($pageNum - 1) * $rowsPerPage;
						$select1=$db->query($selectAll." limit $offset, $rowsPerPage");
						$i=$offset+1;
						//use '$select1' for fetching
						/*************************** for pagination **************************/
                        
                        
                        while ($row = mysql_fetch_array($select1)) {
                        	
                        	//for permission of edit and delete
                        	$loginIDs=$row['loginId'];
                        	$tabProId=$row['proId'];
                        	
                        	$num=$num2=$num3=0;
                        	$tableId = $row['invoiceNo'];
                        	
                        	//for getting username in index
			                        	
			                        	$queryOfLog = "SELECT type from ".TABLE_LOGIN."
				                     				WHERE ".TABLE_LOGIN.".ID='$loginIDs'";   									
				                     				
				                     $rowsLog = $db->query($queryOfLog);		
									 $resLog=mysql_fetch_array($rowsLog);
									 
									 $loginType	=$resLog['type'];
											
                        	if($tabProId==0)
                        	{
								$userName ='Admin';
							}
                        	else if($tabProId!=0) {
							if($loginType=="Branch")
							{
							$qry="SELECT companyName FROM ".TABLE_COMPANY.",".TABLE_LOGIN."
								  WHERE ".TABLE_COMPANY.".ID=".TABLE_LOGIN.".userId
								  AND ".TABLE_LOGIN.".ID=$loginIDs";
							$rows = $db->query($qry);		
							$res=mysql_fetch_array($rows);
							$userName=$res['companyName'];	  
							}
							if($loginType=="Staff")
							{
							$qry="SELECT staffName FROM ".TABLE_STAFF.",".TABLE_LOGIN."
								  WHERE ".TABLE_STAFF.".ID=".TABLE_LOGIN.".userId
								  AND ".TABLE_LOGIN.".ID=$loginIDs";
							$rows = $db->query($qry);		
							$res=mysql_fetch_array($rows);
							$userName=$res['staffName'];
							}
							}
			                        	
			                        	//
                        	
                            ?>
                         <tr>                            
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $userName; ?></td>
                            <td><?php echo "MI".$row['invoiceNo']; ?></td>
                            <td><?php echo $App->dbformat_date($row['invoiceDate']); ?></td>
                            <td><?php echo $row['accountName']; ?></td>
                            <td><?php echo $row['netAmount']; ?></td>
                            <td>
                <a class="show_table_lnk show_table_lnk_view" href="view.php?id=<?php echo $row['ID'];?>">View</a>
                <!--for warning if invoice in purchased-->
                    <?php  
                    $qry = "SELECT ".TABLE_PURCHASE_MISC. ".ID,
                    			   ".TABLE_PURCHASE_MISC. ".invoiceNo
                     		FROM ".TABLE_PURCHASE_MISC. "
                     		where ourInvoiceNo=$tableId and proId=$proId";
                    	$selectPurchase= $db->query($qry);
						$num		=	mysql_num_rows($selectPurchase);
						
						
						if($num!=0){
							$row1		=	mysql_fetch_array($selectPurchase);
							$invoiceNo = $row1['invoiceNo'];
							$purId = $row1['ID'];
							$qry2 = "SELECT ID FROM ".TABLE_CASH_PAYMENT." WHERE proId=$proId and voucherType='miscellaneous' and invoiceNo = $invoiceNo";//echo $qry2;
						$resultQry = $db->query($qry2);
						$num2	=	mysql_num_rows($resultQry);
						}
						
						$qry3 = "SELECT ID FROM ".TABLE_CASH_RECEIPT." where ".TABLE_CASH_RECEIPT.".invoiceNo = $tableId and proId= $proId and voucherType='miscellaneous'";
						$resultQry3 = $db->query($qry3);
						$num3  = mysql_num_rows($resultQry3);
						
					
                    ?>
                    
                
                
                
               <?php if(($invoiceMiscEdit==1 && $_SESSION['travelType']=="Staff" && $logID==$loginIDs) || ($_SESSION['travelType']=="Branch") ){ ?>
					<a class="show_table_lnk show_table_lnk_edit" href="edit.php?id=<?php echo $row['ID'];?>" <?php if($num2!=0){?> onclick="return pay(<?php echo $num2; ?>)" <?php } if($num!=0) { ?> onclick="return purchase(<?php echo $num; ?>)" <?php } ?><?php if($num3!=0) { ?> onclick="return receipt(<?php echo $num3; ?>)" <?php } ?>>Edit</a>
					<?php } else { ?>
						<a href="javascript:void(0)" class="bd_lnk bd_lnk_disabled">Edit</a>
                     <?php } ?>
                
                <?php if(($invoiceMiscDelete==1 && $_SESSION['travelType']=="Staff" && $logID==$loginIDs) || ( $_SESSION['travelType']=="Branch") ){ ?>
                <a class="show_table_lnk show_table_lnk_del" <?php if($num2!=0){?> onclick="return payment_delete(<?php echo $num2; ?>);" <?php } ?><?php if($num3!=0){?> onclick="return receipt_delete(<?php echo $num3; ?>);" <?php } ?><?php if($num==0) { ?> onclick="return delete_type();" <?php } else {?> onclick="return purchase_delete(<?php echo $num; ?>);" <?php } ?> href="do.php?id=<?php echo $row['ID'];?>&op=delete">Delete</a>
                	<?php } else {//end of if condition?>
                    
                    <a href="javascript:void(0)" class="bd_lnk bd_lnk_disabled">Delete</a>
                    <?php } ?>
                
                
                
                <a class="show_table_lnk show_table_lnk_edit" href="do.php?id=<?php echo $row['ID'];?>&op=print" target="_blank">Print</a>
                 <!--<a class="up_doc_trigger" data-target="#up_doc" data-inv_id="<?= $row['ID']; ?>">Upload</a> -->
                 <a class="up_doc_trigger bd_lnk bd_lnk_blu" href="#" data-target="#up_doc" data-inv_id="<?= $row['ID']; ?>"><i class="fa fa-upload"></i></a>
                    <?php if($row['document']!=''){?>
                    <a href="<?php echo $row['document']; ?>" class="bd_lnk bd_lnk_blu" target="_blank"><i class="fa fa-download"></i></a>
                    <?php } if($num >0) { ?>
						<a class="show_table_lnk show_table_lnk_view" href="../purchase/view.php?id=<?php echo $purId; ?>&type=m">PR</a>
				<?php	} ?>              
                            </td>
                        </tr>
		                    <?php
                   		 }
                   		 }
                   /*	}
                   	else{
						?>
						<tr>
							<td colspan="11" align="center">No data found</td>
						</tr>
						<?php
					}*/
                    ?>
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Popups -->
    <!--Popup for account View-->
    <div id="acs_view_pop" class="modal fade bd_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Miscellaneous Services</h4></div>
	                <div class="modal-body">
	                    <div class="acs_view_table_wrap table-responsive">
	                        <table class="table table-bordered table-striped acs_view_table">
	                            <tbody></tbody>
	                        </table>
	                    </div>
	                </div>
	                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<!-- Popups -->
    <!--Popup for account View-->
    <div id="up_doc" class="modal fade bd_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            <form method="post" action="do.php?op=upload" enctype="multipart/form-data">
            <input type="hidden" name="invoiceDocId" id="invoice_doc_id"/>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Document Upload</h4></div>
                <div class="modal-body">
                    <div class="acs_view_table_wrap table-responsive">                   
                        <table class="table table-bordered table-striped acs_view_table">
                            <tbody>
                            	<td>                            	
	                        		<div class="form_block">
										<label>Upload File<span class="valid">*</span></label>
										<input type="file" name="invDoc" id="invDoc" required="" readonly="" >
									</div>
								</td>
								
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" name="save" value="SAVE"><!--UPLOAD</button>-->
                </div>
               </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
<script>
	$(function () {
		$(document).on('click', '.up_doc_trigger', function(e) {
			e.preventDefault();
			$('#up_doc input#invoice_doc_id').val($(this).attr('data-inv_id'));
			$('#up_doc').modal('show');	
		});	
	});	
</script>
<?php
require('../admin_footer2.php');
?>