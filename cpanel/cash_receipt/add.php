<?php
require('../admin_header.php');

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" id="cash_receipt" class="default_form" onsubmit="return check()">
                <div class="bd_panel_head">
                    <h3>CASH RECEIPT</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Date <span class="valid">*</span></label>
                                <input type="text" name="transactionDate" id="transactionDate" class="user_date"
                                       required="" value="<?php echo date("d-m-Y") ?>">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <?php
                                /*$vNo = "SELECT max(voucherNo) as vouNo  FROM " . TABLE_TRANSACTION . " WHERE voucherType='Cash Receipts' ";
                                $voucherRecord = mysql_query($vNo);
                                $result = mysql_fetch_array($voucherRecord);
                                $vouNo = $result['vouNo'] + 1;
*/
			 				$vouNo		=	$db->nextVoucher(TABLE_TRANSACTION,'voucherNo',"voucherType='Cash Receipts' AND proId='$proId'" );
                                ?>
                                <label>Voucher No <span class="valid">*</span></label>
                                <input type="text" name="voucherNo" required="" value="<?php echo 'CR' . $vouNo; ?>"  readonly="">
                            </div>
                        </div>
                        <!--<div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Voucher Type*</label>
                                <select name="invoiceType">
                                    <option value="airline" selected>Airline</option>
                                    <option value="hotel">Hotel</option>
                                    <option value="miscellaneous">Miscellaneous</option>
                                    <option value="transportation">Transportation</option>
                                    <option value="visa">Visa</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account From*</label>
                                <select name="accountFrom" required="">
                                    <option value="">Select</option>
                                    <?php
/*                                    $select = "select * from " . TABLE_ACCOUNTS . "";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        */?>
                                        <option
                                            value="<?php /*echo $row['ID']; */?>"><?php /*echo $row['accountName']; */?></option>
                                        <?php
/*                                    }
                                    */?>
                                </select>
                            </div>
                        </div>-->
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account from Name <span class="valid">*</span></label>
                                <div class="aj_rel_box aj_account_from">
                                    <input name="accountFromName" autocomplete="off" required="" id="accountFromName" type="text">
                                    <ul class="aj_live_items"></ul>
                                    <p class="validation_mes">Please fill this!</p>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-2 col-md-2 col-sm-3">
                            <label>Account From ID <span class="valid">*</span></label>
                            <div class="aj_rel_box aj_account_from">
                                <input type="text" name="accountFrom" id="accountFrom" autocomplete="off" required />
                                <ul class="aj_live_items"></ul>
                                <p class="validation_mes">Please fill this!</p>
                            </div>
                        </div>
                        <!--<div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account To*</label>
                                <select name="accountTo" required="">
                                    <option value="">Select</option>
                                    <?php
                                    $select = "select * from " . TABLE_ACCOUNTS . " where `accountGroupId`='8' OR `accountGroupId`='15'";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        ?>
                                        <option
                                            value="<?php echo $row['ID']; ?>"><?php echo $row['accountName']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>-->
                        <input type="hidden" name="accountTo" value="2">

                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label style="visibility: hidden;">Get Unpaid Bills</label>
                                <button class="bd_btn bd_btn_blue" id="get_bill_details"
                                        data-target="#bill_list_pop_receipt">Get Bill Details
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="cash_payment_table_wrap">
                        <table class="table table-bordered bd_table" id="tbl_cash_receipt">
                            <thead>
                            <tr class="table_head">
                                <th colspan="10" class="text-center">Cash Receipt</th>
                                <!--<th colspan="2" class="text-right">
                                    <button class="bd_btn bd_btn_blue" id="add_cash_receipt_row" type="button">
                                        <i class="ion ion-plus"></i>Add New
                                    </button>
                                </th>-->
                            </tr>
                            <tr>
                                <th>Sl No</th>
                                <th>Against our invoice no*</th>
                                <th>Date</th>
                                <th>Voucher Type</th>
                                <th>Total</th>
                                <th>Balance</th>
                                <th>Discount</th>
                                <th>Paying Amt</th>
                                <th>Remarks</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                            <div class="form_block">
                                <label>Net Amount <span class="valid">*</span></label>
                                <input type="text" name="amount" id="amount" value="0" required="">
                            </div>

                            <div class="form_block">
                                <label>Remark</label>
                                <textarea name="remark" id="remark"></textarea>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="hidden" name="rowCount" id="cash_receipt_row_count" value="1">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
?>

<!-- Pop up for bill list -->
<div id="bill_list_pop_receipt" class="modal fade bd_modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pending Bills</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered" id="bill_list">
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Vouvher No</th>
                        <th>Date</th>
                        <th>Voucher Type</th>
                        <th>Details</th>
                        <th>Total</th>
                        <th>Paid</th>
                        <th>Balance</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="8">No bills found unpaid.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="trigger_cash_payment">Pay Now</button>
            </div>
        </div>
    </div>
</div>
<!-- /Pop up for bill list -->


<!-- div for select box for airline details -->
<!--<div id="cash_receipt_common_row" style="display: none">
    <table class="table table-bordered bd_table" id="tbl_cash_reciept">
        <tbody>
        <tr>
            <td>1</td>
            <td>
                <select name="invoiceType">
                    <option value="airline" selected>Airline</option>
                    <option value="hotel">Hotel</option>
                    <option value="miscellaneous">Miscellaneous</option>
                    <option value="transportation">Transportation</option>
                    <option value="visa">Visa</option>
                    <option value="package">Package</option>
                </select>
            </td>
            <td>
                <input type="text" name="invoiceNo" data-rel="bill_live">
            </td>
            <td>
                <input type="text" data-dp="datepick" name="invoiceDate" readonly>
            </td>
            <td>
                <input type="text" name="total" value="0.00" readonly>
            </td>
            <td>
                <input type="text" name="totalPaid" value="0.00" readonly>
            </td>
            <td>
                <input type="text" data-action="trigger_cash_calculation" name="payingAmount" value="0.00">
            </td>
            <td>
                <textarea name="remark"></textarea>
            </td>
            <td>
                <button class="bd_btn bd_btn_red" type="button" data-action="del_cash_receipt_row">Delete</button>
            </td>
        </tr>
        </tbody>
    </table>
</div>-->
<?php
require('../admin_footer2.php');
?>
