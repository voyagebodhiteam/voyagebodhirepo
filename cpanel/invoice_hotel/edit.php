<?php
require("../admin_header.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';
$i = 0;

$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ".TABLE_INVOICE_HOTEL." where ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>
<script>
function valid()
{	
	flag=false;
	noOfNight=document.getElementById('noOfNight').value;
	adults=document.getElementById('adults').value;
	childrens=document.getElementById('childrens').value;
	totalPersons=document.getElementById('totalPersons').value;
	tds=document.getElementById('tds').value;
	tax=document.getElementById('tax');
	letterNumber = /^[0-9]*\.?[0-9%]+$/;
	otherCharge=document.getElementById('otherCharge').value;
	discount=document.getElementById('discount').value;	
	netAmount=document.getElementById('netAmount').value;
	
	if(isNaN(noOfNight) || noOfNight=="")
	{																			
	document.getElementById('noOfNightDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(adults) || adults=="")
	{																			
	document.getElementById('adultsDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(childrens) || childrens=="")
	{																			
	document.getElementById('childrensDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(totalPersons) || totalPersons=="")
	{																			
	document.getElementById('totalPersonsDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(tds) || tds=="")
	{																			
	document.getElementById('tdsDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(!tax.value.match(letterNumber))
	{																			
	document.getElementById('taxDiv').innerHTML="Enter valid Tax number.";
	flag=true;
	}
	if(isNaN(otherCharge) || otherCharge=="")
	{																			
	document.getElementById('otherChargeDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(discount) || discount=="")
	{																			
	document.getElementById('discountDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(netAmount) || netAmount=="" || netAmount==0 || netAmount <0)
	{																			
	document.getElementById('netAmountDiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
	//for getting alert if invalid 'checkindate' added.
	
	var cIndate	=	document.getElementById('cInVal').value;
	//alert(exdate);
			if(cIndate == 1)
			{
				document.getElementById('checkInDiv').innerHTML = "Invalid Check In Date";
				flag	=	true;
			}
	
	
	//for getting alert if invalid 'fromdate' added.
	var cOutdate	=	document.getElementById('cOutVal').value;
	//alert(exdate);
			if(cOutdate == 1)
			{
				document.getElementById('checkOutDiv').innerHTML = "Invalid Check Out Date";
				flag	=	true;
			}
	
	
	//for customer name and customer id validation	
		var customerName	=	document.getElementById('customerName').value;
		var customerId		=	document.getElementById('customerId').value;
		var getResponse=0;
		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
           // document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
          	getResponse= xmlhttp.responseText;//alert(getResponse);
          	
          	 
          	
          }	
          
	    };
	   
	   	xmlhttp.open("GET", "../../services/customer_check_ajax.php?name="+customerName+"&id="+customerId, false);
	    xmlhttp.send();
	  
	
		 		if(getResponse==0)
	          	{
	          		flag=true;
                                alert("Invalid Customer Name and ID.!");
				}
	
	
	
	if(flag==true)
	{
	return false;
	}
	
	
}

//clear the validation msg

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
	
</script>
<script>
	function total_persons()
	{
	var adults=document.getElementById('adults').value;
	if(adults==''){adults=0;}
	var childrens=document.getElementById('childrens').value;
	if(childrens==''){childrens=0;}	
	
	var tot=parseInt(adults)+parseInt(childrens);	
	document.getElementById('totalPersons').value=tot;
	}

    function total_days()
    {
        var checkInDate=document.getElementById('checkInDate').value;
        var checkOutDate=document.getElementById('checkOutDate').value;
        
        var today = new Date();
        var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 

		today = dd+'-'+mm+'-'+yyyy;
		
		//check in date validation
		
		if(checkInDate != '')
		{
			
			var cInDateSplit= checkInDate.split('-');
			var todaySplit= today.split('-');
			
			var cInDateLast 	= 	new Date(cInDateSplit[2], +cInDateSplit[1]-1, cInDateSplit[0]);
			var todayLast 		= 	new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
			var dateDiff		=	(todayLast.getTime() - cInDateLast.getTime()) / (1000*60*60*24);
			
			if(dateDiff>0)
			{
				document.getElementById('cInVal').value	=1;
				alert("Invalid CheckIn Date");
				flag=true;
			}
			else {
			
			document.getElementById('cInVal').value	='';
		}
		} // end of check in date validation
		
		if(checkOutDate!= '')
		{
			var cOutDateSplit= checkOutDate.split('-');
			var todaySplit= today.split('-');
			
			var cOutDateLast 	= 	new Date(cOutDateSplit[2], +cOutDateSplit[1]-1, cOutDateSplit[0]);
			var todayLast 		= 	new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
			var dateDiff		=	(todayLast.getTime() - cOutDateLast.getTime()) / (1000*60*60*24);
			
			if(dateDiff>0)
			{
				document.getElementById('cOutVal').value	=1;
				alert("Invalid CheckOut Date");
				flag=true;
			}
			else {
			
			document.getElementById('cOutVal').value	='';
		}
			
		}//end of check out date validation
        
       if (checkInDate != "" && checkOutDate != "")
        {
            //alert(checkInDate+checkOutDate);
            
            var checkInDateSplit= checkInDate.split('-');
            var checkOutDateSplit= checkOutDate.split('-')
            
            var checkInLast = new Date(checkInDateSplit[2], +checkInDateSplit[1]-1, checkInDateSplit[0]);
            var checkOutLast = new Date(checkOutDateSplit[2], +checkOutDateSplit[1]-1, checkOutDateSplit[0]);
            var dateDiff=(checkOutLast.getTime() - checkInLast.getTime()) / (1000*60*60*24);
            //var age=Math.round(dateDiff/365);
			if(dateDiff==0)
			{
				dateDiff=1;
			}
			if(dateDiff<0)
			{
				alert("Invalid Dates");
				dateDiff="";
			}
			
            document.getElementById('noOfNight').value=dateDiff;
        }

    }
</script>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form class="default_form" id="invoice_hotel" method="post" action="do.php?op=edit" onsubmit="return valid()">
                 <input type="hidden" name="editId" value="<?php  echo $editId;?>">
                    <div class="bd_panel_head">
                        <h3>Hotel - Invoice</h3>
                    </div>
                    <div class="bd_panel_body">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher No <span class="valid">*</span></label>                                   
                                    <input type="text" name="invoiceNo" value="<?php echo "HI" .$editRow['invoiceNo']; ?>"  required="" readonly="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher Date </label>
                                    <input class="user_date" type="text" name="invoiceDate"
                                          value="<?php echo $App->dbFormat_date($editRow['invoiceDate']); ?>" required="">
                                </div>
                            </div>
                           

                           <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer Name <span style="color:#FF9900;">*</span></label> <!--select from account where accGpId=37-->
								<?php
								$cId=$editRow['customerId']; 
								$editSelect2 = 	"select  accountName                          			
								from ".TABLE_ACCOUNTS." 
								where ID='$cId'";
								$editResult2 = 	$db->query($editSelect2);
								$editRow2	 = 	mysql_fetch_array($editResult2);
								?>
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerName" autocomplete="off" required="" value="<?php echo $editRow2['accountName'];?>" id="customerName">
									<input type="hidden" name="customerId">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
					
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer ID </label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerId" autocomplete="off" required="" value="<?php echo $editRow['customerId'];?>" id="customerId">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
                          <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Hotel <span class="valid">*</span></label>
                                    <select name="hotelId" class="pass_to_table" required="">
                                        <option value="">Select</option>
                                        <?php
                                        $select = "select * from " . TABLE_HOTEL." where proId=$proId and finYear=$finYear";
                                        $res = $db->query($select);
                                        while ($row = mysql_fetch_array($res)) {
                                            ?>
                                            <option
                                                value="<?php echo $row['ID']; ?>" <?php if($row['ID']==$editRow['hotelId']){ echo 'selected';}?>><?php echo $row['hotelName']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>   
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form_section_head"><h3>No of Persons <span style="color:#FF9900;">*</span></h3></div>
                            </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Adults</label>
                                    <input type="text" name="adults"  value="<?php echo $editRow['adults'];?>"  id="adults" onfocus="clearbox('adultsDiv')" onmouseout="total_persons()">
								<div class="valid" id="adultsDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Children</label>
                                    <input type="text" name="childrens"  value="<?php echo $editRow['childrens'];?>" id="childrens" onfocus="clearbox('childrensDiv')" onmouseout="total_persons()">
								<div class="valid" id="childrensDiv"></div>
                                </div>
                            </div>
                           <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Total</label>
                                    <input type="text" name="totalPersons" value="<?php echo $editRow['totalPersons'];?>" id="totalPersons" onfocus="clearbox('totalPersonsDiv')" readonly="">
								<div class="valid" id="totalPersonsDiv"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form_section_head"><h3>Check in <span style="color:#FF9900;">*</span></h3></div>
                            </div>
                            <div class="col-lg-6">
                            	<div class="form_section_head"><h3>Check Out <span style="color:#FF9900;">*</span></h3></div>
                             </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-6"><div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form_block">
                                    <label>Date <span class="valid">*</span></label>
                                    <input class="user_date" type="text" placeholder="Check-in Date" name="checkInDate" id="checkInDate" required="" onchange="total_days()" value="<?php echo $App->dbFormat_date($editRow['checkInDate']);?>" onfocus="clearbox('checkInDiv')">
                                    <input type="hidden" id="cInVal" name="cInVal" value=""/>
                                    <div class="valid" id="checkInDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form_block">
                                    <label>Time</label>
                                    <input type="text" name="checkInTime" value="<?php echo $editRow['checkInTime'];?>">
                                </div>
                            </div>
                        </div></div>

                        <div class="col-lg-6"><div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form_block">
                                    <label>Date <span class="valid">*</span></label>
                                    <input class="user_date" type="text" placeholder="Check-out Date" onchange="total_days()"
                                         id="checkOutDate"   name="checkOutDate" required="" value="<?php echo $App->dbFormat_date($editRow['checkOutDate']);?>" onfocus="clearbox('checkOutDiv')">
                                         <input type="hidden" id="cOutVal" name="cOutVal" value=""/>
                                    <div class="valid" id="checkOutDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form_block">
                                    <label>Time</label>
                                    <input type="text" name="checkOutTime" value="<?php echo $editRow['checkOutTime'];?>">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form_block">
                                  <label>Number of night</label>
                                  <input type="text" name="noOfNight" value="<?php echo $editRow['noOfNight'];?>" id="noOfNight" onfocus="clearbox('noOfNightDiv')">
								<div class="valid" id="noOfNightDiv"></div>
                                </div>
                            </div>
                         </div></div>
                        </div>
                        <div class="form_divider"></div>
                        <div class="multi_det">
                            <div class="multi_det_body">
                                <div class="multi_det_head">
                                    <h3>Room Details</h3>
                                    <div class="multi_det_control">
                                        <ul class="page_nav">
                                            <li>
                                                <a href="#" class="add_hotel_row">
                                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                                    Add New
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="bd_clear"></div>
                                </div>
                                <div class="multi_det_table_wrap table-responsive">
                                    <table class="table table-bordered multi_det_table" id="hotel_room_table">
                                        <thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Room Type</th>
                                            <th>Room/s</th>
                                            <th>Persons</th>
                                            <th>Rate</th>                             
                                            <th>Total Amount</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php				                        
										$editSelect2 = 	"select  * from ".TABLE_HOTEL_CUSTOMER." where hotelInvoiceId='$editId' and proId=$proId";
										$editResult2 = 	$db->query($editSelect2);
										$rowCountEdit	=	mysql_num_rows($editResult2);
										$i=0;
										while($editRow2	= 	mysql_fetch_array($editResult2))
										{
										?>
                                        <tr>
                                            <td><?php echo ++$i; ?> 
<input class="hotel_input" type="hidden" name="tax_<?php echo $i;?>" data-hotel_net="hotel_net_room_trigger" value="<?php echo $editRow2['subTax'];?>"> 
                                            </td>
                                            <td>                                            			                         	
                                                <select class="hotel_input" name="roomType_1">
                                                    <option value="">Select</option>
                                                    <?php
		                                            $selectAll = "select  * from ".TABLE_ROOM_TYPE." where proId=$proId and finYear=$finYear or proId='0'";
					                        		$result = $db->query($selectAll);
					                        		while ($row = mysql_fetch_array($result)) {
					                            	?>
                                                    <option value="<?php echo $row['ID'];?>" <?php if($row['ID']==$editRow2['roomType']){ echo 'selected';}?>><?php echo $row['roomType'];?></option>
                                                    <?php }?>
                                                </select>
                                            </td>
                                            <td><input class="hotel_input" type="text" name="noOfRooms_<?php echo $i;?>" data-hotel_net="hotel_net_room_trigger" value="<?php echo $editRow2['noOfRooms'];?>"></td>
                                            <td><input class="hotel_input" type="text" name="noOfPersons_<?php echo $i;?>" value="<?php echo $editRow2['noOfPersons'];?>"></td>
                                            <td><input class="hotel_input" type="text" name="rate_<?php echo $i;?>" data-hotel_net="hotel_net_room_trigger" value="<?php echo $editRow2['rate'];?>"></td>
                                            <td data-field_name="total_row_amt"><input class="hotel_input" type="text" name="totalAmount_<?php echo $i;?>" value="<?php echo $editRow2['netAmount'];?>"  data-hotel_net="hotel_net_trigger" readonly></td>
                                            <td><a class="hotel_row_del bd_lnk_del" href="#">Delete</a></td>
                                        </tr>
                                        <?php	
										}
										?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>TDS</label>
                                    <input type="text" name="tds" data-hotel_net="hotel_net_trigger" value="<?php echo $editRow['tds'];?>" id="tds" onfocus="clearbox('tdsDiv')">
								<div class="valid" id="tdsDiv"></div>
                                </div>
                            </div>                                                      
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charge</label>
                                    <input type="text" data-hotel_net="hotel_net_trigger" name="otherCharge" value="<?php echo $editRow['otherCharge'];?>" id="otherCharge" onfocus="clearbox('otherChargeDiv')">
								<div class="valid" id="otherChargeDiv"></div>

                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Tax ('%' for percentage)</label>
                                    <input type="text" name="tax" data-hotel_net="hotel_net_trigger" value="<?php echo $editRow['tax'];?>" id="tax" onfocus="clearbox('taxDiv')">
								<div class="valid" id="taxDiv"></div>
                                </div>
                            </div> 
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                   <label>Discount</label>
                                   <input type="text" name="discount" data-hotel_net="hotel_net_trigger"  value="<?php echo $editRow['discount'];?>" id="discount" onfocus="clearbox('discountDiv')">
								<div class="valid" id="discountDiv"></div>
                                </div>
                             </div> 
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                <label>Net Amount <span class="valid">*</span></label>
                                <input type="text" name="netAmount" required="" value="<?php echo $editRow['netAmount'];?>" id="netAmount" onfocus="clearbox('netAmountDiv')" readonly>
								<div class="valid" id="netAmountDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Remarks</label>
                                    <textarea name="remark"><?php echo $editRow['reference']; ?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label></label>
                                    <input type="hidden" name="billingIns" value="<?php echo $editRow['billingIns'];?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="airline_submit">                                   
                                    <input type="hidden" name="rowCount" id="hotel_row_count" value="<?php echo $rowCountEdit?>">
                                    <!--<input type="submit" value="UPDATE">-->
                                    <input type="submit" name="savePurchase" value="UPDATE & PURCHASE" onclick="return check()">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
    <div id="hotel_common_table_row" style="display: none">
        <table>
            <thead>
            <tr>
                <th>Sl No</th>
                <th>Room Type</th>
                <th>No. of Rooms</th>
                <th>No. of Persons</th>
                <th>Rate</th>
                <th>Total Amount</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td>
                    <select class="hotel_input" name="roomType">
                        <option value="">Select</option>
                        <?php
                        $selectAll = "select  * from ".TABLE_ROOM_TYPE." where proId=$proId and finYear=$finYear";
                        $result = $db->query($selectAll);
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <option value="<?php echo $row['ID'];?>"><?php echo $row['roomType'];?></option>
                        <?php }?>
                    </select>
<input class="hotel_input" type="hidden" data-hotel_net="hotel_net_room_trigger" name="tax">
                </td>
                <td><input class="hotel_input" type="text" data-hotel_net="hotel_net_room_trigger" name="noOfRooms"></td>
                <td><input class="hotel_input" type="text" name="noOfPersons"></td>
                <td><input class="hotel_input" type="text" data-hotel_net="hotel_net_room_trigger" name="rate"></td>
                <td data-field_name="total_row_amt"><input class="hotel_input" data-hotel_net="hotel_net_trigger" type="text" name="totalAmount" readonly></td>
                <td>
                    <a class="hotel_row_del bd_lnk_del" href="#">Delete</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<?php
require('../admin_footer2.php');
?>