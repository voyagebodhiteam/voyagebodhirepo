<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId	=	$_SESSION['travelId'];
$loginType	=	$_SESSION['travelType'];
$finYear=$_SESSION['finYear'];
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
         
if($loginType=='Branch')
	{
		$loginName1		=	"SELECT ".TABLE_COMPANY.".companyName 
							   FROM ".TABLE_LOGIN." ,".TABLE_COMPANY." 						  
							  WHERE ".TABLE_COMPANY.".ID=".TABLE_LOGIN.".userId
							  	AND ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	$loginNameRow['companyName'];			  
	}
if($loginType=='Staff')
	{
		$loginName1		=	"SELECT ".TABLE_STAFF.".staffName, 
									".TABLE_COMPANY.".companyName
							   FROM ".TABLE_LOGIN." ,".TABLE_STAFF.",".TABLE_COMPANY." 						  
							  WHERE ".TABLE_STAFF.".ID=".TABLE_LOGIN.".userId
							  	AND ".TABLE_COMPANY.".ID=".TABLE_STAFF.".proId
							  	AND ".TABLE_LOGIN.".ID=$loginId";
		$loginNameQry	=	mysql_query($loginName1);
		$loginNameRow	=	mysql_fetch_array($loginNameQry);		
		$loginName		=	/*$loginNameRow['companyName']."-".*/$loginNameRow['staffName'];			  
	}
	//echo $loginName;die;
 

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['customerId']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
           
            $success = 0;           
			$newInvId			=	$db->nextVoucher(TABLE_INVOICE_HOTEL,'invoiceNo',"proId='$proId'");	
			 $success =  $success2 = 0;
	$accId ="SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Hotel Receivable' ";
			 $acc_record = $db->query_first($accId);
			 $accountId=$acc_record['ID'];	//echo $accountId;die;
				
                $customerId				= 	$App->convert($_REQUEST['customerId']);           
                $data['customerId']		=	$customerId;                
                $data['invoiceNo']		=	$newInvId;
                
                //for getting the customerName	
                $cusName				=	$App->convert($_REQUEST['customerName']);
                //echo $cusName;die;
                if($_REQUEST['invoiceDate']) 
                {
					$data['invoiceDate']	=	$App->dbformat_date($_REQUEST['invoiceDate']);
				}
				
                $data['hotelId']		=	$App->convert($_REQUEST['hotelId']);
                
                //for getting the hotel ID
                $hId = $App->convert($_REQUEST['hotelId']);
                $data['noOfNight']		=	$App->convert($_REQUEST['noOfNight']);
                //for getting the no.of nights
                $noOfNights				=	$App->convert($_REQUEST['noOfNight']);
                $data['adults']			=	$App->convert($_REQUEST['adults']); 
                $data['childrens']		=	$App->convert($_REQUEST['childrens']); 
                $data['totalPersons']	=	$App->convert($_REQUEST['totalPersons']);
                if($_REQUEST['checkInDate'])
                {
					$data['checkInDate']	=	$App->dbformat_date($_REQUEST['checkInDate']);
				}
                $checkInDate = $App->convert($_REQUEST['checkInDate']); 
                $data['checkInTime']	=	$App->convert($_REQUEST['checkInTime']);
                if($_REQUEST['checkOutDate'])
                {
					$data['checkOutDate']	=	$App->dbformat_date($_REQUEST['checkOutDate']);
				}
                $data['checkOutTime']	=	$App->convert($_REQUEST['checkOutTime']);
                $data['billingIns']		=	$App->convert($_REQUEST['billingIns']); 
                $data['reference']		=	$App->convert($_REQUEST['reference']);
                
                $mainTax				=	$App->convert($_REQUEST['tax']);
                $pos 					= 	strpos($mainTax, '%');
                if($pos=='')
                {					 
                $data['tax']		=	$mainTax;               
				}
                
                $data['tds']			=	$tds2	=	$App->convert($_REQUEST['tds']);
                $data['otherCharge']	=	$otherCharge2	=	$App->convert($_REQUEST['otherCharge']); 
                $data['discount']		=	$App->convert($_REQUEST['discount']);
                $data['netAmount']		=	$App->convert($_REQUEST['netAmount']);                  
                $data['status']			=	'Pending';
                $data['proId']			=	$proId;
                $data['loginId']		=	$loginId;
                $data['finYear']		=	$finYear;

                $success = $db->query_insert(TABLE_INVOICE_HOTEL, $data);
               
                $k=1;	//echo $_POST['rowCount'];	die;	
                $sumSubTotal = $sumEachTotal = $totAmt	=	0;		
					for($k=1;$k<=$_POST['rowCount'];$k++)
					{												 
					   $roomType		=	"roomType_".$k;
					   $noOfRooms		=	"noOfRooms_".$k;
					   $noOfPersons		=	"noOfPersons_".$k;
					   $rate			=	"rate_".$k;
					   $subTax			=	"tax_".$k;					   			   
					   $totalAmount		=	"totalAmount_".$k;
					  
                
		               $data2['hotelInvoiceId']	=	$success;		                
		               $data2['roomType']		=	$App->convert($_REQUEST[$roomType]);
		               $data2['noOfRooms']		=	$App->convert($_REQUEST[$noOfRooms]);
		               //for getting the no of rooms
		               $noOfRooms				=	$App->convert($_REQUEST[$noOfRooms]);
		               $data2['noOfPersons']	=	$App->convert($_REQUEST[$noOfPersons]);		              
		               $data2['rate']			=	$App->convert($_REQUEST[$rate]); 
		               $data2['subTax']			=	$App->convert($_REQUEST[$subTax]); 		               
		               $data2['netAmount']		=	$sumEachTotal =	$App->convert($_REQUEST[$totalAmount]); 
		               $data2['proId']			=	$proId;
                	   $data2['loginId']		=	$loginId; 
                	   $data2['finYear']		=	$finYear; 
		               $sumSubTotal	+=$sumEachTotal;
		               $success2 = $db->query_insert(TABLE_HOTEL_CUSTOMER, $data2);
		             }
		             
		             /*-------------------------------------------------------------------------*/
		              //tax calculation()
		              
		              $totAmt	=	$sumSubTotal+$tds2+$otherCharge2;
		              if(strpos ($mainTax,'%'))
						{
							$taxTrim	=	rtrim($mainTax, "%");							
							$mainTax	=	$totAmt*($taxTrim/100);
							//echo $totAmt."<br>".$taxTrim."<br>".$mainTax;die;
							$data4['tax'] 		=  	$mainTax;
							$db->query_update(TABLE_INVOICE_HOTEL, $data4 , "ID=$success" );
						}
		             /*-------------------------------------------------------------------------*/
		           /*-------------------------------------------------------------------------*/
		           //To transaction table
		         															
					/* ------------------------------------------ */
					
					// for getting hotel name
					$select = "	select ". TABLE_HOTEL.".hotelName, 
									   ". TABLE_HOTEL.".city,
									   ". TABLE_HOTEL.".mobile as hotelMobile
								from ". TABLE_HOTEL." 
								where ". TABLE_HOTEL.".ID = $hId";
					//echo $select;die;
                    $res = $db->query($select);
               		$Row = mysql_fetch_array($res);
					$hotelName = $Row['hotelName'];
					$city	   = $Row['city'];
					$hotelMobile	=	$Row['hotelMobile'];
					
					$invoiceDetails	=	"INV#HI$newInvId,Hotel Name :$hotelName,City :$city,Contact No :$hotelMobile,CheckIn Date:$checkInDate, No.of Rooms: $noOfRooms,No.of Nights: $noOfNights,By:$loginName ";
					//echo $invoiceDetails;die;
					
					$tsdata['fromLedger'] 		=  	$customerId;
					$tsdata['toLedger'] 		=  	$accountId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Hotel Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Hotel Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$accountId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Hotel Receivable';
					$tfdata['credit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);
					$tfdata['debit']			= 	 0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Hotel Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					/*----------------------------------------------------------------------------*/
		          //discount
		          	$discount		=	$App->convert($_REQUEST['discount']);	         
		          if($discount>0)
		          {
		          	
		             $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given'";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          	
				  	$tsdata['fromLedger'] 		=  	$discountId;
					$tsdata['toLedger'] 		=  	$customerId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Hotel Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['discount']));
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Hotel Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$customerId;
					$tfdata['toLedger'] 		=  	$discountId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Hotel Receivable';
					$tfdata['credit']			=   abs($App->convert($_REQUEST['discount']));
					$tfdata['debit']			= 	 0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Hotel Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				   //tds
				   $tds = $_POST['tds'];
				   if($tds>0)
		          {
		          	
		          
		             $tdsQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tds' ";
					 $tds_record 		= 	$db->query_first($tdsQuery);
					 $tdsId				=	$tds_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=   $customerId;
					$tsdata['toLedger'] 		=  	$tdsId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Hotel Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_POST['tds']));
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Hotel Receivable:Tds#'.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$tdsId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Hotel Receivable';
					$tfdata['credit']			=   abs($App->convert($_POST['tds']));
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Hotel Receivable:Tds#'.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$tdsSuccess = $db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tax
				   if($mainTax>0)
		          {
		          	 $taxQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tax' ";
					 $tax_record 		= 	$db->query_first($taxQuery);
					 $taxId				=	$tax_record['ID'];
		          
				  	$tsdata['fromLedger'] 		=   $customerId;
					$tsdata['toLedger'] 		=  	$taxId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Hotel Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($mainTax);
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Hotel Receivable:Tax#'.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$taxId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Hotel Receivable';
					$tfdata['credit']			=   abs($mainTax);
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Hotel Receivable:Tax#'.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
									  
				  }
		           $db->close();		            		
	        if ($success) 
	        {
	            //$_SESSION['msg'] = " Details Added Successfully";
	            $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
	        } 
	        else 
	        {
	            $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
	        }      
	        @$sp=$_REQUEST['savePurchase'];
			@$s=$_REQUEST['save'];
			
			if($sp)
			{
				header("location:../purchase/add.php?type=h&pId=$success");
			}
			else if($s)
			{
				header("location:index.php");
			} 
    }
        break;
    // EDIT SECTION
   case 'edit':
        $editId = $_REQUEST['editId'];
         if (!$_REQUEST['customerId']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success =$success1=0;
            
            $existInvId	=	$db->existVoucherNew(TABLE_INVOICE_HOTEL,'invoiceNo'," ID='$editId' and proId=$proId");
			
			//for get the current amount in invoice airline for inserting to log
			$oldAmount				=	$db->query_first("SELECT netAmount FROM ".TABLE_INVOICE_HOTEL." WHERE ID='$editId' and proId=$proId"); 			 
						 
			 @mysql_query("DELETE FROM `" . TABLE_HOTEL_CUSTOMER . "` WHERE hotelInvoiceId='{$editId}' and proId=$proId and finYear=$finYear");
			 @mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$existInvId}' and voucherType='Hotel Receivable' and proId=$proId and finYear=$finYear");
			
			 $accId ="SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Hotel Receivable' ";
			 $acc_record = $db->query_first($accId);
			 $accountId=$acc_record['ID'];	//echo $accountId;die;
				
                $customerId				= 	$App->convert($_REQUEST['customerId']);           
                $data['customerId']		=	$customerId; 
                $newInvId				=	$existInvId;               
                $data['invoiceNo']		=	$newInvId;
                
                //for getting the customerName	
                $cusName				=	$App->convert($_REQUEST['customerName']);
                //echo $cusName;die;
                if($_REQUEST['invoiceDate']) 
                {
					$data['invoiceDate']	=	$App->dbformat_date($_REQUEST['invoiceDate']);
				}
				
                $data['hotelId']		=	$App->convert($_REQUEST['hotelId']);
                
                //for getting the hotel ID
                $hId = $App->convert($_REQUEST['hotelId']);
                $data['noOfNight']		=	$App->convert($_REQUEST['noOfNight']);
                //for getting the no.of nights
                $noOfNights				=	$App->convert($_REQUEST['noOfNight']);
                $data['adults']			=	$App->convert($_REQUEST['adults']); 
                $data['childrens']		=	$App->convert($_REQUEST['childrens']); 
                $data['totalPersons']	=	$App->convert($_REQUEST['totalPersons']);
                if($_REQUEST['checkInDate'])
                {
					$data['checkInDate']	=	$App->dbformat_date($_REQUEST['checkInDate']);
				}
                $checkInDate = $App->convert($_REQUEST['checkInDate']); 
                $data['checkInTime']	=	$App->convert($_REQUEST['checkInTime']);
                if($_REQUEST['checkOutDate'])
                {
					$data['checkOutDate']	=	$App->dbformat_date($_REQUEST['checkOutDate']);
				}
                $data['checkOutTime']	=	$App->convert($_REQUEST['checkOutTime']);
                $data['billingIns']		=	$App->convert($_REQUEST['billingIns']); 
                $data['reference']		=	$App->convert($_REQUEST['reference']);
                
                $mainTax				=	$App->convert($_REQUEST['tax']);
                $pos 					= 	strpos($mainTax, '%');
                if($pos=='')
                {					 
                $data['tax']		=	$mainTax;               
				}
                
                $data['tds']			=	$tds2	=	$App->convert($_REQUEST['tds']);
                $data['otherCharge']	=	$otherCharge2	=	$App->convert($_REQUEST['otherCharge']); 
                $data['discount']		=	$App->convert($_REQUEST['discount']);
                $data['netAmount']		=	$App->convert($_REQUEST['netAmount']);                  
                $data['status']			=	'Pending';
                $data['proId']			=	$proId;
                $data['loginId']		=	$loginId;
                $data['finYear']		=	$finYear;

                $success = $db->query_update(TABLE_INVOICE_HOTEL, $data,"ID=$editId");
               
                $k=1;	//echo $_POST['rowCount'];	die;	
                $sumSubTotal = $sumEachTotal = $totAmt	=	0;		
					for($k=1;$k<=$_POST['rowCount'];$k++)
					{												 
					   $roomType		=	"roomType_".$k;
					   $noOfRooms		=	"noOfRooms_".$k;
					   $noOfPersons		=	"noOfPersons_".$k;
					   $rate			=	"rate_".$k;
					   $subTax			=	"tax_".$k;					   			   
					   $totalAmount		=	"totalAmount_".$k;
					  
                
		               $data2['hotelInvoiceId']	=	$editId;		                
		               $data2['roomType']		=	$App->convert($_REQUEST[$roomType]);
		               $data2['noOfRooms']		=	$App->convert($_REQUEST[$noOfRooms]);
		               //for getting the no of rooms
		               $noOfRooms				=	$App->convert($_REQUEST[$noOfRooms]);
		               $data2['noOfPersons']	=	$App->convert($_REQUEST[$noOfPersons]);		              
		               $data2['rate']			=	$App->convert($_REQUEST[$rate]); 
		               $data2['subTax']			=	$App->convert($_REQUEST[$subTax]); 		               
		               $data2['netAmount']		=	$sumEachTotal =	$App->convert($_REQUEST[$totalAmount]); 
		               $data2['proId']			=	$proId;
                	   $data2['loginId']		=	$loginId; 
                	   $data2['finYear']		=	$finYear; 
		               $sumSubTotal	+=$sumEachTotal;
		               $success2 = $db->query_insert(TABLE_HOTEL_CUSTOMER, $data2);
		             }
		             
		             /*-------------------------------------------------------------------------*/
		              //tax calculation()
		              
		              $totAmt	=	$sumSubTotal+$tds2+$otherCharge2;
		              if(strpos ($mainTax,'%'))
						{
							$taxTrim	=	rtrim($mainTax, "%");							
							$mainTax	=	$totAmt*($taxTrim/100);
							//echo $totAmt."<br>".$taxTrim."<br>".$mainTax;die;
							$data4['tax'] 		=  	$mainTax;
							$db->query_update(TABLE_INVOICE_HOTEL, $data4 , "ID=$editId" );
						}
		             /*-------------------------------------------------------------------------*/
		           /*-------------------------------------------------------------------------*/
		           //To transaction table
		         															
					/* ------------------------------------------ */
					
					// for getting hotel name
					$select = "	select ". TABLE_HOTEL.".hotelName, 
									   ". TABLE_HOTEL.".city,
									   ". TABLE_HOTEL.".mobile as hotelMobile
								from ". TABLE_HOTEL." 
								where ". TABLE_HOTEL.".ID = $hId";
					//echo $select;die;
                    $res = $db->query($select);
               		$Row = mysql_fetch_array($res);
					$hotelName = $Row['hotelName'];
					$city	   = $Row['city'];
					$hotelMobile	=	$Row['hotelMobile'];
					
					$invoiceDetails	=	"INV#HI$newInvId,Hotel Name :$hotelName,City :$city,Contact No :$hotelMobile,CheckIn Date:$checkInDate, No.of Rooms: $noOfRooms,No.of Nights: $noOfNights,By:$loginName ";
					
					$tsdata['fromLedger'] 		=  	$customerId;
					$tsdata['toLedger'] 		=  	$accountId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Hotel Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Hotel Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$accountId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Hotel Receivable';
					$tfdata['credit']			=   ($App->convert($_POST['netAmount'])+$App->convert($_POST['discount']))-($App->convert($_POST['tds'])+$mainTax);
					$tfdata['debit']			= 	 0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Hotel Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					/*----------------------------------------------------------------------------*/
		          //discount
		          	$discount		=	$App->convert($_REQUEST['discount']);	         
		          if($discount>0)
		          {
		          	
		             $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given' and proId=$proId";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          	
				  	$tsdata['fromLedger'] 		=  	$discountId;
					$tsdata['toLedger'] 		=  	$customerId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Hotel Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['discount']));
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Hotel Receivable : '.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$customerId;
					$tfdata['toLedger'] 		=  	$discountId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Hotel Receivable';
					$tfdata['credit']			=   abs($App->convert($_REQUEST['discount']));
					$tfdata['debit']			= 	 0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Hotel Receivable : '.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				   //tds
				   $tds = $_POST['tds'];
				   if($tds>0)
		          {
		          	
		          
		             $tdsQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tds' ";
					 $tds_record 		= 	$db->query_first($tdsQuery);
					 $tdsId				=	$tds_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=   $customerId;
					$tsdata['toLedger'] 		=  	$tdsId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Hotel Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_POST['tds']));
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Hotel Receivable:Tds#'.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$tdsId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Hotel Receivable';
					$tfdata['credit']			=   abs($App->convert($_POST['tds']));
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Hotel Receivable:Tds#'.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tax
				   if($mainTax>0)
		          {
		          	 $taxQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tax' ";
					 $tax_record 		= 	$db->query_first($taxQuery);
					 $taxId				=	$tax_record['ID'];
		          
				  	$tsdata['fromLedger'] 		=   $customerId;
					$tsdata['toLedger'] 		=  	$taxId;
					$tsdata['voucherNo']		= 	$newInvId;
					$tsdata['voucherType']		=  	'Hotel Receivable';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($mainTax);
					$tsdata['addedDate']		=	"NOW()";
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	'Hotel Receivable:Tax#'.$newInvId;
					$tsdata['details']			=  	$invoiceDetails;
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$tsdata['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$taxId;
					$tfdata['toLedger'] 		=  	$customerId;
					$tfdata['voucherNo']		= 	$newInvId;
					$tfdata['voucherType']		=  	'Hotel Receivable';
					$tfdata['credit']			=   abs($mainTax);
					$tfdata['debit']			= 	0;
					$tfdata['addedDate']		=	"NOW()";
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	'Hotel Receivable:Tax#'.$newInvId;
					$tfdata['details']			=  	$invoiceDetails;
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$tfdata['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
									  
				  }
				  
				/*-------------------------------- Inserting to Log --------------------------*/ 
				$description 			=	"NetAmt:".$App->convert($_POST['netAmount']).",Discount:".$App->convert($_POST['discount']).",Tds:".$App->convert($_POST['tds']).",MainTax:".$mainTax;
				$datalog['module']		=	'Invoice Hotel';
				$datalog['tableId']		=	$editId;
				$datalog['invoiceNo']	=	$newInvId;
				$datalog['changeDate']	=	date("Y-m-d");
				$datalog['Status']		=	'Edit';
				$datalog['oldAmt']		=	$oldAmount['netAmount'];
				$datalog['newAmt']		=	$_REQUEST['netAmount'];			
				$datalog['description']	=	$description;
				$datalog['loginId']		=	$loginId;
				$datalog['proId']		=	$proId;
				$datalog['finYear']		=	$finYear;
				
				$db->query_insert(TABLE_LOG, $datalog);
				/*-------------------------------- Inserting to Log Ends --------------------------*/ 
		
		           $db->close();
			if($success){
			$_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");  
		} 
		else{
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to updated details. Please try again.");                   
		}
		@$sp	=	$_REQUEST['savePurchase'];
		@$s		=	$_REQUEST['save'];			
		if($sp){
			header("location:../purchase/edit.php?type=h&pId=$editId");
		}
		else if($s){
			header("location:index.php");
		}
	}        
	break;
    // DELETE SECTION
     case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success1=0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
		
		$existInvId	=	$db->existVoucherNew(TABLE_INVOICE_HOTEL,'invoiceNo'," ID='$deleteId' and proId=$proId");
		
		//for get the current amount in invoice airline for inserting to log
		$oldAmount	=	$db->query_first("SELECT netAmount,invoiceNo FROM ".TABLE_INVOICE_HOTEL." WHERE ID='$deleteId' and proId=$proId");
		$description = "InvoiceAmt:".$oldAmount['netAmount'];
			 $success1=mysql_query("DELETE FROM `" . TABLE_INVOICE_HOTEL . "` WHERE ID='{$deleteId}' and proId=$proId and finYear=$finYear");
			 @mysql_query("DELETE FROM `" . TABLE_HOTEL_CUSTOMER . "` WHERE hotelInvoiceId='{$deleteId}' and proId=$proId and finYear=$finYear");
			 @mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$existInvId}' and voucherType='Hotel Receivable' and proId=$proId and finYear=$finYear");
			
			//purchase
			@$purExe	=	@mysql_query("SELECT invoiceNo,netAmount FROM `" . TABLE_PURCHASE_HOTEL . "` WHERE ourInvoiceNo='{$existInvId}' and proId=$proId and finYear=$finYear");
			@$purNum		=	mysql_num_rows($purExe);
			if($purNum>0)
			{
				$purRes			=	mysql_fetch_array($purExe);
				$purInvoiceNo	=	$purRes['invoiceNo'];
				$description = $description.",PurchaseVoucherNo:".$purRes['invoiceNo'].",PurchaseAmt:".$purRes['netAmount'];

				@mysql_query("DELETE FROM `" . TABLE_PURCHASE_HOTEL . "` WHERE ourInvoiceNo='{$existInvId}' and proId=$proId and finYear=$finYear");
				@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$purInvoiceNo}' and voucherType='Hotel Payable' and proId=$proId and finYear=$finYear");
			}
			
			//payment
			$payAmt = 0;
			@$payExe	=	@mysql_query("SELECT ID,voucherNo,payingAmount FROM `" . TABLE_CASH_PAYMENT . "` WHERE invoiceNo='{$purInvoiceNo}' and voucherType='hotel' and proId=$proId and finYear=$finYear");
			@$payNum		=	mysql_num_rows($payExe);
			if($payNum>0)
			{
				while($payRes	=	mysql_fetch_array($payExe))
				{
					$payId	=	$payRes['ID'];
					$payVoucherNo	=	$payRes['voucherNo'];
					$payAmt = $payAmt+$payRes['payingAmount'];
					@mysql_query("DELETE FROM `" . TABLE_CASH_PAYMENT . "` WHERE ID='{$payId}' and proId=$proId and finYear=$finYear");
					@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$payVoucherNo}' and (voucherType='Cash Payments' || voucherType='Bank Payments') and proId=$proId and finYear=$finYear");
				}
			}
			$description = $description.",PaidAmt:".$payAmt;
			//receipt
			$recAmt	=	0;
			@$resExe	=	@mysql_query("SELECT ID,voucherNo,payingAmount FROM `" . TABLE_CASH_RECEIPT . "` WHERE invoiceNo='{$existInvId}' and voucherType='hotel' and proId=$proId");
			@$resNum		=	mysql_num_rows($resExe);
			if($resNum>0)
			{
				while($ReceiptRes	=	mysql_fetch_array($resExe))
				{
					$resId			=	$ReceiptRes['ID'];
					$resVoucherNo	=	$ReceiptRes['voucherNo'];
					$recAmt = $recAmt+$ReceiptRes['payingAmount'];
					@mysql_query("DELETE FROM `" . TABLE_CASH_RECEIPT . "` WHERE ID='{$resId}' and proId=$proId and finYear=$finYear");
					@mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$resVoucherNo}' and (voucherType='Cash Receipts' || voucherType='Bank Receipts') and proId=$proId and finYear=$finYear");
				}
			}
			$description = $description.",ReceivedAmt:".$recAmt;
			
			/*-------------------------------- Inserting to Log --------------------------*/ 
			$datalog['module']		=	'Invoice Hotel';
			$datalog['tableId']		=	$deleteId;
			$datalog['invoiceNo']	=	$oldAmount['invoiceNo'];
			$datalog['changeDate']	=	date("Y-m-d");
			$datalog['Status']		=	'Delete';
			$datalog['oldAmt']		=	$oldAmount['netAmount'];
			$datalog['newAmt']		=	0;			
			$datalog['description']	=	$description;
			$datalog['loginId']		=	$loginId;
			$datalog['proId']		=	$proId;
			$datalog['finYear']		=	$finYear;
			
			$db->query_insert(TABLE_LOG, $datalog);	
			/*-------------------------------- Inserting to Log -------------------------*/ 
	
        $db->close();
        if ($success1) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
        // Printing  ========================================================================
		case 'print':
				$printID	=	$_REQUEST['id'];
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();	
				
				$userId		=	$_SESSION['proId'];
				$sql2 		= 	"SELECT * FROM `".TABLE_COMPANY."` where ID=$userId";
				$vou_record1 = 	$db->query_first($sql2);
				$company 	= 	$vou_record1['companyName'];
				$caddress 	= 	$vou_record1['address'];
				$cphone		=	$vou_record1['phone'];
				$cmobile	=	$vou_record1['mobile'];	
				$cemail		=	$vou_record1['email'];
				$cpin		=	$vou_record1['pin'];
				

				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print

				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
				$sql2 = "SELECT a.ID,a.invoiceNo,a.invoiceDate,a.netAmount,a.tax,a.tds,
								a.otherCharge,a.discount,c.hotelName,c.city,c.mobile as hotMobile,
								b.accountName,b.address,b.mobile,b.phone,b.pin
						   FROM ".TABLE_INVOICE_HOTEL." as a
					  LEFT JOIN	".TABLE_ACCOUNTS." as b ON b.ID = a.customerId
					  LEFT JOIN ".TABLE_HOTEL." as c ON c.ID= a.hotelId
					      WHERE a.ID=$printID and a.proId=$proId and a.finYear=$finYear
						";
						//echo $sql2;

				$vou_record 	= 	$db->query_first($sql2);
				$hotelID		=	$vou_record['ID'];
						
				$vou_record 	= 	$db->query_first($sql2);
				$invoiceNo 		= 	$vou_record['invoiceNo'];
				$invoiceDate 	= 	$vou_record['invoiceDate'];
				$netAmount		= 	$vou_record['netAmount'];
				$tax 		    = 	$vou_record['tax'];
				//echo $tax;die;
				$tds 			= 	$vou_record['tds'];
				$otherCharge 	= 	$vou_record['otherCharge'];
				$discount	    = 	$vou_record['discount'];
				$mobile 		= 	$vou_record['mobile'];
				$accountName 	= 	$vou_record['accountName'];
				$address 		= 	$vou_record['address'];
				$phone 			= 	$vou_record['phone'];
				$pin 			= 	$vou_record['pin'];
				$hotelName		=	$vou_record['hotelName'];//echo $hotelName;die;
				$city			=	$vou_record['city'];
				$hotelMobile	=	$vou_record['hotMobile'];
				
				$details	=	"Hotel Name:". $hotelName.",City: ". $city.",Contact No: ".$hotelMobile;
				//echo $details;die;
				
				$proCharge		=	$vou_record['otherCharge']+$vou_record['tds'];
				
				$mainTax		=	$vou_record['tax'];
				//echo $mainTax;die;
				if(strpos ($mainTax,'%'))
				{
					$tax1		=	substr($mainTax,'%');
					$tax2		=	$tax1[0];
					$mainTax	=	$proCharge*($mainTax/100);
					//echo $mainTax;die;
				}
				
				if($mainTax)
				{
					
					$mainProcCharge	=	($mainTax+$proCharge)-$vou_record['discount'];
				}
				else
				{
					$mainProcCharge	=	$proCharge-$vou_record['discount'];
				}
				
				//$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);

				//basic info
				$tbl_header1 = '<div><table style="width: 530px;" cellspacing="0" border="0" >';
				$tbl_footer1 = '</table></div>';
				$tbl1 = '';
				
				$tbl1	=	'<tr>
								<td colspan="7" align="left" ><h2>'.strtoupper($company).'</h2></td>
							 </tr>
							 <tr>
								<td colspan="7"></td>
							 </tr>
							 <tr>
							 	<td colspan="4" align="left">'.$caddress.'<br>'.$cpin.'</td>
							 	<td colspan="3" align="left">'.$cphone.'<br>'.$cmobile.'<br>'.$cemail.'</td>
							 </tr>
							 <tr>
								<td colspan="7" ></td>
							 </tr>
							 <tr>
							 	<td colspan="4" style="border-top: 0.1px solid black; border-bottom: 0.1px solid black;">To , <strong>'.$accountName.'</strong></td>
							 	
							 	<td colspan="3" style="border-top: 0.1px solid black;border-bottom: 0.1px solid black;background-color:#F2F2F2; border-left: 0.1px solid black;" align="center">Hotel Invoice (HI)</td>
							 	
							 </tr>
							 
							 <tr>
							 	<td colspan="4"  style="border-bottom: 0.1px solid black;" align="left">'.$address.'<br>'.$pin.'</td>
							 	<td colspan="3" style="border-left: 0.1px solid black; border-bottom: 0.1px solid black;" align="left">
								 	<table >
									 	<tr>
									 		<td style="width:60px;" >Invoice :</td>
									 		<td style="width: 60px;" ><strong>'.$invoiceNo.'</strong></td><td style="width:60px;"><strong>'.$App->dbFormat_date($invoiceDate).'</strong></td>
									 	</tr>
									 	<tr>
									 		<td >Booked By :</td><td colspan="1"></td>
									 	</tr>
									 	<tr>
									 		<td >PNR :</td><td colspan="1">0</td>
									 	</tr>
								 	</table>
							 	</td>
							 	
							 </tr>
							';
				
				$pdf->SetFontSize(9);
				$pdf->writeHTML($tbl_header1 . $tbl1 . $tbl_footer1, true, true, false, false, '');
				//passenger TABLE
			
				$tbl_header = '<div><table style="width: 638px;" cellspacing="0" border="0" >';
				$tbl_footer = '</table></div>';
				$tbl = '';

				$tbl .= '<tr style="background-color:#F2F2F2">
					         <td style="width: 30px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Sr</strong></td>
					        <td style="width: 220px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Details</strong></td>
					        <td style="width: 80px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>No.of Room</strong></td>
					        <td style="width: 50px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>No.of Person</strong></td>
					        <td style="width: 60px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Rate</strong></td>
					       
					        <td style="width: 90px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "><strong>Total</strong></td>
				    	</tr>
				    	<tr>
				    		<td colspan="6"></td>
				    	</tr>
				    	';
					
				$passQry	=	"SELECT d.ID,d.noOfRooms,d.noOfPersons,
										d.netAmount,d.subTax,d.rate,e.roomType
								   FROM ".TABLE_HOTEL_CUSTOMER." as d
							  LEFT JOIN	".TABLE_ROOM_TYPE." as e ON d.roomType = e.ID
							    WHERE	d.hotelInvoiceId=$hotelID and d.proId=$proId and d.finYear=$finYear
								";
								
				$pass		=	mysql_query($passQry);
				$passNum	=	mysql_num_rows($pass);
				$defRow		=	8;	//for showing constant height for the table
				$i=0;
				
                                $sumTotal2=0;

				if($passNum>0)
				{
					while($passRow=mysql_fetch_array($pass))
					{
						$roomType = $passRow['roomType'];
						$sumTotal=$passRow['netAmount'];
						$sumTotal2=$sumTotal2+$sumTotal;
						 
						$tbl .= '<tr>
						        <td >'.++$i.'</td>
						        <td >'.$details.',RoomType :'.$roomType.'</td>
						        <td >'.$passRow['noOfRooms'].'</td>
						        <td >'.$passRow['noOfPersons'].'</td>
						        <td >'.sprintf("%0.2f",round($passRow['rate'],2)).'</td>
								<td >'.sprintf("%0.2f",round($passRow['netAmount'],2)).'</td>
						    	</tr>';	
					}
					
				}
                                  //Total charge with procesing charge
				
				$totalCharge=$mainProcCharge+$sumTotal2;

				if($passNum<$defRow)
				{
					$staticRow	=	$defRow-$passNum;
					for($i=0;$i<$staticRow;$i++)
					{
						$tbl.='<tr>
						       		<td colspan="6"></td>
						       </tr>';
					}
				}
                                $tbl.= '<tr >
							<td colspan="6" ></td>
						</tr>
						
				    		<tr>
				    			<td colspan = "4"  style=" width: 175px; border-top: 0.1px solid black;" ></td>
				    		<td colspan="1" style=" width: 250px; border-top: 0.1px solid black;">Total Charge</td>
				    		<td colspan="1" style=" width:100px; border-top: 0.1px solid black;" align="right">'.sprintf("%0.2f",round($totalCharge,2)).'</td>
				    		
				    		</tr>';
 
				$tbl.= '<tr >
				    		<td colspan = "4"  style=" width: 175px; border-top: 0.1px solid black; border-right: 0.1px solid black" ></td>
				    		<td colspan="1" style=" width: 250px; border-top: 0.1px solid black;" align="left"></td>
				    		<td  align="right" style=" width:100px; border-top: 0.1px solid black; " ></td>
				    	</tr>
				    	
				    	<tr>
				    		<td colspan = "4" style="border-right: 0.1px solid black;" ></td>
				    		<td colspan="1" align="left">Net Invoice Amount</td>
				    		<td align="right" >'.sprintf("%0.2f",round($netAmount,2)).'</td>
				    	</tr>
				    	<tr><td colspan="6" style=" font:courier; border-top: 0.1px solid black" >'.ucfirst($App->amountToWord(round($netAmount,2))).' </td></tr>
				    	
				    	';
				 $pdf->SetFontSize(9);
				 $pdf->writeHTML($tbl_header . $tbl . $tbl_footer, true, true, false, false, '');
				 
				 //terms and conditions
				$tbl_header2 = '<div><table style="width: 530px;" cellspacing="0" border="0" >';
				$tbl_footer2 = '</table></div>';
				$tbl2 = '';
				
				$tbl2	=	'<tr>
								<td  colspan="3" style="border-top: 0.1px solid black" >Terms :</td>
								<td colspan="2" style="border-top: 0.1px solid black" align="right">E.  & O.  E.</td>
								<td style="border-right: 0.1px solid black;border-top: 0.1px solid black"></td>
								<td colspan="1" style="border-top: 0.1px solid black"></td>
								<td colspan="3" style="border-top: 0.1px solid black" align="right" >for '.strtoupper($company).'</td>
							</tr>
							<tr>
								<td colspan="6" style="border-right: 0.1px solid black">
								# Subject to KOZHIKODE jurisdiction.<br>
								# Without original invoice no refund is permissible.<br>
								# Interest @ 24% will be charged on delayed payment<br>
								# Cheque to be drawn in our company name on presentation of invoice.<br>
								# Kindly check all details carefully to avoid un-necessary complications.<br>
									
								</td>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td colspan="4">Ticket Received in Order.</td>
								<td colspan="2" style="border-right: 0.1px solid black" align="right">Receivers Signature</td>
								
								<td colspan="4" align="right">Authorized Signatory</td>
							</tr>
							<tr>
								<td colspan="6" style="border-right: 0.1px solid black; border-bottom: 0.1px solid black">
								</td>
								<td colspan="4" style=" border-bottom: 0.1px solid black"></td>
							</tr>
							
							<tr>
								<td colspan="10" align="center">This is a Computer generated document and does not require any signature.
								</td>
							</tr>
							';
				$pdf->SetFontSize(7);
				$pdf->writeHTML($tbl_header2 . $tbl2 . $tbl_footer2, true, true, false, false, '');
				
				
				// Close and output PDF document
				$pdf->Output("'Invoice hotel'.$invoiceNo.pdf", 'I');		
		break;
			//Upload doc
		case 'upload':				
			    	
		if(!$_REQUEST['invoiceDocId'])
			{				
				$_SESSION['msg'] = $App->sessionMsgCreate('error', "Error, Invalid Details!"); 
				header("location:index.php");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=$document=0;
					
					$invoiceDocId	=	$App->convert($_REQUEST['invoiceDocId']);	
								
				
				// upload file
				$res=mysql_query("select document from ".TABLE_INVOICE_HOTEL." where ID='{$invoiceDocId}'");
				$row=mysql_fetch_array($res);
				$document=$row['document'];					
								
				$date		= 	date("YmdHis");
				$path_info  = 	pathinfo($_FILES["invDoc"]["name"]);
				$ext	 	=	$path_info["extension"];	
				//$allowed 	=  	array('gif','png','jpg');		// allowed image extensions					
				$newName 	= 	$date.".".$ext;
/*
				if(in_array($ext,$allowed) ) 	
				{*/
					$img='';	
					if(move_uploaded_file($_FILES["invDoc"]["tmp_name"],"document/".basename($newName)))
					{
						$doc="document/" . $newName;		
					}
					$data['document']			=	$doc;									
					$success1=$db->query_update(TABLE_INVOICE_HOTEL,$data," ID='{$invoiceDocId}'");					if($document)					
					{										
						if (file_exists($document)) 	
						{
						unlink($document);					
						} 						
					}					
					$db->close();								
				// }
				
				if($success1)
				{	
					$_SESSION['msg'] = $App->sessionMsgCreate('success', "Document uploaded Successfully"); 						
				}
				else
				{
					/*if(!in_array($ext,$allowed) ) 
					{
						$_SESSION['msg']="You have uploaded an invalid image file";
					}
					else
					{*/	
						$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed"); 
					// }
					
				}
				header("location:index.php");
			}
																					
			break;
}

?>