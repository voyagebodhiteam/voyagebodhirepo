<?php
require("../admin_header.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';
$i = 0;

$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ".TABLE_INVOICE_HOTEL." where ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form class="default_form" id="invoice_hotel" method="post" action="do.php?op=edit">
                 <input type="hidden" name="editId" value="<?php  echo $editId;?>">
                    <div class="bd_panel_head">
                        <h3>Hotel - Invoice</h3>
                    </div>
                    <div class="bd_panel_body">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher No</label>                                   
                                    <input type="text" name="invoiceNo" value="<?php echo "HI" .$editRow['invoiceNo']; ?>"  required="" readonly="" disabled="">
                                </div>
                            </div>
                           <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Voucher Date</label>
                                    <input class="user_date" type="text" name="invoiceDate"  disabled=""
                                          value="<?php echo $App->dbFormat_date($editRow['invoiceDate']); ?>" required="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Hotel</label>
                                    <select name="hotelId" class="pass_to_table" required="" disabled="">
                                        <option value="">Select</option>
                                        <?php
                                        $select = "select * from " . TABLE_HOTEL." where proId=$proId";
                                        $res = $db->query($select);
                                        while ($row = mysql_fetch_array($res)) {
                                            ?>
                                            <option
                                                value="<?php echo $row['ID']; ?>" <?php if($row['ID']==$editRow['hotelId']){ echo 'selected';}?>><?php echo $row['hotelName']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                     <label>Customer Name</label> <!--select from account where accGpId=37--> 
                                    <?php
                                    $cId=$editRow['customerId']; 
                                    $editSelect2 = 	"select  accountName                                    				
                                    				 from ".TABLE_ACCOUNTS." 
                                    				 where ID='$cId'";
									$editResult2 = 	$db->query($editSelect2);
									$editRow2	 = 	mysql_fetch_array($editResult2);
                                    ?>
                                    <input type="text" name="customerName" value="<?php echo $editRow2['accountName'];?>" disabled="">
                                    <input type="hidden" name="customerId" value="<?php echo $editRow['customerId'];?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form_section_head"><h3>No of Persons</h3></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Adults</label>
                                    <input type="text" name="adults" required="" value="<?php echo $editRow['adults'];?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Childrens</label>
                                    <input type="text" name="childrens" required="" value="<?php echo $editRow['childrens'];?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Total</label>
                                    <input type="text" name="totalPersons" required="" value="<?php echo $editRow['totalPersons'];?>" disabled="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form_section_head"><h3>Check in</h3></div>
                            </div>
                            <div class="col-lg-6">
                            	<div class="form_section_head"><h3>Check Out</h3></div>
                             </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"><div class="row">
                              <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form_block">
                                    <label>Date</label>
                                    <input class="user_date" type="text" placeholder="Check-in Date" name="checkInDate"  disabled="" required="" value="<?php echo $App->dbFormat_date($editRow['checkInDate']);?>">
                                </div>
                              </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form_block">
                                    <label>Time</label>
                                    <input type="text" name="checkInTime" value="<?php echo $editRow['checkInTime'];?>" disabled="">
                                </div>
                            </div>
                        </div>
                        </div>
                        
                        <div class="col-lg-6"><div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form_block">
                                    <label>Date</label>
                                    <input class="user_date" type="text" placeholder="Check-out Date" name="checkOutDate" required="" value="<?php echo $App->dbFormat_date($editRow['checkOutDate']);?>"  disabled="">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form_block">
                                    <label>Time</label>
                                    <input type="text" name="checkOutTime" value="<?php echo $editRow['checkOutTime'];?>"  disabled="">
                                </div>
                            </div>
                            
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form_block">
                                  <label>Number of night</label>
                                  <input type="text" name="noOfNight" required="" value="<?php echo $editRow['noOfNight'];?>" disabled="">
                                </div>
                            </div>
                        </div></div>
                        </div>
                        <div class="form_divider"></div>
                        <div class="multi_det">
                            <div class="multi_det_body">
                                <div class="multi_det_head">
                                    <h3>Room Details</h3>
                                   
                                    <div class="bd_clear"></div>
                                </div>
                                <div class="multi_det_table_wrap table-responsive">
                                    <table class="table table-bordered multi_det_table" id="hotel_room_table">
                                        <thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Room Type</th>
                                            <th>Room/s</th>
                                            <th>Persons</th>
                                            <th>Rate</th>
                                            <th>Tax Amount</th>                               
                                            <th>Total Amount</th>
                                           
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php				                        
										$editSelect2 = 	"select  * from ".TABLE_HOTEL_CUSTOMER." where hotelInvoiceId='$editId' and proId=$proId";
										$editResult2 = 	$db->query($editSelect2);
										$rowCountEdit	=	mysql_num_rows($editResult2);
										$i=0;
										while($editRow2	= 	mysql_fetch_array($editResult2))
										{
										?>
                                        <tr>
                                            <td><?php echo ++$i; ?></td>
                                            <td>                                            			                         	
                                                <select class="hotel_input" name="roomType_1">
                                                    <option value="">Select</option>
                                                    <?php
                                                    $exitType=$editRow2['roomType'];
		                                            $selectAll = "select  * from ".TABLE_ROOM_TYPE." where ID='$exitType'";
					                        		$result = $db->query($selectAll);
					                        		while ($row = mysql_fetch_array($result)) {
					                            	?>
                                                    <option value="<?php echo $row['ID'];?>" <?php if($row['ID']==$editRow2['roomType']){?> selected="" disabled="" <?php }?>><?php echo $row['roomType'];?></option>
                                                    <?php }?>
                                                </select>
                                            </td>
                                            <td><input class="hotel_input" type="text" name="noOfRooms_1" value="<?php echo $editRow2['noOfRooms'];?>" disabled=""></td>
                                            <td><input class="hotel_input" type="text" name="noOfPersons_1" value="<?php echo $editRow2['noOfPersons'];?>" disabled=""></td>
                                            <td><input class="hotel_input" type="text" name="rate_1" value="<?php echo $editRow2['rate'];?>" disabled=""></td>
                                            <td><input class="hotel_input" type="text" name="tax_1" value="<?php echo $editRow2['subTax'];?>" disabled=""></td>                                           
                                            <td data-field_name="total_row_amt"><input class="hotel_input" type="text" name="totalAmount_1" value="<?php echo $editRow2['netAmount'];?>" disabled=""  data-hotel_net="hotel_net_trigger"></td>
                                          
                                        </tr>
                                        <?php	
										}
										?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">                                                       
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>TDS</label>
                                    <input type="text" name="tds" value="<?php echo $editRow['tds'];?>" disabled="">
                                </div>
                            </div>                                          
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charge</label>
                                    <input type="text" name="otherCharge" value="<?php echo $editRow['otherCharge'];?>" disabled="">
                                </div>
                            </div>
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Tax ('%' for percentage)</label>
                                    <input type="text" name="tax" value="<?php echo $editRow['tax'];?>" disabled="">
                                </div>
                            </div> 
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                   <label>Discount</label>
                                   <input type="text" name="discount"  value="<?php echo $editRow['discount'];?>" disabled="">
                                </div>
                            </div>
                            
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                <label>Net Amount</label>
                                <input type="text" name="netAmount" required="" value="<?php echo $editRow['netAmount'];?>" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Remark</label>
                                   <textarea name="remark" disabled=""><?php echo $editRow['reference'];?></textarea>
                                </div>
                            </div>
                            
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label></label>
                                    <input type="hidden" name="billingIns" value="<?php echo $editRow['billingIns'];?>" disabled="">
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                               
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
    <script>
        $(function () {
            $('.user_date').each(function () {
                var curDatePicker = $(this);
                //alterTarget = curDatePicker.nextAll('.date_hidden');
                curDatePicker.datepicker({
                    dateFormat: "dd-mm-yy"
                    /*altFormat: "yy-mm-dd",
                     altField: alterTarget*/
                });
            });
        });
    </script>
<?php
require('../admin_footer2.php');
?>