<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$editId		=	$_REQUEST['id'];
$editSelect = 	"select  * from ". TABLE_ACADEMICYEAR." where ID='$editId'";
$editResult = 	$db->query($editSelect);
$editRow 	= 	mysql_fetch_array($editResult);
?>
<script>
function nextYear(str)
{
str=parseInt(str)+1;
document.getElementById('toYear').value=str;
}


function status_type()
{
var status=confirm("Do you Want to Edit The Current Academic Year ??");
if(status==true)
{
window.submit();
}
else
{
return false;
}
}

</script>

<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=edit" class="default_form" onsubmit="return valid()">
             <input type="hidden" name="editId" value="<?php echo $editId; ?>">
                <div class="bd_panel_head">
                    <h3>ACADEMIC YEAR REGISTRATION</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>From Year :<span class="valid">*</span></label>
                                <select name="fromYear" id="fromYear" onchange="nextYear(this.value)" title="From Year"  class="form-control2">
              
						<?php 
						for($i=2010;$i<2025;$i++)
						{
						?>
                    	<option value="<?php echo $i;?>" <?php if($i==$editRow['fromYear']){?> selected="selected"<?php }?>><?php echo $i;?></option>
						<?php 
						}
						?>
                    </select>
                            </div>
                        </div>
                         
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>To Year:</label>
                                <input type="text" name="toYear" id="toYear" title="To Year" placeholder="To Year" readonly  class="form-control2" value="<?php echo $editRow['toYear'];?>">
                            </div>
                        </div>
                         
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="UPDATE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
