<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}


if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
<script>
function nextYear(str)
{
str=parseInt(str)+1;
document.getElementById('toYear').value=str;
}


function status_type()
{
var status=confirm("Do you Want to Edit The Current Financial Year ??");
	if(status==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" onsubmit="return valid()">
                <div class="bd_panel_head">
                    <h3>FINANCIAL YEAR REGISTRATION</h3>
                </div>
                <div class="bd_panel_body">
                
                	<div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                            <label>Name </label>
                                <input type="text" name="name" id="name" title="name" placeholder="Name" class="form-control2">
                            </div>
                    	</div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>From Year: <span class="valid">*</span></label>
                    			<select name="fromYear" id="fromYear" onchange="nextYear(this.value)" required title="From Year"  class="form-control2">
			                	<option value="">Select</option>
								<?php 
								for($i=2010;$i<2025;$i++)
								{
								?>
			                	<option value="<?php echo $i;?>"><?php echo $i;?></option>
								<?php 
								}
								?>
		                		</select>
                            </div>
                        </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>To Year:</label>
                                <input type="text" name="toYear" id="toYear" title="To Year" placeholder="To Year" readonly  class="form-control2">
                                
                            </div>
                        </div>
                         <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label style="visibility: hidden;">To Year:</label>
                                <input type="submit" name="form" value="SAVE">
                                
                            </div>
                        </div>
		                
		             </div>
		          </div>
		        </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>