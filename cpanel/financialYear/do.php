<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype){
	// NEW SECTION
	case 'index':

	if(!$_REQUEST['fromYear'])
			{				
				$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");					
				header("location:index.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();											
				$success=0;
				$fromYear=$_REQUEST['fromYear'];	
				$toYear=$_REQUEST['toYear'];
				
				$existId=$db->checkValueExist(TABLE_ACADEMICYEAR,"fromYear='$fromYear' AND toYear='$toYear'");
				if($existId>0)
				{				
				//$_SESSION['msg']="Academic Year Is Already Exist!!";
				$_SESSION['msg'] = $App->sessionMsgCreate('error', "Academic Year Is Already Exist!!");					
				header("location:index.php");
				}
				else
				{
				$data['name']		=	$App->convert($_REQUEST['name']);
				$data['fromYear']		=	$App->convert($_REQUEST['fromYear']);
				$data['toYear']			=	$App->convert($_REQUEST['toYear']);
				$data['status']			=	'NO';
										
				$success=$db->query_insert(TABLE_ACADEMICYEAR, $data);								
				$db->close();
					if($success)
					{
					//$_SESSION['msg']="Academic Year Added Successfully";
					$_SESSION['msg']=$App->sessionMsgCreate("success", "Academic Year Added Successfully");					
					header("location:index.php");
					}
					else
					{
					$_SESSION['msg']=$App->sessionMsgCreate("error", "Failed ");
					header("location:index.php");					
					}
				}
			}		
		break;		
		
		case 'status':
					$id	=	$_REQUEST['id'];
					$success=0;				
		
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$select=mysql_query("SELECT * FROM ".TABLE_ACADEMICYEAR." WHERE status='Yes'");
				$row=mysql_fetch_array($select);
				
				$old_id=$row['ID'];
				
				$data['status']		=	'NO';
				
				$db->query_update(TABLE_ACADEMICYEAR, $data, " ID='{$old_id}'");									
				
				$data['status']		=	'YES';
				
				$success=$db->query_update(TABLE_ACADEMICYEAR, $data, " ID='{$id}'");									
				$db->close();
				
				
				if($success)
					{
					//$_SESSION['msg']="Current Academic Year Updated Successfully";
					$_SESSION['msg'] = $App->sessionMsgCreate('success', "Current Academic Year Updated Successfully");
					header("location:index.php");
					}
					else
					{
					//$_SESSION['msg']="Failed";
					
					$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed");	
					header("location:index.php");					
					}
				
		break;		
		
	// EDIT SECTION
	case 'edit':		
		$editId = $_REQUEST['editId'];  		    
		if(!$_REQUEST['fromYear'])
			{				
				$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");				
				header("location:edit.php?id=$fid");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$fromYear=$_REQUEST['fromYear'];	
				$toYear=$_REQUEST['toYear'];
				
				$existId=$db->existValuesId(TABLE_ACADEMICYEAR,"fromYear='$fromYear' AND toYear='$toYear' AND ID!='{$editId}'");
				if($existId>0)
				{				
				//$_SESSION['msg']="Academic Year Is Already Exist!!";
				$_SESSION['msg'] = $App->sessionMsgCreate("error", "Academic Year Is Already Exist!!");					
				header("location:edit.php?id=$editId");
				}
				else
				{
				$data['name']		=	$App->convert($_REQUEST['name']);
				$data['fromYear']		=	$App->convert($_REQUEST['fromYear']);
				$data['toYear']			=	$App->convert($_REQUEST['toYear']);
				$data['status']			=	'NO';
						
				$success=$db->query_update(TABLE_ACADEMICYEAR, $data ,"ID='{$editId}'");								
				$db->close();
				
				
				if($success)
					{
					//$_SESSION['msg']="Academic Year Updated Successfully";
					$_SESSION['msg'] = $App->sessionMsgCreate("success", "Academic Year Updated Successfully");								
					header("location:index.php");
					}
					else
					{
					//$_SESSION['msg']="Failed";	
					$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to update details. Please try again.");
					header("location:index.php");					
					}
				}
			}		
				
		break;			
	// DELETE SECTION
	//-
	case 'delete':		
				$id		=	$_REQUEST['id'];				
				$success=1;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				//$success=$db->query("DELETE FROM `".TABLE_ACADEMICYEAR."` WHERE ID='{$id}'");				
			
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_ACADEMICYEAR."` WHERE ID='{$id}'");				      
				}
				catch (Exception $e) 
				{
					 //$_SESSION['msg']="You can't edit. Because this data is used some where else";		
					 $_SESSION['msg'] = $App->sessionMsgCreate('error', "You can't edit. Because this data is used some where else");		            
				}																
				$db->close(); 					                
        
        
				if($success)
				{
				     //$_SESSION['msg']="Academic Year Deleted Successfully";	
				     $_SESSION['msg'] = $App->sessionMsgCreate("success", "Academic Year Deleted Successfully");						
					
				}
				else
				{
				$_SESSION['msg'] = $App->sessionMsgCreate('error', "You can't edit. Because this data is used some where else");	
								
				}
				header("location:index.php");			
		break;		
}
?>