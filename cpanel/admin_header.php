<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']==""){
	header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();

$proId 			=	$_SESSION['proId'];
$travelType		=	$_SESSION['travelType'];
$finYear	=	$_SESSION['finYear'];


$basicSetting		=	0;
$basicSettingEdit	=	0;
$basicSettingDelete	=	0;
$package			=	0;
$packageEdit		=	0; 
$packageDelete		=	0; 
$account			=	0;
$accountEdit		=	0;
$accountDelete		=	0;
$cashBank			=	0;
$cashBankEdit		=	0;
$cashBankDelete		=	0;
$invoiceAirline		=	0;
$invoiceAirlineEdit	=	0;
$invoiceAirlineDelete	=	0;
$invoiceHotel		=	0;
$invoiceHotelEdit	=	0;
$invoiceHotelDelete	=	0;
$invoiceVisa		=	0;
$invoiceVisaEdit	=	0;
$invoiceVisaDelete	=	0;
$invoiceMisc		=	0;
$invoiceMiscEdit	=	0;
$invoiceMiscDelete	=	0;
$invoiceTransport		=	0;
$invoiceTransportEdit	=	0;
$invoiceTransportDelete	=	0;
$invoicePackage			=	0;
$invoicePackageEdit		=	0;
$invoicePackageDelete	=	0;
$purchaseAirline		=	0;
$purchaseAirlineEdit	=	0;
$purchaseHotel		=	0;
$purchaseHotelEdit	=	0;
$purchaseVisa		=	0;
$purchaseVisaEdit	=	0;
$purchaseMisc		=	0;
$purchaseMiscEdit	=	0;
$purchaseTransport		=	0;
$purchaseTransportEdit	=	0;
$purchasePackage		=	0;
$purchasePackageEdit	=	0;
$accountReport		=	0;
$otherReport		=	0;
$user		=	0;
$userEdit	=	0;
$userDelete	=	0;

if($travelType=='Staff')
{			
$basicSetting		=	$_SESSION['basicSetting'];
$basicSettingEdit	=	$_SESSION['basicSettingEdit'];
$basicSettingDelete	=	$_SESSION['basicSettingDelete'];
$package			=	$_SESSION['package'];
$packageEdit		=	$_SESSION['packageEdit']; 
$packageDelete		=	$_SESSION['packageDelete']; 
$account			=	$_SESSION['account'];
$accountEdit		=	$_SESSION['accountEdit'];
$accountDelete		=	$_SESSION['accountDelete'];
$cashBank			=	$_SESSION['cashBank'];
$cashBankEdit		=	$_SESSION['cashBankEdit'];
$cashBankDelete		=	$_SESSION['cashBankDelete'];
$invoiceAirline		=	$_SESSION['invoiceAirline'];
$invoiceAirlineEdit	=	$_SESSION['invoiceAirlineEdit'];
$invoiceAirlineDelete	=	$_SESSION['invoiceAirlineDelete'];
$invoiceHotel		=	$_SESSION['invoiceHotel'];
$invoiceHotelEdit	=	$_SESSION['invoiceHotelEdit'];
$invoiceHotelDelete	=	$_SESSION['invoiceHotelDelete'];
$invoiceVisa		=	$_SESSION['invoiceVisa'];
$invoiceVisaEdit	=	$_SESSION['invoiceVisaEdit'];
$invoiceVisaDelete	=	$_SESSION['invoiceVisaDelete'];
$invoiceMisc		=	$_SESSION['invoiceMisc'];
$invoiceMiscEdit	=	$_SESSION['invoiceMiscEdit'];
$invoiceMiscDelete	=	$_SESSION['invoiceMiscDelete'];
$invoiceTransport		=	$_SESSION['invoiceTransport'];
$invoiceTransportEdit	=	$_SESSION['invoiceTransportEdit'];
$invoiceTransportDelete	=	$_SESSION['invoiceTransportDelete'];
$invoicePackage			=	$_SESSION['invoicePackage'];
$invoicePackageEdit		=	$_SESSION['invoicePackageEdit'];
$invoicePackageDelete		=	$_SESSION['invoicePackageDelete'];
$purchaseAirline		=	$_SESSION['purchaseAirline'];
$purchaseAirlineEdit	=	$_SESSION['purchaseAirlineEdit'];
$purchaseHotel		=	$_SESSION['purchaseHotel'];
$purchaseHotelEdit	=	$_SESSION['purchaseHotelEdit'];
$purchaseVisa		=	$_SESSION['purchaseVisa'];
$purchaseVisaEdit	=	$_SESSION['purchaseVisaEdit'];
$purchaseMisc		=	$_SESSION['purchaseMisc'];
$purchaseMiscEdit	=	$_SESSION['purchaseMiscEdit'];
$purchaseTransport		=	$_SESSION['purchaseTransport'];
$purchaseTransportEdit	=	$_SESSION['purchaseTransportEdit'];
$purchasePackage		=	$_SESSION['purchasePackage'];
$purchasePackageEdit	=	$_SESSION['purchasePackageEdit'];
$accountReport		=	$_SESSION['accountReport'];
$otherReport		=	$_SESSION['otherReport'];
$user		=	$_SESSION['user'];
$userEdit	=	$_SESSION['userEdit'];
$userDelete	=	$_SESSION['userDelete'];

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ebzilon</title>
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../../css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="../../css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../../css/perfect-scrollbar.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.structure.css">
	<link rel="stylesheet" type="text/css" href="../../css/jquery-ui.theme.css">
	<link rel="stylesheet/less" type="text/css" href="../../css/timepicker.less">


	<link rel="stylesheet" type="text/css" href="../../css/style.css">

	<script src="../../js/jquery-2.2.1.min.js"></script>
	<script src="../../js/jquery-ui.min.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<script src="../../js/perfect-scrollbar.jquery.js"></script>
	<script src="../../js/bootstrap-timepicker.js"></script>
	<script src="../../js/script.js"></script>

        <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>

</head>
<body>
<div class="side_panel opened">
	<div class="app_logo">
		<img src="../../images/logo.png" alt="" /> <span>Ebzilon</span>
	</div>
	<div class="user_det">
            
	</div>
	<div class="side_nav_wrapper">
		<?php
		if($_SESSION['travelType']=="Admin"){			
			?>

			<ul class="side_nav">
				<li>
					<a href="../admin-dashboard/">
						<span class="side_nav_ico"><i class="ion ion-ios-speedometer"></i></span>
						<span class="side_nav_text">Home</span>
					</a>
				</li>
				<li class="has_child">
					<!--<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
						<span class="side_nav_text">Registration</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">	-->	                
						<li>
							<a href="../company">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Company Registration</span>
							</a>
						</li>
						<li>
							<a href="../financialYear">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Financial Year Registration</span>
							</a>
						</li>
						<li class="has_child">
					
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
							<span class="side_nav_text">Registration</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">	
							<li>
								<a href="../relationShip">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Relationship</span>
								</a>
							</li>	                		               
							<!--<li>
								<a href="../passenger">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Passenger</span>
								</a>
							</li>-->
							<?php
							/*if($package==1 || $packageEdit==1){
								?>
								<li>
									<a href="../packages/index.php">
										<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
										<span class="side_nav_text">Packages</span>
									</a>
								</li>
								<?php }*/?>
							<li>
								<a href="../flight">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Flight</span>
								</a>
							</li>
							<li>
								<a href="../room_type">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Room Type</span>
								</a>
							</li>
							<li>
								<a href="../trans_provider">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Transportation Provider</span>
								</a>
							</li>
							<li>
								<a href="../vehicle_type">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Vehicle Type</span>
								</a>
							</li>
							<li>
								<a href="../hotel">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Hotel</span>
								</a>
							</li>
							<li>
								<a href="../misc_narration">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Misc Narration</span>
								</a>
							</li>
							<li>
								<a href="../visa_for">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Visa For</span>
								</a>
							</li>
<!--<li>
								<a href="../otherPurchase">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Other Purchase</span>
								</a>
							</li>-->
						</ul>
						
				</li>
						<!--
						<li>
							<a href="../flight">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Flight</span>
							</a>
						</li>
						<li>
							<a href="../room_type">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Room Type</span>
							</a>
						</li>
						<li>
							<a href="../vehicle_type">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Vehicle Type</span>
							</a>
						</li>
						<li>
							<a href="../misc_narration">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Misc Narration</span>
							</a>
						</li>
						<li>
							<a href="../visa_for">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Visa For</span>
							</a>
						</li>-->
					<!--</ul>-->
				</li>
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
						<span class="side_nav_text">Accounts</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../ac_group">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Master Group</span>
							</a>
						</li>
						<!--<li>
							<a href="../accounts">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Accounts</span>
							</a>
						</li>
						<li>
							<a href="../account_customer">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Customer</span>
							</a>
						</li>
						<li>
							<a href="../account_supplier">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Supplier</span>
							</a>
						</li>-->
					</ul>
				</li>
	<!---------admin package commented line.--------------------------------------------------->
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
						<span class="side_nav_text">Package</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<!--<li>
							<a href="../packages/index.php">
								<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
								<span class="side_nav_text">Create Package</span>
							</a>
						</li>
						<li>
							<a href="../invoice_packages/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Invoice</span>
							</a>
						</li>-->
						<li>
							<a href="../packages/index.php">
								<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
								<span class="side_nav_text">Create Package</span>
							</a>
						</li>
						<li>
							<a href="../package_report/package.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Status</span>
							</a>
						</li>
						<li>
							<a href="../package_report/package_passenger.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Passenger</span>
							</a>
						</li>
						
					</ul>
				</li>
	<!-------------end of  admin package commented line-------------------------------------->
				<!--<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Invoice</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../invoice_airline/">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Airline</span>
							</a>
						</li>
						<li>
							<a href="../invoice_hotel/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Hotel</span>
							</a>
						</li>
						<li>
							<a href="../invoice_visa/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Visa</span>
							</a>
						</li>
						<li>
							<a href="../invoice_misc/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Miscellaneous</span>
							</a>
						</li>
						<li>
							<a href="../invoice_transport/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Transportation</span>
							</a>
						</li>
						<li>
							<a href="../invoice_packages/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Packages</span>
							</a>
						</li>
					</ul>
				</li>-->
               
				<!--<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
						<span class="side_nav_text">Package</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../packages/index.php">
								<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
								<span class="side_nav_text">Create Package</span>
							</a>
						</li>
						<li>
							<a href="../invoice_packages/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Invoice</span>
							</a>
						</li>
						<li>
							<a href="../invoice_report/package.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Status</span>
							</a>
						</li>
						
					</ul>
				</li>-->
                
				<!--<li>
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-ios-minus"></i></span>
				<span class="side_nav_text">Purchase Return</span>
				</a>
				</li>-->
                
				<!--<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Cash</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../cash_receipt/">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Cash Receipt</span>
							</a>
						</li>
						<li>
							<a href="../cash_payment/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Cash Payment</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Bank</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../bank_payment/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Bank Payment</span>
							</a>
						</li>
						<li>
							<a href="../bank_receipt/">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Bank Receipt</span>
							</a>
						</li>
					</ul>
				</li>-->
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Account Reports</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../account_reports/account_report.php">
								<span class="side_nav_ico"><i class="ion ion-clipboard"></i></span>
								<span class="side_nav_text">Account Reports</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/profit_report.php">
								<span class="side_nav_ico"><i class="ion ion-clipboard"></i></span>
								<span class="side_nav_text">Profit Reports</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/payment_report.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
								<span class="side_nav_text">Payment Reports</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/receipt_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Receipt Reports</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/cash_book.php">
								<span class="side_nav_ico"><i class="ion ion-ios-book"></i></span>
								<span class="side_nav_text">Cash Book</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/day_book.php">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Day Book</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/payable_due.php">
								<span class="side_nav_ico"><i class="ion ion-ios-time"></i></span>
								<span class="side_nav_text">Payable Due Report</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/receivable_due.php">
								<span class="side_nav_ico"><i class="ion ion-ios-time"></i></span>
								<span class="side_nav_text">Receivable Due Report</span>
							</a>
						</li>
						<li>
						<a href="../account_reports/trial_balance.php">
						<span class="side_nav_ico"><i class="ion ion-ios-settings"></i></span>
						<span class="side_nav_text">Trial Balance</span>
						</a>
						</li>
                                                <li>
						<a href="../account_reports/profit_loss.php">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Profit &amp; Loss</span>
						</a>
						</li>
						<li>
						<a href="../account_reports/balance_sheet.php">
						<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
						<span class="side_nav_text">Balance Sheet</span>
						</a>
						</li>
					</ul>
				</li>
                 
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Invoice Reports</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../invoice_report/airline.php">
								<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
								<span class="side_nav_text">Airline Report</span>
							</a>
						</li>
						<li>
							<a href="../invoice_report/visa.php">
								<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
								<span class="side_nav_text">Visa Report</span>
							</a>
						</li>
						<li>
							<a href="../invoice_report/hotel.php">
								<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
								<span class="side_nav_text">Hotel Report</span>
							</a>
						</li>
						<li>
							<a href="../invoice_report/transport.php">
								<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
								<span class="side_nav_text">Transport Report</span>
							</a>
						</li>
						<li>
							<a href="../invoice_report/misc.php">
								<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
								<span class="side_nav_text">Misc Report</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has_child">
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Purchase Reports</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">						
						<li>
							<a href="../purchase/airline.php">
								<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
								<span class="side_nav_text">Airline</span>
							</a>
						</li>
						<li>
							<a href="../purchase/hotel.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Hotel</span>
							</a>
						</li>
						<li>
							<a href="../purchase/visa.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Visa</span>
							</a>
						</li>
						<li>
							<a href="../purchase/misc.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Misc.</span>
							</a>
						</li>
						<li>
							<a href="../purchase/transport.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Transport</span>
							</a>
						</li>
						<!--<li>
							<a href="../purchase/package.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package</span>
							</a>
						</li>	-->				
					</ul>
				</li>
            
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Other Reports</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../other_report/comp_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
								<span class="side_nav_text">Company Report</span>
							</a>
						</li>
						<li>
							<a href="../other_report/passenger_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
								<span class="side_nav_text">Passenger Report</span>
							</a>
						</li>
						
						<li>
							<a href="../other_report/agent.php">
								<span class="side_nav_ico"><i class="ion ion-ios-pricetags"></i></span>
								<span class="side_nav_text">Agent Report</span>
							</a>
						</li>
						<li>
							<a href="../other_report/flight_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-pricetags"></i></span>
								<span class="side_nav_text">Flight Report</span>
							</a>
						</li>
						<li>
							<a href="../other_report/roomtype_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-shuffle"></i></span>
								<span class="side_nav_text">Room Type Report</span>
							</a>
						</li>
						<li>
							<a href="../other_report/transprovider_report.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
								<span class="side_nav_text">Trans Provider Report</span>
							</a>
						</li>
						<li>
							<a href="../other_report/vehicletype_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Vehicle Type Report</span>
							</a>
						</li>
						<li>
							<a href="../other_report/hotel_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Hotel Report</span>
							</a>
						</li>  
						<li>
							<a href="../log_report/log-report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Activity Report</span>
							</a>
						</li>                      
					</ul>
				</li>
				<!-- <li class="has_child">
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
				<span class="side_nav_text">Staff Creation</span>
				<span class="side_nav_drop_ico"></span>
				</a>
				<ul class="side_sub_nav">
				<li>
				<a href="../user/index.php">
				<span class="side_nav_ico"><i class="ion ion-ios-personadd"></i></span>
				<span class="side_nav_text">Add Staff</span>
				</a>
				</li>
				<li>
				<a href="../userPermission/index.php">
				<span class="side_nav_ico"><i class="ion ion-ios-game-controller-a"></i></span>
				<span class="side_nav_text">Staff Permission</span>
				</a>
				</li>
				</ul>
				</li>-->
                
                
                
				<!--  <li>
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-arrow-swap"></i></span>
				<span class="side_nav_text">Account Transfer</span>
				</a>
				</li>
				<li>
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-clipboard"></i></span>
				<span class="side_nav_text">Journal Voucher</span>
				</a>
				</li>
				<li>
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
				<span class="side_nav_text">Customer Status</span>
				</a>
				</li>-->
<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Blocking</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../blocking/company.php">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Company</span>
							</a>
						</li>
						<li>
							<a href="../blocking/staff.php">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Staff</span>
							</a>
						</li>
					</ul>
				</li>

               <li>
							<a href="../backup">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Back Up</span>
							</a>
						</li>
			</ul>
			<?php }
           
		/* *****************************admin end****************************** */
			
           
		else if($_SESSION['travelType']=="Branch"){
			?>

			<ul class="side_nav">
				<li>
					<a href="../dashboard/">
						<span class="side_nav_ico"><i class="ion ion-ios-speedometer"></i></span>
						<span class="side_nav_text">Home</span>
					</a>
				</li>
				<li class="has_child">
					
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
							<span class="side_nav_text">Registration</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">	
							<li>
								<a href="../relationShip">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Relationship</span>
								</a>
							</li>	                		               
							<!--<li>
								<a href="../passenger">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Passenger</span>
								</a>
							</li>-->
							<?php
							/*if($package==1 || $packageEdit==1){
								?>
								<li>
									<a href="../packages/index.php">
										<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
										<span class="side_nav_text">Packages</span>
									</a>
								</li>
								<?php }*/?>
							<li>
								<a href="../flight">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Flight</span>
								</a>
							</li>
							<li>
								<a href="../room_type">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Room Type</span>
								</a>
							</li>
							<li>
								<a href="../trans_provider">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Transportation Provider</span>
								</a>
							</li>
							<li>
								<a href="../vehicle_type">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Vehicle Type</span>
								</a>
							</li>
							<li>
								<a href="../hotel">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Hotel</span>
								</a>
							</li>
							<li>
								<a href="../misc_narration">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Misc Narration</span>
								</a>
							</li>
							<li>
								<a href="../visa_for">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Visa For</span>
								</a>
							</li>
<!--<li>
								<a href="../otherPurchase">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Other Purchase</span>
								</a>
							</li>-->
						</ul>
						
				</li>
				
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
						<span class="side_nav_text">Accounts</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">						
						<li>
							<a href="../accounts">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Accounts</span>
							</a>
						</li>
						<li>
							<a href="../account_customer">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Customer</span>
							</a>
						</li>
						<li>
							<a href="../account_supplier">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Supplier</span>
							</a>
						</li>
<li>
							<a href="../account_agent">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Agent</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has_child">
					<a href="#" >
						<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
						<span class="side_nav_text">Staff Creation</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../user/index.php">
								<span class="side_nav_ico"><i class="ion ion-ios-personadd"></i></span>
								<span class="side_nav_text">Add Staff</span>
							</a>
						</li>
						
						<li>
							<a href="../userPermission/index.php">
								<span class="side_nav_ico"><i class="ion ion-ios-game-controller-a"></i></span>
								<span class="side_nav_text">Staff Permission</span>
							</a>
						</li>
						<li>
							<a href="../user/staff_works.php">
								<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
								<span class="side_nav_text">Staff Works Report</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
						<span class="side_nav_text">Package</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<!--<li>
							<a href="../packages/index.php">
								<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
								<span class="side_nav_text">Create Package</span>
							</a>
						</li>
						<li>
							<a href="../invoice_packages/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Invoice</span>
							</a>
						</li>-->
						<li>
							<a href="../packages/index.php">
								<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
								<span class="side_nav_text">Create Package</span>
							</a>
						</li>
						<li>
							<a href="../package_report/package.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Status</span>
							</a>
						</li>
						
					</ul>
				</li>
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Invoice</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../invoice_airline/">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Airline</span>
							</a>
						</li>
						<li>
							<a href="../invoice_hotel/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Hotel</span>
							</a>
						</li>
						<li>
							<a href="../invoice_visa/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Visa</span>
							</a>
						</li>
						<li>
							<a href="../invoice_misc/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Miscellaneous</span>
							</a>
						</li>
						<li>
							<a href="../invoice_transport/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Transportation</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has_child">
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Payment</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">
							
							<li>
								<a href="../cash_payment/">
									<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
									<span class="side_nav_text">Cash Payment</span>
								</a>
							</li>
							<li>
								<a href="../bank_payment/">
									<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
									<span class="side_nav_text">Bank Payment</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="has_child">
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Receipt</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">
							
							<li>
								<a href="../cash_receipt/">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Cash Receipt</span>
								</a>
							</li>
							<li>
								<a href="../bank_receipt/">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Bank Receipt</span>
								</a>
							</li>
							<li>
								<a href="../advance_receipt/">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Advance Receipt</span>
								</a>
							</li>
						</ul>
					</li>
					<li>
							<a href="../accountTransfer">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Account Transfer</span>
							</a>
					</li>				
				<!--<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
						<span class="side_nav_text">Registration</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">	               
						
						<li>
							<a href="../passenger">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Passenger</span>
							</a>
						</li>
						
						<li>
							<a href="../flight">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Flight</span>
							</a>
						</li>
						<li>
							<a href="../room_type">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Room Type</span>
							</a>
						</li>
						<li>
							<a href="../trans_provider">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Transportation Provider</span>
							</a>
						</li>
						<li>
							<a href="../vehicle_type">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Vehicle Type</span>
							</a>
						</li>
						<li>
							<a href="../hotel">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Hotel</span>
							</a>
						</li>
						<li>
							<a href="../misc_narration">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Misc Narration</span>
							</a>
						</li>
						<li>
							<a href="../visa_for">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Visa For</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
						<span class="side_nav_text">Accounts</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../ac_group">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Master Group</span>
							</a>
						</li>
						<li>
							<a href="../accounts">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Accounts</span>
							</a>
						</li>
						<li>
							<a href="../account_customer">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Customer</span>
							</a>
						</li>
						<li>
							<a href="../account_supplier">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Supplier</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Invoice</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../invoice_airline/">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Airline</span>
							</a>
						</li>
						<li>
							<a href="../invoice_hotel/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Hotel</span>
							</a>
						</li>
						<li>
							<a href="../invoice_visa/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Visa</span>
							</a>
						</li>
						<li>
							<a href="../invoice_misc/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Miscellaneous</span>
							</a>
						</li>
						<li>
							<a href="../invoice_transport/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Transportation</span>
							</a>
						</li>
					</ul>
				</li>
               
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
						<span class="side_nav_text">Package</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../packages/index.php">
								<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
								<span class="side_nav_text">Create Package</span>
							</a>
						</li>
						<li>
							<a href="../invoice_packages/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Invoice</span>
							</a>
						</li>
						<li>
							<a href="../invoice_report/package.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Status</span>
							</a>
						</li>
						
					</ul>
				</li>-->	 
                
				<!--<li>
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-ios-minus"></i></span>
				<span class="side_nav_text">Purchase Return</span>
				</a>
				</li>-->
                
				<!--<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Cash</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../cash_receipt/">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Cash Receipt</span>
							</a>
						</li>
						<li>
							<a href="../cash_payment/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Cash Payment</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Bank</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../bank_payment/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Bank Payment</span>
							</a>
						</li>
						<li>
							<a href="../bank_receipt/">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Bank Receipt</span>
							</a>
						</li>
					</ul>
				</li>-->
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Account Reports</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../account_reports/account_report.php">
								<span class="side_nav_ico"><i class="ion ion-clipboard"></i></span>
								<span class="side_nav_text">Account Reports</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/profit_report.php">
								<span class="side_nav_ico"><i class="ion ion-clipboard"></i></span>
								<span class="side_nav_text">Profit Reports</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/payment_report.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
								<span class="side_nav_text">Payment Reports</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/receipt_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Receipt Reports</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/cash_book.php">
								<span class="side_nav_ico"><i class="ion ion-ios-book"></i></span>
								<span class="side_nav_text">Cash Book</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/day_book.php">
								<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
								<span class="side_nav_text">Day Book</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/payable_due.php">
								<span class="side_nav_ico"><i class="ion ion-ios-time"></i></span>
								<span class="side_nav_text">Payable Due Report</span>
							</a>
						</li>
						<li>
							<a href="../account_reports/receivable_due.php">
								<span class="side_nav_ico"><i class="ion ion-ios-time"></i></span>
								<span class="side_nav_text">Receivable Due Report</span>
							</a>
						</li>
<li>
						<a href="../account_reports/trial_balance.php">
						<span class="side_nav_ico"><i class="ion ion-ios-settings"></i></span>
						<span class="side_nav_text">Trial Balance</span>
						</a>
						</li>
                                                <li>
						<a href="../account_reports/profit_loss.php">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Profit &amp; Loss</span>
						</a>
						</li>
						<li>
						<a href="../account_reports/balance_sheet.php">
						<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
						<span class="side_nav_text">Balance Sheet</span>
						</a>
						</li>
						<!--<li>
						<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-settings"></i></span>
						<span class="side_nav_text">Trial Balance</span>
						</a>
						</li>
						<li>
						<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
						<span class="side_nav_text">Balance Sheet</span>
						</a>
						</li>
						<li>
						<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Profit &amp; Loss</span>
						</a>
						</li>-->
					</ul>
				</li>
                 
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Invoice Reports</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						<li>
							<a href="../invoice_report/airline.php">
								<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
								<span class="side_nav_text">Airline Report</span>
							</a>
						</li>
						<li>
							<a href="../invoice_report/visa.php">
								<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
								<span class="side_nav_text">Visa Report</span>
							</a>
						</li>
						<li>
							<a href="../invoice_report/hotel.php">
								<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
								<span class="side_nav_text">Hotel Report</span>
							</a>
						</li>
						<li>
							<a href="../invoice_report/transport.php">
								<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
								<span class="side_nav_text">Transport Report</span>
							</a>
						</li>
						<li>
							<a href="../invoice_report/misc.php">
								<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
								<span class="side_nav_text">Misc Report</span>
							</a>
						</li>
					</ul>
				</li>
            	<li class="has_child">
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Purchase Reports</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">						
						<li>
							<a href="../purchase/airline.php">
								<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
								<span class="side_nav_text">Airline</span>
							</a>
						</li>
						<li>
							<a href="../purchase/hotel.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Hotel</span>
							</a>
						</li>
						<li>
							<a href="../purchase/visa.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Visa</span>
							</a>
						</li>
						<li>
							<a href="../purchase/misc.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Misc.</span>
							</a>
						</li>
						<li>
							<a href="../purchase/transport.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Transport</span>
							</a>
						</li>
						<!--<li>
							<a href="../purchase/package.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package</span>
							</a>
						</li>	-->				
					</ul>
				</li>
				<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Other Reports</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
						
						<li>
							<a href="../other_report/passenger_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
								<span class="side_nav_text">Passenger Report</span>
							</a>
						</li>
						
						<li>
							<a href="../other_report/agent.php">
								<span class="side_nav_ico"><i class="ion ion-ios-pricetags"></i></span>
								<span class="side_nav_text">Agent Report</span>
							</a>
						</li>
						
						<li>
							<a href="../other_report/flight_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-pricetags"></i></span>
								<span class="side_nav_text">Flight Report</span>
							</a>
						</li>
						<li>
							<a href="../other_report/roomtype_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-shuffle"></i></span>
								<span class="side_nav_text">Room Type Report</span>
							</a>
						</li>
						<li>
							<a href="../other_report/transprovider_report.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
								<span class="side_nav_text">Trans Provider Report</span>
							</a>
						</li>
						<li>
							<a href="../other_report/vehicletype_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Vehicle Type Report</span>
							</a>
						</li>
						<li>
							<a href="../other_report/hotel_report.php">
								<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
								<span class="side_nav_text">Hotel Report</span>
							</a>
						</li> 
						                      
					</ul>
				</li>
				
				
				 
                
                
                
				<!--  <li>
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-arrow-swap"></i></span>
				<span class="side_nav_text">Account Transfer</span>
				</a>
				</li>
				<li>
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-clipboard"></i></span>
				<span class="side_nav_text">Journal Voucher</span>
				</a>
				</li>
				<li>
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
				<span class="side_nav_text">Customer Status</span>
				</a>
				</li>-->
               
			</ul>
			<?php }
           
		/* *****************************branch end****************************** */
          
		if($_SESSION['travelType']=="Staff"){
				
			?>

			<ul class="side_nav">
				<li>
					<a href="../dashboard/">
						<span class="side_nav_ico"><i class="ion ion-ios-speedometer"></i></span>
						<span class="side_nav_text">Home</span>
					</a>
				</li>
				<li class="has_child">
					<?php
					if($basicSetting==1){			
						?>
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
							<span class="side_nav_text">Registration</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">	
							<li>
								<a href="../relationShip">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Relationship</span>
								</a>
							</li>	                		               
							<li>
								<a href="../passenger">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Passenger</span>
								</a>
							</li>
							<?php
							/*if($package==1 || $packageEdit==1){
								?>
								<li>
									<a href="../packages/index.php">
										<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
										<span class="side_nav_text">Packages</span>
									</a>
								</li>
								<?php }*/?>
							<li>
								<a href="../flight">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Flight</span>
								</a>
							</li>
							<li>
								<a href="../room_type">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Room Type</span>
								</a>
							</li>
							<li>
								<a href="../trans_provider">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Transportation Provider</span>
								</a>
							</li>
							<li>
								<a href="../vehicle_type">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Vehicle Type</span>
								</a>
							</li>
							<li>
								<a href="../hotel">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Hotel</span>
								</a>
							</li>
							<li>
								<a href="../misc_narration">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Misc Narration</span>
								</a>
							</li>
							<li>
								<a href="../visa_for">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Visa For</span>
								</a>
							</li>
<li>
								<a href="../otherPurchase">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Other Purchase</span>
								</a>
							</li>
						</ul>
						<?php }?>
				</li>
				<?php
                
				if($account==1){			
					?>
					<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
						<span class="side_nav_text">Accounts</span>
						<span class="side_nav_drop_ico"></span>
					</a>
				
					<ul class="side_sub_nav">
		                
						<li>
							<a href="../accounts">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Accounts</span>
							</a>
						</li>
						<li>
							<a href="../account_customer">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Customer</span>
							</a>
						</li>
						<li>
							<a href="../account_supplier">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Supplier</span>
							</a>
						</li>
<li>
							<a href="../account_agent">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Agent</span>
							</a>
						</li>
						
					</ul>
					<?php }?>
				</li>
                                <li>
					<a href="../journal/">
						<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
						<span class="side_nav_text">Journal</span>
					</a>
				</li>
				<?php  
	           
				if($invoiceAirline==1	|| $invoiceAirlineEdit==1 || $invoiceAirlineDelete==1	|| $invoiceHotel==1 || $invoiceHotelEdit==1 || $invoiceHotelDelete==1 || $invoiceVisa==1	|| $invoiceVisaEdit==1 || $invoiceVisaDelete==1	|| $invoiceMisc==1	|| $invoiceMiscEdit==1 || $invoiceMiscDelete==1	|| $invoiceTransport==1 || $invoiceTransportEdit==1 || $invoiceTransportDelete==1){			
					?>
					<li class="has_child">
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Invoice</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">
							<?php
							if($invoiceAirline	==1	|| $invoiceAirlineEdit==1 || $invoiceAirlineDelete==1){
								?>
								<li>
									<a href="../invoice_airline/">
										<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
										<span class="side_nav_text">Airline</span>
									</a>
								</li>
								<?php
							}
							if($invoiceHotel==1 || $invoiceHotelEdit==1 || $invoiceHotelDelete==1){
								?>
								<li>
									<a href="../invoice_hotel/">
										<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
										<span class="side_nav_text">Hotel</span>
									</a>
								</li>
								<?php
							}
							if($invoiceVisa==1	|| $invoiceVisaEdit==1 || $invoiceVisaDelete==1){
								?>
								<li>
									<a href="../invoice_visa/">
										<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
										<span class="side_nav_text">Visa</span>
									</a>
								</li>
								<?php
							}
							if($invoiceMisc==1	|| $invoiceMiscEdit==1 || $invoiceMiscDelete==1){
								?>
								<li>
									<a href="../invoice_misc/">
										<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
										<span class="side_nav_text">Miscellaneous</span>
									</a>
								</li>
								<?php
							}
							if($invoiceTransport==1 || $invoiceTransportEdit==1 || $invoiceTransportDelete==1){
								?>
								<li>
									<a href="../invoice_transport/">
										<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
										<span class="side_nav_text">Transportation</span>
									</a>
								</li>
								<?php
							}
							/*if($invoicePackage==1	|| $invoicePackageEdit==1){
								?>
								<li>
									<a href="../invoice_packages/">
										<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
										<span class="side_nav_text">Packages</span>
									</a>
								</li>
								<?php
							}*/                    
							?>
						</ul>
					</li>
					<?php } 
	           
				if($package==1 || $packageEdit	==1  || $packageDelete==1 || $invoicePackage==1 || 
					$invoicePackageEdit==1 || $invoicePackageDelete==1){			
					?>
					<li class="has_child">
					<a href="#">
						<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
						<span class="side_nav_text">Package</span>
						<span class="side_nav_drop_ico"></span>
					</a>
					<ul class="side_sub_nav">
					
					<?php
							
							if($invoicePackage==1 || $invoicePackageEdit==1 || $invoicePackageDelete==1){	
						?>
						<li>
							<a href="../invoice_packages/">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Invoice</span>
							</a>
						</li>
						<?php 
						}
						
						?>
						<li>
							<a href="../package_report/package.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Status</span>
							</a>
						</li>
						<li>
							<a href="../package_report/package_passenger.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package Passenger Report</span>
							</a>
						</li>
						
					</ul>
				</li>
                
					
					<?php } 
	               
				if($cashBank==1 || $cashBankEdit==1 || $cashBankDelete==1){			
					?>
					<li class="has_child">
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Payment</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">
							
							<li>
								<a href="../cash_payment/">
									<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
									<span class="side_nav_text">Cash Payment</span>
								</a>
							</li>
							<li>
								<a href="../bank_payment/">
									<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
									<span class="side_nav_text">Bank Payment</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="has_child">
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Receipt</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">
							
							<li>
								<a href="../cash_receipt/">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Cash Receipt</span>
								</a>
							</li>
							<li>
								<a href="../bank_receipt/">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Bank Receipt</span>
								</a>
							</li>

							<li>
								<a href="../advance_receipt/">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Advance Receipt</span>
								</a>
							</li>
						</ul>
					</li>
					<li>
							<a href="../accountTransfer">
								<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
								<span class="side_nav_text">Account Transfer</span>
							</a>
					</li>
					<?php } 
	              
				if($accountReport==1){			
					?>
					<li class="has_child">
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Account Reports</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">
							<li>
								<a href="../account_reports/account_report.php">
									<span class="side_nav_ico"><i class="ion ion-clipboard"></i></span>
									<span class="side_nav_text">Account Reports</span>
								</a>
							</li>
							<li>
							<a href="../account_reports/profit_report.php">
								<span class="side_nav_ico"><i class="ion ion-clipboard"></i></span>
								<span class="side_nav_text">Profit Reports</span>
							</a>
						</li>
							<li>
								<a href="../account_reports/payment_report.php">
									<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
									<span class="side_nav_text">Payment Reports</span>
								</a>
							</li>
							<li>
								<a href="../account_reports/receipt_report.php">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Receipt Reports</span>
								</a>
							</li>
							<li>
								<a href="../account_reports/cash_book.php">
									<span class="side_nav_ico"><i class="ion ion-ios-book"></i></span>
									<span class="side_nav_text">Cash Book</span>
								</a>
							</li>
							<li>
								<a href="../account_reports/day_book.php">
									<span class="side_nav_ico"><i class="ion ion-ios-paper"></i></span>
									<span class="side_nav_text">Day Book</span>
								</a>
							</li>
							<li>
								<a href="../account_reports/payable_due.php">
									<span class="side_nav_ico"><i class="ion ion-ios-time"></i></span>
									<span class="side_nav_text">Payable Due Report</span>
								</a>
							</li>
							<li>
								<a href="../account_reports/receivable_due.php">
									<span class="side_nav_ico"><i class="ion ion-ios-time"></i></span>
									<span class="side_nav_text">Receivable Due Report</span>
								</a>
							</li>
<li>
						<a href="../account_reports/trial_balance.php">
						<span class="side_nav_ico"><i class="ion ion-ios-settings"></i></span>
						<span class="side_nav_text">Trial Balance</span>
						</a>
						</li>
                                                <li>
						<a href="../account_reports/profit_loss.php">
						<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
						<span class="side_nav_text">Profit &amp; Loss</span>
						</a>
						</li>
						<li>
							<a href="../account_reports/balance_sheet.php">
							<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
							<span class="side_nav_text">Balance Sheet</span>
							</a>
						</li>
							<!--<li>
							<a href="#">
							<span class="side_nav_ico"><i class="ion ion-ios-settings"></i></span>
							<span class="side_nav_text">Trial Balance</span>
							</a>
							</li>
							<li>
							<a href="#">
							<span class="side_nav_ico"><i class="ion ion-ios-list"></i></span>
							<span class="side_nav_text">Balance Sheet</span>
							</a>
							</li>
							<li>
							<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Profit &amp; Loss</span>
							</a>
							</li>--> 
						</ul>
					</li>
					<?php } 
	               
				if($invoiceAirline==1	|| $invoiceAirlineEdit==1 || $invoiceAirlineDelete==1 || $invoiceHotel==1 || $invoiceHotelEdit==1 || $invoiceHotelDelete==1 || $invoiceVisa==1	|| $invoiceVisaEdit==1 || $invoiceVisaDelete==1	|| $invoiceMisc==1	|| $invoiceMiscEdit==1 || $invoiceMiscDelete==1	|| $invoiceTransport==1 || $invoiceTransportEdit==1 || $invoiceTransportDelete==1){			
					?>
					<li class="has_child">
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Invoice Reports</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">
							<?php
							if($invoiceAirline==1	|| $invoiceAirlineEdit==1 || $invoiceAirlineDelete==1){
								?>
								<li>
									<a href="../invoice_report/airline.php">
										<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
										<span class="side_nav_text">Airline Report</span>
									</a>
								</li>
								<?php
							}
							if($invoiceHotel==1 || $invoiceHotelEdit==1 || $invoiceHotelDelete==1){
								?>
					 	
								<li>
									<a href="../invoice_report/hotel.php">
										<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
										<span class="side_nav_text">Hotel Report</span>
									</a>
								</li>
                        
								<?php
							}
							if($invoiceVisa==1	|| $invoiceVisaEdit==1 || $invoiceVisaDelete==1){
								?>
								<li>
									<a href="../invoice_report/visa.php">
										<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
										<span class="side_nav_text">Visa Report</span>
									</a>
								</li>
								<?php
							}
							if($invoiceMisc==1	|| $invoiceMiscEdit==1 || $invoiceMiscDelete==1){
								?>
								<li>
									<a href="../invoice_report/misc.php">
										<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
										<span class="side_nav_text">Misc. Report</span>
									</a>
								</li>
								<?php
							}     
							if($invoiceTransport==1 || $invoiceTransportEdit==1 || $invoiceTransportDelete==1){
								?>
								<li>
									<a href="../invoice_report/transport.php">
										<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
										<span class="side_nav_text">Transport Report</span>
									</a>
								</li>
								<?php
							}
                                    
							?>
						</ul>
					</li>
					<?php } 
					
	               if($purchaseAirline==1 || $purchaseAirlineEdit==1 || $purchaseHotel==1 || $purchaseHotelEdit==1|| $purchaseVisa==1 || $purchaseVisaEdit==1 || $purchaseMisc==1 || $purchaseMiscEdit==1 || $purchaseTransport==1 || $purchaseTransportEdit==1){	
	               //|| $purchasePackage==1 || $purchasePackageEdit==1		
					?>
					<li class="has_child">
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Purchase Reports</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">
							<?php
							if($purchaseAirline==1 || $purchaseAirlineEdit==1){
								
					?>
						<li>
							<a href="../purchase/airline.php">
								<span class="side_nav_ico"><i class="ion ion-ios-browsers"></i></span>
								<span class="side_nav_text">Airline</span>
							</a>
						</li>
						<?php 
							}
							if($purchaseHotel==1 || $purchaseHotelEdit==1){	
						?>
						<li>
							<a href="../purchase/hotel.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Hotel</span>
							</a>
						</li>
						<?php 
						}
							if($purchaseVisa==1 || $purchaseVisaEdit==1){
								
						?>
						<li>
							<a href="../purchase/visa.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Visa</span>
							</a>
						</li>
						<?php }
						if($purchaseMisc==1 || $purchaseMiscEdit==1){
								
						?>
						<li>
							<a href="../purchase/misc.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Misc.</span>
							</a>
						</li>
						<?php }
						if($purchaseTransport==1 || $purchaseTransportEdit==1){
								
						?>
						<li>
							<a href="../purchase/transport.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Transport</span>
							</a>
						</li>
						<?php }
						/*if($purchasePackage==1 || $purchasePackageEdit==1){
								
						?>
						<li>
							<a href="../purchase/package.php">
								<span class="side_nav_ico"><i class="ion ion-social-usd-outline"></i></span>
								<span class="side_nav_text">Package</span>
							</a>
						</li>
						<?php }*/?>
					</ul>
				</li>
                
					<!--<li>
					<a href="#">
					<span class="side_nav_ico"><i class="ion ion-ios-minus"></i></span>
					<span class="side_nav_text">Purchase Return</span>
					</a>
					</li>-->
			<?php } 
	               
	                 /* *********************************************/
					
	               
				if($otherReport==1){			
					?>
					<li class="has_child">
						<a href="#">
							<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
							<span class="side_nav_text">Other Reports</span>
							<span class="side_nav_drop_ico"></span>
						</a>
						<ul class="side_sub_nav">
							<li>
								<a href="../other_report/comp_report.php">
									<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
									<span class="side_nav_text">Company Report</span>
								</a>
							</li>
							<li>
								<a href="../other_report/passenger_report.php">
									<span class="side_nav_ico"><i class="ion ion-ios-cart"></i></span>
									<span class="side_nav_text">Passenger Report</span>
								</a>
							</li>
							
							<li>
								<a href="../other_report/agent.php">
									<span class="side_nav_ico"><i class="ion ion-ios-pricetags"></i></span>
									<span class="side_nav_text">Agent Report</span>
								</a>
							</li>
							<li>
								<a href="../other_report/flight_report.php">
									<span class="side_nav_ico"><i class="ion ion-ios-pricetags"></i></span>
									<span class="side_nav_text">Flight Report</span>
								</a>
							</li>
							<li>
								<a href="../other_report/roomtype_report.php">
									<span class="side_nav_ico"><i class="ion ion-ios-shuffle"></i></span>
									<span class="side_nav_text">Room Type Report</span>
								</a>
							</li>
							<li>
								<a href="../other_report/transprovider_report.php">
									<span class="side_nav_ico"><i class="ion ion-social-usd"></i></span>
									<span class="side_nav_text">Trans Provider Report</span>
								</a>
							</li>
							<li>
								<a href="../other_report/vehicletype_report.php">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Vehicle Type Report</span>
								</a>
							</li>
							<li>
								<a href="../other_report/hotel_report.php">
									<span class="side_nav_ico"><i class="ion ion-ios-compose"></i></span>
									<span class="side_nav_text">Hotel Report</span>
								</a>
							</li>                        
						</ul>
					</li>
					<?php } 
				?>
				<!--  <li>
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-arrow-swap"></i></span>
				<span class="side_nav_text">Account Transfer</span>
				</a>
				</li>
				<li>
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-clipboard"></i></span>
				<span class="side_nav_text">Journal Voucher</span>
				</a>
				</li>
				<li>
				<a href="#">
				<span class="side_nav_ico"><i class="ion ion-ios-people"></i></span>
				<span class="side_nav_text">Customer Status</span>
				</a>
				</li>-->
               
			</ul>
			<?php }
           
		/* *****************************staff end****************************** */
		?>
	</div>
</div>
<div class="wrapper give_way">
<div class="header">
	<div class="container-fluid">
		<div class="nav_toggler">
			<span class="ion ion-android-menu"></span>
		</div>
		<div class="header_nav">
			<ul class="header_main_nav">
				<?php
				if($account==1 or $travelType=='Branch'){			
					?>
					<li><a href="../account_customer">Customer</a></li>
					<li><a href="../account_supplier">Supplier</a></li>
					<?php } 
				if($invoiceAirline==1	|| $invoiceAirlineEdit==1 || $invoiceAirlineDelete==1 || $invoiceHotel==1 || $invoiceHotelEdit==1 || $invoiceHotelDelete==1 || $invoiceVisa==1	|| $invoiceVisaEdit==1 || $invoiceVisaDelete==1	|| $invoiceMisc==1	|| $invoiceMiscEdit==1 || $invoiceMiscDelete==1	|| $invoiceTransport==1 || $invoiceTransportEdit==1 || $invoiceTransportDelete==1){
					
					?>
					<li class="has_child">                            
						<a href="#">                                
							<span class="nav_text">Sales</span>                                
							<span class="drop_ico"><i class="ion ion-ios-arrow-down"></i></span>
							<div class="bd_clear"></div>
						</a> 
						<ul class="header_sub_nav">
							<?php
							if($invoiceAirline	==1	|| $invoiceAirlineEdit==1 || $invoiceAirlineDelete==1){
								?>
								<li><a href="../invoice_airline">Airline</a></li>
								<?php
							}
							if($invoiceHotel==1 || $invoiceHotelEdit==1 || $invoiceHotelDelete==1){
								?>
								<li><a href="../invoice_hotel">Hotel</a></li>
								<?php
							}
							if($invoiceVisa==1	|| $invoiceVisaEdit==1 || $invoiceVisaDelete==1){
								?>
								<li><a href="../invoice_visa">Visa</a></li>
								<?php
							}
							if($invoiceMisc==1	|| $invoiceMiscEdit==1 || $invoiceMiscDelete==1){
								?>
								<li><a href="../invoice_misc">Misalliance</a></li>
								<?php
							}
							if($invoiceTransport==1 || $invoiceTransportEdit==1 || $invoiceTransportDelete==1){
								?>
								<li><a href="../invoice_transport">Transportation</a></li>
								<?php
							}
							/*if($invoicePackage==1	|| $invoicePackageEdit==1){
								?>
								<li><a href="../invoice_packages">Packages</a></li>
								<?php
							}*/
							?>
						</ul>
					</li>
					<?php } 
				if($cashBank==1	|| $cashBankEdit==1 || $cashBankDelete==1){  
					?>
					<li class="has_child">                            
						<a href="#">                                
							<span class="nav_text">Payment</span>                                
							<span class="drop_ico"><i class="ion ion-ios-arrow-down"></i></span>
							<div class="bd_clear"></div>
						</a> 
						<ul class="header_sub_nav">
							<li><a href="../cash_payment">Cash Payment</a></li>
							<li><a href="../bank_payment">Bank Payment</a></li>
                               
						</ul>
					</li>
					<li class="has_child">                            
						<a href="#">                                
							<span class="nav_text">Receipt</span>                                
							<span class="drop_ico"><i class="ion ion-ios-arrow-down"></i></span>
							<div class="bd_clear"></div>
						</a> 
						<ul class="header_sub_nav">
							<li><a href="../cash_receipt">Cash Receipt</a></li>
							<li><a href="../bank_receipt">Bank Receipt</a></li>
                               
						</ul>
					</li>
					<?php } 
				if($package==1	|| $packageEdit==1 || $packageDelete==1 || $invoicePackage==1 || $invoicePackageEdit==1 || $invoicePackageDelete==1  || $travelType=='Branch' || $travelType=='Admin'){
					
					?>   
					<li class="has_child">                            
						<a href="#">                                
							<span class="nav_text">Package</span>                                
							<span class="drop_ico"><i class="ion ion-ios-arrow-down"></i></span>
							<div class="bd_clear"></div>
						</a> 
						<ul class="header_sub_nav">
							
								<?php
							if($invoicePackage==1 || $invoicePackageEdit==1 || $invoicePackageDelete==1){
								?>
						
								<li><a href="../invoice_packages">Package Invoice</a></li>
								<?php
							}
							if( $travelType=='Branch' || $travelType=='Admin'){
								?>
								<li><a href="../packages">Create Package</a></li>
								
								<?php } ?> 
								<li><a href="../package_report/package.php">Package Status</a></li>   
								<?php
								if($travelType=='Admin' || $travelType=='Staff'){
								?>
								<li><a href="../package_report/package_passenger.php">Package Passenger</a></li>
								
								<?php } ?> 
						</ul>
					</li>
					<?php } 
				if($invoiceVisa==1	|| $invoiceVisaEdit==1 || $invoiceVisaDelete==1){?>   
					<li class="has_child">                            
						<a href="#">                                
							<span class="nav_text">Visa</span>                                
							<span class="drop_ico"><i class="ion ion-ios-arrow-down"></i></span>
							<div class="bd_clear"></div>
						</a> 
						<ul class="header_sub_nav">
							<li><a href="../invoice_visa/">Visa Invoice</a></li>
							<li><a href="../invoice_report/visa_status.php">Visa Status</a></li>
                               
						</ul>
					</li>
					<?php } ?>
			</ul>
		</div>
		<div class="header_user_controls">
			<ul class="header_main_nav">
				<li class="has_child">
					<a href="#">
						<?php if($_SESSION['travelType']=="Branch") { 
								$id = $_SESSION['travelUserId']; 
								
								$qry = "SELECT * FROM ".TABLE_COMPANY." where ID='$id'";
								$rows = $db->query($qry);		
								$res=mysql_fetch_array($rows);
								$name=$res['companyName'];
								}
								else if($_SESSION['travelType']=="Staff"){
									$id = $_SESSION['travelUserId']; 
									$qry = "SELECT * FROM ".TABLE_STAFF." where ID='$id'";
									$rows = $db->query($qry);		
									$res=mysql_fetch_array($rows);
									$name=$res['staffName'];
								}
								
							 
							
						?>
                        <span class="nav_text"><?php if($_SESSION['travelType']=="Admin") {echo "Admin"; }  else { echo $name; } ?></span>
                        
                        <span class="drop_ico"><i class="ion ion-ios-arrow-down"></i></span>
                        <div class="bd_clear"></div>
                    </a>
					<ul class="header_sub_nav">
<?php if($_SESSION['travelType']=="Admin" || $_SESSION['travelType']=="Branch") { ?>
						<li><a href="../resetPassword">Reset Password</a></li>
						<?php } ?>
						<li><a href="../changePassword">Change Password</a></li>
						<li><a href="../../logout.php">Log Out</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="bd_clear"></div>
	</div>
</div>
<div class="container-fluid">