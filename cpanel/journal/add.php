<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$proId		=	$_SESSION['proId'];
$loginId	=	$_SESSION['travelId'];
$finYear	=	$_SESSION['finYear'];


?>
<script>
	function setType(type)
	{
		if(type=='Debit')
		{
			document.getElementById('accTypeSecnd').value='Credit';
		}
		if(type=='Credit')
		{
			document.getElementById('accTypeSecnd').value='Debit';
		}
	}
	function setAmount(amount)
	{
		document.getElementById('amountSecnd').value = amount;
	}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form">
                <div class="bd_panel_head">
                    <h3>JOURNAL VOUCHER</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                    	<div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Voucher No</label>
                                <input type="text" name="invoiceNo" value="<?php echo "JN" . $db->nextVoucher(TABLE_TRANSACTION, 'voucherNo'," proId=$proId and voucherType='Journal' and finYear=$finYear"); ?>" required="" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="form_block_section_head">
                            <label>From</label>
                        </div>

                    </div>
                    <div class="row">
                    	<div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account<span class="valid">*</span></label>
                                <div class="aj_rel_box aj_account_to">
                                    <select name="accFirst" id="accFirst">
                                    	<option value=" ">Select</option> 
                                    	<?php 
                                    	$accountQry	=	mysql_query("SELECT ID,accountName FROM ".TABLE_ACCOUNTS." WHERE (proId=$proId and finYear=$finYear) or proId=0");
                                    	while($accRow = mysql_fetch_array($accountQry)){?>
                                    	<option value="<?php echo $accRow['ID'];?>" ><?php echo $accRow['accountName'];?></option>
                                    	<?php }?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Credit/Debit <span class="valid">*</span></label>
                               	<select name="accTypeFirst" id="accTypeFirst" required="" onchange="setType(this.value);">
                               		<option>Credit</option>
                               		<option>Debit</option>
                               	</select>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Amount <span class="valid">*</span></label>
                               	<input type="text" name="amountFirst" id="amountFirst" required="" onkeyup="setAmount(this.value);" autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="form_block_section_head">
                            <label>To</label>
                        </div>

                    </div>
                     <div class="row">
                    	<div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Account<span class="valid">*</span></label>
                                <div class="aj_rel_box aj_account_to">
                                    <select name="accSecnd" id="accSecnd">
                                    	<option value=" ">Select</option>  
                                    	<?php 
                                    	$accountQry	=	mysql_query("SELECT ID,accountName FROM ".TABLE_ACCOUNTS." WHERE (proId=$proId and finYear=$finYear) or proId=0");
                                    	while($accRow = mysql_fetch_array($accountQry)){?>
                                    	<option value="<?php echo $accRow['ID'];?>" ><?php echo $accRow['accountName'];?></option>
                                    	<?php }?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Credit/Debit <span class="valid">*</span></label>
                               	<select name="accTypeSecnd" id="accTypeSecnd" required="" disabled="true">
                               		<option>Debit</option>
                               		<option>Credit</option>
                               	</select>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Amount <span class="valid">*</span></label>
                               	<input type="text" name="amountSecnd" id="amountSecnd" required="" readonly=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
