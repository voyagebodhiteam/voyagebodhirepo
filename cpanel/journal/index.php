<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}
$loginType 	=	$_SESSION['travelType'];
$finYear = $_SESSION['finYear'];
if($_SESSION['travelType']=='Admin')
{
	$check	=	" finYear='$finYear'";
}
else
{

	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	" a.proId='$proId' and a.finYear='$finYear'";
}

$type = $_SESSION['travelType'];

//for edit and delete permission
$logID = $_SESSION['travelId'];

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
//delete row in index page

function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Journal Voucher</h3>
                </div>
                <div class="page_controls">                    
                	<form method="post">
                        <?php if($loginType	=='Admin'){?>
						<div class="page_search">
							<select name="company" required="" >
								<option value="">Select Company</option>
								<?php $cmpQry	=	mysql_query("SELECT * FROM ".TABLE_COMPANY." where finYear=$finYear");
								while($cmpRow		=	mysql_fetch_array($cmpQry))
								{
								
								?>
									<option value="<?php echo $cmpRow['ID'];?>"  <?php if(@$_REQUEST['company']==$cmpRow['ID']){ echo "selected";}?>><?php echo $cmpRow['companyName'];?></option>
								<?php 	
								}?>
							</select>
						</div>
						<?php }
						?>
							
						<div class="page_search">
							<input  type="search" name="from" placeholder="From" class="user_date" value="<?php echo @$_REQUEST['from'];?>" required="" >
						</div>
						<div class="page_search" >                      
							<input type="search" name="to" placeholder="To" class="user_date" value="<?php echo @$_REQUEST['to'];?>" required="">                        
						</div>
						<div class="page_search">                      
							<button type="submit" name="submit"><i class="ion ion-android-search"></i></button>                
						</div>  
						<div class="page_nav_menu" style="margin-left: 15px;">
		                    <ul class="page_nav">
		                        <li>
		                            <a href="add.php">
		                                <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
		                                Add New
		                            </a>
		                        </li>
		                    </ul>
            			</div>            
                
                        
                    </form>
                </div>   
                <div class="bd_clear"></div>
                </div>

                
            </div>
        </div>
    
<?php	
$cond="1";
if($loginType=='Admin')
{
	if(@$_POST['company'])
	{
		$proId	=	@$_POST['company'];
	}
	else
	{
		$proId	=	0;
	}
	$check 	= 	" a.proId='$proId' and a.finYear='$finYear'";
}

$fromdate	= 	date('01-m-Y');
$todate		=	date('d-m-Y');						
if(@$_REQUEST['from'])
{
	$fromdate	=	$_REQUEST['from'];
}
if(@$_REQUEST['to'])
{
	$todate		=	$_REQUEST['to'];
}
$cond 		= 	"a.transactionDate BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."'";

?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Voucher No</th>
                            <th>Account</th>
                            <th>Cr/Dr</th>
                            <th>Amount</th>                            
                            <!--<th>Actions</th>-->
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    $selectAll = "SELECT a.ID,
                    					 a.voucherNo,
                    					 a.fromLedger,
                    					 a.credit,
                    					 a.debit,
                    					 a.loginId,
                    					 ".TABLE_ACCOUNTS.".accountName
                    				FROM ".TABLE_TRANSACTION." a
                    		   LEFT JOIN ".TABLE_ACCOUNTS." ON  a.fromLedger=".TABLE_ACCOUNTS.".ID
                    				WHERE $check AND $cond AND voucherType='Journal' ORDER BY ID ASC";
                    
                    $result = $db->query($selectAll);
                    if((mysql_num_rows($result)==0) or (!isset($_POST['submit'])))
                    {
					?>
						<tr><td colspan="5" align="center">No data found. </td></tr
					<?php
					}
					else
					{
	                    while ($row = mysql_fetch_array($result)) {
	                    	$loginIDs=$row['loginId'];
	                        ?>
	                     <tr>                            
	                        <td><?php echo ++$i; ?></td>
	                       	<td><?php echo "JN".$row['voucherNo']; ?></td>
	                        <td><?php echo $row['accountName']; ?></td>
	                        <?php if($row['credit']!=0){ ?>
	                        <td><?php echo "Credit"; ?></td>
	                        <td><?php echo $row['credit']; ?></td>
	                        <?php }if($row['debit']!=0){?>
	                        <td><?php echo "Debit"; ?></td>
	                        <td><?php echo $row['debit']; ?></td>
	                        <?php }?>
	                       				                            
	                        <!--<td>
	                        <?php if($basicSettingEdit && $logID==$loginIDs){?>
	                        <a class="show_table_lnk show_table_lnk_edit" href="edit.php?id=<?php echo $row['ID'];?>">Edit</a>
	                        <a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?id=<?php echo $row['ID'];?>&op=delete">Delete</a>
	                        <?php } //end of if condition?>
	                        </td>-->
	                    </tr>
	                        <?php
	                    }
                    }
                    ?>
				                   
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>