<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear=$_SESSION['finYear'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype){
	// NEW SECTION
	case 'company':

	if(!$_REQUEST['id']){
		$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
		header("location:index.php");
	} 
	else{
		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		$db->connect();
		$success = 0;

		$companyId			= 	$App->convert($_REQUEST['id']);
		$select 			=	"select ID,status from ".TABLE_LOGIN." 
								 where ".TABLE_LOGIN.".userId=$companyId
                        			  and ".TABLE_LOGIN.".type='Branch' and ".TABLE_LOGIN.".finYear=$finYear";
        $result				=	mysql_query($select);
        $res				=	mysql_fetch_array($result);
        $editId				=	$res['ID'];
        $status				=	$res['status'];
        if($status=='Active')
        {
			$data['status']		= 	'Blocked'; 
		}
		else{
			$data['status']		= 	'Active'; 
		}
		       
		$success = $db->query_update(TABLE_LOGIN, $data, " ID='{$editId}'");
		$db->close();																		
		}
			if($success)
			{
				$_SESSION['msg']=$App->sessionMsgCreate("success","Updated successfully");			
			}							
			else{					
				$_SESSION['msg']=$App->sessionMsgCreate("error","Updation Failed");						
			}		
			header("location:company.php");		
	break;
	// EDIT SECTION
	case 'staff':
	if(!$_REQUEST['id']){
		$_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
		header("location:index.php");
	} 
	else{
		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
		$db->connect();
		$success = 0;

		$staffId			= 	$App->convert($_REQUEST['id']);
		$select 			=	"select ID,status from ".TABLE_LOGIN." 
								 where ".TABLE_LOGIN.".userId=$staffId
                        			  and ".TABLE_LOGIN.".type='Staff' and ".TABLE_LOGIN.".finYear=$finYear";
        $result				=	mysql_query($select);
        $res				=	mysql_fetch_array($result);
        $editId				=	$res['ID'];
        $status				=	$res['status'];
	        if($status=='Active')
	        {
				$data['status']		= 	'Blocked'; 
			}
			else{
				$data['status']		= 	'Active'; 
			}
		       
		$success = $db->query_update(TABLE_LOGIN, $data, " ID='{$editId}'");
		$db->close();																		
		}
			if($success)
			{
				$_SESSION['msg']=$App->sessionMsgCreate("success","Updated successfully");			
			}							
			else{					
				$_SESSION['msg']=$App->sessionMsgCreate("error","Updation Failed");						
			}		
			header("location:staff.php");		
	break;
}
?>