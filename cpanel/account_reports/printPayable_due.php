<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");


if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

	
$loginId	=	$_SESSION['travelId'];
$loginType	=	$_SESSION['travelType'];
$finYear	=	$_SESSION['finYear'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
	case 'print':
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
			$db->connect();
			
			//maximum num of rows to show	
			$defRow			= 8;
			
			$proId		=	$_REQUEST['pCompany'];
			$supID		=	$_REQUEST['pAccounts'];
			$fDate		=	$_REQUEST['pFdate'];
			$tDate		=	$_REQUEST['pTdate'];
			
			if($supID)
			{
				$billDetQry = "SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'airline' as invType FROM `".TABLE_PURCHASE_AIRLINE."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`invoiceNo` and voucherType='airline' AND proId=$proId and finYear=$finYear)) and `supplierId`= ".$supID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."' AND proId=$proId and finYear=$finYear
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'hotel' as invType FROM `".TABLE_PURCHASE_HOTEL."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`invoiceNo` and voucherType='hotel' AND proId=$proId and finYear=$finYear)) and `supplierId`= ".$supID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."' AND proId=$proId and finYear=$finYear
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'miscellaneous' as invType FROM `".TABLE_PURCHASE_MISC."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`invoiceNo` and voucherType='miscellaneous' AND proId=$proId and finYear=$finYear)) and `supplierId`= ".$supID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."' AND proId=$proId and finYear=$finYear
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'transportation' as invType FROM `".TABLE_PURCHASE_TRANSPORT."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`invoiceNo` and voucherType='transportation' AND proId=$proId and finYear=$finYear)) and `supplierId`= ".$supID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."' AND proId=$proId and finYear=$finYear
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'visa' as invType FROM `".TABLE_PURCHASE_VISA."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`invoiceNo` and voucherType='visa' AND proId=$proId and finYear=$finYear)) and `supplierId`= ".$supID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."' AND proId=$proId and finYear=$finYear
		
		
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Airline' as invType FROM `".TABLE_PURCHASE_PACKAGE_AIRLINE."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`voucherNo` and voucherType='Package Airline' AND proId=$proId and finYear=$finYear)) and `supplierId`= ".$supID." AND a.`purchaseDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."' AND proId=$proId and finYear=$finYear
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Hotel' as invType FROM `".TABLE_PURCHASE_PACKAGE_HOTEL."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`voucherNo` and voucherType='Package Hotel' AND proId=$proId and finYear=$finYear)) and `supplierId`= ".$supID." AND a.`purchaseDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."' AND proId=$proId and finYear=$finYear
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Transport' as invType FROM `".TABLE_PURCHASE_PACKAGE_TRANSPORT."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`voucherNo` and voucherType='Package Transport' AND proId=$proId and finYear=$finYear)) and `supplierId`= ".$supID." AND a.`purchaseDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."' AND proId=$proId and finYear=$finYear
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Visa' as invType FROM `".TABLE_PURCHASE_PACKAGE_VISA."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`voucherNo` and voucherType='Package Visa' AND proId=$proId and finYear=$finYear)) and `supplierId`= ".$supID." AND a.`purchaseDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."' AND proId=$proId and finYear=$finYear";
			
			//echo $accReportQry;die;
			$billDetRes 	= 	mysql_query($billDetQry);
			$billDetNum		=	mysql_num_rows($billDetRes);
			
			// for getting account name and id
			$accQry		=	mysql_query("SELECT ID,accountName FROM ".TABLE_ACCOUNTS." WHERE ID=".$supID." AND proId!=0 AND proId=$proId AND finYear=$finYear");
			$accRow		=	mysql_fetch_array($accQry);
			$customer	=	$accRow['accountName'].'('.$accRow['ID'].')';				 
			// Printing format starting
			
			require_once('../../printing/tcpdf_include.php');
			require_once('../../printing/tcpdf_config_alt.php');
			require_once('../../printing/tcpdf.php');

			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
			$db->connect();
				
			$sql2 		= 	"SELECT * FROM `".TABLE_COMPANY."` where ID=$proId";
			$vou_record1 = 	$db->query_first($sql2);
			$company 	= 	$vou_record1['companyName'];
			$caddress 	= 	$vou_record1['address'];
			$cphone		=	$vou_record1['phone'];
			$cmobile	=	$vou_record1['mobile'];	
			$cemail		=	$vou_record1['email'];
			$cpin		=	$vou_record1['pin'];
				
			// set default header data
			$pdf->SetHeaderData(0, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING, array(0,0,0), array(0,0,0));
			
			// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			
			// set default font subsetting mode
			$pdf->setFontSubsetting(true);
			
			// Set font
			// dejavusans is a UTF-8 Unicode font, if you only need to
			// print standard ASCII chars, you can use core fonts like
			// helvetica or times to reduce file size.
			$pdf->SetFont('courier', '', 11, '', true);
			
			// Add a page
			$pdf->AddPage();
			
			// Set some content to print
			
															
			
			//basic info
			$tbl_header1 = '<div><table style="width: 530;" cellspacing="0" border="0" >';
			$tbl_footer1 = '</table></div>';
			$tbl1 = '';
			
			$tbl1	=	'<tr>
							<td colspan="10" align="left"><h2>'.strtoupper($company).'</h2></td>
						 </tr>
						 <tr>
						    <td colspan="10"><h3><strong>Receivable Outstanding Statement Detail (Method:Invoice)</strong></h3></td>
						 </tr>
						 <tr>
							<td colspan="8" ><strong>LEDGER ACCOUNT :FROM '.$fDate.' TO '.$tDate.' </strong> </td>
							<td colspan="2" align="right"><strong>Date:'.date("d-m-Y").'</strong></td>
						 </tr>
						 <tr>
							<td  style="width: 50px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black ">CUSTOMER NAME</td>
							<td  style="width: 50px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black ">INVOICE </td>
							<td  style="width: 60px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black "> DATE</td>
							<td  style="width: 60px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black " align="right">DEBIT AMT</td>
							<td  style="width: 50px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black " align="right">CREDIT AMT</td>
							<td  style="width: 60px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black " align="right">PENDING AMT</td>
							<td  style="width: 60px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black ">STATUS</td>
							<td  style="width: 150px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black " colspan="3">DETAILS</td>
							
						 </tr>
						 
						 ';
						 
				 $status		=	0;
				 $pending		=	0;
				 
				 $i				=	0;
				 $debitSum		=	0;
				 $creditSum		=	0;
				 while($billDetRow = mysql_fetch_array($billDetRes))
				 {
				 	$i++;
				 	if($i!=1)  // for showing customer name only onces
				 	{
				 		$customer	=	'';
				 	}
				 	switch ($billDetRow['invType']) 
				 	{
						case "airline" :$invPrefix = "AP";
							break;
						case "hotel" : $invPrefix = "HP";
							break;
						case "miscellaneous" :$invPrefix = "MP";
							break;
						case "transportation" :$invPrefix = "TP";
							break;
						case "visa" :$invPrefix = "VP";
							break;
						case "Package Airline" :$invPrefix = "PAP";
							break;
						case "Package Hotel" :$invPrefix = "PHP";
							break;					
						case "Package Transport" :$invPrefix = "PTP";
							break;
						case "Package Visa" :$invPrefix = "PVP";
							break;
					}
					
					//for getting the total paid amt from cash receipt table
					
					$paidAmount	=	0;
					$billSumQry = "SELECT SUM(payingAmount) as paidAmount FROM `".TABLE_CASH_PAYMENT."` WHERE voucherType = '".$billDetRow['invType']."' AND invoiceNo = ".$billDetRow['invoiceNo']." GROUP BY invoiceNo AND proId=$proId and finYear=$finYear";
					$billSumRes = mysql_query($billSumQry);
					if(mysql_num_rows($billSumRes) > 0)
					{
						$res	=	mysql_fetch_array($billSumRes);
						$paidAmount =	$res['paidAmount'];		
					}
					
					$pending	=	$billDetRow['netAmount']- $paidAmount;
					$status		=	$status+$pending;
					$creditSum	=	$creditSum+$billDetRow['netAmount'];
					$debitSum	=	$debitSum+$paidAmount;
					
					$details	=	'';
					//passenger details
					if($billDetRow['invType']=='airline')
					{
						$airQry		=	mysql_query("SELECT details FROM ".TABLE_TRANSACTION." WHERE voucherType='Airline Payable' AND voucherNo=".$billDetRow['invoiceNo']." AND proId=$proId and finYear=$finYear");
						$airRow		=	mysql_fetch_array($airQry);
						$details	=	$airRow['details'];
					}
					if($billDetRow['invType']=='hotel')
					{
						$hotQry		=	mysql_query("SELECT details FROM ".TABLE_TRANSACTION." WHERE voucherType='Hotel Payable' AND voucherNo=".$billDetRow['invoiceNo']." AND proId=$proId and finYear=$finYear");
						$hotRow		=	mysql_fetch_array($hotQry);
						$details	=	$hotRow['details'];
					}
					if($billDetRow['invType']=='miscellaneous')
					{
						$misQry		=	mysql_query("SELECT details FROM ".TABLE_TRANSACTION." WHERE voucherType='Misc Payable' AND voucherNo=".$billDetRow['invoiceNo']." AND proId=$proId and finYear=$finYear");
						$misRow		=	mysql_fetch_array($misQry);
						$details	=	$misRow['details'];
					}
					if($billDetRow['invType']=='transportation')
					{
						$transQry	=	mysql_query("SELECT details FROM ".TABLE_TRANSACTION." WHERE voucherType='Transport Payable' AND voucherNo=".$billDetRow['invoiceNo']." AND proId=$proId and finYear=$finYear");
						$transRow	=	mysql_fetch_array($transQry);
						$details	=	$transRow['details'];
					}
					if($billDetRow['invType']=='visa')
					{
						$visaQry	=	mysql_query("SELECT details FROM ".TABLE_TRANSACTION." WHERE voucherType='Visa Payable' AND voucherNo=".$billDetRow['invoiceNo']." AND proId=$proId and finYear=$finYear");
						$visaRow	=	mysql_fetch_array($visaQry);
						$details	=	$visaRow['details'];
					}
					if($billDetRow['invType']=='Package Airline')
					{
						$pacQry		=	mysql_query("SELECT details FROM ".TABLE_TRANSACTION." WHERE voucherType='Package Airline' AND voucherNo=".$billDetRow['invoiceNo']." AND proId=$proId and finYear=$finYear");
						$pacRow		=	mysql_fetch_array($pacQry);
						$details	=	$pacRow['details'];
					}
					if($billDetRow['invType']=='Package Hotel')
					{
						$pacQry		=	mysql_query("SELECT details FROM ".TABLE_TRANSACTION." WHERE voucherType='Package Hotel' AND voucherNo=".$billDetRow['invoiceNo']." AND proId=$proId and finYear=$finYear");
						$pacRow		=	mysql_fetch_array($pacQry);
						$details	=	$pacRow['details'];
					}
					if($billDetRow['invType']=='Package Transport')
					{
						$pacQry		=	mysql_query("SELECT details FROM ".TABLE_TRANSACTION." WHERE voucherType='Package Transportation' AND voucherNo=".$billDetRow['invoiceNo']." AND proId=$proId and finYear=$finYear");
						$pacRow		=	mysql_fetch_array($pacQry);
						$details	=	$pacRow['details'];
					}
					if($billDetRow['invType']=='Package Visa')
					{
						$pacQry		=	mysql_query("SELECT details FROM ".TABLE_TRANSACTION." WHERE voucherType='Package Visa' AND voucherNo=".$billDetRow['invoiceNo']." AND proId=$proId and finYear=$finYear");
						$pacRow		=	mysql_fetch_array($pacQry);
						$details	=	$pacRow['details'];
					}
					
					$tbl1.=	 '<tr>
								<td>'.$customer.'</td>
								<td>'.$invPrefix.$billDetRow['invoiceNo'].'</td>
								<td>'.$App->dbformat_date($billDetRow['invoiceDate']).'</td>
								<td align="right">'.sprintf("%0.2f",round(abs($paidAmount),2)).'</td>
								<td align="right">'.sprintf("%0.2f",round(abs($billDetRow['netAmount']),2)).'</td>
								<td align="right">'.sprintf("%0.2f",round(abs($pending),2)).'</td>
								<td>'.sprintf("%0.2f",round($status,2)).'</td>
								<td colspan="3">'.$details.'</td>
							 </tr>';
				 }
				 if($billDetNum<$defRow)
				 {
					 $staticRow	=	$defRow-$billDetNum;
					 for($i=0;$i<$staticRow;$i++)
					 {
						$tbl1.='<tr>
						       		<td colspan="10"></td>
						       </tr>';
					 }
				 } 
				$tbl1.=	 '
						<tr>
							<td colspan="10" style="border-top: 0.1px solid black;">
							</td>
						</tr>
						<tr>
							<td colspan="3" ><strong>TOTAL AMT </strong></td>
							<td align="right"><strong>'.sprintf("%0.2f",round(abs($debitSum),2)).'</strong></td>
							<td align="right"><strong>'.sprintf("%0.2f",round(abs($creditSum),2)).'</strong></td>
							<td ></td>
							<td ><strong>'.sprintf("%0.2f",round(abs($debitSum-$creditSum),2)).'</strong></td>
							<td colspan="3"></td>
						 </tr>
						 <tr>
							<td colspan="10" style="border-bottom: 0.1px solid black;">
							</td>
						</tr>
						';
				
				$pdf->SetFontSize(8);
				$pdf->writeHTML($tbl_header1 . $tbl1 . $tbl_footer1, true, true, false, false, '');

				// Close and output PDF document
				$today	=	date("d-m-Y");
				$pdf->Output("'payable_due.php'.$today.pdf", 'I');
			}	
			else
			{
				$_SESSION['msg'] = $App->sessionMsgCreate('error', "Select one account.");
				header("location:payable_due.php");
			}
			$db->close();
	break;
}
?>