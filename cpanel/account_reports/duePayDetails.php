<?php
require('../admin_header.php');
if(@isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$loginType 	=	$_SESSION['travelType'];

$supID		=	$_GET['id'];
$accountName=	$_GET['name'];
$fromdate	=	$_GET['fdate'];
$todate		=	$_GET['tdate'];
$proId		=	$_GET['cmp'];
$finYear	=	$_SESSION['finYear'];
if($proId)
{
	$check 	= 	"a.proId='$proId'";
}
		
?>
<div class="row">
	<div class="col-lg-12">
		<div class="page_header">
			<div class="page_heading">
				<h3>Payable Due Details</h3>
			</div>
            <div class="page_controls">
            	<div class="page_search">
					<button class="bd_btn bd_btn_blue" id="print_form_trigger" type="button">Print</button>
				</div>
				<form id="print_form" style="display: none" method="post" action="printPayable_due.php?op=print" target="_blank">
					<input type="text" value="<?php echo $supID;?>"  name="pAccounts">
					<input type="text" value="<?php echo $fromdate;?>" name="pFdate">
					<input type="text" value="<?php echo $todate;?>" name="pTdate">
					<input type="text" value="<?php echo $proId;?>" name="pCompany">
				</form>
            </div>
			<div class="bd_clear"></div>
		</div>
	</div>
</div>
    
       
<div class="row">
	<div class="col-lg-12">
		<div class="table-responsive show_table_wrap">
			<table class="table table-bordered table-striped show_table">
				<thead>
					<tr>
						<th colspan="9" style="text-align: center">Account Name   :   <?php echo $accountName ?></td>						
					</tr>
					<tr>
						<th>Sl No</th>
						<th>Invoice No</th>
						<th>Staff</th>
						<th>Invoice Date</th>
						<th>Invoice Type</th> 
						<th>Net amount</th>
						<th>Paid amount</th>
						<th>Discount</th>
						<th>Balance</th>                                                       
					</tr>
				</thead>
				<tbody>
					<?php
                     
					$billDetQry = "SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'airline' as invType,a.loginId FROM `".TABLE_PURCHASE_AIRLINE."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_PAYMENT."` where `invoiceNo`= a.`invoiceNo` and voucherType='airline' AND proId=$proId AND finYear=$finYear)) AND `supplierId`= ".$supID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.finYear=$finYear AND $check
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'hotel' as invType,a.loginId FROM `".TABLE_PURCHASE_HOTEL."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_PAYMENT."` where `invoiceNo`= a.`invoiceNo` and voucherType='hotel' AND proId=$proId AND finYear=$finYear)) and `supplierId`= ".$supID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.finYear=$finYear AND $check
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'miscellaneous' as invType,a.loginId FROM `".TABLE_PURCHASE_MISC."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_PAYMENT."` where `invoiceNo`= a.`invoiceNo` and voucherType='miscellaneous' AND proId=$proId AND finYear=$finYear)) and `supplierId`= ".$supID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.finYear=$finYear AND $check
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'transportation' as invType,a.loginId FROM `".TABLE_PURCHASE_TRANSPORT."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_PAYMENT."` where `invoiceNo`= a.`invoiceNo` and voucherType='transportation' AND proId=$proId AND finYear=$finYear)) and `supplierId`= ".$supID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.finYear=$finYear AND $check
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'visa' as invType,a.loginId FROM `".TABLE_PURCHASE_VISA."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_PAYMENT."` where `invoiceNo`= a.`invoiceNo` and voucherType='visa' AND proId=$proId AND finYear=$finYear)) and `supplierId`= ".$supID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.finYear=$finYear AND $check
		
		
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Airline' as invType,a.loginId FROM `".TABLE_PURCHASE_PACKAGE_AIRLINE."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_PAYMENT."` where `invoiceNo`= a.`voucherNo` and voucherType='Package Airline' AND proId=$proId AND finYear=$finYear)) and `supplierId`= ".$supID." AND a.`purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.finYear=$finYear AND $check 
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Hotel' as invType,a.loginId FROM `".TABLE_PURCHASE_PACKAGE_HOTEL."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_PAYMENT."` where `invoiceNo`= a.`voucherNo` and voucherType='Package Hotel' AND proId=$proId AND finYear=$finYear)) and `supplierId`= ".$supID." AND a.`purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.finYear=$finYear AND $check 
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Transport' as invType,a.loginId FROM `".TABLE_PURCHASE_PACKAGE_TRANSPORT."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_PAYMENT."` where `invoiceNo`= a.`voucherNo` and voucherType='Package Transport' AND proId=$proId AND finYear=$finYear)) and `supplierId`= ".$supID." AND a.`purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.finYear=$finYear AND $check 
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Visa' as invType,a.loginId FROM `".TABLE_PURCHASE_PACKAGE_VISA."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_PAYMENT."` where `invoiceNo`= a.`voucherNo` and voucherType='Package Visa' AND proId=$proId AND finYear=$finYear)) and `supplierId`= ".$supID." AND a.`purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND $check AND a.finYear=$finYear" ;
	
	$billDetRes = mysql_query($billDetQry);
	
	//for getting the opening balance
	
	$opBalQry	=	"SELECT SUM(a.credit) AS totalCredit,SUM(a.debit) AS totalDebit FROM `".TABLE_TRANSACTION."` a WHERE ( a.`fromLedger`='$supID' AND a.`transactionDate` < '". $App->dbformat_date($fromdate)."') AND voucherType='Opening Balance' AND $check AND finYear=$finYear";
	//echo $opBalQry;
	$tot_cd_result 	=  	$db->query($opBalQry);
	$tot_cd			=	mysql_fetch_array($tot_cd_result);
	$openingBal		= 	$tot_cd['totalDebit']-$tot_cd['totalCredit'] ;
	
		
					if(mysql_num_rows($billDetRes) > 0)
					{
						$bill_count = mysql_num_rows($billDetRes);
						$total=$j=0;
					?>
					<tr>							   		
				   		<td colspan="8" align="right">Opening Balance</td>						   
				  		<?php
					    if($openingBal<0)
						{
							$opening_credit = 	$openingBal;
							$opening_debit  = 	0;
							$opening_type	= 	'Cr';
						}
					    else if($openingBal>0)
						{
							$opening_debit 	= 	$openingBal;
							$opening_credit	= 	0;
							$opening_type	= 	'Dr';
						}
						else
						{
							$opening_credit	= 	0;
							$opening_debit  = 	0;
							$opening_type	=	'';
						}
					    ?>
				   	    <td><?php echo abs($opening_credit); ?></td>
				 	</tr>
					<?php
					$total = 0;
					while($billDetRow = mysql_fetch_array($billDetRes))
					{
						switch ($billDetRow['invType']) 
						{
							case "airline" :
								$invPrefix = "AP";
								break;
							case "hotel" :
								$invPrefix = "HP";
								break;
							case "miscellaneous" :
								$invPrefix = "MP";
								break;
							case "transportation" :
								$invPrefix = "TP";
								break;
							case "visa" :
								$invPrefix = "VP";
								break;
							case "Package Airline" :
								$invPrefix = "PAP";
								break;
							case "Package Hotel" :
								$invPrefix = "PHP";
								break;					
							case "Package Transport" :
								$invPrefix = "PTP";
								break;
							case "Package Visa" :
								$invPrefix = "PVP";
								break;
						}
						
						$loginId	=	$billDetRow['loginId'];
						//for getting the username in the index
						$staffQuery = "SELECT * from ".TABLE_STAFF.",".TABLE_LOGIN."
									   WHERE ".TABLE_LOGIN.".ID ='$loginId'
									   AND  ".TABLE_LOGIN.".userId = ".TABLE_STAFF.".ID";
						//echo $staffQuery;			   
						$staffName = $db->query_first($staffQuery);
						
						$invDateArray = explode('-', $billDetRow['invoiceDate']);
						
						$tempArray = array("invoice_no" => $billDetRow['invoiceNo'], "invoice_date" => $invDateArray[2].'-'.$invDateArray[1].'-'.$invDateArray[0], "net_amount" => $billDetRow['netAmount'], "paid_amount" => 0, "inv_type" => $billDetRow['invType'], "inv_prefix" => $invPrefix);
					
						$billSumQry = "SELECT SUM(payingAmount) as paidAmount,SUM(discount) as dis FROM `".TABLE_CASH_PAYMENT."` WHERE voucherType = '".$billDetRow['invType']."' AND invoiceNo = ".$billDetRow['invoiceNo']." AND proId=$proId AND finYear=$finYear GROUP BY invoiceNo";
					//echo $billSumQry;
						$billSumRes = mysql_query($billSumQry);
					
						if (mysql_num_rows($billSumRes) > 0) 
						{
							$res=mysql_fetch_array($billSumRes);
							$tempArray["paid_amount"] = $res['paidAmount'];	
							$tempArray["discount"] 	  = $res['dis'];				
						}
						else
						{
							$tempArray["paid_amount"] = 0;	
							$tempArray["discount"] 	  = 0;	
						}
						?>
						<tr>                            
							<td><?php echo ++$j; ?></td>
							<td><?php echo $tempArray['inv_prefix'].$tempArray['invoice_no']; ?></td>
							<td><?php echo $staffName['staffName']; ?></td>
							<td><?php echo $tempArray['invoice_date']; ?></td>
							<td><?php echo $tempArray['inv_type']; ?></td>
							<td><?php echo $tempArray['net_amount']; ?></td>
							<td><?php echo $tempArray['paid_amount'];?></td>
							<td><?php echo $tempArray['discount'];?></td>
							<td><?php echo $tempArray['net_amount']-($tempArray['paid_amount']); ?></td>
							<?php
							$total=$total+($tempArray['net_amount']- ($tempArray['paid_amount']));
							?>
						</tr>
						<?php
					}
					$total= $total+abs($opening_credit);	
               		?>
                   <tr>
                   <th colspan="7"></th>
                   <th style="text-align: right">Total</th>
                   <th><?php echo $total?></th>
                   </tr>
                   <?php
					}
					else
					{
						?>
						<tr>
							<td colspan="8" align="center">No data found</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>