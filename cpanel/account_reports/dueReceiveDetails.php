<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if(@isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
$custID=$_GET['id'];
$accountName=$_GET['name'];
$fromdate	=	$_GET['fdate'];
$todate		=	$_GET['tdate'];
$proId		=	$_GET['cmp'];
$finYear	=	$_SESSION['finYear'];
?>
<div class="row">
	<div class="col-lg-12">
		<div class="page_header">
			<div class="page_heading">
				<h3>Receivable Due Details</h3>
			</div>
                <div class="page_controls">
                	<div class="page_search">
						<button class="bd_btn bd_btn_blue" id="print_form_trigger" type="button">Print</button>
					</div>
					<form id="print_form" style="display: none" method="post" action="printReceivable_due.php?op=print" target="_blank">
						<input type="text" value="<?php echo $custID;?>"  name="pAccounts">
						<input type="text" value="<?php echo $fromdate;?>" name="pFdate">
						<input type="text" value="<?php echo $todate;?>" name="pTdate">
						<input type="text" value="<?php echo $proId;?>" name="pCompany">
					</form>
                </div>
			<div class="bd_clear"></div>
		</div>
	</div>
</div>
    
       
<div class="row">
	<div class="col-lg-12">
		<div class="table-responsive show_table_wrap">
			<table class="table table-bordered table-striped show_table">
				<thead>
				<tr>
						<th colspan="8" style="text-align: center">Account Name   :   <?php echo $accountName ?></th>						
					</tr>
					<tr>
						<th>Sl No</th>
						<th>Invoice No</th>
						<th>Staff Name</th>
						<th>Invoice Date</th>
						<th>Invoice Type</th> 
						<th>Net amount</th>
						<th>Paid amount</th>
						<th>Balance</th>                                                       
					</tr>
				</thead>
				<tbody>
					<?php
                     
					$billDetQry = "SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'airline' as invType,a.loginId FROM `".TABLE_INVOICE_AIRLINE."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_RECEIPT."` where `invoiceNo`= a.`invoiceNo` and voucherType='airline' AND proId=$proId AND finYear=$finYear)) and `customerId`= ".$custID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND proId=$proId AND finYear=$finYear 
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'hotel' as invType,a.loginId FROM `".TABLE_INVOICE_HOTEL."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_RECEIPT."` where `invoiceNo`= a.`invoiceNo` and voucherType='hotel' AND proId=$proId AND finYear=$finYear)) and `customerId`= ".$custID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND proId=$proId AND finYear=$finYear
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'miscellaneous' as invType ,a.loginId FROM `".TABLE_INVOICE_MISC."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_RECEIPT."` where `invoiceNo`= a.`invoiceNo` and voucherType='miscellaneous' AND proId=$proId AND finYear=$finYear)) and `customerId`= ".$custID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND proId=$proId AND finYear=$finYear
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'transportation' as invType,a.loginId FROM `".TABLE_INVOICE_TRANSPORT."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_RECEIPT."` where `invoiceNo`= a.`invoiceNo` and voucherType='transportation' AND proId=$proId AND finYear=$finYear)) and `customerId`= ".$custID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND proId=$proId AND finYear=$finYear
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'visa' as invType,a.loginId FROM `".TABLE_INVOICE_VISA."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_RECEIPT."` where `invoiceNo`= a.`invoiceNo` and voucherType='visa' AND proId=$proId AND finYear=$finYear)) and `customerId`= ".$custID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND proId=$proId AND finYear=$finYear		
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'package' as invType,a.loginId FROM `".TABLE_INVOICE_PACKAGE."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `".TABLE_CASH_RECEIPT."` where `invoiceNo`= a.`invoiceNo` and voucherType='package' AND proId=$proId AND finYear=$finYear)) and `customerId`= ".$custID." AND a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND proId=$proId AND finYear=$finYear";
		
		//echo $billDetQry;
		
					$billDetRes = mysql_query($billDetQry);
					
					//for getting opening balance
								  
				$opBalQry	=	"SELECT SUM(credit) AS totalCredit,SUM(debit) AS totalDebit 
								  FROM `".TABLE_TRANSACTION."` 
								 WHERE (`fromLedger`='$custID' AND 
								 `transactionDate` < '". $App->dbformat_date($fromdate)."') AND voucherType='Opening Balance' AND proId=$proId AND finYear=$finYear";
				//echo $opBalQry;
				$tot_cd_result 		=  	$db->query($opBalQry);
				$tot_cd				=	mysql_fetch_array($tot_cd_result);
			//$openingBal			=   -$tot_cd['totalCredit'] + $tot_cd['totalDebit'];
				$openingBal			=   $tot_cd['totalDebit']-$tot_cd['totalCredit'] ;
					if(mysql_num_rows($billDetRes) > 0)
					{
						
						?>
						<tr>							   		
					   		<td colspan="7" align="right">Opening Balance</td>							
					   		<?php
							if($openingBal<0){
								$opening_credit = 	$openingBal;
								$opening_debit  = 	0;
								$opening_type	= 	'Cr';
							
							}
							else if($openingBal>0){
								$opening_debit 	= 	$openingBal;
								$opening_credit	= 	0;
								$opening_type	= 	'Dr';
							}
							else{
								$opening_credit	= 	0;
								$opening_debit  = 	0;
								$opening_type	=	'';
							}
							?>
							<td><?php echo abs($opening_debit); ?></td>
							<!--<td><?php echo abs($opening_credit); ?></td>
							<td><?php echo abs($openingBal).' '.$opening_type; ?></td>-->
						</tr>
						<?php
						$bill_count = mysql_num_rows($billDetRes);
						$total=$j=0;
						while($billDetRow = mysql_fetch_array($billDetRes))
						{
							switch ($billDetRow['invType']) 
							{
								case "airline" :
									$invPrefix = "AI";
									break;
								case "hotel" :
									$invPrefix = "HI";
									break;
								case "miscellaneous" :
									$invPrefix = "MI";
									break;
								case "transportation" :
									$invPrefix = "TI";
									break;
								case "visa" :
									$invPrefix = "VI";
									break;
								case "package" :
									$invPrefix = "PI";
									break;
							}
							
						$loginId	=	$billDetRow['loginId'];
						//for getting the username in the index
						$staffQuery = "SELECT * from ".TABLE_STAFF.",".TABLE_LOGIN."
									   WHERE ".TABLE_LOGIN.".ID ='$loginId'
									   AND  ".TABLE_LOGIN.".userId = ".TABLE_STAFF.".ID";
						//echo $staffQuery;			   
						$staffName = $db->query_first($staffQuery);
							
							$invDateArray = explode('-', $billDetRow['invoiceDate']);
				$tempArray = array("invoice_no" => $billDetRow['invoiceNo'], "invoice_date" => $invDateArray[2].'-'.$invDateArray[1].'-'.$invDateArray[0], "net_amount" => $billDetRow['netAmount'], "paid_amount" => 0, "inv_type" => $billDetRow['invType'], "inv_prefix" => $invPrefix);
				
				$billSumQry = "SELECT SUM(payingAmount) as paidAmount FROM `".TABLE_CASH_RECEIPT."` WHERE voucherType = '".$billDetRow['invType']."' AND invoiceNo = ".$billDetRow['invoiceNo']." AND proId=$proId AND finYear=$finYear GROUP BY invoiceNo";
				//echo $billSumQry;
				$billSumRes = mysql_query($billSumQry);
				if(mysql_num_rows($billSumRes) > 0){
								$res=mysql_fetch_array($billSumRes);
								$tempArray["paid_amount"] = $res['paidAmount'];				
							}
							
							
						?>
							<tr>                            
								<td><?php echo ++$j; ?></td>
								<td><?php echo $tempArray['inv_prefix'].$tempArray['invoice_no'] ?></td>
								<td><?php echo $staffName['staffName'];?></td>
								<td><?php echo $tempArray['invoice_date'] ?></td>
								<td><?php echo $tempArray['inv_type'] ?></td>
								<td><?php echo $tempArray['net_amount'] ?></td>
								<td><?php echo $tempArray['paid_amount'] ?></td>
								<td><?php echo $tempArray['net_amount']- $tempArray['paid_amount'] ?></td>	
								<?php
								$total=$total+($tempArray['net_amount']- $tempArray['paid_amount']);
								?>
							</tr>
							<?php
						}	
						$total= $total+abs($opening_debit);	
						?>
                   <tr>
                   <th colspan="7" style="text-align: right">Total</th>
                   <th><?php echo $total?></th>
                   </tr>
                   <?php
					}
					else{
						?>
						<tr>
							<td colspan="8" align="center">No data found</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>