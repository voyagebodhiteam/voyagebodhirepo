<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId		=	$_SESSION['proId'];
$loginId	=	$_SESSION['travelId'];
$loginType	=	$_SESSION['travelType'];

if($loginType =='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$check 	= 	" ".TABLE_TRANSACTION.".proId='$proId'";
}



$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
	case 'print':
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
			$db->connect();
			
			$fromdate	= 	date('d-m-Y');
			$todate		=	date('d-m-Y');	
			$checkGroup	=	1;		
			$accountType=	0;
			$agent		=	1;	
					
			if(@$_POST['pFrom'])  
			{
				$fromdate	=	$_POST['pFrom'];			
			}		
			if(@$_POST['pTo'])
			{				
				$todate		=	$_POST['pTo'];
			}
			if(@$_POST['pAccountGroupId'])
			{				
				$accountGroupId		=	$_POST['pAccountGroupId'];
				$checkGroup 		= 	"".TABLE_ACCOUNTS.".accountGroupId='$accountGroupId'";
			}
			
			if(@$_POST['pAccountType'])
			{				
				$accountType		=	$_POST['pAccountType'];				
			}
			if(@$_POST['pOpBalType'])
			{				
				$opBalType		=	$_POST['pOpBalType'];				
			}
			if(@$_POST['pAgent'])
			{
				$agent	=	"".TABLE_ACCOUNTS.".agentId='".$_POST['pAgent']."'  ";
			}
			if($loginType=='Admin')
			{
				if(@$_POST['pCompany'])
				{
					$proId	=	@$_POST['pCompany'];
				}
				else{
					$proId = 0;
				}
				
				$check 	= 	" ".TABLE_TRANSACTION.".proId='$proId'";
			}
			
			@$accounts		=	1;
			$arrayCount		=	0;
			if(@$_POST['pAccountGroupIds'])
			{
				$customerArray	=	explode(",",$_POST['pAccountGroupIds']);
				$arrayCount		=	count($customerArray);
				$accountsId		=	$_POST['pAccountGroupIds'];
				$accounts		=	"".TABLE_TRANSACTION.".fromLedger IN (".$accountsId.")";
			}
			//echo $arrayCount.",".$proId;die;
			if($arrayCount!=0 and $proId!=0)
			{
				$accReportQry	=	"SELECT ".TABLE_TRANSACTION.".ID,
											".TABLE_TRANSACTION.".credit,
											".TABLE_TRANSACTION.".debit,
											".TABLE_TRANSACTION.".transactionDate,
											".TABLE_TRANSACTION.".details,
											".TABLE_TRANSACTION.".toLedger,
											".TABLE_ACCOUNTS.".accountName
									  FROM `".TABLE_TRANSACTION."`
								 LEFT JOIN  ".TABLE_ACCOUNTS." on ".TABLE_TRANSACTION.".fromLedger=".TABLE_ACCOUNTS.".ID
									  WHERE (".TABLE_TRANSACTION.".transactionDate BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."') 
									  AND $check
									  AND $checkGroup
									  AND $accounts
									  AND $agent
									  ORDER BY `".TABLE_TRANSACTION."`.transactionDate";
				//echo $accReportQry;
				$accReportResult	=	mysql_query($accReportQry);
				$accReportNum		=	mysql_num_rows($accReportResult);
				$defRow				=	8;	
				
				// for getting from account name and id
				$accQry		=	mysql_query("SELECT ID,accountName FROM ".TABLE_ACCOUNTS." WHERE ID=".$customerArray[0]." AND proId!=0 AND proId=$proId");
				$accRow		=	mysql_fetch_array($accQry);
				
				
				// for getting the opening balance
				$opBalQry		=	"SELECT SUM(credit) AS totalCredit,SUM(debit) AS totalDebit FROM `".TABLE_TRANSACTION."` WHERE ( `fromLedger`='$accountsId' AND `transactionDate` < '". $App->dbformat_date($fromdate)."') AND $check ";
				//echo $opBalQry;die;  
				$tot_cd_result 	=  	$db->query($opBalQry);
				$tot_cd			=	mysql_fetch_array($tot_cd_result);
				$openingBal		= 	-$tot_cd['totalCredit'] + $tot_cd['totalDebit'];
				if($openingBal<0)
				{
					$opening_credit = 	$openingBal;
					$opening_debit  = 	0;
					$opening_type	= 	'Cr';
				}
			   else if($openingBal>0)
				{
					$opening_debit 	= 	$openingBal;
					$opening_credit	= 	0;
					$opening_type	= 	'Db';
				}
				else
				{
					$opening_credit	= 	0;
					$opening_debit  = 	0;
					$opening_type	=	'';
				}
				
				//for balance 
				
				$balance = 0;
				$bal_type = '';
			  	$tot_credit =0;
			  	$tot_debit = 0;
			  	$loop = 0;
			  
				// adding opening balance with total credit ,debit and balance if search type is withop
					  	
				if(@$opBalType=='withOp')
				{
					$balance 	= 	$balance+$openingBal;
					
					if($openingBal>0)
					{
					 	$tot_debit = $tot_debit+$openingBal;
					}
					else if($openingBal<0)
					{
					 	$tot_credit = $tot_credit+abs($openingBal);
					}
				}		 
				// Printing format starting
				
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
					
				
				$sql2 		= 	"SELECT * FROM `".TABLE_COMPANY."` where ID=$proId";
				$vou_record1 = 	$db->query_first($sql2);
				$company 	= 	$vou_record1['companyName'];
				$caddress 	= 	$vou_record1['address'];
				$cphone		=	$vou_record1['phone'];
				$cmobile	=	$vou_record1['mobile'];	
				$cemail		=	$vou_record1['email'];
				$cpin		=	$vou_record1['pin'];
					
				// set default header data
				$pdf->SetHeaderData(0, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING, array(0,0,0), array(0,0,0));
				
				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print
				
																
				
				//basic info
				$tbl_header1 = '<div><table style="width: 530;" cellspacing="0" border="0" >';
				$tbl_footer1 = '</table></div>';
				$tbl1 = '';
				
				$tbl1	=	'<tr>
								<td colspan="10" align="left"><h2>'.strtoupper($company).'</h2></td>
							 </tr>
							 <tr>
								<td colspan="7" ><strong>LEDGER ACCOUNT :FROM '.$fromdate.' TO '.$todate.'</strong> </td>
								<td colspan="3" align="right"><strong>Date:'.date("d-m-Y").'</strong></td>
							 </tr>
							 <tr>
								<td colspan="10"><strong>'.$accRow['accountName'].'('.$accRow['ID'].')</strong></td>
							 </tr>
							 <tr>
								<td colspan="2" style="width: 60px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black ">DATE</td>
								<td colspan="3" style="width: 200px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black ">PARTICULARS</td>
								<td  style="width: 70px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black ">To</td>
								<td colspan="1" style="width: 50px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black " align="right">DEBIT</td>
								<td colspan="1" style="width: 70px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black " align="right">CREDIT</td>
								<td colspan="2" style="width: 80px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black " align ="right">BALANCE</td>
							 </tr>';
				 if(@$opBalType=='withOp')
				 {
					$tbl1.=		' <tr>
								<td colspan="6"><strong>'.$fromdate.' BALANCE B/F... </strong></td>
								<td align ="right"><strong>'.sprintf("%0.2f",round(abs($opening_debit),2)).'</strong></td>
								<td align ="right"><strong>'.sprintf("%0.2f",round(abs($opening_credit),2)).'</strong></td>
								<td align ="right" colspan="2"><strong>'.sprintf("%0.2f",round(abs($openingBal),2)).$opening_type.'</strong></td>
								
							 </tr>
							 ';
				}			 
							 if($accReportNum>0)
							 {
								 while($accReportRow = mysql_fetch_array($accReportResult))
								 {
								 	// for getting to account name and id
									$toQry		=	mysql_query("SELECT ID,accountName FROM ".TABLE_ACCOUNTS." WHERE ID=".$accReportRow['toLedger']." AND (proId!=0 or proId=$proId) ");
									$toRow		=	mysql_fetch_array($toQry);
								 	
								 	$balance = $balance+$accReportRow['debit']-$accReportRow['credit'];
			                        if($balance>0)
			                        {
										$bal_type= 'Db';
									}
									elseif($balance<0)
									{
										$bal_type= 'Cr';
									}
									else
									{
										$bal_type= '';
									}
									$tot_credit = $tot_credit+$accReportRow['credit'];
									$tot_debit 	= $tot_debit+$accReportRow['debit'];
									
									$tbl1.=	 '<tr>
												<td colspan="2">'.$App->dbformat_date($accReportRow['transactionDate']).'</td>
												<td colspan="3">'.$accReportRow['details'].'</td>
												<td >'.$toRow['accountName'].'</td>
												<td colspan="1" align="right">'.sprintf("%0.2f",round(abs($accReportRow['debit']),2)).'</td>
												<td colspan="1" align="right">'.sprintf("%0.2f",round(abs($accReportRow['credit']),2)).'</td>
												<td colspan="2" align ="right">'.sprintf("%0.2f",round(abs($balance),2)).$bal_type.'</td>
											 </tr>';
								 }
							 }	
							 if($accReportNum<$defRow)
							 {
								 $staticRow	=	$defRow-$accReportNum;
								 for($i=0;$i<$staticRow;$i++)
								 {
									$tbl1.='<tr>
									       		<td colspan="10"></td>
									       </tr>';
								 }
							 } 
					$tbl1.=	 '<tr>
								<td colspan="2" style="width: 70px; border-top: 0.1px solid black;" ></td>
								<td colspan="4" style="width: 240px; border-top: 0.1px solid black;"><strong>TOTAL AMT </strong></td>
								<td colspan="1" style="width: 70px; border-top: 0.1px solid black;" align ="right"><strong>'.sprintf("%0.2f",round(abs($tot_debit),2)).'</strong></td>
								<td colspan="1" style="width: 70px; border-top: 0.1px solid black;" align ="right"><strong>'.sprintf("%0.2f",round(abs($tot_credit),2)).'</strong></td>
								<td colspan="2" style="width: 80px; border-top: 0.1px solid black;" align ="right"><strong>'.sprintf("%0.2f",round(abs($balance),2)).$bal_type.'</strong></td>
							 </tr>
							';
					
					$pdf->SetFontSize(9);
					$pdf->writeHTML($tbl_header1 . $tbl1 . $tbl_footer1, true, true, false, false, '');

					// Close and output PDF document
					$today	=	date("d-m-Y");
					$pdf->Output("'account_report'.$today.pdf", 'I');
				}
				else
				{
					$_SESSION['msg'] = $App->sessionMsgCreate('error', "Select one account.");
					header("location:account_report.php");
				}
			$db->close();
	break;
}
?>