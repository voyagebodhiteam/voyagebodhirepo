<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

$loginType 	=	$_SESSION['travelType'];

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	" ".TABLE_TRANSACTION.".proId='$proId'";
}

$proId		=	$_SESSION['proId'];
$loginId	=	$_SESSION['travelId'];
$loginType	=	$_SESSION['travelType'];
$finYear	=	$_SESSION['finYear'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
	case 'print':
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
			$db->connect();
			
			$fromdate	= 	date('d-m-Y');
			$todate		=	date('d-m-Y');	
			
			if(@$_POST['pFrom'])  
			{
				$fromdate	=	$_POST['pFrom'];			
			}		
			if(@$_POST['pTo'])
			{				
				$todate		=	$_POST['pTo'];
			}
			if($loginType=='Admin')
			{
				if(@$_POST['pCompany'])
				{
					$proId	=	@$_POST['pCompany'];
				}
				else
				{
					$proId	=	0;
				}
				$check 	= 	"`proId`='$proId'";
			}
			
			$amount = 0;
			
			//purchase
			$listQuery="SELECT ".TABLE_TRANSACTION.".ID,
							   ".TABLE_TRANSACTION.".fromLedger,
							   ".TABLE_TRANSACTION.".toLedger,
							   ".TABLE_TRANSACTION.".voucherNo,
							   ".TABLE_TRANSACTION.".voucherType,
						   sum(".TABLE_TRANSACTION.".debit) as debit,
							   ".TABLE_TRANSACTION.".credit,
							   ".TABLE_TRANSACTION.".addedDate,
							   ".TABLE_TRANSACTION.".referenceNo,
							   ".TABLE_TRANSACTION.".transactionDate
			 FROM `".TABLE_TRANSACTION."`  
			WHERE $check
			AND `transactionDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' 
			AND fromLedger IN (5,7,9,11,13,15)
                        AND finYear=$finYear
			GROUP BY ".TABLE_TRANSACTION.".toLedger
			ORDER BY `transactionDate`,`ID` DESC";
			
			//receipt
			$listQuery1="SELECT * FROM `".TABLE_TRANSACTION."`  
			WHERE $check
			AND `transactionDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."'
			AND `voucherType`='Cash Receipts'
                        AND finYear=$finYear
			ORDER BY `transactionDate`,`ID` DESC";
			
			$result = $db->query($listQuery1);
			$resultArray = mysql_fetch_array($result);
			$voucherType = $resultArray['voucherType'];
			
			//invoice
			$listQuery2="SELECT ".TABLE_TRANSACTION.".ID,
							   ".TABLE_TRANSACTION.".fromLedger,
							   ".TABLE_TRANSACTION.".toLedger,
							   ".TABLE_TRANSACTION.".voucherNo,
							   ".TABLE_TRANSACTION.".voucherType,
						   sum(".TABLE_TRANSACTION.".credit) as credit,
							   ".TABLE_TRANSACTION.".debit,
							   ".TABLE_TRANSACTION.".addedDate,
							   ".TABLE_TRANSACTION.".referenceNo,
							   ".TABLE_TRANSACTION.".transactionDate
			 FROM `".TABLE_TRANSACTION."` 
			WHERE $check
			AND `transactionDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' 
			AND fromLedger IN (4,6,8,10,12,14)
                        AND finYear=$finYear
			GROUP BY ".TABLE_TRANSACTION.".toLedger
			ORDER BY `transactionDate`,`ID` DESC";
			
			$result2= $db->query($listQuery2);
			$number2=mysql_num_rows($result2);	
			
			
							
				
				
				// Printing format starting
				
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
					
				$sql2 		= 	"SELECT * FROM `".TABLE_COMPANY."` where ID=$proId";
				$vou_record1 = 	$db->query_first($sql2);
				$company 	= 	$vou_record1['companyName'];
				$caddress 	= 	$vou_record1['address'];
				$cphone		=	$vou_record1['phone'];
				$cmobile	=	$vou_record1['mobile'];	
				$cemail		=	$vou_record1['email'];
				$cpin		=	$vou_record1['pin'];
					
				// set default header data
				$pdf->SetHeaderData(0, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING, array(0,0,0), array(0,0,0));
				
				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print
				
													
				
				//basic info
				$tbl_header1 = '<div><table style="width:430;"  cellspacing="0" border="0">';
				$tbl_footer1 = '</table></div>';
				$tbl1 = '';
				
				$tbl1	=	'<tr>
								<td colspan="8" align="left"><h2>'.strtoupper($company).'</h2></td>
							 </tr>
							 <tr>
								<td colspan="4" ><strong>DAY BOOK SUMMERY :FROM '.$fromdate.'</strong> </td>
								<td colspan="4" align="right"><strong>Date:'.date("d-m-Y").'</strong></td>
							 </tr>
							 
							 <tr>
								<td colspan="3" style="width:170px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black ">MODULE</td>
								<td colspan="4" style="width:270px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black " align="left">PARTY</td>
								<td colspan="5" align="right" style="width: 70px; border-top: 0.1px solid black;  border-bottom: 0.1px solid black " align="left">AMOUNT</td>
							 </tr>';
				
					
//***************************** For Invoice Module ***********************************//

					$td_c_in = 0;
					$tot_credit_in=0;
					
					//for getting module name as invoice once
					$count_in = 0;
					$module_in = "INVOICE";	
					$listRes2=$db->query($listQuery2);
					while($listRow2=mysql_fetch_array($listRes2)){	
								
						$lid	  =	$listRow2 ['ID'];
		
						$frled_id = $listRow2 ['fromLedger'];
						$toled_id = $listRow2 ['toLedger'];
								
						$from_sql 		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$frled_id";
						$from_record 	= 	$db->query_first($from_sql);
						$fromAccount	=	$from_record['accountName'];
								
						$to_sql 		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$toled_id";
						$to_record  	=   $db->query_first($to_sql);
						$toAccount		=	$to_record['accountName'];
							//echo $listRow2 ['credit'];die;
								
							//for getting module name as invoice once
						
						if($fromAccount=='Airline Receivable')
						{
							$fromAcc='Airline';
							$prefix='AI';
						}
						if($fromAccount=='Visa Receivable')
						{
							$fromAcc='Visa';
							$prefix='VI';
						}
						if($fromAccount=='Package Receivable')
						{
							$fromAcc='Package';
							$prefix='PI';
						}
						if($fromAccount=='Hotel Receivable')
						{
							$fromAcc='Hotel';
							$prefix='HI';
						}
						if($fromAccount=='Transport Receivable')
						{
							$fromAcc='Transport';
							$prefix='TI';
						}
						if($fromAccount=='Misc Receivable')
						{
							$fromAcc='Miscellaneous';
							$prefix='MI';
						}
						
							$count_in++;
						 	if($count_in!=1) 
						 	{
						 		$module_in	=	'';
						 	}
							
							
							$tbl1.=	'<tr>
								<td colspan="3">'.$module_in.'</td>
								<td colspan="4">'.$toAccount.'('.$prefix.')'.'</td>
								<td colspan="5" align="right">'.sprintf("%0.2f",round(abs($listRow2 ['credit']),2)).'</td>
								
							 </tr>';
							 	$tot_credit_in = $tot_credit_in+$listRow2 ['credit'];
								$td_c_in = ($td_c_in==0) ? 1 : 0;	
								 
							 }//end of while
							
							$tbl1.='<tr>
									<td colspan="4"></td>
									<td colspan="2"></td>
									<td colspan="2" align="right"; style="border-top:1px solid black;  border-bottom: 0.1px dashed black ">TOTAL</td>
									<td colspan="4" align="right" style="border-top: 0.1px solid black;  border-bottom: 0.1px dashed black ">'.sprintf("%0.2f",round(abs($tot_credit_in),2)).'</td>
									</tr>';	
									$tbl1.='<tr><td></td></tr>';	
					
					
//************************ For Purchase Module ****************************//					
					$td_c_pu = 0;
					$tot_debit_pu=0;
					//for getting module name as invoice once
					$count_pu = 0;	
					$module_pu = "PURCHASE";
						 
					$listRes=$db->query($listQuery);
							
					while($listRow=mysql_fetch_array($listRes)){							
						$lid	  =	$listRow ['ID'];
		
						$frled_id = $listRow ['fromLedger'];
						$toled_id = $listRow ['toLedger'];
								
						$from_sql 		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$frled_id";
						$from_record 	= 	$db->query_first($from_sql);
						$fromAccount	=	$from_record['accountName'];
								
						$to_sql 		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$toled_id";
						$to_record  	=   $db->query_first($to_sql);
						$toAccount		=	$to_record['accountName'];	
						
						
						if($fromAccount=='Airline Payable')
						{
							$fromAcc='Airline';
							$prefix='AP';
						}
						if($fromAccount=='Visa Payable')
						{
							$fromAcc='Visa';
							$prefix='VP';
						}
						if($fromAccount=='Package Payable')
						{
							$fromAcc='Package';
							$prefix='PP';
						}
						if($fromAccount=='Hotel Payable')
						{
							$fromAcc='Hotel';
							$prefix='HP';
						}
						if($fromAccount=='Transport Payable')
						{
							$fromAcc='Transport';
							$prefix='TP';
						}
						if($fromAccount=='Misc Payable')
						{
							$fromAcc='Miscellaneous';
							$prefix='MP';
						}
						
						
						//for getting module name as invoice once
							$count_pu++;
						 	if($count_pu!=1) 
						 	{
						 		$module_pu	=	'';
						 	}	 
							 
							 $tbl1.=	'<tr>
								<td colspan="3">'.$module_pu.'</td>
								<td colspan="4">'.$toAccount.'('.$prefix.')'.'</td>
								<td colspan="5" align="right">'.sprintf("%0.2f",round(abs($listRow ['debit']),2)).'</td>
								
							 </tr>';
							 
							 	$tot_debit_pu = $tot_debit_pu+$listRow ['debit'];
								$td_c_pu = ($td_c_pu==0) ? 1 : 0;
							 	
							 }
							$tbl1.='<tr>
									<td colspan="4"></td>
									<td colspan="2"></td>
									<td colspan="2" align="right"; style="border-top:1px solid black;  border-bottom: 0.1px dashed black ">TOTAL</td>
									<td colspan="4" align="right" style="border-top: 0.1px solid black;  border-bottom: 0.1px dashed black">'.sprintf("%0.2f",round(abs($tot_debit_pu),2)).'</td>
									</tr>';
									$tbl1.='<tr><td></td></tr>';
							
					
//************************ For Receipt Module ****************************//					
					$td_c_re = 0;
					$tot_debit_re=0;
					//for getting module name as invoice once
					$count_re = 0;	
					$module_re = "RECEIPT";
						 
					$listRes=$db->query($listQuery1);
							
					while($listRow=mysql_fetch_array($listRes)){							
						$lid	  =	$listRow ['ID'];
		
						$frled_id = $listRow ['fromLedger'];
						$toled_id = $listRow ['toLedger'];
								
						$from_sql 		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$frled_id";
						$from_record 	= 	$db->query_first($from_sql);
						$fromAccount	=	$from_record['accountName'];
								
						$to_sql 		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$toled_id";
						$to_record  	=   $db->query_first($to_sql);
						$toAccount		=	$to_record['accountName'];
						
						if($voucherType=='Cash Receipts')
						{
							
							$prefix='CR';
						}
						if($voucherType=='Bank Receipts')
						{
							$prefix='BR';
						}
						
						
						//for getting module name as invoice once
							$count_re++;
						 	if($count_re!=1) 
						 	{
						 		$module_re	=	'';
						 	}	 
							 
							 $tbl1.=	'<tr>
								<td colspan="3">'.$module_re.'</td>
								<td colspan="4">'.$toAccount.'('.$prefix.')'.'</td>
								<td colspan="5" align="right">'.sprintf("%0.2f",round(abs($listRow ['debit']),2)).'</td>
								
							 </tr>';
							 	$tot_debit_re = $tot_debit_re+$listRow ['debit'];
								$td_c_re = ($td_c_re==0) ? 1 : 0;
							 }//end of while
							 
							 $tbl1.='<tr>
									<td colspan="4"></td>
									<td colspan="2"></td>
									<td colspan="2" align="right"; style="border-top:1px solid black;  border-bottom: 0.1px dashed black ">TOTAL</td>
									<td colspan="4" align="right" style="border-top: 0.1px solid black;  border-bottom: 0.1px dashed black ">'.sprintf("%0.2f",round(abs($tot_debit_re),2)).'</td>
									</tr>';
							 
							 $tbl1.='<tr><td></td></tr>';
							 
							 
//******************** FOr Grand Total Module *******************************					 				
					$td_grand= 0;		
					$grand_total=0;
					
								$grand_total = $grand_total+$tot_debit_re+$tot_debit_pu+$tot_credit_in;
								$td_grand = ($td_grand==0) ? 1 : 0;
					
					$tbl1.='<tr>
								<td colspan="4" style="border-top:1px dashed black;border-bottom: 0.1px dashed black "></td>
								<td colspan="3" style="border-top:1px dashed black;  border-bottom: 0.1px dashed black ">GRAND TOTAL
								</td>
								<td colspan="5" align="right" style="border-top:1px dashed black;  border-bottom: 0.1px dashed black ">'.sprintf("%0.2f",round(abs($grand_total),2)).'</td>
							</tr>';
					
					
							
					$pdf->SetFontSize(9);
					$pdf->writeHTML($tbl_header1 . $tbl1 . $tbl_footer1, true, true, false, false, '');

					// Close and output PDF document
					$today	=	date("d-m-Y");
					$pdf->Output("'day_book'.$today.pdf", 'I');
				
			$db->close();
	break;
}
?>