<?php
require('../admin_header.php');
$type=$_SESSION['travelType'];

if(@isset($_SESSION['msg'])){
	echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

$loginType 	=	$_SESSION['travelType'];

if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"proId='$proId'";
}
?>
<script>
function fnExcelReport()
{
		var saleNum = document.getElementById('saleNum').value;
		var purNum  = document.getElementById('purNum').value;
		var payNum  = document.getElementById('payNum').value;
		var recNum  = document.getElementById('recNum').value;
		var from    = document.getElementById('from').value;
		var to      = document.getElementById('to').value; //alert(saleNum+purNum+payNum+recNum+from+to);
		
	var tab_text="<table border='2px'><tr><td colspan='4' style='text-align:center; font-size:11px;'><h3>PROFIT REPORT</h3></td></tr>";
	tab_text=tab_text+"<tr><td colspan='2' style='text-align:center;'><h6>From:"+from+"</h6></td><td colspan='2' style='text-align:center;'><h6>To:"+to+"</h6></td></tr>";
		//SALE		
		var j=0;
	try{	
		if(saleNum == -1)
		{
		   		tab_text=tab_text+"<tr><td  colspan='4' align='center'>There are no Reports in list.</td></tr>";
	    }	  
	    else if(saleNum !=0 && saleNum !=-1)
	    {
	    	tab1 		= document.getElementById('headerTable1');
	    	
			tab_text=tab_text+"<tr><td colspan='4' style='text-align:center;'><h6>SALE</h6></td></tr>";
			tab_text=tab_text+"<tr><td bgcolor='#87AFC6'>Date</td><td bgcolor='#87AFC6'>Particulars</td><td bgcolor='#87AFC6'>Account</td><td bgcolor='#87AFC6'>Amount</td></tr>";
		//<td bgcolor='#87AFC6'>Total Paid</td>
	    	firstColLength=tab1.rows[1].cells.length;//alert(firstColLength);
           for(j = 2 ; j < tab1.rows.length ; j++) 
          {     			
			 tab_text = tab_text + "<tr>";			
		  	 for(k = 0; k < tab1.rows[j].cells.length ; k++ ) 
			 {			 	
                tab_text=tab_text+"<td>"+tab1.rows[j].cells[k].innerHTML+"</td>";              
			 }
				tab_text = tab_text + "</tr>";
          }
	    }
	   //PURCHASE
	   
	   if(purNum == -1)
		{
		   		tab_text=tab_text+"<tr><td  colspan='4' align='center'>There are no Reports in list.</td></tr>";
	    }
	  
	   else if(purNum !=0 && purNum !=-1)
	    {	   
	    	tab2 		= document.getElementById('headerTable2'); 
	    	tab_text=tab_text+"<tr><td colspan='4' style='text-align:center;'><h6>PURCHASE</h6></td></tr>";
			tab_text=tab_text+"<tr><td bgcolor='#87AFC6'>Date</td><td bgcolor='#87AFC6'>Particulars</td><td bgcolor='#87AFC6'>Account</td><td bgcolor='#87AFC6'>Amount</td></tr>";	
           for(j = 2 ; j < tab2.rows.length ; j++) 
          {     			
			 tab_text = tab_text + "<tr>";			
		  	 for(k = 0; k < tab2.rows[j].cells.length ; k++ ) 
			 {			 	
                tab_text=tab_text+"<td>"+tab2.rows[j].cells[k].innerHTML+"</td>";              
			 }
				tab_text = tab_text + "</tr>";
          }
	   }        
	   //payment
	    if(payNum == -1)
		{
		   		tab_text=tab_text+"<tr><td  colspan='4' align='center'>There are no Reports in list.</td></tr>";
	    }
	  
	     else if(payNum !=0 && payNum !=-1)
	    {	   
	    	tab3 		= document.getElementById('headerTable3'); 
	    	tab_text=tab_text+"<tr><td colspan='4' style='text-align:center;'><h6>PAYMENT</h6></td></tr>";
			tab_text=tab_text+"<tr><td bgcolor='#87AFC6'>Date</td><td bgcolor='#87AFC6'>Particulars</td><td bgcolor='#87AFC6'>Account</td><td bgcolor='#87AFC6'>Amount</td></tr>";	
           for(j = 2 ; j < tab3.rows.length ; j++) 
          {     			
			 tab_text = tab_text + "<tr>";			
		  	 for(k = 0; k < tab3.rows[j].cells.length ; k++ ) 
			 {			 	
                tab_text=tab_text+"<td>"+tab3.rows[j].cells[k].innerHTML+"</td>";              
			 }
				tab_text = tab_text + "</tr>";
          }
	   }      
	   //receipt
	    if(recNum == -1)
		{
		   		tab_text=tab_text+"<tr><td  colspan='4' align='center'>There are no Reports in list.</td></tr>";
	    }
	  
	   else if(recNum !=0 && recNum !=-1)
	    {	   
	    	tab4 		= document.getElementById('headerTable4'); 
	    	tab_text=tab_text+"<tr><td colspan='4' style='text-align:center;'><h6>RECEIPT</h6></td></tr>";
			tab_text=tab_text+"<tr><td bgcolor='#87AFC6'>Date</td><td bgcolor='#87AFC6'>Particulars</td><td bgcolor='#87AFC6'>Account</td><td bgcolor='#87AFC6'>Amount</td></tr>";	
           for(j = 2 ; j < tab4.rows.length ; j++) 
          {     			
			 tab_text = tab_text + "<tr>";			
		  	 for(k = 0; k < tab4.rows[j].cells.length ; k++ ) 
			 {			 	
                tab_text=tab_text+"<td>"+tab4.rows[j].cells[k].innerHTML+"</td>";              
			 }
				tab_text = tab_text + "</tr>";
          }
	   }        
}
catch(err)
{
 alert(err);
}
          tab_text=tab_text+"</table>";
		  tab_text= tab_text.replace("<tr></tr>", "");
		  tab_text= tab_text.replace("<tr>  </tr>", "");
	
          tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
          tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
                      tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

               var ua = window.navigator.userAgent;
              var msie = ua.indexOf("MSIE "); 

                 if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                    {
                           txtArea1.document.open("txt/html","replace");
                           txtArea1.document.write(tab_text);						   
                           txtArea1.document.close();
                           txtArea1.focus(); 
                           sa=txtArea1.document.execCommand("SaveAs",true,"");
                     }  
                  else                 //other browser not tested on IE 11
                      sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  


                      return (sa);


	}
</script>

<div class="row">
	<div class="col-lg-12">
		<div class="page_header">
			<div class="page_heading">
				<h3>PROFIT REPORT</h3>                   
			</div>
			<div class="page_controls">
				<form method="post">
				<?php if($loginType	=='Admin'){?>
					<div class="page_search">
						<select name="company" required="" >
							<option value="">Select Company</option>
							<?php $cmpQry	=	mysql_query("SELECT * FROM ".TABLE_COMPANY);
							while($cmpRow		=	mysql_fetch_array($cmpQry))
							{
							
							?>
							<option value="<?php echo $cmpRow['ID'];?>"  <?php if(@$_REQUEST['company']==$cmpRow['ID']){ echo "selected";}?>><?php echo $cmpRow['companyName'];?></option>
							<?php 	
							}?>
						</select>
					</div>
					<?php }?>
					<div class="page_search">					
					<input type="checkbox" name="salePurchase" value="salePurchase" <?php if(@$_REQUEST['salePurchase']){ echo 'checked'; }?>>Sale & Purchase
					
					<input type="checkbox" name="cashPayment" value="payment" <?php if(@$_REQUEST['cashPayment']){ echo 'checked'; }?>>Payment
					<input type="checkbox" name="cashReceipt" value="receipt" <?php if(@$_REQUEST['cashReceipt']){ echo 'checked'; }?>>Receipt
					</div>
					<br><br>
					<div class="page_search">
						<select name="searchType"  id="searchType">
							<option value="Customer">Customer</option>
							<option value="Supplier">Supplier</option>
						</select>
					</div>
					<div class="page_search">
						<input  type="search" name="searchName"  id="searchName" placeholder="Name" >
					</div>
					<div class="page_search">
						<input  type="text" name="from"  id="from" placeholder="From" class="user_date"  value="<?php if(isset($_REQUEST['from'])){ echo $_REQUEST['from']; } ?>">
					</div>
					<div class="page_search">                      
						<input type="text" name="to" id="to" placeholder="To" class="user_date" value="<?php if(isset($_REQUEST['to'])){ echo $_REQUEST['to']; } ?>">                        
					</div>
					
					<div class="page_search">
						<button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
					</div>
					<button onclick="fnExcelReport();"  class="btn btn-default export" title="Export To Excel"></button>
				</form>
				 
			</div>

			<div class="bd_clear"></div>
		</div>
	</div>
</div>
<?php 
			$fromdate	= 	date('d-m-Y');
			$todate		=	date('d-m-Y');
							
			if(@$_POST['from']){
				$fromdate	=	$_POST['from'];			
			}		
			if(@$_POST['to']){				
				$todate		=	$_POST['to'];
			}
			if($loginType=='Admin')
			{
				if(@$_POST['company'])
				{
					$proId	=	@$_POST['company'];
				}
				else
				{
					$proId	=	0;
				}
				$check 	= 	" proId='$proId'";
			}
			$count=0;
			
			if(@$_POST['salePurchase']){				
				$salePurchase			=	$_POST['salePurchase'];
				$count++;
			}
			
			if(@$_POST['cashPayment']){				
				$cashPayment	=	$_POST['cashPayment'];
				$count++;
			}
			if(@$_POST['cashReceipt']){				
				$cashReceipt	=	$_POST['cashReceipt'];
				$count++;
			}
			/*   ******************************   */
			
			
			if(isset($_POST['submit']))
			{
			
			?>
<div class="row">
	<div class="col-lg-12">
		<div class="show_table_wrap">								
			<div class="row">			
			<?php
			$completePaid=$saleNum=$purchaseNum=$payNum=$recNum=$profitSale=$profitPurchase=$tot_debit=0;
			
			if(@$salePurchase)
			{
				$sale = "SELECT a.ID sId,a.invoiceNo sInvNo,a.invoiceDate sInvDate,a.customerId sCusId,a.netAmount sNet, 'Airline' as invType,b.ID pId,b.invoiceNo pInvNo,b.invoiceDate pInvDate,b.supplierId pSupId,b.netAmount pNet FROM `".TABLE_INVOICE_AIRLINE."` a left join `".TABLE_PURCHASE_AIRLINE."` b on a.invoiceNo=b.ourInvoiceNo AND b.$check  WHERE ((a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."')OR(b.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."')) AND a.$check AND a.finYear=$finYear
				UNION
		SELECT a.ID sId,a.invoiceNo sInvNo,a.invoiceDate sInvDate,a.customerId sCusId,a.netAmount sNet, 'Hotel' as invType,b.ID pId,b.invoiceNo pInvNo,b.invoiceDate pInvDate,b.supplierId pSupId,b.netAmount pNet FROM `".TABLE_INVOICE_HOTEL."` a left join `".TABLE_PURCHASE_HOTEL."` b on a.invoiceNo=b.ourInvoiceNo AND b.$check WHERE ((a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."')OR(b.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."')) AND a.$check AND a.finYear=$finYear
		UNION
		SELECT a.ID sId,a.invoiceNo sInvNo,a.invoiceDate sInvDate,a.customerId sCusId,a.netAmount sNet, 'Miscellaneous' as invType,b.ID pId,b.invoiceNo pInvNo,b.invoiceDate pInvDate,b.supplierId pSupId,b.netAmount pNet FROM `".TABLE_INVOICE_MISC."` a left join `".TABLE_PURCHASE_MISC."` b on a.invoiceNo=b.ourInvoiceNo AND b.$check  WHERE ((a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."') OR (b.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."')) AND a.$check AND a.finYear=$finYear
		UNION
		SELECT a.ID sId,a.invoiceNo sInvNo,a.invoiceDate sInvDate,a.customerId sCusId,a.netAmount sNet, 'Transportation' as invType,b.ID pId,b.invoiceNo pInvNo,b.invoiceDate pInvDate,b.supplierId pSupId,b.netAmount pNet FROM `".TABLE_INVOICE_TRANSPORT."` a left join `".TABLE_PURCHASE_TRANSPORT."` b on a.invoiceNo=b.ourInvoiceNo AND b.$check WHERE ((a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."') OR (b.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."')) AND a.$check AND a.finYear=$finYear
		UNION
		SELECT a.ID sId,a.invoiceNo sInvNo,a.invoiceDate sInvDate,a.customerId sCusId,a.netAmount sNet, 'Visa' as invType,b.ID pId,b.invoiceNo pInvNo,b.invoiceDate pInvDate,b.supplierId pSupId,b.netAmount pNet FROM `".TABLE_INVOICE_VISA."` a left join `".TABLE_PURCHASE_VISA."` b on a.invoiceNo=b.ourInvoiceNo AND b.$check WHERE ((a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."') OR (b.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."')) AND a.$check AND a.finYear=$finYear
		UNION
		SELECT a.ID sId,a.`invoiceNo` sInvNo,a.`invoiceDate` sInvDate,a.`customerId` sCusId,a.`netAmount` sNet, 'Package' as invType,'0' pId,'0' pInvNo,'0' pInvDate,'0' pSupId,(COALESCE(SUM(b.netAmount),0) + COALESCE(SUM(c.netAmount),0) + COALESCE(SUM(d.netAmount),0) + COALESCE(SUM(e.netAmount),0)) as pNet FROM `".TABLE_INVOICE_PACKAGE."` a LEFT JOIN `".TABLE_PURCHASE_PACKAGE_AIRLINE."` b ON b.in_packageId=a.ID AND b.$check  LEFT JOIN `".TABLE_PURCHASE_PACKAGE_HOTEL."` c ON c.in_packageId=a.ID AND c.$check LEFT JOIN `".TABLE_PURCHASE_PACKAGE_VISA."` d ON d.in_packageId=a.ID AND d.$check LEFT JOIN `".TABLE_PURCHASE_PACKAGE_TRANSPORT."` e ON e.in_packageId=a.ID AND e.$check WHERE (a.`invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' ) AND a.$check AND a.finYear=$finYear GROUP BY a.ID
		 ";
		//echo $sale;die;
		$saleRes= $db->query($sale);
		//echo($sale);
		$saleNum=mysql_num_rows($saleRes);//echo $saleNum;
		?>
		<div  class="<?php if($count==1){ ?>col-lg-12 col-md-12 col-sm-12
					<?php } if($count==2){ ?>col-lg-6 col-md-6 col-sm-6
					<?php } if($count==3){ ?>col-lg-4 col-md-4 col-sm-4  table-responsive <?php } ?>">
					<div class="table-responsive">
						<table class="table table-bordered table-striped show_table report_table" id="headerTable1">			
				<thead>
					<tr>						
						<th colspan="11" style="text-align: center">SALE & PURCHASE</th>						
					</tr>
					<tr>	
						<th>Date</th>					
						<th>Particulars</th>
						<th>Customer</th>						
						<th>Amount</th>	
						<th>Invoice</th>
						<th>Date</th>
						<th>Particulars</th>										
						<th>Supplier</th>						
						<th>Amount</th>	
						<th>Purchase</th>
						<th>Profit</th>					
					</tr>
				</thead>
				<tbody>
					<?php
					if($saleNum==0){
						$saleNum=-1;
						?>
						<tr>
							<td colspan="11" align="center">
								No data found
							</td>
						</tr>
						<?php
					}
					else{
					$td_c = 0;
					$totAmount=0;
					$totPaid=0;
					
					while($saleRow=mysql_fetch_array($saleRes)){
						$invoiceNo	    =	$saleRow ['sInvNo'];		
						$invoiceDate 	=   $saleRow ['sInvDate'];
						$customerId		= 	$saleRow ['sCusId'];
						$netAmount		= 	$saleRow ['sNet'];	
						$invoiceType 	= 	$saleRow ['invType'];
							
						$cusQuery		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$customerId";
						$cusRecord		= 	$db->query_first($cusQuery);//echo $cusQuery;
						$cusName		=	$cusRecord['accountName'];
						
						$invoiceNo2	    =	$saleRow ['pInvNo'];		
						$invoiceDate2 	=   $saleRow ['pInvDate'];
						$supplierId2	= 	$saleRow ['pSupId'];
						$netAmount2		= 	$saleRow ['pNet'];	
						//$invoiceType 	= 	$saleRow ['invType'];
						
						
							if($invoiceType=='Airline')
							{
								$prefix = 'AI';
								$prefix2 = 'AP';
								$type   = 'airline';							
							}
							if($invoiceType=='Hotel')
							{
								$prefix='HI';
								$prefix2='HP';
								$type   = 'hotel';
							}
							if($invoiceType=='Miscellaneous')
							{
								$prefix='MI';
								$prefix2='MP';
								$type   = 'miscellaneous';
							}
							if($invoiceType=='Transportation')
							{
								$prefix='TI';
								$prefix2='TP';
								$type   = 'transportation';
							}
							if($invoiceType=='Visa')
							{
								$prefix='VI';
								$prefix2='VP';
								$type   = 'visa';
							}
							if($invoiceType=='Package')
							{
								$prefix='PI';
								$prefix2='PP';
								$type   = 'package';
							}
							if($invoiceType=='Package Airline')
							{
								$prefix2='PAP';
							}
							if($invoiceType=='Package Hotel')
							{
								$prefix2='PHP';
							}
							if($invoiceType=='Package Transport')
							{
								$prefix2='PTP';
							}
							if($invoiceType=='Package Visa')
							{
								$prefix2='PVP';
							}
							
						$cashReceiptQry="SELECT sum(payingAmount) as paidAmt FROM `".TABLE_CASH_RECEIPT."` p 
											WHERE voucherType='$type' 
											and invoiceNo='$invoiceNo' 
											AND p.$check 
										GROUP BY invoiceNo,voucherType"; 
								$cashRes= $db->query($cashReceiptQry);
								$cashNum= mysql_num_rows($cashRes);	
								if($cashNum==0)
								{
									$paidAmount=0;
								}
								else{
									$cashRow=mysql_fetch_array($cashRes);
									$paidAmount=$cashRow['paidAmt'];
								}
							?>
							<tr <?php if($td_c==1){ ?> style="background:#FFFFCC; color:#333333;" <?php } ?>>
								<td><?php echo $App->dbformat_date($invoiceDate,'d-m-Y'); ?></td>			
								<td><?php echo $invoiceType."(INV#".$prefix.$invoiceNo.")"; ?></td>							
								<td><?php echo $cusName; ?></td>
								<td><?php echo $netAmount; ?></td>	
								<td><a class="show_table_lnk show_table_lnk_view" 
								<?php if($invoiceType=='Airline'){
									?>
									href="../invoice_airline/view.php?id=<?php echo $saleRow ['sId'];?>"	
									<?php
								}
								elseif($invoiceType=='Hotel'){
									?>
									href="../invoice_hotel/view.php?id=<?php echo $saleRow ['sId'];?>"
									<?php
								}
								elseif($invoiceType=='Miscellaneous'){
									?>
									href="../invoice_misc/view.php?id=<?php echo $saleRow ['sId'];?>"
									<?php
								}
								elseif($invoiceType=='Transportation'){
									?>
									href="../invoice_transport/view.php?id=<?php echo $saleRow ['sId'];?>"	
									<?php
								}elseif($invoiceType=='Visa'){
									?>
									href="../invoice_visa/view.php?id=<?php echo $saleRow ['sId'];?>"	
									<?php
								}
								elseif($invoiceType=='Package'){
									?>
									href="../invoice_packages/view.php?id=<?php echo $saleRow ['sId'];?>"	
									<?php
								}
								?>
								>View</a></td>
								<?php
								if($invoiceNo2){
						$supQuery		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$supplierId2";
						$supRecord		= 	$db->query_first($supQuery);
						$supName		=	$supRecord['accountName'];
						?>
						<td><?php echo $App->dbformat_date($invoiceDate2,'d-m-Y'); ?></td>	
						<td><?php echo $invoiceType."(INV#".$prefix2.$invoiceNo2.")"; ?></td>	
						<td><?php echo $supName; ?></td>
						<td><?php echo $netAmount2; ?></td>	
					 
						<td><a class="show_table_lnk show_table_lnk_view" 
							<?php if($invoiceType=='Airline'){
								?>
								href="../purchase/view.php?id=<?php echo $saleRow ['pId'];?>&type=a"	
								<?php
							}
							elseif($invoiceType=='Hotel'){
								?>
								href="../purchase/view.php?id=<?php echo $saleRow ['pId'];?>&type=h"
								<?php
							}
							elseif($invoiceType=='Miscellaneous'){
								?>
								href="../purchase/view.php?id=<?php echo $saleRow ['pId'];?>&type=m"
								<?php
							}
							elseif($invoiceType=='Transportation'){
								?>
								href="../purchase/view.php?id=<?php echo $saleRow ['pId'];?>&type=t"	
								<?php
							}elseif($invoiceType=='Visa'){
								?>
								href="../purchase/view.php?id=<?php echo $saleRow ['pId'];?>&type=v"
								<?php
							}
							elseif($invoiceType=='Package' || $invoiceType=='Package Airline' || $invoiceType=='Package Hotel'|| $invoiceType=='Package Transport' ||$invoiceType=='Package Visa'){
								?>
								href="#"
								<?php
							}
							?>
							>View</a></td>
							<td><?php echo $netAmount-$netAmount2; ?></td>
						<?php
						}
						else if($invoiceType=='Package')
						{
							?>
							<td></td>			
							<td></td>							
							<td></td>
							<td><?php echo $netAmount2; ?></td>	
							<td></td>
							<td></td>
							<?php
						}
						else
						{
						?>
							<td></td>			
							<td></td>							
							<td></td>
							<td></td>	
							<td></td>
							<td></td>
						<?php	
						}
						?>											
							</tr>
							<?php
							if($netAmount==$paidAmount)
							{
								global $completePaid;
								$completePaid=$completePaid+$netAmount;
							}
							$totAmount = $totAmount+$netAmount;
							$totPaid = $totPaid+$paidAmount;
							$tot_debit = $tot_debit+$netAmount2;
							$td_c = ($td_c==0) ? 1 : 0;
						}	
						?>
					<tr <?php if($td_c==1){ ?> style="background:#FFFFCC; color:#333333;" <?php } ?> >					  
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Total</td>							
						<td><?php echo $totAmount; ?></td>
						<td>&nbsp;</td>
						<!--<td><?php echo $totPaid; ?></td>-->
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Total</td>	
						<td><?php echo $tot_debit; ?></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr  style="background:#fafafa; color:#333333;">						
						<th colspan="8">Profit</th>
						<th colspan="3"><?php echo abs($totAmount-$tot_debit);?></th>												
					</tr>
				</tbody>
				<?php }?>
			</table>
					</div>
					
		</div>							
			<?php  }
			/*if(@$purchase)
			{
			$purchase="SELECT a.`invoiceNo`,a.`invoiceDate`,a.`supplierId`,a.`netAmount`, 'Airline' as invType FROM `".TABLE_PURCHASE_AIRLINE."` a WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check 
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`supplierId`,a.`netAmount`, 'Hotel' as invType FROM `".TABLE_PURCHASE_HOTEL."` a WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`supplierId`,a.`netAmount`, 'Miscellaneous' as invType FROM `".TABLE_PURCHASE_MISC."` a WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check 
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`supplierId`,a.`netAmount`, 'Transportation' as invType FROM `".TABLE_PURCHASE_TRANSPORT."` a WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`supplierId`,a.`netAmount`, 'Visa' as invType FROM `".TABLE_PURCHASE_VISA."` a WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check 
		
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`supplierId`,a.`netAmount`, 'Package Airline' as invType FROM `".TABLE_PURCHASE_PACKAGE_AIRLINE."` a WHERE `purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`supplierId`,a.`netAmount`, 'Package Hotel' as invType FROM `".TABLE_PURCHASE_PACKAGE_HOTEL."` a WHERE `purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check 
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`supplierId`,a.`netAmount`, 'Package Transport' as invType FROM `".TABLE_PURCHASE_PACKAGE_TRANSPORT."` a WHERE `purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check 
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`supplierId`,a.`netAmount`, 'Package Visa' as invType FROM `".TABLE_PURCHASE_PACKAGE_VISA."` a WHERE `purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check "; 
			
			//echo $purchase;
			$purchaseRes= $db->query($purchase);
			$purchaseNum=mysql_num_rows($purchaseRes);		
			?>
				<div  class="<?php if($count==1){ ?>col-lg-12 col-md-12 col-sm-12
							<?php } if($count==2){ ?>col-lg-6 col-md-6 col-sm-6
							<?php } if($count==3){ ?>col-lg-4 col-md-4 col-sm-4 table-responsive
							<?php } if($count==4){ ?>col-lg-3 col-md-3 col-sm-3 table-responsive <?php } ?>">
					<table class="table table-bordered table-striped show_table report_table" id="headerTable2">			
				<thead>
					<tr>						
						<th colspan="4" style="text-align: center">PURCHASE</th>						
					</tr>
					<tr>	
											
					</tr>
				</thead>
				<tbody>
					<?php
					if($purchaseNum==0){
						$purchaseNum=-1;
						?>
						<tr>
							<td colspan="4" align="center">
								No data found
							</td>
						</tr>
						<?php
					}
					else{
					?>
					<?php
					$td_c = 0;
					$tot_debit=0;
					
					while($purchaseRow=mysql_fetch_array($purchaseRes)){							
						$invoiceNo	    =	$purchaseRow ['invoiceNo'];		
						$invoiceDate 	=   $purchaseRow ['invoiceDate'];
						$supplierId		= 	$purchaseRow ['supplierId'];
						$netAmount		= 	$purchaseRow ['netAmount'];	
						$invoiceType 	= 	$purchaseRow ['invType'];
							
						$supQuery		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$supplierId";
						$supRecord		= 	$db->query_first($supQuery);
						$supName		=	$supRecord['accountName'];
						if($invoiceType=='Airline')
							{
								$prefix='IA';
							}
							if($invoiceType=='Hotel')
							{
								$prefix='IH';
							}
							if($invoiceType=='Miscellaneous')
							{
								$prefix='IM';
							}
							if($invoiceType=='Transportation')
							{
								$prefix='IT';
							}
							if($invoiceType=='Visa')
							{
								$prefix='IV';
							}
							if($invoiceType=='Package')
							{
								$prefix='IP';
							}
							
							if($invoiceType=='Package Airline')
							{
								$prefix='PA';
							}
							if($invoiceType=='Package Hotel')
							{
								$prefix='PH';
							}
							if($invoiceType=='Package Transport')
							{
								$prefix='PT';
							}
							if($invoiceType=='Package Visa')
							{
								$prefix='PV';
							}
							
						?>
						<tr <?php if($td_c==1){ ?> style="background:#FFFFCC; color:#333333;" <?php } ?>>
								<td><?php echo $App->dbformat_date($invoiceDate,'d-m-Y'); ?></td>			
								<td><?php echo $invoiceType."(INV#".$prefix.$invoiceNo.")"; ?></td>							
								<td><?php echo $supName; ?></td>
								<td><?php echo $netAmount; ?></td>													
							</tr>
						<?php
						$tot_debit = $tot_debit+$netAmount;
						$td_c = ($td_c==0) ? 1 : 0;
					}
							
					?>
					<tr <?php if($td_c==1){ ?> style="background:#FFFFCC; color:#333333;" <?php } ?> >					  
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Total</td>
						<td><?php echo $tot_debit; ?></td>
					</tr>
										                                         
				</tbody>
				<?php }?>
			</table>
				</div>	
			<?php }*/
			if(@$cashPayment)
			{	
			$subId	=	$db->subAccount(12);
			$payQuery="SELECT * FROM `".TABLE_TRANSACTION."` 
			WHERE $check
			AND finYear=$finYear
			AND `transactionDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' 
			AND ((voucherType='Cash Payments' && fromLedger=2)|| (voucherType='Bank Payments' and fromLedger IN(select ID from ".TABLE_ACCOUNTS." WHERE accountGroupId IN($subId))))
			ORDER BY `transactionDate`,`ID` DESC"; //echo $payQuery;
			$payRes= $db->query($payQuery);
			$payNum=mysql_num_rows($payRes);		
			?>
				<div  class="<?php if($count==1){ ?>col-lg-12 col-md-12 col-sm-12
							<?php } if($count==2){ ?>col-lg-6 col-md-6 col-sm-6
							<?php } if($count==3){ ?>col-lg-4 col-md-4 col-sm-4  table-responsive
							<?php }  ?>">
					<table class="table table-bordered table-striped show_table report_table" id="headerTable3">			
				<thead>
					<tr>						
						<th colspan="4" style="text-align: center">PAYMENTS</th>						
					</tr>
					<tr>	
						<th>Date</th>					
						<th>Particulars</th>
						<th>Account</th>						
						<th>Payments</th>						
					</tr>
				</thead>
				<tbody>
					<?php
					if($payNum==0){
						$payNum=-1;
						?>
						<tr>
							<td colspan="4" align="center">
								No data found
							</td>
						</tr>
						<?php
					}
					else{
					?>
					<?php
					$td_c = 0;
					$pay_credit=0;
					
							
					while($payRow=mysql_fetch_array($payRes)){							
						$lid	   = $payRow ['ID'];		
						$frled_id  = $payRow ['fromLedger'];
						$toled_id  = $payRow ['toLedger'];
						$invoiceNo = $payRow ['voucherNo'];
								
						$from_sql 		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$frled_id";
						$from_record 	= 	$db->query_first($from_sql);
						$fromAccount	=	$from_record['accountName'];
								
						$to_sql 		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$toled_id";
						$to_record  	=   $db->query_first($to_sql);
						$toAccount		=	$to_record['accountName'];
						
						if($frled_id==2)
						{
							$fromAcc='Cash Payments';
							$prefix='CP';
						}
						else
						{
							$fromAcc='Bank Payments';
							$prefix='BP';
						}
						?>
						<tr <?php if($td_c==1){ ?> style="background:#FFFFCC; color:#333333;" <?php } ?>>
							<td><?php echo $App->dbformat_date($payRow ['transactionDate'],'d-m-Y'); ?></td>
						<?php if($frled_id!=2){ ?>
							<td><?php echo $fromAcc."(INV#".$prefix.$invoiceNo.")(".$fromAccount.")"; ?></td>
							<?php } 
							else {?>
							<td><?php echo $fromAcc."(INV#".$prefix.$invoiceNo.")"; ?></td>							
							<?php } ?>			
							
							<td><?php echo $toAccount; ?></td>
							<td><?php echo $payRow ['credit']; ?></td>												
						</tr>
						<?php
						$pay_credit = $pay_credit+$payRow ['credit'];
						$td_c = ($td_c==0) ? 1 : 0;
					}
							
					?>
					<tr <?php if($td_c==1){ ?> style="background:#FFFFCC; color:#333333;" <?php } ?> >					  
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Total</td>
						<td><?php echo abs($pay_credit); ?></td>
					</tr>
											                                         
				</tbody>
				<?php }?>
			</table>
				</div>				
			<?php		
			  } 
			if(@$cashReceipt)
			{			
			$subId	=	$db->subAccount(12);
			$recQuery="SELECT * FROM `".TABLE_TRANSACTION."` 
			WHERE $check
			AND `transactionDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' 
			AND ((voucherType='Cash Receipts' && fromLedger=2)|| (voucherType='Bank Receipts' and fromLedger IN(select ID from ".TABLE_ACCOUNTS." WHERE accountGroupId IN($subId))))
			ORDER BY `transactionDate`,`ID` DESC"; //echo $payQuery;
			$recRes= $db->query($recQuery);
			$recNum=mysql_num_rows($recRes);		
			?>
				<div  class="<?php if($count==1){ ?>col-lg-12 col-md-12 col-sm-12
							<?php } if($count==2){ ?>col-lg-6 col-md-6 col-sm-6
							<?php } if($count==3){ ?>col-lg-4 col-md-4 col-sm-4 table-responsive
							<?php } ?>">
					<table class="table table-bordered table-striped show_table report_table" id="headerTable4">			
				<thead>
					<tr>						
						<th colspan="4" style="text-align: center">RECEIPTS</th>						
					</tr>
					<tr>	
						<th>Date</th>					
						<th>Particulars</th>
						<th>Account</th>						
						<th>Payments</th>						
					</tr>
				</thead>
				<tbody>
					<?php
					if($recNum==0){
						$recNum=-1;
						?>
						<tr>
							<td colspan="4" align="center">
								No data found
							</td>
						</tr>
						<?php
					}
					else{
					?>
					<?php
					$td_c = 0;
					$pay_debit=0;
					
							
					while($recRow=mysql_fetch_array($recRes)){							
						$lid	   = $recRow ['ID'];		
						$frled_id  = $recRow ['fromLedger'];
						$toled_id  = $recRow ['toLedger'];
						$invoiceNo = $recRow ['voucherNo'];
								
						$from_sql 		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$frled_id";
						$from_record 	= 	$db->query_first($from_sql);
						$fromAccount	=	$from_record['accountName'];
								
						$to_sql 		=	"SELECT * FROM `".TABLE_ACCOUNTS."` WHERE ID=$toled_id";
						$to_record  	=   $db->query_first($to_sql);
						$toAccount		=	$to_record['accountName'];
						
						if($frled_id==2)
						{
							$fromAcc='Cash Receipts';
							$prefix='CP';
						}
						else
						{
							$fromAcc='Bank Receipts';
							$prefix='BP';
						}
						?>
						<tr <?php if($td_c==1){ ?> style="background:#FFFFCC; color:#333333;" <?php } ?>>
						<td><?php echo $App->dbformat_date($recRow ['transactionDate'],'d-m-Y'); ?></td>
						<?php if($frled_id!=2){ ?>
							<td><?php echo $fromAcc."(INV#".$prefix.$invoiceNo.")(".$fromAccount.")"; ?></td>
							<?php } 
							else {?>
							<td><?php echo $fromAcc."(INV#".$prefix.$invoiceNo.")"; ?></td>							
							<?php } ?>			
							
							<td><?php echo $toAccount; ?></td>
							<td><?php echo $recRow ['debit']; ?></td>												
						</tr>
						<?php
						$pay_debit = $pay_debit+$recRow ['debit'];
						$td_c = ($td_c==0) ? 1 : 0;
					}
							
					?>
					<tr <?php if($td_c==1){ ?> style="background:#FFFFCC; color:#333333;" <?php } ?> >					  
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Total</td>
						<td><?php echo abs($pay_debit); ?></td>
					</tr>
											                                         
				</tbody>
				<?php }?>
			</table>				
				</div>				
			<?php } ?>					
			</div>
			<?php 
			/*$saleProfit = "SELECT sum(a.`netAmount`) as net  FROM `".TABLE_INVOICE_AIRLINE."` a WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check 
		UNION
		SELECT sum(b.`netAmount`)  as net   FROM `".TABLE_INVOICE_HOTEL."` b WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND b.$check
		UNION
		SELECT sum(c.`netAmount`)  as net   FROM `".TABLE_INVOICE_MISC."` c WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND c.$check 
		UNION
		SELECT sum(d.`netAmount`) as net   FROM `".TABLE_INVOICE_TRANSPORT."` d WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND d.$check
		UNION
		SELECT sum(e.`netAmount`)  as net   FROM `".TABLE_INVOICE_VISA."` e WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND e.$check 
		UNION
		SELECT sum(f.`netAmount`)  as net   FROM `".TABLE_INVOICE_PACKAGE."` f WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND f.$check"; 
		//echo $saleProfit;
		$saleProfitRes= $db->query($saleProfit);
		$saleProfitNum=mysql_num_rows($saleProfitRes);	
		while($saleProfitRow=mysql_fetch_array($saleProfitRes)){
						
						$a=$saleProfitRow ['net'];						
						$profitSale+=$a;
						}
						
		//profitPurchase
		$purchaseProfit="SELECT sum(a.`netAmount`) as net2  FROM `".TABLE_PURCHASE_AIRLINE."` a WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check 
		UNION
		SELECT sum(a.`netAmount`) as net2 FROM `".TABLE_PURCHASE_HOTEL."` a WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check
		UNION
		SELECT sum(a.`netAmount`) as net2 FROM `".TABLE_PURCHASE_MISC."` a WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check 
		UNION
		SELECT sum(a.`netAmount`) as net2 FROM `".TABLE_PURCHASE_TRANSPORT."` a WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check
		UNION
		SELECT sum(a.`netAmount`) as net2 FROM `".TABLE_PURCHASE_VISA."` a WHERE `invoiceDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check 
		
		UNION
		SELECT sum(a.`netAmount`) as net2 FROM `".TABLE_PURCHASE_PACKAGE_AIRLINE."` a WHERE `purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check
		UNION
		SELECT sum(a.`netAmount`) as net2 FROM `".TABLE_PURCHASE_PACKAGE_HOTEL."` a WHERE `purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check 
		UNION
		SELECT sum(a.`netAmount`) as net2 FROM `".TABLE_PURCHASE_PACKAGE_TRANSPORT."` a WHERE `purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check 
		UNION
		SELECT sum(a.`netAmount`) as net2 FROM `".TABLE_PURCHASE_PACKAGE_VISA."` a WHERE `purchaseDate` BETWEEN '". $App->dbformat_date($fromdate)."' AND '". $App->dbformat_date($todate)."' AND a.$check "; 
			
			//echo $purchase;
			$purchaseProfitRes= $db->query($purchaseProfit);
			$purchaseProfitNum=mysql_num_rows($purchaseProfitRes);													while($purchaseProfitRow=mysql_fetch_array($purchaseProfitRes)){
						
						$b=$purchaseProfitRow ['net2'];						
						$profitPurchase+=$b;
						}	*/
			?>
			<!--<table class="table table-bordered" id="headerTable">			
				<thead>
					<tr  style="background:#fafafa; color:#333333;">							
						
						<th>Profit</th>
						<th><?php echo $profitSale-$profitPurchase;?></th>												
					</tr>
				</thead>
			</table>	-->	
				<input type="hidden" id="saleNum" name="saleNum" value="<?php echo @$saleNum; ?>">
				<input type="hidden" id="purNum" name="purNum" value="<?php echo @$purchaseNum; ?>">
				<input type="hidden" id="payNum" name="payNum" value="<?php echo @$payNum; ?>">
				<input type="hidden" id="recNum" name="recNum" value="<?php echo @$recNum; ?>">
		</div>		
	</div>
</div>
<?php } ?>
<script>
	$(function () {
			$('.user_date').each(function () {
					var curDatePicker = $(this);
					//alterTarget = curDatePicker.nextAll('.date_hidden');
					curDatePicker.datepicker({
							dateFormat: "dd-mm-yy"
							/*altFormat: "yy-mm-dd",
							altField: alterTarget*/
						});
				});
		});
</script>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>