<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

//for edit and delete permission
$logID = $_SESSION['travelId'];

if($_SESSION['travelType']=='Admin')
{
	$check	=	"a.finYear='$finYear'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"a.proId='$proId' and a.finYear='$finYear'";
}

$type = $_SESSION['travelType'];

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
/////**************************** ALL DELETE FUNCTIONS ****************///

// warning if the invoice is going to delete
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
// warning if the invoice purchased then deleting
function purchase_delete(k)
{
if(k) {
		var del=confirm("This invoice is Purchased. Do you want to delete ?");
		
	}
	
	return del;
}
//warning if the invoice's payment is over then going to delete
function payment_delete(l)
{
if(l) {
		var del=confirm("This invoice's Payment is over. Do you want to delete ?");
		
	}
	
	return del;
}
// warning if the invoice's receipt is over then going to delete
function receipt_delete(m)
{
if(m) {
		var del=confirm("This invoice's receipt is over. Do you want to delete ?");
		
	}
	
	return del;
}
/////**************************** ALL EDIT FUNCTIONS ****************///

//for warning if the invoice's payment is over while edit
function pay(x)
{
	if(x) {
		var edit=confirm("This invoice's Payment is over. Do you want to edit ?");		
	}
	return edit;	
}
//for warning edit if the invoice receipt is going to edit
function receipt(z)
{
	if(z) {
		var edit=confirm("This invoice's receipt is over. Do you want to edit ?");		
	}
	return edit;	
}
//for warning edit if the invoice is purchased.
function purchase(y)
{

	if(y) {
		var edit=confirm("This invoice is Purchased. Do you want to edit ?");		
	}	
	return edit;	
}


//for incurrect invoiceNo search
function count()
{
var invoiceNo = document.getElementById('invoiceNo').value;
//alert(invoiceNo);
if((invoiceNo.length < 3) && (invoiceNo!='') ){
	document.getElementById('check').innerHTML= "Enter Current Invoice No";
	return false;
		}
}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>OTHER PURCHASE</h3>
                </div>
                <div class="page_controls">
                    <div class="page_search">
                        <form method="post" onsubmit="return count()"><!-- count()-> for alert message -->
                        
                        	<input type="text" name="invoiceNo" id="invoiceNo" placeholder="Voucher No" value="<?php echo @$_REQUEST['invoiceNo']; ?>">
                            <input type="search" name="invoiceDate" placeholder="Voucher Date"  class="user_date" value="<?php echo @$_REQUEST['invoiceDate']; ?>">
                            <button type="submit" name="submit"><i class="ion ion-android-search"></i></button>
                            <div class="valid" id="check"></div><!-- id for message  -->
                        </form>
                    </div>
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="add.php">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    
<?php	
$cond="1";
if(@$_REQUEST['invoiceNo'])
{
	//deleting the first two character from a string
	$str = $_POST['invoiceNo'];
	$str2 = substr($str, 2);
	$cond=$cond." and a.invoiceNo like'".$str2."'";
}
if(@$_REQUEST['invoiceDate'])
{
$cond=$cond." and a.invoiceDate like'%".$App->dbFormat_date($_POST['invoiceDate'])."%'";
}

?>
    
    
    
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                         <tr>
                            <th>Sl No</th>
                            <th>Staff Name</th>
                            <th>Voucher No</th>
                            <th>Voucher Date</th>
                            <th>Supplier Name</th>
                            <th>Amount</th>                     
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                     $i=0;
                        $selectAll = "select a.ID,
                        					 a.invoiceNo,
                        					 a.invoiceDate,
                        					 a.loginId,
                        					 b.accountName,
                        					 a.netAmount,
                        					 d.staffName
			                          from ".TABLE_OTHER_PURCHASE." a,
			                          	   ".TABLE_ACCOUNTS." b,
			                          	   ".TABLE_LOGIN." c,
			                          	   ".TABLE_STAFF." d
			                          where a.supplierId=b.ID
			                          and a.loginId = c.ID
			                          AND c.userId  = d.ID
			                          and $cond
			                          and $check
			                          order by a.ID desc";
                        $result = $db->query($selectAll);
                        $selectCount = mysql_num_rows($result);
                        
                        if($selectCount==0)
						{
						?>
							 <tr>
								<td align="center" colspan="7">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
                        /*********************** for pagination ******************************/
						$rowsPerPage = ROWS_PER_PAGE;
						if(isset($_GET['page']))
						{
							$pageNum = $_GET['page'];
						}
						else
						{
							$pageNum =1;
						}
						$offset = ($pageNum - 1) * $rowsPerPage;
						$select1=$db->query($selectAll." limit $offset, $rowsPerPage");
						$i=$offset+1;
						//use '$select1' for fetching
						/*************************** for pagination **************************/
                        
                        
                        while ($row = mysql_fetch_array($select1)) {
                        	
                        	//for permission of edit and delete
                        	$loginIDs=$row['loginId'];
                        	
                        	$num=$num2=$num3=0;
                        	$tableId = $row['invoiceNo'];
                            ?>
                         <tr>                            
                            <td><?php echo $i++; ?></td>
                             <td><?php echo $row['staffName']; ?></td>
                            <td><?php echo "PO".$row['invoiceNo']; ?></td>
                            <td><?php echo $App->dbformat_date($row['invoiceDate']); ?></td>
                            <td><?php echo $row['accountName']; ?></td>
                            <td><?php echo $row['netAmount']; ?></td>
                            <td>
                <a class="show_table_lnk show_table_lnk_view" href="view.php?id=<?php echo $row['ID'];?>">View</a>
                <!--for warning if invoice in purchased-->
                    <?php  
                    $qry = "SELECT invoiceNo FROM ".TABLE_PURCHASE_MISC. " where ourInvoiceNo=$tableId and proId=$proId";
                    	$selectPurchase= $db->query($qry);
						$num		=	mysql_num_rows($selectPurchase);
						
						
						if($num!=0){
							$row1		=	mysql_fetch_array($selectPurchase);
							$invoiceNo = $row1['invoiceNo'];
							$qry2 = "SELECT ID FROM ".TABLE_CASH_PAYMENT." WHERE proId=$proId and voucherType='miscellaneous' and invoiceNo = $invoiceNo";//echo $qry2;
						$resultQry = $db->query($qry2);
						$num2	=	mysql_num_rows($resultQry);
						}
						
						$qry3 = "SELECT ID FROM ".TABLE_CASH_RECEIPT." where ".TABLE_CASH_RECEIPT.".invoiceNo = $tableId and proId= $proId and voucherType='miscellaneous'";
						$resultQry3 = $db->query($qry3);
						$num3  = mysql_num_rows($resultQry3);
						
					
                    ?>
                    
                
                
                <?php if($invoiceMiscEdit && $logID==$loginIDs){?>
                <a class="show_table_lnk show_table_lnk_edit" href="edit.php?id=<?php echo $row['ID'];?>" <?php if($num2!=0){?> onclick="return pay(<?php echo $num2; ?>)" <?php } if($num!=0) { ?> onclick="return purchase(<?php echo $num; ?>)" <?php } ?><?php if($num3!=0) { ?> onclick="return receipt(<?php echo $num3; ?>)" <?php } ?>>Edit</a>
                
                <a class="show_table_lnk show_table_lnk_del" <?php if($num2!=0){?> onclick="return payment_delete(<?php echo $num2; ?>);" <?php } ?><?php if($num3!=0){?> onclick="return receipt_delete(<?php echo $num3; ?>);" <?php } ?><?php if($num==0) { ?> onclick="return delete_type();" <?php } else {?> onclick="return purchase_delete(<?php echo $num; ?>);" <?php } ?> href="do.php?id=<?php echo $row['ID'];?>&op=delete">Delete</a>
                
                <?php } //end of if condition?>
                        
                            </td>
                        </tr>
		                    <?php
                   		 }
                   		 }
                   /*	}
                   	else{
						?>
						<tr>
							<td colspan="11" align="center">No data found</td>
						</tr>
						<?php
					}*/
                    ?>
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Popups -->
    <!--Popup for account View-->
    <div id="acs_view_pop" class="modal fade bd_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Miscellaneous Services</h4></div>
	                <div class="modal-body">
	                    <div class="acs_view_table_wrap table-responsive">
	                        <table class="table table-bordered table-striped acs_view_table">
	                            <tbody></tbody>
	                        </table>
	                    </div>
	                </div>
	                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');

require('../admin_footer2.php');
?>