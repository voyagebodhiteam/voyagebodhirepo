<?php
require('../admin_header.php');
if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>

<script>
function valid()
{
flag=false;
	proCharge=document.getElementById('proCharge').value;
	otherCharges= document.getElementById('otherCharges').value;
	tds =document.getElementById('tds').value;
	refundable=document.getElementById('refundable').value;
	taxInPer= document.getElementById('taxInPer').value;
	tac= document.getElementById('tac').value;
	roundAmount=document.getElementById('roundAmount').value;
	netAmount=document.getElementById('netAmount').value;
        inNet   = document.getElementById('inNet').value;
	
	if(isNaN(proCharge) || proCharge=="")
		{																			///for procharge
		document.getElementById('proChargeDiv').innerHTML="Enter number only.";
		flag=true;
		}
	if(isNaN(otherCharges) || otherCharges=="")
		{																			///for otherCharges
		document.getElementById('otherChargesDiv').innerHTML="Enter number only.";
		flag=true;
		}
	if(isNaN(tds) || tds=="")
		{																			///for tds
		document.getElementById('tdsDiv').innerHTML="Enter number only.";
		flag=true;
		}
	if(isNaN(refundable) || refundable=="")
		{																			///for refundable
		document.getElementById('refundableDiv').innerHTML="Enter number only.";
		flag=true;
		}	
	if(isNaN(taxInPer) || taxInPer=="")
		{																			///for taxInPer
		document.getElementById('taxInPerDiv').innerHTML="Enter number only.";
		flag=true;
		}
	if(isNaN(tac) || tac=="")
		{																			///for taxInPer
		document.getElementById('tacDiv').innerHTML="Enter number only.";
		flag=true;
		}
	if(isNaN(roundAmount) || roundAmount=="")
		{																			///for roundAmount
		document.getElementById('roundAmountDiv').innerHTML="Enter number only.";
		flag=true;
		}
	if(isNaN(netAmount) || netAmount=="")
		{																			///for netAmount
		document.getElementById('netAmountDiv').innerHTML="Enter number only.";
		flag=true;
		}
		if(netAmount>inNet)
		{
			document.getElementById('netAmountDiv').innerHTML="Net amount should less than invoice net amount.";
			flag=true;
		}
		if(flag==true)
		{
		return false;
		}
	
																				
}
//clear the validation msg

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" id="purchase_form" onsubmit="return valid()" >                 
                <div class="bd_panel_head">
                    <h3>OTHER PURCHASE</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                            <div class="form_block">
                                <label>Voucher No <span class="valid">*</span></label>
                                <?php
			 $newinvoiceId	=	$db->nextVoucher(TABLE_OTHER_PURCHASE,'invoiceNo',"proId='$proId'");								
            ?>
                               <input type="text" name="invoiceNo" value="<?php echo "PO".$newinvoiceId ?>" readonly="" required="">                                
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>Voucher Date <span class="valid">*</span></label>
                                <input type="text" name="invoiceDate" class="user_date" value="<?php echo date('d-m-Y') ?>" required="">
                            </div>
                        </div>                                                                    
                         <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">    
                            <div class="form_block">
                                <label>Basic Amount<span class="valid">*</span></label>
                                <input type="text" data-action="live_net" name="basicAmount" id="basicAmount" required="">
                            </div>
                         </div>                          
                          <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">   
                            <div class="form_block">
                                <label>Service Tax Reg No</label>
                                <input type="text" name="taxRegNo" id="taxRegNo" >
                            </div>
                          </div>
                          <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">                           
                            <div class="form_block">
                                <label>Supplier <span class="valid">*</span></label>
                                <select name="supplierId" required="">
	                                <option value="">Select</option>
	                                <?php
	                                $select 	= 	"select  ID,accountName from ". TABLE_ACCOUNTS." where accountGroupId='40' AND proId=$proId";
									$result 	= 	$db->query($select);							 	
									while($row=mysql_fetch_array($result))
									{
									?>
										<option value="<?php echo $row['ID'] ?>"><?php echo $row['accountName'] ?></option>
									<?php	
									}
									?>
                               </select>
                            </div>
                          </div>
                       </div>
                     <div class="row">                                                            
                         <div class="col-lg-2 col-md-2 col-sm-2 form_block_row"> 
                            <div class="form_block">
                                <label>Proc Charge</label>
                                <input type="text" data-action="live_net" name="proCharge" id="proCharge" value="0" onfocus="clearbox('proChargeDiv')">
		<div  id="proChargeDiv" class="valid"></div>
                            </div>
                        </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">                            
                            <div class="form_block">
                                <label>Other Charges</label>
                                <input type="text" name="otherCharges" data-action="live_net" id="otherCharges" value="0" onfocus="clearbox('otherChargesDiv')">
		<div  id="otherChargesDiv" class="valid"></div>                                
                            </div>
                        </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                            <div class="form_block">
                                <label>TDS</label>
                                <input type="text" name="tds" id="tds" data-action="live_net" value="0" onfocus="clearbox('tdsDiv')">
		<div  id="tdsDiv" class="valid"></div>                                
                            </div>                            
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                            <div class="form_block">
                                <label>Refundable</label>
                                <input type="text" name="refundable" id="refundable" data-action="live_net" value="0" onfocus="clearbox('refundableDiv')">
		<div  id="refundableDiv" class="valid"></div>                                
                            </div>
                        </div>
                        
                        <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                            <div class="form_block">
                                <label>Service Tax & Cess Amount</label>
                                <input type="text" name="tax" id="tax" data-action="live_net" value="0" onfocus="clearbox('taxInPerDiv')">
		<div  id="taxInPerDiv" class="valid"></div>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                         
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form_block">
                                <label>TAC</label>
                                <input type="text" name="tac" data-action="live_net"   id="tac" value="0" required="" onfocus="clearbox('tacDiv')">
								<div  id="tacDiv" class="valid"></div>  
                            </div>
                        </div>
                         <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                            <div class="form_block">
                                <label>Rounded off</label>
                                <input type="text" name="roundAmount" id="roundAmount" data-action="live_net" value="0" onfocus="clearbox('roundAmountDiv')">
		<div  id="roundAmountDiv" class="valid"></div>                               
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                            <div class="form_block">
                                <label>Net Amount <span class="valid">*</span></label>
                                <input type="text" name="netAmount" id="netAmount" required="" onfocus="clearbox('netAmountDiv')"  value="" readonly="">
		<div  id="netAmountDiv" class="valid"></div>                               
                            </div>
                        </div>
                                 	
                    	
                         <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">
                            <div class="form_block">
                                <label>Remark</label>
                                <textarea name="remark" id="remark"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 form_block_row">   
                            <div class="form_block">
                                <label>Description</label>
                                <textarea name="description" id="description"></textarea>
                            </div>
                          </div> 
                         
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="OK">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
