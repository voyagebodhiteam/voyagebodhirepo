<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

$proId=$_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear=$_SESSION['finYear'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
            	
         	$newinvoiceId	=	$db->nextVoucher(TABLE_OTHER_PURCHASE,'invoiceNo',"proId='$proId'");            	
            $accId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Other Purchase'";             		 				
				$acc_record 	= 	$db->query_first($accId);
			 	$accountId		=	$acc_record['ID'];
			 	$supplierId			=	$App->convert($_REQUEST['supplierId']);
				
                $data['invoiceNo']			=	$newinvoiceId;    
                $data['invoiceDate']		=	$App->dbformat_date($_REQUEST['invoiceDate']);
                $data['supplierId']			=	$App->convert($_REQUEST['supplierId']); 
                $data['taxRegNo']			=	$App->convert($_REQUEST['taxRegNo']);
                
                $data['basicAmount']		=	$basicAmount2	=	$App->convert($_REQUEST['basicAmount']);
                $data['proCharge']			=	$procCharge2	=	$App->convert($_REQUEST['proCharge']);
                $data['otherCharges']		=	$otherCharge2	=  $App->convert($_REQUEST['otherCharges']);
                $data['tds']				=	$tds2			=	$App->convert($_REQUEST['tds']); 
                $data['refundable']			=	$refundable2	=	$App->convert($_REQUEST['refundable']); 
              
                $totAmt					=	$basicAmount2+$tds2+$procCharge2+$otherCharge2+$refundable2;
                $mainTax				=	$App->convert($_REQUEST['tax']);
				if(strpos ($mainTax,'%'))
				{
					$taxTrim	=	rtrim($mainTax, "%");							
					$mainTax	=	$totAmt*($taxTrim/100);
					$data['taxInPer']		=	$mainTax; 
				}
				else{
					$data['taxInPer']		=	$mainTax;
				}           
                $data['roundAmount']		=	$App->convert($_REQUEST['roundAmount']); 
                $data['netAmount']			=	$App->convert($_REQUEST['netAmount']);
                $data['remark']				=	$App->convert($_REQUEST['remark']); 
                $data['description']		=	$App->convert($_REQUEST['description']);     
                $data['tac']				=	$App->convert($_REQUEST['tac']);          
                $data['proId']				=	$proId;
                $data['loginId']			=	$loginId;
                $data['finYear']			=	$finYear;
                
 				$success = $db->query_insert(TABLE_OTHER_PURCHASE, $data);
 
		         /*-------------------------------------------------------------------------*/
		           //To transaction table		           
					$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$accountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase'; 
					$tsdata['credit']			= 	$App->convert($_POST['netAmount'])-$App->convert($_REQUEST['roundAmount'])-$App->convert($_REQUEST['tds'])-$mainTax+$App->convert($_REQUEST['tac']);
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	$App->convert($_REQUEST['remark']); 
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$accountId;
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	$App->convert($_POST['netAmount'])-$App->convert($_REQUEST['roundAmount'])-$App->convert($_REQUEST['tds'])-$mainTax+$App->convert($_REQUEST['tac']);
					
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	$App->convert($_REQUEST['remark']);
					$tfdata['details']			=	$App->convert($_REQUEST['remark']); 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  /*----------------------------------------------------------------------------*/
		             /*----------------------------------------------------------------------------*/
		          //discount
		          	
		          	$discount		=	$App->convert($_REQUEST['roundAmount']);
		          	$tds			=	$App->convert($_REQUEST['tds']);
		          	$tac			=	$App->convert($_REQUEST['tac']);
		           	         
		          if($discount>0)
		          {
		          	
		          	 $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$discountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase';
					$tsdata['credit']			= 	abs($App->convert($_REQUEST['roundAmount']));
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Discount Given:inv#".$newinvoiceId;
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$App->convert($_REQUEST['remark']);
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $discountId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($App->convert($_REQUEST['roundAmount']));
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Discount Given:inv#".$newinvoiceId; 
					$tfdata['details']			=	$App->convert($_REQUEST['remark']);
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				   
					 	         
		          if($discount<0)
		          {
		          	
		          	 $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Received' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$discountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['roundAmount']));
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Discount Rcvd:inv#".$newinvoiceId; 
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $discountId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   abs($App->convert($_REQUEST['roundAmount']));
					$tfdata['debit']			= 	0;
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Discount Rcvd:inv#".$newinvoiceId;
					$tfdata['details']			=	$App->convert($_REQUEST['remark']); 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tds
				  if($tds)
		          {
		          	
		          	 $tdsQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tds' ";
					 $tds_record 		= 	$db->query_first($tdsQuery);
					 $tdsId		=	$tds_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$tdsId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase';
					$tsdata['credit']			= 	abs($App->convert($_REQUEST['tds']));
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tds:inv#".$newinvoiceId;
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $tdsId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($App->convert($_REQUEST['tds']));
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tds:inv#".$newinvoiceId; 
					$tfdata['details']			=	$App->convert($_REQUEST['remark']);
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tax
				  if($mainTax)
		          {
		          	
		          	 $taxQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tax' ";
					 $tax_record 		= 	$db->query_first($taxQuery);
					 $taxId				=	$tax_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$taxId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase';
					$tsdata['credit']			= 	abs($mainTax);
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tax:inv#".$newinvoiceId;
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $taxId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($mainTax);
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tax:inv#".$newinvoiceId; 
					$tfdata['details']			=	$App->convert($_REQUEST['remark']);
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tac
				  if($tac)
		          {
		          	
		          	 $tacQuery		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tac' ";
					 $tac_record 	= 	$db->query_first($tacQuery);
					 $tacId			=	$tac_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$tacId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['tac']));
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tac:inv#".$newinvoiceId; 
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $tacId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   abs($App->convert($_REQUEST['tac']));
					$tfdata['debit']			= 	0;
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tac:inv#".$newinvoiceId;
					$tfdata['details']			=	$App->convert($_REQUEST['remark']); 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
		          
		          /*--------------------------------------------------------------------------*/ 
		             $db->close();

		                if ($success) 
		                {		                  
		                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
		                } 
		                else 
		                {
		                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
		                }		               
	                  header("location:index.php");					 
        break;
    // EDIT SECTION
    case 'edit':
    		$editId = $_REQUEST['editId'];	
    		$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = $totAmt=0;           
			$existInvId	=	$db->existVoucherNew(TABLE_OTHER_PURCHASE,'invoiceNo'," ID='$editId' and proId=$proId");
          
            $supplierId		=	$App->convert($_REQUEST['supplierId']);
            @mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$existInvId}' and voucherType='Other Purchase' and proId=$proId and finYear=$finYear");	
            $accId 			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Other Purchase'";             		 				
				$acc_record 	= 	$db->query_first($accId);
			 	$accountId		=	$acc_record['ID'];
			 	$supplierId		=	$App->convert($_REQUEST['supplierId']);
			 	$newinvoiceId	=	$existInvId;
				
                $data['invoiceNo']			=	$newinvoiceId;    
                $data['invoiceDate']		=	$App->dbformat_date($_REQUEST['invoiceDate']);
                $data['supplierId']			=	$App->convert($_REQUEST['supplierId']); 
                $data['taxRegNo']			=	$App->convert($_REQUEST['taxRegNo']);
                
                $data['basicAmount']		=	$basicAmount2	=	$App->convert($_REQUEST['basicAmount']);
                $data['proCharge']			=	$procCharge2	=	$App->convert($_REQUEST['proCharge']);
                $data['otherCharges']		=	$otherCharge2	=  $App->convert($_REQUEST['otherCharges']);
                $data['tds']				=	$tds2			=	$App->convert($_REQUEST['tds']); 
                $data['refundable']			=	$refundable2	=	$App->convert($_REQUEST['refundable']); 
              
                $totAmt					=	$basicAmount2+$tds2+$procCharge2+$otherCharge2+$refundable2;
                $mainTax				=	$App->convert($_REQUEST['tax']);
				if(strpos ($mainTax,'%'))
				{
					$taxTrim	=	rtrim($mainTax, "%");							
					$mainTax	=	$totAmt*($taxTrim/100);
					$data['taxInPer']		=	$mainTax; 
				}
				else{
					$data['taxInPer']		=	$mainTax;
				}           
                $data['roundAmount']		=	$App->convert($_REQUEST['roundAmount']); 
                $data['netAmount']			=	$App->convert($_REQUEST['netAmount']);
                $data['remark']				=	$App->convert($_REQUEST['remark']); 
                $data['description']		=	$App->convert($_REQUEST['description']);     
                $data['tac']				=	$App->convert($_REQUEST['tac']);          
                $data['proId']				=	$proId;
                $data['loginId']			=	$loginId;
                $data['finYear']			=	$finYear;
                
 				$success = $db->query_update(TABLE_OTHER_PURCHASE, $data,"ID=$editId");
		         /*-------------------------------------------------------------------------*/
		           //To transaction table		           
					$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$accountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase'; 
					$tsdata['credit']			= 	$App->convert($_POST['netAmount'])-$App->convert($_REQUEST['roundAmount'])-$App->convert($_REQUEST['tds'])-$mainTax+$App->convert($_REQUEST['tac']);
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	$App->convert($_REQUEST['remark']); 
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=  	$accountId;
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	$App->convert($_POST['netAmount'])-$App->convert($_REQUEST['roundAmount'])-$App->convert($_REQUEST['tds'])-$mainTax+$App->convert($_REQUEST['tac']);
					
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	$App->convert($_REQUEST['remark']);
					$tfdata['details']			=	$App->convert($_REQUEST['remark']); 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  /*----------------------------------------------------------------------------*/
		             /*----------------------------------------------------------------------------*/
		          //discount
		          	
		          	$discount		=	$App->convert($_REQUEST['roundAmount']);
		          	$tds			=	$App->convert($_REQUEST['tds']);
		          	$tac			=	$App->convert($_REQUEST['tac']);
		           	         
		          if($discount>0)
		          {
		          	
		          	 $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Given' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$discountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase';
					$tsdata['credit']			= 	abs($App->convert($_REQUEST['roundAmount']));
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Discount Given:inv#".$newinvoiceId;
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$App->convert($_REQUEST['remark']);
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $discountId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($App->convert($_REQUEST['roundAmount']));
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Discount Given:inv#".$newinvoiceId; 
					$tfdata['details']			=	$App->convert($_REQUEST['remark']);
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				   
					 	         
		          if($discount<0)
		          {
		          	
		          	 $disId 		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Discount Received' ";
					 $dis_record 	= 	$db->query_first($disId);
					 $discountId		=	$dis_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$discountId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['roundAmount']));
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Discount Rcvd:inv#".$newinvoiceId; 
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $discountId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   abs($App->convert($_REQUEST['roundAmount']));
					$tfdata['debit']			= 	0;
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Discount Rcvd:inv#".$newinvoiceId;
					$tfdata['details']			=	$App->convert($_REQUEST['remark']); 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tds
				  if($tds)
		          {
		          	
		          	 $tdsQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tds' ";
					 $tds_record 		= 	$db->query_first($tdsQuery);
					 $tdsId		=	$tds_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$tdsId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase';
					$tsdata['credit']			= 	abs($App->convert($_REQUEST['tds']));
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tds:inv#".$newinvoiceId;
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $tdsId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($App->convert($_REQUEST['tds']));
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tds:inv#".$newinvoiceId; 
					$tfdata['details']			=	$App->convert($_REQUEST['remark']);
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tax
				  if($mainTax)
		          {
		          	
		          	 $taxQuery			=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tax' ";
					 $tax_record 		= 	$db->query_first($taxQuery);
					 $taxId				=	$tax_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$taxId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase';
					$tsdata['credit']			= 	abs($mainTax);
					$tsdata['debit']			=   0;
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tax:inv#".$newinvoiceId;
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $taxId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	abs($mainTax);
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tax:inv#".$newinvoiceId; 
					$tfdata['details']			=	$App->convert($_REQUEST['remark']);
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
				  //tac
				  if($tac)
		          {
		          	
		          	 $tacQuery		=	"SELECT ID FROM `".TABLE_ACCOUNTS."` WHERE `accountName`='Tac' ";
					 $tac_record 	= 	$db->query_first($tacQuery);
					 $tacId			=	$tac_record['ID'];
		          
		          	
				  	$tsdata['fromLedger'] 		=  	$supplierId;
					$tsdata['toLedger'] 		=  	$tacId;
					$tsdata['voucherNo']		= 	$newinvoiceId;
					$tsdata['voucherType']		=  	'Other Purchase';
					$tsdata['credit']			= 	0;
					$tsdata['debit']			=   abs($App->convert($_REQUEST['tac']));
					//$tsdata['referenceNo']		=   $newinvoiceId;
					$tsdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tsdata['remark']			=  	"Tac:inv#".$newinvoiceId; 
					$tsdata['details']			=	$App->convert($_REQUEST['remark']);
					$tsdata['loginId']			=	$loginId;
					$tsdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
					
					$db->query_insert(TABLE_TRANSACTION, $tsdata);						
									
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['fromLedger'] 		=   $tacId;	
					$tfdata['toLedger'] 		=  	$supplierId;
					$tfdata['voucherNo']		= 	$newinvoiceId;
					$tfdata['voucherType']		=  	'Other Purchase';
					$tfdata['credit']			=   abs($App->convert($_REQUEST['tac']));
					$tfdata['debit']			= 	0;
					//$tfdata['referenceNo']		=   $newinvoiceId;
					$tfdata['addedDate']		=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['transactionDate']	=	$App->dbFormat_date($_POST['invoiceDate']);
					$tfdata['remark']			=  	"Tac:inv#".$newinvoiceId;
					$tfdata['details']			=	$App->convert($_REQUEST['remark']); 
					$tfdata['loginId']			=	$loginId;
					$tfdata['proId']			=	$proId;
					$data['finYear']			=	$finYear;
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);	
					
				  }
		          
		          /*--------------------------------------------------------------------------*/ 
		             $db->close();

		                if ($success) 
		                {		                  
		                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details updated successfully");                
		                } 
		                else 
		                {
		                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to update details. Please try again.");                   
		                }		               
	                  header("location:index.php");					 
        break;
   // DELETE SECTION
	//-
	 case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success1 = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
		   $existInvId	=	$db->existVoucherNew(TABLE_OTHER_PURCHASE,'invoiceNo'," ID='$deleteId' and proId=$proId");
           $success1=mysql_query("DELETE FROM `" . TABLE_OTHER_PURCHASE . "` WHERE ID='{$deleteId}' and proId=$proId and finYear=$finYear");
			 
			 @mysql_query("DELETE FROM `" . TABLE_TRANSACTION . "` WHERE voucherNo='{$existInvId}' and voucherType='Other Purchase' and proId=$proId and finYear=$finYear");	
						
        $db->close();
        if ($success1) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
      }