<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
    header("location:../../logout.php");
}


if($_SESSION['travelType']=='Admin')
{
    $check	=	1;
}
else
{
    $check	=	'';
    $logId	=	$_SESSION['travelId'];
    $proId	=	$_SESSION['proId'];
    $check 	= 	"(".TABLE_ACCOUNTS.".proId='$proId' or " . TABLE_ACCOUNTS .".masterLedger='1')";
}


if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

?>
    <style>
        body{
            background: #efefef;
        }
    </style>
    <div class="row">
    <div class="col-lg-12">
        <h3 class="dash_section_heads">Account Report</h3>
        <div class="clearfix dash_row">
            <div class="dash_box">
                <a class="info-box clearfix" href="../account_reports/profit_report.php" style="margin-left:0;">
                    <span class="info-box-icon aqua"><i class="ion ion-android-checkmark-circle"></i></span>

                    <div class="info-box-content">
                        <!--<span class="info-box-text">Profit</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-aqua">Profit</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../account_reports/payment_report.php">
                    <span class="info-box-icon red"><i class="ion ion-log-out"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">Payment</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-red">Payment</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../account_reports/receipt_report.php">
                    <span class="info-box-icon green"><i class="ion ion-log-in"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">Receipt</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-green">Receipt</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../account_reports/payable_due.php">
                    <span class="info-box-icon yellow"><i class="ion ion-arrow-return-right"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">Payment Due</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-yellow">Payment Due</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../account_reports/receivable_due.php">
                    <span class="info-box-icon purple"><i class="ion ion-arrow-return-left"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">Receipt Due</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-purple">Receipt Due</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <h3 class="dash_section_heads">Sale</h3>
        <div class="clearfix dash_row">
            <div class="dash_box">
                <a class="info-box clearfix" href="../invoice_report/airline.php">
                    <span class="info-box-icon aqua"><i class="ion ion-android-plane"></i></span>

                    <div class="info-box-content">
                        <!--<span class="info-box-text">Airline</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-aqua">Airline</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../invoice_report/visa.php">
                    <span class="info-box-icon red"><i class="ion ion-document-text"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">Visa</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-red">Visa</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../invoice_report/hotel.php">
                    <span class="info-box-icon green info-box-icon-fa"><i class="fa fa-building"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">Hotel</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-green">Hotel</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../invoice_report/transport.php">
                    <span class="info-box-icon yellow"><i class="ion ion-android-car"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">Transport</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 10071204</span>-->
                        <span class="info-box-number bg-yellow">Transport</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../invoice_report/misc.php">
                    <span class="info-box-icon purple"><i class="ion ion-grid"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">miscellaneous</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 10071204</span>-->
                        <span class="info-box-number bg-purple">Miscellaneous</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <h3 class="dash_section_heads">Purchase</h3>
        <div class="clearfix dash_row">
            <div class="dash_box">
                <a class="info-box clearfix" href="../purchase/airline.php" style="margin-left:0;">
                    <span class="info-box-icon aqua"><i class="ion ion-android-plane"></i></span>

                    <div class="info-box-content">
                        <!--<span class="info-box-text">Airline</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-aqua">Airline</span>
                    </div>
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../purchase/visa.php">
                    <span class="info-box-icon red"><i class="ion ion-document-text"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">Visa</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-red">Visa</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../purchase/hotel.php">
                    <span class="info-box-icon green info-box-icon-fa"><i class="fa fa-building"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">Hotel</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 9871204</span>-->
                        <span class="info-box-number bg-green">Hotel</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../purchase/transport.php">
                    <span class="info-box-icon yellow"><i class="ion ion-android-car"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">Transport</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 10071204</span>-->
                        <span class="info-box-number bg-yellow">Transport</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
            <div class="dash_box">
                <a class="info-box clearfix" href="../purchase/misc.php">
                    <span class="info-box-icon purple"><i class="ion ion-grid"></i></span>
                    <div class="info-box-content">
                        <!--<span class="info-box-text">miscellaneous</span>
                        <span class="info-box-number"><small><i class="fa fa-rupee"></i></small> 10071204</span>-->
                        <span class="info-box-number bg-purple">Miscellaneous</span>
                    </div>
                    <!-- /.info-box-content -->
                </a>
            </div>
        </div>
    </div>
    </div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>