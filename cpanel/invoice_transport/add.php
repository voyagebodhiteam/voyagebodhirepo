<?php
require("../admin_header.php");
if (isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
$_SESSION['msg'] = '';
$i = 0;
?>
<script>
function valid()
{	
	flag=false;
	
	adults=document.getElementById('adults').value;
	childrens=document.getElementById('childrens').value;
	totalPersons=document.getElementById('totalPersons').value;
	totalDays=document.getElementById('totalDays').value;
	kmAllowed=document.getElementById('kmAllowed').value;	
	netAmount=document.getElementById('netAmount').value;
	proCharge=document.getElementById('proCharge').value;
	otherCharges=document.getElementById('otherCharges').value;
	fare=document.getElementById('fare').value;
	
	fromDate=document.getElementById('fromDate').value;
	toDate=document.getElementById('toDate').value;
	
	if(isNaN(adults))
	{																			
	document.getElementById('adultsDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(childrens))
	{																			
	document.getElementById('childrensDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(totalPersons) || totalPersons=="" || totalPersons==0)
	{																			
	document.getElementById('totalPersonsDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(totalDays) || totalDays=="")
	{																			
	document.getElementById('totalDaysDiv').innerHTML="Enter number only.";
	flag=true;
	}	
	if(isNaN(kmAllowed))
	{																			
	document.getElementById('kmAllowedDiv').innerHTML="Enter number only.";
	flag=true;
	}
	if(isNaN(fare))
	{																			
	document.getElementById('farediv').innerHTML="Enter valid amount";
	flag=true;
	}
	if(isNaN(proCharge))
	{																			
	document.getElementById('proChargediv').innerHTML="Enter valid amount";
	flag=true;
	}
	if(isNaN(otherCharges))
	{																			
	document.getElementById('otherChargesdiv').innerHTML="Enter valid amount";
	flag=true;
	}
	if(isNaN(netAmount) || netAmount=="" || netAmount==0 || netAmount <0)
	{																			
	document.getElementById('netAmountDiv').innerHTML="Enter valid amount";
	flag=true;
	}
	if(fromDate=="")
	{																			
	document.getElementById('fromDateDiv').innerHTML="Can't leave this empty";
	flag=true;
	}
	if(toDate=="")
	{																			
	document.getElementById('toDateDiv').innerHTML="Can't leave this empty";
	flag=true;
	}
	//for getting alert if invalid 'todate' added.
	
	var todate	=	document.getElementById('toDateVal').value;
	//alert(exdate);
			if(todate == 1)
			{
				document.getElementById('toDateDiv').innerHTML = "Invalid To Date";
				flag	=	true;
			}
	
	
	//for getting alert if invalid 'fromdate' added.
	var fromdate	=	document.getElementById('fromDateVal').value;
	//alert(exdate);
			if(fromdate == 1)
			{
				document.getElementById('fromDateDiv').innerHTML = "Invalid From Date";
				flag	=	true;
			}
	//for customer name and customer id validation	
		var customerName	=	document.getElementById('customerName').value;
		var customerId		=	document.getElementById('customerId').value;
		var getResponse=0;
		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
           // document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
          	getResponse= xmlhttp.responseText;//alert(getResponse);
          	
          	 
          	
          }	
          
	    };
	   
	   	xmlhttp.open("GET", "../../services/customer_check_ajax.php?name="+customerName+"&id="+customerId, false);
	    xmlhttp.send();
	  
	
		 		if(getResponse==0)
	          	{
	          		flag=true;
                                alert("Invalid Customer Name and ID.!");
				}
	
	
	
	if(flag==true)
	{
	return false;
	}
	
}

//clear the validation msg

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

function calculation()
{	
	proCharge	=	document.getElementById('proCharge').value;
	otherCharges=	document.getElementById('otherCharges').value;
	fare1		=	document.getElementById('fare').value;
	
	var pCharge		=	0;
	var oCharges	=	0;
	var fare		=	0;
	if(proCharge != "" && !isNaN(proCharge))
	{
		pCharge		=	parseFloat(proCharge);
	}
	if(otherCharges != "" && !isNaN(otherCharges))
	{
		oCharges	=	parseFloat(otherCharges);
	}
	if(fare1 != "" && !isNaN(fare1))
	{
		fare	=	parseFloat(fare1);
	}
	
	total	=	(pCharge+oCharges+fare).toFixed(2);
	document.getElementById('netAmount').value	=	total;
}
function countPerson()
{
	adult	= 0;
	child	= 0;
	
	adultCount	=	document.getElementById('adults').value;
	childCount	=	document.getElementById('childrens').value;
	
	if(adultCount!='' && !isNaN(adultCount))
	{
		adult	=	Number(adultCount);
	}
	if(childCount!='' && !isNaN(childCount))
	{
		child	=	Number(childCount);
	}
	total = adult+child;
	document.getElementById('totalPersons').value	=	total;
}
function countDays()
{	
	flag = false;
	var fromDate = document.getElementById('fromDate').value;	
	var toDate   = document.getElementById('toDate').value;
	
		var today = new Date();
        var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 

		today = dd+'-'+mm+'-'+yyyy;
		
		//from date validation
		
		if(fromDate != '')
		{
			
			var fromSplit= fromDate.split('-');
			var todaySplit= today.split('-');
			
			var fromLast 	= 	new Date(fromSplit[2], +fromSplit[1]-1, fromSplit[0]);
			var todayLast 		= 	new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
			var dateDiff		=	(todayLast.getTime() - fromLast.getTime()) / (1000*60*60*24);
			if(dateDiff>0)
			{
				document.getElementById('fromDateVal').value	=1;
				alert("Invalid From Date");
				flag=true;
			}
			else {
			
			document.getElementById('fromDateVal').value	='';
		}
		} // end of from validation
		
		// to date validation
		if(toDate!= '')
		{
			var toSplit= toDate.split('-');
			var todaySplit= today.split('-');
			
			var toLast 	= 	new Date(toSplit[2], +toSplit[1]-1, toSplit[0]);
			var todayLast 		= 	new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
			var dateDiff		=	(todayLast.getTime() - toLast.getTime()) / (1000*60*60*24);
			
			if(dateDiff>0 && dateDiff!=1)
			{
				document.getElementById('toDateVal').value	=1;
				alert("Invalid To Date");
				flag=true;
			}
			else {
			
			document.getElementById('toDateVal').value	='';
		}
			
		}//end of to date validation
		
	
	if (fromDate != "" && toDate != "")
        {
	
			var checkInDateSplit= fromDate.split('-');
			var checkOutDateSplit= toDate.split('-');

			var checkInLast = new Date(checkInDateSplit[2], +checkInDateSplit[1]-1, checkInDateSplit[0]);
			var checkOutLast = new Date(checkOutDateSplit[2], +checkOutDateSplit[1]-1, checkOutDateSplit[0]);
			var dateDiff=(checkOutLast.getTime() - checkInLast.getTime()) / (1000*60*60*24);
			//var age=Math.round(dateDiff/365);
			if(dateDiff==0)
			{
				dateDiff = 1;
			}
			if(dateDiff<0)
					{
						alert("Invalid Dates");
						dateDiff="";
					}
					
			document.getElementById('totalDays').value	=	dateDiff;
			
		} //end of if condition fromDate != "" && toDate != ""
		
		if(flag==true)
		{
		return false;
		}
		
}//end of function countDays()
</script>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
    <div class="row">
        <div class="col-lg-12">
            <div class="bd_panel bd_panel_default bd_panel_shadow">
                <form class="default_form" id="invoice_hotel" method="post" action="do.php?op=index" onsubmit="return valid()">
                    <div class="bd_panel_head">
                        <h3>Transportation - Invoice</h3>
                    </div>
                    <div class="bd_panel_body">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Invoice No <span class="valid">*</span></label>                                    
                                    <input type="text" name="invoiceNo" value="<?php echo "TI" . $db->nextVoucher(TABLE_INVOICE_TRANSPORT,'invoiceNo',"proId='$proId'"); ?>"  required="" readonly="">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Date</label>
                                    <input class="user_date" type="text" name="invoiceDate" value="<?php echo date('d-m-Y') ?>" required="">
                                </div>
                            </div>

                        <div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer Name <span class="valid">*</span></label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerName" autocomplete="off" required="" id="customerName">								
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>					
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Customer ID</label> <!--select from account where accGpId=37-->
								<div class="aj_rel_box aj_customer">
									<input type="text" name="customerId" autocomplete="off" required="" id="customerId">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
						 <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Service Provider</label>
                                    <select name="serviceProviderId" class="pass_to_table">
                                        <option value="">Select</option>
                                        <?php
                                        $select = "select * from " . TABLE_TRANSPORT_PROVIDER." where (proId='$proId' and finYear='$finYear') || (proId='0')";
                                        $res = $db->query($select);
                                        while ($row = mysql_fetch_array($res)) {
                                            ?>
                                            <option value="<?php echo $row['ID']; ?>"><?php echo $row['providerName']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form_section_head"><h3>No of persons <span class="valid">*</span></h3></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Adults</label>
                                     <input type="text" name="adults"  id="adults" onfocus="clearbox('adultsDiv')" onkeyup="countPerson()" required="">
									 <div class="valid" id="adultsDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Childrens</label>
                                     <input type="text" name="childrens"  id="childrens" onfocus="clearbox('childrensDiv')" onkeyup="countPerson()" value="0" >
									<div class="valid" id="childrensDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Total persons</label>
                                     <input type="text" name="totalPersons"  id="totalPersons" onfocus="clearbox('totalPersonsDiv')" readonly="" >
								<div class="valid" id="totalPersonsDiv"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form_section_head"><h3>No of days <span class="valid">*</span></h3></div>
                            </div>
                        </div>
                       <div class="row">
                       
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>From</label>
                                    <input type="text" name="fromDate" id="fromDate" required="" class="user_date" readonly="" onchange="countDays()" onfocus="clearbox('fromDateDiv')">
                                    <input type="hidden" name="fromDateVal" id="fromDateVal" value=""/>
                                    <div class="valid" id="fromDateDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>To</label>
                                    <input type="text" name="toDate" id="toDate" required="" readonly=""  class="user_date" onchange="countDays()" onfocus="clearbox('toDateDiv')">
                                    <input type="hidden" id="toDateVal" name="toDateVal" value=""/>
                                    <div class="valid" id="toDateDiv"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Total Days</label>
                                    <input type="text" name="totalDays" id="totalDays" onfocus="clearbox('totalDaysDiv')" readonly="">
									<div class="valid" id="totalDaysDiv"></div>
                                </div>
                            </div>
                           <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Hire vehicle <span class="valid">*</span></label>
                                    <select name="vehicleType" required="">
                                    	 <option value="">Select</option>
                                        <?php
                                        $select2 = "select * from " . TABLE_VEHICLE_TYPES ." where (proId='$proId' && finYear='$finYear') OR proId='0'";
                                        $res2 = $db->query($select2);
                                        while ($row2 = mysql_fetch_array($res2)) {
                                            ?>
                                        <option value="<?php echo $row2['ID']; ?>"><?php echo $row2['vehicleType']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2" style="display: none">
                                <div class="form_block">
                                    <label>KM Allowed</label>
                                   <input type="text" name="kmAllowed" id="kmAllowed" onfocus="clearbox('kmAllowedDiv')">
								<div class="valid" id="kmAllowedDiv"></div>
                                </div>
                            </div>
                        	<div class="col-lg-2 col-md-2 col-sm-2" style="display: none">
                                <div class="form_block">
                                    <label>Pick Up from</label>
                                    <input type="text" name="pickUpFrom" >
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2" style="display: none">
                                <div class="form_block">
                                    <label>Pick Up Time</label>
                                    <input type="text" name="pickUpTime" >
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2" style="display: none">
                                <div class="form_block">
                                    <label>Drop at</label>
                                    <input type="text" name="dropAt" >
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2" style="display: none">
                                <div class="form_block">
                                    <label>Drop Time</label>
                                    <input type="text" name="dropTime" >
                                </div>
                            </div>
                            </div>
                            <div class="row">
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Fare</label>
                                    <input type="text" name="fare" id="fare" onfocus="clearbox('farediv')" onkeyup="calculation()" >
                                    <div id="farediv" class="valid" style="color:#FF6600;"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Processing charges</label>
                                    <input type="text" name="proCharge" id="proCharge" onfocus="clearbox('proChargediv')" onkeyup="calculation()" >
                                    <div id="proChargediv" class="valid" style="color:#FF6600;"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Other Charges</label>
                                    <input type="text" name="otherCharges" id="otherCharges" onfocus="clearbox('otherChargesdiv')" onkeyup="calculation()">
                                    <div id="otherChargesdiv" class="valid" style="color:#FF6600;"></div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Net Amount <span class="valid">*</span></label>
                                    <input type="text" name="netAmount" id="netAmount" onfocus="clearbox('netAmountDiv')" readonly="" required="">
								<div class="valid" id="netAmountDiv"></div>
                                </div>
                            </div>                           
                            
                            
                        </div>
                        <div class="row">
                        	<div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Arrival Details</label>
                                    <textarea name="arrivalDetails" ></textarea>
                                </div>
                            </div>
                           <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Departure Details</label>
                                    <textarea name="DepartureDetails" ></textarea>
                                </div>
                            </div>
                           <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Route</label>
                                    <textarea name="route" ></textarea>
                                </div>
                            </div>
                             <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form_block">
                                    <label>Inclusive</label>
                                    <textarea name="inclusive"></textarea>
                                </div>
                            </div>
                         	
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                 <div class="form_block">
                                    <label>Remark</label>
                                    <textarea name="remark" ></textarea>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="airline_submit">
                                <input type="submit" name="savePurchase" value="SAVE & PURCHASE" onclick="return check()">
								<!--<input type="submit" name="save" value="SAVE">-->
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
require('../admin_footer1.php');
?>
    <script>
        $(function () {
            $('.user_date').each(function () {
                var curDatePicker = $(this);
                //alterTarget = curDatePicker.nextAll('.date_hidden');
                curDatePicker.datepicker({
                    dateFormat: "dd-mm-yy"
                    /*altFormat: "yy-mm-dd",
                     altField: alterTarget*/
                });
            });
        });
    </script>
<?php
require('../admin_footer2.php');
?>