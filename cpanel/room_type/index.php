<?php
require('../admin_header.php');

if($_SESSION['travelType']=='Admin')
{
	$check	=	"".TABLE_ROOM_TYPE.".finYear='$finYear' or ".TABLE_ROOM_TYPE.".proId='0'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"(".TABLE_ROOM_TYPE.".proId='$proId' and ".TABLE_ROOM_TYPE.".finYear=$finYear) or  (".TABLE_ROOM_TYPE.".proId='0')";
	
}

$type = $_SESSION['travelType'];

$proId	=	$_SESSION['proId'];

//for edit and delete permission
$logID = $_SESSION['travelId'];

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
   
//delete row in index page

function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>

    <div class="row">
        <div class="col-lg-12">
            <div class="page_header">
                <div class="page_heading">
                    <h3>Room Type Registration</h3>
                </div>
                <div class="page_controls">
                    
                    <div class="page_nav_menu">
                        <ul class="page_nav">
                            <li>
                                <a href="add.php">
                                    <span class="page_nav_ico"><i class="ion ion-plus"></i></span>
                                    Add New
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="bd_clear"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive show_table_wrap">
                <table class="table table-bordered table-striped show_table">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>User Name</th>
                             <th>Room type</th>                                               
                             <?php if($basicSettingEdit || $_SESSION['travelType']=="Admin" || $_SESSION['travelType']=="Branch"){?>
                            <th>Actions</th>
                            <?php } //end of if condition?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    $selectAll = "select  * from ".TABLE_ROOM_TYPE." where $check order by ID desc";
                    /*$selectAll = "SELECT ".TABLE_ROOM_TYPE.".ID,
                    					 ".TABLE_ROOM_TYPE.".roomType,
                    					 ".TABLE_ROOM_TYPE.".loginId,
                    					 ".TABLE_ROOM_TYPE.".proId,
                    					 ".TABLE_STAFF.".staffName
                    					 FROM ".TABLE_ROOM_TYPE."
                    					 LEFT JOIN ".TABLE_LOGIN." ON ".TABLE_ROOM_TYPE.".loginId=".TABLE_LOGIN.".ID
                    					 LEFT JOIN ".TABLE_STAFF." ON ".TABLE_LOGIN.".userId=".TABLE_STAFF.".ID
            					       WHERE $check GROUP BY ".TABLE_ROOM_TYPE.".ID
            					       order by ".TABLE_ROOM_TYPE.".ID desc";*/
                    $result = $db->query($selectAll);
                    if(mysql_num_rows($result)==0)
                    {
					?>
						<tr>
							<td colspan="4" align="center"> There is no data in list. </td>
						</tr>
					<?php
					}
					else
					{
	                    while ($row = mysql_fetch_array($result)) 
	                    {
	                    	$loginIDs=$row['loginId'];
	                    	$hproId = $row['proId'];
	                    	
	                    	//for getting username in index
	                    	$queryOfLog = "SELECT type from ".TABLE_LOGIN."
				                     				WHERE ".TABLE_LOGIN.".ID='$loginIDs'";   									
				                     				
				                     $rowsLog = $db->query($queryOfLog);		
									 $resLog=mysql_fetch_array($rowsLog);
									 
									 $loginType	=$resLog['type'];
											
                        	if($hproId==0)
                        	{
								$userName ='Admin';
							}
                        	else if($hproId!=0) {
							if($loginType=="Branch")
							{
							$qry="SELECT companyName FROM ".TABLE_COMPANY.",".TABLE_LOGIN."
								  WHERE ".TABLE_COMPANY.".ID=".TABLE_LOGIN.".userId
								  AND ".TABLE_LOGIN.".ID=$loginIDs";
							$rows = $db->query($qry);		
							$res=mysql_fetch_array($rows);
							$userName=$res['companyName'];	  
							}
							if($loginType=="Staff")
							{
							$qry="SELECT staffName FROM ".TABLE_STAFF.",".TABLE_LOGIN."
								  WHERE ".TABLE_STAFF.".ID=".TABLE_LOGIN.".userId
								  AND ".TABLE_LOGIN.".ID=$loginIDs";
							$rows = $db->query($qry);		
							$res=mysql_fetch_array($rows);
							$userName=$res['staffName'];
							}
							}
	                    	//
	                        ?>
	                     <tr>                            
	                        <td><?php echo ++$i; ?></td>
	                        <td><?php if($hproId==0) {echo "Admin";} else { echo $userName; } ?></td>			                           
	                        <td><?php echo $row['roomType']; ?></td>			                           
	                        <td>
	                         <?php if($_SESSION['travelType']=="Admin"){?>
	                        	<a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?id=<?php echo $row['ID'];?>&op=delete">Delete</a>
	                         <?php } //end of if condition?>
	                         
	                         <?php if($_SESSION['travelType']=="Branch" && $proId==$hproId){?>
	                        	<a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?id=<?php echo $row['ID'];?>&op=delete">Delete</a>
	                         <?php } //end of if condition?>
	                         
	                         <?php if($_SESSION['travelType']=="Staff" && $basicSettingDelete && $logID==$loginIDs){?>
	                         <a class="show_table_lnk show_table_lnk_del" onclick="return delete_type();" href="do.php?id=<?php echo $row['ID'];?>&op=delete">Delete</a>
	                         <?php } else if($_SESSION['travelType']=="Staff" && $logID!=$loginIDs) {//end of if condition?>
				                            <a href="javascript:void(0)" class="bd_lnk bd_lnk_disabled">Delete</a>
				                            <?php } ?>
	                         </td>
	                    </tr>
	                        <?php
	                    }
	                }
	                    ?>
					            
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>