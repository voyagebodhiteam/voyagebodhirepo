<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
        $finYear = $_SESSION['finYear'];
	$check 	= 	"".TABLE_VISAFOR.".loginId='$logId' AND ".TABLE_VISAFOR.".proId='$proId' and ".TABLE_VISAFOR.".finYear='$finYear'";
}
$proId 	 	= 	$_SESSION['proId'];
$loginId	=	$_SESSION['travelId'];
$finYear=$_SESSION['finYear'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['visaFor']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
			$visaFor			= $App->convert($_REQUEST['visaFor']);
			
            $existId = $db->existValuesId(TABLE_VISAFOR, "visafor='$visaFor' and proId=$proId");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
                $data['visafor']	=	$visaFor;
                $data['proId']		=	$proId;
                $data['loginId']	=	$loginId;
                $data['finYear']		=	$finYear;
                
				$success = $db->query_insert(TABLE_VISAFOR, $data);
				
                $db->close();
				if ($success) 
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");
                }
                else
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
        break;
    
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();

		$success1= @mysql_query("DELETE FROM `".TABLE_VISAFOR."` WHERE ID='{$deleteId}' AND $check ");
		
        $db->close();
        if ($success1) 
        {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } 
        else 
        {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
}
?>