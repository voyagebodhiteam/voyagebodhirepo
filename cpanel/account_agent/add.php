<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<script>
	function check()
	{
		var proceed=confirm("Do you Want to Continue ?");
		if(proceed==true)
		{
		window.submit();
		}
		else
		{
		return false;
		}
	}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" onsubmit="return check()">
                <div class="bd_panel_head">
                    <h3>AGENT</h3>
                </div>
                <div class="bd_panel_body">
                	<!--<div class="row">
                    	<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Agent Name</label>--> <!--select from account where accGpId=37-->
								<!--<div class="aj_rel_box aj_agent">
									<input type="text" name="agentName" autocomplete="off"  id="agentName">								
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>					
						<div class="col-lg-2 col-md-2 col-sm-2">
							<div class="form_block">
								<label>Agent ID</label> --><!--select from account where accGpId=37-->
							<!--	<div class="aj_rel_box aj_agent">
									<input type="text" name="agentId" autocomplete="off"  id="agentId">
									<ul class="aj_live_items">

									</ul>
								</div>
							</div>
						</div>
					</div>-->
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-md-3 form_block_row">
                            <div class="form_block">
                                <label>Agent Name <span class="valid">*</span></label>
                                <input type="text" name="accountName" required="">
                            </div>
                        </div>
                   </div>
                   <div class="row">
                        <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form_block">
                                <label>Mobile <span class="valid">*</span></label>
                                <input type="text" name="mobile" required="" >
                            </div>
                        </div>
                   </div>
                   <div class="row">
                         <div class="col-lg-3 col-sm-3 col-md-3">
                            <div class="form_block">
                                <label>Address</label>
                                <textarea name="address" required=""></textarea>
                            </div>
                      	  </div>
                   </div>
                   <input type="hidden" name="accountGroupId" id="accountGroupId" value="48" >
                   <input type="hidden" name="agentId" id="agentId" value="0" >
                        <!--<div class="col-lg-2 col-sm-2 col-md-2">
                            <div class="form_block">
                                <label>Master Group*</label>
                                <select name="accountGroupId" required="">
                                    <option value="">Select</option>
                                    <?php
                                    $select = "select * from " . TABLE_ACCOUNT_GROUP . " Order by acGroup";
                                    $res = $db->query($select);
                                    while ($row = mysql_fetch_array($res)) {
                                        ?>
                                        <option
                                            value="<?php echo $row['ID']; ?>"><?php echo $row['acGroup']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                         </div>-->
                         
                         <!-- 37 means Supplier/ Sundry Creditors in account_group table-->
                         <div class="row">
                         <div class="col-lg-2 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block">
                                <label>Opening Balance <span class="valid">*</span></label>
                                <input type="text" name="openingBal" required="" value="0">
                            </div>
                         </div>
                         <div class="col-lg-2 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block" >
                                <label>Credit/Debit <span class="valid">*</span></label>
                                <select name="payType" required="">
                                    <option value="Credit" >Credit</option>
                                   <!-- <option value="Debit">Debit</option>-->
                                </select>
                            </div>
						  </div>
						  <div class="col-lg-2 col-sm-2 col-md-2"  style="display: none;">	
                            <div class="form_block">
                                <label>Description</label>
                                <textarea name="description" ></textarea>
                            </div>
                          </div>
                          
                          <div class="row" >
                          <div class="col-lg-12 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block_section_head">
                                <label>Contact Details</label>
                            </div>
                          </div>
                          </div>
                          <div class="col-lg-2 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block">
                                <label>Contact Person Name</label>
                                <input type="text" name="contactPerson" >
                            </div>
                          </div>	
                        <div class="col-lg-2 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block">
                                <label>Pin</label>
                                <input type="text" name="pin" style="display: none;">
                            </div>
						</div>
						<div class="col-lg-2 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block">
                                <label>City</label>
                                <input type="text" name="city" style="display: none;">
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block">
                                <label>Phone</label>
                                <input type="text" name="phone" style="display: none;">
                            </div>
                        </div>
                        
                        <div class="col-lg-2 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block">
                                <label>Email</label>
                                <input type="text" name="email" >
                            </div>
                        </div>
                        
                         
                        
                        <div class="row">
                        <div class="col-lg-12 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block_section_head">
                                <label>Correspondance Address</label>
                            </div>
                        </div>
                        </div>
                        
                        <div class="col-lg-2 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block">
                                <label>Address2</label>
                                <textarea name="address2" ></textarea>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block">
                                <label>Pin2</label>
                                <input type="text" name="pin2" style="display: none;">
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2 col-md-2" style="display: none;">
                            <div class="form_block">
                                <label>City2</label>
                                <input type="text" name="city2" style="display: none;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
