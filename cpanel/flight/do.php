<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}


$proId = $_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$finYear	=	$_SESSION['finYear'];

$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch ($optype) 
{
    // NEW SECTION
    case 'index':

        if (!$_REQUEST['flightName']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = 0;
			$flightNo			= $App->convert($_REQUEST['flightNo']);
            $flightName			= $App->capitalize($_REQUEST['flightName']); 
            
            $existId = $db->existValuesId(TABLE_FLIGHT, "flightName='$flightName' and proId=$proId");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            {
                $data['flightNo']	=	$flightNo;
                $data['flightName']	=	$flightName;
               	$data['proId']		=	$proId;
                $data['loginId']	=	$loginId; 
                $data['finYear']	=	$finYear;
                if($_FILES["image"]["name"])
                {
                	$image	=	$App->imageValidation($_FILES["image"]["name"]);
	                if($image==1)
	                {
						$date	= 	date("YmdHis");
		                $temp 	= 	explode(".", $_FILES["image"]["name"]);
						$newfilename = $date.'.' . end($temp);
						if(move_uploaded_file($_FILES["image"]["tmp_name"], "../../uploads/flight/" . $newfilename))
		                {
							$img="../../uploads/flight/" . $newfilename;		
						}

						$data['image']			=	$App->convert($img);
						$success = $db->query_insert(TABLE_FLIGHT, $data);
					}
				}
				else
				{
					$success = $db->query_insert(TABLE_FLIGHT, $data);
				}

                $db->close();
				if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details added successfully");                
                } 
                
                else if ($image==0 and $success==0)
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Invalid image format.");                   
                }
                else
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to add details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
        break;
        
     case 'edit':
    	$editId = $_REQUEST['editId'];
    	if (!$_REQUEST['flightName']) 
        {
            $_SESSION['msg'] = $App->sessionMsgCreate("error", "Error, Invalid Details!<br />Please try again.");
            header("location:index.php");
        } 
        else 
        {
            $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
            $db->connect();
            $success = $image = 0;
            $flightNo			= $App->convert($_REQUEST['flightNo']);
            $flightName			= $App->capitalize($_REQUEST['flightName']); 
              $existId = $db->existValuesId(TABLE_FLIGHT, "flightName='$flightName' and proId=$proId and ID!='$editId'");
            if ($existId > 0) 
            {
                $_SESSION['msg'] = $App->sessionMsgCreate('error', "Details already exist. Please try again with correct details.");
                header("location:index.php");
            }
            else 
            { 
            $data['flightNo']	=	$flightNo;
            $data['flightName']	=	$flightName;
           	$data['proId']		=	$proId;
            $data['loginId']	=	$loginId;
            $data['finYear']	=	$finYear;                   
            if ($_FILES["image"]["name"]) {
				$res=mysql_query("select image from ".TABLE_FLIGHT." where ID='{$editId}'");
				$row=mysql_fetch_array($res);
				$imageDel=$row['image'];
				if (file_exists($imageDel))
				{
					unlink($imageDel);
				}
			}
           if($_FILES["image"]["name"])
                {
                	$image	=	$App->imageValidation($_FILES["image"]["name"]);
	                if($image==1)
	                {
						$date	= 	date("YmdHis");
		                $temp 	= 	explode(".", $_FILES["image"]["name"]);
						$newfilename = $date.'.' . end($temp);
						if(move_uploaded_file($_FILES["image"]["tmp_name"], "../../uploads/flight/" . $newfilename))
		                {
							$img="../../uploads/flight/" . $newfilename;		
						}

						$data['image']			=	$App->convert($img);
						$success = $db->query_update(TABLE_FLIGHT, $data, "ID = $editId");
					}
				}
				else
				{
					$success = $db->query_update(TABLE_FLIGHT, $data, "ID = $editId");
				}

                $db->close();
				if ($success) 
                {
                    //$_SESSION['msg'] = " Details Added Successfully";
                    $_SESSION['msg'] = $App->sessionMsgCreate('success', "Details uploaded successfully");                
                } 
                
                else if ($image==0 and $success==0)
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Invalid image format.");                   
                }
                else
                {
                    $_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed to upload details. Please try again.");                   
                }
                 header("location:index.php");
            }
        }
        break;
    
    // DELETE SECTION
    case 'delete':
        $deleteId = $_REQUEST['id'];//echo $deleteId;die;
        $success = 0;

        $db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
        $db->connect();
  
            $res=mysql_query("select image from ".TABLE_FLIGHT." where ID='{$deleteId}' and finYear=$finYear");
			$row=mysql_fetch_array($res);
			$image=$row['image'];	
			$success1= @mysql_query("DELETE FROM `".TABLE_FLIGHT."` WHERE ID='{$deleteId}' and finYear=$finYear");
			if($success1)					
			{			//echo $image;die;																														
				if (file_exists($image)) 	
				{
				unlink($image);					
				} 						
			}	
        
        $db->close();
        if ($success1) {
            $_SESSION['msg'] =  $App->sessionMsgCreate('success', "Details Deleted Successfully");	
        } else {
            $_SESSION['msg'] =  $App->sessionMsgCreate('error', "You can't delete. Because this data is used some where else");
        }
        header("location:index.php");
        break;
}
?>