<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post" action="do.php?op=index" class="default_form" enctype="multipart/form-data">
                <div class="bd_panel_head">
                    <h3>FLIGHT REGISTRATION</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                        	<div class="form_block" style="display: none;">
                                <label>Flight No</label>
                                <input type="text" name="flightNo" id="flightNo" value="0" >
                            </div>
                            <div class="form_block">
                                <label>Flight Name <span class="valid">*</span></label>
                                <input type="text" name="flightName" id="flightName" required="">
                            </div>
                            <div class="form_block">
                                <label>Image</label>
                                <input type="file" name="image" id="image" >
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="bd_panel_footer">
                    <div class="panel_row">
                        <div class="form_block_full">
                            <input type="submit" name="form" value="SAVE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
