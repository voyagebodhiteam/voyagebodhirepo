<?php
require("../../config/config.inc.php");
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}
error_reporting(E_ALL ^ E_DEPRECATED);

$proId = $_SESSION['proId'];
$loginId=$_SESSION['travelId'];
$optype = (strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
switch ($optype) 
{
    // NEW SECTION
    case 'backup':
				backup_tables(DB_SERVER,DB_USER,DB_PASS,DB_DATABASE);
				//backup_tables('localhost','root','','markaz');
				/* backup the db OR just a table */
				$_SESSION['msg'] = $App->sessionMsgCreate('success', "Downloded successfully");		
                header("location:index.php");
    break;
    
    //delete file from folder
    case 'delete':
    	$success = 0;
    	$file = $_REQUEST['file'];
    	$success =  unlink($file);
    	if($success)
    	{
			$_SESSION['msg'] = $App->sessionMsgCreate('success', "Deleted successfully");	
		}
		else
		{
			$_SESSION['msg'] = $App->sessionMsgCreate('error', "Failed.");
		}
		header("location:index.php");
    	
    break;
    
    case 'download':
    	$file = $_REQUEST['file'];
    	/*//$filename = 'backup2.sql';			
		$mime = "application/x-gzip";
		header( "Content-Type: " . $mime );		
		header( 'Content-Disposition: attachment; filename="' . $file . '"' );*/
	    
	    header("Content-Type: application/octet-stream");

		$file = $_REQUEST['file'];
		header( 'Content-Disposition: attachment; filename="' . $file . '"' );  
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Description: File Transfer");            
		header("Content-Length: " . filesize($file));
		flush(); // this doesn't really matter.
		$fp = fopen($file, "r");
		while (!feof($fp))
		{
		    echo fread($fp, 65536);
		    flush(); // this is essential for large downloads
		} 
		fclose($fp);
	    
    break;
    
    
}

function backup_tables($host,$user,$pass,$name,$tables = '*')
				{
					$link = mysql_connect($host,$user,$pass);
					mysql_select_db($name,$link);
					
					//get all of the tables
					if($tables == '*')
					{
						$tables = array();
						$result = mysql_query('SHOW TABLES');
						while($row = mysql_fetch_row($result))
						{
							$tables[] = $row[0];
						}
					}
					else
					{
						$tables = is_array($tables) ? $tables : explode(',',$tables);
					}
					$return = "/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
								/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
								/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
								/*!40101 SET NAMES utf8 */;
								/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
								/*!40103 SET TIME_ZONE='+00:00' */;
								/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
								/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
								/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
								/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
								";
					//cycle through
					foreach($tables as $table)
					{
						$result = mysql_query('SELECT * FROM '.$table);
						$num_fields = mysql_num_fields($result);
						
						$return.= 'DROP TABLE IF EXISTS '.$table.';';
						$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
						$return.= "\n\r".$row2[1].";\n\r";
						
						for ($i = 0; $i < $num_fields; $i++) 
						{
							while($row = mysql_fetch_row($result))
							{
								$return.= 'INSERT INTO '.$table.' VALUES(';
								for($j=0; $j < $num_fields; $j++) 
								{
									$row[$j] = addslashes($row[$j]);
									$row[$j] = ereg_replace("\n","\\n",$row[$j]);
									if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
									if ($j < ($num_fields-1)) { $return.= ','; }
								}
								$return.= ");\n";
							}
						}
						$return.="\n\r";
					}
					$return.="\n\r
								/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
								/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
								/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
								/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
								/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
								/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
								/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
								
						";
					
					//save file
					$handle = fopen('../../backup/markaz-'.date('Y-m-d H-i-s').'.sql','w+');
					fwrite($handle,$return);
					fclose($handle);
    			}
    			
    			
?>