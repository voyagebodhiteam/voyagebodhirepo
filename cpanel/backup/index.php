<?php
require('../admin_header.php');

if($_SESSION['travelId']=="")
{
	header("location:../../logout.php");
}

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);

?>
<script>
   
//delete row in index page

function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="bd_panel bd_panel_default bd_panel_shadow">
            <form method="post"  action="do.php?op=backup" class="default_form" enctype="multipart/form-data">
                <div class="bd_panel_head">
                    <h3>Backup</h3>
                </div>
                <div class="bd_panel_body">
                    <div class="row">
                    	<div class="col-lg-4 col-md-4 col-sm-4 form_block_row">
                        	<div class="form_block" >
                        		<input type="submit" value="BACKUP" />
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive show_table_wrap">
            <table class="table table-bordered table-striped show_table">
                <tbody>
                	<?php 
							$scan = scandir('../../backup',1);
							$flag = 0;
							foreach($scan as $file)
							{
							    if (!is_dir("../../backup/$file"))
							    {
							    	$flag = 1;
								?>
								<tr>
								<td><?php echo date("F d Y H:i:s.",filemtime('../../backup/'.$file));?></td>
								<!--<td><a href="../../backup/<?php echo $file;?>">Download</a></td>-->
								<td><a href="do.php?op=download&file=../../backup/<?php echo $file;?>">Download</a></td>
								<td><a onclick="return delete_type();" href="do.php?op=delete&file=../../backup/<?php echo $file;?>">DELETE</a></td>
								</tr>
								<?php
								}
						   }
						   if($flag==0)
						   {
						   	?>
							<tr>
							<td colspan="3" align="center">There is no files</td>
							</tr>
							<?php
						   }
						   
                	?>
                </tbody>
             </table>
        </div>
    </div>
</div>

<?php
require('../admin_footer1.php');
require('../admin_footer2.php');
?>
