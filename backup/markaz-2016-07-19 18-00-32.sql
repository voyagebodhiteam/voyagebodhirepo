/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
								/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
								/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
								/*!40101 SET NAMES utf8 */;
								/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
								/*!40103 SET TIME_ZONE='+00:00' */;
								/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
								/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
								/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
								/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
								DROP TABLE IF EXISTS account_group;
CREATE TABLE `account_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `acGroup` text NOT NULL,
  `parent` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
INSERT INTO account_group VALUES("1","Account","0","0","0");
INSERT INTO account_group VALUES("2","Balance Sheet","1","0","0");
INSERT INTO account_group VALUES("3","Profit & Loss Account","1","0","0");
INSERT INTO account_group VALUES("4","Assets","2","0","0");
INSERT INTO account_group VALUES("5","Liabilities","2","0","0");
INSERT INTO account_group VALUES("6","Current Assets","4","0","0");
INSERT INTO account_group VALUES("7","Fixed Assets","4","0","0");
INSERT INTO account_group VALUES("8","Cash In Hand","6","0","0");
INSERT INTO account_group VALUES("9","Deposits","6","0","0");
INSERT INTO account_group VALUES("10","Investments","6","0","0");
INSERT INTO account_group VALUES("11","Loans & Advances (asset)","6","0","0");
INSERT INTO account_group VALUES("12","Bank A/c","6","0","0");
INSERT INTO account_group VALUES("13","Expenses","3","0","0");
INSERT INTO account_group VALUES("14","Income","3","0","0");
INSERT INTO account_group VALUES("15","Cash","8","0","0");
INSERT INTO account_group VALUES("16","Capital","5","0","0");
INSERT INTO account_group VALUES("17","Current Liabilities","5","0","0");
INSERT INTO account_group VALUES("18","Loans (liability)","5","0","0");
INSERT INTO account_group VALUES("19","Suspense Accounts","5","0","0");
INSERT INTO account_group VALUES("20","Bank OD A/C","17","0","0");
INSERT INTO account_group VALUES("21","Duties & Taxes","17","0","0");
INSERT INTO account_group VALUES("22","Journal Accounts","17","0","0");
INSERT INTO account_group VALUES("23","Provisions","17","0","0");
INSERT INTO account_group VALUES("24","Direct Expense","13","0","0");
INSERT INTO account_group VALUES("25","Indirect Expense","13","0","0");
INSERT INTO account_group VALUES("26","Direct Income","14","0","0");
INSERT INTO account_group VALUES("27","Indirect Income","14","0","0");
INSERT INTO account_group VALUES("28","Fittings","7","0","0");
INSERT INTO account_group VALUES("29","Advertisement","25","0","0");
INSERT INTO account_group VALUES("30","Donation Given","25","0","0");
INSERT INTO account_group VALUES("31","Electricity Bills","25","0","0");
INSERT INTO account_group VALUES("32","Miscellaneous Expense","25","0","0");
INSERT INTO account_group VALUES("33","Office Expense","25","0","0");
INSERT INTO account_group VALUES("34","Other Expenses","25","0","0");
INSERT INTO account_group VALUES("35","Telephone Bills","25","0","0");
INSERT INTO account_group VALUES("36","Other Income","27","0","0");
INSERT INTO account_group VALUES("37","Customer/ Sundry Debtors","6","0","0");
INSERT INTO account_group VALUES("38","Shipping Agents","17","0","0");
INSERT INTO account_group VALUES("39","Staff Deposits","17","0","0");
INSERT INTO account_group VALUES("40","Supplier/ Sundry Creditors","17","0","0");
INSERT INTO account_group VALUES("41","Discount Given ","24","0","0");
INSERT INTO account_group VALUES("42","Job Works","24","0","0");
INSERT INTO account_group VALUES("43","Purchases","24","0","0");
INSERT INTO account_group VALUES("44","Courier","26","0","0");
INSERT INTO account_group VALUES("45","Discount Recieved","26","0","0");
INSERT INTO account_group VALUES("46","Sales","26","0","0");
INSERT INTO account_group VALUES("47","Services","26","0","0");
INSERT INTO account_group VALUES("48","Commission Agent","25","0","0");
INSERT INTO account_group VALUES("49","Incentives Given","25","0","0");
INSERT INTO account_group VALUES("50","Printing Expense","25","0","0");
INSERT INTO account_group VALUES("51","Rent Given","25","0","0");
INSERT INTO account_group VALUES("52","Salaries","25","0","0");
INSERT INTO account_group VALUES("53","Sales Expenses","25","0","0");
INSERT INTO account_group VALUES("54","Tax Paid","25","0","0");
INSERT INTO account_group VALUES("55","Tax Recieved","25","0","0");
INSERT INTO account_group VALUES("56","Incentives Recieved","27","0","0");
INSERT INTO account_group VALUES("57","Branches","6","0","0");
INSERT INTO account_group VALUES("58","Main Branches","17","0","0");

DROP TABLE IF EXISTS accounts;
CREATE TABLE `accounts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `agentId` int(11) NOT NULL,
  `accountGroupId` int(11) NOT NULL,
  `accountName` varchar(200) NOT NULL,
  `openingBal` double NOT NULL,
  `payType` varchar(20) NOT NULL,
  `masterLedger` int(11) NOT NULL,
  `addDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `description` text NOT NULL,
  `contactPerson` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address2` text NOT NULL,
  `pin2` varchar(20) NOT NULL,
  `city2` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `accountGroupId` (`accountGroupId`),
  CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`accountGroupId`) REFERENCES `account_group` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;
INSERT INTO accounts VALUES("1","0","19","Suspense","0","Credit","1","2016-03-22","1","0","Suspense Account","","","","","","","","","","");
INSERT INTO accounts VALUES("2","0","8","Cash Account","0","Credit","1","2016-03-22","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("3","0","24","Salary Payments","0","Credit","1","2016-03-22","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("4","0","14","Airline Receivable","0","Debit","1","2016-04-05","1","0","Airline charge received","","","","","","","","","","");
INSERT INTO accounts VALUES("5","0","13","Airline Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("6","0","14","Visa Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("7","0","13","Visa Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("8","0","14","Package Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("9","0","13","Package Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("10","0","14","Hotel Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("11","0","13","Hotel Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("12","0","14","Transport Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("13","0","13","Transport Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("14","0","14","Misc Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("15","0","13","Misc Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("16","0","25","Discount Given","0","Credit","1","2016-05-25","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("17","0","27","Discount Received","0","Credit","1","2016-05-25","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("18","0","25","Tax","0","Credit","1","2016-06-24","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("19","0","25","Tds","0","Credit","1","2016-06-24","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("20","0","27","Tac","0","Credit","1","2016-06-24","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("21","0","25","Package Extra Chrg","0","Credit","1","2016-06-29","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("40","0","37","CREATIVE KDNGLR 2","0","Debit","0","2016-07-16","5","2","","SHANMUGAN","","","KODUNGALLUR","0480 3005058","0480 3005058","visakdr@creativeholidaysindia.com","","","");
INSERT INTO accounts VALUES("41","0","40","ALPHA CCJ","0","Credit","0","2016-07-16","5","2","","SALAM","","","KOZHIKODE","0495 2435946","9400655946","visa@alphaindia.biz","","","");
INSERT INTO accounts VALUES("42","0","40","ALTA CCJ","0","Credit","0","2016-07-16","5","2","","SHAMNAS","","","KOZHIKODE","0495 4050502","0495 4050503","visa@altatravel.in","","","");
INSERT INTO accounts VALUES("43","0","40","SALIL BOM","0","Credit","0","2016-07-16","5","2","","SALIL","","","BOMBAY","9892519222","9987244222","salilcs84@gmail.com","","","");
INSERT INTO accounts VALUES("44","0","40","TRAVEL PARTNER","0","Credit","0","2016-07-16","5","2","","RISANA","","","","0495 4016677","9544053344","CCJ@travelpartnerholidays.com","","","");
INSERT INTO accounts VALUES("45","0","37","sahad staff","0","Debit","0","2016-07-16","5","2","","SAHAD","","","TRISSUR","","9567656784","mtmsahad@gmail.com","","","");
INSERT INTO accounts VALUES("46","0","37","CREATIVE KDNGLR 1","0","Debit","0","2016-07-16","5","2","","SHANMUGAN","","","KODUNGALLUR","0480 3006000","9349231973","kdr@creative-india.com","","","");
INSERT INTO accounts VALUES("47","0","37","WALKING CUSTOMER","0","Credit","0","2016-07-16","5","2","","","","","","","0","","","","");
INSERT INTO accounts VALUES("48","0","12","FEDERAL BANK","0","Credit","0","2016-07-18","5","2","","","","","","","","","","","");
INSERT INTO accounts VALUES("49","0","12","SOUTH INDIAN BANK","0","Credit","0","2016-07-18","5","2","","","","","","","","","","","");
INSERT INTO accounts VALUES("50","0","37","MTM VETTICHIRA","0","Credit","0","2016-07-18","6","2","","","","","","04942617140","0","","","","");
INSERT INTO accounts VALUES("52","0","37","KUNHAYI PANNIKOTATH","0","Credit","0","2016-07-18","6","2","","","","","","","9946607862","","","","");
INSERT INTO accounts VALUES("53","0","37","NAFEESA MOYTHEENKOYA","0","Credit","0","2016-07-18","6","2","","","","","","","9447399396","","","","");
INSERT INTO accounts VALUES("54","0","37","SUHARA MAMBATH","0","Credit","0","2016-07-18","6","2","","","","","","","8891570223","","","","");
INSERT INTO accounts VALUES("55","0","37","PATHUMMA THURUTHIYIL MEETHAL","0","Credit","0","2016-07-18","6","2","","","","","","","8289843711","","","","");
INSERT INTO accounts VALUES("56","0","37","MAYIN KUTTY THONIKKADAVATH","0","Credit","0","2016-07-18","6","2","","","","","","","9447357815","","","","");
INSERT INTO accounts VALUES("57","0","37","Ibraheem Sqf Kottur","0","Credit","0","2016-07-18","6","2","","","","","","","9846345001","","","","");
INSERT INTO accounts VALUES("58","0","37","SUBAIDA VELLARATHODI","0","Credit","0","2016-07-18","6","2","","","","","","","9895925036","","","","");
INSERT INTO accounts VALUES("59","0","37","MUSTHAFA KAYANIKARA","0","Credit","0","2016-07-18","6","2","","","","","","","8606162786","","","","");
INSERT INTO accounts VALUES("60","0","37","FLYHIND TLY","0","Debit","0","2016-07-18","5","2","","LAJIMA","","","TALASSERY","0490 2344533","0","tlyflyhind@gmail.com","","","");
INSERT INTO accounts VALUES("61","0","37","AMEER","0","Credit","0","2016-07-18","6","2","","","","","","","9947809526","","","","");
INSERT INTO accounts VALUES("62","0","37","ABDUL RASHEED MANNARATTIL","0","Credit","0","2016-07-18","6","2","","","","","","","9847020540","","","","");
INSERT INTO accounts VALUES("63","0","37","PAIKAT TRAVELS","0","Debit","0","2016-07-18","5","2","","","","","","","04954012999","paikattravels@gmail.com","","","");
INSERT INTO accounts VALUES("64","0","37","SALIL BOM","0","Debit","0","2016-07-18","5","2","","","","","","","9892519222","salilcs84@gmail.com","","","");

DROP TABLE IF EXISTS airline_flight;
CREATE TABLE `airline_flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlinePassId` int(11) NOT NULL,
  `airlineName` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `journeyDate` date NOT NULL,
  `sectorFrom` varchar(100) NOT NULL,
  `sectorTo` varchar(100) NOT NULL,
  `classType` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `tax` varchar(30) NOT NULL,
  `yq` float NOT NULL,
  `procCharge` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlinePassId` (`airlinePassId`),
  CONSTRAINT `airline_flight_ibfk_2` FOREIGN KEY (`airlinePassId`) REFERENCES `airline_passenger` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO airline_flight VALUES("1","1","","ix25322","8965754664","2016-07-27","ccj","jed","e","45600","1200","0","500","2","5");

DROP TABLE IF EXISTS airline_passenger;
CREATE TABLE `airline_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlineId` int(11) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationship` varchar(30) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlineId` (`airlineId`),
  CONSTRAINT `airline_passenger_ibfk_1` FOREIGN KEY (`airlineId`) REFERENCES `invoice_airline` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO airline_passenger VALUES("1","1","Test","2016-07-19","0","M","kerala","","india","","E000200","2016-07-20","2016-07-26","kerala","2","5");

DROP TABLE IF EXISTS cash_payment;
CREATE TABLE `cash_payment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS cash_receipt;
CREATE TABLE `cash_receipt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
INSERT INTO cash_receipt VALUES("3","1","visa","1","14900","","ONLINE","BANK","2","5");
INSERT INTO cash_receipt VALUES("4","1","visa","2","44700","","ONLINE","BANK","2","5");
INSERT INTO cash_receipt VALUES("6","2","visa","4","0","","0","CASH","2","5");
INSERT INTO cash_receipt VALUES("8","2","visa","10","15500","","0","BANK","2","5");

DROP TABLE IF EXISTS company;
CREATE TABLE `company` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `tds` int(11) NOT NULL,
  `circle` varchar(100) NOT NULL,
  `resPerson` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `remark` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO company VALUES("2","Mtm Travel Service","","673004","KOZHIKODE","04954011515","8547116314","mtmvisa@gmail.com","","0","","","","");

DROP TABLE IF EXISTS flight;
CREATE TABLE `flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `flightNo` varchar(50) NOT NULL,
  `flightName` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO flight VALUES("1","0","Saudi Airlines","","2","6");
INSERT INTO flight VALUES("2","0","Air India","","2","6");

DROP TABLE IF EXISTS hotel;
CREATE TABLE `hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(10) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO hotel VALUES("1","Mubarak Plaza","","0","Makka","","Ksa","00","000","","","2","6");

DROP TABLE IF EXISTS hotel_customer;
CREATE TABLE `hotel_customer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelInvoiceId` int(11) NOT NULL,
  `roomType` varchar(20) NOT NULL,
  `noOfRooms` int(11) NOT NULL,
  `noOfPersons` int(11) NOT NULL,
  `rate` float NOT NULL,
  `subTax` varchar(30) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_airline;
CREATE TABLE `invoice_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float NOT NULL,
  `mainProcCharge` float NOT NULL,
  `mainOtherCharge` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `mainTax` varchar(30) NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO invoice_airline VALUES("1","1","2016-07-19","47","0","0","0","","","0","0","47300","Pending","2","5");

DROP TABLE IF EXISTS invoice_hotel;
CREATE TABLE `invoice_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `hotelId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `noOfNight` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `checkInDate` date NOT NULL,
  `checkInTime` varchar(20) NOT NULL,
  `checkOutDate` date NOT NULL,
  `checkOutTime` varchar(20) NOT NULL,
  `billingIns` varchar(100) NOT NULL,
  `reference` varchar(100) NOT NULL,
  `tax` varchar(30) NOT NULL,
  `tds` float NOT NULL,
  `otherCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_misc;
CREATE TABLE `invoice_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `basicAmount` float NOT NULL,
  `tds` float NOT NULL,
  `tax` varchar(20) NOT NULL,
  `otherCharge` float NOT NULL,
  `procCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `narration` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_package;
CREATE TABLE `invoice_package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `packageId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `proCharge` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `discount` float DEFAULT '0',
  `netAmount` float NOT NULL DEFAULT '0',
  `description` varchar(200) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package VALUES("1","1","2016-07-16","1","47","0","0","0","0","0","0","","2","5");
INSERT INTO invoice_package VALUES("8","2","2016-07-18","1","50","0","0","0","0","0","0","","2","6");
INSERT INTO invoice_package VALUES("9","3","2016-07-18","1","52","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("10","4","2016-07-18","1","53","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("11","5","2016-07-18","1","54","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("12","6","2016-07-18","1","55","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("13","7","2016-07-18","1","56","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("15","8","2016-07-18","1","57","0","0","0","0","0","0","","2","6");
INSERT INTO invoice_package VALUES("16","9","2016-07-18","1","58","0","0","0","0","0","700000","","2","6");
INSERT INTO invoice_package VALUES("20","13","2016-07-18","1","59","0","0","0","0","0","0","","2","6");
INSERT INTO invoice_package VALUES("18","11","2016-07-18","1","61","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("19","12","2016-07-18","1","62","0","0","0","0","0","700000","","2","6");

DROP TABLE IF EXISTS invoice_package_airline;
CREATE TABLE `invoice_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_airline VALUES("1","1","1","","0","2","5");
INSERT INTO invoice_package_airline VALUES("19","8","19","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("18","8","18","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("17","8","17","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("16","8","16","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("20","8","20","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("21","9","21","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("22","10","22","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("23","11","23","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("24","12","24","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("25","13","25","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("27","15","27","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("28","15","28","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("29","16","29","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("30","16","30","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("37","20","37","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("36","20","36","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("33","18","33","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("34","19","34","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("35","19","35","","350000","2","6");

DROP TABLE IF EXISTS invoice_package_hotel;
CREATE TABLE `invoice_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `roomType` varchar(100) NOT NULL,
  `bedStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_passenger;
CREATE TABLE `invoice_package_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `packageId` int(11) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` date DEFAULT NULL,
  `age` int(11) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `birthPlace` varchar(100) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(50) NOT NULL,
  `dateOfIssue` date DEFAULT NULL,
  `validUpTo` date DEFAULT NULL,
  `issuePlace` varchar(100) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_passenger VALUES("1","1","1","","","","0","","","","","","","","","0","2","5");
INSERT INTO invoice_package_passenger VALUES("18","8","1","MOOSSA KUMBALATH VALAPPIL","M","1965-05-02","51","Indian","kerala","","","G7880366","2009-03-18","2019-03-17","ABUDHABI","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("19","8","1","MARIYAKUTTY CHEMBALAKKARA","F","1963-06-01","53","Indian","kerala","","","K9735468","2013-03-13","2023-03-12","MALAPPURAM","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("20","8","1","KHADEEJA ORAVIL","F","1951-01-01","66","indian","kerala","","","L6381798","2013-12-17","2023-12-16","MALAPPURAM","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("17","8","1","NASEEMA MUSTHAFA","F","1974-03-12","42","Indian","kerala","HUSBAND","","L4214724","2013-10-09","2023-10-08","MALAPPURAM","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("16","8","1","MUSTHAFA HASSAN","M","1962-05-16","54","Indian","kerala","","","K8746279","2013-01-06","","05-01-2023","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("21","9","1","KUNHAYI PANNIKOTATH","M","1955-11-10","61","indian","kerala","","","M5807886","2015-01-28","2025-01-27","KOZHIKODE","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("22","10","1","NAFEESA MOYTHEENKOYA","F","1943-01-01","74","indian","kerala","","","L1578471","2013-04-25","2023-04-24","KOZHIKODE","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("23","11","1","SUHARA MAMBATH","F","1950-01-01","67","indian","kerala","","","N2162499","","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("24","12","1","PATHUMMA THURUTHIYIL MEETHAL","F","1960-05-10","56","indian","kerala","","","J8212967","2011-06-09","2021-06-08","KOZHIKODE","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("25","13","1","MAYIN KUTTY THONIKKADAVATH","M","1958-10-21","58","indian","kerala","","","N8565859","2016-03-09","2026-03-08","MALAPPURAM","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("27","15","1","KADEEJA MADATHIL","F","1955-01-01","62","Indian","kerala","","","K6698357","2012-11-22","2022-11-21","KOZHIKODE","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("28","15","1","FATHIMA KUNJALAVI","F","1946-01-01","71","indian","kerala","","","K6473146","2013-04-05","2023-04-04","KOZHIKODE","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("29","16","1","SUBAIDA VELLARATHODI","F","1968-05-31","48","indian","kerala","","","P0919902","2016-06-15","2026-06-14","MALAPPURAM","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("30","16","1","KUNJEEVI VELLARATHODI","F","1946-01-01","71","indian","kerala","","","K5368916","2012-10-29","2022-10-28","MALAPPURAM","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("37","20","1","NILAKKADA VATH HYRUNNEESA CHEMMEKKOTTU","F","1976-05-23","40","Indian","kerala","HUSBAND","","N0419816","2015-12-15","2025-12-14","KUWAIT","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("36","20","1","MUSTHAFA KAYANIKARA ","M","1970-06-01","46","Indian","kerala","","","N0419931","2015-12-16","2025-12-15","MALAPPURAM","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("33","18","1","SULAIMAN CHAMAKKALAYI MUHAMMED KUTTY","M","1967-01-15","50","Indian","kerala","","","N7797851","2016-02-09","2026-02-08","KOZHIKODE","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("34","19","1","ABDUL RASHEED MANNARATTIL","M","1976-04-10","40","indian","kerala","","","G1782208","2007-04-04","2017-04-03","KOZHIKODE","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("35","19","1","NASREENA ABDUL RASHEED","F","1987-10-08","29","indian","kerala","","","K9864596","2013-03-06","2023-03-05","KOZHIKODE","350000","2","6");

DROP TABLE IF EXISTS invoice_package_transportation;
CREATE TABLE `invoice_package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `seatStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_visa;
CREATE TABLE `invoice_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_transport;
CREATE TABLE `invoice_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `serviceProviderId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `totalDays` int(11) NOT NULL,
  `vehicleType` varchar(100) NOT NULL,
  `kmAllowed` float NOT NULL,
  `arrivalDetails` text NOT NULL,
  `pickUpFrom` varchar(100) NOT NULL,
  `pickUpTime` varchar(20) NOT NULL,
  `DepartureDetails` text NOT NULL,
  `dropAt` varchar(100) NOT NULL,
  `dropTime` varchar(20) NOT NULL,
  `route` text NOT NULL,
  `inclusive` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_visa;
CREATE TABLE `invoice_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `mainOtherCharge` double NOT NULL,
  `discount` varchar(50) NOT NULL,
  `netAmount` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
INSERT INTO invoice_visa VALUES("1","1","2016-07-16","40","0","0","15300","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("2","2","2016-07-16","40","0","0","44700","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("3","3","2016-07-16","45","0","0","15200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("4","4","2016-07-16","40","0","0","14900","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("5","5","2016-07-16","40","0","0","14900","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("6","6","2016-07-16","46","0","0","5250","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("7","7","2016-07-18","40","0","0","14900","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("8","8","2016-07-18","40","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("10","9","2016-07-18","60","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("11","10","2016-07-18","45","0","0","15500","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("12","11","2016-07-18","63","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("13","12","2016-07-18","40","0","0","29800","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("14","13","2016-07-18","40","0","0","29800","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("15","14","2016-07-18","45","0","0","15200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("16","15","2016-07-18","40","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("17","16","2016-07-18","64","0","0","17400","","","Pending","2","5");

DROP TABLE IF EXISTS login;
CREATE TABLE `login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
INSERT INTO login VALUES("1","0","admin","81dc9bdb52d04dc20036dbd8313ed055","Admin");
INSERT INTO login VALUES("3","2","sidheeq","ba856b0417652289251c866c3e22824e","Branch");
INSERT INTO login VALUES("5","2","irfad","11d0a18c25c1f6b40791a75f6bde5a97","Staff");
INSERT INTO login VALUES("6","3","sha","ca794fb2d950acf25c964ecc35f2d7e2","Staff");

DROP TABLE IF EXISTS misc_narration;
CREATE TABLE `misc_narration` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `narration` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package;
CREATE TABLE `package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageName` varchar(200) NOT NULL,
  `staff` int(11) NOT NULL,
  `leader` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `place` varchar(100) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `tds` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `remark` varchar(100) NOT NULL,
  `services` varchar(100) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `confirm` int(11) NOT NULL,
  `discount` float NOT NULL,
  `extra` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO package VALUES("1","Haj","2","Ameer","0","2016-08-10","haj 2016","40","0","0","0","","airline","350000","350000","50000","2","3","0","0","0");
INSERT INTO package VALUES("2","Haj 2016 B","3","Ameer","0","2016-08-10","","","0","0","0","","airline,hotel,visa,transportation","380000","365000","145000","2","6","0","0","0");

DROP TABLE IF EXISTS package_airline;
CREATE TABLE `package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `yq` float NOT NULL DEFAULT '0',
  `procCharge` float NOT NULL DEFAULT '0',
  `tax` float NOT NULL DEFAULT '0',
  `adultCharge` float NOT NULL,
  `childCharge` float NOT NULL,
  `infantCharge` float NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `packageId` (`packageId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO package_airline VALUES("1","1","0","0","0","350000","350000","50000","350000","350000","50000","","2","3");
INSERT INTO package_airline VALUES("3","2","0","0","0","85000","80000","20000","85000","80000","20000","","2","6");

DROP TABLE IF EXISTS package_airline_flights;
CREATE TABLE `package_airline_flights` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_airlineId` int(11) NOT NULL,
  `airline` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `sectorFrom` varchar(200) NOT NULL,
  `sectorTo` varchar(200) NOT NULL,
  `jDate` date NOT NULL,
  `class` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO package_airline_flights VALUES("1","1","1","","000","ccj","jed","2016-08-10","","2","3");
INSERT INTO package_airline_flights VALUES("3","2","3","1","SV000","ccj","jed","2016-08-10","e","2","6");

DROP TABLE IF EXISTS package_hotel;
CREATE TABLE `package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `hotelId` int(11) NOT NULL,
  `checkinDate` date DEFAULT NULL,
  `checkoutDate` date DEFAULT NULL,
  `noOfDays` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO package_hotel VALUES("2","2","1","2016-08-05","2016-08-19","14","2","6","0");

DROP TABLE IF EXISTS package_room_type;
CREATE TABLE `package_room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_hotelId` int(11) NOT NULL,
  `roomId` int(11) NOT NULL,
  `rate` float NOT NULL,
  `otherCharge` float NOT NULL,
  `total` float NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package_transportation;
CREATE TABLE `package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `vehicle` int(11) NOT NULL,
  `seatCharge` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO package_transportation VALUES("2","2","1","20000","0","0","0","2","6");

DROP TABLE IF EXISTS package_visa;
CREATE TABLE `package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `visaFor` varchar(100) NOT NULL,
  `visaFee` float NOT NULL DEFAULT '0',
  `vfs` float NOT NULL DEFAULT '0',
  `ddCharge` float NOT NULL DEFAULT '0',
  `serviceCharge` float NOT NULL DEFAULT '0',
  `otherCharge` float NOT NULL DEFAULT '0',
  `courierCharge` float NOT NULL DEFAULT '0',
  `total` float NOT NULL DEFAULT '0',
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO package_visa VALUES("2","2","2","125000","0","0","0","0","0","125000","0","0","2","6");

DROP TABLE IF EXISTS passenger;
CREATE TABLE `passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationship` varchar(30) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `mofa` varchar(50) NOT NULL,
  `emofa` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
INSERT INTO passenger VALUES("1","40","Bismi Ramshad","0000-00-00","0","F","","","Indian","","M2551864","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("2","40","Shamnas","0000-00-00","0","M","","","Indian","","P0917864","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("3","40","Shanaza","0000-00-00","0","F","","","Indian","","N9972437","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("4","40","Suhail","0000-00-00","0","M","","","Indian","","K5983739","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("5","40","Ashitha Moideen Kutty","0000-00-00","0","F","","","Indian","","M36831335","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("6","40","Fathima Isha","0000-00-00","0","F","","","Indian","","L6574162","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("7","40","Akhila Meenakshi","0000-00-00","0","F","","","Indian","","M8417567","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("8","40","Asain Kumar","0000-00-00","0","M","","","Indian","","K6166640","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("9","40","Sruthi","0000-00-00","0","M","","","Indian","","N6776784","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("10","40","Princy Mol","0000-00-00","0","M","","","Indian","","L2985948","0000-00-00","0000-00-00","","","","2","5");

DROP TABLE IF EXISTS purchase_airline;
CREATE TABLE `purchase_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO purchase_airline VALUES("1","1","2016-07-19","2016-07-19","44","5","1","46800","0","0","0","0","0","0","0","46800","","","0","0","2","5");

DROP TABLE IF EXISTS purchase_hotel;
CREATE TABLE `purchase_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_misc;
CREATE TABLE `purchase_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_pac_airline;
CREATE TABLE `purchase_pac_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO purchase_pac_airline VALUES("1","1","0","0","0","0","2","3");
INSERT INTO purchase_pac_airline VALUES("3","2","0","0","0","0","2","6");

DROP TABLE IF EXISTS purchase_package_airline;
CREATE TABLE `purchase_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date DEFAULT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL DEFAULT '0',
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_hotel;
CREATE TABLE `purchase_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_transport;
CREATE TABLE `purchase_package_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_visa;
CREATE TABLE `purchase_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_transport;
CREATE TABLE `purchase_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_visa;
CREATE TABLE `purchase_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
INSERT INTO purchase_visa VALUES("1","1","2016-07-16","2016-07-16","41","7","2","44400","0","0","0","0","0","0","0","44400","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("2","2","2016-07-16","2016-07-16","41","7","3","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("3","3","2016-07-16","2016-07-16","43","7","4","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("4","4","2016-07-16","2016-07-16","43","7","5","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("5","5","2016-07-16","2016-07-16","42","7","6","5150","0","0","0","0","0","0","0","5150","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("6","6","2016-07-18","2016-07-18","43","7","7","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("7","7","2016-07-18","2016-07-18","41","7","8","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("8","8","2016-07-18","2016-07-18","43","7","9","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("9","9","2016-07-18","2016-07-18","43","7","10","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("10","10","2016-07-18","2016-07-18","43","7","11","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("11","11","2016-07-18","2016-07-18","43","7","12","29000","0","0","0","0","0","0","0","29000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("12","12","2016-07-18","2016-07-18","43","7","13","29000","0","0","0","0","0","0","0","29000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("13","13","2016-07-18","2016-07-18","43","7","14","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("14","14","2016-07-18","2016-07-18","41","7","15","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("15","15","2016-07-18","2016-07-18","44","7","16","17000","0","0","0","0","0","0","0","17000","","","0","0","2","5");

DROP TABLE IF EXISTS room_type;
CREATE TABLE `room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `roomType` varchar(30) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
INSERT INTO room_type VALUES("1","Sharing","0","1");
INSERT INTO room_type VALUES("2","Twin","0","1");
INSERT INTO room_type VALUES("3","Triple","0","1");
INSERT INTO room_type VALUES("4","Four Bed","0","1");
INSERT INTO room_type VALUES("5","Five Bed","0","1");
INSERT INTO room_type VALUES("6","Six Bed","0","1");
INSERT INTO room_type VALUES("7","Child With Bed","0","1");
INSERT INTO room_type VALUES("8","Child Without Bed","0","1");

DROP TABLE IF EXISTS staff;
CREATE TABLE `staff` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffName` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `userType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
INSERT INTO staff VALUES("2","Irfad","9746915105","irfadamanath1@gmail.","staf","2","3");
INSERT INTO staff VALUES("3","Arshak","8089684701","muhammedsha87@gmail.","","2","3");

DROP TABLE IF EXISTS transaction;
CREATE TABLE `transaction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `fromLedger` int(11) NOT NULL,
  `toLedger` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(100) NOT NULL,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `remark` text NOT NULL,
  `addedDate` date NOT NULL,
  `transactionDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `referenceNo` int(11) NOT NULL,
  `details` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=latin1;
INSERT INTO transaction VALUES("1","40","6","1","Visa Receivable","0","15300","Visa Receivable : 1","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV1, submissionDate:16-07-2016, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("2","6","40","1","Visa Receivable","15300","0","Visa Receivable : 1","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV1, submissionDate:16-07-2016, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("3","40","6","2","Visa Receivable","0","44700","Visa Receivable : 2","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV2, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("4","6","40","2","Visa Receivable","44700","0","Visa Receivable : 2","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV2, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("5","41","7","1","Visa Payable","44400","0","","2016-07-16","2016-07-16","5","2","0","INV#1,sup:41,:against:2");
INSERT INTO transaction VALUES("6","7","41","1","Visa Payable","0","44400","","2016-07-16","2016-07-16","5","2","0","INV#1,sup:41,:against:2");
INSERT INTO transaction VALUES("7","45","6","3","Visa Receivable","0","15200","Visa Receivable : 3","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV3, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("8","6","45","3","Visa Receivable","15200","0","Visa Receivable : 3","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV3, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("9","41","7","2","Visa Payable","15000","0","","2016-07-16","2016-07-16","5","2","0","INV#2,sup:41,:against:3");
INSERT INTO transaction VALUES("10","7","41","2","Visa Payable","0","15000","","2016-07-16","2016-07-16","5","2","0","INV#2,sup:41,:against:3");
INSERT INTO transaction VALUES("11","40","6","4","Visa Receivable","0","14900","Visa Receivable : 4","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV4, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("12","6","40","4","Visa Receivable","14900","0","Visa Receivable : 4","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV4, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("13","43","7","3","Visa Payable","14500","0","","2016-07-16","2016-07-16","5","2","0","INV#3,sup:43,:against:4");
INSERT INTO transaction VALUES("14","7","43","3","Visa Payable","0","14500","","2016-07-16","2016-07-16","5","2","0","INV#3,sup:43,:against:4");
INSERT INTO transaction VALUES("15","40","6","5","Visa Receivable","0","14900","Visa Receivable : 5","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV5, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("16","6","40","5","Visa Receivable","14900","0","Visa Receivable : 5","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV5, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("17","43","7","4","Visa Payable","14500","0","","2016-07-16","2016-07-16","5","2","0","INV#4,sup:43,:against:5");
INSERT INTO transaction VALUES("18","7","43","4","Visa Payable","0","14500","","2016-07-16","2016-07-16","5","2","0","INV#4,sup:43,:against:5");
INSERT INTO transaction VALUES("19","46","6","6","Visa Receivable","0","5250","Visa Receivable : 6","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV6, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("20","6","46","6","Visa Receivable","5250","0","Visa Receivable : 6","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV6, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("21","42","7","5","Visa Payable","5150","0","","2016-07-16","2016-07-16","5","2","0","INV#5,sup:42,:against:6");
INSERT INTO transaction VALUES("22","7","42","5","Visa Payable","0","5150","","2016-07-16","2016-07-16","5","2","0","INV#5,sup:42,:against:6");
INSERT INTO transaction VALUES("23","47","8","1","Package Receivable","0","0","Invoice Package: 1","2016-07-16","2016-07-16","5","2","1","INV#IP1,Pac:Haj ,Pax: ,By:Irfad");
INSERT INTO transaction VALUES("24","8","47","1","Package Receivable","0","0","Invoice Package: 1","2016-07-16","2016-07-16","5","2","1","INV#IP1,Pac:Haj ,Pax: ,By:Irfad");
INSERT INTO transaction VALUES("29","40","48","1","Bank Receipts","59600","0","","2016-07-18","2016-07-18","5","2","0","INV#BR1,by:Irfad,from:CREATIVE KDNGLR 2");
INSERT INTO transaction VALUES("30","48","40","1","Bank Receipts","0","59600","","2016-07-18","2016-07-18","5","2","0","INV#BR1,by:Irfad,from:CREATIVE KDNGLR 2");
INSERT INTO transaction VALUES("33","40","2","2","Cash Receipts","0","0","","2016-07-18","2016-07-18","5","2","0","Cash Receipts(CR), INV#CR, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("34","2","40","2","Cash Receipts","0","0","","2016-07-18","2016-07-18","5","2","0","Cash Receipts(CR), INV#CR, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("47","50","8","2","Package Receivable","0","0","Invoice Package: 2","2016-07-18","2016-07-18","6","2","2","INV#IP2,Pac:Haj ,Pax:MUSTHAFA HASSAN,NASEEMA MUSTHAFA,MOOSSA KUMBALATH VALAPPIL,MARIYAKUTTY CHEMBALAKKARA,KHADEEJA ORAVIL ,By:Arshak");
INSERT INTO transaction VALUES("48","8","50","2","Package Receivable","0","0","Invoice Package: 2","2016-07-18","2016-07-18","6","2","2","INV#IP2,Pac:Haj ,Pax:MUSTHAFA HASSAN,NASEEMA MUSTHAFA,MOOSSA KUMBALATH VALAPPIL,MARIYAKUTTY CHEMBALAKKARA,KHADEEJA ORAVIL ,By:Arshak");
INSERT INTO transaction VALUES("49","52","8","3","Package Receivable","0","350000","Invoice Package: 3","2016-07-18","2016-07-18","6","2","3","INV#IP3,Pac:Haj ,Pax:KUNHAYI PANNIKOTATH ,By:Arshak");
INSERT INTO transaction VALUES("50","8","52","3","Package Receivable","350000","0","Invoice Package: 3","2016-07-18","2016-07-18","6","2","3","INV#IP3,Pac:Haj ,Pax:KUNHAYI PANNIKOTATH ,By:Arshak");
INSERT INTO transaction VALUES("51","53","8","4","Package Receivable","0","350000","Invoice Package: 4","2016-07-18","2016-07-18","6","2","4","INV#IP4,Pac:Haj ,Pax:NAFEESA MOYTHEENKOYA ,By:Arshak");
INSERT INTO transaction VALUES("52","8","53","4","Package Receivable","350000","0","Invoice Package: 4","2016-07-18","2016-07-18","6","2","4","INV#IP4,Pac:Haj ,Pax:NAFEESA MOYTHEENKOYA ,By:Arshak");
INSERT INTO transaction VALUES("53","54","8","5","Package Receivable","0","350000","Invoice Package: 5","2016-07-18","2016-07-18","6","2","5","INV#IP5,Pac:Haj ,Pax:SUHARA MAMBATH ,By:Arshak");
INSERT INTO transaction VALUES("54","8","54","5","Package Receivable","350000","0","Invoice Package: 5","2016-07-18","2016-07-18","6","2","5","INV#IP5,Pac:Haj ,Pax:SUHARA MAMBATH ,By:Arshak");
INSERT INTO transaction VALUES("55","55","8","6","Package Receivable","0","350000","Invoice Package: 6","2016-07-18","2016-07-18","6","2","6","INV#IP6,Pac:Haj ,Pax:PATHUMMA THURUTHIYIL MEETHAL ,By:Arshak");
INSERT INTO transaction VALUES("56","8","55","6","Package Receivable","350000","0","Invoice Package: 6","2016-07-18","2016-07-18","6","2","6","INV#IP6,Pac:Haj ,Pax:PATHUMMA THURUTHIYIL MEETHAL ,By:Arshak");
INSERT INTO transaction VALUES("57","56","8","7","Package Receivable","0","350000","Invoice Package: 7","2016-07-18","2016-07-18","6","2","7","INV#IP7,Pac:Haj ,Pax:MAYIN KUTTY THONIKKADAVATH ,By:Arshak");
INSERT INTO transaction VALUES("58","8","56","7","Package Receivable","350000","0","Invoice Package: 7","2016-07-18","2016-07-18","6","2","7","INV#IP7,Pac:Haj ,Pax:MAYIN KUTTY THONIKKADAVATH ,By:Arshak");
INSERT INTO transaction VALUES("61","57","8","8","Package Receivable","0","0","Invoice Package: 8","2016-07-18","2016-07-18","6","2","8","INV#IP8,Pac:Haj ,Pax:KADEEJA MADATHIL,FATHIMA KUNJALAVI ,By:Arshak");
INSERT INTO transaction VALUES("62","8","57","8","Package Receivable","0","0","Invoice Package: 8","2016-07-18","2016-07-18","6","2","8","INV#IP8,Pac:Haj ,Pax:KADEEJA MADATHIL,FATHIMA KUNJALAVI ,By:Arshak");
INSERT INTO transaction VALUES("63","58","8","9","Package Receivable","0","700000","Invoice Package: 9","2016-07-18","2016-07-18","6","2","9","INV#IP9,Pac:Haj ,Pax:SUBAIDA VELLARATHODI,KUNJEEVI VELLARATHODI ,By:Arshak");
INSERT INTO transaction VALUES("64","8","58","9","Package Receivable","700000","0","Invoice Package: 9","2016-07-18","2016-07-18","6","2","9","INV#IP9,Pac:Haj ,Pax:SUBAIDA VELLARATHODI,KUNJEEVI VELLARATHODI ,By:Arshak");
INSERT INTO transaction VALUES("65","40","6","7","Visa Receivable","0","14900","Visa Receivable : 7","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV7, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("66","6","40","7","Visa Receivable","14900","0","Visa Receivable : 7","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV7, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("67","43","7","6","Visa Payable","14500","0","","2016-07-18","2016-07-18","5","2","0","INV#6,sup:43,:against:7");
INSERT INTO transaction VALUES("68","7","43","6","Visa Payable","0","14500","","2016-07-18","2016-07-18","5","2","0","INV#6,sup:43,:against:7");
INSERT INTO transaction VALUES("69","40","6","8","Visa Receivable","0","15100","Visa Receivable : 8","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV8, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("70","6","40","8","Visa Receivable","15100","0","Visa Receivable : 8","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV8, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("71","41","7","7","Visa Payable","15000","0","","2016-07-18","2016-07-18","5","2","0","INV#7,sup:41,:against:8");
INSERT INTO transaction VALUES("72","7","41","7","Visa Payable","0","15000","","2016-07-18","2016-07-18","5","2","0","INV#7,sup:41,:against:8");
INSERT INTO transaction VALUES("79","60","6","9","Visa Receivable","0","15100","Visa Receivable : 9","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV9, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("80","6","60","9","Visa Receivable","15100","0","Visa Receivable : 9","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV9, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("81","43","7","8","Visa Payable","14500","0","","2016-07-18","2016-07-18","5","2","0","INV#8,sup:43,:against:9");
INSERT INTO transaction VALUES("82","7","43","8","Visa Payable","0","14500","","2016-07-18","2016-07-18","5","2","0","INV#8,sup:43,:against:9");
INSERT INTO transaction VALUES("83","61","8","11","Package Receivable","0","350000","Invoice Package: 11","2016-07-18","2016-07-18","6","2","11","INV#IP11,Pac:Haj ,Pax:SULAIMAN CHAMAKKALAYI MUHAMMED KUTTY ,By:Arshak");
INSERT INTO transaction VALUES("84","8","61","11","Package Receivable","350000","0","Invoice Package: 11","2016-07-18","2016-07-18","6","2","11","INV#IP11,Pac:Haj ,Pax:SULAIMAN CHAMAKKALAYI MUHAMMED KUTTY ,By:Arshak");
INSERT INTO transaction VALUES("85","45","6","10","Visa Receivable","0","15500","Visa Receivable : 10","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV10, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("86","6","45","10","Visa Receivable","15500","0","Visa Receivable : 10","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV10, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("87","43","7","9","Visa Payable","14500","0","","2016-07-18","2016-07-18","5","2","0","INV#9,sup:43,:against:10");
INSERT INTO transaction VALUES("88","7","43","9","Visa Payable","0","14500","","2016-07-18","2016-07-18","5","2","0","INV#9,sup:43,:against:10");
INSERT INTO transaction VALUES("89","62","8","12","Package Receivable","0","700000","Invoice Package: 12","2016-07-18","2016-07-18","6","2","12","INV#IP12,Pac:Haj ,Pax:ABDUL RASHEED MANNARATTIL,NASREENA ABDUL RASHEED ,By:Arshak");
INSERT INTO transaction VALUES("90","8","62","12","Package Receivable","700000","0","Invoice Package: 12","2016-07-18","2016-07-18","6","2","12","INV#IP12,Pac:Haj ,Pax:ABDUL RASHEED MANNARATTIL,NASREENA ABDUL RASHEED ,By:Arshak");
INSERT INTO transaction VALUES("91","59","8","13","Package Receivable","0","0","Invoice Package: 13","2016-07-18","2016-07-18","6","2","13","INV#IP13,Pac:Haj ,Pax:MUSTHAFA KAYANIKARA ,NILAKKADA VATH HYRUNNEESA CHEMMEKKOTTU ,By:Arshak");
INSERT INTO transaction VALUES("92","8","59","13","Package Receivable","0","0","Invoice Package: 13","2016-07-18","2016-07-18","6","2","13","INV#IP13,Pac:Haj ,Pax:MUSTHAFA KAYANIKARA ,NILAKKADA VATH HYRUNNEESA CHEMMEKKOTTU ,By:Arshak");
INSERT INTO transaction VALUES("93","63","6","11","Visa Receivable","0","15100","Visa Receivable : 11","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV11, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("94","6","63","11","Visa Receivable","15100","0","Visa Receivable : 11","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV11, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("95","43","7","10","Visa Payable","14500","0","","2016-07-18","2016-07-18","5","2","0","INV#10,sup:43,:against:11");
INSERT INTO transaction VALUES("96","7","43","10","Visa Payable","0","14500","","2016-07-18","2016-07-18","5","2","0","INV#10,sup:43,:against:11");
INSERT INTO transaction VALUES("97","40","6","12","Visa Receivable","0","29800","Visa Receivable : 12","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV12, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("98","6","40","12","Visa Receivable","29800","0","Visa Receivable : 12","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV12, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("99","43","7","11","Visa Payable","29000","0","","2016-07-18","2016-07-18","5","2","0","INV#11,sup:43,:against:12");
INSERT INTO transaction VALUES("100","7","43","11","Visa Payable","0","29000","","2016-07-18","2016-07-18","5","2","0","INV#11,sup:43,:against:12");
INSERT INTO transaction VALUES("101","40","6","13","Visa Receivable","0","29800","Visa Receivable : 13","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV13, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("102","6","40","13","Visa Receivable","29800","0","Visa Receivable : 13","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV13, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("103","43","7","12","Visa Payable","29000","0","","2016-07-18","2016-07-18","5","2","0","INV#12,sup:43,:against:13");
INSERT INTO transaction VALUES("104","7","43","12","Visa Payable","0","29000","","2016-07-18","2016-07-18","5","2","0","INV#12,sup:43,:against:13");
INSERT INTO transaction VALUES("105","45","6","14","Visa Receivable","0","15200","Visa Receivable : 14","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV14, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("106","6","45","14","Visa Receivable","15200","0","Visa Receivable : 14","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV14, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("107","43","7","13","Visa Payable","14500","0","","2016-07-18","2016-07-18","5","2","0","INV#13,sup:43,:against:14");
INSERT INTO transaction VALUES("108","7","43","13","Visa Payable","0","14500","","2016-07-18","2016-07-18","5","2","0","INV#13,sup:43,:against:14");
INSERT INTO transaction VALUES("109","40","6","15","Visa Receivable","0","15100","Visa Receivable : 15","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV15, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("110","6","40","15","Visa Receivable","15100","0","Visa Receivable : 15","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV15, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("111","41","7","14","Visa Payable","15000","0","","2016-07-18","2016-07-18","5","2","0","INV#14,sup:41,:against:15");
INSERT INTO transaction VALUES("112","7","41","14","Visa Payable","0","15000","","2016-07-18","2016-07-18","5","2","0","INV#14,sup:41,:against:15");
INSERT INTO transaction VALUES("113","64","6","16","Visa Receivable","0","17400","Visa Receivable : 16","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV16, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("114","6","64","16","Visa Receivable","17400","0","Visa Receivable : 16","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV16, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("115","44","7","15","Visa Payable","17000","0","","2016-07-18","2016-07-18","5","2","0","INV#15,sup:44,:against:16");
INSERT INTO transaction VALUES("116","7","44","15","Visa Payable","0","17000","","2016-07-18","2016-07-18","5","2","0","INV#15,sup:44,:against:16");
INSERT INTO transaction VALUES("117","45","48","2","Bank Receipts","15500","0","","2016-07-19","2016-07-19","5","2","0","INV#BR2,by:Irfad,from:sahad staff");
INSERT INTO transaction VALUES("118","48","45","2","Bank Receipts","0","15500","","2016-07-19","2016-07-19","5","2","0","INV#BR2,by:Irfad,from:sahad staff");
INSERT INTO transaction VALUES("119","47","4","1","Airline Receivable","0","47300","Airline Receivable: 1","2016-07-19","2016-07-19","5","2","0","Airline Receivable(IA), INV#IA1, Pax:Test, Date:27-07-2016, Tkt:8965754664, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("120","4","47","1","Airline Receivable","47300","0","Airline Receivable: 1","2016-07-19","2016-07-19","5","2","0","Airline Receivable(IA), INV#IA1, Pax:Test, Date:27-07-2016, Tkt:8965754664, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("121","44","5","1","Airline Payable","46800","0","","2016-07-19","2016-07-19","5","2","0","INV#1,sup:44,:against:1");
INSERT INTO transaction VALUES("122","5","44","1","Airline Payable","0","46800","","2016-07-19","2016-07-19","5","2","0","INV#1,sup:44,:against:1");

DROP TABLE IF EXISTS transport_provider;
CREATE TABLE `transport_provider` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `providerName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO transport_provider VALUES("1","Al Ojur Company","","","2","6");

DROP TABLE IF EXISTS user_permission;
CREATE TABLE `user_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NOT NULL,
  `basicSetting` int(11) NOT NULL DEFAULT '0',
  `basicSettingEdit` int(11) NOT NULL DEFAULT '0',
  `package` int(11) NOT NULL DEFAULT '0',
  `packageEdit` int(11) NOT NULL DEFAULT '0',
  `account` int(11) NOT NULL DEFAULT '0',
  `accountEdit` int(11) NOT NULL DEFAULT '0',
  `cashBank` int(11) NOT NULL DEFAULT '0',
  `cashBankEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceAirline` int(11) NOT NULL DEFAULT '0',
  `invoiceAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceHotel` int(11) NOT NULL DEFAULT '0',
  `invoiceHotelEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceVisa` int(11) NOT NULL DEFAULT '0',
  `invoiceVisaEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceMisc` int(11) NOT NULL DEFAULT '0',
  `invoiceMiscEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceTransport` int(11) NOT NULL DEFAULT '0',
  `invoiceTransportEdit` int(11) NOT NULL DEFAULT '0',
  `invoicePackage` int(11) NOT NULL DEFAULT '0',
  `invoicePackageEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseAirline` int(11) NOT NULL DEFAULT '0',
  `purchaseAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseHotel` int(11) NOT NULL DEFAULT '0',
  `purchaseHotelEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseVisa` int(11) NOT NULL DEFAULT '0',
  `purchaseVisaEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseMisc` int(11) NOT NULL DEFAULT '0',
  `purchaseMiscEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseTransport` int(11) NOT NULL DEFAULT '0',
  `purchaseTransportEdit` int(11) NOT NULL DEFAULT '0',
  `purchasePackage` int(11) NOT NULL DEFAULT '0',
  `purchasePackageEdit` int(11) NOT NULL DEFAULT '0',
  `accountReport` int(11) NOT NULL DEFAULT '0',
  `otherReport` int(11) NOT NULL DEFAULT '0',
  `user` int(11) NOT NULL DEFAULT '0',
  `userEdit` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `staffId` (`staffId`),
  CONSTRAINT `user_permission_ibfk_1` FOREIGN KEY (`staffId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
INSERT INTO user_permission VALUES("2","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0");
INSERT INTO user_permission VALUES("3","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");

DROP TABLE IF EXISTS vehicle_types;
CREATE TABLE `vehicle_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `vehicleType` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO vehicle_types VALUES("1","Bus","2","6");

DROP TABLE IF EXISTS visa_passenger;
CREATE TABLE `visa_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visaId` int(11) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(20) NOT NULL,
  `passportNo` varchar(50) NOT NULL,
  `visaFor` varchar(50) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `tokenNo` varchar(20) NOT NULL,
  `visaFee` float NOT NULL,
  `vfs` float NOT NULL,
  `ddCharge` float NOT NULL,
  `serviceCharge` float NOT NULL,
  `otherCharge` float NOT NULL,
  `courierCharge` float NOT NULL,
  `subTotal` float NOT NULL,
  `travelDate` date NOT NULL,
  `visaMessage` varchar(20) NOT NULL,
  `msgChngBy` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
INSERT INTO visa_passenger VALUES("1","1","Bismi Ramshad","0000-00-00","F","M2551864","2","2016-07-16","0000-00-00","0000-00-00","","15000","0","0","300","0","0","15300","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("2","2","Shamnas","0000-00-00","M","P0917864","2","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","100","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("3","2","Shanaza","0000-00-00","F","N9972437","2","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","100","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("4","2","Suhail","0000-00-00","M","K5983739","2","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","100","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("5","3","SAKEER LASINA","0000-00-00","M","L4417087","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","200","0","0","15200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("6","4","ANAND","0000-00-00","M","J6643549","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","400","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("7","5","SAFAR","0000-00-00","M","H7376973","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","400","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("8","6","KUMARAN","0000-00-00","M","K0060995","4","0000-00-00","0000-00-00","0000-00-00","","5150","0","0","100","0","0","5250","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("9","7","DON","0000-00-00","M","K7228415","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","400","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("10","8","AYISHA","0000-00-00","M","M3617677","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("12","10","","0000-00-00","","","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","600","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("13","11","ANOOP MOONAKAPARMBIL","0000-00-00","M","F7229447","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","1000","0","0","15500","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("14","12","DURAI PANDYAN","0000-00-00","M","N2573742","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","600","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("15","13","JESINTHA & SAMEER THANALOTT","0000-00-00","M","K4277103 & H0862662","2","0000-00-00","0000-00-00","0000-00-00","","29000","0","0","800","0","0","29800","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("16","14","ANIL KUMAR & LATHA NATHAN","0000-00-00","M","K7816303  & Z3140980","2","0000-00-00","0000-00-00","0000-00-00","","29000","0","0","800","0","0","29800","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("17","15","NAVAS KOLAVAYAL","0000-00-00","M","J1054938","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","700","0","0","15200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("18","16","LATHA MATTAN","0000-00-00","M","Z3140980","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("19","17","JISHNU MANIYANTHARA","0000-00-00","M","M6709343","2","0000-00-00","0000-00-00","0000-00-00","","17000","0","0","400","0","0","17400","0000-00-00","","0","","2","5");

DROP TABLE IF EXISTS visafor;
CREATE TABLE `visafor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visafor` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
INSERT INTO visafor VALUES("1","90 DAYS DXB  WITH OTB","2","5");
INSERT INTO visafor VALUES("2","90 DAYS DXB  WITHOUT OTB","2","5");
INSERT INTO visafor VALUES("3","30 DAYS DXB WITH OTB","2","5");
INSERT INTO visafor VALUES("4","30 DAYS DXB WITHOUT OTB","2","5");
INSERT INTO visafor VALUES("5","30 DAYS QATAR BUSINESS","2","5");
INSERT INTO visafor VALUES("6","30 DAYS QATAR TOURIST","2","5");
INSERT INTO visafor VALUES("7","90 DAYS QATAR","2","5");
INSERT INTO visafor VALUES("8","90 DAYS UAE MULTIPLE","2","5");
INSERT INTO visafor VALUES("9","30 DAYS UAE MULTIPLE","2","5");
INSERT INTO visafor VALUES("10","30 DAYS UAE  LESURE","2","5");
INSERT INTO visafor VALUES("11","90 DAYS UAE LESURE","2","5");



								/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
								/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
								/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
								/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
								/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
								/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
								/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
								
						