/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
								/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
								/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
								/*!40101 SET NAMES utf8 */;
								/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
								/*!40103 SET TIME_ZONE='+00:00' */;
								/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
								/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
								/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
								/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
								DROP TABLE IF EXISTS account_group;
CREATE TABLE `account_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `acGroup` text NOT NULL,
  `parent` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
INSERT INTO account_group VALUES("1","Account","0","0","0");
INSERT INTO account_group VALUES("2","Balance Sheet","1","0","0");
INSERT INTO account_group VALUES("3","Profit & Loss Account","1","0","0");
INSERT INTO account_group VALUES("4","Assets","2","0","0");
INSERT INTO account_group VALUES("5","Liabilities","2","0","0");
INSERT INTO account_group VALUES("6","Current Assets","4","0","0");
INSERT INTO account_group VALUES("7","Fixed Assets","4","0","0");
INSERT INTO account_group VALUES("8","Cash In Hand","6","0","0");
INSERT INTO account_group VALUES("9","Deposits","6","0","0");
INSERT INTO account_group VALUES("10","Investments","6","0","0");
INSERT INTO account_group VALUES("11","Loans & Advances (asset)","6","0","0");
INSERT INTO account_group VALUES("12","Bank A/c","6","0","0");
INSERT INTO account_group VALUES("13","Expenses","3","0","0");
INSERT INTO account_group VALUES("14","Income","3","0","0");
INSERT INTO account_group VALUES("15","Cash","8","0","0");
INSERT INTO account_group VALUES("16","Capital","5","0","0");
INSERT INTO account_group VALUES("17","Current Liabilities","5","0","0");
INSERT INTO account_group VALUES("18","Loans (liability)","5","0","0");
INSERT INTO account_group VALUES("19","Suspense Accounts","5","0","0");
INSERT INTO account_group VALUES("20","Bank OD A/C","17","0","0");
INSERT INTO account_group VALUES("21","Duties & Taxes","17","0","0");
INSERT INTO account_group VALUES("22","Journal Accounts","17","0","0");
INSERT INTO account_group VALUES("23","Provisions","17","0","0");
INSERT INTO account_group VALUES("24","Direct Expense","13","0","0");
INSERT INTO account_group VALUES("25","Indirect Expense","13","0","0");
INSERT INTO account_group VALUES("26","Direct Income","14","0","0");
INSERT INTO account_group VALUES("27","Indirect Income","14","0","0");
INSERT INTO account_group VALUES("28","Fittings","7","0","0");
INSERT INTO account_group VALUES("29","Advertisement","25","0","0");
INSERT INTO account_group VALUES("30","Donation Given","25","0","0");
INSERT INTO account_group VALUES("31","Electricity Bills","25","0","0");
INSERT INTO account_group VALUES("32","Miscellaneous Expense","25","0","0");
INSERT INTO account_group VALUES("33","Office Expense","25","0","0");
INSERT INTO account_group VALUES("34","Other Expenses","25","0","0");
INSERT INTO account_group VALUES("35","Telephone Bills","25","0","0");
INSERT INTO account_group VALUES("36","Other Income","27","0","0");
INSERT INTO account_group VALUES("37","Customer/ Sundry Debtors","6","0","0");
INSERT INTO account_group VALUES("38","Shipping Agents","17","0","0");
INSERT INTO account_group VALUES("39","Staff Deposits","17","0","0");
INSERT INTO account_group VALUES("40","Supplier/ Sundry Creditors","17","0","0");
INSERT INTO account_group VALUES("41","Discount Given ","24","0","0");
INSERT INTO account_group VALUES("42","Job Works","24","0","0");
INSERT INTO account_group VALUES("43","Purchases","24","0","0");
INSERT INTO account_group VALUES("44","Courier","26","0","0");
INSERT INTO account_group VALUES("45","Discount Recieved","26","0","0");
INSERT INTO account_group VALUES("46","Sales","26","0","0");
INSERT INTO account_group VALUES("47","Services","26","0","0");
INSERT INTO account_group VALUES("48","Commission Agent","25","0","0");
INSERT INTO account_group VALUES("49","Incentives Given","25","0","0");
INSERT INTO account_group VALUES("50","Printing Expense","25","0","0");
INSERT INTO account_group VALUES("51","Rent Given","25","0","0");
INSERT INTO account_group VALUES("52","Salaries","25","0","0");
INSERT INTO account_group VALUES("53","Sales Expenses","25","0","0");
INSERT INTO account_group VALUES("54","Tax Paid","25","0","0");
INSERT INTO account_group VALUES("55","Tax Recieved","25","0","0");
INSERT INTO account_group VALUES("56","Incentives Recieved","27","0","0");
INSERT INTO account_group VALUES("57","Branches","6","0","0");
INSERT INTO account_group VALUES("58","Main Branches","17","0","0");

DROP TABLE IF EXISTS accounts;
CREATE TABLE `accounts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `agentId` int(11) NOT NULL,
  `accountGroupId` int(11) NOT NULL,
  `accountName` varchar(200) NOT NULL,
  `openingBal` double NOT NULL,
  `payType` varchar(20) NOT NULL,
  `masterLedger` int(11) NOT NULL,
  `addDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `description` text NOT NULL,
  `contactPerson` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address2` text NOT NULL,
  `pin2` varchar(20) NOT NULL,
  `city2` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `accountGroupId` (`accountGroupId`),
  CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`accountGroupId`) REFERENCES `account_group` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
INSERT INTO accounts VALUES("1","0","19","Suspense","0","Credit","1","2016-03-22","1","0","Suspense Account","","","","","","","","","","");
INSERT INTO accounts VALUES("2","0","8","Cash Account","0","Credit","1","2016-03-22","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("3","0","24","Salary Payments","0","Credit","1","2016-03-22","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("4","0","14","Airline Receivable","0","Debit","1","2016-04-05","1","0","Airline charge received","","","","","","","","","","");
INSERT INTO accounts VALUES("5","0","13","Airline Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("6","0","14","Visa Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("7","0","13","Visa Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("8","0","14","Package Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("9","0","13","Package Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("10","0","14","Hotel Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("11","0","13","Hotel Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("12","0","14","Transport Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("13","0","13","Transport Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("14","0","14","Misc Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("15","0","13","Misc Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("16","0","25","Discount Given","0","Credit","1","2016-05-25","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("17","0","27","Discount Received","0","Credit","1","2016-05-25","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("18","0","25","Tax","0","Credit","1","2016-06-24","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("19","0","25","Tds","0","Credit","1","2016-06-24","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("20","0","27","Tac","0","Credit","1","2016-06-24","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("21","0","25","Package Extra Chrg","0","Credit","1","2016-06-29","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("90","0","37","CUSTOMER","1000","Credit","0","2016-07-02","34","11","CUSTOMER","CUSTOMER","CUSTOMER","123456","CUSTOMER","04832654816","9746111621","CUSTOMER@gmail.com","CUSTOMER","123456","CUSTOMER");
INSERT INTO accounts VALUES("91","0","37","cust1","100","Debit","0","2016-07-02","36","12","","cust","","45","jj","455","899","hhjg@fg.mm","","","");
INSERT INTO accounts VALUES("92","0","37","cust2","0","Credit","0","2016-07-02","36","12","","","","","","","789","","","","");
INSERT INTO accounts VALUES("93","0","37","cust3","0","Credit","0","2016-07-02","36","12","","","","","","","788","","","","");
INSERT INTO accounts VALUES("94","0","48","Agent","10","Credit","0","2016-07-02","34","11","231","Agent","Agent","123456","Agent","123456","987456","Agent@gmail.com","Agent","52345234","4346Agent");
INSERT INTO accounts VALUES("95","94","40","Supplier","1000","Credit","0","2016-07-02","34","11","Supplier","Supplier","Supplier","634573573","Supplier","56883422312","45634573573","supplier@gmail.com","Supplier","16498526989","Supplier");
INSERT INTO accounts VALUES("96","0","12","SBT rmz trv","0","Credit","0","2016-07-02","36","12","","","","","","","","","","","");
INSERT INTO accounts VALUES("97","0","40","akbar airline","0","Credit","0","2016-07-02","36","12","","","","","","","455","","","","");
INSERT INTO accounts VALUES("98","0","40","indian airline","0","Credit","0","2016-07-02","36","12","","","","","","","122","","","","");
INSERT INTO accounts VALUES("99","0","40","raviz","0","Credit","0","2016-07-02","36","12","","","","","","","22","","","","");
INSERT INTO accounts VALUES("100","0","40","taj","0","Credit","0","2016-07-02","36","12","","","","","","","22","","","","");
INSERT INTO accounts VALUES("101","0","40","kallada","0","Credit","0","2016-07-02","36","12","","","","","","","555","","","","");
INSERT INTO accounts VALUES("102","0","40","galaxy","0","Credit","0","2016-07-02","36","12","","","","","","","ee","","","","");
INSERT INTO accounts VALUES("103","0","40","uduppi","0","Credit","0","2016-07-02","36","12","","","","","","","33","","","","");
INSERT INTO accounts VALUES("104","0","48","agent","0","Credit","0","2016-07-02","36","12","","","","","","","","","","","");
INSERT INTO accounts VALUES("105","0","48","agent2","0","Credit","0","2016-07-02","36","12","","","","","","","","","","","");
INSERT INTO accounts VALUES("106","94","12","banking","100000","Credit","0","2016-07-02","34","11","","banking","banking","874954","banking","98498654598","89458789949","banking@gmail.com","banking","432465462","banking");

DROP TABLE IF EXISTS airline_flight;
CREATE TABLE `airline_flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlinePassId` int(11) NOT NULL,
  `airlineName` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `journeyDate` date NOT NULL,
  `sectorFrom` varchar(100) NOT NULL,
  `sectorTo` varchar(100) NOT NULL,
  `classType` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `tax` varchar(30) NOT NULL,
  `yq` float NOT NULL,
  `procCharge` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlinePassId` (`airlinePassId`),
  CONSTRAINT `airline_flight_ibfk_2` FOREIGN KEY (`airlinePassId`) REFERENCES `airline_passenger` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
INSERT INTO airline_flight VALUES("1","34","22","ai001","9845161453","2016-07-14","Calicut","Behrain","1st","23821","3456","100","50","11","34");
INSERT INTO airline_flight VALUES("2","35","23","","","0000-00-00","","","","100","2","0","5","12","36");
INSERT INTO airline_flight VALUES("3","36","23","qw233","we33","2016-07-06","","","","235","3","2","9","12","36");
INSERT INTO airline_flight VALUES("4","37","25","sd22","122","0000-00-00","","","","12","","0","0","12","36");
INSERT INTO airline_flight VALUES("5","38","24","","","0000-00-00","","","","10","","0","0","12","36");
INSERT INTO airline_flight VALUES("6","39","23","","","0000-00-00","","","","12","","0","0","12","36");
INSERT INTO airline_flight VALUES("7","40","23","","","0000-00-00","","","","12","","0","0","12","36");
INSERT INTO airline_flight VALUES("8","41","23","34","4","2016-07-14","45","gf","dfg","34","56","56","65","12","36");
INSERT INTO airline_flight VALUES("9","42","","","","0000-00-00","","","","0","","0","0","12","36");
INSERT INTO airline_flight VALUES("10","43","","","","0000-00-00","","","","0","","0","0","12","36");
INSERT INTO airline_flight VALUES("13","46","","","","0000-00-00","","","","0","","0","0","12","36");
INSERT INTO airline_flight VALUES("15","48","23","2","2","0000-00-00","dfg","dfg","df","0","23","23","32","12","36");
INSERT INTO airline_flight VALUES("16","49","23","3","","2016-07-05","jhj","jh","dsf","1","2","3","4","12","36");

DROP TABLE IF EXISTS airline_passenger;
CREATE TABLE `airline_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlineId` int(11) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationship` varchar(30) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlineId` (`airlineId`),
  CONSTRAINT `airline_passenger_ibfk_1` FOREIGN KEY (`airlineId`) REFERENCES `invoice_airline` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
INSERT INTO airline_passenger VALUES("34","41","Passenger","1993-02-20","23","M","Passenger","Passenger","Indian","Passenger","123456","2010-07-14","2021-07-20","Passenger","11","34");
INSERT INTO airline_passenger VALUES("35","42","Passn3","2006-07-18","10","M","wert","","Indian","","we123","2011-07-13","2021-07-22","gfrt","12","36");
INSERT INTO airline_passenger VALUES("36","43","Passngertst","2006-07-05","10","F","qwe","","canada","","qw32","0000-00-00","2016-07-22","qwe","12","36");
INSERT INTO airline_passenger VALUES("37","44","Pas","2015-01-07","1","F","","","","","","0000-00-00","0000-00-00","","12","36");
INSERT INTO airline_passenger VALUES("38","45","Passn3","2006-07-18","10","M","wert","","Indian","","we123","2011-07-13","2021-07-22","gfrt","12","36");
INSERT INTO airline_passenger VALUES("39","46","Pasn1","1999-07-23","17","M","asd","","Indian","","as123","1999-07-11","2016-07-21","qwe","12","36");
INSERT INTO airline_passenger VALUES("40","47","Pasn1","1999-07-23","17","M","asd","","Indian","","as123","1999-07-11","2016-07-21","qwe","12","36");
INSERT INTO airline_passenger VALUES("41","48","Pass","2016-07-01","0","F","sd","","sd","","3","0000-00-00","2016-07-13","45","12","36");
INSERT INTO airline_passenger VALUES("42","49","Dfg","2016-07-14","0","","","","","","","0000-00-00","0000-00-00","","12","36");
INSERT INTO airline_passenger VALUES("43","50","Sd","0000-00-00","0","M","","","","","","0000-00-00","0000-00-00","","12","36");
INSERT INTO airline_passenger VALUES("46","53","Edssd","0000-00-00","0","","","","","","","0000-00-00","0000-00-00","","12","36");
INSERT INTO airline_passenger VALUES("48","55","Asd","0000-00-00","0","F","ewr","","wer","","1","0000-00-00","0000-00-00","dfgdfg","12","36");
INSERT INTO airline_passenger VALUES("49","56","Pasn1","1986-07-23","30","M","asd","","Indian","","as123","1999-07-11","2016-07-21","qwe","12","36");

DROP TABLE IF EXISTS cash_payment;
CREATE TABLE `cash_payment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
INSERT INTO cash_payment VALUES("8","1","airline","1","10000","","0","CASH","11","34");
INSERT INTO cash_payment VALUES("9","2","airline","1","17592","","0","CASH","11","34");
INSERT INTO cash_payment VALUES("10","1","hotel","1","17272","","0","BANK","11","34");
INSERT INTO cash_payment VALUES("11","1","airline","12","318","","0","CASH","12","36");
INSERT INTO cash_payment VALUES("12","1","visa","1","23","","0","BANK","12","36");

DROP TABLE IF EXISTS cash_receipt;
CREATE TABLE `cash_receipt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
INSERT INTO cash_receipt VALUES("26","2","hotel","1","6830","","0","CASH","11","34");
INSERT INTO cash_receipt VALUES("27","3","hotel","1","10000","","0","CASH","11","34");
INSERT INTO cash_receipt VALUES("28","1","airline","1","29342","","0","BANK","11","34");
INSERT INTO cash_receipt VALUES("29","2","airline","1","0","","0","BANK","11","34");
INSERT INTO cash_receipt VALUES("30","1","airline","2","120","","0","CASH","12","36");
INSERT INTO cash_receipt VALUES("31","1","airline","4","10","","0","CASH","12","36");
INSERT INTO cash_receipt VALUES("32","1","miscellaneous","1","14","","0","CASH","12","36");
INSERT INTO cash_receipt VALUES("33","1","transportation","2","20","","0","CASH","12","36");
INSERT INTO cash_receipt VALUES("34","2","transportation","1","14","","0","CASH","12","36");
INSERT INTO cash_receipt VALUES("35","1","airline","2","152","","0","BANK","12","36");
INSERT INTO cash_receipt VALUES("36","1","airline","3","12","","0","BANK","12","36");

DROP TABLE IF EXISTS company;
CREATE TABLE `company` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `tds` int(11) NOT NULL,
  `circle` varchar(100) NOT NULL,
  `resPerson` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `remark` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
INSERT INTO company VALUES("0","Markaz Travels","Jafarkhan colony ,\nMavoor Road,\nCalicut ","","Calicut","0495 2565656","9968574710","","","0","","","","");
INSERT INTO company VALUES("11","Binfasc","binfasc","123456","malappuram","04832654816","12356789","binfasc@gmail.com","www.noo.com","323514","calicut","Modi","10","binfasc");
INSERT INTO company VALUES("12","Rameez","nk complex \nnadapuram calicut","673506","nadapuram","04962560440","9995646254","muhammed.rameez@gmail.com","www.rameeztravels.com","0","nadapuram","rameez","md","good at service");
INSERT INTO company VALUES("13","Test","","","","","44","","","0","","","","");

DROP TABLE IF EXISTS flight;
CREATE TABLE `flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `flightNo` varchar(50) NOT NULL,
  `flightName` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
INSERT INTO flight VALUES("22","0","Flight","../../uploads/flight/20160702123648.jpg","11","34");
INSERT INTO flight VALUES("23","0","Qater Airways","","12","36");
INSERT INTO flight VALUES("24","0","Itihad Airways","","12","36");
INSERT INTO flight VALUES("25","0","Indian Airlines","","12","36");

DROP TABLE IF EXISTS hotel;
CREATE TABLE `hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(10) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
INSERT INTO hotel VALUES("17","Uduppi","","","","","","12","","","","12","36");
INSERT INTO hotel VALUES("16","Taj","","","","","","","","","","12","36");
INSERT INTO hotel VALUES("15","Kadavu","","","","","","1233","","","","12","36");
INSERT INTO hotel VALUES("14","Burj Dubai","Downtown Dubai,\n\nEmaar Square,\n\nBuilding 3, Ground level\n\nLocation Map\n\nTel +800-Emaar (800-36227)\n\nFax + 971 4 367 5566","944000","Sheikh Mohammed Bin Rashid Blvd","Dubai","United Arab Emirates","800 28843867","+9714 8888888","info@aththetop.ae","www.burjkhalifa.ae/","11","34");
INSERT INTO hotel VALUES("13","Thaj","fdgsdf","123456","Calicut","Kerala","India","32534625","456356345","thaj@gmail.com","thaj.com","11","34");
INSERT INTO hotel VALUES("18","Indaia Gate","","","","","","","","","","12","36");
INSERT INTO hotel VALUES("19","The Gateway","","","","","","sss","","","","12","36");

DROP TABLE IF EXISTS hotel_customer;
CREATE TABLE `hotel_customer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelInvoiceId` int(11) NOT NULL,
  `roomType` varchar(20) NOT NULL,
  `noOfRooms` int(11) NOT NULL,
  `noOfPersons` int(11) NOT NULL,
  `rate` float NOT NULL,
  `subTax` varchar(30) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
INSERT INTO hotel_customer VALUES("22","15","37","1","3","8400","15","16830","11","34");
INSERT INTO hotel_customer VALUES("23","16","40","1","2","25","2","27","12","36");
INSERT INTO hotel_customer VALUES("24","17","40","5","32","25","5","1200","12","36");
INSERT INTO hotel_customer VALUES("25","18","40","1","2","12","1","208","12","36");
INSERT INTO hotel_customer VALUES("26","19","5","1","1","5","4","144","12","36");
INSERT INTO hotel_customer VALUES("27","20","2","2","3","12","2","252","12","36");
INSERT INTO hotel_customer VALUES("28","21","","0","0","0","","0","12","36");
INSERT INTO hotel_customer VALUES("29","22","40","1","1","1","1","46","12","36");

DROP TABLE IF EXISTS invoice_airline;
CREATE TABLE `invoice_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float NOT NULL,
  `mainProcCharge` float NOT NULL,
  `mainOtherCharge` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `mainTax` varchar(30) NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
INSERT INTO invoice_airline VALUES("41","1","2016-07-02","90","2000","50","200","","","15","350","29342","Pending","11","34");
INSERT INTO invoice_airline VALUES("42","1","2016-07-02","91","4","7","0","","","50","0","168","Pending","12","36");
INSERT INTO invoice_airline VALUES("43","2","2016-07-02","91","7","4","4","","","8","0","272","Pending","12","36");
INSERT INTO invoice_airline VALUES("44","3","2016-07-02","91","0","0","0","","","0","0","12","Pending","12","36");
INSERT INTO invoice_airline VALUES("45","4","2016-07-02","91","0","0","0","","","0","0","10","Pending","12","36");
INSERT INTO invoice_airline VALUES("46","5","2016-07-02","91","0","0","0","","","0","0","12","Pending","12","36");
INSERT INTO invoice_airline VALUES("47","6","2016-07-02","91","0","0","0","","","0","0","12","Pending","12","36");
INSERT INTO invoice_airline VALUES("48","7","2016-07-04","91","23","45","56","","","56","0","391","Pending","12","36");
INSERT INTO invoice_airline VALUES("49","8","2016-07-05","91","1","1","0","","","0","0","2","Pending","12","36");
INSERT INTO invoice_airline VALUES("50","9","2016-07-05","91","1","2","0","","","1","0","4","Pending","12","36");
INSERT INTO invoice_airline VALUES("53","11","2016-07-05","91","1","23","0","","","1","3","22","Pending","12","36");
INSERT INTO invoice_airline VALUES("55","10","2016-07-05","91","1","0","0","","","10","2","87","Pending","12","36");
INSERT INTO invoice_airline VALUES("56","12","2016-07-05","91","1","0","1","","","1","1","12","Pending","12","36");

DROP TABLE IF EXISTS invoice_hotel;
CREATE TABLE `invoice_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `hotelId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `noOfNight` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `checkInDate` date NOT NULL,
  `checkInTime` varchar(20) NOT NULL,
  `checkOutDate` date NOT NULL,
  `checkOutTime` varchar(20) NOT NULL,
  `billingIns` varchar(100) NOT NULL,
  `reference` varchar(100) NOT NULL,
  `tax` varchar(30) NOT NULL,
  `tds` float NOT NULL,
  `otherCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
INSERT INTO invoice_hotel VALUES("15","1","2016-07-02","13","90","2","2","1","3","2016-07-12","12","2016-07-14","12","none","","0","0","0","0","16830","Pending","11","34");
INSERT INTO invoice_hotel VALUES("16","1","2016-07-02","17","92","1","2","2","4","2016-07-02","","2016-07-03","","none","","1","5","3","1","35","Pending","12","36");
INSERT INTO invoice_hotel VALUES("17","2","2016-07-02","16","93","8","23","9","32","2016-07-02","","2016-07-10","","none","","4","7","44","24","1231","Pending","12","36");
INSERT INTO invoice_hotel VALUES("18","3","2016-07-02","16","91","16","5","0","5","2016-07-02","","2016-07-18","","none","","1","2","0","1","210","Pending","12","36");
INSERT INTO invoice_hotel VALUES("19","4","2016-07-02","17","93","16","1","0","1","2016-07-02","","2016-07-18","","none","","0","0","0","0","144","Pending","12","36");
INSERT INTO invoice_hotel VALUES("20","5","2016-07-04","17","91","9","1","2","3","2016-07-04","2.00pm","2016-07-13","","none","","0","0","0","0","252","Pending","12","36");
INSERT INTO invoice_hotel VALUES("21","6","2016-07-04","17","92","7","1","1","2","2016-07-04","","2016-07-11","","none","","0","0","2","0","2","Pending","12","36");
INSERT INTO invoice_hotel VALUES("22","7","2016-07-04","17","91","23","1","1","2","2016-07-04","","2016-07-27","","none","","021","2","12","12","69","Pending","12","36");

DROP TABLE IF EXISTS invoice_misc;
CREATE TABLE `invoice_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `basicAmount` float NOT NULL,
  `tds` float NOT NULL,
  `tax` varchar(20) NOT NULL,
  `otherCharge` float NOT NULL,
  `procCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `narration` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
INSERT INTO invoice_misc VALUES("14","1","90","2016-07-02","2016-07-02","Passenger","","2000","300","15%","200","100","22","2968","Pending","0","11","34");
INSERT INTO invoice_misc VALUES("15","1","91","2016-07-02","2016-07-02","passgr","","10","1","1","1","2","1","14","Pending","12","12","36");
INSERT INTO invoice_misc VALUES("16","2","92","2016-07-02","2016-07-02","pass","","10","0","0","0","0","0","10","Pending","13","12","36");
INSERT INTO invoice_misc VALUES("17","3","92","2016-07-02","2016-07-02","pass","","10","0","0","0","0","0","10","Pending","13","12","36");
INSERT INTO invoice_misc VALUES("18","4","91","2016-07-02","2016-07-02","Passn3","","45","0","0","0","0","0","45","Pending","0","12","36");
INSERT INTO invoice_misc VALUES("19","5","91","2016-07-04","2016-07-04","sdf","rret","234","4","54","5","4","5","296","Pending","12","12","36");
INSERT INTO invoice_misc VALUES("20","6","91","2016-07-05","2016-07-05","Pasn1","ert","343","0","34","34","34","0","445","Pending","13","12","36");

DROP TABLE IF EXISTS invoice_package;
CREATE TABLE `invoice_package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `packageId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `proCharge` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `discount` float DEFAULT '0',
  `netAmount` float NOT NULL DEFAULT '0',
  `description` varchar(200) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package VALUES("12","2","2016-07-05","12","91","0","0","0","0","10","62","","12","36");
INSERT INTO invoice_package VALUES("9","1","2016-07-02","10","91","34","4","4","5","45","73","","12","36");

DROP TABLE IF EXISTS invoice_package_airline;
CREATE TABLE `invoice_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_hotel;
CREATE TABLE `invoice_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `roomType` varchar(100) NOT NULL,
  `bedStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_passenger;
CREATE TABLE `invoice_package_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `packageId` int(11) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` date DEFAULT NULL,
  `age` int(11) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `birthPlace` varchar(100) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(50) NOT NULL,
  `dateOfIssue` date DEFAULT NULL,
  `validUpTo` date DEFAULT NULL,
  `issuePlace` varchar(100) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_passenger VALUES("30","12","12","Pasn1","M","1986-07-23","30","Indian","asd","","","as123","1999-07-11","2016-07-21","qwe","72","12","36");
INSERT INTO invoice_package_passenger VALUES("27","9","10","pass","F","2016-07-07","0","","","","","","","2016-07-13","","71","12","36");

DROP TABLE IF EXISTS invoice_package_transportation;
CREATE TABLE `invoice_package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `seatStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_transportation VALUES("25","9","27","0","0","12","36");

DROP TABLE IF EXISTS invoice_package_visa;
CREATE TABLE `invoice_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_visa VALUES("25","12","30","0000-00-00","0000-00-00","0000-00-00","43","12","36");

DROP TABLE IF EXISTS invoice_transport;
CREATE TABLE `invoice_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `serviceProviderId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `totalDays` int(11) NOT NULL,
  `vehicleType` varchar(100) NOT NULL,
  `kmAllowed` float NOT NULL,
  `arrivalDetails` text NOT NULL,
  `pickUpFrom` varchar(100) NOT NULL,
  `pickUpTime` varchar(20) NOT NULL,
  `DepartureDetails` text NOT NULL,
  `dropAt` varchar(100) NOT NULL,
  `dropTime` varchar(20) NOT NULL,
  `route` text NOT NULL,
  `inclusive` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
INSERT INTO invoice_transport VALUES("8","1","2016-07-02","12","90","2","2","4","2016-07-13","2016-07-28","15","22","0","ooty","","","calicut","","","vayanadu","food","2000","200","20","2220","","Pending","11","34");
INSERT INTO invoice_transport VALUES("9","1","2016-07-02","14","91","5","0","5","2016-07-02","2016-07-10","8","29","0","","","","","","","","","10","2","2","14","","Pending","12","36");
INSERT INTO invoice_transport VALUES("10","2","2016-07-02","15","91","2","0","2","2016-07-03","2016-07-03","1","29","0","","","","","","","","","20","0","0","20","","Pending","12","36");
INSERT INTO invoice_transport VALUES("11","3","2016-07-04","14","91","1","2","3","2016-07-21","2016-07-31","10","27","0","","","","","","","","","0","234","23","257","","Pending","12","36");

DROP TABLE IF EXISTS invoice_visa;
CREATE TABLE `invoice_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `mainOtherCharge` double NOT NULL,
  `discount` varchar(50) NOT NULL,
  `netAmount` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
INSERT INTO invoice_visa VALUES("34","2","2016-07-04","92","0","34","101","","","Pending","12","36");
INSERT INTO invoice_visa VALUES("35","3","2016-07-04","91","3","4","14","","","Pending","12","36");
INSERT INTO invoice_visa VALUES("38","4","2016-07-04","93","8","4","4","","","Pending","12","36");
INSERT INTO invoice_visa VALUES("13","1","2016-07-02","90","0","0","8445","","","Pending","11","34");
INSERT INTO invoice_visa VALUES("33","1","2016-07-04","91","0","0","87","","","Pending","12","36");

DROP TABLE IF EXISTS login;
CREATE TABLE `login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
INSERT INTO login VALUES("1","0","admin","81dc9bdb52d04dc20036dbd8313ed055","Admin");
INSERT INTO login VALUES("33","11","binfasc","202cb962ac59075b964b07152d234b70","Branch");
INSERT INTO login VALUES("34","22","binfas1","202cb962ac59075b964b07152d234b70","Staff");
INSERT INTO login VALUES("35","12","rameez","c84a267115caac9f2fc20c307e0ee438","Branch");
INSERT INTO login VALUES("36","23","staff1","4d7d719ac0cf3d78ea8a94701913fe47","Staff");
INSERT INTO login VALUES("37","24","staff2","8bc01711b8163ec3f2aa0688d12cdf3b","Staff");
INSERT INTO login VALUES("38","13","test","098f6bcd4621d373cade4e832627b4f6","Branch");
INSERT INTO login VALUES("39","25","staff","1253208465b1efa876f982d8a9e73eef","Staff");

DROP TABLE IF EXISTS misc_narration;
CREATE TABLE `misc_narration` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `narration` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
INSERT INTO misc_narration VALUES("12","Dance","12","36");
INSERT INTO misc_narration VALUES("13","Surprc Party","12","36");

DROP TABLE IF EXISTS package;
CREATE TABLE `package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageName` varchar(200) NOT NULL,
  `staff` int(11) NOT NULL,
  `leader` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `place` varchar(100) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `tds` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `remark` varchar(100) NOT NULL,
  `services` varchar(100) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `confirm` int(11) NOT NULL,
  `discount` float NOT NULL,
  `extra` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
INSERT INTO package VALUES("12","Aaa","23","A","2334354","2016-07-20","dfg","2","23","3","3","dfg","visa","72","72","72","12","36","0","0","0");
INSERT INTO package VALUES("11","New Pack","23","Leader1","6756788989","2016-07-05","new","3","89","87","56","new","visa","2702","2702","2702","12","35","1","0","0");
INSERT INTO package VALUES("10","New2","23","L","7878787","2016-07-03","lk","7","34","3","34","87","transportation","116","116","71","12","36","0","0","0");
INSERT INTO package VALUES("9","New","23","L","7888","2016-07-03","l","34","34","67","67","l","hotel","168","168","168","12","36","0","0","0");

DROP TABLE IF EXISTS package_airline;
CREATE TABLE `package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `yq` float NOT NULL DEFAULT '0',
  `procCharge` float NOT NULL DEFAULT '0',
  `tax` float NOT NULL DEFAULT '0',
  `adultCharge` float NOT NULL,
  `childCharge` float NOT NULL,
  `infantCharge` float NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `packageId` (`packageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package_airline_flights;
CREATE TABLE `package_airline_flights` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_airlineId` int(11) NOT NULL,
  `airline` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `sectorFrom` varchar(200) NOT NULL,
  `sectorTo` varchar(200) NOT NULL,
  `jDate` date NOT NULL,
  `class` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package_hotel;
CREATE TABLE `package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `hotelId` int(11) NOT NULL,
  `checkinDate` date DEFAULT NULL,
  `checkoutDate` date DEFAULT NULL,
  `noOfDays` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
INSERT INTO package_hotel VALUES("9","9","16","2016-07-14","2016-07-27","13","12","36","0");

DROP TABLE IF EXISTS package_room_type;
CREATE TABLE `package_room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_hotelId` int(11) NOT NULL,
  `roomId` int(11) NOT NULL,
  `rate` float NOT NULL,
  `otherCharge` float NOT NULL,
  `total` float NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package_transportation;
CREATE TABLE `package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `vehicle` int(11) NOT NULL,
  `seatCharge` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
INSERT INTO package_transportation VALUES("8","10","28","45","fgh","98","5465","12","36");

DROP TABLE IF EXISTS package_visa;
CREATE TABLE `package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `visaFor` varchar(100) NOT NULL,
  `visaFee` float NOT NULL DEFAULT '0',
  `vfs` float NOT NULL DEFAULT '0',
  `ddCharge` float NOT NULL DEFAULT '0',
  `serviceCharge` float NOT NULL DEFAULT '0',
  `otherCharge` float NOT NULL DEFAULT '0',
  `courierCharge` float NOT NULL DEFAULT '0',
  `total` float NOT NULL DEFAULT '0',
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
INSERT INTO package_visa VALUES("7","12","27","2","0","34","2","0","2","43","98","23","12","36");
INSERT INTO package_visa VALUES("6","11","25","2300","34","12","45","0","34","2470","99","67","12","35");

DROP TABLE IF EXISTS passenger;
CREATE TABLE `passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationship` varchar(30) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `mofa` varchar(50) NOT NULL,
  `emofa` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
INSERT INTO passenger VALUES("33","90","Passenger","1993-02-20","23","M","Passenger","Passenger","Indian","Passenger","123456","2010-07-14","2021-07-20","Passenger","Passenger","Passenger","11","34");
INSERT INTO passenger VALUES("34","91","Pasn1","1986-07-23","30","M","asd","","Indian","","as123","1999-07-11","2016-07-21","qwe","qww","qwert","12","36");
INSERT INTO passenger VALUES("35","91","Panng2","1999-07-13","17","F","ert","","Indian","","","0000-00-00","0000-00-00","","","","12","36");
INSERT INTO passenger VALUES("36","91","Passn3","2006-07-18","10","M","wert","","Indian","","we123","2011-07-13","2021-07-22","gfrt","","","12","36");
INSERT INTO passenger VALUES("37","92","Passng4","2006-07-12","10","F","dff","","Indian","","err","2016-07-01","2016-07-26","","","","12","36");
INSERT INTO passenger VALUES("38","92","Panng5","2009-07-02","7","F","err","ss","american","","qw23","2009-07-09","2016-07-27","d","","","12","36");
INSERT INTO passenger VALUES("39","93","Passng6","2016-06-03","0","F","qw","w","Indian","","er","2016-07-01","2016-07-29","s","","","12","36");
INSERT INTO passenger VALUES("40","93","Passng7","1996-05-02","20","F","tr","","canedian","","sd1223","2016-07-01","2016-12-27","","","","12","36");

DROP TABLE IF EXISTS purchase_airline;
CREATE TABLE `purchase_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
INSERT INTO purchase_airline VALUES("33","1","2016-07-02","2016-07-02","95","5","1","27377","0","100","0","0","100","15","0","27592","","","0","0","11","34");
INSERT INTO purchase_airline VALUES("34","1","2016-07-02","2016-07-02","98","5","1","102","0","4","2","5","2","3","4","120","","","2","2","12","36");
INSERT INTO purchase_airline VALUES("35","2","2016-07-02","2016-07-02","97","5","2","240","0","75","45","7","2","0","6","370","","","5","0","12","36");
INSERT INTO purchase_airline VALUES("36","3","2016-07-02","2016-07-02","101","5","3","12","0","0","0","0","0","0","0","12","","","0","0","12","36");
INSERT INTO purchase_airline VALUES("37","4","2016-07-02","2016-07-02","101","5","3","12","0","0","0","0","0","0","0","12","","","0","0","12","36");
INSERT INTO purchase_airline VALUES("38","5","2016-07-02","2016-07-02","101","5","3","12","0","0","0","0","0","0","0","12","","","0","0","12","36");
INSERT INTO purchase_airline VALUES("39","6","2016-07-02","2016-07-02","101","5","3","12","0","0","0","0","0","0","0","12","","","0","0","12","36");
INSERT INTO purchase_airline VALUES("40","7","2016-07-02","2016-07-02","101","5","3","12","0","0","0","0","0","0","0","12","","","0","0","12","36");
INSERT INTO purchase_airline VALUES("41","8","2016-07-02","2016-07-02","100","5","4","10","0","0","0","0","0","0","0","10","","","0","0","12","36");
INSERT INTO purchase_airline VALUES("42","9","2016-07-02","2016-07-02","102","5","6","12","0","0","0","0","0","0","0","12","","","0","0","12","36");
INSERT INTO purchase_airline VALUES("43","10","2016-07-02","2016-07-02","102","5","6","12","0","0","0","0","0","0","0","12","","","0","0","12","36");
INSERT INTO purchase_airline VALUES("44","11","2016-07-02","2016-07-02","102","5","6","12","0","0","0","0","0","0","0","12","","","0","0","12","36");
INSERT INTO purchase_airline VALUES("45","12","2016-07-04","2016-07-04","98","5","7","146","3","12","34","56","45","45","56","318","","","76","0","12","36");
INSERT INTO purchase_airline VALUES("46","13","2016-07-05","2016-07-05","99","5","12","6","9","0","1","2","3","4","6","17","","","5","0","12","36");

DROP TABLE IF EXISTS purchase_hotel;
CREATE TABLE `purchase_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
INSERT INTO purchase_hotel VALUES("12","1","2016-07-02","2016-07-02","95","11","1","16830","0","100","20","300","5","15","10","17272","","","8","0","11","34");
INSERT INTO purchase_hotel VALUES("13","1","2016-07-02","2016-07-02","99","11","1","27","0","0","0","0","0","0","0","20","","","7","2","12","36");
INSERT INTO purchase_hotel VALUES("14","2","2016-07-02","2016-07-02","100","11","3","208","0","5","10","5","10","1","8","243","","","4","1","12","36");
INSERT INTO purchase_hotel VALUES("15","3","2016-07-02","2016-07-02","100","11","4","144","0","88","12","7","10","9","7","270","","","7","0","12","36");

DROP TABLE IF EXISTS purchase_misc;
CREATE TABLE `purchase_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
INSERT INTO purchase_misc VALUES("4","1","2016-07-02","2016-07-02","95","15","1","2000","45754","100","200","100","30","15","0","2430","","","0","0","11","34");
INSERT INTO purchase_misc VALUES("5","1","2016-07-02","2016-07-02","102","15","1","10","0","0","0","0","0","0","0","10","","","0","0","12","36");
INSERT INTO purchase_misc VALUES("6","2","2016-07-02","2016-07-02","103","15","3","10","0","0","0","0","0","0","0","10","","","0","0","12","36");
INSERT INTO purchase_misc VALUES("7","3","2016-07-02","2016-07-02","103","15","3","10","0","0","0","0","0","0","0","10","","","0","0","12","36");
INSERT INTO purchase_misc VALUES("8","4","2016-07-02","2016-07-02","103","15","3","10","0","0","0","0","0","0","0","10","","","0","0","12","36");
INSERT INTO purchase_misc VALUES("9","5","2016-07-02","2016-07-02","103","15","3","10","0","0","0","0","0","0","0","10","","","0","0","12","36");
INSERT INTO purchase_misc VALUES("10","6","2016-07-02","2016-07-02","99","15","4","45","0","0","0","0","0","0","0","45","","","0","0","12","36");
INSERT INTO purchase_misc VALUES("11","7","2016-07-02","2016-07-02","99","15","4","45","0","0","0","0","0","0","0","45","","","0","0","12","36");
INSERT INTO purchase_misc VALUES("12","8","2016-07-05","2016-07-05","98","15","6","343","1","2","2","2","2","2","2","353","hh","hh","2","0","12","36");

DROP TABLE IF EXISTS purchase_pac_airline;
CREATE TABLE `purchase_pac_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_airline;
CREATE TABLE `purchase_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date DEFAULT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL DEFAULT '0',
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_hotel;
CREATE TABLE `purchase_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_transport;
CREATE TABLE `purchase_package_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_visa;
CREATE TABLE `purchase_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_transport;
CREATE TABLE `purchase_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
INSERT INTO purchase_transport VALUES("10","1","2016-07-02","2016-07-02","101","13","1","10","0","0","0","0","0","0","0","10","","","0","0","12","36");
INSERT INTO purchase_transport VALUES("11","2","2016-07-02","2016-07-02","100","13","2","20","0","0","0","0","0","0","0","20","","","0","0","12","36");
INSERT INTO purchase_transport VALUES("12","3","2016-07-02","2016-07-02","100","13","2","20","0","0","0","0","0","0","0","20","","","0","0","12","36");
INSERT INTO purchase_transport VALUES("13","4","2016-07-02","2016-07-02","100","13","2","20","0","0","0","0","0","0","0","20","","","0","0","12","36");
INSERT INTO purchase_transport VALUES("14","5","2016-07-02","2016-07-02","100","13","2","20","0","0","0","0","0","0","0","20","","","0","0","12","36");

DROP TABLE IF EXISTS purchase_visa;
CREATE TABLE `purchase_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
INSERT INTO purchase_visa VALUES("10","1","2016-07-02","2016-07-02","95","7","1","8000","0","0","0","0","0","0","0","8000","","","0","0","11","34");
INSERT INTO purchase_visa VALUES("26","1","2016-07-04","2016-07-04","98","7","1","23","2","0","0","0","0","0","0","23","","","0","0","12","36");

DROP TABLE IF EXISTS room_type;
CREATE TABLE `room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `roomType` varchar(30) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
INSERT INTO room_type VALUES("1","Sharing","0","1");
INSERT INTO room_type VALUES("2","Twin","0","1");
INSERT INTO room_type VALUES("3","Triple","0","1");
INSERT INTO room_type VALUES("4","Four Bed","0","1");
INSERT INTO room_type VALUES("5","Five Bed","0","1");
INSERT INTO room_type VALUES("6","Six Bed","0","1");
INSERT INTO room_type VALUES("7","Child With Bed","0","1");
INSERT INTO room_type VALUES("8","Child Without Bed","0","1");
INSERT INTO room_type VALUES("34","1st","11","34");
INSERT INTO room_type VALUES("35","2nd","11","34");
INSERT INTO room_type VALUES("36","3rd","11","34");
INSERT INTO room_type VALUES("37","5star","11","34");
INSERT INTO room_type VALUES("38","3star","11","34");
INSERT INTO room_type VALUES("40","Double Room","12","36");
INSERT INTO room_type VALUES("41","Villa","12","36");

DROP TABLE IF EXISTS staff;
CREATE TABLE `staff` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffName` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `userType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
INSERT INTO staff VALUES("22","Binfas1","123456789","binfas1@gmail.com","all","11","33");
INSERT INTO staff VALUES("23","Staff1","6545","ramm@gjail.com","head clerk","12","35");
INSERT INTO staff VALUES("24","Staff2","1245","fgg@fgg.com","clerk","12","35");
INSERT INTO staff VALUES("25","Teststaff","444","","","13","38");

DROP TABLE IF EXISTS transaction;
CREATE TABLE `transaction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `fromLedger` int(11) NOT NULL,
  `toLedger` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(100) NOT NULL,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `remark` text NOT NULL,
  `addedDate` date NOT NULL,
  `transactionDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `referenceNo` int(11) NOT NULL,
  `details` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=495 DEFAULT CHARSET=latin1;
INSERT INTO transaction VALUES("2","1","90","1","Opening Balance","0","1000","Opening Balance","2016-07-02","2016-07-02","34","11","0","INV#1,acc:CUSTOMER,Opening:1000");
INSERT INTO transaction VALUES("3","91","91","1","Opening Balance","0","100","Opening Balance","2016-07-02","2016-07-02","36","12","0","INV#1, acc:cust1,Opening:100");
INSERT INTO transaction VALUES("4","1","91","1","Opening Balance","100","0","Opening Balance","2016-07-02","2016-07-02","36","12","0","INV#1,acc:cust1,Opening:100");
INSERT INTO transaction VALUES("5","94","94","2","Opening Balance","10","0","Opening Balance","2016-07-02","2016-07-02","34","11","0","INV#2, acc:Agent ,Opening:10");
INSERT INTO transaction VALUES("6","1","94","2","Opening Balance","0","10","Opening Balance","2016-07-02","2016-07-02","34","11","0","INV#2, acc:Agent ,Opening:10");
INSERT INTO transaction VALUES("7","90","90","0","Opening Balance","1000","0","Opening Balance","2016-07-02","2016-07-02","34","0","0","INV#1, acc:CUSTOMER,Opening:1000");
INSERT INTO transaction VALUES("8","1","90","0","Opening Balance","0","1000","Opening Balance","2016-07-02","2016-07-02","34","0","0","INV#1, acc:CUSTOMER,Opening:1000");
INSERT INTO transaction VALUES("9","95","95","3","Opening Balance","1000","0","Opening Balance","2016-07-02","2016-07-02","34","11","0","INV#3, acc:Supplier,Opening:1000");
INSERT INTO transaction VALUES("10","1","95","3","Opening Balance","0","1000","Opening Balance","2016-07-02","2016-07-02","34","11","0","INV#3, acc:Supplier,Opening:1000");
INSERT INTO transaction VALUES("11","90","4","1","Airline Receivable","0","27677","Airline Receivable: 1","2016-07-02","2016-07-02","34","11","0","Airline Receivable(IA), INV#IA1, Pax:Passenger, Date:14-07-2016, Tkt:9845161453, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("12","4","90","1","Airline Receivable","27677","0","Airline Receivable: 1","2016-07-02","2016-07-02","34","11","0","Airline Receivable(IA), INV#IA1, Pax:Passenger, Date:14-07-2016, Tkt:9845161453, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("13","16","90","1","Airline Receivable","0","350","Airline Receivable:Dis# 1","2016-07-02","2016-07-02","34","11","0","Airline Receivable(IA), INV#IA1, Pax:Passenger, Date:14-07-2016, Tkt:9845161453, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("14","90","16","1","Airline Receivable","350","0","Airline Receivable:Dis#1","2016-07-02","2016-07-02","34","11","0","Airline Receivable(IA), INV#IA1, Pax:Passenger, Date:14-07-2016, Tkt:9845161453, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("15","90","19","1","Airline Receivable","0","2000","Airline Receivable:Tds#1","2016-07-02","2016-07-02","34","11","0","Airline Receivable(IA), INV#IA1, Pax:Passenger, Date:14-07-2016, Tkt:9845161453, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("16","19","90","1","Airline Receivable","2000","0","Airline Receivable:Tds#1","2016-07-02","2016-07-02","34","11","0","Airline Receivable(IA), INV#IA1, Pax:Passenger, Date:14-07-2016, Tkt:9845161453, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("17","90","18","1","Airline Receivable","0","15","Airline Receivable:Tax#1","2016-07-02","2016-07-02","34","11","0","Airline Receivable(IA), INV#IA1, Pax:Passenger, Date:14-07-2016, Tkt:9845161453, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("18","18","90","1","Airline Receivable","15","0","Airline Receivable:Tax#1","2016-07-02","2016-07-02","34","11","0","Airline Receivable(IA), INV#IA1, Pax:Passenger, Date:14-07-2016, Tkt:9845161453, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("19","95","5","1","Airline Payable","27577","0","","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("20","5","95","1","Airline Payable","0","27577","","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("21","95","18","1","Airline Payable","15","0","Tax:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("22","18","95","1","Airline Payable","0","15","Tax:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("23","90","10","1","Hotel Receivable","0","16830","Hotel Receivable : 1","2016-07-02","2016-07-02","34","11","0","Hotel Receivable (IH), INV#IH1, Date:12-07-2016, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("24","10","90","1","Hotel Receivable","16830","0","Hotel Receivable : 1","2016-07-02","2016-07-02","34","11","0","Hotel Receivable (IH), INV#IH1, Date:12-07-2016, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("25","95","11","1","Hotel Payable","16955","0","","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("26","11","95","1","Hotel Payable","0","16955","","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("27","95","16","1","Hotel Payable","10","0","Discount Given:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("28","16","95","1","Hotel Payable","0","10","Discount Given:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("29","95","19","1","Hotel Payable","300","0","Tds:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("30","19","95","1","Hotel Payable","0","300","Tds:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("31","95","18","1","Hotel Payable","15","0","Tax:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("32","18","95","1","Hotel Payable","0","15","Tax:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("33","95","20","1","Hotel Payable","0","8","Tac:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("34","20","95","1","Hotel Payable","8","0","Tac:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("35","91","4","1","Airline Receivable","0","114","Airline Receivable: 1","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA1, Pax:Passn3, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("36","4","91","1","Airline Receivable","114","0","Airline Receivable: 1","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA1, Pax:Passn3, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("37","91","19","1","Airline Receivable","0","4","Airline Receivable:Tds#1","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA1, Pax:Passn3, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("38","19","91","1","Airline Receivable","4","0","Airline Receivable:Tds#1","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA1, Pax:Passn3, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("39","91","18","1","Airline Receivable","0","50","Airline Receivable:Tax#1","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA1, Pax:Passn3, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("40","18","91","1","Airline Receivable","50","0","Airline Receivable:Tax#1","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA1, Pax:Passn3, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("41","98","5","1","Airline Payable","110","0","","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("42","5","98","1","Airline Payable","0","110","","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("43","98","16","1","Airline Payable","4","0","Discount Given:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("44","16","98","1","Airline Payable","0","4","Discount Given:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("45","98","19","1","Airline Payable","5","0","Tds:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("46","19","98","1","Airline Payable","0","5","Tds:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("47","98","18","1","Airline Payable","3","0","Tax:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("48","18","98","1","Airline Payable","0","3","Tax:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("49","98","20","1","Airline Payable","0","2","Tac:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("50","20","98","1","Airline Payable","2","0","Tac:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("51","90","6","1","Visa Receivable","0","8445","Visa Receivable : 1","2016-07-02","2016-07-02","34","11","0","Visa Receivable (IV), INV#IV1, submissionDate:14-07-2016, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("52","6","90","1","Visa Receivable","8445","0","Visa Receivable : 1","2016-07-02","2016-07-02","34","11","0","Visa Receivable (IV), INV#IV1, submissionDate:14-07-2016, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("53","91","4","2","Airline Receivable","0","257","Airline Receivable: 2","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA2, Pax:passngertst, Date:06-07-2016, Tkt:we33, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("54","4","91","2","Airline Receivable","257","0","Airline Receivable: 2","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA2, Pax:passngertst, Date:06-07-2016, Tkt:we33, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("55","91","19","2","Airline Receivable","0","7","Airline Receivable:Tds#2","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA2, Pax:passngertst, Date:06-07-2016, Tkt:we33, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("56","19","91","2","Airline Receivable","7","0","Airline Receivable:Tds#2","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA2, Pax:passngertst, Date:06-07-2016, Tkt:we33, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("57","95","7","1","Visa Payable","8000","0","","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("58","91","18","2","Airline Receivable","0","8","Airline Receivable:Tax#2","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA2, Pax:passngertst, Date:06-07-2016, Tkt:we33, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("59","7","95","1","Visa Payable","0","8000","","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("60","18","91","2","Airline Receivable","8","0","Airline Receivable:Tax#2","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA2, Pax:passngertst, Date:06-07-2016, Tkt:we33, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("61","97","5","2","Airline Payable","362","0","","2016-07-02","2016-07-02","36","12","0","INV#2,sup:97,:against:2");
INSERT INTO transaction VALUES("62","5","97","2","Airline Payable","0","362","","2016-07-02","2016-07-02","36","12","0","INV#2,sup:97,:against:2");
INSERT INTO transaction VALUES("63","97","16","2","Airline Payable","6","0","Discount Given:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:97,:against:2");
INSERT INTO transaction VALUES("64","16","97","2","Airline Payable","0","6","Discount Given:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:97,:against:2");
INSERT INTO transaction VALUES("65","97","19","2","Airline Payable","7","0","Tds:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:97,:against:2");
INSERT INTO transaction VALUES("66","19","97","2","Airline Payable","0","7","Tds:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:97,:against:2");
INSERT INTO transaction VALUES("67","97","20","2","Airline Payable","0","5","Tac:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:97,:against:2");
INSERT INTO transaction VALUES("68","20","97","2","Airline Payable","5","0","Tac:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:97,:against:2");
INSERT INTO transaction VALUES("71","95","2","1","Cash Payments","0","10000","","2016-07-02","2016-07-02","34","11","0","Cash Payments(CP), INV#CP, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("72","2","95","1","Cash Payments","10000","0","","2016-07-02","2016-07-02","34","11","0","Cash Payments(CP), INV#CP, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("73","95","2","2","Cash Payments","0","17592","","2016-07-02","2016-07-02","34","11","0","Cash Payments(CP), INV#CP, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("74","2","95","2","Cash Payments","17592","0","","2016-07-02","2016-07-02","34","11","0","Cash Payments(CP), INV#CP, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("75","92","10","1","Hotel Receivable","0","36","Hotel Receivable : 1","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH1, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("76","10","92","1","Hotel Receivable","36","0","Hotel Receivable : 1","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH1, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("77","0","92","1","Hotel Receivable","0","1","Hotel Receivable : 1","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH1, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("78","92","0","1","Hotel Receivable","1","0","Hotel Receivable : 1","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH1, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("79","99","11","1","Hotel Payable","27","0","","2016-07-02","2016-07-02","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("80","11","99","1","Hotel Payable","0","27","","2016-07-02","2016-07-02","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("81","99","20","1","Hotel Payable","0","7","Tac:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("82","20","99","1","Hotel Payable","7","0","Tac:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("83","93","10","2","Hotel Receivable","0","1255","Hotel Receivable : 2","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH2, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("84","10","93","2","Hotel Receivable","1255","0","Hotel Receivable : 2","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH2, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("85","0","93","2","Hotel Receivable","0","24","Hotel Receivable : 2","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH2, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("86","93","0","2","Hotel Receivable","24","0","Hotel Receivable : 2","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH2, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("87","91","10","3","Hotel Receivable","0","211","Hotel Receivable : 3","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH3, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("88","10","91","3","Hotel Receivable","211","0","Hotel Receivable : 3","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH3, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("89","0","91","3","Hotel Receivable","0","1","Hotel Receivable : 3","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH3, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("90","91","0","3","Hotel Receivable","1","0","Hotel Receivable : 3","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH3, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("91","100","11","2","Hotel Payable","233","0","","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:3");
INSERT INTO transaction VALUES("92","11","100","2","Hotel Payable","0","233","","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:3");
INSERT INTO transaction VALUES("93","100","16","2","Hotel Payable","8","0","Discount Given:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:3");
INSERT INTO transaction VALUES("94","16","100","2","Hotel Payable","0","8","Discount Given:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:3");
INSERT INTO transaction VALUES("95","100","19","2","Hotel Payable","5","0","Tds:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:3");
INSERT INTO transaction VALUES("96","19","100","2","Hotel Payable","0","5","Tds:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:3");
INSERT INTO transaction VALUES("97","100","18","2","Hotel Payable","1","0","Tax:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:3");
INSERT INTO transaction VALUES("98","18","100","2","Hotel Payable","0","1","Tax:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:3");
INSERT INTO transaction VALUES("99","100","20","2","Hotel Payable","0","4","Tac:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:3");
INSERT INTO transaction VALUES("100","20","100","2","Hotel Payable","4","0","Tac:inv#2","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:3");
INSERT INTO transaction VALUES("101","93","10","4","Hotel Receivable","0","144","Hotel Receivable : 4","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH4, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("102","10","93","4","Hotel Receivable","144","0","Hotel Receivable : 4","2016-07-02","2016-07-02","36","12","0","Hotel Receivable (IH), INV#IH4, Date:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("103","100","11","3","Hotel Payable","254","0","","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:4");
INSERT INTO transaction VALUES("104","11","100","3","Hotel Payable","0","254","","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:4");
INSERT INTO transaction VALUES("105","100","16","3","Hotel Payable","7","0","Discount Given:inv#3","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:4");
INSERT INTO transaction VALUES("106","16","100","3","Hotel Payable","0","7","Discount Given:inv#3","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:4");
INSERT INTO transaction VALUES("107","100","19","3","Hotel Payable","7","0","Tds:inv#3","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:4");
INSERT INTO transaction VALUES("108","19","100","3","Hotel Payable","0","7","Tds:inv#3","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:4");
INSERT INTO transaction VALUES("109","100","18","3","Hotel Payable","9","0","Tax:inv#3","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:4");
INSERT INTO transaction VALUES("110","18","100","3","Hotel Payable","0","9","Tax:inv#3","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:4");
INSERT INTO transaction VALUES("111","100","20","3","Hotel Payable","0","7","Tac:inv#3","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:4");
INSERT INTO transaction VALUES("112","20","100","3","Hotel Payable","7","0","Tac:inv#3","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:4");
INSERT INTO transaction VALUES("117","98","7","1","Visa Payable","215","0","","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("118","7","98","1","Visa Payable","0","215","","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("119","98","16","1","Visa Payable","2","0","Discount Given:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("120","16","98","1","Visa Payable","0","2","Discount Given:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("121","98","19","1","Visa Payable","7","0","Tds:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("122","19","98","1","Visa Payable","0","7","Tds:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("123","98","18","1","Visa Payable","9","0","Tax:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("124","18","98","1","Visa Payable","0","9","Tax:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("125","98","20","1","Visa Payable","0","7","Tac:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("126","20","98","1","Visa Payable","7","0","Tac:inv#1","2016-07-02","2016-07-02","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("129","90","12","1","Transport Receivable","0","2220","Transport Receivable : 1","2016-07-02","2016-07-02","34","11","0","Transport Receivable (IT), INV#IT1, fromDate:13-07-2016, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("130","12","90","1","Transport Receivable","2220","0","Transport Receivable : 1","2016-07-02","2016-07-02","34","11","0","Transport Receivable (IT), INV#IT1, fromDate:13-07-2016, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("153","102","7","3","Visa Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#3,sup:102,:against:4");
INSERT INTO transaction VALUES("154","90","14","1","Misc Receivable","0","2675","Misc Receivable : 1","2016-07-02","2016-07-02","34","11","0","Misc Receivable (IM), INV#IM1,By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("155","7","102","3","Visa Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#3,sup:102,:against:4");
INSERT INTO transaction VALUES("156","14","90","1","Misc Receivable","2675","0","Misc Receivable : 1","2016-07-02","2016-07-02","34","11","0","Misc Receivable (IM), INV#IM1,By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("157","16","90","1","Misc Receivable","0","22","Misc Receivable : 1","2016-07-02","2016-07-02","34","11","0","Misc Receivable (IM), INV#IM1,By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("158","102","7","4","Visa Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#4,sup:102,:against:4");
INSERT INTO transaction VALUES("159","90","16","1","Misc Receivable","22","0","Misc Receivable : 1","2016-07-02","2016-07-02","34","11","0","Misc Receivable (IM), INV#IM1,By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("160","7","102","4","Visa Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#4,sup:102,:against:4");
INSERT INTO transaction VALUES("161","90","19","1","Misc Receivable","0","300","Misc Receivable: 1","2016-07-02","2016-07-02","34","11","0","Misc Receivable (IM), INV#IM1,By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("162","19","90","1","Misc Receivable","300","0","Misc Receivable: 1","2016-07-02","2016-07-02","34","11","0","Misc Receivable (IM), INV#IM1,By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("163","102","7","5","Visa Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#5,sup:102,:against:4");
INSERT INTO transaction VALUES("164","90","18","1","Misc Receivable","0","15","Misc Receivable: 1","2016-07-02","2016-07-02","34","11","0","Misc Receivable (IM), INV#IM1,By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("165","7","102","5","Visa Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#5,sup:102,:against:4");
INSERT INTO transaction VALUES("166","18","90","1","Misc Receivable","15","0","Misc Receivable: 1","2016-07-02","2016-07-02","34","11","0","Misc Receivable (IM), INV#IM1,By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("169","95","15","1","Misc Payable","2315","0","","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("170","15","95","1","Misc Payable","0","2315","","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("171","95","19","1","Misc Payable","100","0","Tds:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("172","19","95","1","Misc Payable","0","100","Tds:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("173","95","18","1","Misc Payable","15","0","Tax:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("174","18","95","1","Misc Payable","0","15","Tax:inv#1","2016-07-02","2016-07-02","34","11","0","INV#1,sup:95,:against:1");
INSERT INTO transaction VALUES("177","99","7","6","Visa Payable","20","0","","2016-07-02","2016-07-02","36","12","0","INV#6,sup:99,:against:6");
INSERT INTO transaction VALUES("178","7","99","6","Visa Payable","0","20","","2016-07-02","2016-07-02","36","12","0","INV#6,sup:99,:against:6");
INSERT INTO transaction VALUES("179","99","7","7","Visa Payable","20","0","","2016-07-02","2016-07-02","36","12","0","INV#7,sup:99,:against:6");
INSERT INTO transaction VALUES("180","7","99","7","Visa Payable","0","20","","2016-07-02","2016-07-02","36","12","0","INV#7,sup:99,:against:6");
INSERT INTO transaction VALUES("181","99","7","8","Visa Payable","20","0","","2016-07-02","2016-07-02","36","12","0","INV#8,sup:99,:against:6");
INSERT INTO transaction VALUES("182","7","99","8","Visa Payable","0","20","","2016-07-02","2016-07-02","36","12","0","INV#8,sup:99,:against:6");
INSERT INTO transaction VALUES("183","99","7","9","Visa Payable","20","0","","2016-07-02","2016-07-02","36","12","0","INV#9,sup:99,:against:6");
INSERT INTO transaction VALUES("184","7","99","9","Visa Payable","0","20","","2016-07-02","2016-07-02","36","12","0","INV#9,sup:99,:against:6");
INSERT INTO transaction VALUES("187","98","7","10","Visa Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#10,sup:98,:against:7");
INSERT INTO transaction VALUES("188","7","98","10","Visa Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#10,sup:98,:against:7");
INSERT INTO transaction VALUES("189","98","7","11","Visa Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#11,sup:98,:against:7");
INSERT INTO transaction VALUES("190","7","98","11","Visa Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#11,sup:98,:against:7");
INSERT INTO transaction VALUES("191","98","7","12","Visa Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("192","7","98","12","Visa Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("193","98","7","13","Visa Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#13,sup:98,:against:7");
INSERT INTO transaction VALUES("194","7","98","13","Visa Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#13,sup:98,:against:7");
INSERT INTO transaction VALUES("195","98","7","14","Visa Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#14,sup:98,:against:7");
INSERT INTO transaction VALUES("196","7","98","14","Visa Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#14,sup:98,:against:7");
INSERT INTO transaction VALUES("201","90","2","2","Cash Receipts","6830","0","","2016-07-02","2016-07-02","34","11","0","Cash Receipts(CR), INV#CR, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("202","2","90","2","Cash Receipts","0","6830","","2016-07-02","2016-07-02","34","11","0","Cash Receipts(CR), INV#CR, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("203","103","7","2","Visa Payable","150","0","","2016-07-02","2016-07-02","36","12","0","INV#2,sup:103,:against:3");
INSERT INTO transaction VALUES("204","7","103","2","Visa Payable","0","150","","2016-07-02","2016-07-02","36","12","0","INV#2,sup:103,:against:3");
INSERT INTO transaction VALUES("205","90","2","3","Cash Receipts","10000","0","","2016-07-02","2016-07-02","34","11","0","Cash Receipts(CR), INV#CR, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("206","2","90","3","Cash Receipts","0","10000","","2016-07-02","2016-07-02","34","11","0","Cash Receipts(CR), INV#CR, By:Binfasc-Binfas1 ");
INSERT INTO transaction VALUES("207","91","4","3","Airline Receivable","0","12","Airline Receivable: 3","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA3, Pax:pas, Date:, Tkt:122, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("208","4","91","3","Airline Receivable","12","0","Airline Receivable: 3","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA3, Pax:pas, Date:, Tkt:122, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("209","101","5","3","Airline Payable","12","0","","2016-07-02","2016-07-02","36","12","0","INV#3,sup:101,:against:3");
INSERT INTO transaction VALUES("210","5","101","3","Airline Payable","0","12","","2016-07-02","2016-07-02","36","12","0","INV#3,sup:101,:against:3");
INSERT INTO transaction VALUES("211","101","5","4","Airline Payable","12","0","","2016-07-02","2016-07-02","36","12","0","INV#4,sup:101,:against:3");
INSERT INTO transaction VALUES("212","5","101","4","Airline Payable","0","12","","2016-07-02","2016-07-02","36","12","0","INV#4,sup:101,:against:3");
INSERT INTO transaction VALUES("213","101","5","5","Airline Payable","12","0","","2016-07-02","2016-07-02","36","12","0","INV#5,sup:101,:against:3");
INSERT INTO transaction VALUES("214","5","101","5","Airline Payable","0","12","","2016-07-02","2016-07-02","36","12","0","INV#5,sup:101,:against:3");
INSERT INTO transaction VALUES("215","101","5","6","Airline Payable","12","0","","2016-07-02","2016-07-02","36","12","0","INV#6,sup:101,:against:3");
INSERT INTO transaction VALUES("216","5","101","6","Airline Payable","0","12","","2016-07-02","2016-07-02","36","12","0","INV#6,sup:101,:against:3");
INSERT INTO transaction VALUES("217","101","5","7","Airline Payable","12","0","","2016-07-02","2016-07-02","36","12","0","INV#7,sup:101,:against:3");
INSERT INTO transaction VALUES("218","5","101","7","Airline Payable","0","12","","2016-07-02","2016-07-02","36","12","0","INV#7,sup:101,:against:3");
INSERT INTO transaction VALUES("219","106","106","4","Opening Balance","100000","0","Opening Balance","2016-07-02","2016-07-02","34","11","0","INV#4, acc:banking,Opening:100000");
INSERT INTO transaction VALUES("220","1","106","4","Opening Balance","0","100000","Opening Balance","2016-07-02","2016-07-02","34","11","0","INV#4, acc:banking,Opening:100000");
INSERT INTO transaction VALUES("221","91","4","4","Airline Receivable","0","10","Airline Receivable: 4","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA4, Pax:Passn3, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("222","4","91","4","Airline Receivable","10","0","Airline Receivable: 4","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA4, Pax:Passn3, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("223","100","5","8","Airline Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#8,sup:100,:against:4");
INSERT INTO transaction VALUES("224","5","100","8","Airline Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#8,sup:100,:against:4");
INSERT INTO transaction VALUES("225","91","14","1","Misc Receivable","0","13","Misc Receivable : 1","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM1,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("226","14","91","1","Misc Receivable","13","0","Misc Receivable : 1","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM1,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("227","16","91","1","Misc Receivable","0","1","Misc Receivable : 1","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM1,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("228","91","16","1","Misc Receivable","1","0","Misc Receivable : 1","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM1,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("229","91","19","1","Misc Receivable","0","1","Misc Receivable: 1","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM1,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("230","19","91","1","Misc Receivable","1","0","Misc Receivable: 1","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM1,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("231","91","18","1","Misc Receivable","0","1","Misc Receivable: 1","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM1,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("232","18","91","1","Misc Receivable","1","0","Misc Receivable: 1","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM1,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("233","102","15","1","Misc Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#1,sup:102,:against:1");
INSERT INTO transaction VALUES("234","15","102","1","Misc Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#1,sup:102,:against:1");
INSERT INTO transaction VALUES("235","92","14","2","Misc Receivable","0","10","Misc Receivable : 2","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM2,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("236","14","92","2","Misc Receivable","10","0","Misc Receivable : 2","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM2,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("237","92","14","3","Misc Receivable","0","10","Misc Receivable : 3","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM3,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("238","14","92","3","Misc Receivable","10","0","Misc Receivable : 3","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM3,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("239","103","15","2","Misc Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#2,sup:103,:against:3");
INSERT INTO transaction VALUES("240","15","103","2","Misc Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#2,sup:103,:against:3");
INSERT INTO transaction VALUES("241","103","15","3","Misc Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#3,sup:103,:against:3");
INSERT INTO transaction VALUES("242","15","103","3","Misc Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#3,sup:103,:against:3");
INSERT INTO transaction VALUES("243","103","15","4","Misc Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#4,sup:103,:against:3");
INSERT INTO transaction VALUES("244","15","103","4","Misc Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#4,sup:103,:against:3");
INSERT INTO transaction VALUES("245","103","15","5","Misc Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#5,sup:103,:against:3");
INSERT INTO transaction VALUES("246","15","103","5","Misc Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#5,sup:103,:against:3");
INSERT INTO transaction VALUES("247","90","106","1","Bank Receipts","29342","0","","2016-07-02","2016-07-02","34","11","0","INV#BR1,by:Binfas1,from:CUSTOMER");
INSERT INTO transaction VALUES("248","106","90","1","Bank Receipts","0","29342","","2016-07-02","2016-07-02","34","11","0","INV#BR1,by:Binfas1,from:CUSTOMER");
INSERT INTO transaction VALUES("249","91","14","4","Misc Receivable","0","45","Misc Receivable : 4","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM4,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("250","14","91","4","Misc Receivable","45","0","Misc Receivable : 4","2016-07-02","2016-07-02","36","12","0","Misc Receivable (IM), INV#IM4,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("251","99","15","6","Misc Payable","45","0","","2016-07-02","2016-07-02","36","12","0","INV#6,sup:99,:against:4");
INSERT INTO transaction VALUES("252","15","99","6","Misc Payable","0","45","","2016-07-02","2016-07-02","36","12","0","INV#6,sup:99,:against:4");
INSERT INTO transaction VALUES("253","99","15","7","Misc Payable","45","0","","2016-07-02","2016-07-02","36","12","0","INV#7,sup:99,:against:4");
INSERT INTO transaction VALUES("254","15","99","7","Misc Payable","0","45","","2016-07-02","2016-07-02","36","12","0","INV#7,sup:99,:against:4");
INSERT INTO transaction VALUES("255","91","12","1","Transport Receivable","0","14","Transport Receivable : 1","2016-07-02","2016-07-02","36","12","0","Transport Receivable (IT), INV#IT1, fromDate:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("256","12","91","1","Transport Receivable","14","0","Transport Receivable : 1","2016-07-02","2016-07-02","36","12","0","Transport Receivable (IT), INV#IT1, fromDate:02-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("257","101","13","1","Transport Payable","10","0","","2016-07-02","2016-07-02","36","12","0","INV#1,sup:101,:against:1");
INSERT INTO transaction VALUES("258","13","101","1","Transport Payable","0","10","","2016-07-02","2016-07-02","36","12","0","INV#1,sup:101,:against:1");
INSERT INTO transaction VALUES("259","91","12","2","Transport Receivable","0","20","Transport Receivable : 2","2016-07-02","2016-07-02","36","12","0","Transport Receivable (IT), INV#IT2, fromDate:03-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("260","12","91","2","Transport Receivable","20","0","Transport Receivable : 2","2016-07-02","2016-07-02","36","12","0","Transport Receivable (IT), INV#IT2, fromDate:03-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("261","100","13","2","Transport Payable","20","0","","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:2");
INSERT INTO transaction VALUES("262","13","100","2","Transport Payable","0","20","","2016-07-02","2016-07-02","36","12","0","INV#2,sup:100,:against:2");
INSERT INTO transaction VALUES("263","100","13","3","Transport Payable","20","0","","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:2");
INSERT INTO transaction VALUES("264","13","100","3","Transport Payable","0","20","","2016-07-02","2016-07-02","36","12","0","INV#3,sup:100,:against:2");
INSERT INTO transaction VALUES("265","100","13","4","Transport Payable","20","0","","2016-07-02","2016-07-02","36","12","0","INV#4,sup:100,:against:2");
INSERT INTO transaction VALUES("266","13","100","4","Transport Payable","0","20","","2016-07-02","2016-07-02","36","12","0","INV#4,sup:100,:against:2");
INSERT INTO transaction VALUES("267","100","13","5","Transport Payable","20","0","","2016-07-02","2016-07-02","36","12","0","INV#5,sup:100,:against:2");
INSERT INTO transaction VALUES("268","13","100","5","Transport Payable","0","20","","2016-07-02","2016-07-02","36","12","0","INV#5,sup:100,:against:2");
INSERT INTO transaction VALUES("269","90","106","2","Bank Receipts","0","0","","2016-07-02","2016-07-02","34","11","0","INV#BR2,by:Binfas1,from:CUSTOMER");
INSERT INTO transaction VALUES("270","106","90","2","Bank Receipts","0","0","","2016-07-02","2016-07-02","34","11","0","INV#BR2,by:Binfas1,from:CUSTOMER");
INSERT INTO transaction VALUES("271","95","106","1","Bank Payments","0","17272","","2016-07-02","2016-07-02","34","11","0","INV#BP1,by:Binfas1,to:Supplier");
INSERT INTO transaction VALUES("272","106","95","1","Bank Payments","17272","0","","2016-07-02","2016-07-02","34","11","0","INV#BP1,by:Binfas1,to:Supplier");
INSERT INTO transaction VALUES("273","91","2","1","Cash Receipts","164","0","","2016-07-02","2016-07-02","36","12","0","Cash Receipts(CR), INV#CR, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("274","2","91","1","Cash Receipts","0","164","","2016-07-02","2016-07-02","36","12","0","Cash Receipts(CR), INV#CR, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("283","91","8","1","Package Receivable","0","118","Invoice Package: 1","2016-07-02","2016-07-02","36","12","1","INV#IP1,Pac:New2 ,Pax:pass ,By:Staff1");
INSERT INTO transaction VALUES("284","8","91","1","Package Receivable","118","0","Invoice Package: 1","2016-07-02","2016-07-02","36","12","1","INV#IP1,Pac:New2 ,Pax:pass ,By:Staff1");
INSERT INTO transaction VALUES("285","16","91","1","Package Receivable","0","45","Invoice Package: 1","2016-07-02","2016-07-02","36","12","1","INV#IP1,Pac:New2 ,By:Staff1,To:cust1");
INSERT INTO transaction VALUES("286","91","16","1","Package Receivable","45","0","Invoice Package: 1","2016-07-02","2016-07-02","36","12","1","INV#IP1,Pac:New2 ,By:Staff1,To:cust1");
INSERT INTO transaction VALUES("287","91","19","1","Package Receivable","0","34","Package Receivable: 1","2016-07-02","2016-07-02","36","12","1","INV#IP1,Pac:New2 ,By:Staff1,To:Tds");
INSERT INTO transaction VALUES("288","19","91","1","Package Receivable","34","0","Package Receivable: 1","2016-07-02","2016-07-02","36","12","1","INV#IP1,Pac:New2 ,By:Staff1,To:Tds");
INSERT INTO transaction VALUES("289","91","18","1","Package Receivable","0","4","Package Receivable: 1","2016-07-02","2016-07-02","36","12","1","INV#IP1,Pac:New2 ,By:Staff1,To:Tax");
INSERT INTO transaction VALUES("290","18","91","1","Package Receivable","4","0","Package Receivable: 1","2016-07-02","2016-07-02","36","12","1","INV#IP1,Pac:New2 ,By:Staff1,To:Tax");
INSERT INTO transaction VALUES("291","91","4","5","Airline Receivable","0","12","Airline Receivable: 5","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA5, Pax:Pasn1, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("292","4","91","5","Airline Receivable","12","0","Airline Receivable: 5","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA5, Pax:Pasn1, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("293","91","4","6","Airline Receivable","0","12","Airline Receivable: 6","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA6, Pax:Pasn1, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("294","4","91","6","Airline Receivable","12","0","Airline Receivable: 6","2016-07-02","2016-07-02","36","12","0","Airline Receivable(IA), INV#IA6, Pax:Pasn1, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("295","102","5","9","Airline Payable","12","0","","2016-07-02","2016-07-02","36","12","0","INV#9,sup:102,:against:6");
INSERT INTO transaction VALUES("296","5","102","9","Airline Payable","0","12","","2016-07-02","2016-07-02","36","12","0","INV#9,sup:102,:against:6");
INSERT INTO transaction VALUES("297","102","5","10","Airline Payable","12","0","","2016-07-02","2016-07-02","36","12","0","INV#10,sup:102,:against:6");
INSERT INTO transaction VALUES("298","5","102","10","Airline Payable","0","12","","2016-07-02","2016-07-02","36","12","0","INV#10,sup:102,:against:6");
INSERT INTO transaction VALUES("299","102","5","11","Airline Payable","12","0","","2016-07-02","2016-07-02","36","12","0","INV#11,sup:102,:against:6");
INSERT INTO transaction VALUES("300","5","102","11","Airline Payable","0","12","","2016-07-02","2016-07-02","36","12","0","INV#11,sup:102,:against:6");
INSERT INTO transaction VALUES("305","99","7","1","Visa Payable","3413","0","dfg","2016-07-04","2016-07-04","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("306","7","99","1","Visa Payable","0","3413","dfg","2016-07-04","2016-07-04","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("307","99","16","1","Visa Payable","7","0","Discount Given:inv#1","2016-07-04","2016-07-04","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("308","16","99","1","Visa Payable","0","7","Discount Given:inv#1","2016-07-04","2016-07-04","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("309","99","19","1","Visa Payable","6","0","Tds:inv#1","2016-07-04","2016-07-04","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("310","19","99","1","Visa Payable","0","6","Tds:inv#1","2016-07-04","2016-07-04","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("311","99","18","1","Visa Payable","9","0","Tax:inv#1","2016-07-04","2016-07-04","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("312","18","99","1","Visa Payable","0","9","Tax:inv#1","2016-07-04","2016-07-04","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("313","99","20","1","Visa Payable","0","6","Tac:inv#1","2016-07-04","2016-07-04","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("314","20","99","1","Visa Payable","6","0","Tac:inv#1","2016-07-04","2016-07-04","36","12","0","INV#1,sup:99,:against:1");
INSERT INTO transaction VALUES("329","91","4","7","Airline Receivable","0","312","Airline Receivable: 7","2016-07-04","2016-07-04","36","12","0","Airline Receivable(IA), INV#IA7, Pax:pass, Date:14-07-2016, Tkt:4, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("330","4","91","7","Airline Receivable","312","0","Airline Receivable: 7","2016-07-04","2016-07-04","36","12","0","Airline Receivable(IA), INV#IA7, Pax:pass, Date:14-07-2016, Tkt:4, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("331","91","19","7","Airline Receivable","0","23","Airline Receivable:Tds#7","2016-07-04","2016-07-04","36","12","0","Airline Receivable(IA), INV#IA7, Pax:pass, Date:14-07-2016, Tkt:4, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("332","19","91","7","Airline Receivable","23","0","Airline Receivable:Tds#7","2016-07-04","2016-07-04","36","12","0","Airline Receivable(IA), INV#IA7, Pax:pass, Date:14-07-2016, Tkt:4, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("333","91","18","7","Airline Receivable","0","56","Airline Receivable:Tax#7","2016-07-04","2016-07-04","36","12","0","Airline Receivable(IA), INV#IA7, Pax:pass, Date:14-07-2016, Tkt:4, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("334","18","91","7","Airline Receivable","56","0","Airline Receivable:Tax#7","2016-07-04","2016-07-04","36","12","0","Airline Receivable(IA), INV#IA7, Pax:pass, Date:14-07-2016, Tkt:4, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("335","98","5","12","Airline Payable","237","0","","2016-07-04","2016-07-04","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("336","5","98","12","Airline Payable","0","237","","2016-07-04","2016-07-04","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("337","98","16","12","Airline Payable","56","0","Discount Given:inv#12","2016-07-04","2016-07-04","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("338","16","98","12","Airline Payable","0","56","Discount Given:inv#12","2016-07-04","2016-07-04","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("339","98","19","12","Airline Payable","56","0","Tds:inv#12","2016-07-04","2016-07-04","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("340","19","98","12","Airline Payable","0","56","Tds:inv#12","2016-07-04","2016-07-04","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("341","98","18","12","Airline Payable","45","0","Tax:inv#12","2016-07-04","2016-07-04","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("342","18","98","12","Airline Payable","0","45","Tax:inv#12","2016-07-04","2016-07-04","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("343","98","20","12","Airline Payable","0","76","Tac:inv#12","2016-07-04","2016-07-04","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("344","20","98","12","Airline Payable","76","0","Tac:inv#12","2016-07-04","2016-07-04","36","12","0","INV#12,sup:98,:against:7");
INSERT INTO transaction VALUES("345","91","10","5","Hotel Receivable","0","252","Hotel Receivable : 5","2016-07-04","2016-07-04","36","12","0","Hotel Receivable (IH), INV#IH5, Date:04-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("346","10","91","5","Hotel Receivable","252","0","Hotel Receivable : 5","2016-07-04","2016-07-04","36","12","0","Hotel Receivable (IH), INV#IH5, Date:04-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("363","91","14","5","Misc Receivable","0","243","Misc Receivable : 5","2016-07-04","2016-07-04","36","12","0","Misc Receivable (IM), INV#IM5,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("364","14","91","5","Misc Receivable","243","0","Misc Receivable : 5","2016-07-04","2016-07-04","36","12","0","Misc Receivable (IM), INV#IM5,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("365","16","91","5","Misc Receivable","0","5","Misc Receivable : 5","2016-07-04","2016-07-04","36","12","0","Misc Receivable (IM), INV#IM5,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("366","91","16","5","Misc Receivable","5","0","Misc Receivable : 5","2016-07-04","2016-07-04","36","12","0","Misc Receivable (IM), INV#IM5,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("367","91","19","5","Misc Receivable","0","4","Misc Receivable: 5","2016-07-04","2016-07-04","36","12","0","Misc Receivable (IM), INV#IM5,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("368","19","91","5","Misc Receivable","4","0","Misc Receivable: 5","2016-07-04","2016-07-04","36","12","0","Misc Receivable (IM), INV#IM5,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("369","91","18","5","Misc Receivable","0","54","Misc Receivable: 5","2016-07-04","2016-07-04","36","12","0","Misc Receivable (IM), INV#IM5,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("370","18","91","5","Misc Receivable","54","0","Misc Receivable: 5","2016-07-04","2016-07-04","36","12","0","Misc Receivable (IM), INV#IM5,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("371","91","12","3","Transport Receivable","0","257","Transport Receivable : 3","2016-07-04","2016-07-04","36","12","0","Transport Receivable (IT), INV#IT3, fromDate:21-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("372","12","91","3","Transport Receivable","257","0","Transport Receivable : 3","2016-07-04","2016-07-04","36","12","0","Transport Receivable (IT), INV#IT3, fromDate:21-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("373","91","6","1","Visa Receivable","0","87","Visa Receivable : 1","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV1, submissionDate:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("374","6","91","1","Visa Receivable","87","0","Visa Receivable : 1","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV1, submissionDate:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("375","98","7","1","Visa Payable","23","0","","2016-07-04","2016-07-04","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("376","7","98","1","Visa Payable","0","23","","2016-07-04","2016-07-04","36","12","0","INV#1,sup:98,:against:1");
INSERT INTO transaction VALUES("377","92","6","2","Visa Receivable","0","135","Visa Receivable : 2","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV2, submissionDate:28-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("378","6","92","2","Visa Receivable","135","0","Visa Receivable : 2","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV2, submissionDate:28-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("379","16","92","2","Visa Receivable","0","34","Visa Receivable : 2","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV2, submissionDate:28-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("380","92","16","2","Visa Receivable","34","0","Visa Receivable : 2","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV2, submissionDate:28-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("381","91","6","3","Visa Receivable","0","18","Visa Receivable : 3","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV3, submissionDate:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("382","6","91","3","Visa Receivable","18","0","Visa Receivable : 3","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV3, submissionDate:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("383","16","91","3","Visa Receivable","0","4","Visa Receivable : 3","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV3, submissionDate:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("384","91","16","3","Visa Receivable","4","0","Visa Receivable : 3","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV3, submissionDate:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("389","92","10","6","Hotel Receivable","0","2","Hotel Receivable : 6","2016-07-04","2016-07-04","36","12","0","Hotel Receivable (IH), INV#IH6, Date:04-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("390","10","92","6","Hotel Receivable","2","0","Hotel Receivable : 6","2016-07-04","2016-07-04","36","12","0","Hotel Receivable (IH), INV#IH6, Date:04-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("395","93","6","4","Visa Receivable","0","8","Visa Receivable : 4","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV4, submissionDate:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("396","6","93","4","Visa Receivable","8","0","Visa Receivable : 4","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV4, submissionDate:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("397","16","93","4","Visa Receivable","0","4","Visa Receivable : 4","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV4, submissionDate:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("398","93","16","4","Visa Receivable","4","0","Visa Receivable : 4","2016-07-04","2016-07-04","36","12","0","Visa Receivable (IV), INV#IV4, submissionDate:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("399","91","10","7","Hotel Receivable","0","81","Hotel Receivable : 7","2016-07-04","2016-07-04","36","12","0","Hotel Receivable (IH), INV#IH7, Date:04-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("400","10","91","7","Hotel Receivable","81","0","Hotel Receivable : 7","2016-07-04","2016-07-04","36","12","0","Hotel Receivable (IH), INV#IH7, Date:04-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("401","0","91","7","Hotel Receivable","0","12","Hotel Receivable : 7","2016-07-04","2016-07-04","36","12","0","Hotel Receivable (IH), INV#IH7, Date:04-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("402","91","0","7","Hotel Receivable","12","0","Hotel Receivable : 7","2016-07-04","2016-07-04","36","12","0","Hotel Receivable (IH), INV#IH7, Date:04-07-2016, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("403","98","2","1","Cash Payments","0","318","","2016-07-04","2016-07-04","36","12","0","Cash Payments(CP), INV#CP, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("404","2","98","1","Cash Payments","318","0","","2016-07-04","2016-07-04","36","12","0","Cash Payments(CP), INV#CP, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("405","91","2","2","Cash Receipts","14","0","","2016-07-04","2016-07-04","36","12","0","Cash Receipts(CR), INV#CR, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("406","2","91","2","Cash Receipts","0","14","","2016-07-04","2016-07-04","36","12","0","Cash Receipts(CR), INV#CR, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("407","98","96","1","Bank Payments","0","23","","2016-07-05","2016-07-05","36","12","0","INV#BP1,by:Staff1,to:indian airline");
INSERT INTO transaction VALUES("408","96","98","1","Bank Payments","23","0","","2016-07-05","2016-07-05","36","12","0","INV#BP1,by:Staff1,to:indian airline");
INSERT INTO transaction VALUES("409","91","96","1","Bank Receipts","164","0","sdfsdf","2016-07-05","2016-07-05","36","12","0","INV#BR1,by:Staff1,from:cust1");
INSERT INTO transaction VALUES("410","96","91","1","Bank Receipts","0","164","sdfsdf","2016-07-05","2016-07-05","36","12","0","INV#BR1,by:Staff1,from:cust1");
INSERT INTO transaction VALUES("411","91","4","8","Airline Receivable","0","1","Airline Receivable: 8","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA8, Pax:dfg, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("412","4","91","8","Airline Receivable","1","0","Airline Receivable: 8","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA8, Pax:dfg, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("413","91","19","8","Airline Receivable","0","1","Airline Receivable:Tds#8","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA8, Pax:dfg, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("414","19","91","8","Airline Receivable","1","0","Airline Receivable:Tds#8","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA8, Pax:dfg, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("415","91","4","9","Airline Receivable","0","2","Airline Receivable: 9","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA9, Pax:sd, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("416","4","91","9","Airline Receivable","2","0","Airline Receivable: 9","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA9, Pax:sd, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("417","91","19","9","Airline Receivable","0","1","Airline Receivable:Tds#9","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA9, Pax:sd, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("418","19","91","9","Airline Receivable","1","0","Airline Receivable:Tds#9","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA9, Pax:sd, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("419","91","18","9","Airline Receivable","0","1","Airline Receivable:Tax#9","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA9, Pax:sd, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("420","18","91","9","Airline Receivable","1","0","Airline Receivable:Tax#9","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA9, Pax:sd, Date:, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("425","91","8","2","Package Receivable","0","72","Invoice Package: 2","2016-07-05","2016-07-05","36","12","2","INV#IP2,Pac:Aaa ,Pax:Pasn1 ,By:Staff1");
INSERT INTO transaction VALUES("426","8","91","2","Package Receivable","72","0","Invoice Package: 2","2016-07-05","2016-07-05","36","12","2","INV#IP2,Pac:Aaa ,Pax:Pasn1 ,By:Staff1");
INSERT INTO transaction VALUES("427","16","91","2","Package Receivable","0","10","Invoice Package: 2","2016-07-05","2016-07-05","36","12","2","INV#IP2,Pac:Aaa ,By:Staff1,To:cust1");
INSERT INTO transaction VALUES("428","91","16","2","Package Receivable","10","0","Invoice Package: 2","2016-07-05","2016-07-05","36","12","2","INV#IP2,Pac:Aaa ,By:Staff1,To:cust1");
INSERT INTO transaction VALUES("439","91","14","6","Misc Receivable","0","411","Misc Receivable : 6","2016-07-05","2016-07-05","36","12","0","Misc Receivable (IM), INV#IM6,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("440","14","91","6","Misc Receivable","411","0","Misc Receivable : 6","2016-07-05","2016-07-05","36","12","0","Misc Receivable (IM), INV#IM6,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("441","91","18","6","Misc Receivable","0","34","Misc Receivable: 6","2016-07-05","2016-07-05","36","12","0","Misc Receivable (IM), INV#IM6,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("442","18","91","6","Misc Receivable","34","0","Misc Receivable: 6","2016-07-05","2016-07-05","36","12","0","Misc Receivable (IM), INV#IM6,By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("443","98","15","8","Misc Payable","349","0","hh","2016-07-05","2016-07-05","36","12","0","INV#8,sup:98,:against:6");
INSERT INTO transaction VALUES("444","15","98","8","Misc Payable","0","349","hh","2016-07-05","2016-07-05","36","12","0","INV#8,sup:98,:against:6");
INSERT INTO transaction VALUES("445","98","16","8","Misc Payable","2","0","Discount Given:inv#8","2016-07-05","2016-07-05","36","12","0","INV#8,sup:98,:against:6");
INSERT INTO transaction VALUES("446","16","98","8","Misc Payable","0","2","Discount Given:inv#8","2016-07-05","2016-07-05","36","12","0","INV#8,sup:98,:against:6");
INSERT INTO transaction VALUES("447","98","19","8","Misc Payable","2","0","Tds:inv#8","2016-07-05","2016-07-05","36","12","0","INV#8,sup:98,:against:6");
INSERT INTO transaction VALUES("448","19","98","8","Misc Payable","0","2","Tds:inv#8","2016-07-05","2016-07-05","36","12","0","INV#8,sup:98,:against:6");
INSERT INTO transaction VALUES("449","98","18","8","Misc Payable","2","0","Tax:inv#8","2016-07-05","2016-07-05","36","12","0","INV#8,sup:98,:against:6");
INSERT INTO transaction VALUES("450","18","98","8","Misc Payable","0","2","Tax:inv#8","2016-07-05","2016-07-05","36","12","0","INV#8,sup:98,:against:6");
INSERT INTO transaction VALUES("451","98","20","8","Misc Payable","0","2","Tac:inv#8","2016-07-05","2016-07-05","36","12","0","INV#8,sup:98,:against:6");
INSERT INTO transaction VALUES("452","20","98","8","Misc Payable","2","0","Tac:inv#8","2016-07-05","2016-07-05","36","12","0","INV#8,sup:98,:against:6");
INSERT INTO transaction VALUES("453","91","4","11","Airline Receivable","0","23","Airline Receivable: 11","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA11, Pax:Edssd, Date:00-00-0000, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("454","4","91","11","Airline Receivable","23","0","Airline Receivable: 11","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA11, Pax:Edssd, Date:00-00-0000, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("455","16","91","11","Airline Receivable","0","3","Airline Receivable:Dis# 11","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA11, Pax:Edssd, Date:00-00-0000, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("456","91","16","11","Airline Receivable","3","0","Airline Receivable:Dis#11","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA11, Pax:Edssd, Date:00-00-0000, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("457","91","19","11","Airline Receivable","0","1","Airline Receivable:Tds#11","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA11, Pax:Edssd, Date:00-00-0000, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("458","19","91","11","Airline Receivable","1","0","Airline Receivable:Tds#11","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA11, Pax:Edssd, Date:00-00-0000, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("459","91","18","11","Airline Receivable","0","1","Airline Receivable:Tax#11","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA11, Pax:Edssd, Date:00-00-0000, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("460","18","91","11","Airline Receivable","1","0","Airline Receivable:Tax#11","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA11, Pax:Edssd, Date:00-00-0000, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("469","91","4","10","Airline Receivable","0","78","Airline Receivable: 10","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA10, Pax:Asd, Date:00-00-0000, Tkt:2, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("470","4","91","10","Airline Receivable","78","0","Airline Receivable: 10","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA10, Pax:Asd, Date:00-00-0000, Tkt:2, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("471","16","91","10","Airline Receivable","0","2","Airline Receivable:Dis# 10","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA10, Pax:Asd, Date:00-00-0000, Tkt:2, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("472","91","16","10","Airline Receivable","2","0","Airline Receivable:Dis#10","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA10, Pax:Asd, Date:00-00-0000, Tkt:2, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("473","91","19","10","Airline Receivable","0","1","Airline Receivable:Tds#10","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA10, Pax:Asd, Date:00-00-0000, Tkt:2, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("474","19","91","10","Airline Receivable","1","0","Airline Receivable:Tds#10","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA10, Pax:Asd, Date:00-00-0000, Tkt:2, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("475","91","18","10","Airline Receivable","0","10","Airline Receivable:Tax#10","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA10, Pax:Asd, Date:00-00-0000, Tkt:2, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("476","18","91","10","Airline Receivable","10","0","Airline Receivable:Tax#10","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA10, Pax:Asd, Date:00-00-0000, Tkt:2, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("477","91","4","12","Airline Receivable","0","11","Airline Receivable: 12","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA12, Pax:Pasn1, Date:05-07-2016, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("478","4","91","12","Airline Receivable","11","0","Airline Receivable: 12","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA12, Pax:Pasn1, Date:05-07-2016, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("479","16","91","12","Airline Receivable","0","1","Airline Receivable:Dis# 12","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA12, Pax:Pasn1, Date:05-07-2016, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("480","91","16","12","Airline Receivable","1","0","Airline Receivable:Dis#12","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA12, Pax:Pasn1, Date:05-07-2016, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("481","91","19","12","Airline Receivable","0","1","Airline Receivable:Tds#12","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA12, Pax:Pasn1, Date:05-07-2016, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("482","19","91","12","Airline Receivable","1","0","Airline Receivable:Tds#12","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA12, Pax:Pasn1, Date:05-07-2016, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("483","91","18","12","Airline Receivable","0","1","Airline Receivable:Tax#12","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA12, Pax:Pasn1, Date:05-07-2016, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("484","18","91","12","Airline Receivable","1","0","Airline Receivable:Tax#12","2016-07-05","2016-07-05","36","12","0","Airline Receivable(IA), INV#IA12, Pax:Pasn1, Date:05-07-2016, Tkt:, By:Rameez-Staff1 ");
INSERT INTO transaction VALUES("485","99","5","13","Airline Payable","10","0","","2016-07-05","2016-07-05","36","12","0","INV#13,sup:99,:against:12");
INSERT INTO transaction VALUES("486","5","99","13","Airline Payable","0","10","","2016-07-05","2016-07-05","36","12","0","INV#13,sup:99,:against:12");
INSERT INTO transaction VALUES("487","99","16","13","Airline Payable","6","0","Discount Given:inv#13","2016-07-05","2016-07-05","36","12","0","INV#13,sup:99,:against:12");
INSERT INTO transaction VALUES("488","16","99","13","Airline Payable","0","6","Discount Given:inv#13","2016-07-05","2016-07-05","36","12","0","INV#13,sup:99,:against:12");
INSERT INTO transaction VALUES("489","99","19","13","Airline Payable","2","0","Tds:inv#13","2016-07-05","2016-07-05","36","12","0","INV#13,sup:99,:against:12");
INSERT INTO transaction VALUES("490","19","99","13","Airline Payable","0","2","Tds:inv#13","2016-07-05","2016-07-05","36","12","0","INV#13,sup:99,:against:12");
INSERT INTO transaction VALUES("491","99","18","13","Airline Payable","4","0","Tax:inv#13","2016-07-05","2016-07-05","36","12","0","INV#13,sup:99,:against:12");
INSERT INTO transaction VALUES("492","18","99","13","Airline Payable","0","4","Tax:inv#13","2016-07-05","2016-07-05","36","12","0","INV#13,sup:99,:against:12");
INSERT INTO transaction VALUES("493","99","20","13","Airline Payable","0","5","Tac:inv#13","2016-07-05","2016-07-05","36","12","0","INV#13,sup:99,:against:12");
INSERT INTO transaction VALUES("494","20","99","13","Airline Payable","5","0","Tac:inv#13","2016-07-05","2016-07-05","36","12","0","INV#13,sup:99,:against:12");

DROP TABLE IF EXISTS transport_provider;
CREATE TABLE `transport_provider` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `providerName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
INSERT INTO transport_provider VALUES("12","Kallada","Manjeri","94778595564","11","34");
INSERT INTO transport_provider VALUES("13","Uber","","","12","36");
INSERT INTO transport_provider VALUES("14","Ola","","","12","36");
INSERT INTO transport_provider VALUES("15","Galaxy","","","12","36");
INSERT INTO transport_provider VALUES("16","Kallada","","","12","36");

DROP TABLE IF EXISTS user_permission;
CREATE TABLE `user_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NOT NULL,
  `basicSetting` int(11) NOT NULL DEFAULT '0',
  `basicSettingEdit` int(11) NOT NULL DEFAULT '0',
  `package` int(11) NOT NULL DEFAULT '0',
  `packageEdit` int(11) NOT NULL DEFAULT '0',
  `account` int(11) NOT NULL DEFAULT '0',
  `accountEdit` int(11) NOT NULL DEFAULT '0',
  `cashBank` int(11) NOT NULL DEFAULT '0',
  `cashBankEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceAirline` int(11) NOT NULL DEFAULT '0',
  `invoiceAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceHotel` int(11) NOT NULL DEFAULT '0',
  `invoiceHotelEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceVisa` int(11) NOT NULL DEFAULT '0',
  `invoiceVisaEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceMisc` int(11) NOT NULL DEFAULT '0',
  `invoiceMiscEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceTransport` int(11) NOT NULL DEFAULT '0',
  `invoiceTransportEdit` int(11) NOT NULL DEFAULT '0',
  `invoicePackage` int(11) NOT NULL DEFAULT '0',
  `invoicePackageEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseAirline` int(11) NOT NULL DEFAULT '0',
  `purchaseAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseHotel` int(11) NOT NULL DEFAULT '0',
  `purchaseHotelEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseVisa` int(11) NOT NULL DEFAULT '0',
  `purchaseVisaEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseMisc` int(11) NOT NULL DEFAULT '0',
  `purchaseMiscEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseTransport` int(11) NOT NULL DEFAULT '0',
  `purchaseTransportEdit` int(11) NOT NULL DEFAULT '0',
  `purchasePackage` int(11) NOT NULL DEFAULT '0',
  `purchasePackageEdit` int(11) NOT NULL DEFAULT '0',
  `accountReport` int(11) NOT NULL DEFAULT '0',
  `otherReport` int(11) NOT NULL DEFAULT '0',
  `user` int(11) NOT NULL DEFAULT '0',
  `userEdit` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `staffId` (`staffId`),
  CONSTRAINT `user_permission_ibfk_1` FOREIGN KEY (`staffId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
INSERT INTO user_permission VALUES("17","22","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO user_permission VALUES("18","23","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO user_permission VALUES("19","24","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","1","1","1","1","0","0","1","1","1","1");
INSERT INTO user_permission VALUES("20","25","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");

DROP TABLE IF EXISTS vehicle_types;
CREATE TABLE `vehicle_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `vehicleType` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
INSERT INTO vehicle_types VALUES("20","Car","11","34");
INSERT INTO vehicle_types VALUES("21","Bus","11","34");
INSERT INTO vehicle_types VALUES("22","Truck","11","34");
INSERT INTO vehicle_types VALUES("23","Plane","11","34");
INSERT INTO vehicle_types VALUES("25","Jet","11","34");
INSERT INTO vehicle_types VALUES("26","Two Wheeler","12","36");
INSERT INTO vehicle_types VALUES("27","Three Wheeler","12","36");
INSERT INTO vehicle_types VALUES("28","Bus","12","36");
INSERT INTO vehicle_types VALUES("29","Car","12","36");
INSERT INTO vehicle_types VALUES("30","Truck","12","36");

DROP TABLE IF EXISTS visa_passenger;
CREATE TABLE `visa_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visaId` int(11) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(20) NOT NULL,
  `passportNo` varchar(50) NOT NULL,
  `visaFor` varchar(50) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `tokenNo` varchar(20) NOT NULL,
  `visaFee` float NOT NULL,
  `vfs` float NOT NULL,
  `ddCharge` float NOT NULL,
  `serviceCharge` float NOT NULL,
  `otherCharge` float NOT NULL,
  `courierCharge` float NOT NULL,
  `subTotal` float NOT NULL,
  `travelDate` date NOT NULL,
  `visaMessage` varchar(20) NOT NULL,
  `msgChngBy` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
INSERT INTO visa_passenger VALUES("10","13","Passenger","1993-02-20","M","123456","20","2016-07-14","2016-07-15","2016-07-16","test","8000","200","20","125","35","65","8445","0000-00-00","","0","","11","34");
INSERT INTO visa_passenger VALUES("34","33","Pasn1","1986-07-23","M","as123","26","0000-00-00","0000-00-00","0000-00-00","","23","34","53","0","0","0","87","0000-00-00","","0","","12","36");
INSERT INTO visa_passenger VALUES("35","34","Passng4","2006-07-12","F","err","26","2016-07-28","2016-07-25","2016-07-27","","12","0","34","56","45","0","135","0000-00-00","","0","","12","36");
INSERT INTO visa_passenger VALUES("36","35","Panng2","1999-07-13","F","","26","0000-00-00","0000-00-00","0000-00-00","","2","2","5","8","0","0","15","0000-00-00","","0","","12","36");

DROP TABLE IF EXISTS visafor;
CREATE TABLE `visafor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visafor` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
INSERT INTO visafor VALUES("27","qater","12","36");
INSERT INTO visafor VALUES("26","kenya","12","36");
INSERT INTO visafor VALUES("25","kuwait","12","36");
INSERT INTO visafor VALUES("24","dubai","12","36");
INSERT INTO visafor VALUES("23","america","12","36");
INSERT INTO visafor VALUES("22","Umra","11","34");
INSERT INTO visafor VALUES("21","hajj","11","34");
INSERT INTO visafor VALUES("20","Visiting","11","34");
INSERT INTO visafor VALUES("19","Study","11","34");
INSERT INTO visafor VALUES("18","Job","11","34");



								/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
								/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
								/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
								/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
								/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
								/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
								/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
								
						