/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
								/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
								/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
								/*!40101 SET NAMES utf8 */;
								/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
								/*!40103 SET TIME_ZONE='+00:00' */;
								/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
								/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
								/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
								/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
								DROP TABLE IF EXISTS academicyear;
CREATE TABLE `academicyear` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `fromYear` varchar(20) NOT NULL,
  `toYear` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
INSERT INTO academicyear VALUES("2","new year2","2016","2017","YES");

DROP TABLE IF EXISTS account_group;
CREATE TABLE `account_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `acGroup` text NOT NULL,
  `parent` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
INSERT INTO account_group VALUES("1","Account","0","0","0");
INSERT INTO account_group VALUES("2","Balance Sheet","1","0","0");
INSERT INTO account_group VALUES("3","Profit & Loss Account","1","0","0");
INSERT INTO account_group VALUES("4","Assets","2","0","0");
INSERT INTO account_group VALUES("5","Liabilities","2","0","0");
INSERT INTO account_group VALUES("6","Current Assets","4","0","0");
INSERT INTO account_group VALUES("7","Fixed Assets","4","0","0");
INSERT INTO account_group VALUES("8","Cash In Hand","6","0","0");
INSERT INTO account_group VALUES("9","Deposits","6","0","0");
INSERT INTO account_group VALUES("10","Investments","6","0","0");
INSERT INTO account_group VALUES("11","Loans & Advances (asset)","6","0","0");
INSERT INTO account_group VALUES("12","Bank A/c","6","0","0");
INSERT INTO account_group VALUES("13","Expenses","3","0","0");
INSERT INTO account_group VALUES("14","Income","3","0","0");
INSERT INTO account_group VALUES("15","Cash","8","0","0");
INSERT INTO account_group VALUES("16","Capital","5","0","0");
INSERT INTO account_group VALUES("17","Current Liabilities","5","0","0");
INSERT INTO account_group VALUES("18","Loans (liability)","5","0","0");
INSERT INTO account_group VALUES("19","Suspense Accounts","5","0","0");
INSERT INTO account_group VALUES("20","Bank OD A/C","17","0","0");
INSERT INTO account_group VALUES("21","Duties & Taxes","17","0","0");
INSERT INTO account_group VALUES("22","Journal Accounts","17","0","0");
INSERT INTO account_group VALUES("23","Provisions","17","0","0");
INSERT INTO account_group VALUES("24","Direct Expense","13","0","0");
INSERT INTO account_group VALUES("25","Indirect Expense","13","0","0");
INSERT INTO account_group VALUES("26","Direct Income","14","0","0");
INSERT INTO account_group VALUES("27","Indirect Income","14","0","0");
INSERT INTO account_group VALUES("28","Fittings","7","0","0");
INSERT INTO account_group VALUES("29","Advertisement","25","0","0");
INSERT INTO account_group VALUES("30","Donation Given","25","0","0");
INSERT INTO account_group VALUES("31","Electricity Bills","25","0","0");
INSERT INTO account_group VALUES("32","Miscellaneous Expense","25","0","0");
INSERT INTO account_group VALUES("33","Office Expense","25","0","0");
INSERT INTO account_group VALUES("34","Other Expenses","25","0","0");
INSERT INTO account_group VALUES("35","Telephone Bills","25","0","0");
INSERT INTO account_group VALUES("36","Other Income","27","0","0");
INSERT INTO account_group VALUES("37","Customer/ Sundry Debtors","6","0","0");
INSERT INTO account_group VALUES("38","Shipping Agents","17","0","0");
INSERT INTO account_group VALUES("39","Staff Deposits","17","0","0");
INSERT INTO account_group VALUES("40","Supplier/ Sundry Creditors","17","0","0");
INSERT INTO account_group VALUES("41","Discount Given ","24","0","0");
INSERT INTO account_group VALUES("42","Job Works","24","0","0");
INSERT INTO account_group VALUES("43","Purchases","24","0","0");
INSERT INTO account_group VALUES("44","Courier","26","0","0");
INSERT INTO account_group VALUES("45","Discount Recieved","26","0","0");
INSERT INTO account_group VALUES("46","Sales","26","0","0");
INSERT INTO account_group VALUES("47","Services","26","0","0");
INSERT INTO account_group VALUES("48","Commission Agent","25","0","0");
INSERT INTO account_group VALUES("49","Incentives Given","25","0","0");
INSERT INTO account_group VALUES("50","Printing Expense","25","0","0");
INSERT INTO account_group VALUES("51","Rent Given","25","0","0");
INSERT INTO account_group VALUES("52","Salaries","25","0","0");
INSERT INTO account_group VALUES("53","Sales Expenses","25","0","0");
INSERT INTO account_group VALUES("54","Tax Paid","25","0","0");
INSERT INTO account_group VALUES("55","Tax Recieved","25","0","0");
INSERT INTO account_group VALUES("56","Incentives Recieved","27","0","0");
INSERT INTO account_group VALUES("57","Branches","6","0","0");
INSERT INTO account_group VALUES("58","Main Branches","17","0","0");

DROP TABLE IF EXISTS accounts;
CREATE TABLE `accounts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `agentId` int(11) NOT NULL,
  `accountGroupId` int(11) NOT NULL,
  `accountName` varchar(200) NOT NULL,
  `openingBal` double NOT NULL,
  `payType` varchar(20) NOT NULL,
  `masterLedger` int(11) NOT NULL,
  `addDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `description` text NOT NULL,
  `contactPerson` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address2` text NOT NULL,
  `pin2` varchar(20) NOT NULL,
  `city2` varchar(200) NOT NULL,
  `customerKey` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `accountGroupId` (`accountGroupId`),
  CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`accountGroupId`) REFERENCES `account_group` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=latin1;
INSERT INTO accounts VALUES("1","0","19","Suspense","0","Credit","1","2016-03-22","1","0","0","Suspense Account","","","","","","","","","","","");
INSERT INTO accounts VALUES("2","0","8","Cash Account","0","Credit","1","2016-03-22","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("3","0","24","Salary Payments","0","Credit","1","2016-03-22","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("4","0","14","Airline Receivable","0","Debit","1","2016-04-05","1","0","0","Airline charge received","","","","","","","","","","","");
INSERT INTO accounts VALUES("5","0","13","Airline Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("6","0","14","Visa Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("7","0","13","Visa Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("8","0","14","Package Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("9","0","13","Package Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("10","0","14","Hotel Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("11","0","13","Hotel Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("12","0","14","Transport Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("13","0","13","Transport Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("14","0","14","Misc Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("15","0","13","Misc Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("16","0","25","Discount Given","0","Credit","1","2016-05-25","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("17","0","27","Discount Received","0","Credit","1","2016-05-25","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("18","0","25","Tax","0","Credit","1","2016-06-24","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("19","0","25","Tds","0","Credit","1","2016-06-24","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("20","0","27","Tac","0","Credit","1","2016-06-24","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("21","0","25","Package Extra Chrg","0","Credit","1","2016-06-29","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("22","0","13","Other Purchase","0","Credit","0","2016-08-16","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("79","0","48","WC","0","Credit","0","2016-08-31","2","1","2","","WC","0","","","","0","","","","","");
INSERT INTO accounts VALUES("80","0","48","None","0","Credit","0","2016-09-01","6","2","2","","None","0","","","","0","","","","","");
INSERT INTO accounts VALUES("81","80","40","MTM  CCJ","0","Credit","0","2016-09-01","6","2","2","","MR FAISAL","CALICUT","","CALICUT","0495 4011414","9605198598","faizaltravelmart@gmail.com","","","","");
INSERT INTO accounts VALUES("82","80","40","MTM VISA","0","Credit","0","2016-09-01","6","2","2","","MR SAHAD","MARKAZ COMPLES ANNEX\nMAVOOR ROAD CALICUT","673004","CALICUT","0495 4011414","7736538597","mtmsahad@gmail.com","","","","");
INSERT INTO accounts VALUES("83","80","40","Creative Tours & Travels - KODUNGALLUR","0","Credit","0","2016-09-01","6","2","2","","Ms SABITHA","Creative Tours & Travels (India) Pvt. Ltd.\n PRABHA BUILDING, NORTH NADA,\n KODUNGALLUR - 680664\n DIST. - TRISSUR\n TEL : 0480 3005013","680664","KODUNGALLUR","04803005013","9947610703","sabithakm2010@gmail.com","","","","");
INSERT INTO accounts VALUES("84","80","40","LAWSON TRAVEL","0","Credit","0","2016-09-01","6","2","2","SG Ticket  shintu.v@lawson.travel","MR AZEEZ MP","Mangaden center, OPP. Asma tower , Mavoor road, Calicut - 673 004\n\nazeez.mp@lawson.travel\nSG Ticket  shintu.v@lawson.travel","673004","CALICUT","495-4093013","9847759510","","","","","");
INSERT INTO accounts VALUES("85","80","40","FLYHIND CCJ","0","Credit","0","2016-09-01","6","2","2","","Nikhil Shyam","1st Floor, Shobha Tower, Mavoor Road, Calicut - 673 004\n\nnikhil@flyhind.in","673004","CALICUT","0495 4025555","9746744717","nikhilsyam59@gmail.com","","","","");
INSERT INTO accounts VALUES("86","80","37","ABOOBACKAR  E P","0","Credit","0","2016-09-01","6","2","2","","ABOOBACKAR","CHAMRAVATTAM","000000","CHAMRAVATTAM","00971555013564","00971555013564","cholayilparambil@gmail.com","NON","000000","NON","");
INSERT INTO accounts VALUES("87","80","37","ASAD KC (BAVUTTY)","0","Credit","0","2016-09-01","6","2","2","","ASAD","RANDATHANI\nPOVANCHINA","676510","RANDATHANI","9020570402","920739224","asad.randathani@gmail.com","RANDATHANI","676510","RANDATHANI","");
INSERT INTO accounts VALUES("88","80","37","AHAMED KUTTY - KOTTAKKAL -KUTTIPPURAM","0","Credit","0","2016-09-08","6","2","2","","AHAMED KUTTY","KOTTAKKAL \nKUTTIPPURAM","676503","KOTTAKKAL","9947039616","00971559278031","mtmvettichira@gmail.com","KALLANGATTU KUZHIYIL","676503","KOTTAKKAL","");
INSERT INTO accounts VALUES("89","80","37","MOHAMED ALI MT","0","Credit","0","2016-09-03","6","2","2","","MOHAMED ALI MT","MACHINCHERY THOOMBIL","676510","RANDATHANI","00971508403056","00971508403056","cholayilparambil@gmail.com","RANDATHANI","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("90","80","37","RAHEEM HAJI  CV","0","Credit","0","2016-09-03","6","2","2","","MANU","VATTA PARAMBU","676510","RANDATHANI","7034637861","00971504959957","cholayilparambil@gmail.com","VATTA PARAMBU","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("91","80","37","RASHEED PARAMBATH","0","Credit","0","2016-09-03","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","00971551777134","00971551777134","mateenasm134@gmail.com","RANDATHANI","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("92","80","37","MUHAMMED MUKTHAR  MALIYEKKAL","0","Credit","0","2016-09-05","6","2","2","","HANEEFA","ABU DHABI","676510","RANDATHANI","9526187272","00971558918388","haneefa237@gmail.com","RANDATHANI","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("93","79","37","GAMA HOLIDAYS CCJ","35900","Debit","0","2016-09-05","10","1","2","","ARSHAD","KOZHIKODE","000000","KOZHIKODE","+91 903 7777 019 | 9","+91 903 7777 019 | 9","markaztravelmart@gmail.com","KOZHIKODE","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("94","79","40","SALIL PURCHASE","83925","Debit","0","2016-09-05","10","1","2","","SALIL","SHOP NO.2, Millenium Society\n\nRoad No.3, Liberty Garden\n\nMalad (west), Mumbai-400064","400064","MUMBAI","91-22-28892250 / 225","9892519222/998724422","salilcs84@gmail.com","SHOP NO.2, Millenium Society\n\nRoad No.3, Liberty Garden\n\nMalad (west), Mumbai-400064","400064","MUMBAI","");
INSERT INTO accounts VALUES("95","79","37","CREATIVE KODUNGALLUR","0","Debit","0","2016-09-05","10","1","2","","SHANMUGAN","VISA CONSULTANT \n101,Prabha Building,Opp, Municipal Bus stand,\nKodungallur-680 664,Thrissur Dist.Kerala ,","000000","KODUNGALLUR","9349803028","0480-3005047, 300500","markaztravelmart@gmail.com","TRISSUR","000000","TRISSUR","");
INSERT INTO accounts VALUES("96","79","37","SAFUVAN STAFF","0","Debit","0","2016-09-08","10","1","2","","SAFUVAN","AIKARAPADI","000000","MALAPPUARAM","9995959734","9995959734","safuvan.m4@gmail.com","AIKARAPPADI","000000","MALAPPURAM","");
INSERT INTO accounts VALUES("97","79","40","UNKNWON SUPPLIER","0","Credit","0","2016-09-05","10","1","2","","0","0","000000","0","0","8547116314","markaztravelmart@gmail.com","0","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("98","80","37","JAYAFER AINIKUNNAN","0","Credit","0","2016-09-05","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","9895936252","9895936252","cholayilparambil@gmail.com","AAA","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("99","79","37","ALHIND COK","0","Debit","0","2016-09-06","10","1","2","","AMOOLYA","Alhind Tours & Travels Pvt Ltd\nElmar Square\nAbove HDFC BANK ,M.G.Road,Ravipuram .Cochin-16","000000","COCHIN","0484-3082025,3082024","9496094990, 94960180","helpdesk2.cok@alhindonline.com","COCHIN","000000","COCHIN","");
INSERT INTO accounts VALUES("100","80","37","ABOOBACKER KARINGAPPARA","0","Credit","0","2016-09-05","6","2","2","","ABOOBACKER","VETTICHIRA","676510","VETTICHIRA","9846338837","9846338837","mtmvettichira@gmail.com","vettichira","676510","vettichira","");
INSERT INTO accounts VALUES("101","79","37","SALIL SALES","0","Debit","0","2016-09-05","10","1","2","","SALIL","SHOP NO.2, Millenium Society\n\nRoad No.3, Liberty Garden\n\nMalad (west), Mumbai-400064\n\nPh: +91-22-28892250 / 2255","000000","MALAD,MUMBAI","9892519222","9892519222/998724422","salilcs84@gmail.com","SHOP NO.2, Millenium Society\n\nRoad No.3, Liberty Garden\n\nMalad (west), Mumbai-400064\n\nPh: +91-22-28892250 / 2255","000000","MUMBAI","");
INSERT INTO accounts VALUES("102","79","40","TRAVEL PARTNER PURCHASE","90100","Debit","0","2016-09-05","10","1","2","","SHAMNAS","Sky Tower, Near OmanAir...ways,\nBank Rd, Mavoor Road Jn Calicut-1","000000","KOZHIKODE","495 4016677","9544053344","CCJ@travelpartnerholidays.com","Sky Tower, Near OmanAir...ways,\nBank Rd, Mavoor Road Jn Calicut-1","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("103","79","37","MTM VETTICHIRA","2500","Debit","0","2016-09-05","10","1","2","","MANU","VETTICHIRA","000000","MALAPPURAM","04942617140","04942617140","mtmvettichira@gmail.com","VETTICHIRA","000000","MALAPPURAM","");
INSERT INTO accounts VALUES("104","0","48","KKH","0","Credit","0","2016-09-05","8","1","2","","KKH","WAYANAD","","","","0000","","","","","");
INSERT INTO accounts VALUES("105","104","37","HARIS","0","Credit","0","2016-09-05","8","1","2","","HARIS","WAYANAD","673637","WAYANAD","000","00","safuvan.m4@gmail.com","WAY","637637","WAY","");
INSERT INTO accounts VALUES("106","0","48","FAIZAL","0","Credit","0","2016-09-05","17","4","2","","FAIZAL","...","","","","9605198598","","","","","");
INSERT INTO accounts VALUES("107","0","48","SIRAJ SAQUAFI","0","Credit","0","2016-09-05","17","4","2","","SIRAJ SAQUAFI","CHERAPURAM","","","","9961953870","","","","","");
INSERT INTO accounts VALUES("108","106","37","FAIZEEE","0","Debit","0","2016-09-05","17","4","2","","AKHIL","NN","673004","CCJ","9988774455","665544778899","FAIZALTRAVELMART@GMAIL.COM","MMM","673004","673004","");
INSERT INTO accounts VALUES("109","0","48","arshak","0","Credit","0","2016-09-05","21","4","2","","arshak","mele edakkat house","","","","8089684701","","","","","");
INSERT INTO accounts VALUES("110","0","48","AKI","0","Credit","0","2016-09-05","17","4","2","","AKI","GUDALUR","","","","9486747270","","","","","");
INSERT INTO accounts VALUES("111","106","40","IX","0","Credit","0","2016-09-05","17","4","2","","","","","","","8855223366","","","","","");
INSERT INTO accounts VALUES("112","110","40","AL HIND","1000","Debit","0","2016-09-05","17","4","2","","","","","","","1","","","","","");
INSERT INTO accounts VALUES("113","0","48","ZIYARA","0","Credit","0","2016-09-05","17","4","2","","ZIYARA","WAYANAD","","","","9999009990","","","","","");
INSERT INTO accounts VALUES("114","79","37","CREATIVE THIRUVANANDHAPURAM","0","Debit","0","2016-09-09","10","1","2","","Shahina Afsa","Shahina Afsal\n VISA CONSULTANT\n101,Prabha Building,Opp, Municipal Bus stand,\nKodungallur-680 664,Thrissur Dist.Kerala ,\n0480-3005047, 3005000(60 lines) 3006000(40 lines) 9349803028","000000","KODUNGALLUR","0480-3005047,","9349803028","visakdr@creativeholidaysindia.com","KODUNGLLUR","000000","0","");
INSERT INTO accounts VALUES("115","107","37","SIRAJ CHERAPURAM","0","Credit","0","2016-09-05","19","4","2","NOT","SIRAJ","KOZHIKODE\nMAVOOR ROAD","673004","KOZHIKODE","0","9845488545","mtmrecruits@gmail.com","KOZHIKODE\nMAVOOR ROAD","673004","KOZHIKODE","");
INSERT INTO accounts VALUES("116","107","40","AL UJOOR","0","Credit","0","2016-09-05","19","4","2","UMRA VISA","ABBAS","MAKKA","","","","00966000000000","","","","","");
INSERT INTO accounts VALUES("117","0","48","MARVA TRAVELS","0","Credit","0","2016-09-05","18","4","2","","MARVA TRAVELS","KERAla","","","","99999999","","","","","");
INSERT INTO accounts VALUES("118","110","37","AKASH","0","Credit","0","2016-09-05","22","4","2","","AAA","AA","123456","0","123","123","AKHILKP496@GMAIL.COM","AAA\n\nASS","123456","AA","");
INSERT INTO accounts VALUES("119","117","37","HYDER","0","Credit","0","2016-09-05","18","4","2","","HYDER","KERALA","673638","CALICUT","9999999","0","hydermkm@gmail.com","KERALA","673638","CALICUT","");
INSERT INTO accounts VALUES("123","79","37","MOHAMMED ISMAIL","0","Debit","0","2016-09-06","8","1","2","","MOHAMMED ISMAIL","ABDUL HAFEEZE","000000","0","7598109313","07418443464","MTMVISA@GMAIL.COM","WC","000000","WC","");
INSERT INTO accounts VALUES("124","79","37","MUHAMMED KUTTIKKA E","0","Credit","0","2016-09-06","5","1","2","","Muhammed Kuttika","edavannapara","673004","edavannapara","9446768538","9446768538","mtmrecruits@gmail.com","Edavannapara","673004","malappuram","");
INSERT INTO accounts VALUES("125","79","40","counter16","0","Credit","0","2016-09-06","5","1","2","","","","","","04952723831","04952723831","","","","","");
INSERT INTO accounts VALUES("126","80","37","SALEEM AAPARAMBIL","0","Credit","0","2016-09-06","6","2","2","","MANU","MTM","676510","RANDATHANI","00971563883556","00971563883556","seliapm@gmail.com","Abu dhabi","000000","Abudhabi","");
INSERT INTO accounts VALUES("127","79","40","ALTA CCJ","750","Credit","0","2016-09-09","10","1","2","","JAMSHAD","CD TOWER,KOZHIKODE","000000","KOZHIKODE","0495405052","9746084777","info@altatravel.in","CD TOWER,KOZHIKODE","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("128","80","37","SIDHEEQUE PUTHUPARAMBIL","0","Credit","0","2016-09-06","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","00971558848321","00971558848321","sidheeque321@gmail.com","RANDATHANI","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("129","79","37","FLYHIND THALASSERY","0","Debit","0","2016-09-06","9","1","2","","LANCHIMA","Sajad Tower , \nOpp: Old Bus Stand,\n\nHospital road , \n\nThalassery 673 101\n\nKerala, India.","673004","THALASSERY","9746744748","9746744746","flyhindtly@gmail.cpm","Sajad Tower , \nOpp: Old Bus Stand,\n\nHospital road , \n\nThalassery 673 101\n\nKerala, India.","673004","thalssery","");
INSERT INTO accounts VALUES("130","79","37","MAUSIM TRAVELS TIRUR","0","Debit","0","2016-09-06","9","1","2","","RUMANA","Mausim Travel and Tours Pvt Ltd |\n\nCity Tower | City Jn |  Tirur | Malappuram Dt | Kerala - INDIA\n\nTel + 91 494 654 1666 | +91 494 654 2666 | Mob  + 91 9288241666 | + 91 9497221666 |","673005","TIRUR","9497221666","9497221666","visa@mausim.in","Mausim Travel and Tours Pvt Ltd |\n\nCity Tower | City Jn |  Tirur | Malappuram Dt | Kerala - INDIA\n\nTel + 91 494 654 1666 | +91 494 654 2666 | Mob  + 91 9288241666 | + 91 9497221666 |","673005","tirur","");
INSERT INTO accounts VALUES("131","0","48","SAFUVAN","0","Credit","0","2016-09-06","2","1","2","","SAFUVAN","AIKARAPPADI","","","","9995959734","","","","","");
INSERT INTO accounts VALUES("132","79","40","ALPHA TOURS AND TRAVELS","0","Debit","0","2016-09-07","9","1","2","","SALAMKKA","Alpha Travel & Tours\n\nMob – +919400655946 / office – +914952435946\nE-Mail – visa@alphaindia.biz sales@alphaindia.biz alphatravels222@gmail.com","673005","CALICUT","9400655946","9400655946","","","","","");
INSERT INTO accounts VALUES("133","79","12","FEDERAL BANK","0","Debit","0","2016-09-07","2","1","2","","MUHAMMED","regd.ofc:Aluva Branch:Puthiyara IFSC:FDRL0001306","673004","KOZHIKODE","04952720888","9895925503","pya@fderalbank.co.in","regd.ofc:Aluva Branch:Puthiyara IFSC:FDRL0001306","673004","KOZHIKODE","12#AC1133");
INSERT INTO accounts VALUES("134","79","12","SOUTH INDIAN BANK","0","Debit","0","2016-09-07","2","1","2","","BOBAN V.J.","Kozhikode (nri) Branch,5 3410h4,galleria Trade Centre,near Markaz Complex, Kozhikode, Kerala, Pin-673004","673004","KOZHIKODE","495-2720588","495-2720588","CUSTOMERCARE@SIB.CO.IN","Kozhikode (nri) Branch,5 3410h4,galleria Trade Centre,near Markaz Complex, Kozhikode, Kerala, Pin-673004","673004","KOZHIKODE","12#AC1134");
INSERT INTO accounts VALUES("135","79","12","STATE BANK","0","Debit","0","2016-09-07","2","1","2","","","","","","","","","","","","12#AC1135");
INSERT INTO accounts VALUES("136","79","37","HANEEFA","0","Debit","0","2016-09-07","9","1","2","","MUHAMMED HANEEF","BLUE BERRY INTERIOR DESIGNS","673004","DUBAI","971527433307","971 4 2640240","haneefa@blueberryinterior.com","dubai","673001","DUBAI","");
INSERT INTO accounts VALUES("138","79","37","MTT TRAVELS","0","Debit","0","2016-09-07","10","1","2","","X","MTT Holidays Calicut\n\n1st Floor, Vyaparabhavan,Bank Road, Calicut- 673001","000000","KOZHIKODE","0495-3062656","9349384858/702506002","info@mttholidays.com","MTT Holidays Calicut\n\n1st Floor, Vyaparabhavan,Bank Road, Calicut- 67300","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("139","80","37","ABOOBACKER KOORIPPARAMBIL","0","Credit","0","2016-09-07","6","2","2","","MANU","AA","676510","ABU DHBAI","00971503287504","00971503287504","a.backer125@gmail.com","ABUDHABI","000000","ABUDHABI","");
INSERT INTO accounts VALUES("140","79","40","GULF INDIA TRAVELS","0","Debit","0","2016-09-08","9","1","2","","NAVAS","Muhammed Navas | +974 70301006 DOHA | +91 70 255 666 00\n\nGulf Air Travels | Pattambi, Palakkad, Kerala +91 466 2213430 | Cochin, Kerala +91 484 4070430","673004","","","484 4070430","","","","","");
INSERT INTO accounts VALUES("141","79","37","LAWASON TRAVELS COCHIN","0","Debit","0","2016-09-08","9","1","2","","SHIJI","SHIJI\nVisa Consultant\n\n39/6842,Lawson Plaza, Ravipuram, M.G.Road, Kochi - 682015.\n\nT  :  91-484-4091000","673305","KOCHIN","04844091000","04844091000","visa.cok@lawsononline.com","visa.cok@lawsononline.com","673004","cochin","");
INSERT INTO accounts VALUES("142","79","37","SAHAD STAFF","0","Debit","0","2016-09-08","9","1","2","","SAHD","ALIYAMINTAKATH HOUSE \nPALAPPETTY","679598","PALAPPETTY","9567656784","7736538597","mtmsahad@gmail.com","vadakkepurath house","689579","palappetty","");
INSERT INTO accounts VALUES("143","79","37","ALI PATTANMARU VALAPPIL","0","Debit","0","2016-09-08","10","1","2","","ALIL","AMBALA VAYAYAL","673593","KOZHIKODE","00","000","MUHAMMEDUNAISPM007@GMAIL.COM","AMBALA VAYAYAL,WAYANDU","673593","WAYANAD","");
INSERT INTO accounts VALUES("144","79","37","AHMED ORMUNDAKKA","0","Debit","0","2016-09-08","10","1","2","","AHMED ORMUNDAKKA","THARUVANA,KARANTHUR,KUNNAMANGALAM,KOZHIKODE","673571","KOZHIKODE","9400501168","9400501168","markaztravelmart@gmail.com","THARUVANA,KARANTHUR,KUNNAMANGALAM,KOZHIKODE","673571","KOZHIKODE","");
INSERT INTO accounts VALUES("145","79","37","RIYAZ MKC","0","Debit","0","2016-09-08","9","1","2","","RIYAZ","Mohamed Riyas A.A\nCEO Office Manager\nMarkaz Knowledge City\nwww.markazcity.com\n09544712226","673004","CALICUT","9544712226","9544712226","riyascalicut2@gmail.com","riyascalicut2@gmail.com","673004","CALICUT","");
INSERT INTO accounts VALUES("146","79","40","MARKAZ TRAVEL MART","0","Debit","0","2016-09-08","10","1","2","","SIDHEEK","MARKAZ COMPLEX ,KOZHIKODE","000000","KOZHIKODE","04954011515","04954011414","markaztravelmart@gmail.com","MARKAZ COMPLEX ,KOZHIKODE","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("147","79","40","SHAMNAS PURCHASE","1450","Debit","0","2016-09-08","10","1","2","","SHAMNAS","SHANAS","000000","KOZHIKODE","0495 4050502","9895090917","anawallahtravel@gmail.com","SHAMNAS","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("148","79","37","mohammed","0","Debit","0","2016-09-08","8","1","2","","mohammed","dervekshan","682551","lakshadeep","9447235706","9447235706","MTMVISA@GMAIL.COM","lakshadeep","000000","lakshadeep","");
INSERT INTO accounts VALUES("149","131","37","fathima travels","0","Debit","0","2016-09-08","8","1","2","","GAFOOR","THAMARASSERY","000000","THAMARASSERY","9447275477","9447275477","MTMVISA@GMAIL.COM","THAMARASSERY","000000","THAMARASSERY","");
INSERT INTO accounts VALUES("150","80","37","ABDUL KAREEM VALIYA VALAPPIL","0","Credit","0","2016-09-09","6","2","2","","MANU","PAVITTAPPURAM","000000","ABU DHBAI","00971504913624","9995122177","kareem_va@yahoo.in","ABUDHABI","000000","ABUDHABI","");
INSERT INTO accounts VALUES("151","79","37","Walking Passenger","0","Credit","0","2016-09-09","5","1","2","","Walking","walking","673004","kozhikode","0495","0495","tssidheeq@gmail.com","walking","673004","kozhkode","");
INSERT INTO accounts VALUES("152","131","37","SALALAH TRAVELS","0","Debit","0","2016-09-10","8","1","2","","HANEEFA","HANEEFA","000000","HANEEFA","9844840707","9535500007","MTMVISA@GMAIL.COM","HANEEFA","000000","HANEEFA","");
INSERT INTO accounts VALUES("153","131","37","APPLO TRAVELS","0","Debit","0","2016-09-10","8","1","2","","MUJEEB RAHMAN","KANJHANGAD","000000","KANJHANGAD","9447243744","9447243744","MTMVISA@GMAIL.COM","KANJHANGAD","000000","KANJHANGAD","");
INSERT INTO accounts VALUES("154","80","37","CHEMMUKKAN/MOHAMED SHEHEER","0","Credit","0","2016-09-10","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","9995795950","00971506656949","sheheer.sai@gmail.com","ABU DHABI","000000","ABUDHABI","");
INSERT INTO accounts VALUES("155","0","48","SALERY","0","Credit","0","2016-09-10","33","1","2","","SALERY","00","","","","000","","","","","");
INSERT INTO accounts VALUES("156","80","37","SAMEER - Akshaya pookiparambe","0","Credit","0","2016-09-10","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","9207999932","9846999932","aksmpm250@gmail.com","Akshaya pookiparambe","000000","pookiparambe","");
INSERT INTO accounts VALUES("157","80","40","SALEEM  - AUH","0","Credit","0","2016-09-14","6","2","2","","SALEEM","ABU DHABI","000000","ABU DHBAI","","0563883556","seliapm@gmail.com","saleem almanar tràvels\nABU DHABI","","","");
INSERT INTO accounts VALUES("158","0","48","NONE","0","Credit","0","2016-09-15","34","3","2","","NONE","0","","","","00000","","","","","");
INSERT INTO accounts VALUES("159","158","40","MTM VETTICHIRA","0","Credit","0","2016-09-15","34","3","2","","MANU","","","","","9544306950","","","","","");
INSERT INTO accounts VALUES("160","158","40","MTM KZKD","0","Credit","0","2016-09-15","34","3","2","","","","","","","04954011515","","","","","");
INSERT INTO accounts VALUES("161","158","40","NOT FIXED","0","Credit","0","2016-09-15","34","3","2","","","","","","","0488","","","","","");
INSERT INTO accounts VALUES("162","158","37","WALIKING CUSTOMER","0","Credit","0","2016-09-15","34","3","2","","WALIKING CUSTOMER","0","000000","PATB","0488","00","SAIFUINBOX@GMAIL.COM","0","000000","0","");
INSERT INTO accounts VALUES("163","158","12","FEDERAL BANK","0","Credit","0","2016-09-15","35","3","2","","","","","","","","","","","","12#AC3163");
INSERT INTO accounts VALUES("164","79","37","FAISAL STAFF","0","Debit","0","2016-09-16","9","1","2","","FAISAL","MARKAZ TRAVEEL MART","673004","KOZHIKODE","9605198598","9605198598","faizaltravelmart@gmail.com","markaz travel mart","673004","kozhikode","");
INSERT INTO accounts VALUES("165","79","37","MOULAVI TOURS AND TRAVELS","0","Debit","0","2016-09-16","9","1","2","","NOUFAL","MOULAVI TOURS AND TRAVELS\n\n 1st floor ,\n\nvyaparabhavan,near airindia city office \n\nBank Road, Kozhikode-673001\n\n0495-4054333,331 .+91 9567 864089,84,86.","673004","CALICUT","9567 864089","9567 864089","visa.moulavi@gmail.com","9567 864089","673004","kozhikode","");
INSERT INTO accounts VALUES("166","80","37","ANWAR  PALLIKKALAKATH","0","Credit","0","2016-09-17","6","2","2","","MANU","VETTICHIRA","676552","VETTICHIRA","00971555641255","9946577886","anwararabian157@gmail.com","DUBAI","676552","DUBAI","");
INSERT INTO accounts VALUES("167","80","37","ABDUL LATHEEF ( PAPPAYI )","0","Credit","0","2016-09-17","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","9544122199","00971508218343","latheefpappayi@gmail.com","DUBAI","000000","DUBAI","");
INSERT INTO accounts VALUES("168","80","37","MUHAMMED AFSAL CHEMMALA ( VENGARA)","0","Credit","0","2016-09-17","6","2","2","","MANU","VENGARA","000000","VENGARA","00919747255774","9747255774","cholayilparambil@gmail.com","ABU DHABI","000000","ABUDHABI","");
INSERT INTO accounts VALUES("169","79","37","NOORUL HAYATH TOURS AND TRAVELS","0","Debit","0","2016-09-17","9","1","2","","SALAH","Nooralhayat tours&travels\n\nEmarald mall ,mavoorroad","673004","CALICUT","91-0495 4060662  606","91-0495 4060662  606","visa@nooralhayattravels.com","Nooralhayat tours&travels\n\nEmarald mall ,mavoorroad","673004","CALICUT","");
INSERT INTO accounts VALUES("170","79","37","NIHAL TOURS AND TRAVELS","0","Debit","0","2016-09-17","9","1","2","","RANIYA","TIRUR","673004","TIRUR","9526940606","9526940505","NIHALCCJ@GMAIL.COM","KOOTTAYI","673300","TIRUR","");
INSERT INTO accounts VALUES("171","79","37","MTM PATTAMBI","0","Debit","0","2016-09-19","9","1","2","","SAIFU","MARKAZ TRAVEL MART BR","673004","PATTAMBI","04662214545","04662214545","mtmvisa@gmail.com","mark","673004","pattambi","");
INSERT INTO accounts VALUES("172","79","40","Al Ojoor","0","Credit","0","2016-09-19","36","1","2","","0000","0","954681","makka","056485654","056485654","","","","","");
INSERT INTO accounts VALUES("173","80","37","MOIDEEN  HAJI AYAPPALLI","0","Credit","0","2016-09-19","6","2","2","","MOIDEEN","KANMANAM","000000","KANMANAM","9526909093","00919526909093","mtmvettichira@gmail.com","KANMANAM","000000","AAAAAAAAA","");
INSERT INTO accounts VALUES("174","80","37","JURAIGE PANDARATHIL","0","Credit","0","2016-09-19","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","00971508329913","00971568687142","juraigep123@gmail.com","AAAA","000000","AAAAAA","");
INSERT INTO accounts VALUES("175","79","40","ABBAS HAJI","0","Credit","0","2016-09-19","36","1","2","","","","","","","056486556","","","","","");
INSERT INTO accounts VALUES("176","0","48","ASHRAF SAQUAFI SYS","0","Credit","0","2016-09-19","2","1","2","","ASHRAF SAQUAFI SYS","SYS DISTRICT OFFICE","","","","9961953870","","","","","");
INSERT INTO accounts VALUES("177","176","37","MUHAMMED KOYA","0","Credit","0","2016-09-19","36","1","2","","MUHAMMED KOYA","PALATTU THAZHAM","673020","KOZHIKODE","9847339784","8089505027","sirajsaquafi@gmail.com","NIL","673020","NIL","");
INSERT INTO accounts VALUES("178","158","37","VAKAYIL MUHAMMED/ALI AKBER","0","Credit","0","2016-09-19","35","3","2","","VAKAYIL MUHAMMED/ALI AKBER","PATTAMBI","000000","PATTAMBI","00918281780509","8281780509","mtmpattambi@gmail.com","PATTAMBI","000000","PATTAMBI","");
INSERT INTO accounts VALUES("181","79","44","COURIER EXP","0","Credit","0","2016-09-19","33","1","2","","","","","","4016363","4016363000","","","","","44#AC1181");
INSERT INTO accounts VALUES("182","79","57","DELHI NEW OFFICE EXP","0","Debit","0","2016-09-19","33","1","2","","","","","","","00917838894905","","","","","57#AC1182");
INSERT INTO accounts VALUES("183","80","37","KATTIL/HUSSAIN - C/o Saqafi","0","Credit","0","2016-09-20","6","2","2","","SAQAFI","AAAAA","000000","VETTICHIRA","0091 9847951033","9847951033","mtmvettichira@gmail.com","AAAAAA","000000","VETTICHIRA","");
INSERT INTO accounts VALUES("184","80","37","(MANU) MOHAMMED CHOORAPPULAKKAL","0","Credit","0","2016-09-20","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","0091 9048612159","9048612159","mtmvettichira@gmail.com","AAAAAA","676510","AAAAAA","");
INSERT INTO accounts VALUES("185","79","12","MARKAZ TRAVEL MART BANK","0","Credit","0","2016-09-20","10","1","2","","SIDHEEK","MARKAZ COMPLEX","000000","0","0495 4011414","900000000","markaztravelmart@gmail.com","MMARKAZ COMPLEX","000000","KOZHIKODE","12#AC1185");
INSERT INTO accounts VALUES("186","79","37","AZEEZ NARACHUMMANTAKATH","0","Credit","0","2016-09-20","10","1","2","","AZEEZ","KOYILANDI","000000","KOZHIKODE","9048290366","96646672346","Aabdulazeez.nv@gmail.com","KOYILAND","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("187","79","37","NAUSHAD ATTARWALA","0","Debit","0","2016-09-20","10","1","2","","NAUSHAD","KOZHIKODE","000000","KOZHIKODE","000888887","90010000","markaztravelmart@gmail.com","KOZHIKODE","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("188","79","37","HYDERALI","0","Debit","0","2016-09-20","10","1","2","","HYDER","MUNDAKKULAM","000000","KONDOTTY","9562706022","9562706022","MTMHYDER@GMAIL.COM","KONDOTY","000000","MALAPPURAM","");
INSERT INTO accounts VALUES("189","79","37","IRFAD","0","Debit","0","2016-09-20","10","1","2","","IRFAD","POONOOR","673574","KOZHIKODE","9567676714","9746915105","mtmirfad@gmail.com","poonoor","673574","KOZHIKODE","");
INSERT INTO accounts VALUES("190","79","40","INDO ARAB CCJ","0","Credit","0","2016-09-20","10","1","2","","HARIS","DARUSSALAM COMPLEX,MAVOOR ROAD","000000","KOZHIKODE","90020000","922145522","harisindoarab@gmail.com","KOZHIKODE","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("191","0","48","none","0","Credit","0","2016-09-20","38","11","2","","none","0","","","","0","","","","","");
INSERT INTO accounts VALUES("192","191","15","umra16","0","Credit","0","2016-09-20","38","11","2","","","","","","","","","","","","15#AC11192");
INSERT INTO accounts VALUES("193","191","15","mtm","0","Credit","0","2016-09-20","38","11","2","","","","","","","7","","","","","15#AC11193");
INSERT INTO accounts VALUES("194","79","37","SIRAJ SAQAFI","0","Debit","0","2016-09-20","10","1","2","","SIRAJ SAQAFI","VADAKARA","000000","KOZHIKODE","049222100","982224001","markaztravelmart@gmail.com","VADAKARA","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("195","131","37","SANA","0","Debit","0","2016-09-20","8","1","2","","SAMAD BHAI","AREEKKAD","000000","AREEKKAD","9446411062","9446411062","MTMVISA@GMAIL.COM","AREEKKAD","000000","AREEKKAD","");
INSERT INTO accounts VALUES("196","131","37","INDO ARAB CALICUT","0","Debit","0","2016-09-20","8","1","2","","ANWAR","CALICUT","000000","CALICUT","04954011244","4011244","MTMVISA@GMAIL.COM","CALICUT","000000","CALICUT","");
INSERT INTO accounts VALUES("197","191","8","visa mtm","0","Credit","0","2016-09-20","38","11","2","","","","","","099","08","","","","","8#AC11197");
INSERT INTO accounts VALUES("198","80","40","AKSHAYA PUTHANATHANI","0","Credit","0","2016-09-21","6","2","2","","","","","PUTHANATHANI","","9747655512","aksmpm142@gmail.com","","","","");
INSERT INTO accounts VALUES("199","80","37","SULAIMAN  KP ( KARIPPOL)","0","Credit","0","2016-09-21","6","2","2","","MANU","KARIPPOL","676552","KARIPPOL","00971502685929","71 502685929","mtmvettichira@gmail.com","VETTICHIRA","676552","VETTICHIRA","");
INSERT INTO accounts VALUES("200","80","31","KSEB","0","Credit","0","2016-09-21","6","2","2","","","","","PUTHANATHANI","0494 2615115","","","","","","31#AC2200");
INSERT INTO accounts VALUES("201","155","52","safuvan salary","0","Debit","0","2016-09-21","33","1","2","","","","","","","0000000000","","","","","52#AC1201");
INSERT INTO accounts VALUES("202","155","52","sahad salary","0","Debit","0","2016-09-21","33","1","2","","","","","","","0000.000000","","","","","52#AC1202");
INSERT INTO accounts VALUES("203","79","52","IRFAD SALERY","0","Credit","0","2016-09-21","33","1","2","","","","","","","000..000000","","","","","52#AC1203");
INSERT INTO accounts VALUES("204","79","25","BODHI SOLUTIION","0","Debit","0","2016-09-21","33","1","2","","","","","","","0000.00..0.0","","","","","25#AC1204");
INSERT INTO accounts VALUES("205","80","29","SIRAJ  Newspaper","0","Credit","0","2016-09-21","6","2","2","","RAFI  PONMUNDAM","","","","","9946158607","sirajprintx@gmail.com","","","","29#AC2205");
INSERT INTO accounts VALUES("206","80","35","BSNL","0","Credit","0","2016-09-21","6","2","2","","","KADAMPUZA","676552","","0494 2617000","2617000","","","","","35#AC2206");
INSERT INTO accounts VALUES("207","80","34","taxi and other exp","0","Credit","0","2016-09-21","6","2","2","","","","","","000000000","0000000000","","","","","34#AC2207");
INSERT INTO accounts VALUES("208","79","37","HAMEED","0","Debit","0","2016-09-21","10","1","2","","HAMEED","PERAMBRA","673725","KOZHIKODE","8606352145","9645422409","markaztravelmart@gmail.com","PERAMBRA","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("209","79","37","YOONUS SOUTHERN","0","Debit","0","2016-09-22","9","1","2","","YOONUS","SouthernAlliance International Tours & Travels\n\nmajalan building,jaferkhan colony road\n\ncalicut\n\ncalicut-673006.","673006","CALICUT","9895317613","9895317613","smarif1314@gmail.com","SouthernAlliance International Tours & Travels\n\nmajalan building,jaferkhan colony road\n\ncalicut\n\ncalicut-673006.","673006","CALICUT","");
INSERT INTO accounts VALUES("210","80","37","NOUFI  CHEMMUKKAN","0","Credit","0","2016-09-22","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","00971 55 1004103","9516 868686","noufi.chip@gmail.com","RANDATHANI","676510","RANDATHANI","");
INSERT INTO accounts VALUES("211","80","37","ISMAIL PARAMBATH","0","Credit","0","2016-09-22","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","9645449720","9526738216","mtmvettichira@gmail.com","RANDATHANI","676510","RANDATHANI","");
INSERT INTO accounts VALUES("212","79","37","counter 16 purchase","0","Credit","0","2016-09-22","10","1","2","","xx","xxx","000000","KOZHIKODE","9720000000","9886556222","markaztravelmart@gmail.com","xxx","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("213","79","13","fake note","0","Credit","0","2016-09-22","33","1","2","","","","","","","0000000..","","","","","13#AC1213");
INSERT INTO accounts VALUES("214","79","15","Cash In Hand with Manager","0","Credit","0","2016-09-22","5","1","2","","","","","","","888","","","","","15#AC1214");

DROP TABLE IF EXISTS airline_flight;
CREATE TABLE `airline_flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlinePassId` int(11) NOT NULL,
  `airlineName` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `journeyDate` date NOT NULL,
  `sectorFrom` varchar(100) NOT NULL,
  `sectorTo` varchar(100) NOT NULL,
  `classType` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `tax` varchar(30) NOT NULL,
  `yq` float NOT NULL,
  `procCharge` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlinePassId` (`airlinePassId`),
  CONSTRAINT `airline_flight_ibfk_2` FOREIGN KEY (`airlinePassId`) REFERENCES `airline_passenger` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
INSERT INTO airline_flight VALUES("1","1","2","IX - 348","NHI1RD","2016-09-16","AUH","CCJ","Y","1000","0","0","0","2","6","2");
INSERT INTO airline_flight VALUES("2","2","3","EY398/254","607-9164761434","2016-12-07","DOH","CCJ","Y","11776","2239","0","35","2","6","2");
INSERT INTO airline_flight VALUES("3","3","2","IX346","AEDKL6","2016-09-05","DXB","CCJ","Y","14730","0","0","270","2","6","2");
INSERT INTO airline_flight VALUES("4","4","1","AI993","098-9164977278","2016-12-22","CCJ","DXB","Y","1471","834","6775","420","2","6","2");
INSERT INTO airline_flight VALUES("7","7","13","FD171","CW3YVB","2016-10-17","COK","DMK","Y","8870","0","0","730","2","6","2");
INSERT INTO airline_flight VALUES("8","7","13","FD170","CW3YVB","2016-10-19","DMK","COK","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("10","9","1","AI998","098-9164977299","2016-09-05","SHJ","CCJ","Y","2370","732","6028","570","2","6","2");
INSERT INTO airline_flight VALUES("12","11","3","EY257","607-9164977330","2016-10-04","CCJ","AUH","Y","5006","1019","4025","250","2","6","2");
INSERT INTO airline_flight VALUES("13","12","3","EY257","607-9164977328","2016-10-04","CCJ","AUH","Y","5006","1019","4025","250","2","6","2");
INSERT INTO airline_flight VALUES("14","13","12","9W207","589-9164977329","2017-01-01","SHJ","CCJ","Y","2191","2175","4019","315","2","6","2");
INSERT INTO airline_flight VALUES("29","23","1","AI582","098-9164977343","2016-09-04","CCJ","BOM","Y","11024","3092","7714","470","2","6","2");
INSERT INTO airline_flight VALUES("30","23","1","AI921","098-9164977343","2016-09-04","BOM","RUH","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("31","24","1","AI582","098-9164977344","2016-09-04","CCJ","BOM","Y","11024","3092","7714","470","2","6","2");
INSERT INTO airline_flight VALUES("32","24","1","AI921","098-9164977344","2016-09-04","BOM","RUH","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("33","25","1","AI582","098 9164977342","2016-09-04","CCJ","BOM","Y","12866","3205","7714","715","2","6","2");
INSERT INTO airline_flight VALUES("34","25","1","AI921","098 9164977342","2016-09-04","BOM","RUH","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("35","26","1","AI582","098-9164977341","2016-09-04","CCJ","BOM","Y","12979","3092","7714","715","2","6","2");
INSERT INTO airline_flight VALUES("36","26","1","AI921","098-9164977341","2016-09-04","BOM","RUH","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("38","28","2","IX350","X7IPF7","2016-10-13","CCJ","MCT","Y","1300","0","0","500","2","6","2");
INSERT INTO airline_flight VALUES("39","29","","SG53","06665879224","2016-09-08","CCJ","DXB","Y","500","","0","0","4","18","2");
INSERT INTO airline_flight VALUES("40","30","","EY","456456","2016-09-06","CCJ","DXB","","15000","","0","100","4","22","2");
INSERT INTO airline_flight VALUES("41","31","2","IX - 538","P1YKWK","2016-09-06","AUH","TRV","Y","20830","0","0","170","2","6","2");
INSERT INTO airline_flight VALUES("42","31","2","IX - 537","WP4IO2","2016-09-25","TRV","AUH","Y","11390","0","0","110","2","6","2");
INSERT INTO airline_flight VALUES("43","32","1","AI998","098-9164977437","2016-09-07","SHJ","CCJ","Y","12426","2092","6727","1255","2","6","2");
INSERT INTO airline_flight VALUES("44","33","2","IX344","0B134I","2016-12-16","DXB","CCJ","Y","126500","","0","2500","2","6","2");
INSERT INTO airline_flight VALUES("45","33","2","IX343","0B134I","2017-01-07","CCJ","DXB","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("46","34","7","QR539","157-9164977454","2016-09-08","CCJ","DOH","N","12097","1661","4172","470","2","6","2");
INSERT INTO airline_flight VALUES("47","34","7","QR1046","157-9164977454","2016-09-08","DOH","AUH","N","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("48","35","7","","157-9164977450","2016-09-08","CCJ","DOH","S","400","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("49","35","7","","","2016-09-08","DOH","AUH","S","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("50","36","2","IX373","M7AS4L","2016-09-11","CCJ","DOH","Y","24520","0","0","280","2","6","2");
INSERT INTO airline_flight VALUES("51","37","2","IX363","CCSTFD","2016-09-16","CCJ","AUH","Y","67030","0","0","970","2","6","2");
INSERT INTO airline_flight VALUES("52","38","2","IX363","","2016-09-16","CCJ","AUH","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("53","39","2","AI997","098-9165446755","2016-09-20","CCJ","SHJ","Y","9481","1335","6724","960","2","6","2");
INSERT INTO airline_flight VALUES("54","40","1","AI582","098-9165446759","2016-09-20","CCJ","BOM","K","6857","1169","6724","750","2","6","2");
INSERT INTO airline_flight VALUES("55","40","1","AI945","098-9165446759","2016-09-20","BOM","AUH","L","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("56","41","2","IX351","BSZ6TZ","2016-09-16","CCJ","SHJ","Y","29260","0","0","140","2","6","2");
INSERT INTO airline_flight VALUES("57","42","16","682","098 9165446922","2016-09-20","COK","AUH","E","12600","","0","500","3","35","2");
INSERT INTO airline_flight VALUES("61","46","10","SG53","I6073692","2016-10-18","CCJ","DXB","Y","5100","0","0","200","2","6","2");
INSERT INTO airline_flight VALUES("62","47","2","IX346","UPQB5X","2016-09-21","DXB","CCJ","Y","13170","0","0","830","2","6","2");
INSERT INTO airline_flight VALUES("63","48","2","IX363","74LUSH","2016-09-24","CCJ","AUH","Y","10200","0","0","400","2","6","2");
INSERT INTO airline_flight VALUES("64","49","1","AI937","098-9165774873","2016-09-21","CCJ","DXB","G","5665","1099","6771","1265","2","6","2");
INSERT INTO airline_flight VALUES("65","50","2","IX348","AUKPRG","2016-09-22","AUH","CCJ","Y","23980","0","0","1020","2","6","2");
INSERT INTO airline_flight VALUES("66","50","2","IX351","AUKPRG","2016-09-24","CCJ","SHJ","Y","0","0","0","0","2","6","2");
INSERT INTO airline_flight VALUES("67","51","1","AI937","098-9165774899","2016-09-21","CCJ","DXB","G","7575","1219","6771","935","2","6","2");
INSERT INTO airline_flight VALUES("68","52","","9W534","589-9165774925","2016-09-29","COK","MCT","Y","9156","1161","4023","260","3","35","2");
INSERT INTO airline_flight VALUES("69","53","12","9W596","589-9165774938","2016-09-05","CCJ","DOH","O","10081","1371","4023","325","2","6","2");
INSERT INTO airline_flight VALUES("70","54","2","IX395","3XAZ47","2016-09-21","CCJ","KWI","Y","23810","0","0","190","2","6","2");
INSERT INTO airline_flight VALUES("71","55","4","EK533","176-9165944010","2016-09-21","COK","DXB","W","17883","1982","4035","200","2","6","2");
INSERT INTO airline_flight VALUES("72","56","2","IX474","74YNNB","2016-09-20","CCJ","COK","Y","11210","0","0","290","2","6","2");
INSERT INTO airline_flight VALUES("73","57","2","IX363","QSTU3I","2016-10-13","CCJ","AUH","Y","3980","0","0","220","2","6","2");
INSERT INTO airline_flight VALUES("74","58","2","IX363","4KR1B7","2016-10-13","CCJ","AUH","Y","12250","0","0","450","2","6","2");
INSERT INTO airline_flight VALUES("75","59","2","IX345","VH1BIE","2016-09-27","CCJ","DXB","Y","14950","0","0","250","2","6","2");
INSERT INTO airline_flight VALUES("76","60","2","IX351","S1LUMS","2016-09-27","CCJ","SHJ","Y","15470","0","0","130","2","6","2");

DROP TABLE IF EXISTS airline_passenger;
CREATE TABLE `airline_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlineId` int(11) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationship` varchar(30) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `prefix` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlineId` (`airlineId`),
  CONSTRAINT `airline_passenger_ibfk_1` FOREIGN KEY (`airlineId`) REFERENCES `invoice_airline` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
INSERT INTO airline_passenger VALUES("1","1","Ittikaparambil Palakkal, Aboobackar","1978-01-10","39","M","CHAMRAVATTAM","","Indian","","G1284698","2008-02-10","2018-02-09","ABU DHABI","2","6","2","");
INSERT INTO airline_passenger VALUES("2","2","Thale Kunnath/jaseem","0000-00-00","0","M","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("3","3","Machincherithoombil/mohammed Sakeeb","1996-05-26","20","M","TIRUR","","Indian","","M8018076","2015-04-13","2025-04-12","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("4","4","Cheruvakkath/abdul Raheem","1960-01-23","57","M","VATTAPARAMBU","","Indian","","H0109737","2009-05-26","2019-05-25","DUBAI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("7","5","Cheruvakkath/abdul Raheem","1960-01-23","57","M","VATTAPARAMBU","","Indian","","H0109737","2009-05-26","2019-05-25","DUBAI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("9","7","Oduvil/ayyoob Mr","1979-03-10","38","M","10-03-1979","","Indian","","M7851287","2015-04-10","2025-04-09","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("11","8","Maliyekkal/muhammed Mukthar","1988-03-20","28","M","EDARIKODE","","Indian","","H0356591","2008-08-26","2018-08-25","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("12","8","Poovatil/juhana","1996-04-06","20","F","FEROKE","","Indian","","M2664637","2014-10-08","2024-10-07","MALAPPURAM","2","6","2","Mrs");
INSERT INTO airline_passenger VALUES("13","9","Poovatil/juhana","1996-04-06","20","F","FEROKE","","Indian","","M2664637","2014-10-08","2024-10-07","MALAPPURAM","2","6","2","Mrs");
INSERT INTO airline_passenger VALUES("23","11","Alqahtani/albandari Faisal","0000-00-00","0","F","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Ms");
INSERT INTO airline_passenger VALUES("24","11","Alqahtani/norah Faisal","0000-00-00","0","F","","","SAUDI","","","0000-00-00","0000-00-00","","2","6","2","Ms");
INSERT INTO airline_passenger VALUES("25","10","Alqahtani/renad Faisal","0000-00-00","0","","","","SAUDI","","","0000-00-00","0000-00-00","","2","6","2","Ms");
INSERT INTO airline_passenger VALUES("26","10","Alqahtani/qamraa Saad","0000-00-00","0","F","","","SAUDI","","","0000-00-00","0000-00-00","","2","6","2","Mrs");
INSERT INTO airline_passenger VALUES("28","13","Aboobacker Karingappara","0000-00-00","0","M","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("29","14","Riyas","0000-00-00","0","","","","Indian","","0","0000-00-00","0000-00-00","","4","18","2","Mr");
INSERT INTO airline_passenger VALUES("30","15","Aruncut","0000-00-00","0","","","","Indian","","AAAA","2016-09-13","2016-09-30","CALI","4","22","2","");
INSERT INTO airline_passenger VALUES("31","16","Suneer Majeed","0000-00-00","0","M","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("32","17","Sidheek Pookayil","1971-06-30","45","M","CHERUSHOLA","","Indian","","J2072600","2010-05-04","2020-05-03","ABUDHABI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("33","18","Machinchery Thoombil, Mohamed","0000-00-00","0","","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("34","19","Kunnathodi/abdul Jaleel","1979-01-01","38","M","POOVANCHINA","","Indian","","G7230920","2008-02-08","2018-02-07","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("35","20","Kunnathodi/abdul Jaleel","1979-01-01","38","M","POOVANCHINA","","Indian","","G7230920","2008-02-08","2018-02-07","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("36","12","Ainikkunnan Jayafer","1992-08-02","24","M","VETTICHIRA","","Indian","","M7297991","2015-03-19","2025-03-18","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("37","21","Valiya Valappil Ali Abdul Kareem","1967-05-01","49","M","PAVITTAPPURAM","","Indian","","K4621024","2012-03-19","2022-03-18","ABU DHABI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("38","21","Nadeera Abdul Kareem","1970-05-15","46","F","MANNALAMKUNNU","","Indian","","G5039690","2008-12-03","2018-12-02","ABUDHABI","2","6","2","Mrs");
INSERT INTO airline_passenger VALUES("39","22","Chemmukkan/mohamed Sheheer","1987-12-10","29","M","KOTTAKKAL","","Indian","","L5482407","2013-11-07","2023-11-06","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("40","23","Chemmukkil/mohamed Haneefa","1976-04-23","40","M","CHERUSHOLA","","Indian","","L1182314","2013-05-14","2023-05-13","ABUDHABI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("41","24","Pookayil/sidheek","1971-06-30","45","M","CHERUSHOLA","","Indian","","J2072600","2010-05-04","2020-05-03","ABU DHABI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("42","25","Ali Askar","1988-02-25","29","M","KERALA","","Indian","","N2455497","0000-00-00","0000-00-00","","3","35","2","Mr");
INSERT INTO airline_passenger VALUES("46","28","Anwar Pallikkalakath","1975-05-16","41","M","CHINNAMPADI","","Indian","","M6141348","2015-02-06","2025-02-05","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("47","29","Rasheed Narungali , Neenan Marakkar","1977-07-15","39","M","PERUMANNA","","Indian","","F9251602","2007-08-14","2017-08-13","ABUDHABI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("48","30","Muhammed Afsal Chemmala","1990-06-30","26","M","O K MURI","","Indian","","G4251068","2007-08-28","2017-06-27","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("49","31","Ayappalli Moideen","1960-05-16","56","M","KANMANAM","","Indian","","L3813768","2013-07-03","2023-06-02","DUBAI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("50","32","Pandarathil Juraige","1986-08-22","30","M","RANDATHANI","","Indian","","G5721484","2007-11-05","2017-11-04","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("51","33","Thalakottupuram/abdul Latheef","0000-00-00","0","M","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("52","34","Vakayil Muhammed/ali Akber","0000-00-00","0","M","","","Indian","","","0000-00-00","0000-00-00","","3","35","2","Mr");
INSERT INTO airline_passenger VALUES("53","35","Kattil/hussain","1978-02-01","39","M","chirakkal","","Indian","","M5493217","2015-01-14","2025-01-13","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("54","36","Mohammed Choorappulakkal","1973-05-30","43","M","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("55","37","Manakkadvakkathodi/abdulazeez","1990-11-13","26","M","PERINTALMANNA","","Indian","","K2511923","2012-01-19","2022-01-18","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("56","38","Mohammed Shaji Ponnanagathodi","0000-00-00","0","","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("57","39","Chemmukkan, Noufir","0000-00-00","0","M","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("58","40","Shamsiya Kadakkadan &  Inf","0000-00-00","0","F","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mrs");
INSERT INTO airline_passenger VALUES("59","41","Mohamed Saife Kalodi","0000-00-00","0","F","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("60","42","Ismail Parambath","1977-09-01","39","M","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");

DROP TABLE IF EXISTS cash_payment;
CREATE TABLE `cash_payment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `discount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=302 DEFAULT CHARSET=latin1;
INSERT INTO cash_payment VALUES("3","1","visa","22","15900","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("4","1","visa","23","15900","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("5","1","airline","1","1000","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("6","1","airline","3","14730","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("14","2","airline","2","14015","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("15","2","airline","4","9080","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("16","2","airline","5","8870","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("17","2","airline","6","9130","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("18","2","airline","7","20100","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("19","2","airline","8","8385","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("20","2","airline","9","47570","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("21","2","airline","10","43660","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("22","3","visa","1","15000","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("25","2","visa","86","8500","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("26","3","visa","36","5150","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("27","4","visa","18","5100","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("28","4","visa","19","100","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("29","5","visa","87","900","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("33","6","visa","77","5500","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("34","6","visa","78","7000","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("35","4","airline","11","24520","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("36","4","airline","12","1300","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("37","4","airline","13","32220","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("47","7","miscellaneous","1","1500","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("53","7","airline","15","30970","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("54","5","airline","14","21245","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("55","5","airline","16","17930","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("56","5","airline","17","400","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("57","5","airline","14","21245","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("58","5","airline","16","17930","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("59","5","airline","17","400","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("60","5","airline","14","42490","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("61","5","airline","16","35860","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("62","5","airline","17","800","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("129","8","visa","9","3300","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("187","8","airline","15","126500","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("188","9","airline","18","67030","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("189","7","visa","117","7400","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("190","7","visa","119","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("191","7","visa","121","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("192","7","visa","125","29200","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("193","7","visa","127","34000","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("194","6","visa","37","29600","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("195","6","visa","39","16000","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("196","6","visa","40","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("197","6","visa","41","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("198","6","visa","42","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("199","6","visa","43","29600","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("200","6","visa","44","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("201","6","visa","45","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("202","6","visa","48","44400","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("203","6","visa","50","29600","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("204","6","visa","52","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("205","6","visa","53","29600","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("206","6","visa","57","44400","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("207","6","visa","69","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("208","6","visa","84","44400","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("209","6","visa","100","15800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("210","6","visa","101","15800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("211","6","visa","107","15800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("212","6","visa","110","29600","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("213","6","visa","114","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("214","6","visa","115","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("215","6","visa","116","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("216","6","visa","117","7400","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("217","5","visa","68","1400","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("218","5","visa","79","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("219","5","visa","81","5100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("220","5","visa","82","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("221","5","visa","83","5100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("222","5","visa","103","5100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("223","5","visa","104","20400","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("224","5","visa","105","5100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("225","5","visa","108","14200","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("226","5","visa","109","1200","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("227","5","visa","111","14200","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("228","4","visa","46","183300","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("229","4","visa","49","14300","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("230","4","visa","51","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("231","4","visa","54","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("232","4","visa","55","28200","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("233","4","visa","56","5100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("234","4","visa","66","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("235","4","visa","67","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("236","4","visa","68","12700","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("237","3","visa","2","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("238","3","visa","3","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("239","3","visa","5","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("240","3","visa","6","5100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("241","3","visa","7","28200","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("242","3","visa","8","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("243","3","visa","10","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("244","3","visa","13","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("245","3","visa","19","200","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("246","3","visa","25","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("247","3","visa","26","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("248","3","visa","27","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("249","3","visa","28","300","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("250","3","visa","31","40800","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("251","3","visa","32","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("252","3","visa","35","14100","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("253","3","visa","38","28200","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("254","3","visa","47","28200","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("255","3","visa","49","13900","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("256","1","visa","102","78325","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("257","2","visa","62","6900","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("258","2","visa","63","4600","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("259","2","visa","64","2600","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("260","2","visa","65","14900","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("261","2","visa","70","7700","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("262","2","visa","72","14900","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("263","2","visa","73","900","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("264","2","visa","74","58400","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("265","13","visa","189","15000","0","","0","CASH","1","33","2");
INSERT INTO cash_payment VALUES("266","14","miscellaneous","2","15000","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("267","12","miscellaneous","1","1650","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("268","8","visa","111","14000","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("269","8","visa","112","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("270","8","visa","113","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("271","8","visa","118","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("272","8","visa","120","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("273","8","visa","122","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("274","8","visa","123","5100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("275","8","visa","124","28200","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("276","8","visa","126","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("277","8","visa","128","28200","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("278","8","visa","130","141000","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("279","8","visa","132","15300","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("280","8","visa","134","98700","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("281","8","visa","136","70500","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("282","8","visa","139","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("283","8","visa","141","5100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("284","8","visa","144","28200","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("285","8","visa","145","28200","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("286","8","visa","146","25500","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("287","8","visa","147","15300","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("288","8","visa","148","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("289","8","visa","149","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("290","8","visa","152","900","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("291","8","visa","154","300","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("292","8","visa","155","600","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("293","8","visa","156","600","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("294","8","visa","159","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("295","8","visa","160","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("296","8","visa","161","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("297","8","visa","163","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("298","8","visa","164","28200","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("299","8","visa","165","14100","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("300","8","visa","167","40600","0","","0","BANK","1","33","2");
INSERT INTO cash_payment VALUES("301","8","visa","168","28100","0","","0","BANK","1","33","2");

DROP TABLE IF EXISTS cash_receipt;
CREATE TABLE `cash_receipt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `discount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=latin1;
INSERT INTO cash_receipt VALUES("1","1","airline","4","9500","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("2","1","airline","5","9600","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("3","2","airline","12","1800","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("4","3","airline","11","24800","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("5","1","visa","22","5300","0","","0","CASH","1","5","2");
INSERT INTO cash_receipt VALUES("6","2","visa","18","5200","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("7","2","visa","19","300","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("10","5","visa","31","16500","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("11","6","visa","21","3700","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("12","7","visa","59","400","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("13","7","visa","61","14600","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("14","4","airline","2","14050","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("15","4","airline","9","49000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("16","4","airline","10","44600","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("18","6","visa","1","10000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("19","7","airline","1","1000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("20","5","airline","14","15000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("32","8","airline","14","7500","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("33","8","miscellaneous","1","1700","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("34","9","visa","82","16000","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("36","10","visa","86","40000","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("40","11","visa","94","3800","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("41","12","visa","37","5200","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("44","13","visa","100","7750","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("45","13","visa","101","7750","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("46","9","airline","13","32500","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("61","14","miscellaneous","2","24750","0","","0","CASH","1","5","2");
INSERT INTO cash_receipt VALUES("62","3","visa","23","5000","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("63","4","visa","24","16000","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("64","15","visa","23","12400","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("65","10","visa","4","300","0","","000","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("66","10","visa","105","5200","0","","000","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("67","10","visa","106","20800","0","","000","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("68","10","airline","20","15500","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("69","11","airline","7","20600","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("70","11","airline","8","8700","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("71","11","visa","3","17200","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("72","12","airline","18","68000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("73","13","airline","19","18500","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("74","1","airline","1","13100","0","CASH RCEIVED","0","CASH","3","35","2");
INSERT INTO cash_receipt VALUES("75","14","airline","22","2000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("90","15","airline","23","14000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("92","16","visa","86","5000","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("102","2","airline","2","14600","0","","0","CASH","3","35","2");
INSERT INTO cash_receipt VALUES("103","17","package","1","25000","0","Advance","0","CASH","1","36","2");
INSERT INTO cash_receipt VALUES("104","18","visa","129","50000","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("105","16","airline","25","14800","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("106","17","airline","21","25000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("107","19","visa","185","5800","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("108","18","airline","16","10000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("109","19","airline","27","15000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("110","20","airline","27","300","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("111","15","visa","5","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("112","15","visa","15","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("113","15","visa","43","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("114","15","visa","46","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("115","15","visa","48","29200","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("116","15","visa","55","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("117","15","visa","114","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("118","15","visa","123","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("119","15","visa","147","29200","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("121","13","visa","39","29600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("122","13","visa","40","16000","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("123","12","visa","33","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("124","12","visa","35","16200","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("125","12","visa","58","19000","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("126","11","visa","16","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("127","11","visa","26","10600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("128","11","visa","34","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("129","11","visa","38","29600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("130","11","visa","42","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("131","11","visa","44","15700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("132","14","visa","32","25000","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("133","20","visa","186","5800","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("134","21","visa","187","10000","0","","0","CASH","1","10","2");
INSERT INTO cash_receipt VALUES("135","21","airline","22","2000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("136","22","visa","112","30600","0","","0","CASH","1","33","2");
INSERT INTO cash_receipt VALUES("137","9","visa","104","78325","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("138","8","visa","92","50000","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("139","7","visa","2","28700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("140","7","visa","7","29200","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("141","7","visa","8","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("142","7","visa","10","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("143","7","visa","11","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("144","7","visa","12","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("145","7","visa","13","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("146","7","visa","17","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("147","7","visa","26","4000","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("148","7","visa","29","300","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("149","6","visa","70","15418","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("150","4","visa","28","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("151","4","visa","30","29600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("152","4","visa","32","5700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("153","3","visa","81","15000","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("154","2","visa","62","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("155","2","visa","63","6900","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("156","2","visa","64","4600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("157","2","visa","65","2600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("158","2","visa","66","14900","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("159","2","visa","71","7700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("160","2","visa","72","43800","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("161","2","visa","74","14900","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("162","2","visa","75","900","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("163","2","visa","76","26700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("164","1","visa","49","46123","0","DEPOSITED BY MUHAMMED KOTTATHAYIL  IN SOUTH INDIAN","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("165","16","visa","44","13700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("166","16","visa","47","189800","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("167","16","visa","50","29200","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("168","16","visa","51","29400","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("169","16","visa","56","29200","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("170","16","visa","68","8700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("171","23","visa","188","7800","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("172","24","visa","192","18000","0","","0","CASH","1","33","2");
INSERT INTO cash_receipt VALUES("173","25","visa","209","1000","0","","0","CASH","1","33","2");
INSERT INTO cash_receipt VALUES("174","22","airline","29","24000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("175","23","miscellaneous","1","1800","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("177","26","visa","236","3000","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("178","27","visa","94","2500","0","","0","CASH","1","33","2");
INSERT INTO cash_receipt VALUES("184","17","visa","25","15000","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("185","17","visa","27","15000","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("186","17","visa","36","15000","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("187","17","visa","91","300","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("188","17","visa","97","200","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("189","28","package","1","25000","0","","0","CASH","1","36","2");
INSERT INTO cash_receipt VALUES("190","18","visa","32","10100","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("191","18","visa","41","14700","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("192","18","visa","57","5100","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("193","18","visa","83","5100","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("194","18","visa","84","14700","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("195","18","visa","125","300","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("196","18","visa","141","0","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("197","29","visa","237","17400","0","","0","CASH","1","33","2");
INSERT INTO cash_receipt VALUES("198","19","visa","127","29000","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("199","19","visa","128","14500","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("200","19","visa","129","8000","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("201","19","visa","130","29000","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("202","19","visa","131","29000","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("203","19","visa","132","145000","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("204","19","visa","133","29000","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("205","19","visa","135","16500","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("206","19","visa","136","0","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("207","30","visa","238","2500","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("208","24","airline","32","4200","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("209","24","airline","33","12700","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("210","24","airline","34","15200","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("211","31","visa","252","22000","0","","0","CASH","1","33","2");
INSERT INTO cash_receipt VALUES("212","20","visa","208","14700","0","","0","BANK","1","33","2");
INSERT INTO cash_receipt VALUES("213","25","airline","26","25000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("214","21","visa","68","5900","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("215","21","visa","76","31700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("216","21","visa","77","88200","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("217","21","visa","111","1200","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("218","21","visa","113","29200","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("219","21","visa","115","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("220","21","visa","116","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("221","21","visa","117","12100","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("222","22","visa","164","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("223","22","visa","166","29200","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("224","22","visa","168","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("225","22","visa","173","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("226","22","visa","193","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("227","22","visa","198","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("228","22","visa","205","14800","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("229","22","visa","216","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("230","22","visa","218","6200","0","","0","BANK","1","15","2");

DROP TABLE IF EXISTS company;
CREATE TABLE `company` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `tds` int(11) NOT NULL,
  `circle` varchar(100) NOT NULL,
  `resPerson` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `remark` text NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
INSERT INTO company VALUES("1","Mtm Travel Mart Kzkd","","0","KOZHIKODE","0495 4","07736538597","mtmvisa@gmail.com","","0","","Sa ad","Manager","","2");
INSERT INTO company VALUES("2","Mtm Travel Mart - Vettichira","-VETTICHIRA,CHUNGAM- MALAPPURAM","0","VETTICHIRA","04942617140","09497141053","mtmvettichira@gmail.com","","0","","MANU MUHAMMED","MANAGER","","2");
INSERT INTO company VALUES("3","Mtm Travel Mart -ptb","1St floor, \nBank Building\nNear Court, Pattambi-","679303","PATTAMBI","04662214545","9846414143","mtmpattambi@gmail.com","","0","","SAIFUDHEEN","MANAGER","","2");
INSERT INTO company VALUES("4","Test 1","","000000","0","0","8547116314","markaztravelmart@gmail.com","","0","","","","","2");
INSERT INTO company VALUES("11","Tssidheeq-mtm","","673004","KOZHIKODE","0","0","tssidheeq@gmail.com","","0","","","","","2");

DROP TABLE IF EXISTS flight;
CREATE TABLE `flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `flightNo` varchar(50) NOT NULL,
  `flightName` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
INSERT INTO flight VALUES("1","0","Air India","","2","6","2");
INSERT INTO flight VALUES("2","0","Air India Express","","2","6","2");
INSERT INTO flight VALUES("3","0","Etihad Airways","","2","6","2");
INSERT INTO flight VALUES("4","0","Emirates  Airlines","","2","6","2");
INSERT INTO flight VALUES("5","0","Air Arabia","","2","6","2");
INSERT INTO flight VALUES("6","0","Saudi Arabian Airlines","","2","6","2");
INSERT INTO flight VALUES("7","0","Qatar Airways","","2","6","2");
INSERT INTO flight VALUES("8","0","Oman Air","","2","6","2");
INSERT INTO flight VALUES("9","0","Kuwait Airways","","2","6","2");
INSERT INTO flight VALUES("10","0","Spicejet","","2","6","2");
INSERT INTO flight VALUES("11","0","Indigo","","2","6","2");
INSERT INTO flight VALUES("12","0","Jet Airways","","2","6","2");
INSERT INTO flight VALUES("13","0","Thai  Airasia","","2","6","2");
INSERT INTO flight VALUES("14","0","Sv","","4","19","2");
INSERT INTO flight VALUES("15","0","Air India Express","","3","34","2");
INSERT INTO flight VALUES("16","0","Air India","","3","34","2");
INSERT INTO flight VALUES("17","0","Air Arabia","","3","34","2");
INSERT INTO flight VALUES("18","0","Spice Jet","","3","34","2");
INSERT INTO flight VALUES("19","0","Oman (wy)","","1","36","2");

DROP TABLE IF EXISTS hotel;
CREATE TABLE `hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(10) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
INSERT INTO hotel VALUES("1","Janadriyya","","000000","Makka","Soudi","Soudi","00000","00000000","","","4","19","2");
INSERT INTO hotel VALUES("5","Sample","","","","","India","454654643","","","","2","6","2");
INSERT INTO hotel VALUES("6","Janadiriyya","0","0","Makka","Soudi","Soudi","0000","0","sirajsaquafi@gmail.com","0","1","36","2");

DROP TABLE IF EXISTS hotel_customer;
CREATE TABLE `hotel_customer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelInvoiceId` int(11) NOT NULL,
  `roomType` varchar(20) NOT NULL,
  `noOfRooms` int(11) NOT NULL,
  `noOfPersons` int(11) NOT NULL,
  `rate` float NOT NULL,
  `subTax` varchar(30) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
INSERT INTO hotel_customer VALUES("1","1","1","2","3","3","","6","2","6","2");
INSERT INTO hotel_customer VALUES("2","2","1","2","1","2","","4","2","6","2");
INSERT INTO hotel_customer VALUES("3","2","","2","3","23","","46","2","6","2");

DROP TABLE IF EXISTS invoice_airline;
CREATE TABLE `invoice_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float NOT NULL,
  `mainProcCharge` float NOT NULL,
  `mainOtherCharge` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `mainTax` varchar(30) NOT NULL,
  `discount` float NOT NULL,
  `pnr` varchar(20) NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `ticketDetails` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
INSERT INTO invoice_airline VALUES("1","1","2016-09-01","86","0","0","0","Reservation change fee collected","","0","0","NHI1RD","1000","Pending","document/20160901125156.doc","","2","6","2");
INSERT INTO invoice_airline VALUES("2","2","2016-09-01","87","0","0","0","EY PNR HTBDPY","","0","0","3I53TT","14050","Pending","document/20160901125119.docx","","2","6","2");
INSERT INTO invoice_airline VALUES("3","3","2016-09-03","89","0","0","0","","","0","0","AEDKL6","15000","Pending","document/20160903150903.doc","","2","6","2");
INSERT INTO invoice_airline VALUES("4","4","2016-09-03","90","0","0","0","","","0","0","3OEJL9","9500","Pending","document/20160903150745.doc","","2","6","2");
INSERT INTO invoice_airline VALUES("5","5","2016-09-03","90","0","0","0","PAYMENT REF * NAM8CG","","0","0","CW3YVB","9600","Pending","document/20160903150551.pdf","","2","6","2");
INSERT INTO invoice_airline VALUES("7","6","2016-09-03","91","0","0","0","","","0","0","0","9700","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("8","7","2016-09-05","92","0","0","0","","","0","0","3OX2YM","20600","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("9","8","2016-09-05","92","0","0","0","","","0","0","3OX2YM","8700","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("10","9","2016-09-05","87","0","0","0","","","0","0","3OYGIE","49000","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("11","10","2016-09-05","87","0","0","0","","","0","0","3OYEHF","44600","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("12","11","2016-09-05","98","0","0","0","","","0","0","M7AS4L","24800","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("13","12","2016-09-05","100","0","0","0","Ticket ree issue from IX CCJ Office","","0","0","0","1800","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("14","1","2016-09-05","119","65","500","0","","","0","0","0","1065","Pending","","","4","18","2");
INSERT INTO invoice_airline VALUES("15","2","2016-09-05","118","0","0","0","","","0","0","0","15100","Pending","","","4","22","2");
INSERT INTO invoice_airline VALUES("16","13","2016-09-06","126","0","0","0","","","0","0","0","32500","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("17","14","2016-09-06","128","0","0","0","","","0","0","0","22500","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("18","15","2016-09-06","89","0","0","0","MACHINCHERY THOOMBIL/MOHAMED MR \nEDATHADATHIL/SOUBHANATH MRS\nMACHINCHERY THOOMBIL/AFRAH MISS(CHD/22AUG10)\n.MACHINCHERY THOOMBIL/ASHDA MISS(CHD/27MAY07)","","0","0","0","129000","Pending","document/20160907173332.doc","","2","6","2");
INSERT INTO invoice_airline VALUES("19","16","2016-09-06","87","0","0","0","","","0","0","0","18400","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("20","17","2016-09-06","87","0","0","0","Void ticket","","0","0","0","400","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("21","18","2016-09-09","150","0","0","0","","","0","0","0","68000","Pending","document/20160910133905.docx","<p>F</p>","2","6","2");
INSERT INTO invoice_airline VALUES("22","19","2016-09-10","154","0","0","0","","","0","0","0","18500","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("23","20","2016-09-10","156","0","0","0","","","0","0","34DZAZ","15500","Pending","","<p>&nbsp;</p>\n<table class=\"MsoNormalTable\" style=\"width: 31.96%; mso-cellspacing: 1.5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 2.25pt 2.25pt 2.25pt 2.25pt;\" border=\"0\" width=\"31%\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td style=\"width: 98.02%; background: #DDEAFF; padding: 2.25pt 2.25pt 2.25pt 2.25pt;\" width=\"98%\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><strong><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">Passenger(s)</span></strong></p>\n</td>\n</tr>\n</tbody>\n</table>\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\'; display: none; mso-hide: all;\">&nbsp;</span></p>\n<table class=\"MsoNormalTable\" style=\"width: 31.96%; mso-cellspacing: 1.5pt; mso-yfti-tbllook: 1184;\" border=\"0\" width=\"31%\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td style=\"width: 98.04%; border: none; border-bottom: dotted gray 1.0pt; mso-border-bottom-alt: dotted gray .75pt; padding: 3.0pt 3.0pt 3.0pt 3.0pt;\" width=\"98%\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><strong><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">MACHINCHERY THOOMBIL, MOHAMED</span></strong></p>\n</td>\n</tr>\n</tbody>\n</table>\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\'; display: none; mso-hide: all;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\'; display: none; mso-hide: all;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\'; display: none; mso-hide: all;\">&nbsp;</span></p>\n<table class=\"MsoNormalTable\" style=\"width: 31.96%; mso-cellspacing: 1.5pt; mso-yfti-tbllook: 1184;\" border=\"0\" width=\"31%\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td style=\"width: 98.04%; border: none; border-bottom: dotted gray 1.0pt; mso-border-bottom-alt: dotted gray .75pt; padding: 3.0pt 3.0pt 3.0pt 3.0pt;\" width=\"98%\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><strong><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">EDATHADATHIL, SOUBHANATH</span></strong></p>\n</td>\n</tr>\n</tbody>\n</table>\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\'; display: none; mso-hide: all;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\'; display: none; mso-hide: all;\">&nbsp;</span></p>\n<table class=\"MsoNormalTable\" style=\"width: 31.96%; mso-cellspacing: 1.5pt; mso-yfti-tbllook: 1184;\" border=\"0\" width=\"31%\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td style=\"width: 98.04%; border: none; border-bottom: dotted gray 1.0pt; mso-border-bottom-alt: dotted gray .75pt; padding: 3.0pt 3.0pt 3.0pt 3.0pt;\" width=\"98%\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><strong><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">MACHINCHERY THOOMBIL, AFRAH</span></strong></p>\n</td>\n</tr>\n</tbody>\n</table>\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\'; display: none; mso-hide: all;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\'; display: none; mso-hide: all;\">&nbsp;</span></p>\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\'; display: none; mso-hide: all;\">&nbsp;</span></p>\n<table class=\"MsoNormalTable\" style=\"width: 31.96%; mso-cellspacing: 1.5pt; mso-yfti-tbllook: 1184;\" border=\"0\" width=\"31%\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td style=\"width: 98.04%; border: none; border-bottom: dotted gray 1.0pt; mso-border-bottom-alt: dotted gray .75pt; padding: 3.0pt 3.0pt 3.0pt 3.0pt;\" width=\"98%\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><strong><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">MACHINCHERY THOOMBIL, ASHDA</span></strong></p>\n</td>\n</tr>\n</tbody>\n</table>\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\'; display: none; mso-hide: all;\">&nbsp;</span></p>\n<table class=\"MsoNormalTable\" style=\"width: 31.96%; mso-cellspacing: 1.5pt; mso-yfti-tbllook: 1184;\" border=\"0\" width=\"31%\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td style=\"width: 98.04%; border: none; border-bottom: dotted gray 1.0pt; mso-border-bottom-alt: dotted gray .75pt; padding: 3.0pt 3.0pt 3.0pt 3.0pt;\" width=\"98%\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><strong><span lang=\"EN-US\" style=\"font-size: 8.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">Bag Allowance: <br /> ADT 30Kgs/CHD 30Kgs/INF 10Kgs</span></strong></p>\n</td>\n</tr>\n</tbody>\n</table>","2","6","2");
INSERT INTO invoice_airline VALUES("24","21","2016-09-15","128","0","0","0","","","0","0","0","29400","Pending","","<p>&nbsp;</p>\n<table class=\"MsoNormalTable\" style=\"mso-cellspacing: 1.5pt; background: #EFEFEF; border: solid black 1.5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 2.25pt 2.25pt 2.25pt 2.25pt;\" border=\"1\" cellpadding=\"0\">\n<tbody>\n<tr>\n<td style=\"width: 30.0%; border: none; background: #DDEAFF; padding: 2.25pt 2.25pt 2.25pt 2.25pt;\" width=\"30%\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><strong><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">FROM/TO</span></strong></p>\n</td>\n<td style=\"width: 15.0%; border: none; background: #DDEAFF; padding: 2.25pt 2.25pt 2.25pt 2.25pt;\" width=\"15%\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt; text-align: center;\" align=\"center\"><strong><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">FLIGHT</span></strong></p>\n</td>\n<td style=\"width: 10.0%; border: none; background: #DDEAFF; padding: 2.25pt 2.25pt 2.25pt 2.25pt;\" width=\"10%\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt; text-align: center;\" align=\"center\"><strong><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">STOPS</span></strong></p>\n</td>\n<td style=\"width: 20.0%; border: none; background: #DDEAFF; padding: 2.25pt 2.25pt 2.25pt 2.25pt;\" width=\"20%\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt; text-align: center;\" align=\"center\"><strong><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">DEPARTURE</span></strong></p>\n</td>\n<td style=\"width: 20.0%; border: none; background: #DDEAFF; padding: 2.25pt 2.25pt 2.25pt 2.25pt;\" width=\"20%\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt; text-align: center;\" align=\"center\"><strong><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">ARRIVAL</span></strong></p>\n</td>\n</tr>\n<tr>\n<td style=\"border: none; background: white; padding: 2.25pt 2.25pt 2.25pt 2.25pt;\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt;\"><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">DXB ( Dubai, Terminal 2, DXB) Terminal 2 / CCJ ( Kozhikode,International Terminal, CCJ) </span></p>\n</td>\n<td style=\"border: none; background: white; padding: 2.25pt 2.25pt 2.25pt 2.25pt;\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt; text-align: center;\" align=\"center\"><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">IX - 344</span></p>\n</td>\n<td style=\"border: none; background: white; padding: 2.25pt 2.25pt 2.25pt 2.25pt;\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt; text-align: center;\" align=\"center\"><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">0</span></p>\n</td>\n<td style=\"border: none; background: white; padding: 2.25pt 2.25pt 2.25pt 2.25pt;\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt; text-align: center;\" align=\"center\"><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">Fri-16Dec2016 02:20</span></p>\n</td>\n<td style=\"border: none; background: white; padding: 2.25pt 2.25pt 2.25pt 2.25pt;\">\n<p class=\"MsoNormal\" style=\"margin-bottom: 0.0001pt; text-align: center;\" align=\"center\"><span lang=\"EN-US\" style=\"font-size: 10.0pt; font-family: \'Lucida Sans Unicode\',\'sans-serif\'; mso-fareast-font-family: \'Malgun Gothic\';\">Fri-16Dec2016 07:35</span></p>\n</td>\n</tr>\n</tbody>\n</table>","2","6","2");
INSERT INTO invoice_airline VALUES("25","1","2016-09-15","162","0","0","0","","","0","0","0","13100","Pending","document/20160915144046.doc","<p>T</p>","3","35","2");
INSERT INTO invoice_airline VALUES("28","22","2016-09-17","166","0","0","0","","","0","0","0","5300","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("29","23","2016-09-17","167","0","0","0","","","0","0","0","14000","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("30","24","2016-09-17","168","0","0","0","","","0","0","0","10600","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("31","25","2016-09-19","173","0","0","0","","","0","0","4IYCPH","14800","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("32","26","2016-09-19","174","0","0","0","","","0","0","AUKPRG","25000","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("33","27","2016-09-19","87","0","0","0","","","0","0","0","16500","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("34","2","2016-09-19","178","0","0","0","","","0","0","0","14600","Pending","","","3","35","2");
INSERT INTO invoice_airline VALUES("35","28","2016-09-20","183","0","0","0","","","0","0","4JHTIV","15800","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("36","29","2016-09-20","184","0","0","0","","","0","0","3XAZ47","24000","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("37","30","2016-09-20","87","0","0","0","","","0","0","4LWZD7","24100","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("38","31","2016-09-20","87","0","0","0","","","0","0","0","11500","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("39","32","2016-09-22","210","0","0","0","Reservation change","","0","0","0","4200","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("40","33","2016-09-22","210","0","0","0","","","0","0","0","12700","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("41","34","2016-09-22","210","0","0","0","","","0","0","0","15200","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("42","35","2016-09-22","211","0","0","0","","","0","0","0","15600","Pending","","","2","6","2");

DROP TABLE IF EXISTS invoice_hotel;
CREATE TABLE `invoice_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `hotelId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `noOfNight` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `checkInDate` date NOT NULL,
  `checkInTime` varchar(20) NOT NULL,
  `checkOutDate` date NOT NULL,
  `checkOutTime` varchar(20) NOT NULL,
  `billingIns` varchar(100) NOT NULL,
  `reference` varchar(100) NOT NULL,
  `tax` varchar(30) NOT NULL,
  `tds` float NOT NULL,
  `otherCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_misc;
CREATE TABLE `invoice_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `basicAmount` float NOT NULL,
  `tds` float NOT NULL,
  `tax` varchar(20) NOT NULL,
  `otherCharge` float NOT NULL,
  `procCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `narration` int(11) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
INSERT INTO invoice_misc VALUES("1","1","143","2016-09-08","2016-09-08","ALI PATTNMARU VAL","","1500","0","0","0","200","0","1700","Pending","1","","1","10","2");
INSERT INTO invoice_misc VALUES("2","2","151","2016-09-09","2016-09-09","Aslam and wife","","15000","0","0","9750","0","0","24750","Pending","2","","1","5","2");
INSERT INTO invoice_misc VALUES("4","1","199","2016-09-21","2016-09-21","NOUFIYA SHIRIN KARINGAPPARA","","1650","0","0","0","150","0","1800","Pending","5","","2","6","2");

DROP TABLE IF EXISTS invoice_package;
CREATE TABLE `invoice_package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `packageId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `proCharge` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `discount` float DEFAULT '0',
  `netAmount` float NOT NULL DEFAULT '0',
  `description` varchar(200) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package VALUES("2","1","2016-09-19","2","177","0","0","0","0","0","125000","","1","36","2");
INSERT INTO invoice_package VALUES("3","2","2016-09-22","2","177","0","0","0","0","0","125000","","1","36","2");

DROP TABLE IF EXISTS invoice_package_airline;
CREATE TABLE `invoice_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_airline VALUES("4","2","398","","40000","1","36","2");
INSERT INTO invoice_package_airline VALUES("3","2","397","","40000","1","36","2");
INSERT INTO invoice_package_airline VALUES("5","3","545","","40000","1","36","2");
INSERT INTO invoice_package_airline VALUES("6","3","546","","40000","1","36","2");

DROP TABLE IF EXISTS invoice_package_hotel;
CREATE TABLE `invoice_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `roomType` varchar(100) NOT NULL,
  `bedStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_hotel VALUES("4","2","398","1","0","15000","1","36","2");
INSERT INTO invoice_package_hotel VALUES("3","2","397","1","0","15000","1","36","2");
INSERT INTO invoice_package_hotel VALUES("5","3","545","1","0","15000","1","36","2");
INSERT INTO invoice_package_hotel VALUES("6","3","546","1","0","15000","1","36","2");

DROP TABLE IF EXISTS invoice_package_passenger;
CREATE TABLE `invoice_package_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `packageId` int(11) NOT NULL,
  `prefix` varchar(20) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` date DEFAULT NULL,
  `age` int(11) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `birthPlace` varchar(100) NOT NULL,
  `relation` int(11) NOT NULL DEFAULT '0',
  `mobile` varchar(50) NOT NULL DEFAULT 'NA',
  `passportNo` varchar(50) NOT NULL,
  `dateOfIssue` date DEFAULT NULL,
  `validUpTo` date DEFAULT NULL,
  `issuePlace` varchar(100) NOT NULL,
  `mofa` varchar(50) NOT NULL,
  `emofa` varchar(50) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_passenger VALUES("4","2","2","Ms","AYISHABI PALLITHAZHATU","F","1954-12-15","62","Indian","KERALA","2","MUHAMMED KOYA CHEELANGATTU","N9063084","2016-03-31","2026-03-03","KOZHIKODE","","","62500","1","36","2");
INSERT INTO invoice_package_passenger VALUES("3","2","2","Mr","MUHAMMED KOYA CHEELANGATTU","M","1954-01-01","63","Indian","KERALA","0","","N9062177","2016-03-30","2026-03-29","KOZHIKODE","","","62500","1","36","2");
INSERT INTO invoice_package_passenger VALUES("5","3","2","Ms","SAKEENA NALUKUDI PARAMBIL","F","1974-05-30","42","Indian","KERALA","4","","L7056798","2014-01-30","2024-01-29","KOZHIKODE","","","62500","1","36","2");
INSERT INTO invoice_package_passenger VALUES("6","3","2","Ms","SUBAIDA IBRAHIM","F","1962-07-30","54","Indian","KERALA","8","","N9467657","2016-04-22","2026-04-21","KOZHIKODE","","","62500","1","36","2");

DROP TABLE IF EXISTS invoice_package_transportation;
CREATE TABLE `invoice_package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `seatStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_visa;
CREATE TABLE `invoice_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_visa VALUES("4","2","398","0000-00-00","0000-00-00","0000-00-00","7500","1","36","2");
INSERT INTO invoice_package_visa VALUES("3","2","397","0000-00-00","0000-00-00","0000-00-00","7500","1","36","2");
INSERT INTO invoice_package_visa VALUES("5","3","545","0000-00-00","0000-00-00","0000-00-00","7500","1","36","2");
INSERT INTO invoice_package_visa VALUES("6","3","546","0000-00-00","0000-00-00","0000-00-00","7500","1","36","2");

DROP TABLE IF EXISTS invoice_transport;
CREATE TABLE `invoice_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `serviceProviderId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `totalDays` int(11) NOT NULL,
  `vehicleType` varchar(100) NOT NULL,
  `kmAllowed` float NOT NULL,
  `arrivalDetails` text NOT NULL,
  `pickUpFrom` varchar(100) NOT NULL,
  `pickUpTime` varchar(20) NOT NULL,
  `DepartureDetails` text NOT NULL,
  `dropAt` varchar(100) NOT NULL,
  `dropTime` varchar(20) NOT NULL,
  `route` text NOT NULL,
  `inclusive` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_visa;
CREATE TABLE `invoice_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `mainOtherCharge` double NOT NULL,
  `discount` varchar(50) NOT NULL,
  `netAmount` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `customerKey` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=257 DEFAULT CHARSET=utf8;
INSERT INTO invoice_visa VALUES("1","1","2016-09-03","88","0","0","16500","","","Pending","","2","6","2","1IV#188");
INSERT INTO invoice_visa VALUES("2","1","2016-09-05","93","0","0","6200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("3","2","2016-09-05","95","0","0","28700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("4","3","2016-09-05","96","0","0","14800","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("5","4","2016-09-05","96","0","0","300","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("6","5","2016-09-05","99","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("7","6","2016-09-05","96","0","0","5200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("8","7","2016-09-05","95","0","0","29200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("9","8","2016-09-05","95","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("10","9","2016-09-05","101","0","0","14800","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("11","10","2016-09-05","95","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("12","11","2016-09-05","95","0","0","14700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("13","12","2016-09-05","95","0","0","14700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("14","13","2016-09-05","95","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("15","14","2016-09-05","103","0","0","14700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("16","15","2016-09-05","99","0","0","14800","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("17","16","2016-09-05","95","0","0","14700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("18","17","2016-09-05","95","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("19","18","2016-09-06","96","0","0","5200","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("20","19","2016-09-06","96","0","0","300","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("21","20","2016-09-06","123","0","0","3800","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("22","21","2016-09-06","123","0","100","3700","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("23","22","2016-09-06","124","0","0","5300","","","Pending","","1","5","2","");
INSERT INTO invoice_visa VALUES("24","23","2016-09-06","96","0","0","17400","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("25","24","2016-09-06","96","0","0","16400","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("26","25","2016-09-06","129","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("27","26","2016-09-06","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("28","27","2016-09-06","129","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("29","28","2016-09-06","130","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("30","29","2016-09-06","95","0","0","300","","","Pending","","1","2","2","");
INSERT INTO invoice_visa VALUES("31","30","2016-09-06","130","0","0","29600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("32","31","2016-09-06","96","0","0","16500","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("33","32","2016-09-06","130","0","0","40800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("34","33","2016-09-07","93","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("35","34","2016-09-07","95","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("36","35","2016-09-07","93","0","0","16200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("37","36","2016-09-07","129","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("38","37","2016-09-07","101","0","0","5200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("39","38","2016-09-07","95","0","0","29600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("40","39","2016-09-07","114","0","0","29600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("41","40","2016-09-07","114","0","0","16000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("42","41","2016-09-07","130","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("43","42","2016-09-07","95","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("44","43","2016-09-07","99","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("45","44","2016-09-07","95","0","0","29400","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("46","45","2016-09-07","129","0","0","15100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("47","46","2016-09-07","99","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("48","47","2016-09-07","95","0","0","189800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("49","48","2016-09-07","99","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("50","49","2016-09-07","136","0","0","46200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("51","50","2016-09-07","95","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("52","51","2016-09-07","95","0","0","29400","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("53","52","2016-09-07","129","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("54","53","2016-09-07","101","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("55","54","2016-09-07","101","0","0","29600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("56","55","2016-09-07","99","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("57","56","2016-09-07","95","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("58","57","2016-09-07","130","0","0","5100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("59","58","2016-09-07","93","0","0","45300","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("60","59","2016-09-07","138","0","0","400","OLD BALANCE UPDATED 14600","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("61","60","2016-09-07","138","0","0","600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("62","61","2016-09-07","138","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("63","62","2016-09-07","95","0","0","14700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("64","63","2016-09-07","95","0","0","6900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("65","64","2016-09-07","95","0","0","4600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("66","65","2016-09-07","95","0","0","2600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("67","66","2016-09-07","95","0","0","14900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("68","67","2016-09-07","96","0","0","15100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("69","68","2016-09-07","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("70","69","2016-09-07","129","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("71","70","2016-09-07","136","0","0","15400","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("72","2","2016-09-07","139","0","0","5800","","","Pending","","2","6","2","");
INSERT INTO invoice_visa VALUES("73","71","2016-09-07","95","0","0","7700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("74","72","2016-09-07","95","0","0","43800","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("107","105","2016-09-10","96","0","0","5200","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("76","74","2016-09-07","95","0","0","14900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("77","75","2016-09-07","95","0","0","900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("78","76","2016-09-07","95","0","0","58400","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("79","77","2016-09-07","95","0","0","88200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("80","78","2016-09-08","101","0","0","10000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("81","79","2016-09-08","101","0","0","5800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("82","80","2016-09-08","141","0","0","6600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("83","81","2016-09-08","142","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("84","82","2016-09-08","144","0","500","16000","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("85","83","2016-09-08","130","0","0","5100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("86","84","2016-09-08","130","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("87","85","2016-09-08","103","0","0","5500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("88","86","2016-09-08","145","0","0","45000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("89","87","2016-09-08","96","0","0","16000","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("90","88","2016-09-08","96","0","0","11000","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("91","89","2016-09-08","129","0","0","900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("92","90","2016-09-08","129","0","0","11100","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("93","91","2016-09-08","129","0","0","300","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("94","92","2016-09-08","129","0","0","59200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("106","104","2016-09-09","99","0","0","78325","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("96","94","2016-09-08","148","0","0","6300","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("97","95","2016-09-08","129","0","0","1200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("98","96","2016-09-08","129","0","0","15900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("99","97","2016-09-08","129","0","0","300","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("100","98","2016-09-08","129","0","0","15900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("101","99","2016-09-08","129","0","0","300","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("102","100","2016-09-08","149","0","0","7750","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("103","101","2016-09-08","149","0","0","7750","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("104","102","2016-09-09","114","0","0","16500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("105","103","2016-09-09","93","0","0","16200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("108","106","2016-09-10","96","0","0","20800","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("109","107","2016-09-10","152","0","0","5200","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("110","108","2016-09-10","96","0","0","11500","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("111","109","2016-09-10","153","0","0","16300","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("112","110","2016-09-14","96","0","0","14700","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("113","3","2016-09-14","92","0","0","17200","","","Pending","","2","6","2","");
INSERT INTO invoice_visa VALUES("114","111","2016-09-15","95","0","0","1200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("115","112","2016-09-16","164","0","0","30600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("116","113","2016-09-16","95","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("117","114","2016-09-16","99","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("118","115","2016-09-16","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("119","116","2016-09-16","95","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("120","117","2016-09-16","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("121","118","2016-09-16","101","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("122","119","2016-09-16","165","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("123","120","2016-09-16","165","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("124","121","2016-09-16","165","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("125","122","2016-09-17","165","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("126","123","2016-09-17","99","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("127","124","2016-09-17","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("128","125","2016-09-17","130","0","0","5100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("129","126","2016-09-17","95","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("130","127","2016-09-17","169","0","0","29000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("131","128","2016-09-17","169","0","0","14500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("132","129","2016-09-17","169","0","0","58000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("133","130","2016-09-17","169","0","0","29000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("134","131","2016-09-17","169","0","0","29000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("135","132","2016-09-17","169","0","0","145000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("136","133","2016-09-17","169","0","0","29000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("137","134","2016-09-17","170","0","0","16200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("138","135","2016-09-19","169","0","0","29000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("139","136","2016-09-19","169","0","0","101500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("140","137","2016-09-19","169","0","0","58000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("141","138","2016-09-19","169","0","0","72500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("142","139","2016-09-19","114","0","0","16000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("143","140","2016-09-19","169","0","0","87000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("144","141","2016-09-19","130","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("145","142","2016-09-19","114","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("146","143","2016-09-19","171","0","0","5500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("147","144","2016-09-19","169","0","0","29000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("148","145","2016-09-19","114","0","0","58800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("149","146","2016-09-19","169","0","0","29000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("150","147","2016-09-19","99","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("151","148","2016-09-19","169","0","0","25625","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("152","149","2016-09-19","169","0","0","15375","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("153","150","2016-09-19","169","0","0","14500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("154","151","2016-09-19","130","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("155","152","2016-09-19","129","0","0","30200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("156","153","2016-09-19","171","0","0","10500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("157","154","2016-09-19","95","0","0","900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("158","155","2016-09-19","170","0","0","31400","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("159","156","2016-09-19","130","0","0","300","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("160","157","2016-09-19","96","0","0","600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("161","158","2016-09-19","95","0","0","600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("162","159","2016-09-19","101","0","0","29600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("163","160","2016-09-19","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("164","161","2016-09-19","165","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("165","162","2016-09-19","130","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("166","163","2016-09-19","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("167","164","2016-09-19","99","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("168","165","2016-09-19","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("169","166","2016-09-19","99","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("170","167","2016-09-19","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("171","168","2016-09-19","99","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("172","169","2016-09-19","169","0","0","40800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("173","170","2016-09-19","169","0","0","45900","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("174","171","2016-09-19","169","0","0","66300","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("175","172","2016-09-19","169","0","0","20400","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("176","173","2016-09-19","99","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("177","174","2016-09-19","95","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("178","175","2016-09-19","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("179","176","2016-09-19","130","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("180","177","2016-09-19","165","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("181","178","2016-09-19","96","0","0","10400","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("182","179","2016-09-19","130","0","0","5100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("183","180","2016-09-19","130","0","0","10200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("184","181","2016-09-19","169","0","0","10200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("185","182","2016-09-19","101","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("186","183","2016-09-19","142","0","0","15100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("187","184","2016-09-19","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("188","185","2016-09-20","151","0","0","6800","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("189","186","2016-09-20","151","0","0","5800","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("190","187","2016-09-20","186","0","0","18500","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("191","188","2016-09-20","187","0","0","7800","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("192","189","2016-09-20","142","0","0","70200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("193","190","2016-09-20","188","0","0","54700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("194","191","2016-09-20","189","0","0","17400","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("195","192","2016-09-20","188","0","0","18000","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("196","193","2016-09-20","99","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("197","194","2016-09-20","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("198","195","2016-09-20","165","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("199","196","2016-09-20","142","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("200","197","2016-09-20","101","0","0","10000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("201","198","2016-09-20","99","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("202","199","2016-09-20","169","0","0","14500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("203","200","2016-09-20","169","0","0","43500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("204","201","2016-09-20","169","0","0","29000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("205","202","2016-09-20","169","0","0","87000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("206","203","2016-09-20","169","0","0","43500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("207","204","2016-09-20","114","0","0","14500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("208","205","2016-09-20","99","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("209","206","2016-09-20","114","0","0","14500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("210","207","2016-09-20","169","0","0","5100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("211","208","2016-09-20","96","0","0","14700","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("212","209","2016-09-20","194","0","0","1000","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("239","236","2016-09-21","151","0","0","6200","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("238","235","2016-09-20","114","0","0","14500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("215","212","2016-09-20","195","0","0","16000","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("216","213","2016-09-20","96","0","0","10400","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("217","214","2016-09-20","169","0","0","45900","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("218","215","2016-09-20","114","0","0","14500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("219","216","2016-09-20","99","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("220","217","2016-09-20","114","0","0","14500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("221","218","2016-09-20","99","0","0","6200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("222","219","2016-09-20","96","0","0","10400","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("223","220","2016-09-20","129","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("224","221","2016-09-20","142","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("225","222","2016-09-20","96","0","0","14700","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("226","223","2016-09-20","96","0","0","14700","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("227","224","2016-09-20","101","0","0","59200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("228","225","2016-09-20","96","0","0","34500","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("229","226","2016-09-20","95","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("230","227","2016-09-20","96","0","0","14700","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("231","228","2016-09-20","114","0","0","14500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("232","229","2016-09-20","169","0","0","29000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("233","230","2016-09-20","169","0","0","29000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("234","231","2016-09-20","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("235","232","2016-09-20","130","0","0","5100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("236","233","2016-09-20","196","0","0","45000","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("237","234","2016-09-20","95","0","0","43500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("240","237","2016-09-21","151","0","0","17400","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("241","238","2016-09-21","208","0","0","5200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("242","239","2016-09-21","208","0","0","300","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("243","240","2016-09-22","114","0","0","14500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("244","241","2016-09-22","114","0","0","14500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("245","242","2016-09-22","169","0","0","15300","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("246","243","2016-09-22","101","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("247","244","2016-09-22","129","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("248","245","2016-09-22","142","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("249","246","2016-09-22","209","0","0","15800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("250","247","2016-09-22","209","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("251","248","2016-09-22","95","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("252","249","2016-09-22","165","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("253","250","2016-09-22","129","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("254","251","2016-09-22","151","0","0","22000","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("255","252","2016-09-22","151","0","0","22000","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("256","253","2016-09-22","96","0","0","71800","","","Pending","","1","10","2","");

DROP TABLE IF EXISTS log;
CREATE TABLE `log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(50) NOT NULL,
  `tableId` int(11) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `changeDate` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `oldAmt` float NOT NULL,
  `newAmt` float NOT NULL,
  `description` text NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
INSERT INTO log VALUES("1","Invoice Airline","5","5","2016-09-03","Edit","9600","9600","NetAmt:9600.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("2","Invoice Airline","6","6","2016-09-03","Edit","9700","9700","NetAmt:9700,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("3","Invoice Airline","6","6","2016-09-03","Delete","9700","0","InvoiceAmt:9700,PurchaseVoucherNo:6,PurchaseAmt:9130,PaidAmt:0,ReceivedAmt:0","6","2","2");
INSERT INTO log VALUES("4","Invoice Airline","8","7","2016-09-05","Edit","10300","20600","NetAmt:20600.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("5","Invoice Airline","10","9","2016-09-05","Edit","24500","49000","NetAmt:49000.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("6","Invoice Airline","10","9","2016-09-05","Edit","49000","49000","NetAmt:49000.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("7","Invoice Airline","10","9","2016-09-05","Edit","49000","49000","NetAmt:49000.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("8","Invoice Airline","11","10","2016-09-05","Edit","22300","44600","NetAmt:44600.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("9","Invoice Airline","10","9","2016-09-05","Edit","49000","49000","NetAmt:49000.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("10","Invoice Visa","25","24","2016-09-06","Edit","16400","16400","NetAmt:16400,Discount:0","8","1","2");
INSERT INTO log VALUES("11","Invoice Visa","25","24","2016-09-06","Edit","16400","16400","NetAmt:16400,Discount:0","8","1","2");
INSERT INTO log VALUES("12","Invoice Visa","24","23","2016-09-06","Edit","16400","16400","NetAmt:16400,Discount:0","8","1","2");
INSERT INTO log VALUES("13","Invoice Visa","60","59","2016-09-07","Edit","400","400","NetAmt:400,Discount:0","10","1","2");
INSERT INTO log VALUES("14","Invoice Airline","12","11","2016-09-07","Edit","24800","24800","NetAmt:24800.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("15","Invoice Miscellaneous","1","1","2016-09-08","Edit","1700","1700","NetAmt:1700.00,Discount:0,Tds:0,MainTax:0","10","1","2");
INSERT INTO log VALUES("16","Invoice Miscellaneous","1","1","2016-09-08","Edit","1700","1700","NetAmt:1700,Discount:0,Tds:0,MainTax:0","10","1","2");
INSERT INTO log VALUES("17","Invoice Miscellaneous","1","1","2016-09-08","Edit","1700","1700","NetAmt:1700,Discount:0,Tds:0,MainTax:0","10","1","2");
INSERT INTO log VALUES("18","Invoice Visa","98","96","2016-09-08","Edit","16400","15900","NetAmt:15900,Discount:0","10","1","2");
INSERT INTO log VALUES("19","Invoice Visa","95","93","2016-09-08","Edit","59200","59200","NetAmt:59200,Discount:0","10","1","2");
INSERT INTO log VALUES("20","Invoice Visa","95","93","2016-09-08","Edit","59200","59200","NetAmt:59200,Discount:0","10","1","2");
INSERT INTO log VALUES("21","Invoice Visa","75","73","2016-09-09","Delete","43800","0","InvoiceAmt:43800,PurchaseVoucherNo:71,PurchaseAmt:43800,PaidAmt:0,ReceivedAmt:0","10","1","2");
INSERT INTO log VALUES("22","Invoice Visa","93","91","2016-09-09","Edit","300","300","NetAmt:300,Discount:0","10","1","2");
INSERT INTO log VALUES("23","Invoice Visa","94","92","2016-09-09","Edit","59200","59200","NetAmt:59200,Discount:0","10","1","2");
INSERT INTO log VALUES("24","Invoice Visa","94","92","2016-09-09","Edit","59200","59200","NetAmt:59200,Discount:0","10","1","2");
INSERT INTO log VALUES("25","Invoice Visa","97","95","2016-09-09","Edit","1200","1200","NetAmt:1200,Discount:0","10","1","2");
INSERT INTO log VALUES("26","Invoice Visa","95","93","2016-09-09","Delete","59200","0","InvoiceAmt:59200,PurchaseVoucherNo:90,PurchaseAmt:59200,PaidAmt:0,ReceivedAmt:49100","10","1","2");
INSERT INTO log VALUES("27","Invoice Visa","92","90","2016-09-09","Edit","14700","11400","NetAmt:11400,Discount:0","10","1","2");
INSERT INTO log VALUES("28","Invoice Visa","74","72","2016-09-09","Edit","43800","43800","NetAmt:43800,Discount:0","10","1","2");
INSERT INTO log VALUES("29","Invoice Visa","74","72","2016-09-09","Edit","43800","43800","NetAmt:43800,Discount:0","10","1","2");
INSERT INTO log VALUES("30","Invoice Miscellaneous","2","2","2016-09-09","Edit","24750","24750","NetAmt:24750,Discount:0,Tds:0,MainTax:0","5","1","2");
INSERT INTO log VALUES("31","Invoice Visa","74","72","2016-09-10","Edit","43800","43800","NetAmt:43800,Discount:0","10","1","2");
INSERT INTO log VALUES("32","Invoice Visa","17","16","2016-09-10","Edit","14700","14700","NetAmt:14700,Discount:0","10","1","2");
INSERT INTO log VALUES("33","Invoice Visa","24","23","2016-09-10","Edit","16400","17400","NetAmt:17400,Discount:0","8","1","2");
INSERT INTO log VALUES("34","Invoice Visa","24","23","2016-09-10","Edit","17400","17400","NetAmt:17400,Discount:0","8","1","2");
INSERT INTO log VALUES("35","Invoice Visa","74","72","2016-09-10","Edit","43800","43800","NetAmt:43800,Discount:0","10","1","2");
INSERT INTO log VALUES("36","Invoice Airline","27","23","2016-09-17","Edit","14000","14000","NetAmt:14000.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("37","Invoice Airline","27","23","2016-09-17","Delete","14000","0","InvoiceAmt:14000,PaidAmt:0,ReceivedAmt:0","6","2","2");
INSERT INTO log VALUES("38","Invoice Airline","26","22","2016-09-17","Delete","5300","0","InvoiceAmt:5300,PurchaseVoucherNo:22,PurchaseAmt:5100,PaidAmt:0,ReceivedAmt:0","6","2","2");
INSERT INTO log VALUES("39","Invoice Miscellaneous","3","3","2016-09-19","Delete","107","0","InvoiceAmt:107,PurchaseVoucherNo:3,PurchaseAmt:23,PaidAmt:0,ReceivedAmt:0","10","1","2");
INSERT INTO log VALUES("40","Invoice Visa","188","185","2016-09-20","Edit","5800","6800","NetAmt:6800,Discount:0","8","1","2");
INSERT INTO log VALUES("41","Invoice Visa","193","190","2016-09-20","Edit","71200","54700","NetAmt:54700,Discount:0","10","1","2");
INSERT INTO log VALUES("42","Invoice Visa","193","190","2016-09-20","Edit","54700","54700","NetAmt:54700,Discount:0","10","1","2");
INSERT INTO log VALUES("43","Invoice Visa","195","192","2016-09-20","Edit","16500","16500","NetAmt:16500,Discount:0","10","1","2");
INSERT INTO log VALUES("44","Invoice Visa","195","192","2016-09-20","Edit","16500","18000","NetAmt:18000,Discount:0","10","1","2");
INSERT INTO log VALUES("45","Invoice Visa","187","184","2016-09-20","Edit","14600","14600","NetAmt:14600,Discount:0","9","1","2");
INSERT INTO log VALUES("46","Invoice Visa","214","211","2016-09-20","Delete","15000","0","InvoiceAmt:15000,PaidAmt:0,ReceivedAmt:0","8","1","2");
INSERT INTO log VALUES("47","Invoice Visa","213","210","2016-09-20","Delete","45675","0","InvoiceAmt:45675,PaidAmt:0,ReceivedAmt:0","9","1","2");
INSERT INTO log VALUES("48","Invoice Visa","96","94","2016-09-21","Edit","3800","6300","NetAmt:6300,Discount:0","8","1","2");
INSERT INTO log VALUES("49","Invoice Visa","92","90","2016-09-21","Edit","11400","11100","NetAmt:11100,Discount:0","10","1","2");
INSERT INTO log VALUES("50","Invoice Visa","241","238","2016-09-21","Edit","5500","5200","NetAmt:5200,Discount:0","10","1","2");
INSERT INTO log VALUES("51","Invoice Visa","241","238","2016-09-21","Edit","5200","5200","NetAmt:5200,Discount:0","10","1","2");
INSERT INTO log VALUES("52","Invoice Visa","241","238","2016-09-21","Edit","5200","5200","NetAmt:5200,Discount:0","10","1","2");
INSERT INTO log VALUES("53","Invoice Visa","192","189","2016-09-22","Edit","85300","70200","NetAmt:70200,Discount:0","10","1","2");
INSERT INTO log VALUES("54","Invoice Visa","255","252","2016-09-22","Edit","22000","22000","NetAmt:22000,Discount:0","8","1","2");
INSERT INTO log VALUES("55","Invoice Visa","221","218","2016-09-22","Edit","12400","6200","NetAmt:6200,Discount:0","9","1","2");
INSERT INTO log VALUES("56","Invoice Visa","196","193","2016-09-22","Edit","14800","14600","NetAmt:14600,Discount:0","9","1","2");

DROP TABLE IF EXISTS login;
CREATE TABLE `login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
INSERT INTO login VALUES("1","0","admin","81dc9bdb52d04dc20036dbd8313ed055","Admin","Active","0");
INSERT INTO login VALUES("2","1","mtmkzkd","ad8dc86031cc2b133b014e4887a649a5","Branch","Active","2");
INSERT INTO login VALUES("3","2","MTMVTRA","01b08dc6d86c837d2057e82c1bcc32d5","Branch","Active","2");
INSERT INTO login VALUES("4","3","MTMPTB","d29e62f0d944da737e90b336a035ee3b","Branch","Active","2");
INSERT INTO login VALUES("5","1","sidheeq","ba856b0417652289251c866c3e22824e","Staff","Active","2");
INSERT INTO login VALUES("6","2","MANU","a866c79ccdd20e88e036d93a740f454c","Staff","Active","2");
INSERT INTO login VALUES("7","3","NASAR","73459038a6d3b1f36a8ac676f6973fff","Staff","Active","2");
INSERT INTO login VALUES("8","4","SAFUVAN","e10adc3949ba59abbe56e057f20f883e","Staff","Active","2");
INSERT INTO login VALUES("9","5","SAHAD","bbb97d09796c1836e464439d690a82da","Staff","Active","2");
INSERT INTO login VALUES("10","6","IRFAD","b4fbb06cf89e72c88e8f77937160f8b6","Staff","Active","2");
INSERT INTO login VALUES("15","11","AKHIL","8724aa758c2f662d79952870ef486ea6","Staff","Active","2");
INSERT INTO login VALUES("17","4","TEST","033bd94b1168d7e4f0d644c3c95e35bf","Branch","Active","2");
INSERT INTO login VALUES("18","13","HYDER","bd02b22685e2122f2a4b800fc5856c95","Staff","Active","2");
INSERT INTO login VALUES("20","15","FAIZY","e5fc0be25dd312817792969b6d984a80","Staff","Active","2");
INSERT INTO login VALUES("21","16","sha1","74a89174426b40307102e165374ab8ab","Staff","Active","2");
INSERT INTO login VALUES("22","17","AKHIL K P","8724aa758c2f662d79952870ef486ea6","Staff","Active","2");
INSERT INTO login VALUES("23","18","SADHAM","3c2ef6af516eb7b7e369531cdac2f98e","Staff","Active","2");
INSERT INTO login VALUES("24","19","FAIZYY","d5e3e8c37821a6a25fa6b60c65382ddd","Staff","Active","2");
INSERT INTO login VALUES("33","22","FAIZAL","c64fd02bb7f1cb6f7d81771d3c0c86d8","Staff","Active","2");
INSERT INTO login VALUES("34","23","SAIFU","3b268c3e7e207f74cdbc1f29e8d424e6","Staff","Active","2");
INSERT INTO login VALUES("35","24","SHAMSHAD","48b639fab5f8ef0f155d0b40a67f3eb7","Staff","Active","2");
INSERT INTO login VALUES("36","25","SIRAJ","014db098eb6d8d7fbfa951d282590e52","Staff","Active","2");
INSERT INTO login VALUES("37","11","tssidheeq","bb8a792c7ae90240fec484a84adafd45","Branch","Active","2");
INSERT INTO login VALUES("38","26","sidheeqts","bb8a792c7ae90240fec484a84adafd45","Staff","Active","2");

DROP TABLE IF EXISTS misc_narration;
CREATE TABLE `misc_narration` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `narration` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
INSERT INTO misc_narration VALUES("1","Passport Service","1","10","2");
INSERT INTO misc_narration VALUES("2","Flight Date Change","1","5","2");
INSERT INTO misc_narration VALUES("3","Passport Service","3","34","2");
INSERT INTO misc_narration VALUES("4","Cash Receipt","11","38","2");
INSERT INTO misc_narration VALUES("5","Passport Ser","2","6","2");

DROP TABLE IF EXISTS other_purchase;
CREATE TABLE `other_purchase` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package;
CREATE TABLE `package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageName` varchar(200) NOT NULL,
  `staff` int(11) NOT NULL,
  `leader` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `place` varchar(100) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `tds` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `remark` varchar(100) NOT NULL,
  `services` varchar(100) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `confirm` int(11) NOT NULL,
  `discount` float NOT NULL,
  `extra` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO package VALUES("1","Umra","1","Kareem Saquafi","9846445342","2016-11-10","","","0","0","0","","airline,hotel,visa","62500","45500","15500","0","0","0","0","0","0");
INSERT INTO package VALUES("2","Umra","1","Kareem Saquafi","9846445342","2016-11-10","1st Batch","","0","0","0","","airline,hotel,visa","62500","45500","15500","1","2","2","0","0","0");

DROP TABLE IF EXISTS package_airline;
CREATE TABLE `package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `yq` float NOT NULL DEFAULT '0',
  `procCharge` float NOT NULL DEFAULT '0',
  `tax` float NOT NULL DEFAULT '0',
  `adultCharge` float NOT NULL,
  `childCharge` float NOT NULL,
  `infantCharge` float NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `packageId` (`packageId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO package_airline VALUES("1","1","0","0","0","40000","38000","8000","40000","38000","8000","Transit time : 11h 20m","0","0");
INSERT INTO package_airline VALUES("2","2","0","0","0","40000","38000","8000","40000","38000","8000","Transit time : 11h 20m","1","2");

DROP TABLE IF EXISTS package_airline_flights;
CREATE TABLE `package_airline_flights` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_airlineId` int(11) NOT NULL,
  `airline` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `sectorFrom` varchar(200) NOT NULL,
  `sectorTo` varchar(200) NOT NULL,
  `jDate` date NOT NULL,
  `airTime` varchar(20) NOT NULL,
  `aDate` date NOT NULL,
  `aTime` varchar(20) NOT NULL,
  `class` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO package_airline_flights VALUES("1","1","1","19","WY-292","CCJ","JED","2016-11-10","08:10","2016-11-10","00:05","E","0","0");
INSERT INTO package_airline_flights VALUES("2","2","2","19","WY-292","CCJ","JED","2016-11-10","08:10","2016-11-11","00:05","E","1","2");

DROP TABLE IF EXISTS package_hotel;
CREATE TABLE `package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `hotelId` int(11) NOT NULL,
  `checkinDate` date DEFAULT NULL,
  `checkoutDate` date DEFAULT NULL,
  `noOfDays` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO package_hotel VALUES("1","1","6","2016-11-11","2016-11-20","9","0","0","172");
INSERT INTO package_hotel VALUES("2","2","6","2016-11-11","2016-11-20","9","1","2","175");

DROP TABLE IF EXISTS package_room_type;
CREATE TABLE `package_room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_hotelId` int(11) NOT NULL,
  `roomId` int(11) NOT NULL,
  `rate` float NOT NULL,
  `otherCharge` float NOT NULL,
  `total` float NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
INSERT INTO package_room_type VALUES("1","1","1","1","15000","0","15000","0","0","0");
INSERT INTO package_room_type VALUES("2","1","1","2","0","0","0","0","0","0");
INSERT INTO package_room_type VALUES("3","1","1","3","0","0","0","0","0","0");
INSERT INTO package_room_type VALUES("4","1","1","4","0","0","0","0","0","0");
INSERT INTO package_room_type VALUES("5","1","1","5","0","0","0","0","0","0");
INSERT INTO package_room_type VALUES("6","1","1","6","0","0","0","0","0","0");
INSERT INTO package_room_type VALUES("7","1","1","7","0","0","0","0","0","0");
INSERT INTO package_room_type VALUES("8","1","1","8","0","0","0","0","0","0");
INSERT INTO package_room_type VALUES("9","2","2","1","15000","0","15000","13000","1","2");
INSERT INTO package_room_type VALUES("10","2","2","2","0","0","0","0","1","2");
INSERT INTO package_room_type VALUES("11","2","2","3","0","0","0","0","1","2");
INSERT INTO package_room_type VALUES("12","2","2","4","0","0","0","0","1","2");
INSERT INTO package_room_type VALUES("13","2","2","5","0","0","0","0","1","2");
INSERT INTO package_room_type VALUES("14","2","2","6","0","0","0","0","1","2");
INSERT INTO package_room_type VALUES("15","2","2","7","0","0","0","0","1","2");
INSERT INTO package_room_type VALUES("16","2","2","8","0","0","0","0","1","2");

DROP TABLE IF EXISTS package_transportation;
CREATE TABLE `package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `vehicle` int(11) NOT NULL,
  `seatCharge` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package_visa;
CREATE TABLE `package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `visaFor` varchar(100) NOT NULL,
  `visaFee` float NOT NULL DEFAULT '0',
  `vfs` float NOT NULL DEFAULT '0',
  `ddCharge` float NOT NULL DEFAULT '0',
  `serviceCharge` float NOT NULL DEFAULT '0',
  `otherCharge` float NOT NULL DEFAULT '0',
  `courierCharge` float NOT NULL DEFAULT '0',
  `total` float NOT NULL DEFAULT '0',
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO package_visa VALUES("1","1","42","7500","0","0","0","0","0","7500","172","5000","0","0");
INSERT INTO package_visa VALUES("2","2","42","7500","0","0","0","0","0","7500","172","5000","1","2");

DROP TABLE IF EXISTS passenger;
CREATE TABLE `passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `prefix` varchar(20) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationshipId` int(11) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `mofa` varchar(50) NOT NULL,
  `emofa` varchar(50) NOT NULL,
  `passPhoto` varchar(200) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=551 DEFAULT CHARSET=latin1;
INSERT INTO passenger VALUES("1","86","","ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR","1978-01-10","39","M","CHAMRAVATTAM","0","Indian","","G1284698","2008-02-10","2018-02-09","ABU DHABI","","","","2","6","2");
INSERT INTO passenger VALUES("2","87","Mr","THALE KUNNATH/JASEEM","0000-00-00","0","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("3","88","","MAIMOONA MANJAPILLI AHAMMAD ","1971-05-22","0","F","","0","","","N1595786","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("4","89","Mr","MACHINCHERITHOOMBIL/MOHAMMED SAKEEB","1996-05-26","20","M","TIRUR","0","Indian","","M8018076","2015-04-13","2025-04-12","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("5","90","Mr","CHERUVAKKATH/ABDUL RAHEEM","1960-01-23","57","M","VATTAPARAMBU","0","Indian","","H0109737","2009-05-26","2019-05-25","DUBAI","","","","2","6","2");
INSERT INTO passenger VALUES("6","91","Mr","ODUVIL/AYYOOB","1979-03-10","38","M","THAZHEKODE","0","Indian","","M7851287","2015-04-10","2025-04-09","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("7","90","Mr","Cheruvakkath/abdul Raheem","1960-01-23","57","M","VATTAPARAMBU","0","Indian","","H0109737","2009-05-26","2019-05-25","DUBAI","","","","2","6","2");
INSERT INTO passenger VALUES("8","91","Mr","ODUVIL/AYYOOB MR","1979-03-10","38","M","10-03-1979","0","Indian","","M7851287","2015-04-10","2025-04-09","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("9","92","Mr","MALIYEKKAL/MUHAMMED MUKTHAR","1988-03-20","28","M","EDARIKODE","0","Indian","","H0356591","2008-08-26","2018-08-25","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("10","92","Mrs","POOVATIL/JUHANA ","1996-04-06","20","F","FEROKE","0","Indian","","M2664637","2014-10-08","2024-10-07","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("11","87","Mrs","ALQAHTANI/QAMRAA SAAD","0000-00-00","0","","","0","SAUDI","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("12","87","Mrs","ALQAHTANI/QAMRAA SAAD ","0000-00-00","0","","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("13","87","Ms","ALQAHTANI/RENAD FAISAL ","0000-00-00","0","","","0","SAUDI","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("14","87","","","0000-00-00","0","","","0","","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("15","87","Ms","ALQAHTANI/ALBANDARI FAISAL","0000-00-00","0","F","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("16","87","Ms","ALQAHTANI/NORAH FAISAL ","0000-00-00","0","F","","0","SAUDI","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("17","87","Mrs","ALQAHTANI/QAMRAA SAAD ","0000-00-00","0","F","","0","SAUDI","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("18","93","","MUFEED ABDUL AZIZ","0000-00-00","0","M","","0","","","Z3668844","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("19","95","","VISAKH","0000-00-00","0","M","","0","","","N1140524","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("20","96","","ABDUL GAFOOR","0000-00-00","0","M","","0","","","H2626002","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("21","96","","ABDUL GAFOOR","0000-00-00","0","M","","0","","","H2626002","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("22","99","","SHAHUNL ABDULNASAR","0000-00-00","0","M","","0","","","G8487574","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("23","96","","SHAJAHAN PALLILATH","0000-00-00","0","M","","0","","","H5103542","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("24","95","","SIMON","2000-09-05","0","M","","0","","","J1485230","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("25","95","","ELSY","2000-09-05","0","F","","0","","","J1485339","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("26","95","","AMEER","0000-00-00","0","M","","0","","","M0314613","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("27","98","Mr","AINIKKUNNAN JAYAFER","1992-08-02","24","M","VETTICHIRA","0","Indian","","M7297991","2015-03-19","2025-03-18","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("28","100","Mr","ABOOBACKER KARINGAPPARA","0000-00-00","0","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("29","101","","ALLY JOY","0000-00-00","0","M","","0","","","J0885006","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("30","95","","DENIAL","0000-00-00","0","M","","0","","","M9715530","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("31","95","","SUSY","0000-00-00","0","M","","0","","","N9538535","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("32","95","","RAHMATHUNNISA ","0000-00-00","0","F","","0","","","L9256080","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("33","95","","SHANU MON","0000-00-00","0","M","","0","","","J2491039","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("34","103","","MAIMOONA MANJAPPILLY","0000-00-00","0","F","","0","","","M159786","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("35","99","","MEENA HILARY","0000-00-00","0","F","","0","","","M1342981","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("36","114","","MISRIYA","0000-00-00","0","M","","0","","","P1902660","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("37","95","","AGHIL","0000-00-00","0","M","","0","","","L9642581","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("38","119","Mr","RIYAS ","0000-00-00","0","","","0","Indian","","0","0000-00-00","0000-00-00","","","","","4","18","2");
INSERT INTO passenger VALUES("39","118","","ARUNCUT","0000-00-00","0","","","0","Indian","","AAAA","2016-09-13","2016-09-30","CALI","","","","4","22","2");
INSERT INTO passenger VALUES("40","96","","ABDUL SALEEM","0000-00-00","0","M","","0","","","K1233711","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("41","96","","ABDUL SALEEM","0000-00-00","0","M","","0","","","K1233711","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("42","123","","MOHAMMED ISMAIL","0000-00-00","0","M","","0","","","L715982","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("43","123","","MOHAMMED ISMAIL","0000-00-00","0","M","","0","","","L715982","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("44","124","","ABDUL GAFOOR","1976-09-06","0","M","","0","","","H2626002","0000-00-00","0000-00-00","","","","","1","5","2");
INSERT INTO passenger VALUES("45","96","","FATHIMA HASHIFA","0000-00-00","0","F","","0","","","N7809054","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("46","96","","626SHIRIN SHAHANA","0000-00-00","0","M","","0","","","P1438308","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("47","126","Mr","SUNEER MAJEED","0000-00-00","0","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("48","96","","626SHIRIN SHAHANA","0000-00-00","0","M","","0","","","P1438308","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("49","96","","626SHIRIN SHAHANA","0000-00-00","0","M","","0","","","P1438308","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("50","96","","FATHIMA HASHIFA","0000-00-00","0","F","","0","","","N7809054","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("51","128","Mr","SIDHEEK POOKAYIL","1971-06-30","45","M","CHERUSHOLA","0","Indian","","J2072600","2010-05-04","2020-05-03","ABUDHABI","","","","2","6","2");
INSERT INTO passenger VALUES("52","89","Mr","MACHINCHERY THOOMBIL, MOHAMED","0000-00-00","0","","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("53","87","Mr","KUNNATHODI/ABDUL JALEEL","1979-01-01","38","M","POOVANCHINA","0","Indian","","G7230920","2008-02-08","2018-02-07","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("54","129","","MUHAMMED ANSAB","0000-00-00","0","M","","0","","","N1900984","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("55","95","","ANSAR","0000-00-00","0","M","","0","","","P2698527","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("56","129","","SAFNAS","0000-00-00","0","M","","0","","","H6710271","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("57","130","","HASSAN PUTHIYATI","0000-00-00","0","M","","0","","","G9210752","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("58","95","","ANEEM","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","2","2");
INSERT INTO passenger VALUES("59","130","","HEZZA","2016-09-06","0","F","","0","","","N8801988","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("60","130","","ISMATHUL HAMRA","2016-09-06","0","F","","0","","","N8239724","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("61","96","","MTM570 JAMAL YASAR","0000-00-00","0","M","","0","","","H3365841","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("62","130","","MANZOOR","2016-09-06","0","M","","0","","","P0914263","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("63","130","","NOUFAL","2016-09-06","0","M","","0","","","J5706307","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("64","130","","PERAMAYIAN","2016-09-06","0","M","","0","","","L9319426","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("65","130","","ARAVINDAN","2016-09-06","0","M","","0","","","M2761361","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("66","130","","SAINUDHEEN","2016-09-06","0","M","","0","","","Z2993148","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("67","130","","RUKIYA","2016-09-06","0","F","","0","","","K3086549","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("68","130","","HAMNA","2016-09-06","0","F","","0","","","N9974878","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("69","130","","IDHAYATHUL HAMDAN","2016-09-06","0","M","","0","","","N9974878","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("70","93","","ANSIL","0000-00-00","0","M","","0","","","L1756857","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("71","95","","ZAINABA","0000-00-00","0","M","","0","","","J8564596","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("72","93","","IBRAHIM KUTTY","0000-00-00","0","M","","0","","","P2813905","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("73","129","","MUHAMMED","0000-00-00","0","M","","0","","","N5120158","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("74","101","","MANU BHAI","0000-00-00","0","M","","0","","","N7456851","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("75","95","","SAROJA","2016-09-07","0","F","","0","","","K6272814","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("76","95","","JAFIN","2016-09-07","0","F","","0","","","M3827069","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("77","114","","CHANDRAN PULIKKATT","2016-09-07","0","M","","0","","","J5352156","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("78","114","","SHEEJA","2016-09-07","0","F","","0","","","N4524757","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("79","114","","NILAA NIKHIL","0000-00-00","0","M","","0","","","P0422439","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("80","130","","JAMES CLEETUS","0000-00-00","0","M","","0","","","N5427392","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("81","95","","MERLIN BABU","0000-00-00","0","F","","0","","","M0477973","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("82","99","","ASHNA","0000-00-00","0","F","","0","","","M6093308","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("83","95","","SHAMEEM","2016-09-07","0","M","","0","","","K3072306","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("84","95","","SRIMATHY","2016-09-07","0","F","","0","","","P4071810","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("85","129","","SHEEBA","2016-09-07","0","F","","0","","","N5159159","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("86","99","","JUSTIN JAMES","2016-09-07","0","M","","0","","","L5914253","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("87","95","","ASOK KUMAR","2016-09-07","0","M","","0","","","M1674522","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("88","95","","DILEEP","2016-09-07","0","M","","0","","","G6067478","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("89","95","","HASHIM","2016-09-07","0","M","","0","","","H6275220","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("90","95","","RAMAKRISHNAN","2016-09-07","0","M","","0","","","G6160411","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("91","95","","JERIN JAMES","2016-09-07","0","M","","0","","","K5427677","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("92","95","","SABEEH","2016-09-07","0","M","","0","","","H3049393","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("93","95","","FAISAL","2016-09-07","0","M","","0","","","K3033987","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("94","95","","UMMER","2016-09-07","0","M","","0","","","M4950208","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("95","95","","HAFIZ","2016-09-07","0","M","","0","","","J2006760","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("96","95","","KUNJHU MON","2016-09-07","0","M","","0","","","M7807535","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("97","95","","MADAVADAS","2016-09-07","0","M","","0","","","J5254307","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("98","95","","VENUGOPALAN","2016-09-07","0","M","","0","","","H5709904","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("99","95","","SAROJINI","2016-09-07","0","F","","0","","","H5708579","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("100","99","","VARGHESE","2016-09-07","0","M","","0","","","M832608","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("101","99","","SUDHEESH","2016-09-07","0","M","","0","","","N6950516","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("102","136","","RAJA RAM","2016-09-07","0","M","","0","","","H7437321","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("103","136","","VINOD","2016-09-07","0","M","","0","","","N9184559","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("104","136","","MANOJ KUMAR","2016-09-07","0","M","","0","","","K5356456","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("105","95","","ARIF","2016-09-07","0","M","","0","","","M9418291","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("106","95","","BONEY JOSEPH","2016-09-07","0","M","","0","","","G8586116","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("107","95","","SALIHA","2016-09-07","0","M","","0","","","M0174249","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("108","95","","NIMMY","2016-09-07","0","M","","0","","","L2844381","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("109","129","","SARATH KRISHNA","2016-09-07","0","M","","0","","","K6243609","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("110","101","","HABEEBA SHAFEEEK","2016-09-07","0","M","","0","","","P2642443","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("111","101","","HASNA","2016-09-07","0","M","","0","","","P1163918","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("112","101","","ZAHIDA","2016-09-07","0","M","","0","","","M9909492","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("113","99","","DAISAN DAVID","2016-09-07","0","M","","0","","","N1142416","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("114","95","","SIJO JOSEPH","2016-09-07","0","M","","0","","","N8591047","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("115","95","","AJAY KRISHNA","2016-09-07","0","M","","0","","","G9194546","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("116","130","","ABDUL AZEEZ","2016-09-07","0","M","","0","","","L8040492","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("117","93","","NAFSIYA KOLAMBIL","2016-09-07","0","F","","0","","","G4454840","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("118","93","","RSHAN AHAMMED","2016-09-07","0","M","","0","","","P0352855","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("119","93","","TANIA ISRA","2016-09-07","0","F","","0","","","K5269033","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("120","138","","NAHIM","0000-00-00","0","M","","0","","","K2828090","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("121","138","","ABDU RAHIMAN ","2000-09-01","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("122","138","","SREEJITH","2000-09-01","0","M","","0","","","K8893537","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("123","138","","JAMES ","0000-00-00","0","M","","0","","","J4059812","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("124","138","","NAHIM","0000-00-00","0","M","","0","","","K2828090","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("125","98","Mr","Ainikkunnan Jayafer","1992-08-02","24","M","VETTICHIRA","0","Indian","","M7297991","2015-03-19","2025-03-18","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("126","95","","ANSAR","0000-00-00","0","M","","0","","","M5169954","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("127","95","","ANILA","2000-09-01","0","F","","0","","","N7681273","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("128","95","","PABRI CK","2000-09-01","0","F","","0","","","N7686960","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("129","95","","PATRICIA ANJELIN","2000-09-01","0","F","","0","","","P2488318","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("130","95","","REMA KUMARI","2000-09-01","0","F","","0","","","N9101342","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("131","95","","KAVITHA","2000-09-01","0","F","","0","","","K6437126","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("132","95","","RAMANATHAN","0000-00-00","0","M","","0","","","M6237420","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("133","95","","Fathima Hanna","0000-00-00","0","F","","0","","","L7896144","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("134","96","","RICHARD","2016-09-07","0","M","","0","","","H4603974","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("135","95","","THAJUDHEEN","2016-09-07","0","M","","0","","","K1076374","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("136","129","","SUSHANTH","2016-09-07","0","M","","0","","","K5230981","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("137","136","","SAJEEVAN","2016-09-07","0","M","","0","","","M0569663","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("138","139","","AYISHA KUNNATH","1978-01-02","0","F","","0","","","P0915315","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("139","95","","SABEENA 1118","0000-00-00","0","F","","0","","","	J5242293","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("140","95","","FERVEES 1199","2000-09-05","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("141","95","","VIJESH 1222","2000-09-01","0","M","","0","","","	K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("142","95","","ANEEM 1223","2000-09-01","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("143","95","","FERVEES 1199","2000-09-05","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("144","95","","VIJESH 1222","2000-09-01","0","M","","0","","","	K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("145","95","","ANEEM 1223","2000-09-01","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("146","95","","KAVERI","0000-00-00","0","M","","0","","","K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("147","95","","AKHILA 12200","2016-09-01","0","F","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("148","95","","ISHAK 1224","2000-09-01","0","M","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("149","95","","SREESHMA 1226","2000-09-01","0","F","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("150","95","","VYSHAKH 1233","2000-09-06","0","M","","0","","","n9553909 ","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("151","95","","AHAMEDKUTTY 1258","2000-09-01","0","M","","0","","","J2077389","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("152","95","","THARUN 1259","2000-09-01","0","M","","0","","","P0246374","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("153","95","","BINOY 1261","2000-09-01","0","M","","0","","","L6725925","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("154","95","","NOORUNNISA 1234","2000-09-01","0","F","","0","","","M7276307","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("155","95","","MUHAMMED RASHID 1234","2000-09-01","0","M","","0","","","P3999764","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("156","95","","ASHA MATHARI 1240","2000-09-01","0","F","","0","","","J5238983","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("157","95","","GEETHA 1250","2000-09-01","0","F","","0","","","N5577038","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("158","95","","1262	ANU MOLE","2000-09-01","0","F","","0","","","N7798897","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("159","95","","1264	VIJITHA","2000-09-01","0","F","","0","","","	P0428557","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("160","101","","VIKAS VINOD","2016-09-08","0","M","","0","","","J3924121","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("161","101","","VIKAS VINOD","2016-09-08","0","M","","0","","","J3924121","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("162","141","","AMAL PRABHAKAR","2016-09-08","0","M","","0","","","J66565291","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("163","142","","MUHAMMED SHAMMAS","2016-09-08","0","M","","0","","","L9931582","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("164","144","","MTM564 AHMED ORMUNDAKKAL","0000-00-00","0","M","","0","","","J4887754","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("165","130","","RAJAN KOCHU","2016-09-08","0","M","","0","","","K3751349","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("166","130","","MATHUKUTTY","2016-09-08","0","M","","0","","","L1747043","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("167","103","","AYISHA","0000-00-00","0","F","","0","","","P0615315","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("168","145","","RAHILA","2016-09-08","0","F","","0","","","L4220391","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("169","145","","FATHIMA MIHRA","2016-09-08","0","F","","0","","","M3684407","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("170","145","","MUHAMMED MIRSHAD","2016-09-08","0","F","","0","","","N5538499","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("171","96","","HIDAYATHULLA","0000-00-00","0","M","","0","","","J8843095","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("172","96","","MTM615 HIDAYATHULLA","0000-00-00","0","M","","0","","","J8843095","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("173","129","","AYISHA","2000-09-01","0","F","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("174","129","","FATHIMA","2000-09-01","0","F","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("175","129","","FAHEENA","2000-09-01","0","M","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("176","129","","1196	MUHAMMED ALI","0000-00-00","0","M","","0","","","K6459585","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("177","95","","1196	MUHAMMED ALI","0000-00-00","0","M","","0","","","0","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("178","95","","1197	SAREENA","2000-09-01","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("179","95","","1198   VALSALA","2000-09-01","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("180","95","","1245	NASREENA ","2000-09-01","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("181","95","","1245    AYSHANA RAMEES","2010-09-01","0","F","","0","","","M5856848	","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("182","95","","1197	SAREENA","2000-09-01","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("183","95","","1198   VALSALA","2000-09-01","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("184","95","","1245	NASREENA ","2000-09-01","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("185","95","","1245    AYSHANA RAMEES","2010-09-01","0","F","","0","","","M5856848	","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("186","148","","mohammed","0000-00-00","0","M","","0","","","k8000829","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("187","95","","SAREENA VALSALA NASREENA /AYSHANA RAMEES","2010-09-01","0","F","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("188","95","","VALSALA","2010-09-01","0","F","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("189","95","","NASREENA","2000-09-01","0","F","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("190","95","","AYSHANA RAMEES","2000-09-01","0","F","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("191","129","","1247	HISHAM MASHOOD","0000-00-00","0","M","","0","","","J5440840","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("192","129","","1247	HISHAM MASHOOD","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("193","129","","1247	HISHAM MASHOOD","0000-00-00","0","M","","0","","","J5440840","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("194","129","","1254	ASHRAF PURATH PARAMBATH","0000-00-00","0","M","","0","","","H5686487","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("195","129","","1254	ASHRAF PURATH PARAMBATH","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("196","129","","1197	SAREENA","0000-00-00","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("197","129","","1198   VALSALA","0000-00-00","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("198","129","","1245	NASREENA","0000-00-00","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("199","129","","1245    AYSHANA RAMEES","0000-00-00","0","F","","0","","","M5856848","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("200","129","","1197	SAREENA","0000-00-00","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("201","129","","1198   VALSALA","0000-00-00","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("202","129","","1245	NASREENA","0000-00-00","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("203","129","","1245    AYSHANA RAMEES","0000-00-00","0","F","","0","","","M5856848","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("204","149","","SABAD MUHAMMED","0000-00-00","0","M","","0","","","M4747049","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("205","149","","MUHAMMED ALI MUHAMMED","0000-00-00","0","M","","0","","","H5110153","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("206","114","","HAMDAN HAMEED","2016-09-09","0","M","","0","","","P2203649","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("207","93","","JAMSHAD","2016-09-09","0","M","","0","","","H1608955","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("208","129","","1196	MUHAMMED ALI","0000-00-00","0","M","","0","","","0","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("209","129","","1197	SAREENA","0000-00-00","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("210","129","","1198   VALSALA","0000-00-00","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("211","129","","1245	NASREENA","0000-00-00","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("212","129","","1245    AYSHANA RAMEES","0000-00-00","0","F","","0","","","M5856848","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("213","129","","1245    AYSHANA RAMEES","0000-00-00","0","F","","0","","","M5856848","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("214","129","","1245	NASREENA","0000-00-00","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("215","129","","1198   VALSALA","0000-00-00","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("216","129","","1197	SAREENA","0000-00-00","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("217","129","","SAREENA VALSALA NASREENA /AYSHANA RAMEES","0000-00-00","0","F","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("218","129","","VALSALA","0000-00-00","0","F","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("219","129","","NASREENA","0000-00-00","0","F","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("220","129","","AYSHANA RAMEES","0000-00-00","0","F","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("221","129","","1196	MUHAMMED ALI","0000-00-00","0","M","","0","","","K6459585","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("222","99","","COUNTER BALANCE,","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("223","150","Mr","VALIYA VALAPPIL ALI ABDUL KAREEM","1967-05-01","49","M","PAVITTAPPURAM","0","Indian","","K4621024","2012-03-19","2022-03-18","ABU DHABI","","","","2","6","2");
INSERT INTO passenger VALUES("224","150","Mrs","NADEERA ABDUL KAREEM","1970-05-15","46","F","MANNALAMKUNNU","0","Indian","","G5039690","2008-12-03","2018-12-02","ABUDHABI","","","","2","6","2");
INSERT INTO passenger VALUES("225","95","","FERVEES 1199","0000-00-00","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("226","95","","VIJESH 1222","0000-00-00","0","M","","0","","","K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("227","95","","ANEEM 1223","0000-00-00","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("228","95","","FERVEES 1199","0000-00-00","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("229","95","","VIJESH 1222","0000-00-00","0","M","","0","","","K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("230","95","","ANEEM 1223","0000-00-00","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("231","95","","FERVEES 1199","0000-00-00","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("232","95","","VIJESH 1222","0000-00-00","0","M","","0","","","K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("233","95","","ANEEM 1223","0000-00-00","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("234","96","","UDAYA MOOLYA","0000-00-00","0","M","","0","","","J7091327","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("235","96","","KADAMBU NEBISA,ANNATH,MAHAMMAD SHAFIK,MAHAMMAD NAW","0000-00-00","0","M","","0","","","L6156737.P2778220,P2","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("236","152","","SHAFJAN PATTILLATH CHANDERA","0000-00-00","0","M","","0","","","H5103542","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("237","96","","","0000-00-00","0","","","0","","","","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("238","95","","MISRIYA","0000-00-00","0","M","","0","","","P1902660","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("239","96","","FATHIMA HASHIFA","0000-00-00","0","F","","0","","","N7809054","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("240","96","","FATHIMA HASHIFA","0000-00-00","0","F","","0","","","N7809054","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("241","95","","ANEEM 1223","0000-00-00","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("242","95","","VIJESH 1222","0000-00-00","0","M","","0","","","K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("243","95","","FERVEES 1199","0000-00-00","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("244","153","","USMAN PORKKALAM","0000-00-00","0","M","","0","","","J6960023","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("245","154","Mr","CHEMMUKKAN/MOHAMED SHEHEER","1987-12-10","29","M","KOTTAKKAL","0","Indian","","L5482407","2013-11-07","2023-11-06","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("246","156","Mr","CHEMMUKKIL/MOHAMED HANEEFA","1976-04-23","40","M","CHERUSHOLA","0","Indian","","L1182314","2013-05-14","2023-05-13","ABUDHABI","","","","2","6","2");
INSERT INTO passenger VALUES("247","96","","SEBEER PANEER","0000-00-00","0","M","","0","","","L1356529","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("248","128","Mr","POOKAYIL/SIDHEEK","1971-06-30","45","M","CHERUSHOLA","0","Indian","","J2072600","2010-05-04","2020-05-03","ABU DHABI","","","","2","6","2");
INSERT INTO passenger VALUES("249","162","Mr","ALI ASKAR","1988-02-25","29","M","KERALA","0","Indian","","N2455497","0000-00-00","0000-00-00","","","","","3","35","2");
INSERT INTO passenger VALUES("250","95","","ANU MOLE","2016-09-01","0","F","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("251","95","","SABEERA","2016-09-01","0","F","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("252","95","","REMAKUMARI","2016-09-01","0","F","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("253","95","","SHAHARAS","2016-09-01","0","M","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("254","164","","RIYAS","2016-09-16","0","M","","0","","","P1018115","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("255","164","","RAHINA THOUFEEK","2016-09-16","0","F","","0","","","P0412729","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("256","95","","KUNJHU MOIDEEN","2016-09-16","0","M","","0","","","K0385278","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("257","95","","AYISHA","2016-09-16","0","F","","0","","","K0385277","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("258","99","","NITHEESH","2016-09-16","0","M","","0","","","M2502907","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("259","95","","DAVIES MANJALY","2016-09-16","0","M","","0","","","H9993108","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("260","95","","NIRMALA","2016-09-16","0","F","","0","","","N9068639","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("261","95","","SHANAZ","2016-09-16","0","M","","0","","","P4074758","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("262","101","","DISHA ABISHEK","2016-09-16","0","F","","0","","","P2352218","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("263","165","","YOONUS","2016-09-16","0","M","","0","","","N554446","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("264","165","","EBRAHIM","2016-09-16","0","M","","0","","","M1043706","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("265","165","","AKBAR SHAJAHAN","2016-09-16","0","M","","0","","","P3482267","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("266","166","Mr","ANWAR PALLIKKALAKATH","1975-05-16","41","M","CHINNAMPADI","0","Indian","","M6141348","2015-02-06","2025-02-05","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("267","167","Mr","RASHEED NARUNGALI , NEENAN MARAKKAR","1977-07-15","39","M","PERUMANNA","0","Indian","","F9251602","2007-08-14","2017-08-13","ABUDHABI","","","","2","6","2");
INSERT INTO passenger VALUES("268","167","Mr","Rasheed Narungali , Neenan Marakkar","1977-07-15","39","M","PERUMANNA","0","Indian","","F9251602","2007-08-14","2017-08-13","ABUDHABI","","","","2","6","2");
INSERT INTO passenger VALUES("269","168","Mr","MUHAMMED AFSAL CHEMMALA","1990-06-30","26","M","O K MURI","0","Indian","","G4251068","2007-08-28","2017-06-27","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("270","165","","ASKAR","2016-09-17","0","M","","0","","","N0925550","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("271","99","","TREESA JOSE","2016-09-17","0","F","","0","","","K7236815","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("272","95","","AMIN SHERIF","2016-09-17","0","M","","0","","","J8570711","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("273","130","","JAFER","0000-00-00","0","M","","0","","","L6355058","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("274","95","","ABDUL SHERIF","2016-09-17","0","M","","0","","","L7528787","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("275","95","","SARFUNNIZA","2016-09-17","0","M","","0","","","N2147570","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("276","169","","SARA IBRAHIM","2016-09-17","0","F","","0","","","G3509227","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("277","169","","ATIFUL DULFIYA","2016-09-17","0","F","","0","","","P4000338","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("278","169","","ABDULLA","2016-09-17","0","M","","0","","","G9716233","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("279","169","","NUSRATH","2016-09-17","0","F","","0","","","M2789304","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("280","169","","MINHA FATHIMA","2016-09-17","0","F","","0","","","M4145622","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("281","169","","MALLIK KUMARSWAMY","2016-09-17","0","F","","0","","","J8582907","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("282","169","","MARIYA","2016-09-17","0","F","","0","","","L1316251","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("283","169","","SAIF ABDUL AZEEZ","2016-09-17","0","M","","0","","","H7400492","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("284","169","","ABDUL RAHIMAN","2016-09-17","0","M","","0","","","L3684474","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("285","169","","MURSINA","2016-09-17","0","F","","0","","","P0092227","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("286","169","","PRAJITHA","2016-09-17","0","F","","0","","","N3122093","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("287","169","","RASHIDH ","2016-09-17","0","M","","0","","","P4059622","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("288","169","","MANZOOR","2016-09-17","0","M","","0","","","N9960716","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("289","169","","FIJAS","2016-09-17","0","M","","0","","","P1282872","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("290","169","","VIPIN","2016-09-17","0","M","","0","","","L4827297","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("291","169","","SALIH","2016-09-17","0","M","","0","","","K4635001","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("292","169","","SHANAVAS","2016-09-17","0","M","","0","","","L9686169","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("293","169","","SAHID MON","2016-09-17","0","M","","0","","","H6480490","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("294","169","","SANIDH","2016-09-17","0","M","","0","","","M5654047","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("295","169","","DILEEP","2016-09-17","0","M","","0","","","K4038159","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("296","169","","BAJEESH","2016-09-17","0","M","","0","","","N4336784","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("297","169","","SANOOF","2016-09-17","0","M","","0","","","N8234508","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("298","169","","SHABEENA","2016-09-17","0","F","","0","","","M8557330","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("299","170","","NOORJAHAN","2016-09-17","0","F","","0","","","K9052971","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("300","170","","MOHAMMED ISMAIL","2016-09-17","0","M","","0","","","Z2209883","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("301","170","","ARIFA MAJEED","2016-09-17","0","M","","0","","","K2442805","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("302","169","","MUBASHIRA","2016-09-19","0","F","","0","","","N5210758","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("303","169","","USHA PADMANABAN","2016-09-19","0","F","","0","","","H4651646","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("304","169","","AFSAL ALI","2016-09-19","0","M","","0","","","J8837024","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("305","169","","VINI JOSE","2016-09-19","0","M","","0","","","H9689436","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("306","169","","RASHEED","2016-09-19","0","M","","0","","","N9138711","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("307","169","","MIDHLAJ","2016-09-19","0","M","","0","","","L7920538","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("308","169","","MOHAMMED LIBASIN","2016-09-19","0","M","","0","","","H7232734","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("309","169","","HYDERALI","2016-09-19","0","M","","0","","","P4052692","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("310","169","","ABDUL LATHEEF","2016-09-19","0","M","","0","","","M2403126","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("311","169","","NIHAS","2016-09-19","0","M","","0","","","M6039206","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("312","169","","AZMIN FARAS","2016-09-19","0","F","","0","","","L9706041","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("313","169","","PHARISHA","2016-09-19","0","F","","0","","","L9642701","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("314","169","","GREESHMA","2016-09-19","0","F","","0","","","N9499762","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("315","169","","THANZEER","2016-09-19","0","M","","0","","","G5437199","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("316","169","","VIDESH","2016-09-19","0","M","","0","","","G4842975","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("317","169","","MUSTHAFA","2016-09-19","0","M","","0","","","K0825228","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("318","169","","MOHAMMED BASHEER","2016-09-19","0","M","","0","","","J5523932","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("319","169","","JAYA GOPALAN","2016-09-19","0","M","","0","","","H9597787","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("320","114","","UMAIR SHAMSUDHEEN","2016-09-19","0","M","","0","","","M2434982","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("321","169","","FASALURAHMAN","2016-09-19","0","M","","0","","","N2555523","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("322","169","","ANWAR","2016-09-19","0","M","","0","","","K9738262","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("323","169","","FASEELA MUJEEB","2016-09-19","0","F","","0","","","K1586353","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("324","169","","MINHA MARIYAM","2016-09-19","0","F","","0","","","K53600143","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("325","169","","AYISHA THARAKKAL","2016-09-19","0","F","","0","","","K3056624","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("326","169","","SHAN ADAM","2016-09-19","0","F","","0","","","M9113339","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("327","130","","ABDUL NASEER","2016-09-19","0","M","","0","","","K9823040","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("328","114","","ASTHER LOURD","2016-09-19","0","M","","0","","","J1943467","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("329","171","","SIAHUDHEEN","2016-09-19","0","M","","0","","","K9057849","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("330","169","","KANZA","2016-09-19","0","F","","0","","","N7026492","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("331","169","","NAZIYA","2016-09-19","0","F","","0","","","N8159169","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("332","114","","BEEGUM","2016-09-19","0","F","","0","","","M4585433","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("333","114","","MUKTHAR MOHAMMED","2016-09-19","0","M","","0","","","P0621596","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("334","114","","ANSI BADAR","2016-09-19","0","F","","0","","","L9385374","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("335","114","","SWALIHA","2016-09-19","0","F","","0","","","N3176850","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("336","169","","SREEJITH","2016-09-19","0","M","","0","","","L8535331","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("337","169","","NOORUDHEEN","2016-09-19","0","M","","0","","","K7515194","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("338","99","","SEBASTIAN","2016-09-19","0","M","","0","","","H4429316","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("339","99","","ANU CHETTU","2016-09-19","0","M","","0","","","K1061340","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("340","169","","AZAAN","2016-09-19","0","M","","0","","","M2529672","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("341","169","","SAIMA","2016-09-19","0","F","","0","","","M2236990","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("342","169","","SAKEENA","2016-09-19","0","F","","0","","","L9018447","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("343","169","","MOHAMMED ABID","2016-09-19","0","M","","0","","","Z1910668","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("344","169","","FEMINA","2016-09-19","0","F","","0","","","K1583268","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("345","169","","JAKIR ALI","2016-09-19","0","M","","0","","","J8710589","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("346","169","","HARIHARA","2016-09-19","0","M","","0","","","J8732899","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("347","169","","SHEIKH","2016-09-19","0","M","","0","","","J8711078","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("348","169","","UMMER ISMAYIL","2016-09-19","0","M","","0","","","P4073615","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("349","130","","TITTU","2016-09-19","0","M","","0","","","K6437253","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("350","129","","SUHAIB","2016-09-19","0","M","","0","","","L2822761","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("351","129","","LALITHA","2016-09-19","0","F","","0","","","J89747590","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("352","171","","BASHEER","2016-09-19","0","M","","0","","","G2821855","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("353","95","","MADAVAS","2016-09-01","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("354","95","","ASHRAF","2016-09-01","0","M","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("355","95","","KAVITHA","2016-09-01","0","M","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("356","170","","SAHIR &ABDUL JALEEL","1983-04-13","0","M","","0","","","L2371853/	N7113488	","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("357","130","","MATHUKUTTY","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("358","96","","KADER","2016-09-01","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("359","96","","AYISHA","2016-09-04","0","F","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("360","173","Mr","AYAPPALLI MOIDEEN","1960-05-16","56","M","KANMANAM","0","Indian","","L3813768","2013-07-03","2023-06-02","DUBAI","","","","2","6","2");
INSERT INTO passenger VALUES("361","174","Mr","PANDARATHIL JURAIGE","1986-08-22","30","M","RANDATHANI","0","Indian","","G5721484","2007-11-05","2017-11-04","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("362","87","Mr","THALAKOTTUPURAM/ABDUL LATHEEF","0000-00-00","0","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("363","95","","PHILIP","2016-09-01","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("364","95","","MSARIYAMMA","2016-09-01","0","F","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("365","101","","ISTHIAQ","2016-09-19","0","M","","0","","","L2498982","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("366","101","","SMITHA","2016-09-19","0","F","","0","","","K4201402","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("367","0","Mr","MUHAMMED KOYA CHEELANGATTU","1954-01-01","63","M","VELLAYIL, KERALA","0","Indian","","N9062177","2016-03-30","2026-03-29","KOZHIKODE","","","","1","36","2");
INSERT INTO passenger VALUES("368","0","Ms","AYISHABI PALLITHAZHATU","1954-12-15","62","F","KERALA","0","Indian","","N9063084","2016-03-31","2026-03-30","KOZHIKODE","","","","1","36","2");
INSERT INTO passenger VALUES("369","95","","MUHAMMED MUSTHAFA","2016-09-19","0","M","","0","","","L1466281","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("370","165","","JOBIN","2016-09-19","0","M","","0","","","K6230924","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("371","130","","AKHIL","2016-09-19","0","M","","0","","","K5367262","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("372","95","","SJAJAHAN","2016-09-19","0","M","","0","","","J5958608","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("373","99","","SOOSAMMA CHANDY","2016-09-19","0","F","","0","","","N8816782","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("374","95","","FIROS","2016-09-19","0","M","","0","","","L5907266","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("375","99","","CERIN XAVIER","2016-09-19","0","M","","0","","","K1079157","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("376","99","","RONI MOOKAYIL","2016-09-19","0","M","","0","","","M0651353","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("377","95","","ANISH KUMAR","2016-09-19","0","M","","0","","","H4653637","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("378","99","","TOMY MOL","2016-09-19","0","F","","0","","","P4063035","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("379","169","","MOHAMMED REHAN","2016-09-19","0","M","","0","","","N0482654","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("380","169","","AFSAL","2016-09-19","0","M","","0","","","Z2953998","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("381","169","","MUHAMMED NOUFAL","2016-09-19","0","M","","0","","","M2830526","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("382","169","","PARATTU SAIDU","2016-09-19","0","M","","0","","","H1113535","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("383","169","","FATHIMA","2016-09-19","0","F","","0","","","G6290489","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("384","169","","SAYYAF","2016-09-19","0","M","","0","","","K3979073","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("385","169","","TAJUDHENN","2016-09-19","0","M","","0","","","J2040267","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("386","169","","ABDU RAHIMAN","2016-09-19","0","M","","0","","","N0161120","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("387","169","","MOHAMMED JIHAD","2016-09-19","0","M","","0","","","N1597279","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("388","169","","FAHIS","2016-09-19","0","M","","0","","","P2332064","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("389","169","","FARSANA","2016-09-19","0","F","","0","","","H5552428","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("390","169","","HANA FATHIMA","2016-09-19","0","F","","0","","","L3650539","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("391","169","","ZAMEEL","2016-09-19","0","M","","0","","","L8267861","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("392","169","","KHADEEJA","2016-09-19","0","F","","0","","","J2671347","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("393","169","","JUSTIN","2016-09-19","0","M","","0","","","N2088987","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("394","169","","RASHEED","2016-09-19","0","M","","0","","","M3712539","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("395","169","","SUBAIDA","2016-09-19","0","F","","0","","","L2355344","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("396","178","Mr","VAKAYIL MUHAMMED/ALI AKBER ","0000-00-00","0","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","3","35","2");
INSERT INTO passenger VALUES("397","177","Mr","MUHAMMED KOYA CHEELANGATTU","1954-01-01","63","M","KERALA","0","Indian","","N9062177","2016-03-30","2026-03-29","KOZHIKODE","","","","1","36","2");
INSERT INTO passenger VALUES("398","177","Ms","AYISHABI PALLITHAZHATU","1954-12-15","62","F","KERALA","0","Indian","","N9063084","2016-03-31","2026-03-03","KOZHIKODE","","","","1","36","2");
INSERT INTO passenger VALUES("399","169","","JANSI BANU","2016-09-19","0","F","","0","","","K3509049","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("400","169","","MOHAMMED","2016-09-19","0","M","","0","","","K4048589","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("401","169","","AHAMMED AFREED","2016-09-19","0","M","","0","","","M9386753","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("402","169","","ANEESA","2016-09-19","0","F","","0","","","M9867590","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("403","169","","HANA FATHIMA","2016-09-19","0","F","","0","","","N2757430","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("404","169","","MUHAMMED HAMNA","2016-09-19","0","F","","0","","","N2757718","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("405","169","","SURENDRAN","2016-09-19","0","M","","0","","","L7060373","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("406","169","","SAIDALI","2016-09-19","0","M","","0","","","L1222266","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("407","169","","AMUL KOYA","2016-09-19","0","M","","0","","","H9381424","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("408","169","","SARA","2016-09-19","0","F","","0","","","H2809593","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("409","169","","SUMAYA MOL","2016-09-19","0","F","","0","","","G6157755","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("410","169","","RIFA","2016-09-19","0","F","","0","","","N8086443","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("411","169","","ABDUL RAOOF","2016-09-19","0","M","","0","","","Z2105213","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("412","169","","VIJAYAMMA","2016-09-19","0","F","","0","","","P0248026","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("413","169","","MUHAMMED SHAFI","2016-09-19","0","M","","0","","","H1477969","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("414","169","","KAMARU","2016-09-19","0","M","","0","","","Z3403231","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("415","169","","ABDUL JABBAR","2016-09-19","0","M","","0","","","M0236862","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("416","99","","SAJU RAJ","2016-09-19","0","M","","0","","","K6463555","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("417","95","","FATHIMA NASEER","2016-09-19","0","F","","0","","","N2809487","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("418","95","","JOBIN JOSE MARY","2016-09-19","0","M","","0","","","N4351083","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("419","130","","PARVATHI","2016-09-19","0","F","","0","","","K9986602","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("420","165","","LITHAS","2016-09-19","0","M","","0","","","L8303251","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("421","96","","MUNAVVAR","2016-09-19","0","M","","0","","","J6244054","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("422","96","","DANIS","2016-09-19","0","M","","0","","","K7417702","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("423","130","","MOHAMMED ISHAK","2016-09-19","0","M","","0","","","N3264522","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("424","130","","MOHAMMED ISHAK","2016-09-19","0","M","","0","","","N3264522","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("425","130","","THAHIRA","2016-09-19","0","F","","0","","","J5239951","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("426","169","","ASHA JHON","2016-09-19","0","F","","0","","","P2812641","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("427","169","","JUSTIN","2016-09-19","0","M","","0","","","G7715071","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("428","101","","SOMA BANERGI","2016-09-19","0","F","","0","","","H4493704","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("429","142","","FATHIMA SHIFANATH","2016-09-19","0","F","","0","","","P2512185","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("430","95","","LINTO","2016-09-19","0","M","","0","","","M2693421","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("431","183","Mr","KATTIL/HUSSAIN","1978-02-01","39","M","chirakkal","0","Indian","","M5493217","2015-01-14","2025-01-13","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("432","151","","SHAMEEM","0000-00-00","0","M","","0","","","H0047499","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("433","184","Mr","MOHAMMED CHOORAPPULAKKAL","1973-05-30","43","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("434","151","","MUSTHAK ALI AHAMMED","0000-00-00","0","M","","0","","","M1904121","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("435","186","","AZEEZ NARACHUMMANTAKATH","0000-00-00","0","M","","0","","","K8764291","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("436","187","","MUHAMMED IMRAN/MTM571 KHADEEJA PALLIPURAY","0000-00-00","0","M","","0","","","N2014312/ N8496095","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("437","142","","COUNTER:65 78 79 91 94 95 100 119 138 226 228 333 ","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("438","188","","COUNTER :128 129 358 390 522 1231","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("439","189","","counter :156 332 429 955","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("440","188","","MTM603 AMEERUDHEEN ,  MOHAMMED AL","2016-09-01","0","M","","0","","","J8840991 ","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("441","188","","RAHMATH ALI","2016-09-01","0","M","","0","","","K1301847","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("442","188","","MOHAMMED AL","2016-09-01","0","M","","0","","","K4044842","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("443","87","Mr","MANAKKADVAKKATHODI/ABDULAZEEZ","1990-11-13","26","M","PERINTALMANNA","0","Indian","","K2511923","2012-01-19","2022-01-18","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("444","99","","MUHAMMED ANAS","2016-09-20","0","M","","0","","","L1312268","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("445","95","","KRISHANANUNNY","2016-09-20","0","M","","0","","","N2509254","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("446","165","","NOUSHAD","2016-09-20","0","M","","0","","","L9056502","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("447","165","","ASKAR","2016-09-20","0","M","","0","","","N2124342","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("448","142","","NITHIN VARGHESE","2016-09-20","0","M","","0","","","K5969564","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("449","101","","PRINCE","2016-09-20","0","M","","0","","","N1725019","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("450","99","","MUHAMMED MUSTHAFA","2016-09-20","0","M","","0","","","G7682506","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("451","169","","SHAROOK","2016-09-20","0","M","","0","","","N0479933","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("452","169","","NOUFAL","2016-09-20","0","M","","0","","","L4528400","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("453","169","","MOHAMMED AFLAH","2016-09-20","0","M","","0","","","L421240","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("454","169","","SHAFEEK","2016-09-20","0","M","","0","","","H3389846","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("455","169","","MATHAI","2016-09-20","0","M","","0","","","H5378488","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("456","169","","ACHAMMA","2016-09-20","0","F","","0","","","H7575435","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("457","87","Mr","MOHAMMED SHAJI PONNANAGATHODI  ","0000-00-00","0","","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("458","169","","RASHID ALUNGAL","2016-09-20","0","M","","0","","","K9765697","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("459","169","","MOHAMMED RASHID","2016-09-20","0","M","","0","","","K9744697","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("460","169","","SAMEER","2016-09-20","0","M","","0","","","G8235703","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("461","169","","SHAMEEM","2016-09-20","0","M","","0","","","M6015145","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("462","169","","ANOOP","2016-09-20","0","M","","0","","","M49736009","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("463","169","","MUHAMMED SABIR","2016-09-20","0","M","","0","","","N7803090","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("464","169","","BUSHARA","2016-09-20","0","F","","0","","","K1586814","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("465","169","","ABDUL BASITH","2016-09-20","0","M","","0","","","N7631192","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("466","169","","SIRAJ","2016-09-20","0","M","","0","","","M8148005","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("467","114","","RAJESH ANAND","2016-09-20","0","M","","0","","","H3990335","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("468","99","","SHANLY SIMON","2016-09-20","0","F","","0","","","H6455482","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("469","114","","MANSOOR","2016-09-20","0","M","","0","","","G4551263","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("470","169","","SHANAS","2016-09-20","0","M","","0","","","G6369762","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("471","96","","RICHARD PINTO","0000-00-00","0","M","","0","","","H4603974","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("472","194","","MOOSA NEYYAN","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("473","169","","ANIL KUMAR","2016-09-20","0","M","","0","","","M2296507","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("474","169","","JAYASREE","2016-09-20","0","M","","0","","","K0085069","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("475","169","","SAYED MUHAMMED","2016-09-20","0","M","","0","","","K9227945","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("476","169","","SAYIDATHA JAFAR","2016-09-20","0","F","","0","","","P1015108","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("477","169","","LIYAKATH","2016-09-20","0","M","","0","","","L1155076","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("478","169","","MARY KLISTED","2016-09-20","0","F","","0","","","H5041984","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("479","169","","JHON","2016-09-20","0","F","","0","","","Z1971834","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("480","169","","NAFEESU KODOTH","2016-09-20","0","F","","0","","","G2804883","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("481","169","","LUKMNUL KAKEEM","2016-09-20","0","M","","0","","","M8565689","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("482","195","","MUHAMMED ARAFATH","0000-00-00","0","M","","0","","","N0481584","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("483","195","","","0000-00-00","0","","","0","","","","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("484","96","","SMITA , SAIESHA","0000-00-00","0","M","","0","","","","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("485","169","","ANIL KUMAR","2016-09-20","0","M","","0","","","M2296507","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("486","169","","JAYASREE","2016-09-20","0","F","","0","","","K0085069","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("487","169","","SAYED MUHAMMED","2016-09-20","0","M","","0","","","K9227945","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("488","169","","SAYIDATHA JAFAR","2016-09-20","0","F","","0","","","P1015108","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("489","169","","LIYAKATH","2016-09-20","0","M","","0","","","L1155076","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("490","169","","MARY KLISTED","2016-09-20","0","F","","0","","","H5041984","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("491","169","","JHON","2016-09-20","0","F","","0","","","Z1971834","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("492","169","","NAFEESU KODOTH","2016-09-20","0","M","","0","","","G2804883","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("493","169","","LUKMNUL KAKEEM","2016-09-20","0","M","","0","","","M8565689","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("494","114","","MANJU MURALI","2016-09-20","0","F","","0","","","P2098797","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("495","99","","YOOSAF","2016-09-20","0","M","","0","","","N9071534","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("496","114","","SAIDU ALI AKBAR","2016-09-20","0","M","","0","","","K6559055","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("497","99","","VINEETH KUNDANNUR","2016-09-20","0","M","","0","","","K567937","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("498","99","","RAVEENDRAN","2016-09-20","0","M","","0","","","K9336948","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("499","96","","MUNAVAR , DANIS","0000-00-00","0","M","","0","","","J6244054 , K7417702","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("500","129","","SAKIRA ","2016-09-20","0","F","","0","","","M5045102","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("501","142","","ABDUL JALEEL","2016-09-20","0","M","","0","","","P2516541","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("502","96","","SALMAN KHAN","0000-00-00","0","M","","0","","","N7724337","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("503","96","","SHAJAHAN KUDUNGARA HALLI","0000-00-00","0","M","","0","","","N2498971","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("504","101","","PRADISHA","2016-09-20","0","F","","0","","","L7914814","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("505","101","","AYDIN","2016-09-20","0","F","","0","","","L8284123","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("506","101","","MANIYAMMAL","2016-09-20","0","F","","0","","","L8208398","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("507","101","","YEMI ABAN","2016-09-20","0","F","","0","","","N9206672","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("508","96","","MOHAMED ASLAM","0000-00-00","0","M","","0","","","Z1980263","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("509","95","","JESNI","2016-09-20","0","F","","0","","","N4797473","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("510","96","","MOHAMMED ATIF","0000-00-00","0","M","","0","","","L6242738","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("511","114","","SHAMNAS SALIM","2016-09-20","0","M","","0","","","J5808373","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("512","169","","SHERSHAD","2016-09-20","0","M","","0","","","H2289981","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("513","169","","MUHAMMED ASHIK","2016-09-20","0","M","","0","","","G9707646","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("514","169","","SANTHA","2016-09-20","0","F","","0","","","M6047012","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("515","169","","HASNA","2016-09-20","0","F","","0","","","K2449588","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("516","95","","NOUSHERE","2016-09-20","0","M","","0","","","M3925390","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("517","130","","NOUFAL","2016-09-20","0","M","","0","","","L5479760","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("518","196","","MAJIDA , LASIN , LAIBA","0000-00-00","0","M","","0","","","N1847699 , N2128473 ","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("519","95","","AJMAL","2016-09-20","0","M","","0","","","K1961060","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("520","95","","ASGAR","2016-09-20","0","M","","0","","","H6100414","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("521","95","","ASIF","2016-09-20","0","M","","0","","","K036848","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("522","114","","ANAS ABDUL MAJEED","2016-09-20","0","M","","0","","","L7677299","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("523","151","","NAVAB","0000-00-00","0","M","","0","","","P2198094","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("524","151","","khalid , NAJEEB , ALI AKBAR ","0000-00-00","0","M","","0","","","K2266285 , N4044387 ","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("525","208","","HAMEED OTTAPILAVULLATHIL","0000-00-00","0","M","","0","","","L28333806","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("526","208","","HAMEED","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("527","114","","MUHAMMED MALIK","2016-09-22","0","M","","0","","","N0175503","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("528","114","","SREEJA","2016-09-22","0","M","","0","","","P0625401","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("529","169","","SAID HUMAM","2016-09-22","0","M","","0","","","L8905859","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("530","169","","SHAFEER","2016-09-22","0","M","","0","","","L1309942","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("531","169","","ABIN THOMAS","2016-09-22","0","M","","0","","","N9065681","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("532","101","","SHAMSUDHEEN","2016-09-22","0","M","","0","","","L9685743","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("533","129","","SEENATH","2016-09-22","0","M","","0","","","P1506921","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("534","142","","DADU","2016-09-22","0","M","","0","","","K5287361","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("535","209","","NAJEEB","2016-09-22","0","M","","0","","","H0537872","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("536","209","","SAYYID MUHAMMED JIFRI","2016-09-22","0","M","","0","","","G3872348","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("537","95","","SHABIN","2016-09-22","0","M","","0","","","N6825621","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("538","95","","NAHAS","2016-09-22","0","M","","0","","","H6098882","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("539","210","Mr","CHEMMUKKAN, NOUFIR","0000-00-00","0","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("540","165","","AMAL PARATH","2016-09-22","0","M","","0","","","P039069","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("541","210","Mrs","SHAMSIYA KADAKKADAN &  INF ","0000-00-00","0","F","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("542","210","Mr","MOHAMED SAIFE KALODI","0000-00-00","0","F","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("543","211","Mr","ISMAIL PARAMBATH","1977-09-01","39","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("544","129","","MUHABIR","2016-09-22","0","M","","0","","","M6486378","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("545","177","Ms","SAKEENA NALUKUDI PARAMBIL","1974-05-30","42","F","KERALA","0","Indian","","L7056798","2014-01-30","2024-01-29","KOZHIKODE","","","","1","36","2");
INSERT INTO passenger VALUES("546","177","Ms","SUBAIDA IBRAHIM","1962-07-30","54","F","KERALA","0","Indian","","N9467657","2016-04-22","2026-04-21","KOZHIKODE","","","","1","36","2");
INSERT INTO passenger VALUES("547","151","","HASSAINAR","0000-00-00","0","M","","0","","","H63896913","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("548","151","","HASSAINAR","0000-00-00","0","M","","0","","","H63896913","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("549","99","","RAVEENDRAN","2016-09-20","0","M","","0","","","K9336948","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("550","96","","COUNTER :383 541 739 742 749 750 764 805 842 853 9","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");

DROP TABLE IF EXISTS purchase_airline;
CREATE TABLE `purchase_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
INSERT INTO purchase_airline VALUES("1","1","2016-09-01","2016-09-01","81","5","1","1000","0","0","0","0","0","0","0","1000","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("2","2","2016-09-01","2016-09-01","83","5","2","14015","0","0","0","0","0","0","0","14015","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("3","3","2016-09-03","2016-09-03","81","5","3","14730","0","0","0","0","0","0","0","14730","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("4","4","2016-09-03","2016-09-03","83","5","4","9080","0","0","0","0","0","0","0","9080","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("5","5","2016-09-03","2016-09-03","83","5","5","8870","0","0","0","0","0","0","0","8870","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("7","6","2016-09-03","2016-09-03","83","5","6","9130","0","0","0","0","0","0","0","9130","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("8","7","2016-09-05","2016-09-05","83","5","7","20100","0","0","0","0","0","0","0","20100","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("9","8","2016-09-05","2016-09-05","83","5","8","8385","0","0","0","0","0","0","0","8385","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("10","9","2016-09-05","2016-09-05","83","5","9","47570","0","0","0","0","0","0","0","47570","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("11","10","2016-09-05","2016-09-05","83","5","10","43660","0","0","0","0","0","0","0","43660","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("12","11","2016-09-05","2016-09-07","81","5","11","24520","0","0","0","0","0","0","0","24520","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("13","12","2016-09-05","2016-09-05","81","5","12","1300","0","0","0","0","0","0","0","1300","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("14","13","2016-09-06","2016-09-06","81","5","13","32220","0","0","0","0","0","0","0","32220","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("15","14","2016-09-06","2016-09-06","83","5","14","21245","0","0","0","0","0","0","0","21245","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("16","15","2016-09-06","2016-09-06","81","5","15","126500","0","0","0","0","0","0","0","126500","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("17","16","2016-09-06","2016-09-06","83","5","16","17930","0","0","0","0","0","0","0","17930","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("18","17","2016-09-06","2016-09-06","83","5","17","400","0","0","0","0","0","0","0","400","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("19","18","2016-09-09","2016-09-09","81","5","18","67030","0","0","0","0","0","0","0","67030","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("20","19","2016-09-10","2016-09-10","83","5","19","17540","0","0","0","0","0","0","0","17540","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("21","20","2016-09-10","2016-09-10","83","5","20","14750","0","0","0","0","0","0","0","14750","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("22","21","2016-09-15","2016-09-15","81","5","21","29260","0","0","0","0","0","0","0","29260","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("23","1","2016-09-15","2016-09-15","159","5","1","12600","0","0","0","0","0","0","0","12600","","","0","0","3","35","2");
INSERT INTO purchase_airline VALUES("25","22","2016-09-17","2016-09-17","84","5","22","5100","0","0","0","0","0","0","0","5100","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("26","23","2016-09-17","2016-09-17","81","5","23","13170","0","0","0","0","0","0","0","13170","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("27","24","2016-09-17","2016-09-17","81","5","24","10200","0","0","0","0","0","0","0","10200","Cash payed in IX CCJ office","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("28","25","2016-09-19","2016-09-19","83","5","25","13535","0","0","0","0","0","0","0","13535","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("29","26","2016-09-19","2016-09-19","81","5","26","23980","0","0","0","0","0","0","0","23980","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("30","27","2016-09-19","2016-09-19","83","5","27","15565","0","0","0","0","0","0","0","15565","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("31","2","2016-09-19","2016-09-19","160","5","2","14340","0","0","0","0","0","0","0","14340","","","0","0","3","35","2");
INSERT INTO purchase_airline VALUES("32","28","2016-09-20","2016-09-20","83","5","28","15475","0","0","0","0","0","0","0","15475","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("33","29","2016-09-20","2016-09-20","81","5","29","23810","0","0","0","0","0","0","0","23810","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("34","30","2016-09-20","2016-09-20","83","5","30","23900","0","0","0","0","0","0","0","23900","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("35","31","2016-09-20","2016-09-20","81","5","31","11210","0","0","0","0","0","0","0","11210","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("36","32","2016-09-22","2016-09-22","81","5","32","3980","0","0","0","0","0","0","0","3980","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("37","33","2016-09-22","2016-09-22","81","5","33","12250","0","0","0","0","0","0","0","12250","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("38","34","2016-09-22","2016-09-22","81","5","34","14950","0","0","0","0","0","0","0","14950","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("39","35","2016-09-22","2016-09-22","81","5","35","15470","0","0","0","0","0","0","0","15470","","","0","0","2","6","2");

DROP TABLE IF EXISTS purchase_hotel;
CREATE TABLE `purchase_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_misc;
CREATE TABLE `purchase_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO purchase_misc VALUES("1","1","2016-09-08","2016-09-08","146","15","1","1500","0","0","0","0","0","0","0","1500","","","0","0","1","10","2");
INSERT INTO purchase_misc VALUES("2","2","2016-09-09","2016-09-09","146","15","2","15000","0","0","0","0","0","0","0","15000","Fly Hind haj ticket date change","","0","0","1","5","2");
INSERT INTO purchase_misc VALUES("4","1","2016-09-21","2016-09-21","198","15","1","1650","0","0","0","0","0","0","0","1650","","","0","0","2","6","2");

DROP TABLE IF EXISTS purchase_pac_airline;
CREATE TABLE `purchase_pac_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO purchase_pac_airline VALUES("1","1","146","39000","37000","5000","0","0");
INSERT INTO purchase_pac_airline VALUES("2","2","146","39000","37000","5000","1","2");

DROP TABLE IF EXISTS purchase_package_airline;
CREATE TABLE `purchase_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date DEFAULT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL DEFAULT '0',
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_hotel;
CREATE TABLE `purchase_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_transport;
CREATE TABLE `purchase_package_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_visa;
CREATE TABLE `purchase_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_transport;
CREATE TABLE `purchase_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_visa;
CREATE TABLE `purchase_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=296 DEFAULT CHARSET=utf8;
INSERT INTO purchase_visa VALUES("45","1","2016-09-03","2016-09-03","82","7","1","15000","0","0","0","0","0","0","0","15000","","","0","0","2","6","2");
INSERT INTO purchase_visa VALUES("46","1","2016-09-05","2016-09-05","94","7","1","5800","0","0","0","0","0","0","0","5800","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("47","2","2016-09-05","2016-09-05","94","7","2","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("48","3","2016-09-05","2016-09-05","94","7","3","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("49","4","2016-09-05","2016-09-05","97","7","4","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("50","5","2016-09-05","2016-09-05","94","7","5","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("51","6","2016-09-05","2016-09-05","94","7","6","5100","0","0","0","0","0","0","0","5100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("52","7","2016-09-05","2016-09-05","94","7","7","28200","0","0","0","0","0","0","0","28200","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("53","8","2016-09-05","2016-09-05","94","7","8","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("54","9","2016-09-05","2016-09-05","102","7","9","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("55","10","2016-09-05","2016-09-05","94","7","10","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("56","11","2016-09-05","2016-09-05","102","7","11","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("57","12","2016-09-05","2016-09-05","102","7","12","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("58","13","2016-09-05","2016-09-05","94","7","13","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("59","14","2016-09-05","2016-09-05","102","7","14","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("60","15","2016-09-05","2016-09-05","102","7","15","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("61","16","2016-09-05","2016-09-10","102","7","16","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("62","17","2016-09-05","2016-09-05","102","7","17","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("63","18","2016-09-06","2016-09-06","94","7","18","5100","0","0","0","0","0","0","0","5100","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("64","19","2016-09-06","2016-09-06","94","7","19","300","0","0","0","0","0","0","0","300","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("65","20","2016-09-06","2016-09-06","97","7","21","3300","0","0","0","0","0","0","0","3300","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("66","21","2016-09-06","2016-09-06","125","7","22","5300","0","0","0","0","0","0","0","5300","","","0","0","1","5","2");
INSERT INTO purchase_visa VALUES("67","22","2016-09-06","2016-09-10","127","7","23","15900","0","0","0","0","0","0","0","15900","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("68","23","2016-09-06","2016-09-06","127","7","24","15900","0","0","0","0","0","0","0","15900","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("69","24","2016-09-06","2016-09-06","102","7","25","14700","0","0","0","0","0","0","0","14700","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("70","25","2016-09-06","2016-09-06","94","7","26","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("71","26","2016-09-06","2016-09-06","94","7","27","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("72","27","2016-09-06","2016-09-06","94","7","28","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("73","28","2016-09-06","2016-09-06","94","7","29","300","0","0","0","0","0","0","0","300","","","0","0","1","2","2");
INSERT INTO purchase_visa VALUES("74","29","2016-09-06","2016-09-06","97","7","30","29600","0","0","0","0","0","0","0","29600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("75","30","2016-09-06","2016-09-06","125","7","31","15500","0","0","0","0","0","0","0","15500","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("76","31","2016-09-06","2016-09-06","94","7","32","40800","0","0","0","0","0","0","0","40800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("77","32","2016-09-07","2016-09-07","94","7","33","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("78","33","2016-09-07","2016-09-07","102","7","34","14700","0","0","0","0","0","0","0","14700","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("79","34","2016-09-07","2016-09-07","97","7","35","15800","0","0","0","0","0","0","0","15800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("80","35","2016-09-07","2016-09-07","94","7","36","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("81","36","2016-09-07","2016-09-07","127","7","37","5150","0","0","0","0","0","0","0","5150","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("82","37","2016-09-07","2016-09-07","132","7","38","29600","0","0","0","0","0","0","0","29600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("83","38","2016-09-07","2016-09-07","94","7","39","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("84","39","2016-09-07","2016-09-07","132","7","40","16000","0","0","0","0","0","0","0","16000","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("85","40","2016-09-07","2016-09-07","132","7","41","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("86","41","2016-09-07","2016-09-07","132","7","42","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("87","42","2016-09-07","2016-09-07","132","7","43","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("88","43","2016-09-07","2016-09-07","132","7","44","29600","0","0","0","0","0","0","0","29600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("89","44","2016-09-07","2016-09-07","132","7","45","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("90","45","2016-09-07","2016-09-07","132","7","46","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("91","46","2016-09-07","2016-09-07","94","7","47","183300","0","0","0","0","0","0","0","183300","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("92","47","2016-09-07","2016-09-07","94","7","48","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("93","48","2016-09-07","2016-09-07","132","7","49","44400","0","0","0","0","0","0","0","44400","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("94","49","2016-09-07","2016-09-07","94","7","50","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("95","50","2016-09-07","2016-09-07","132","7","51","29600","0","0","0","0","0","0","0","29600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("96","51","2016-09-07","2016-09-07","94","7","52","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("97","52","2016-09-07","2016-09-07","132","7","53","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("98","53","2016-09-07","2016-09-07","132","7","54","29600","0","0","0","0","0","0","0","29600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("99","54","2016-09-07","2016-09-07","94","7","55","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("100","55","2016-09-07","2016-09-07","94","7","56","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("101","56","2016-09-07","2016-09-07","94","7","57","5100","0","0","0","0","0","0","0","5100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("102","57","2016-09-07","2016-09-07","132","7","58","44400","0","0","0","0","0","0","0","44400","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("103","58","2016-09-07","2016-09-07","125","7","59","400","0","0","0","0","0","0","0","400","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("104","59","2016-09-07","2016-09-07","125","7","60","600","0","0","0","0","0","0","0","600","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("105","60","2016-09-07","2016-09-07","125","7","61","14600","0","0","0","0","0","0","0","14600","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("106","61","2016-09-07","2016-09-07","125","7","62","14200","0","0","0","0","0","0","0","14200","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("107","62","2016-09-07","2016-09-07","125","7","63","6900","0","0","0","0","0","0","0","6900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("108","63","2016-09-07","2016-09-07","125","7","64","4600","0","0","0","0","0","0","0","4600","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("109","64","2016-09-07","2016-09-07","125","7","65","2600","0","0","0","0","0","0","0","2600","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("110","65","2016-09-07","2016-09-07","125","7","66","14900","0","0","0","0","0","0","0","14900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("111","66","2016-09-07","2016-09-07","94","7","67","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("112","67","2016-09-07","2016-09-07","94","7","68","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("113","68","2016-09-07","2016-09-07","94","7","69","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("114","69","2016-09-07","2016-09-07","132","7","70","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("115","2","2016-09-07","2016-09-07","82","7","2","5500","0","0","0","0","0","0","0","5500","","","0","0","2","6","2");
INSERT INTO purchase_visa VALUES("116","70","2016-09-07","2016-09-07","125","7","71","7700","0","0","0","0","0","0","0","7700","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("118","72","2016-09-07","2016-09-07","125","7","74","14900","0","0","0","0","0","0","0","14900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("119","73","2016-09-07","2016-09-07","125","7","75","900","0","0","0","0","0","0","0","900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("120","74","2016-09-07","2016-09-07","125","7","76","58400","0","0","0","0","0","0","0","58400","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("121","75","2016-09-07","2016-09-07","125","7","77","88200","0","0","0","0","0","0","0","88200","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("122","76","2016-09-08","2016-09-08","97","7","78","8000","0","0","0","0","0","0","0","8000","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("123","77","2016-09-08","2016-09-08","140","7","79","5500","0","0","0","0","0","0","0","5500","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("124","78","2016-09-08","2016-09-08","140","7","80","6500","0","0","0","0","0","0","0","6500","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("125","79","2016-09-08","2016-09-08","94","7","81","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("126","80","2016-09-08","2016-09-08","125","7","82","16500","0","0","0","0","0","0","0","16500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("127","81","2016-09-08","2016-09-08","94","7","83","5100","0","0","0","0","0","0","0","5100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("128","82","2016-09-08","2016-09-08","94","7","84","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("129","83","2016-09-08","2016-09-08","94","7","85","5100","0","0","0","0","0","0","0","5100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("130","84","2016-09-08","2016-09-08","132","7","86","44400","0","0","0","0","0","0","0","44400","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("131","85","2016-09-08","2016-09-08","125","7","87","14500","0","0","0","0","0","0","0","14500","COUNTER 1204","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("132","86","2016-09-08","2016-09-08","125","7","88","11000","0","0","0","0","0","0","0","11000","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("133","87","2016-09-08","2016-09-08","147","7","89","900","0","0","0","0","0","0","0","900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("134","88","2016-09-08","2016-09-21","125","7","90","11100","0","0","0","0","0","0","0","11100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("135","89","2016-09-08","2016-09-09","97","7","91","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("137","91","2016-09-08","2016-09-21","97","7","94","6000","0","0","0","0","0","0","0","6000","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("138","92","2016-09-08","2016-09-09","97","7","95","1200","0","0","0","0","0","0","0","1200","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("139","93","2016-09-08","2016-09-08","125","7","96","15900","0","0","0","0","0","0","0","15900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("140","94","2016-09-08","2016-09-08","97","7","97","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("141","95","2016-09-08","2016-09-08","97","7","97","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("142","96","2016-09-08","2016-09-08","125","7","98","15900","0","0","0","0","0","0","0","15900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("143","97","2016-09-08","2016-09-08","97","7","99","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("144","98","2016-09-08","2016-09-08","125","7","100","7000","0","0","0","0","0","0","0","7000","COUNTER 1180","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("145","99","2016-09-08","2016-09-08","125","7","101","7000","0","0","0","0","0","0","0","7000","COUNTER 1182","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("146","100","2016-09-09","2016-09-09","132","7","102","15800","0","0","0","0","0","0","0","15800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("147","101","2016-09-09","2016-09-09","132","7","103","15800","0","0","0","0","0","0","0","15800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("148","102","2016-09-09","2016-09-09","125","7","104","78325","0","0","0","0","0","0","0","78325","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("149","103","2016-09-10","2016-09-10","94","7","105","5100","0","0","0","0","0","0","0","5100","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("150","104","2016-09-10","2016-09-10","94","7","106","20400","0","0","0","0","0","0","0","20400","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("151","105","2016-09-10","2016-09-10","94","7","107","5100","0","0","0","0","0","0","0","5100","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("152","106","2016-09-10","2016-09-10","97","7","108","11000","0","0","0","0","0","0","0","11000","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("153","107","2016-09-10","2016-09-10","132","7","109","15800","0","0","0","0","0","0","0","15800","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("154","108","2016-09-14","2016-09-14","94","7","110","14200","0","0","0","0","0","0","0","14200","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("155","3","2016-09-14","2016-09-14","157","7","3","15700","0","0","0","0","0","0","0","15700","","","0","0","2","6","2");
INSERT INTO purchase_visa VALUES("156","109","2016-09-15","2016-09-15","94","7","111","1200","0","0","0","0","0","0","0","1200","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("157","110","2016-09-16","2016-09-16","132","7","112","29600","0","0","0","0","0","0","0","29600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("158","111","2016-09-16","2016-09-16","94","7","113","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("159","112","2016-09-16","2016-09-16","94","7","114","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("160","113","2016-09-16","2016-09-16","94","7","115","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("161","114","2016-09-16","2016-09-16","132","7","116","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("162","115","2016-09-16","2016-09-16","132","7","117","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("163","116","2016-09-16","2016-09-16","132","7","118","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("164","117","2016-09-16","2016-09-16","132","7","119","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("165","118","2016-09-16","2016-09-16","94","7","120","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("166","119","2016-09-16","2016-09-16","132","7","121","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("167","120","2016-09-17","2016-09-17","94","7","122","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("168","121","2016-09-17","2016-09-17","132","7","123","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("169","122","2016-09-17","2016-09-17","94","7","124","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("170","123","2016-09-17","2016-09-17","94","7","125","5100","0","0","0","0","0","0","0","5100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("171","124","2016-09-17","2016-09-17","94","7","126","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("172","125","2016-09-17","2016-09-17","132","7","127","29200","0","0","0","0","0","0","0","29200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("173","126","2016-09-17","2016-09-17","94","7","128","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("174","127","2016-09-17","2016-09-17","132","7","129","58400","0","0","0","0","0","0","0","58400","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("175","128","2016-09-17","2016-09-17","94","7","130","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("176","129","2016-09-17","2016-09-17","132","7","131","29200","0","0","0","0","0","0","0","29200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("177","130","2016-09-17","2016-09-17","94","7","132","141000","0","0","0","0","0","0","0","141000","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("178","131","2016-09-17","2016-09-17","132","7","133","29200","0","0","0","0","0","0","0","29200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("179","132","2016-09-17","2016-09-17","94","7","134","15300","0","0","0","0","0","0","0","15300","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("180","133","2016-09-19","2016-09-19","132","7","135","29200","0","0","0","0","0","0","0","29200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("181","134","2016-09-19","2016-09-19","94","7","136","98700","0","0","0","0","0","0","0","98700","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("182","135","2016-09-19","2016-09-19","132","7","137","58400","0","0","0","0","0","0","0","58400","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("183","136","2016-09-19","2016-09-19","94","7","138","70500","0","0","0","0","0","0","0","70500","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("184","137","2016-09-19","2016-09-19","132","7","139","15600","0","0","0","0","0","0","0","15600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("185","138","2016-09-19","2016-09-19","132","7","140","87600","0","0","0","0","0","0","0","87600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("186","139","2016-09-19","2016-09-19","94","7","141","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("187","140","2016-09-19","2016-09-19","132","7","142","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("188","141","2016-09-19","2016-09-19","94","7","143","5100","0","0","0","0","0","0","0","5100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("189","142","2016-09-19","2016-09-19","132","7","144","29200","0","0","0","0","0","0","0","29200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("190","143","2016-09-19","2016-09-19","132","7","145","58400","0","0","0","0","0","0","0","58400","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("191","144","2016-09-19","2016-09-19","94","7","146","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("192","145","2016-09-19","2016-09-19","94","7","147","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("193","146","2016-09-19","2016-09-19","94","7","148","25500","0","0","0","0","0","0","0","25500","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("194","147","2016-09-19","2016-09-19","94","7","149","15300","0","0","0","0","0","0","0","15300","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("195","148","2016-09-19","2016-09-19","94","7","150","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("196","149","2016-09-19","2016-09-19","94","7","151","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("197","150","2016-09-19","2016-09-19","132","7","152","29200","0","0","0","0","0","0","0","29200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("198","151","2016-09-19","2016-09-19","132","7","153","10250","0","0","0","0","0","0","0","10250","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("199","152","2016-09-19","2016-09-19","94","7","154","900","0","0","0","0","0","0","0","900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("200","153","2016-09-19","2016-09-19","125","7","155","31400","0","0","0","0","0","0","0","31400","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("201","154","2016-09-19","2016-09-19","94","7","156","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("202","155","2016-09-19","2016-09-19","94","7","157","600","0","0","0","0","0","0","0","600","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("203","156","2016-09-19","2016-09-19","94","7","158","600","0","0","0","0","0","0","0","600","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("204","157","2016-09-19","2016-09-19","132","7","159","29200","0","0","0","0","0","0","0","29200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("205","158","2016-09-19","2016-09-19","132","7","160","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("206","159","2016-09-19","2016-09-19","94","7","161","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("207","160","2016-09-19","2016-09-19","94","7","162","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("208","161","2016-09-19","2016-09-19","94","7","163","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("209","162","2016-09-19","2016-09-19","132","7","164","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("210","163","2016-09-19","2016-09-19","94","7","165","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("211","164","2016-09-19","2016-09-19","94","7","166","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("212","165","2016-09-19","2016-09-19","94","7","167","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("213","166","2016-09-19","2016-09-19","132","7","168","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("214","167","2016-09-19","2016-09-19","94","7","169","40600","0","0","0","0","0","0","0","40600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("215","168","2016-09-19","2016-09-19","94","7","170","45675","0","0","0","0","0","0","0","45675","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("216","169","2016-09-19","2016-09-19","94","7","171","65975","0","0","0","0","0","0","0","65975","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("217","170","2016-09-19","2016-09-19","94","7","172","20300","0","0","0","0","0","0","0","20300","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("218","171","2016-09-19","2016-09-19","94","7","173","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("219","172","2016-09-19","2016-09-19","132","7","174","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("220","173","2016-09-19","2016-09-19","94","7","175","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("221","174","2016-09-19","2016-09-19","132","7","176","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("222","175","2016-09-19","2016-09-19","94","7","177","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("223","176","2016-09-19","2016-09-19","94","7","178","10150","0","0","0","0","0","0","0","10150","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("224","177","2016-09-19","2016-09-19","94","7","180","10150","0","0","0","0","0","0","0","10150","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("225","178","2016-09-19","2016-09-19","94","7","181","10150","0","0","0","0","0","0","0","10150","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("226","179","2016-09-19","2016-09-19","132","7","182","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("227","180","2016-09-19","2016-09-19","132","7","183","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("228","181","2016-09-19","2016-09-20","132","7","184","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("229","182","2016-09-20","2016-09-20","94","7","185","6100","0","0","0","0","0","0","0","6100","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("230","183","2016-09-20","2016-09-20","94","7","186","5500","0","0","0","0","0","0","0","5500","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("231","184","2016-09-20","2016-09-20","97","7","187","17200","0","0","0","0","0","0","0","17200","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("232","185","2016-09-20","2016-09-20","125","7","188","7800","0","0","0","0","0","0","0","7800","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("233","186","2016-09-20","2016-09-22","125","7","189","70200","0","0","0","0","0","0","0","70200","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("234","187","2016-09-20","2016-09-20","125","7","190","54700","0","0","0","0","0","0","0","54700","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("235","188","2016-09-20","2016-09-20","125","7","191","17400","0","0","0","0","0","0","0","17400","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("236","189","2016-09-20","2016-09-20","190","7","192","15000","0","0","0","0","0","0","0","15000","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("237","190","2016-09-20","2016-09-22","132","7","193","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("238","191","2016-09-20","2016-09-20","94","7","194","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("239","192","2016-09-20","2016-09-20","94","7","195","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("240","193","2016-09-20","2016-09-20","94","7","196","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("241","194","2016-09-20","2016-09-20","97","7","197","8000","0","0","0","0","0","0","0","8000","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("242","195","2016-09-20","2016-09-20","94","7","198","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("243","196","2016-09-20","2016-09-20","132","7","199","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("244","197","2016-09-20","2016-09-20","94","7","200","42300","0","0","0","0","0","0","0","42300","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("245","198","2016-09-20","2016-09-20","132","7","201","29200","0","0","0","0","0","0","0","29200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("246","199","2016-09-20","2016-09-20","94","7","202","84600","0","0","0","0","0","0","0","84600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("247","200","2016-09-20","2016-09-20","132","7","203","43800","0","0","0","0","0","0","0","43800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("248","201","2016-09-20","2016-09-20","94","7","204","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("249","202","2016-09-20","2016-09-20","132","7","205","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("250","203","2016-09-20","2016-09-20","94","7","206","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("251","204","2016-09-20","2016-09-20","94","7","207","5075","0","0","0","0","0","0","0","5075","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("252","205","2016-09-20","2016-09-20","94","7","208","14200","0","0","0","0","0","0","0","14200","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("253","206","2016-09-20","2016-09-20","125","7","209","1000","0","0","0","0","0","0","0","1000","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("254","207","2016-09-20","2016-09-20","132","7","212","14200","0","0","0","0","0","0","0","14200","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("255","208","2016-09-20","2016-09-20","127","7","213","10300","0","0","0","0","0","0","0","10300","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("256","209","2016-09-20","2016-09-20","94","7","214","45675","0","0","0","0","0","0","0","45675","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("257","210","2016-09-20","2016-09-20","132","7","215","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("258","211","2016-09-20","2016-09-20","94","7","216","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("259","212","2016-09-20","2016-09-20","94","7","217","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("260","213","2016-09-20","2016-09-22","94","7","218","5800","0","0","0","0","0","0","0","5800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("261","214","2016-09-20","2016-09-20","94","7","219","10200","0","0","0","0","0","0","0","10200","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("262","215","2016-09-20","2016-09-20","132","7","220","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("263","216","2016-09-20","2016-09-20","94","7","221","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("264","217","2016-09-20","2016-09-20","94","7","222","14200","0","0","0","0","0","0","0","14200","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("265","218","2016-09-20","2016-09-20","94","7","223","14200","0","0","0","0","0","0","0","14200","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("266","219","2016-09-20","2016-09-20","132","7","224","58400","0","0","0","0","0","0","0","58400","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("267","220","2016-09-20","2016-09-20","132","7","226","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("268","221","2016-09-20","2016-09-20","94","7","225","33000","0","0","0","0","0","0","0","33000","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("269","222","2016-09-20","2016-09-20","94","7","227","14200","0","0","0","0","0","0","0","14200","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("270","223","2016-09-20","2016-09-20","94","7","228","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("271","224","2016-09-20","2016-09-20","94","7","229","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("272","225","2016-09-20","2016-09-20","132","7","230","29200","0","0","0","0","0","0","0","29200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("273","226","2016-09-20","2016-09-20","132","7","231","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("274","227","2016-09-20","2016-09-20","132","7","232","5250","0","0","0","0","0","0","0","5250","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("275","228","2016-09-20","2016-09-20","94","7","233","42600","0","0","0","0","0","0","0","42600","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("276","229","2016-09-20","2016-09-20","94","7","234","42300","0","0","0","0","0","0","0","42300","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("277","230","2016-09-20","2016-09-20","132","7","235","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("278","231","2016-09-21","2016-09-21","97","7","236","5500","0","0","0","0","0","0","0","5500","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("279","232","2016-09-21","2016-09-21","94","7","237","15600","0","0","0","0","0","0","0","15600","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("280","233","2016-09-21","2016-09-21","94","7","238","5100","0","0","0","0","0","0","0","5100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("281","234","2016-09-21","2016-09-21","97","7","239","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("282","235","2016-09-22","2016-09-22","132","7","240","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("283","236","2016-09-22","2016-09-22","132","7","241","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("284","237","2016-09-22","2016-09-22","94","7","242","15225","0","0","0","0","0","0","0","15225","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("285","238","2016-09-22","2016-09-22","132","7","243","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("286","239","2016-09-22","2016-09-22","132","7","244","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("287","240","2016-09-22","2016-09-22","94","7","245","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("288","241","2016-09-22","2016-09-22","132","7","246","15600","0","0","0","0","0","0","0","15600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("289","242","2016-09-22","2016-09-22","94","7","247","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("290","243","2016-09-22","2016-09-22","94","7","248","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("291","244","2016-09-22","2016-09-22","132","7","249","14600","0","0","0","0","0","0","0","14600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("292","245","2016-09-22","2016-09-22","94","7","250","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("293","246","2016-09-22","2016-09-22","97","7","252","18900","0","0","0","0","0","0","0","18900","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("294","247","2016-09-22","2016-09-22","97","7","252","19000","0","0","0","0","0","0","0","19000","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("295","248","2016-09-22","2016-09-22","125","7","253","71800","0","0","0","0","0","0","0","71800","","","0","0","1","10","2");

DROP TABLE IF EXISTS relationship;
CREATE TABLE `relationship` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `relationship` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
INSERT INTO relationship VALUES("2","HUSBAND","1","36","2");
INSERT INTO relationship VALUES("3","SON","1","36","2");
INSERT INTO relationship VALUES("4","FATHER","1","36","2");
INSERT INTO relationship VALUES("5","BROTHER","1","36","2");
INSERT INTO relationship VALUES("6","GRAND FATHER","1","36","2");
INSERT INTO relationship VALUES("7","WOMEN GROUP","1","36","2");
INSERT INTO relationship VALUES("8","NEPHEW","1","36","2");
INSERT INTO relationship VALUES("9","HUSBAND BROTHER","1","36","2");
INSERT INTO relationship VALUES("10","FATHER IN LAW","1","36","2");
INSERT INTO relationship VALUES("11","FATHER BROTHER","1","36","2");
INSERT INTO relationship VALUES("12","GRAND SON","1","36","2");
INSERT INTO relationship VALUES("13","MOTHER BROTHER","1","36","2");
INSERT INTO relationship VALUES("14","SON IN LAW","1","36","2");

DROP TABLE IF EXISTS room_type;
CREATE TABLE `room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `roomType` varchar(30) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
INSERT INTO room_type VALUES("1","Sharing","0","1","0");
INSERT INTO room_type VALUES("2","Twin","0","1","0");
INSERT INTO room_type VALUES("3","Triple","0","1","0");
INSERT INTO room_type VALUES("4","Four Bed","0","1","0");
INSERT INTO room_type VALUES("5","Five Bed","0","1","0");
INSERT INTO room_type VALUES("6","Six Bed","0","1","0");
INSERT INTO room_type VALUES("7","Child With Bed","0","1","0");
INSERT INTO room_type VALUES("8","Child Without Bed","0","1","0");

DROP TABLE IF EXISTS staff;
CREATE TABLE `staff` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffName` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `userType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
INSERT INTO staff VALUES("1","Sidheeq","8547116314","tssidheeq@gmail.com","staf","1","2","2");
INSERT INTO staff VALUES("2","Manu","9544306950","mtmvettichira@gmail.com","","2","3","2");
INSERT INTO staff VALUES("3","Abdul Nasar","8281757040","mtmvettichira@gmail.com","","2","3","2");
INSERT INTO staff VALUES("4","Safuvan","9995959734","mtmsafuvan@gmail.com","VISA STAFF","1","2","2");
INSERT INTO staff VALUES("5","Sahad","9567656784","mtmsahad@gmail.com","VISA STAFF","1","2","2");
INSERT INTO staff VALUES("6","Irfad","9746915105","irfadamanath1@gmail.com","VISA STAFF","1","2","2");
INSERT INTO staff VALUES("7","Hyderali Staff","9562706022","hydermkm@gmail.com","TICKETING STAFF","1","2","2");
INSERT INTO staff VALUES("9","Muhammedsha","8089684701","muhammedshamtm@gmail.com","umrah","1","2","2");
INSERT INTO staff VALUES("11","Akhil","9486747270","akhilkpmtm@gmail.com","ACCOUNTS","1","2","2");
INSERT INTO staff VALUES("12","Hyder","9562706022","hydermkm@gmail.com","","1","2","2");
INSERT INTO staff VALUES("13","Hyderali","9846529001","hydermkm@gmail.com","","4","17","2");
INSERT INTO staff VALUES("14","Siraj Saquafi","9961953870","sirajsaquafimtm@gmail.com","Umra","4","17","2");
INSERT INTO staff VALUES("15","Faizal M","9605198598","","","4","17","2");
INSERT INTO staff VALUES("16","Muhammedsha","8089684701","muhammedsha87@gmail.com","umrah","4","17","2");
INSERT INTO staff VALUES("17","Akhil K P","9486747270","AKHILKP496@GMAIL.COM","ACCOUNTS","4","17","2");
INSERT INTO staff VALUES("18","Sadham","9999009999","","","4","17","2");
INSERT INTO staff VALUES("19","Faizyyyyy","9605198598","","","4","17","2");
INSERT INTO staff VALUES("22","Faizal","9605198598","FAIZAL@TRAVELMART.BIZ","ACCOUNTS","1","2","2");
INSERT INTO staff VALUES("23","Saifudheen","9846414143","SAIFUINBOX@GMAIL.COM","MANAGER","3","4","2");
INSERT INTO staff VALUES("24","Shamshad Ali","8281220920","SAMSADALIKKS65@GMAIL.COM","TICKETS","3","4","2");
INSERT INTO staff VALUES("25","Siraj Saquafi","9961953870","sirajsaquafimtm@gmail.com","Umra","1","2","2");
INSERT INTO staff VALUES("26","Sidheeqts","0","tssidheeq@gmail.com","","11","37","2");

DROP TABLE IF EXISTS transaction;
CREATE TABLE `transaction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `fromLedger` int(11) NOT NULL,
  `toLedger` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(100) NOT NULL,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `remark` text NOT NULL,
  `addedDate` date NOT NULL,
  `transactionDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `referenceNo` int(11) NOT NULL,
  `details` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1805 DEFAULT CHARSET=latin1;
INSERT INTO transaction VALUES("1","86","4","1","Airline Receivable","0","1000","Airline Receivable: 1","2016-09-01","2016-09-01","6","2","2","0","INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("2","4","86","1","Airline Receivable","1000","0","Airline Receivable: 1","2016-09-01","2016-09-01","6","2","2","0","INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("3","81","5","1","Airline Payable","1000","0","","2016-09-01","2016-09-01","6","2","2","0","Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("4","5","81","1","Airline Payable","0","1000","","2016-09-01","2016-09-01","6","2","2","0","Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("5","87","4","2","Airline Receivable","0","14050","Airline Receivable: 2","2016-09-01","2016-09-01","6","2","2","0","INV#IA2, Pax:THALE KUNNATH/JASEEM, Passport No:, Sector:DOH/CCJ, Date:07-12-2016, Tkt:607-9164761434, PNR:3I53TT, By:Manu");
INSERT INTO transaction VALUES("6","4","87","2","Airline Receivable","14050","0","Airline Receivable: 2","2016-09-01","2016-09-01","6","2","2","0","INV#IA2, Pax:THALE KUNNATH/JASEEM, Passport No:, Sector:DOH/CCJ, Date:07-12-2016, Tkt:607-9164761434, PNR:3I53TT, By:Manu");
INSERT INTO transaction VALUES("7","83","5","2","Airline Payable","14015","0","","2016-09-01","2016-09-01","6","2","2","0","Details:INV#IA2, Pax:THALE KUNNATH/JASEEM, Passport No:, Sector:DOH/CCJ, Date:07-12-2016, Tkt:607-9164761434, PNR:3I53TT, By:Manu");
INSERT INTO transaction VALUES("8","5","83","2","Airline Payable","0","14015","","2016-09-01","2016-09-01","6","2","2","0","Details:INV#IA2, Pax:THALE KUNNATH/JASEEM, Passport No:, Sector:DOH/CCJ, Date:07-12-2016, Tkt:607-9164761434, PNR:3I53TT, By:Manu");
INSERT INTO transaction VALUES("9","88","6","1","Visa Receivable","0","16500","Visa Receivable : 1","2016-09-03","2016-09-03","6","2","2","0","INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("10","6","88","1","Visa Receivable","16500","0","Visa Receivable : 1","2016-09-03","2016-09-03","6","2","2","0","INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("11","82","7","1","Visa Payable","15000","0","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("12","7","82","1","Visa Payable","0","15000","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("13","89","4","3","Airline Receivable","0","15000","Airline Receivable: 3","2016-09-03","2016-09-03","6","2","2","0","INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("14","4","89","3","Airline Receivable","15000","0","Airline Receivable: 3","2016-09-03","2016-09-03","6","2","2","0","INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("15","81","5","3","Airline Payable","14730","0","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("16","5","81","3","Airline Payable","0","14730","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("17","90","4","4","Airline Receivable","0","9500","Airline Receivable: 4","2016-09-03","2016-09-03","6","2","2","0","INV#IA4, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:CCJ/DXB, Date:22-12-2016, Tkt:098-9164977278, PNR:3OEJL9, By:Manu");
INSERT INTO transaction VALUES("18","4","90","4","Airline Receivable","9500","0","Airline Receivable: 4","2016-09-03","2016-09-03","6","2","2","0","INV#IA4, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:CCJ/DXB, Date:22-12-2016, Tkt:098-9164977278, PNR:3OEJL9, By:Manu");
INSERT INTO transaction VALUES("19","83","5","4","Airline Payable","9080","0","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA4, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:CCJ/DXB, Date:22-12-2016, Tkt:098-9164977278, PNR:3OEJL9, By:Manu");
INSERT INTO transaction VALUES("20","5","83","4","Airline Payable","0","9080","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA4, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:CCJ/DXB, Date:22-12-2016, Tkt:098-9164977278, PNR:3OEJL9, By:Manu");
INSERT INTO transaction VALUES("29","90","2","1","Cash Receipts","19100","0","","2016-09-03","2016-09-03","6","2","2","0","VouNo#CR 1,INV#IA4,IA5 Received By :Manu,Details:INV#IA5, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:COK/DMK, Date:17-10-2016, Tkt:CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("30","2","90","1","Cash Receipts","0","19100","","2016-09-03","2016-09-03","6","2","2","0","VouNo#CR 1,INV#IA4,IA5 Received By :Manu,Details:INV#IA5, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:COK/DMK, Date:17-10-2016, Tkt:CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("31","90","4","5","Airline Receivable","0","9600","Airline Receivable: 5","2016-09-03","2016-09-03","6","2","2","0","INV#IA5, Pax:Cheruvakkath/abdul Raheem, Passport No:H0109737, Sector:DMK/COK, Date:17-10-2016/19-10-2016, Tkt:CW3YVB/CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("32","4","90","5","Airline Receivable","9600","0","Airline Receivable: 5","2016-09-03","2016-09-03","6","2","2","0","INV#IA5, Pax:Cheruvakkath/abdul Raheem, Passport No:H0109737, Sector:DMK/COK, Date:17-10-2016/19-10-2016, Tkt:CW3YVB/CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("33","83","5","5","Airline Payable","8870","0","","2016-09-03","2016-09-03","6","2","2","0","INV#5,sup:83,:against:5,Details:INV#IA5, Pax:Cheruvakkath/abdul Raheem, Passport No:H0109737, Sector:DMK/COK, Date:17-10-2016/19-10-2016, Tkt:CW3YVB/CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("34","5","83","5","Airline Payable","0","8870","","2016-09-03","2016-09-03","6","2","2","0","INV#5,sup:83,:against:5,Details:INV#IA5, Pax:Cheruvakkath/abdul Raheem, Passport No:H0109737, Sector:DMK/COK, Date:17-10-2016/19-10-2016, Tkt:CW3YVB/CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("39","91","4","6","Airline Receivable","0","9700","Airline Receivable: 6","2016-09-03","2016-09-03","6","2","2","0","INV#IA6, Pax:ODUVIL/AYYOOB MR, Passport No:M7851287, Sector:SHJ/CCJ, Date:05-09-2016, Tkt:098-9164977299, PNR:0, By:Manu");
INSERT INTO transaction VALUES("40","4","91","6","Airline Receivable","9700","0","Airline Receivable: 6","2016-09-03","2016-09-03","6","2","2","0","INV#IA6, Pax:ODUVIL/AYYOOB MR, Passport No:M7851287, Sector:SHJ/CCJ, Date:05-09-2016, Tkt:098-9164977299, PNR:0, By:Manu");
INSERT INTO transaction VALUES("41","83","5","6","Airline Payable","9130","0","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA6, Pax:ODUVIL/AYYOOB MR, Passport No:M7851287, Sector:SHJ/CCJ, Date:05-09-2016, Tkt:098-9164977299, PNR:0, By:Manu");
INSERT INTO transaction VALUES("42","5","83","6","Airline Payable","0","9130","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA6, Pax:ODUVIL/AYYOOB MR, Passport No:M7851287, Sector:SHJ/CCJ, Date:05-09-2016, Tkt:098-9164977299, PNR:0, By:Manu");
INSERT INTO transaction VALUES("47","92","4","7","Airline Receivable","0","20600","Airline Receivable: 7","2016-09-05","2016-09-05","6","2","2","0","INV#IA7, Pax:Maliyekkal/muhammed Mukthar/POOVATIL/JUHANA , Passport No:M2664637, Sector:CCJ/AUH, Date:04-10-2016/04-10-2016, Tkt:607-9164977330/607-9164977328, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("48","4","92","7","Airline Receivable","20600","0","Airline Receivable: 7","2016-09-05","2016-09-05","6","2","2","0","INV#IA7, Pax:Maliyekkal/muhammed Mukthar/POOVATIL/JUHANA , Passport No:M2664637, Sector:CCJ/AUH, Date:04-10-2016/04-10-2016, Tkt:607-9164977330/607-9164977328, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("49","83","5","7","Airline Payable","20100","0","","2016-09-05","2016-09-05","6","2","2","0","INV#7,sup:83,:against:7,Details:INV#IA7, Pax:Maliyekkal/muhammed Mukthar/POOVATIL/JUHANA , Passport No:M2664637, Sector:CCJ/AUH, Date:04-10-2016/04-10-2016, Tkt:607-9164977330/607-9164977328, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("50","5","83","7","Airline Payable","0","20100","","2016-09-05","2016-09-05","6","2","2","0","INV#7,sup:83,:against:7,Details:INV#IA7, Pax:Maliyekkal/muhammed Mukthar/POOVATIL/JUHANA , Passport No:M2664637, Sector:CCJ/AUH, Date:04-10-2016/04-10-2016, Tkt:607-9164977330/607-9164977328, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("51","92","4","8","Airline Receivable","0","8700","Airline Receivable: 8","2016-09-05","2016-09-05","6","2","2","0","INV#IA8, Pax:POOVATIL/JUHANA , Passport No:M2664637, Sector:SHJ/CCJ, Date:01-01-2017, Tkt:589-9164977329, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("52","4","92","8","Airline Receivable","8700","0","Airline Receivable: 8","2016-09-05","2016-09-05","6","2","2","0","INV#IA8, Pax:POOVATIL/JUHANA , Passport No:M2664637, Sector:SHJ/CCJ, Date:01-01-2017, Tkt:589-9164977329, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("53","83","5","8","Airline Payable","8385","0","","2016-09-05","2016-09-05","6","2","2","0","Details:INV#IA8, Pax:POOVATIL/JUHANA , Passport No:M2664637, Sector:SHJ/CCJ, Date:01-01-2017, Tkt:589-9164977329, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("54","5","83","8","Airline Payable","0","8385","","2016-09-05","2016-09-05","6","2","2","0","Details:INV#IA8, Pax:POOVATIL/JUHANA , Passport No:M2664637, Sector:SHJ/CCJ, Date:01-01-2017, Tkt:589-9164977329, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("73","87","4","10","Airline Receivable","0","44600","Airline Receivable: 10","2016-09-05","2016-09-05","6","2","2","0","INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("74","4","87","10","Airline Receivable","44600","0","Airline Receivable: 10","2016-09-05","2016-09-05","6","2","2","0","INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("75","83","5","10","Airline Payable","43660","0","","2016-09-05","2016-09-05","6","2","2","0","INV#10,sup:83,:against:10,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("76","5","83","10","Airline Payable","0","43660","","2016-09-05","2016-09-05","6","2","2","0","INV#10,sup:83,:against:10,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("77","87","4","9","Airline Receivable","0","49000","Airline Receivable: 9","2016-09-05","2016-09-05","6","2","2","0","INV#IA9, Pax:Alqahtani/renad Faisal/ALQAHTANI/QAMRAA SAAD , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098 9164977342/098 9164977342/098-9164977341/098-9164977341, PNR:3OYGIE, By:Manu");
INSERT INTO transaction VALUES("78","4","87","9","Airline Receivable","49000","0","Airline Receivable: 9","2016-09-05","2016-09-05","6","2","2","0","INV#IA9, Pax:Alqahtani/renad Faisal/ALQAHTANI/QAMRAA SAAD , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098 9164977342/098 9164977342/098-9164977341/098-9164977341, PNR:3OYGIE, By:Manu");
INSERT INTO transaction VALUES("79","83","5","9","Airline Payable","47570","0","","2016-09-05","2016-09-05","6","2","2","0","INV#9,sup:83,:against:9,Details:INV#IA9, Pax:Alqahtani/renad Faisal/ALQAHTANI/QAMRAA SAAD , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098 9164977342/098 9164977342/098-9164977341/098-9164977341, PNR:3OYGIE, By:Manu");
INSERT INTO transaction VALUES("80","5","83","9","Airline Payable","0","47570","","2016-09-05","2016-09-05","6","2","2","0","INV#9,sup:83,:against:9,Details:INV#IA9, Pax:Alqahtani/renad Faisal/ALQAHTANI/QAMRAA SAAD , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098 9164977342/098 9164977342/098-9164977341/098-9164977341, PNR:3OYGIE, By:Manu");
INSERT INTO transaction VALUES("81","93","93","1","Opening Balance","0","35900","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#1, acc:GAMA HOLIDAYS CCJ,Opening:35900");
INSERT INTO transaction VALUES("82","1","93","1","Opening Balance","35900","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#1,acc:GAMA HOLIDAYS CCJ,Opening:35900");
INSERT INTO transaction VALUES("83","94","94","2","Opening Balance","83925","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#2, acc:SALIL PURCHASE,Opening:83925");
INSERT INTO transaction VALUES("84","1","94","2","Opening Balance","0","83925","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#2, acc:SALIL PURCHASE,Opening:83925");
INSERT INTO transaction VALUES("85","93","6","1","Visa Receivable","0","6200","Visa Receivable : 1","2016-09-05","2016-09-05","10","1","2","0","INV#IV1,Pax :MUFEED ABDUL AZIZ, PassNo :Z3668844/ visaFor:30 DAYS DXB SETTING,By:Irfad Staff");
INSERT INTO transaction VALUES("86","6","93","1","Visa Receivable","6200","0","Visa Receivable : 1","2016-09-05","2016-09-05","10","1","2","0","INV#IV1,Pax :MUFEED ABDUL AZIZ, PassNo :Z3668844/ visaFor:30 DAYS DXB SETTING,By:Irfad Staff");
INSERT INTO transaction VALUES("87","94","7","1","Visa Payable","5800","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("88","7","94","1","Visa Payable","0","5800","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("89","95","6","2","Visa Receivable","0","28700","Visa Receivable : 2","2016-09-05","2016-09-05","10","1","2","0","INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("90","6","95","2","Visa Receivable","28700","0","Visa Receivable : 2","2016-09-05","2016-09-05","10","1","2","0","INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("91","94","7","2","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("92","7","94","2","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("94","1","96","3","Opening Balance","130500","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#3,acc:SAFUVAN STAFF,Opening:130500");
INSERT INTO transaction VALUES("95","96","6","3","Visa Receivable","0","14800","Visa Receivable : 3","2016-09-05","2016-09-05","10","1","2","0","INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("96","6","96","3","Visa Receivable","14800","0","Visa Receivable : 3","2016-09-05","2016-09-05","10","1","2","0","INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("97","94","7","3","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("98","7","94","3","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("99","96","6","4","Visa Receivable","0","300","Visa Receivable : 4","2016-09-05","2016-09-05","10","1","2","0","INV#IV4,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("100","6","96","4","Visa Receivable","300","0","Visa Receivable : 4","2016-09-05","2016-09-05","10","1","2","0","INV#IV4,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("101","97","7","4","Visa Payable","300","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV4,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("102","7","97","4","Visa Payable","0","300","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV4,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("104","1","99","4","Opening Balance","94225","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#4,acc:ALHIND COK,Opening:94225");
INSERT INTO transaction VALUES("105","99","6","5","Visa Receivable","0","14600","Visa Receivable : 5","2016-09-05","2016-09-05","10","1","2","0","INV#IV5,Pax :SHAHUNL ABDULNASAR, PassNo :G8487574/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("106","6","99","5","Visa Receivable","14600","0","Visa Receivable : 5","2016-09-05","2016-09-05","10","1","2","0","INV#IV5,Pax :SHAHUNL ABDULNASAR, PassNo :G8487574/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("107","94","7","5","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV5,Pax :SHAHUNL ABDULNASAR, PassNo :G8487574/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("108","7","94","5","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV5,Pax :SHAHUNL ABDULNASAR, PassNo :G8487574/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("109","96","6","6","Visa Receivable","0","5200","Visa Receivable : 6","2016-09-05","2016-09-05","10","1","2","0","INV#IV6,Pax :SHAJAHAN PALLILATH, PassNo :H5103542/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("110","6","96","6","Visa Receivable","5200","0","Visa Receivable : 6","2016-09-05","2016-09-05","10","1","2","0","INV#IV6,Pax :SHAJAHAN PALLILATH, PassNo :H5103542/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("111","94","7","6","Visa Payable","5100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV6,Pax :SHAJAHAN PALLILATH, PassNo :H5103542/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("112","7","94","6","Visa Payable","0","5100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV6,Pax :SHAJAHAN PALLILATH, PassNo :H5103542/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("113","95","6","7","Visa Receivable","0","29200","Visa Receivable : 7","2016-09-05","2016-09-05","10","1","2","0","INV#IV7,Pax :SIMON, PassNo :J1485230/Pax :ELSY, PassNo :J1485339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("114","6","95","7","Visa Receivable","29200","0","Visa Receivable : 7","2016-09-05","2016-09-05","10","1","2","0","INV#IV7,Pax :SIMON, PassNo :J1485230/Pax :ELSY, PassNo :J1485339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("115","94","7","7","Visa Payable","28200","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV7,Pax :SIMON, PassNo :J1485230/Pax :ELSY, PassNo :J1485339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("116","7","94","7","Visa Payable","0","28200","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV7,Pax :SIMON, PassNo :J1485230/Pax :ELSY, PassNo :J1485339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("117","95","6","8","Visa Receivable","0","14600","Visa Receivable : 8","2016-09-05","2016-09-05","10","1","2","0","INV#IV8,Pax :AMEER, PassNo :M0314613/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("118","6","95","8","Visa Receivable","14600","0","Visa Receivable : 8","2016-09-05","2016-09-05","10","1","2","0","INV#IV8,Pax :AMEER, PassNo :M0314613/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("119","94","7","8","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV8,Pax :AMEER, PassNo :M0314613/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("120","7","94","8","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV8,Pax :AMEER, PassNo :M0314613/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("121","94","94","0","Opening Balance","0","83925","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#1, acc:SALIL PURCHASE,Opening:83925");
INSERT INTO transaction VALUES("122","1","94","0","Opening Balance","83925","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#1, acc:SALIL PURCHASE,Opening:83925");
INSERT INTO transaction VALUES("127","100","4","12","Airline Receivable","0","1800","Airline Receivable: 12","2016-09-05","2016-09-05","6","2","2","0","INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("128","4","100","12","Airline Receivable","1800","0","Airline Receivable: 12","2016-09-05","2016-09-05","6","2","2","0","INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("129","81","5","12","Airline Payable","1300","0","","2016-09-05","2016-09-05","6","2","2","0","Details:INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("130","5","81","12","Airline Payable","0","1300","","2016-09-05","2016-09-05","6","2","2","0","Details:INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("131","102","102","5","Opening Balance","0","90100","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#5, acc:TRAVEL PARTNER PURCHASE,Opening:90100");
INSERT INTO transaction VALUES("132","1","102","5","Opening Balance","90100","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#5, acc:TRAVEL PARTNER PURCHASE,Opening:90100");
INSERT INTO transaction VALUES("133","101","6","9","Visa Receivable","0","14800","Visa Receivable : 9","2016-09-05","2016-09-05","10","1","2","0","INV#IV9,Pax :ALLY JOY, PassNo :J0885006/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("134","6","101","9","Visa Receivable","14800","0","Visa Receivable : 9","2016-09-05","2016-09-05","10","1","2","0","INV#IV9,Pax :ALLY JOY, PassNo :J0885006/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("135","102","7","9","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV9,Pax :ALLY JOY, PassNo :J0885006/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("136","7","102","9","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV9,Pax :ALLY JOY, PassNo :J0885006/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("137","95","6","10","Visa Receivable","0","14600","Visa Receivable : 10","2016-09-05","2016-09-05","10","1","2","0","INV#IV10,Pax :DENIAL, PassNo :M9715530/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("138","6","95","10","Visa Receivable","14600","0","Visa Receivable : 10","2016-09-05","2016-09-05","10","1","2","0","INV#IV10,Pax :DENIAL, PassNo :M9715530/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("139","94","7","10","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV10,Pax :DENIAL, PassNo :M9715530/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("140","7","94","10","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV10,Pax :DENIAL, PassNo :M9715530/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("141","95","6","11","Visa Receivable","0","14700","Visa Receivable : 11","2016-09-05","2016-09-05","10","1","2","0","INV#IV11,Pax :SUSY, PassNo :N9538535/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("142","6","95","11","Visa Receivable","14700","0","Visa Receivable : 11","2016-09-05","2016-09-05","10","1","2","0","INV#IV11,Pax :SUSY, PassNo :N9538535/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("143","102","7","11","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV11,Pax :SUSY, PassNo :N9538535/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("144","7","102","11","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV11,Pax :SUSY, PassNo :N9538535/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("145","95","6","12","Visa Receivable","0","14700","Visa Receivable : 12","2016-09-05","2016-09-05","10","1","2","0","INV#IV12,Pax :RAHMATHUNNISA , PassNo :L9256080/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("146","6","95","12","Visa Receivable","14700","0","Visa Receivable : 12","2016-09-05","2016-09-05","10","1","2","0","INV#IV12,Pax :RAHMATHUNNISA , PassNo :L9256080/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("147","102","7","12","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV12,Pax :RAHMATHUNNISA , PassNo :L9256080/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("148","7","102","12","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV12,Pax :RAHMATHUNNISA , PassNo :L9256080/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("149","95","6","13","Visa Receivable","0","14600","Visa Receivable : 13","2016-09-05","2016-09-05","10","1","2","0","INV#IV13,Pax :SHANU MON, PassNo :J2491039/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("150","6","95","13","Visa Receivable","14600","0","Visa Receivable : 13","2016-09-05","2016-09-05","10","1","2","0","INV#IV13,Pax :SHANU MON, PassNo :J2491039/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("151","94","7","13","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV13,Pax :SHANU MON, PassNo :J2491039/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("152","7","94","13","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV13,Pax :SHANU MON, PassNo :J2491039/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("153","103","103","6","Opening Balance","0","2500","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#6, acc:MTM VETTICHIRA,Opening:2500");
INSERT INTO transaction VALUES("154","1","103","6","Opening Balance","2500","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#6,acc:MTM VETTICHIRA,Opening:2500");
INSERT INTO transaction VALUES("155","103","6","14","Visa Receivable","0","14700","Visa Receivable : 14","2016-09-05","2016-09-05","10","1","2","0","INV#IV14,Pax :MAIMOONA MANJAPPILLY, PassNo :M159786/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("156","6","103","14","Visa Receivable","14700","0","Visa Receivable : 14","2016-09-05","2016-09-05","10","1","2","0","INV#IV14,Pax :MAIMOONA MANJAPPILLY, PassNo :M159786/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("157","102","7","14","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV14,Pax :MAIMOONA MANJAPPILLY, PassNo :M159786/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("158","7","102","14","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV14,Pax :MAIMOONA MANJAPPILLY, PassNo :M159786/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("159","100","2","2","Cash Receipts","1800","0","","2016-09-05","2016-09-05","6","2","2","0","VouNo#CR 2,INV#IA12 Received By :Manu,Details:INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("160","2","100","2","Cash Receipts","0","1800","","2016-09-05","2016-09-05","6","2","2","0","VouNo#CR 2,INV#IA12 Received By :Manu,Details:INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("161","98","2","3","Cash Receipts","24800","0","","2016-09-05","2016-09-05","6","2","2","0","VouNo#CR 3,INV#IA11 Received By :Manu,Details:INV#IA11, Pax:AINIKKUNNAN JAYAFER, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("162","2","98","3","Cash Receipts","0","24800","","2016-09-05","2016-09-05","6","2","2","0","VouNo#CR 3,INV#IA11 Received By :Manu,Details:INV#IA11, Pax:AINIKKUNNAN JAYAFER, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("163","99","6","15","Visa Receivable","0","14800","Visa Receivable : 15","2016-09-05","2016-09-05","10","1","2","0","INV#IV15,Pax :MEENA HILARY, PassNo :M1342981/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("164","6","99","15","Visa Receivable","14800","0","Visa Receivable : 15","2016-09-05","2016-09-05","10","1","2","0","INV#IV15,Pax :MEENA HILARY, PassNo :M1342981/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("165","102","7","15","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV15,Pax :MEENA HILARY, PassNo :M1342981/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("166","7","102","15","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV15,Pax :MEENA HILARY, PassNo :M1342981/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("167","112","112","1","Opening Balance","0","1000","Opening Balance","2016-09-05","2016-09-05","17","4","2","0","INV#1, acc:AL HIND,Opening:1000");
INSERT INTO transaction VALUES("168","1","112","1","Opening Balance","1000","0","Opening Balance","2016-09-05","2016-09-05","17","4","2","0","INV#1, acc:AL HIND,Opening:1000");
INSERT INTO transaction VALUES("173","95","6","17","Visa Receivable","0","14600","Visa Receivable : 17","2016-09-05","2016-09-05","10","1","2","0","INV#IV17,Pax :AGHIL, PassNo :L9642581/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("174","6","95","17","Visa Receivable","14600","0","Visa Receivable : 17","2016-09-05","2016-09-05","10","1","2","0","INV#IV17,Pax :AGHIL, PassNo :L9642581/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("175","102","7","17","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV17,Pax :AGHIL, PassNo :L9642581/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("176","7","102","17","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV17,Pax :AGHIL, PassNo :L9642581/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("177","119","4","1","Airline Receivable","0","1000","Airline Receivable: 1","2016-09-05","2016-09-05","18","4","2","0","INV#IA1, Pax:RIYAS , Passport No:0, Sector:CCJ/DXB, Date:08-09-2016, Tkt:06665879224, PNR:0, By:Hyderali");
INSERT INTO transaction VALUES("178","4","119","1","Airline Receivable","1000","0","Airline Receivable: 1","2016-09-05","2016-09-05","18","4","2","0","INV#IA1, Pax:RIYAS , Passport No:0, Sector:CCJ/DXB, Date:08-09-2016, Tkt:06665879224, PNR:0, By:Hyderali");
INSERT INTO transaction VALUES("179","119","19","1","Airline Receivable","0","65","Airline Receivable:Tds#1","2016-09-05","2016-09-05","18","4","2","0","INV#IA1, Pax:RIYAS , Passport No:0, Sector:CCJ/DXB, Date:08-09-2016, Tkt:06665879224, PNR:0, By:Hyderali");
INSERT INTO transaction VALUES("180","19","119","1","Airline Receivable","65","0","Airline Receivable:Tds#1","2016-09-05","2016-09-05","18","4","2","0","INV#IA1, Pax:RIYAS , Passport No:0, Sector:CCJ/DXB, Date:08-09-2016, Tkt:06665879224, PNR:0, By:Hyderali");
INSERT INTO transaction VALUES("181","118","4","2","Airline Receivable","0","15100","Airline Receivable: 2","2016-09-05","2016-09-05","22","4","2","0","INV#IA2, Pax:ARUNCUT, Passport No:AAAA, Sector:CCJ/DXB, Date:06-09-2016, Tkt:456456, PNR:0, By:Akhil K P");
INSERT INTO transaction VALUES("182","4","118","2","Airline Receivable","15100","0","Airline Receivable: 2","2016-09-05","2016-09-05","22","4","2","0","INV#IA2, Pax:ARUNCUT, Passport No:AAAA, Sector:CCJ/DXB, Date:06-09-2016, Tkt:456456, PNR:0, By:Akhil K P");
INSERT INTO transaction VALUES("183","96","6","18","Visa Receivable","0","5200","Visa Receivable : 18","2016-09-06","2016-09-06","8","1","2","0","INV#IV18,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan Staff");
INSERT INTO transaction VALUES("184","6","96","18","Visa Receivable","5200","0","Visa Receivable : 18","2016-09-06","2016-09-06","8","1","2","0","INV#IV18,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan Staff");
INSERT INTO transaction VALUES("185","94","7","18","Visa Payable","5100","0","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV18,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan Staff");
INSERT INTO transaction VALUES("186","7","94","18","Visa Payable","0","5100","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV18,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan Staff");
INSERT INTO transaction VALUES("187","96","6","19","Visa Receivable","0","300","Visa Receivable : 19","2016-09-06","2016-09-06","8","1","2","0","INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("188","6","96","19","Visa Receivable","300","0","Visa Receivable : 19","2016-09-06","2016-09-06","8","1","2","0","INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("189","94","7","19","Visa Payable","300","0","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("190","7","94","19","Visa Payable","0","300","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("191","123","6","20","Visa Receivable","0","3800","Visa Receivable : 20","2016-09-06","2016-09-06","8","1","2","0","INV#IV20,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("192","6","123","20","Visa Receivable","3800","0","Visa Receivable : 20","2016-09-06","2016-09-06","8","1","2","0","INV#IV20,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("193","123","6","21","Visa Receivable","0","3800","Visa Receivable : 21","2016-09-06","2016-09-06","8","1","2","0","INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("194","6","123","21","Visa Receivable","3800","0","Visa Receivable : 21","2016-09-06","2016-09-06","8","1","2","0","INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("195","16","123","21","Visa Receivable","0","100","Visa Receivable : 21","2016-09-06","2016-09-06","8","1","2","0","INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("196","123","16","21","Visa Receivable","100","0","Visa Receivable : 21","2016-09-06","2016-09-06","8","1","2","0","INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("197","97","7","20","Visa Payable","3300","0","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("198","7","97","20","Visa Payable","0","3300","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("203","124","6","22","Visa Receivable","0","5300","Visa Receivable : 22","2016-09-06","2016-09-06","5","1","2","0","INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("204","6","124","22","Visa Receivable","5300","0","Visa Receivable : 22","2016-09-06","2016-09-06","5","1","2","0","INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("205","125","7","21","Visa Payable","5300","0","","2016-09-06","2016-09-06","5","1","2","0","Details:INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("206","7","125","21","Visa Payable","0","5300","","2016-09-06","2016-09-06","5","1","2","0","Details:INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("207","124","2","1","Cash Receipts","5300","0","receipt for couter 16","2016-09-06","2016-09-06","5","1","2","0","VouNo#CR 1,INV#IV22 Received By :Sidheeq,Details:INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("208","2","124","1","Cash Receipts","0","5300","receipt for couter 16","2016-09-06","2016-09-06","5","1","2","0","VouNo#CR 1,INV#IV22 Received By :Sidheeq,Details:INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("217","96","2","2","Cash Receipts","5500","0","","2016-09-06","2016-09-06","15","1","2","0","VouNo#CR 2,INV#IV18,IV19 Received By :Akhil,Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("218","2","96","2","Cash Receipts","0","5500","","2016-09-06","2016-09-06","15","1","2","0","VouNo#CR 2,INV#IV18,IV19 Received By :Akhil,Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("223","126","4","13","Airline Receivable","0","32500","Airline Receivable: 13","2016-09-06","2016-09-06","6","2","2","0","INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("224","4","126","13","Airline Receivable","32500","0","Airline Receivable: 13","2016-09-06","2016-09-06","6","2","2","0","INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("225","81","5","13","Airline Payable","32220","0","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("226","5","81","13","Airline Payable","0","32220","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("231","96","6","24","Visa Receivable","0","16400","Visa Receivable : 24","2016-09-06","2016-09-06","8","1","2","0","INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("232","6","96","24","Visa Receivable","16400","0","Visa Receivable : 24","2016-09-06","2016-09-06","8","1","2","0","INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("233","127","7","23","Visa Payable","15900","0","","2016-09-06","2016-09-06","8","1","2","0","INV#23,sup:127,:against:24,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("234","7","127","23","Visa Payable","0","15900","","2016-09-06","2016-09-06","8","1","2","0","INV#23,sup:127,:against:24,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("239","127","2","1","Cash Payments","0","31800","","2016-09-06","2016-09-06","15","1","2","0","VouNo#CP1,INV#PV22,PV23 Paid By :Akhil To : ALTA CCJ,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("240","2","127","1","Cash Payments","31800","0","","2016-09-06","2016-09-06","15","1","2","0","VouNo#CP1,INV#PV22,PV23 Paid By :Akhil To : ALTA CCJ,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("241","128","4","14","Airline Receivable","0","22500","Airline Receivable: 14","2016-09-06","2016-09-06","6","2","2","0","INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("242","4","128","14","Airline Receivable","22500","0","Airline Receivable: 14","2016-09-06","2016-09-06","6","2","2","0","INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("243","83","5","14","Airline Payable","21245","0","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("244","5","83","14","Airline Payable","0","21245","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("245","89","4","15","Airline Receivable","0","129000","Airline Receivable: 15","2016-09-06","2016-09-06","6","2","2","0","INV#IA15, Pax:MACHINCHERY THOOMBIL, MOHAMED, Passport No:, Sector:CCJ/DXB, Date:16-12-2016/07-01-2017, Tkt:0B134I/0B134I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("246","4","89","15","Airline Receivable","129000","0","Airline Receivable: 15","2016-09-06","2016-09-06","6","2","2","0","INV#IA15, Pax:MACHINCHERY THOOMBIL, MOHAMED, Passport No:, Sector:CCJ/DXB, Date:16-12-2016/07-01-2017, Tkt:0B134I/0B134I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("247","81","5","15","Airline Payable","126500","0","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA15, Pax:MACHINCHERY THOOMBIL, MOHAMED, Passport No:, Sector:CCJ/DXB, Date:16-12-2016/07-01-2017, Tkt:0B134I/0B134I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("248","5","81","15","Airline Payable","0","126500","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA15, Pax:MACHINCHERY THOOMBIL, MOHAMED, Passport No:, Sector:CCJ/DXB, Date:16-12-2016/07-01-2017, Tkt:0B134I/0B134I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("249","87","4","16","Airline Receivable","0","18400","Airline Receivable: 16","2016-09-06","2016-09-06","6","2","2","0","INV#IA16, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977454/157-9164977454, PNR:0, By:Manu");
INSERT INTO transaction VALUES("250","4","87","16","Airline Receivable","18400","0","Airline Receivable: 16","2016-09-06","2016-09-06","6","2","2","0","INV#IA16, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977454/157-9164977454, PNR:0, By:Manu");
INSERT INTO transaction VALUES("251","83","5","16","Airline Payable","17930","0","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA16, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977454/157-9164977454, PNR:0, By:Manu");
INSERT INTO transaction VALUES("252","5","83","16","Airline Payable","0","17930","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA16, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977454/157-9164977454, PNR:0, By:Manu");
INSERT INTO transaction VALUES("253","87","4","17","Airline Receivable","0","400","Airline Receivable: 17","2016-09-06","2016-09-06","6","2","2","0","INV#IA17, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977450/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("254","4","87","17","Airline Receivable","400","0","Airline Receivable: 17","2016-09-06","2016-09-06","6","2","2","0","INV#IA17, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977450/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("255","83","5","17","Airline Payable","400","0","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA17, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977450/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("256","5","83","17","Airline Payable","0","400","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA17, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977450/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("257","129","6","25","Visa Receivable","0","15000","Visa Receivable : 25","2016-09-06","2016-09-06","9","1","2","0","INV#IV25,Pax :MUHAMMED ANSAB, PassNo :N1900984/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("258","6","129","25","Visa Receivable","15000","0","Visa Receivable : 25","2016-09-06","2016-09-06","9","1","2","0","INV#IV25,Pax :MUHAMMED ANSAB, PassNo :N1900984/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("259","102","7","24","Visa Payable","14700","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV25,Pax :MUHAMMED ANSAB, PassNo :N1900984/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("260","7","102","24","Visa Payable","0","14700","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV25,Pax :MUHAMMED ANSAB, PassNo :N1900984/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("261","95","6","26","Visa Receivable","0","14600","Visa Receivable : 26","2016-09-06","2016-09-06","9","1","2","0","INV#IV26,Pax :ANSAR, PassNo :P2698527/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("262","6","95","26","Visa Receivable","14600","0","Visa Receivable : 26","2016-09-06","2016-09-06","9","1","2","0","INV#IV26,Pax :ANSAR, PassNo :P2698527/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("263","94","7","25","Visa Payable","14100","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV26,Pax :ANSAR, PassNo :P2698527/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("264","7","94","25","Visa Payable","0","14100","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV26,Pax :ANSAR, PassNo :P2698527/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("265","129","6","27","Visa Receivable","0","15000","Visa Receivable : 27","2016-09-06","2016-09-06","9","1","2","0","INV#IV27,Pax :SAFNAS, PassNo :H6710271/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("266","6","129","27","Visa Receivable","15000","0","Visa Receivable : 27","2016-09-06","2016-09-06","9","1","2","0","INV#IV27,Pax :SAFNAS, PassNo :H6710271/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("267","94","7","26","Visa Payable","14100","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV27,Pax :SAFNAS, PassNo :H6710271/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("268","7","94","26","Visa Payable","0","14100","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV27,Pax :SAFNAS, PassNo :H6710271/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("269","130","6","28","Visa Receivable","0","14700","Visa Receivable : 28","2016-09-06","2016-09-06","9","1","2","0","INV#IV28,Pax :HASSAN PUTHIYATI, PassNo :G9210752/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("270","6","130","28","Visa Receivable","14700","0","Visa Receivable : 28","2016-09-06","2016-09-06","9","1","2","0","INV#IV28,Pax :HASSAN PUTHIYATI, PassNo :G9210752/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("271","94","7","27","Visa Payable","14100","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV28,Pax :HASSAN PUTHIYATI, PassNo :G9210752/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("272","7","94","27","Visa Payable","0","14100","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV28,Pax :HASSAN PUTHIYATI, PassNo :G9210752/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("273","95","6","29","Visa Receivable","0","300","Visa Receivable : 29","2016-09-06","2016-09-06","2","1","2","0","INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("274","6","95","29","Visa Receivable","300","0","Visa Receivable : 29","2016-09-06","2016-09-06","2","1","2","0","INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("275","94","7","28","Visa Payable","300","0","","2016-09-06","2016-09-06","2","1","2","0","Details:INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("276","7","94","28","Visa Payable","0","300","","2016-09-06","2016-09-06","2","1","2","0","Details:INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("277","130","6","30","Visa Receivable","0","29600","Visa Receivable : 30","2016-09-06","2016-09-06","9","1","2","0","INV#IV30,Pax :HEZZA, PassNo :N8801988/Pax :ISMATHUL HAMRA, PassNo :N8239724/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("278","6","130","30","Visa Receivable","29600","0","Visa Receivable : 30","2016-09-06","2016-09-06","9","1","2","0","INV#IV30,Pax :HEZZA, PassNo :N8801988/Pax :ISMATHUL HAMRA, PassNo :N8239724/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("279","97","7","29","Visa Payable","29600","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV30,Pax :HEZZA, PassNo :N8801988/Pax :ISMATHUL HAMRA, PassNo :N8239724/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("280","7","97","29","Visa Payable","0","29600","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV30,Pax :HEZZA, PassNo :N8801988/Pax :ISMATHUL HAMRA, PassNo :N8239724/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("281","96","6","31","Visa Receivable","0","16500","Visa Receivable : 31","2016-09-06","2016-09-06","8","1","2","0","INV#IV31,Pax :MTM570 JAMAL YASAR, PassNo :H3365841/ visaFor:30 DAYS SAUDI,By:Safuvan Staff");
INSERT INTO transaction VALUES("282","6","96","31","Visa Receivable","16500","0","Visa Receivable : 31","2016-09-06","2016-09-06","8","1","2","0","INV#IV31,Pax :MTM570 JAMAL YASAR, PassNo :H3365841/ visaFor:30 DAYS SAUDI,By:Safuvan Staff");
INSERT INTO transaction VALUES("283","125","7","30","Visa Payable","15500","0","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV31,Pax :MTM570 JAMAL YASAR, PassNo :H3365841/ visaFor:30 DAYS SAUDI,By:Safuvan Staff");
INSERT INTO transaction VALUES("284","7","125","30","Visa Payable","0","15500","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV31,Pax :MTM570 JAMAL YASAR, PassNo :H3365841/ visaFor:30 DAYS SAUDI,By:Safuvan Staff");
INSERT INTO transaction VALUES("287","130","6","32","Visa Receivable","0","40800","Visa Receivable : 32","2016-09-06","2016-09-06","9","1","2","0","INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("288","6","130","32","Visa Receivable","40800","0","Visa Receivable : 32","2016-09-06","2016-09-06","9","1","2","0","INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("289","94","7","31","Visa Payable","40800","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("290","7","94","31","Visa Payable","0","40800","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("291","123","2","6","Cash Receipts","3700","0","vist visa","2016-09-06","2016-09-06","15","1","2","0","VouNo#CR 6,INV#IV21 Received By :Akhil,Details:INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("292","2","123","6","Cash Receipts","0","3700","vist visa","2016-09-06","2016-09-06","15","1","2","0","VouNo#CR 6,INV#IV21 Received By :Akhil,Details:INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("293","93","6","33","Visa Receivable","0","14800","Visa Receivable : 33","2016-09-07","2016-09-07","9","1","2","0","INV#IV33,Pax :ANSIL, PassNo :L1756857/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("294","6","93","33","Visa Receivable","14800","0","Visa Receivable : 33","2016-09-07","2016-09-07","9","1","2","0","INV#IV33,Pax :ANSIL, PassNo :L1756857/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("295","94","7","32","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV33,Pax :ANSIL, PassNo :L1756857/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("296","7","94","32","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV33,Pax :ANSIL, PassNo :L1756857/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("297","95","6","34","Visa Receivable","0","14700","Visa Receivable : 34","2016-09-07","2016-09-07","9","1","2","0","INV#IV34,Pax :ZAINABA, PassNo :J8564596/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("298","6","95","34","Visa Receivable","14700","0","Visa Receivable : 34","2016-09-07","2016-09-07","9","1","2","0","INV#IV34,Pax :ZAINABA, PassNo :J8564596/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("299","102","7","33","Visa Payable","14700","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV34,Pax :ZAINABA, PassNo :J8564596/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("300","7","102","33","Visa Payable","0","14700","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV34,Pax :ZAINABA, PassNo :J8564596/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("301","93","6","35","Visa Receivable","0","16200","Visa Receivable : 35","2016-09-07","2016-09-07","9","1","2","0","INV#IV35,Pax :IBRAHIM KUTTY, PassNo :P2813905/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("302","6","93","35","Visa Receivable","16200","0","Visa Receivable : 35","2016-09-07","2016-09-07","9","1","2","0","INV#IV35,Pax :IBRAHIM KUTTY, PassNo :P2813905/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("303","97","7","34","Visa Payable","15800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV35,Pax :IBRAHIM KUTTY, PassNo :P2813905/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("304","7","97","34","Visa Payable","0","15800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV35,Pax :IBRAHIM KUTTY, PassNo :P2813905/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("305","129","6","36","Visa Receivable","0","15000","Visa Receivable : 36","2016-09-07","2016-09-07","9","1","2","0","INV#IV36,Pax :MUHAMMED, PassNo :N5120158/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("306","6","129","36","Visa Receivable","15000","0","Visa Receivable : 36","2016-09-07","2016-09-07","9","1","2","0","INV#IV36,Pax :MUHAMMED, PassNo :N5120158/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("307","94","7","35","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV36,Pax :MUHAMMED, PassNo :N5120158/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("308","7","94","35","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV36,Pax :MUHAMMED, PassNo :N5120158/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("309","101","6","37","Visa Receivable","0","5200","Visa Receivable : 37","2016-09-07","2016-09-07","9","1","2","0","INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("310","6","101","37","Visa Receivable","5200","0","Visa Receivable : 37","2016-09-07","2016-09-07","9","1","2","0","INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("311","127","7","36","Visa Payable","5150","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("312","7","127","36","Visa Payable","0","5150","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("313","95","6","38","Visa Receivable","0","29600","Visa Receivable : 38","2016-09-07","2016-09-07","9","1","2","0","INV#IV38,Pax :SAROJA, PassNo :K6272814/Pax :JAFIN, PassNo :M3827069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("314","6","95","38","Visa Receivable","29600","0","Visa Receivable : 38","2016-09-07","2016-09-07","9","1","2","0","INV#IV38,Pax :SAROJA, PassNo :K6272814/Pax :JAFIN, PassNo :M3827069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("315","132","7","37","Visa Payable","29600","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV38,Pax :SAROJA, PassNo :K6272814/Pax :JAFIN, PassNo :M3827069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("316","7","132","37","Visa Payable","0","29600","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV38,Pax :SAROJA, PassNo :K6272814/Pax :JAFIN, PassNo :M3827069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("317","114","6","39","Visa Receivable","0","29600","Visa Receivable : 39","2016-09-07","2016-09-07","9","1","2","0","INV#IV39,Pax :CHANDRAN PULIKKATT, PassNo :J5352156/Pax :SHEEJA, PassNo :N4524757/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("318","6","114","39","Visa Receivable","29600","0","Visa Receivable : 39","2016-09-07","2016-09-07","9","1","2","0","INV#IV39,Pax :CHANDRAN PULIKKATT, PassNo :J5352156/Pax :SHEEJA, PassNo :N4524757/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("319","94","7","38","Visa Payable","28200","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV39,Pax :CHANDRAN PULIKKATT, PassNo :J5352156/Pax :SHEEJA, PassNo :N4524757/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("320","7","94","38","Visa Payable","0","28200","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV39,Pax :CHANDRAN PULIKKATT, PassNo :J5352156/Pax :SHEEJA, PassNo :N4524757/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("321","114","6","40","Visa Receivable","0","16000","Visa Receivable : 40","2016-09-07","2016-09-07","9","1","2","0","INV#IV40,Pax :NILAA NIKHIL, PassNo :P0422439/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("322","6","114","40","Visa Receivable","16000","0","Visa Receivable : 40","2016-09-07","2016-09-07","9","1","2","0","INV#IV40,Pax :NILAA NIKHIL, PassNo :P0422439/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("323","132","7","39","Visa Payable","16000","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV40,Pax :NILAA NIKHIL, PassNo :P0422439/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("324","7","132","39","Visa Payable","0","16000","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV40,Pax :NILAA NIKHIL, PassNo :P0422439/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("325","130","6","41","Visa Receivable","0","14700","Visa Receivable : 41","2016-09-07","2016-09-07","9","1","2","0","INV#IV41,Pax :JAMES CLEETUS, PassNo :N5427392/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("326","6","130","41","Visa Receivable","14700","0","Visa Receivable : 41","2016-09-07","2016-09-07","9","1","2","0","INV#IV41,Pax :JAMES CLEETUS, PassNo :N5427392/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("327","132","7","40","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV41,Pax :JAMES CLEETUS, PassNo :N5427392/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("328","7","132","40","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV41,Pax :JAMES CLEETUS, PassNo :N5427392/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("329","95","6","42","Visa Receivable","0","14700","Visa Receivable : 42","2016-09-07","2016-09-07","9","1","2","0","INV#IV42,Pax :MERLIN BABU, PassNo :M0477973/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("330","6","95","42","Visa Receivable","14700","0","Visa Receivable : 42","2016-09-07","2016-09-07","9","1","2","0","INV#IV42,Pax :MERLIN BABU, PassNo :M0477973/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("331","132","7","41","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV42,Pax :MERLIN BABU, PassNo :M0477973/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("332","7","132","41","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV42,Pax :MERLIN BABU, PassNo :M0477973/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("333","99","6","43","Visa Receivable","0","14800","Visa Receivable : 43","2016-09-07","2016-09-07","9","1","2","0","INV#IV43,Pax :ASHNA, PassNo :M6093308/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("334","6","99","43","Visa Receivable","14800","0","Visa Receivable : 43","2016-09-07","2016-09-07","9","1","2","0","INV#IV43,Pax :ASHNA, PassNo :M6093308/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("335","132","7","42","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV43,Pax :ASHNA, PassNo :M6093308/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("336","7","132","42","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV43,Pax :ASHNA, PassNo :M6093308/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("337","95","6","44","Visa Receivable","0","29400","Visa Receivable : 44","2016-09-07","2016-09-07","9","1","2","0","INV#IV44,Pax :SHAMEEM, PassNo :K3072306/Pax :SRIMATHY, PassNo :P4071810/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("338","6","95","44","Visa Receivable","29400","0","Visa Receivable : 44","2016-09-07","2016-09-07","9","1","2","0","INV#IV44,Pax :SHAMEEM, PassNo :K3072306/Pax :SRIMATHY, PassNo :P4071810/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("339","132","7","43","Visa Payable","29600","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV44,Pax :SHAMEEM, PassNo :K3072306/Pax :SRIMATHY, PassNo :P4071810/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("340","7","132","43","Visa Payable","0","29600","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV44,Pax :SHAMEEM, PassNo :K3072306/Pax :SRIMATHY, PassNo :P4071810/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("341","129","6","45","Visa Receivable","0","15100","Visa Receivable : 45","2016-09-07","2016-09-07","9","1","2","0","INV#IV45,Pax :SHEEBA, PassNo :N5159159/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("342","6","129","45","Visa Receivable","15100","0","Visa Receivable : 45","2016-09-07","2016-09-07","9","1","2","0","INV#IV45,Pax :SHEEBA, PassNo :N5159159/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("343","132","7","44","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV45,Pax :SHEEBA, PassNo :N5159159/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("344","7","132","44","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV45,Pax :SHEEBA, PassNo :N5159159/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("345","99","6","46","Visa Receivable","0","14600","Visa Receivable : 46","2016-09-07","2016-09-07","9","1","2","0","INV#IV46,Pax :JUSTIN JAMES, PassNo :L5914253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("346","6","99","46","Visa Receivable","14600","0","Visa Receivable : 46","2016-09-07","2016-09-07","9","1","2","0","INV#IV46,Pax :JUSTIN JAMES, PassNo :L5914253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("347","132","7","45","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV46,Pax :JUSTIN JAMES, PassNo :L5914253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("348","7","132","45","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV46,Pax :JUSTIN JAMES, PassNo :L5914253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("349","95","6","47","Visa Receivable","0","189800","Visa Receivable : 47","2016-09-07","2016-09-07","9","1","2","0","INV#IV47,Pax :ASOK KUMAR, PassNo :M1674522/Pax :DILEEP, PassNo :G6067478/Pax :HASHIM, PassNo :H6275220/Pax :RAMAKRISHNAN, PassNo :G6160411/Pax :JERIN JAMES, PassNo :K5427677/Pax :SABEEH, PassNo :H3049393/Pax :FAISAL, PassNo :K3033987/Pax :UMMER, PassNo :M4950208/Pax :HAFIZ, PassNo :J2006760/Pax :KUNJHU MON, PassNo :M7807535/Pax :MADAVADAS, PassNo :J5254307/Pax :VENUGOPALAN, PassNo :H5709904/Pax :SAROJINI, PassNo :H5708579/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("350","6","95","47","Visa Receivable","189800","0","Visa Receivable : 47","2016-09-07","2016-09-07","9","1","2","0","INV#IV47,Pax :ASOK KUMAR, PassNo :M1674522/Pax :DILEEP, PassNo :G6067478/Pax :HASHIM, PassNo :H6275220/Pax :RAMAKRISHNAN, PassNo :G6160411/Pax :JERIN JAMES, PassNo :K5427677/Pax :SABEEH, PassNo :H3049393/Pax :FAISAL, PassNo :K3033987/Pax :UMMER, PassNo :M4950208/Pax :HAFIZ, PassNo :J2006760/Pax :KUNJHU MON, PassNo :M7807535/Pax :MADAVADAS, PassNo :J5254307/Pax :VENUGOPALAN, PassNo :H5709904/Pax :SAROJINI, PassNo :H5708579/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("351","94","7","46","Visa Payable","183300","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV47,Pax :ASOK KUMAR, PassNo :M1674522/Pax :DILEEP, PassNo :G6067478/Pax :HASHIM, PassNo :H6275220/Pax :RAMAKRISHNAN, PassNo :G6160411/Pax :JERIN JAMES, PassNo :K5427677/Pax :SABEEH, PassNo :H3049393/Pax :FAISAL, PassNo :K3033987/Pax :UMMER, PassNo :M4950208/Pax :HAFIZ, PassNo :J2006760/Pax :KUNJHU MON, PassNo :M7807535/Pax :MADAVADAS, PassNo :J5254307/Pax :VENUGOPALAN, PassNo :H5709904/Pax :SAROJINI, PassNo :H5708579/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("352","7","94","46","Visa Payable","0","183300","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV47,Pax :ASOK KUMAR, PassNo :M1674522/Pax :DILEEP, PassNo :G6067478/Pax :HASHIM, PassNo :H6275220/Pax :RAMAKRISHNAN, PassNo :G6160411/Pax :JERIN JAMES, PassNo :K5427677/Pax :SABEEH, PassNo :H3049393/Pax :FAISAL, PassNo :K3033987/Pax :UMMER, PassNo :M4950208/Pax :HAFIZ, PassNo :J2006760/Pax :KUNJHU MON, PassNo :M7807535/Pax :MADAVADAS, PassNo :J5254307/Pax :VENUGOPALAN, PassNo :H5709904/Pax :SAROJINI, PassNo :H5708579/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("353","99","6","48","Visa Receivable","0","29200","Visa Receivable : 48","2016-09-07","2016-09-07","9","1","2","0","INV#IV48,Pax :VARGHESE, PassNo :M832608/Pax :SUDHEESH, PassNo :N6950516/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("354","6","99","48","Visa Receivable","29200","0","Visa Receivable : 48","2016-09-07","2016-09-07","9","1","2","0","INV#IV48,Pax :VARGHESE, PassNo :M832608/Pax :SUDHEESH, PassNo :N6950516/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("355","94","7","47","Visa Payable","28200","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV48,Pax :VARGHESE, PassNo :M832608/Pax :SUDHEESH, PassNo :N6950516/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("356","7","94","47","Visa Payable","0","28200","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV48,Pax :VARGHESE, PassNo :M832608/Pax :SUDHEESH, PassNo :N6950516/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("357","136","6","49","Visa Receivable","0","46200","Visa Receivable : 49","2016-09-07","2016-09-07","9","1","2","0","INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("358","6","136","49","Visa Receivable","46200","0","Visa Receivable : 49","2016-09-07","2016-09-07","9","1","2","0","INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("359","132","7","48","Visa Payable","44400","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("360","7","132","48","Visa Payable","0","44400","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("361","95","6","50","Visa Receivable","0","29200","Visa Receivable : 50","2016-09-07","2016-09-07","9","1","2","0","INV#IV50,Pax :ARIF, PassNo :M9418291/Pax :BONEY JOSEPH, PassNo :G8586116/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("362","6","95","50","Visa Receivable","29200","0","Visa Receivable : 50","2016-09-07","2016-09-07","9","1","2","0","INV#IV50,Pax :ARIF, PassNo :M9418291/Pax :BONEY JOSEPH, PassNo :G8586116/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("363","94","7","49","Visa Payable","28200","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV50,Pax :ARIF, PassNo :M9418291/Pax :BONEY JOSEPH, PassNo :G8586116/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("364","7","94","49","Visa Payable","0","28200","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV50,Pax :ARIF, PassNo :M9418291/Pax :BONEY JOSEPH, PassNo :G8586116/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("365","95","6","51","Visa Receivable","0","29400","Visa Receivable : 51","2016-09-07","2016-09-07","9","1","2","0","INV#IV51,Pax :SALIHA, PassNo :M0174249/Pax :NIMMY, PassNo :L2844381/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("366","6","95","51","Visa Receivable","29400","0","Visa Receivable : 51","2016-09-07","2016-09-07","9","1","2","0","INV#IV51,Pax :SALIHA, PassNo :M0174249/Pax :NIMMY, PassNo :L2844381/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("367","132","7","50","Visa Payable","29600","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV51,Pax :SALIHA, PassNo :M0174249/Pax :NIMMY, PassNo :L2844381/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("368","7","132","50","Visa Payable","0","29600","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV51,Pax :SALIHA, PassNo :M0174249/Pax :NIMMY, PassNo :L2844381/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("369","81","2","1","Cash Payments","0","15730","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP1,INV#PA1,PA3 Paid By :Manu To : MTM  CCJ,Details:INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("370","2","81","1","Cash Payments","15730","0","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP1,INV#PA1,PA3 Paid By :Manu To : MTM  CCJ,Details:INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("371","129","6","52","Visa Receivable","0","15000","Visa Receivable : 52","2016-09-07","2016-09-07","9","1","2","0","INV#IV52,Pax :SARATH KRISHNA, PassNo :K6243609/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("372","6","129","52","Visa Receivable","15000","0","Visa Receivable : 52","2016-09-07","2016-09-07","9","1","2","0","INV#IV52,Pax :SARATH KRISHNA, PassNo :K6243609/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("373","94","7","51","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV52,Pax :SARATH KRISHNA, PassNo :K6243609/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("374","7","94","51","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV52,Pax :SARATH KRISHNA, PassNo :K6243609/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("375","101","6","53","Visa Receivable","0","14800","Visa Receivable : 53","2016-09-07","2016-09-07","9","1","2","0","INV#IV53,Pax :HABEEBA SHAFEEEK, PassNo :P2642443/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("376","6","101","53","Visa Receivable","14800","0","Visa Receivable : 53","2016-09-07","2016-09-07","9","1","2","0","INV#IV53,Pax :HABEEBA SHAFEEEK, PassNo :P2642443/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("377","132","7","52","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV53,Pax :HABEEBA SHAFEEEK, PassNo :P2642443/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("378","7","132","52","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV53,Pax :HABEEBA SHAFEEEK, PassNo :P2642443/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("379","101","6","54","Visa Receivable","0","29600","Visa Receivable : 54","2016-09-07","2016-09-07","9","1","2","0","INV#IV54,Pax :HASNA, PassNo :P1163918/Pax :ZAHIDA, PassNo :M9909492/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("380","6","101","54","Visa Receivable","29600","0","Visa Receivable : 54","2016-09-07","2016-09-07","9","1","2","0","INV#IV54,Pax :HASNA, PassNo :P1163918/Pax :ZAHIDA, PassNo :M9909492/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("381","132","7","53","Visa Payable","29600","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV54,Pax :HASNA, PassNo :P1163918/Pax :ZAHIDA, PassNo :M9909492/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("382","7","132","53","Visa Payable","0","29600","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV54,Pax :HASNA, PassNo :P1163918/Pax :ZAHIDA, PassNo :M9909492/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("385","99","6","55","Visa Receivable","0","14600","Visa Receivable : 55","2016-09-07","2016-09-07","9","1","2","0","INV#IV55,Pax :DAISAN DAVID, PassNo :N1142416/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("386","6","99","55","Visa Receivable","14600","0","Visa Receivable : 55","2016-09-07","2016-09-07","9","1","2","0","INV#IV55,Pax :DAISAN DAVID, PassNo :N1142416/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("387","94","7","54","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV55,Pax :DAISAN DAVID, PassNo :N1142416/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("388","7","94","54","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV55,Pax :DAISAN DAVID, PassNo :N1142416/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("389","95","6","56","Visa Receivable","0","29200","Visa Receivable : 56","2016-09-07","2016-09-07","9","1","2","0","INV#IV56,Pax :SIJO JOSEPH, PassNo :N8591047/Pax :AJAY KRISHNA, PassNo :G9194546/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("390","6","95","56","Visa Receivable","29200","0","Visa Receivable : 56","2016-09-07","2016-09-07","9","1","2","0","INV#IV56,Pax :SIJO JOSEPH, PassNo :N8591047/Pax :AJAY KRISHNA, PassNo :G9194546/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("391","94","7","55","Visa Payable","28200","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV56,Pax :SIJO JOSEPH, PassNo :N8591047/Pax :AJAY KRISHNA, PassNo :G9194546/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("392","7","94","55","Visa Payable","0","28200","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV56,Pax :SIJO JOSEPH, PassNo :N8591047/Pax :AJAY KRISHNA, PassNo :G9194546/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("393","130","6","57","Visa Receivable","0","5100","Visa Receivable : 57","2016-09-07","2016-09-07","9","1","2","0","INV#IV57,Pax :ABDUL AZEEZ, PassNo :L8040492/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("394","6","130","57","Visa Receivable","5100","0","Visa Receivable : 57","2016-09-07","2016-09-07","9","1","2","0","INV#IV57,Pax :ABDUL AZEEZ, PassNo :L8040492/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("395","94","7","56","Visa Payable","5100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV57,Pax :ABDUL AZEEZ, PassNo :L8040492/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("396","7","94","56","Visa Payable","0","5100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV57,Pax :ABDUL AZEEZ, PassNo :L8040492/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("397","93","6","58","Visa Receivable","0","45300","Visa Receivable : 58","2016-09-07","2016-09-07","9","1","2","0","INV#IV58,Pax :NAFSIYA KOLAMBIL, PassNo :G4454840/Pax :RSHAN AHAMMED, PassNo :P0352855/Pax :TANIA ISRA, PassNo :K5269033/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("398","6","93","58","Visa Receivable","45300","0","Visa Receivable : 58","2016-09-07","2016-09-07","9","1","2","0","INV#IV58,Pax :NAFSIYA KOLAMBIL, PassNo :G4454840/Pax :RSHAN AHAMMED, PassNo :P0352855/Pax :TANIA ISRA, PassNo :K5269033/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("399","132","7","57","Visa Payable","44400","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV58,Pax :NAFSIYA KOLAMBIL, PassNo :G4454840/Pax :RSHAN AHAMMED, PassNo :P0352855/Pax :TANIA ISRA, PassNo :K5269033/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("400","7","132","57","Visa Payable","0","44400","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV58,Pax :NAFSIYA KOLAMBIL, PassNo :G4454840/Pax :RSHAN AHAMMED, PassNo :P0352855/Pax :TANIA ISRA, PassNo :K5269033/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("401","83","2","2","Cash Payments","0","138980","160810 - 21830 = 138980","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP2,INV#PA2,PA4,PA5,PA6,PA7,PA8,PA9,PA10 Paid By :Manu To : Creative Tours & Travels - KODUNGALLUR,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("402","2","83","2","Cash Payments","138980","0","160810 - 21830 = 138980","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP2,INV#PA2,PA4,PA5,PA6,PA7,PA8,PA9,PA10 Paid By :Manu To : Creative Tours & Travels - KODUNGALLUR,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("407","138","6","60","Visa Receivable","0","600","Visa Receivable : 60","2016-09-07","2016-09-07","10","1","2","0","INV#IV60,Pax :ABDU RAHIMAN , PassNo :k000000/Pax :SREEJITH, PassNo :K8893537/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("408","6","138","60","Visa Receivable","600","0","Visa Receivable : 60","2016-09-07","2016-09-07","10","1","2","0","INV#IV60,Pax :ABDU RAHIMAN , PassNo :k000000/Pax :SREEJITH, PassNo :K8893537/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("409","125","7","59","Visa Payable","600","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV60,Pax :ABDU RAHIMAN , PassNo :k000000/Pax :SREEJITH, PassNo :K8893537/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("410","7","125","59","Visa Payable","0","600","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV60,Pax :ABDU RAHIMAN , PassNo :k000000/Pax :SREEJITH, PassNo :K8893537/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("411","138","6","61","Visa Receivable","0","14600","Visa Receivable : 61","2016-09-07","2016-09-07","10","1","2","0","INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("412","6","138","61","Visa Receivable","14600","0","Visa Receivable : 61","2016-09-07","2016-09-07","10","1","2","0","INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("413","125","7","60","Visa Payable","14600","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("414","7","125","60","Visa Payable","0","14600","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("415","138","2","7","Cash Receipts","15000","0","COUNTER 16","2016-09-07","2016-09-07","15","1","2","0","VouNo#CR 7,INV#IV59,IV61 Received By :Akhil,Details:INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("416","2","138","7","Cash Receipts","0","15000","COUNTER 16","2016-09-07","2016-09-07","15","1","2","0","VouNo#CR 7,INV#IV59,IV61 Received By :Akhil,Details:INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("417","87","2","4","Cash Receipts","107650","0","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 4,INV#IA2,IA9,IA10 Received By :Manu,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("418","2","87","4","Cash Receipts","0","107650","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 4,INV#IA2,IA9,IA10 Received By :Manu,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("421","138","6","59","Visa Receivable","0","400","Visa Receivable : 59","2016-09-07","2016-09-07","10","1","2","0","INV#IV59,Pax :NAHIM, PassNo :K2828090/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad Staff");
INSERT INTO transaction VALUES("422","6","138","59","Visa Receivable","400","0","Visa Receivable : 59","2016-09-07","2016-09-07","10","1","2","0","INV#IV59,Pax :NAHIM, PassNo :K2828090/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad Staff");
INSERT INTO transaction VALUES("423","125","7","58","Visa Payable","400","0","","2016-09-07","2016-09-07","10","1","2","0","INV#58,sup:125,:against:59,Details:INV#IV59,Pax :NAHIM, PassNo :K2828090/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad Staff");
INSERT INTO transaction VALUES("424","7","125","58","Visa Payable","0","400","","2016-09-07","2016-09-07","10","1","2","0","INV#58,sup:125,:against:59,Details:INV#IV59,Pax :NAHIM, PassNo :K2828090/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad Staff");
INSERT INTO transaction VALUES("425","82","2","3","Cash Payments","0","15000","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP3,INV#PV1 Paid By :Manu To : MTM VISA,Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("426","2","82","3","Cash Payments","15000","0","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP3,INV#PV1 Paid By :Manu To : MTM VISA,Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("427","88","2","6","Cash Receipts","10000","0","6500 balance","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 6,INV#IV1 Received By :Manu,Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("428","2","88","6","Cash Receipts","0","10000","6500 balance","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 6,INV#IV1 Received By :Manu,Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("429","86","2","7","Cash Receipts","1000","0","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 7,INV#IA1 Received By :Manu,Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("430","2","86","7","Cash Receipts","0","1000","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 7,INV#IA1 Received By :Manu,Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("431","128","2","5","Cash Receipts","15000","0","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 5,INV#IA14 Received By :Manu,Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("432","2","128","5","Cash Receipts","0","15000","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 5,INV#IA14 Received By :Manu,Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("433","98","4","11","Airline Receivable","0","24800","Airline Receivable: 11","2016-09-07","2016-09-05","6","2","2","0","INV#IA11, Pax:Ainikkunnan Jayafer, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("434","4","98","11","Airline Receivable","24800","0","Airline Receivable: 11","2016-09-07","2016-09-05","6","2","2","0","INV#IA11, Pax:Ainikkunnan Jayafer, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("435","81","5","11","Airline Payable","24520","0","","2016-09-07","2016-09-05","6","2","2","0","INV#11,sup:81,:against:11,Details:INV#IA11, Pax:Ainikkunnan Jayafer, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("436","5","81","11","Airline Payable","0","24520","","2016-09-07","2016-09-05","6","2","2","0","INV#11,sup:81,:against:11,Details:INV#IA11, Pax:Ainikkunnan Jayafer, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("439","95","6","62","Visa Receivable","0","14700","Visa Receivable : 62","2016-09-07","2016-09-07","10","1","2","0","INV#IV62,Pax :ANSAR, PassNo :M5169954/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("440","6","95","62","Visa Receivable","14700","0","Visa Receivable : 62","2016-09-07","2016-09-07","10","1","2","0","INV#IV62,Pax :ANSAR, PassNo :M5169954/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("441","125","7","61","Visa Payable","14200","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV62,Pax :ANSAR, PassNo :M5169954/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("442","7","125","61","Visa Payable","0","14200","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV62,Pax :ANSAR, PassNo :M5169954/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("443","95","6","63","Visa Receivable","0","6900","Visa Receivable : 63","2016-09-07","2016-09-07","10","1","2","0","INV#IV63,Pax :ANILA, PassNo :N7681273/Pax :PABRI CK, PassNo :N7686960/Pax :PATRICIA ANJELIN, PassNo :P2488318/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("444","6","95","63","Visa Receivable","6900","0","Visa Receivable : 63","2016-09-07","2016-09-07","10","1","2","0","INV#IV63,Pax :ANILA, PassNo :N7681273/Pax :PABRI CK, PassNo :N7686960/Pax :PATRICIA ANJELIN, PassNo :P2488318/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("445","125","7","62","Visa Payable","6900","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV63,Pax :ANILA, PassNo :N7681273/Pax :PABRI CK, PassNo :N7686960/Pax :PATRICIA ANJELIN, PassNo :P2488318/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("446","7","125","62","Visa Payable","0","6900","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV63,Pax :ANILA, PassNo :N7681273/Pax :PABRI CK, PassNo :N7686960/Pax :PATRICIA ANJELIN, PassNo :P2488318/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("447","95","6","64","Visa Receivable","0","4600","Visa Receivable : 64","2016-09-07","2016-09-07","10","1","2","0","INV#IV64,Pax :REMA KUMARI, PassNo :N9101342/Pax :KAVITHA, PassNo :K6437126/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("448","6","95","64","Visa Receivable","4600","0","Visa Receivable : 64","2016-09-07","2016-09-07","10","1","2","0","INV#IV64,Pax :REMA KUMARI, PassNo :N9101342/Pax :KAVITHA, PassNo :K6437126/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("449","125","7","63","Visa Payable","4600","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV64,Pax :REMA KUMARI, PassNo :N9101342/Pax :KAVITHA, PassNo :K6437126/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("450","7","125","63","Visa Payable","0","4600","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV64,Pax :REMA KUMARI, PassNo :N9101342/Pax :KAVITHA, PassNo :K6437126/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("451","95","6","65","Visa Receivable","0","2600","Visa Receivable : 65","2016-09-07","2016-09-07","10","1","2","0","INV#IV65,Pax :RAMANATHAN, PassNo :M6237420/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("452","6","95","65","Visa Receivable","2600","0","Visa Receivable : 65","2016-09-07","2016-09-07","10","1","2","0","INV#IV65,Pax :RAMANATHAN, PassNo :M6237420/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("453","125","7","64","Visa Payable","2600","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV65,Pax :RAMANATHAN, PassNo :M6237420/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("454","7","125","64","Visa Payable","0","2600","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV65,Pax :RAMANATHAN, PassNo :M6237420/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("455","95","6","66","Visa Receivable","0","14900","Visa Receivable : 66","2016-09-07","2016-09-07","10","1","2","0","INV#IV66,Pax :Fathima Hanna, PassNo :L7896144/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("456","6","95","66","Visa Receivable","14900","0","Visa Receivable : 66","2016-09-07","2016-09-07","10","1","2","0","INV#IV66,Pax :Fathima Hanna, PassNo :L7896144/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("457","125","7","65","Visa Payable","14900","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV66,Pax :Fathima Hanna, PassNo :L7896144/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("458","7","125","65","Visa Payable","0","14900","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV66,Pax :Fathima Hanna, PassNo :L7896144/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("459","96","6","67","Visa Receivable","0","15100","Visa Receivable : 67","2016-09-07","2016-09-07","9","1","2","0","INV#IV67,Pax :RICHARD, PassNo :H4603974/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("460","6","96","67","Visa Receivable","15100","0","Visa Receivable : 67","2016-09-07","2016-09-07","9","1","2","0","INV#IV67,Pax :RICHARD, PassNo :H4603974/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("461","94","7","66","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV67,Pax :RICHARD, PassNo :H4603974/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("462","7","94","66","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV67,Pax :RICHARD, PassNo :H4603974/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("463","95","6","68","Visa Receivable","0","14600","Visa Receivable : 68","2016-09-07","2016-09-07","9","1","2","0","INV#IV68,Pax :THAJUDHEEN, PassNo :K1076374/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("464","6","95","68","Visa Receivable","14600","0","Visa Receivable : 68","2016-09-07","2016-09-07","9","1","2","0","INV#IV68,Pax :THAJUDHEEN, PassNo :K1076374/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("465","94","7","67","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV68,Pax :THAJUDHEEN, PassNo :K1076374/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("466","7","94","67","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV68,Pax :THAJUDHEEN, PassNo :K1076374/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("467","129","6","69","Visa Receivable","0","15000","Visa Receivable : 69","2016-09-07","2016-09-07","9","1","2","0","INV#IV69,Pax :SUSHANTH, PassNo :K5230981/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("468","6","129","69","Visa Receivable","15000","0","Visa Receivable : 69","2016-09-07","2016-09-07","9","1","2","0","INV#IV69,Pax :SUSHANTH, PassNo :K5230981/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("469","94","7","68","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV69,Pax :SUSHANTH, PassNo :K5230981/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("470","7","94","68","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV69,Pax :SUSHANTH, PassNo :K5230981/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("471","136","6","70","Visa Receivable","0","15400","Visa Receivable : 70","2016-09-07","2016-09-07","9","1","2","0","INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("472","6","136","70","Visa Receivable","15400","0","Visa Receivable : 70","2016-09-07","2016-09-07","9","1","2","0","INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("473","132","7","69","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("474","7","132","69","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("475","139","6","2","Visa Receivable","0","5800","Visa Receivable : 2","2016-09-07","2016-09-07","6","2","2","0","INV#IV2,Pax :AYISHA KUNNATH, PassNo :P0915315/ visaFor:UAE  30 Days,By:Manu");
INSERT INTO transaction VALUES("476","6","139","2","Visa Receivable","5800","0","Visa Receivable : 2","2016-09-07","2016-09-07","6","2","2","0","INV#IV2,Pax :AYISHA KUNNATH, PassNo :P0915315/ visaFor:UAE  30 Days,By:Manu");
INSERT INTO transaction VALUES("477","82","7","2","Visa Payable","5500","0","","2016-09-07","2016-09-07","6","2","2","0","Details:INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("478","7","82","2","Visa Payable","0","5500","","2016-09-07","2016-09-07","6","2","2","0","Details:INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("479","95","6","71","Visa Receivable","0","7700","Visa Receivable : 71","2016-09-07","2016-09-07","10","1","2","0","INV#IV71,Pax :SABEENA 1118, PassNo :	J5242293/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("480","6","95","71","Visa Receivable","7700","0","Visa Receivable : 71","2016-09-07","2016-09-07","10","1","2","0","INV#IV71,Pax :SABEENA 1118, PassNo :	J5242293/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("481","125","7","70","Visa Payable","7700","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV71,Pax :SABEENA 1118, PassNo :	J5242293/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("482","7","125","70","Visa Payable","0","7700","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV71,Pax :SABEENA 1118, PassNo :	J5242293/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("489","95","6","74","Visa Receivable","0","14900","Visa Receivable : 74","2016-09-07","2016-09-07","10","1","2","0","INV#IV74,Pax :KAVERI, PassNo :K7027177/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("490","6","95","74","Visa Receivable","14900","0","Visa Receivable : 74","2016-09-07","2016-09-07","10","1","2","0","INV#IV74,Pax :KAVERI, PassNo :K7027177/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("491","125","7","72","Visa Payable","14900","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV74,Pax :KAVERI, PassNo :K7027177/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("492","7","125","72","Visa Payable","0","14900","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV74,Pax :KAVERI, PassNo :K7027177/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("493","95","6","75","Visa Receivable","0","900","Visa Receivable : 75","2016-09-07","2016-09-07","10","1","2","0","INV#IV75,Pax :AKHILA 12200, PassNo :k000000/Pax :ISHAK 1224, PassNo :K00000/Pax :SREESHMA 1226, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("494","6","95","75","Visa Receivable","900","0","Visa Receivable : 75","2016-09-07","2016-09-07","10","1","2","0","INV#IV75,Pax :AKHILA 12200, PassNo :k000000/Pax :ISHAK 1224, PassNo :K00000/Pax :SREESHMA 1226, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("495","125","7","73","Visa Payable","900","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV75,Pax :AKHILA 12200, PassNo :k000000/Pax :ISHAK 1224, PassNo :K00000/Pax :SREESHMA 1226, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("496","7","125","73","Visa Payable","0","900","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV75,Pax :AKHILA 12200, PassNo :k000000/Pax :ISHAK 1224, PassNo :K00000/Pax :SREESHMA 1226, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("497","95","6","76","Visa Receivable","0","58400","Visa Receivable : 76","2016-09-07","2016-09-07","10","1","2","0","INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("498","6","95","76","Visa Receivable","58400","0","Visa Receivable : 76","2016-09-07","2016-09-07","10","1","2","0","INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("499","125","7","74","Visa Payable","58400","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("500","7","125","74","Visa Payable","0","58400","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("501","95","6","77","Visa Receivable","0","88200","Visa Receivable : 77","2016-09-07","2016-09-07","10","1","2","0","INV#IV77,Pax :NOORUNNISA 1234, PassNo :M7276307/Pax :MUHAMMED RASHID 1234, PassNo :P3999764/Pax :ASHA MATHARI 1240, PassNo :J5238983/Pax :GEETHA 1250, PassNo :N5577038/Pax :1262	ANU MOLE, PassNo :N7798897/Pax :1264	VIJITHA, PassNo :	P0428557/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("502","6","95","77","Visa Receivable","88200","0","Visa Receivable : 77","2016-09-07","2016-09-07","10","1","2","0","INV#IV77,Pax :NOORUNNISA 1234, PassNo :M7276307/Pax :MUHAMMED RASHID 1234, PassNo :P3999764/Pax :ASHA MATHARI 1240, PassNo :J5238983/Pax :GEETHA 1250, PassNo :N5577038/Pax :1262	ANU MOLE, PassNo :N7798897/Pax :1264	VIJITHA, PassNo :	P0428557/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("503","125","7","75","Visa Payable","88200","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV77,Pax :NOORUNNISA 1234, PassNo :M7276307/Pax :MUHAMMED RASHID 1234, PassNo :P3999764/Pax :ASHA MATHARI 1240, PassNo :J5238983/Pax :GEETHA 1250, PassNo :N5577038/Pax :1262	ANU MOLE, PassNo :N7798897/Pax :1264	VIJITHA, PassNo :	P0428557/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("504","7","125","75","Visa Payable","0","88200","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV77,Pax :NOORUNNISA 1234, PassNo :M7276307/Pax :MUHAMMED RASHID 1234, PassNo :P3999764/Pax :ASHA MATHARI 1240, PassNo :J5238983/Pax :GEETHA 1250, PassNo :N5577038/Pax :1262	ANU MOLE, PassNo :N7798897/Pax :1264	VIJITHA, PassNo :	P0428557/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("507","101","6","78","Visa Receivable","0","10000","Visa Receivable : 78","2016-09-08","2016-09-08","9","1","2","0","INV#IV78,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:10 DAYS OMAN,By:Sahad Staff");
INSERT INTO transaction VALUES("508","6","101","78","Visa Receivable","10000","0","Visa Receivable : 78","2016-09-08","2016-09-08","9","1","2","0","INV#IV78,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:10 DAYS OMAN,By:Sahad Staff");
INSERT INTO transaction VALUES("509","97","7","76","Visa Payable","8000","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV78,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:10 DAYS OMAN,By:Sahad Staff");
INSERT INTO transaction VALUES("510","7","97","76","Visa Payable","0","8000","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV78,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:10 DAYS OMAN,By:Sahad Staff");
INSERT INTO transaction VALUES("511","101","6","79","Visa Receivable","0","5800","Visa Receivable : 79","2016-09-08","2016-09-08","9","1","2","0","INV#IV79,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:30 DAYS QATAR TOURIST,By:Sahad Staff");
INSERT INTO transaction VALUES("512","6","101","79","Visa Receivable","5800","0","Visa Receivable : 79","2016-09-08","2016-09-08","9","1","2","0","INV#IV79,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:30 DAYS QATAR TOURIST,By:Sahad Staff");
INSERT INTO transaction VALUES("513","140","7","77","Visa Payable","5500","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV79,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:30 DAYS QATAR TOURIST,By:Sahad Staff");
INSERT INTO transaction VALUES("514","7","140","77","Visa Payable","0","5500","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV79,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:30 DAYS QATAR TOURIST,By:Sahad Staff");
INSERT INTO transaction VALUES("515","128","2","8","Cash Receipts","7500","0","","2016-09-08","2016-09-08","6","2","2","0","VouNo#CR 8,INV#IA14 Received By :Manu,Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("516","2","128","8","Cash Receipts","0","7500","","2016-09-08","2016-09-08","6","2","2","0","VouNo#CR 8,INV#IA14 Received By :Manu,Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("517","141","6","80","Visa Receivable","0","6600","Visa Receivable : 80","2016-09-08","2016-09-08","9","1","2","0","INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("518","6","141","80","Visa Receivable","6600","0","Visa Receivable : 80","2016-09-08","2016-09-08","9","1","2","0","INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("519","140","7","78","Visa Payable","6500","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("520","7","140","78","Visa Payable","0","6500","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("521","142","6","81","Visa Receivable","0","15000","Visa Receivable : 81","2016-09-08","2016-09-08","9","1","2","0","INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("522","6","142","81","Visa Receivable","15000","0","Visa Receivable : 81","2016-09-08","2016-09-08","9","1","2","0","INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("523","94","7","79","Visa Payable","14100","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("524","7","94","79","Visa Payable","0","14100","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("533","143","2","8","Cash Receipts","1700","0","PASSPORT SERVICE","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 8,INV#IM1 Received By :Akhil,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("534","2","143","8","Cash Receipts","0","1700","PASSPORT SERVICE","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 8,INV#IM1 Received By :Akhil,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("535","144","6","82","Visa Receivable","0","16500","Visa Receivable : 82","2016-09-08","2016-09-08","10","1","2","0","INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("536","6","144","82","Visa Receivable","16500","0","Visa Receivable : 82","2016-09-08","2016-09-08","10","1","2","0","INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("537","16","144","82","Visa Receivable","0","500","Visa Receivable : 82","2016-09-08","2016-09-08","10","1","2","0","INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("538","144","16","82","Visa Receivable","500","0","Visa Receivable : 82","2016-09-08","2016-09-08","10","1","2","0","INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("539","125","7","80","Visa Payable","16500","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("540","7","125","80","Visa Payable","0","16500","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("541","144","2","9","Cash Receipts","16000","0","OMAN VISA","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 9,INV#IV82 Received By :Akhil,Details:INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("542","2","144","9","Cash Receipts","0","16000","OMAN VISA","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 9,INV#IV82 Received By :Akhil,Details:INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("545","130","6","83","Visa Receivable","0","5100","Visa Receivable : 83","2016-09-08","2016-09-08","9","1","2","0","INV#IV83,Pax :RAJAN KOCHU, PassNo :K3751349/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("546","6","130","83","Visa Receivable","5100","0","Visa Receivable : 83","2016-09-08","2016-09-08","9","1","2","0","INV#IV83,Pax :RAJAN KOCHU, PassNo :K3751349/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("547","94","7","81","Visa Payable","5100","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV83,Pax :RAJAN KOCHU, PassNo :K3751349/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("548","7","94","81","Visa Payable","0","5100","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV83,Pax :RAJAN KOCHU, PassNo :K3751349/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("551","130","6","84","Visa Receivable","0","14700","Visa Receivable : 84","2016-09-08","2016-09-08","9","1","2","0","INV#IV84,Pax :MATHUKUTTY, PassNo :L1747043/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("552","6","130","84","Visa Receivable","14700","0","Visa Receivable : 84","2016-09-08","2016-09-08","9","1","2","0","INV#IV84,Pax :MATHUKUTTY, PassNo :L1747043/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("553","94","7","82","Visa Payable","14100","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV84,Pax :MATHUKUTTY, PassNo :L1747043/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("554","7","94","82","Visa Payable","0","14100","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV84,Pax :MATHUKUTTY, PassNo :L1747043/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("555","103","6","85","Visa Receivable","0","5500","Visa Receivable : 85","2016-09-08","2016-09-08","9","1","2","0","INV#IV85,Pax :AYISHA, PassNo :P0615315/ visaFor:30 DAYS DXB WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("556","6","103","85","Visa Receivable","5500","0","Visa Receivable : 85","2016-09-08","2016-09-08","9","1","2","0","INV#IV85,Pax :AYISHA, PassNo :P0615315/ visaFor:30 DAYS DXB WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("557","94","7","83","Visa Payable","5100","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV85,Pax :AYISHA, PassNo :P0615315/ visaFor:30 DAYS DXB WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("558","7","94","83","Visa Payable","0","5100","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV85,Pax :AYISHA, PassNo :P0615315/ visaFor:30 DAYS DXB WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("559","145","6","86","Visa Receivable","0","45000","Visa Receivable : 86","2016-09-08","2016-09-08","9","1","2","0","INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("560","6","145","86","Visa Receivable","45000","0","Visa Receivable : 86","2016-09-08","2016-09-08","9","1","2","0","INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("561","132","7","84","Visa Payable","44400","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("562","7","132","84","Visa Payable","0","44400","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("563","145","2","10","Cash Receipts","40000","0","VISA FOR 3 MONTHS","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 10,INV#IV86 Received By :Akhil,Details:INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("564","2","145","10","Cash Receipts","0","40000","VISA FOR 3 MONTHS","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 10,INV#IV86 Received By :Akhil,Details:INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("567","143","14","1","Misc Receivable","0","1700","Misc Receivable : 1","2016-09-08","2016-09-08","10","1","2","0","INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("568","14","143","1","Misc Receivable","1700","0","Misc Receivable : 1","2016-09-08","2016-09-08","10","1","2","0","INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("569","146","15","1","Misc Payable","1500","0","","2016-09-08","2016-09-08","10","1","2","0","INV#1,sup:146,:against:1,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("570","15","146","1","Misc Payable","0","1500","","2016-09-08","2016-09-08","10","1","2","0","INV#1,sup:146,:against:1,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("573","96","6","87","Visa Receivable","0","16000","Visa Receivable : 87","2016-09-08","2016-09-08","8","1","2","0","INV#IV87,Pax :HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("574","6","96","87","Visa Receivable","16000","0","Visa Receivable : 87","2016-09-08","2016-09-08","8","1","2","0","INV#IV87,Pax :HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("575","125","7","85","Visa Payable","14500","0","COUNTER 1204","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV87,Pax :HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("576","7","125","85","Visa Payable","0","14500","COUNTER 1204","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV87,Pax :HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("577","147","147","7","Opening Balance","0","1450","Opening Balance","2016-09-08","2016-09-08","10","1","2","0","INV#7, acc:SHAMNAS PURCHASE ,Opening:1450");
INSERT INTO transaction VALUES("578","1","147","7","Opening Balance","1450","0","Opening Balance","2016-09-08","2016-09-08","10","1","2","0","INV#7, acc:SHAMNAS PURCHASE ,Opening:1450");
INSERT INTO transaction VALUES("579","96","6","88","Visa Receivable","0","11000","Visa Receivable : 88","2016-09-08","2016-09-08","8","1","2","0","INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("580","6","96","88","Visa Receivable","11000","0","Visa Receivable : 88","2016-09-08","2016-09-08","8","1","2","0","INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("581","125","7","86","Visa Payable","11000","0","","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("582","7","125","86","Visa Payable","0","11000","","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("583","125","2","2","Cash Payments","0","8500","COUNTER 1204","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP2,INV#PV86 Paid By :Akhil To : counter16,Details:INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("584","2","125","2","Cash Payments","8500","0","COUNTER 1204","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP2,INV#PV86 Paid By :Akhil To : counter16,Details:INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("585","129","6","89","Visa Receivable","0","900","Visa Receivable : 89","2016-09-08","2016-09-08","10","1","2","0","INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("586","6","129","89","Visa Receivable","900","0","Visa Receivable : 89","2016-09-08","2016-09-08","10","1","2","0","INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("587","147","7","87","Visa Payable","900","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("588","7","147","87","Visa Payable","0","900","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("613","148","2","11","Cash Receipts","3800","0","paid cash for visa amount","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 11,INV#IV94 Received By :Akhil,Details:INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA,By:Safuvan");
INSERT INTO transaction VALUES("614","2","148","11","Cash Receipts","0","3800","paid cash for visa amount","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 11,INV#IV94 Received By :Akhil,Details:INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA,By:Safuvan");
INSERT INTO transaction VALUES("619","129","6","97","Visa Receivable","0","300","Visa Receivable : 97","2016-09-08","2016-09-08","10","1","2","0","INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("620","6","129","97","Visa Receivable","300","0","Visa Receivable : 97","2016-09-08","2016-09-08","10","1","2","0","INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("621","97","7","94","Visa Payable","300","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("622","7","97","94","Visa Payable","0","300","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("623","97","7","95","Visa Payable","300","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("624","7","97","95","Visa Payable","0","300","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("625","129","6","96","Visa Receivable","0","15900","Visa Receivable : 96","2016-09-08","2016-09-08","10","1","2","0","INV#IV96,Pax :1247	HISHAM MASHOOD, PassNo :J5440840/ visaFor:90 DAYS DXB SETTING, By:Irfad");
INSERT INTO transaction VALUES("626","6","129","96","Visa Receivable","15900","0","Visa Receivable : 96","2016-09-08","2016-09-08","10","1","2","0","INV#IV96,Pax :1247	HISHAM MASHOOD, PassNo :J5440840/ visaFor:90 DAYS DXB SETTING, By:Irfad");
INSERT INTO transaction VALUES("627","125","7","93","Visa Payable","15900","0","","2016-09-08","2016-09-08","10","1","2","0","INV#93,sup:125,:against:96,Details:INV#IV96,Pax :1247	HISHAM MASHOOD, PassNo :J5440840/ visaFor:90 DAYS DXB SETTING, By:Irfad");
INSERT INTO transaction VALUES("628","7","125","93","Visa Payable","0","15900","","2016-09-08","2016-09-08","10","1","2","0","INV#93,sup:125,:against:96,Details:INV#IV96,Pax :1247	HISHAM MASHOOD, PassNo :J5440840/ visaFor:90 DAYS DXB SETTING, By:Irfad");
INSERT INTO transaction VALUES("629","129","6","98","Visa Receivable","0","15900","Visa Receivable : 98","2016-09-08","2016-09-08","10","1","2","0","INV#IV98,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :H5686487/ visaFor:90 DAYS DXB SETTING,By:Irfad");
INSERT INTO transaction VALUES("630","6","129","98","Visa Receivable","15900","0","Visa Receivable : 98","2016-09-08","2016-09-08","10","1","2","0","INV#IV98,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :H5686487/ visaFor:90 DAYS DXB SETTING,By:Irfad");
INSERT INTO transaction VALUES("631","125","7","96","Visa Payable","15900","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV98,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :H5686487/ visaFor:90 DAYS DXB SETTING,By:Irfad");
INSERT INTO transaction VALUES("632","7","125","96","Visa Payable","0","15900","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV98,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :H5686487/ visaFor:90 DAYS DXB SETTING,By:Irfad");
INSERT INTO transaction VALUES("633","129","6","99","Visa Receivable","0","300","Visa Receivable : 99","2016-09-08","2016-09-08","10","1","2","0","INV#IV99,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("634","6","129","99","Visa Receivable","300","0","Visa Receivable : 99","2016-09-08","2016-09-08","10","1","2","0","INV#IV99,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("635","97","7","97","Visa Payable","300","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV99,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("636","7","97","97","Visa Payable","0","300","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV99,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("643","101","2","12","Cash Receipts","5200","0","adjust salil a/c","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 12,INV#IV37 Received By :Akhil,Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("644","2","101","12","Cash Receipts","0","5200","adjust salil a/c","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 12,INV#IV37 Received By :Akhil,Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("645","127","2","3","Cash Payments","0","5150","adjust to salil a/c","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP3,INV#PV36 Paid By :Akhil To : ALTA CCJ,Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("646","2","127","3","Cash Payments","5150","0","adjust to salil a/c","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP3,INV#PV36 Paid By :Akhil To : ALTA CCJ,Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("647","94","2","4","Cash Payments","0","5200","adjusted to IV37 manu bahi","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP4,INV#PV18,PV19 Paid By :Akhil To : SALIL PURCHASE,Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("648","2","94","4","Cash Payments","5200","0","adjusted to IV37 manu bahi","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP4,INV#PV18,PV19 Paid By :Akhil To : SALIL PURCHASE,Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("651","147","2","5","Cash Payments","0","900","paid cash","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP5,INV#PV87 Paid By :Akhil To : SHAMNAS PURCHASE,Details:INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("652","2","147","5","Cash Payments","900","0","paid cash","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP5,INV#PV87 Paid By :Akhil To : SHAMNAS PURCHASE,Details:INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("653","149","6","100","Visa Receivable","0","7750","Visa Receivable : 100","2016-09-08","2016-09-08","8","1","2","0","INV#IV100,Pax :SABAD MUHAMMED, PassNo :M4747049/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("654","6","149","100","Visa Receivable","7750","0","Visa Receivable : 100","2016-09-08","2016-09-08","8","1","2","0","INV#IV100,Pax :SABAD MUHAMMED, PassNo :M4747049/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("655","125","7","98","Visa Payable","7000","0","COUNTER 1180","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV100,Pax :SABAD MUHAMMED, PassNo :M4747049/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("656","7","125","98","Visa Payable","0","7000","COUNTER 1180","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV100,Pax :SABAD MUHAMMED, PassNo :M4747049/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("657","149","6","101","Visa Receivable","0","7750","Visa Receivable : 101","2016-09-08","2016-09-08","8","1","2","0","INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("658","6","149","101","Visa Receivable","7750","0","Visa Receivable : 101","2016-09-08","2016-09-08","8","1","2","0","INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("659","125","7","99","Visa Payable","7000","0","COUNTER 1182","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("660","7","125","99","Visa Payable","0","7000","COUNTER 1182","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("661","149","2","13","Cash Receipts","15500","0","counter 16","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 13,INV#IV100,IV101 Received By :Akhil,Details:INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("662","2","149","13","Cash Receipts","0","15500","counter 16","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 13,INV#IV100,IV101 Received By :Akhil,Details:INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("663","126","2","9","Cash Receipts","32500","0","","2016-09-09","2016-09-09","6","2","2","0","VouNo#CR 9,INV#IA13 Received By :Manu,Details:INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("664","2","126","9","Cash Receipts","0","32500","","2016-09-09","2016-09-09","6","2","2","0","VouNo#CR 9,INV#IA13 Received By :Manu,Details:INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("665","114","6","102","Visa Receivable","0","16500","Visa Receivable : 102","2016-09-09","2016-09-09","9","1","2","0","INV#IV102,Pax :HAMDAN HAMEED, PassNo :P2203649/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("666","6","114","102","Visa Receivable","16500","0","Visa Receivable : 102","2016-09-09","2016-09-09","9","1","2","0","INV#IV102,Pax :HAMDAN HAMEED, PassNo :P2203649/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("667","132","7","100","Visa Payable","15800","0","","2016-09-09","2016-09-09","9","1","2","0","Details:INV#IV102,Pax :HAMDAN HAMEED, PassNo :P2203649/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("668","7","132","100","Visa Payable","0","15800","","2016-09-09","2016-09-09","9","1","2","0","Details:INV#IV102,Pax :HAMDAN HAMEED, PassNo :P2203649/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("669","93","6","103","Visa Receivable","0","16200","Visa Receivable : 103","2016-09-09","2016-09-09","9","1","2","0","INV#IV103,Pax :JAMSHAD, PassNo :H1608955/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("670","6","93","103","Visa Receivable","16200","0","Visa Receivable : 103","2016-09-09","2016-09-09","9","1","2","0","INV#IV103,Pax :JAMSHAD, PassNo :H1608955/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("671","132","7","101","Visa Payable","15800","0","","2016-09-09","2016-09-09","9","1","2","0","Details:INV#IV103,Pax :JAMSHAD, PassNo :H1608955/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("672","7","132","101","Visa Payable","0","15800","","2016-09-09","2016-09-09","9","1","2","0","Details:INV#IV103,Pax :JAMSHAD, PassNo :H1608955/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("673","127","127","0","Opening Balance","750","0","Opening Balance","2016-09-09","2016-09-09","10","1","2","0","INV#1, acc:ALTA CCJ,Opening:750");
INSERT INTO transaction VALUES("674","1","127","0","Opening Balance","0","750","Opening Balance","2016-09-09","2016-09-09","10","1","2","0","INV#1, acc:ALTA CCJ,Opening:750");
INSERT INTO transaction VALUES("677","140","2","6","Cash Payments","0","12500","PAID CASH THROUGH FEDERAL BANK","2016-09-09","2016-09-09","15","1","2","0","VouNo#CP6,INV#PV77,PV78 Paid By :Akhil To : GULF INDIA TRAVELS,Details:INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("678","2","140","6","Cash Payments","12500","0","PAID CASH THROUGH FEDERAL BANK","2016-09-09","2016-09-09","15","1","2","0","VouNo#CP6,INV#PV77,PV78 Paid By :Akhil To : GULF INDIA TRAVELS,Details:INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("679","129","6","91","Visa Receivable","0","300","Visa Receivable : 91","2016-09-09","2016-09-08","10","1","2","0","INV#IV91,Pax :1196	MUHAMMED ALI, PassNo :0/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("680","6","129","91","Visa Receivable","300","0","Visa Receivable : 91","2016-09-09","2016-09-08","10","1","2","0","INV#IV91,Pax :1196	MUHAMMED ALI, PassNo :0/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("681","97","7","89","Visa Payable","300","0","","2016-09-09","2016-09-08","10","1","2","0","INV#89,sup:97,:against:91,Details:INV#IV91,Pax :1196	MUHAMMED ALI, PassNo :0/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("682","7","97","89","Visa Payable","0","300","","2016-09-09","2016-09-08","10","1","2","0","INV#89,sup:97,:against:91,Details:INV#IV91,Pax :1196	MUHAMMED ALI, PassNo :0/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("685","129","6","92","Visa Receivable","0","59200","Visa Receivable : 92","2016-09-09","2016-09-08","10","1","2","0","INV#IV92,Pax :1245    AYSHANA RAMEES, PassNo :M5856848/Pax :1245	NASREENA, PassNo :M0882717/Pax :1198   VALSALA, PassNo :N2189073/Pax :1197	SAREENA, PassNo :K9858317/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("686","6","129","92","Visa Receivable","59200","0","Visa Receivable : 92","2016-09-09","2016-09-08","10","1","2","0","INV#IV92,Pax :1245    AYSHANA RAMEES, PassNo :M5856848/Pax :1245	NASREENA, PassNo :M0882717/Pax :1198   VALSALA, PassNo :N2189073/Pax :1197	SAREENA, PassNo :K9858317/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("689","129","6","95","Visa Receivable","0","1200","Visa Receivable : 95","2016-09-09","2016-09-08","10","1","2","0","INV#IV95,Pax :SAREENA VALSALA NASREENA /AYSHANA RAMEES, PassNo :k000000/Pax :VALSALA, PassNo :K000000/Pax :NASREENA, PassNo :K00000/Pax :AYSHANA RAMEES, PassNo :K00000/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("690","6","129","95","Visa Receivable","1200","0","Visa Receivable : 95","2016-09-09","2016-09-08","10","1","2","0","INV#IV95,Pax :SAREENA VALSALA NASREENA /AYSHANA RAMEES, PassNo :k000000/Pax :VALSALA, PassNo :K000000/Pax :NASREENA, PassNo :K00000/Pax :AYSHANA RAMEES, PassNo :K00000/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("691","97","7","92","Visa Payable","1200","0","","2016-09-09","2016-09-08","10","1","2","0","INV#92,sup:97,:against:95,Details:INV#IV95,Pax :SAREENA VALSALA NASREENA /AYSHANA RAMEES, PassNo :k000000/Pax :VALSALA, PassNo :K000000/Pax :NASREENA, PassNo :K00000/Pax :AYSHANA RAMEES, PassNo :K00000/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("692","7","97","92","Visa Payable","0","1200","","2016-09-09","2016-09-08","10","1","2","0","INV#92,sup:97,:against:95,Details:INV#IV95,Pax :SAREENA VALSALA NASREENA /AYSHANA RAMEES, PassNo :k000000/Pax :VALSALA, PassNo :K000000/Pax :NASREENA, PassNo :K00000/Pax :AYSHANA RAMEES, PassNo :K00000/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("707","99","6","104","Visa Receivable","0","78325","Visa Receivable : 104","2016-09-09","2016-09-09","10","1","2","0","INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("708","6","99","104","Visa Receivable","78325","0","Visa Receivable : 104","2016-09-09","2016-09-09","10","1","2","0","INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("709","125","7","102","Visa Payable","78325","0","","2016-09-09","2016-09-09","10","1","2","0","Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("710","7","125","102","Visa Payable","0","78325","","2016-09-09","2016-09-09","10","1","2","0","Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("715","150","4","18","Airline Receivable","0","68000","Airline Receivable: 18","2016-09-09","2016-09-09","6","2","2","0","INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("716","4","150","18","Airline Receivable","68000","0","Airline Receivable: 18","2016-09-09","2016-09-09","6","2","2","0","INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("717","81","5","18","Airline Payable","67030","0","","2016-09-09","2016-09-09","6","2","2","0","Details:INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("718","5","81","18","Airline Payable","0","67030","","2016-09-09","2016-09-09","6","2","2","0","Details:INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("731","146","15","2","Misc Payable","15000","0","Fly Hind haj ticket date change","2016-09-09","2016-09-09","5","1","2","0","Details:INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("732","15","146","2","Misc Payable","0","15000","Fly Hind haj ticket date change","2016-09-09","2016-09-09","5","1","2","0","Details:INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("733","151","14","2","Misc Receivable","0","24750","Misc Receivable : 2","2016-09-09","2016-09-09","5","1","2","0","INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("734","14","151","2","Misc Receivable","24750","0","Misc Receivable : 2","2016-09-09","2016-09-09","5","1","2","0","INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("735","151","2","14","Cash Receipts","24750","0","Haj Ticket date change penalty","2016-09-09","2016-09-09","5","1","2","0","VouNo#CR 14,INV#IM2 Received By :Sidheeq,Details:INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("736","2","151","14","Cash Receipts","0","24750","Haj Ticket date change penalty","2016-09-09","2016-09-09","5","1","2","0","VouNo#CR 14,INV#IM2 Received By :Sidheeq,Details:INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("741","96","6","105","Visa Receivable","0","5200","Visa Receivable : 105","2016-09-10","2016-09-10","8","1","2","0","INV#IV105,Pax :UDAYA MOOLYA, PassNo :J7091327/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("742","6","96","105","Visa Receivable","5200","0","Visa Receivable : 105","2016-09-10","2016-09-10","8","1","2","0","INV#IV105,Pax :UDAYA MOOLYA, PassNo :J7091327/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("743","94","7","103","Visa Payable","5100","0","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV105,Pax :UDAYA MOOLYA, PassNo :J7091327/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("744","7","94","103","Visa Payable","0","5100","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV105,Pax :UDAYA MOOLYA, PassNo :J7091327/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("745","146","2","7","Cash Payments","0","1500","PAID CASH TO TRAVEL MART","2016-09-10","2016-09-10","15","1","2","0","VouNo#CP7,INV#PM1 Paid By :Akhil To : MARKAZ TRAVEL MART,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("746","2","146","7","Cash Payments","1500","0","PAID CASH TO TRAVEL MART","2016-09-10","2016-09-10","15","1","2","0","VouNo#CP7,INV#PM1 Paid By :Akhil To : MARKAZ TRAVEL MART,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("747","96","6","106","Visa Receivable","0","20800","Visa Receivable : 106","2016-09-10","2016-09-10","8","1","2","0","INV#IV106,Pax :KADAMBU NEBISA, PassNo :L6156737.P2778220/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("748","6","96","106","Visa Receivable","20800","0","Visa Receivable : 106","2016-09-10","2016-09-10","8","1","2","0","INV#IV106,Pax :KADAMBU NEBISA, PassNo :L6156737.P2778220/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("749","94","7","104","Visa Payable","20400","0","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV106,Pax :KADAMBU NEBISA, PassNo :L6156737.P2778220/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("750","7","94","104","Visa Payable","0","20400","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV106,Pax :KADAMBU NEBISA, PassNo :L6156737.P2778220/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("751","152","6","107","Visa Receivable","0","5200","Visa Receivable : 107","2016-09-10","2016-09-10","8","1","2","0","INV#IV107,Pax :SHAFJAN PATTILLATH CHANDERA, PassNo :H5103542/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("752","6","152","107","Visa Receivable","5200","0","Visa Receivable : 107","2016-09-10","2016-09-10","8","1","2","0","INV#IV107,Pax :SHAFJAN PATTILLATH CHANDERA, PassNo :H5103542/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("753","94","7","105","Visa Payable","5100","0","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV107,Pax :SHAFJAN PATTILLATH CHANDERA, PassNo :H5103542/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("754","7","94","105","Visa Payable","0","5100","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV107,Pax :SHAFJAN PATTILLATH CHANDERA, PassNo :H5103542/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("755","96","6","108","Visa Receivable","0","11500","Visa Receivable : 108","2016-09-10","2016-09-10","8","1","2","0","INV#IV108,Pax :, PassNo :/ visaFor:KUWAIT STAMPING,By:Safuvan");
INSERT INTO transaction VALUES("756","6","96","108","Visa Receivable","11500","0","Visa Receivable : 108","2016-09-10","2016-09-10","8","1","2","0","INV#IV108,Pax :, PassNo :/ visaFor:KUWAIT STAMPING,By:Safuvan");
INSERT INTO transaction VALUES("757","97","7","106","Visa Payable","11000","0","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV108,Pax :, PassNo :/ visaFor:KUWAIT STAMPING,By:Safuvan");
INSERT INTO transaction VALUES("758","7","97","106","Visa Payable","0","11000","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV108,Pax :, PassNo :/ visaFor:KUWAIT STAMPING,By:Safuvan");
INSERT INTO transaction VALUES("759","95","6","16","Visa Receivable","0","14700","Visa Receivable : 16","2016-09-10","2016-09-05","10","1","2","0","INV#IV16,Pax :MISRIYA, PassNo :P1902660/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("760","6","95","16","Visa Receivable","14700","0","Visa Receivable : 16","2016-09-10","2016-09-05","10","1","2","0","INV#IV16,Pax :MISRIYA, PassNo :P1902660/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("761","102","7","16","Visa Payable","14500","0","","2016-09-10","2016-09-05","10","1","2","0","INV#16,sup:102,:against:16,Details:INV#IV16,Pax :MISRIYA, PassNo :P1902660/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("762","7","102","16","Visa Payable","0","14500","","2016-09-10","2016-09-05","10","1","2","0","INV#16,sup:102,:against:16,Details:INV#IV16,Pax :MISRIYA, PassNo :P1902660/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("767","96","6","23","Visa Receivable","0","17400","Visa Receivable : 23","2016-09-10","2016-09-06","8","1","2","0","INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("768","6","96","23","Visa Receivable","17400","0","Visa Receivable : 23","2016-09-10","2016-09-06","8","1","2","0","INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("769","96","2","3","Cash Receipts","5000","0","","2016-09-10","2016-09-06","15","1","2","0","VouNo#CR 3,INV#IV23 Received By :Akhil,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("770","2","96","3","Cash Receipts","0","5000","","2016-09-10","2016-09-06","15","1","2","0","VouNo#CR 3,INV#IV23 Received By :Akhil,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("771","96","2","4","Cash Receipts","16000","0","","2016-09-10","2016-09-06","15","1","2","0","VouNo#CR 4,INV#IV24 Received By :Akhil,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("772","2","96","4","Cash Receipts","0","16000","","2016-09-10","2016-09-06","15","1","2","0","VouNo#CR 4,INV#IV24 Received By :Akhil,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("773","96","2","15","Cash Receipts","12400","0","","2016-09-10","2016-09-10","15","1","2","0","VouNo#CR 15,INV#IV23 Received By :Akhil,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("774","2","96","15","Cash Receipts","0","12400","","2016-09-10","2016-09-10","15","1","2","0","VouNo#CR 15,INV#IV23 Received By :Akhil,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("775","95","6","72","Visa Receivable","0","43800","Visa Receivable : 72","2016-09-10","2016-09-07","10","1","2","0","INV#IV72,Pax :ANEEM 1223, PassNo :K5283027/Pax :VIJESH 1222, PassNo :K7027177/Pax :FERVEES 1199, PassNo :J5802083/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("776","6","95","72","Visa Receivable","43800","0","Visa Receivable : 72","2016-09-10","2016-09-07","10","1","2","0","INV#IV72,Pax :ANEEM 1223, PassNo :K5283027/Pax :VIJESH 1222, PassNo :K7027177/Pax :FERVEES 1199, PassNo :J5802083/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("777","97","7","0","Visa Payable","43800","0","","2016-09-10","0000-00-00","10","1","2","0","INV#,sup:97,:against:,Details:");
INSERT INTO transaction VALUES("778","7","97","0","Visa Payable","0","43800","","2016-09-10","0000-00-00","10","1","2","0","INV#,sup:97,:against:,Details:");
INSERT INTO transaction VALUES("779","127","7","22","Visa Payable","15900","0","","2016-09-10","2016-09-06","8","1","2","0","INV#22,sup:127,:against:23,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("780","7","127","22","Visa Payable","0","15900","","2016-09-10","2016-09-06","8","1","2","0","INV#22,sup:127,:against:23,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("781","153","6","109","Visa Receivable","0","16300","Visa Receivable : 109","2016-09-10","2016-09-10","8","1","2","0","INV#IV109,Pax :USMAN PORKKALAM, PassNo :J6960023/ visaFor:90 DAYS DXB SETTING,By:Safuvan");
INSERT INTO transaction VALUES("782","6","153","109","Visa Receivable","16300","0","Visa Receivable : 109","2016-09-10","2016-09-10","8","1","2","0","INV#IV109,Pax :USMAN PORKKALAM, PassNo :J6960023/ visaFor:90 DAYS DXB SETTING,By:Safuvan");
INSERT INTO transaction VALUES("783","132","7","107","Visa Payable","15800","0","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV109,Pax :USMAN PORKKALAM, PassNo :J6960023/ visaFor:90 DAYS DXB SETTING,By:Safuvan");
INSERT INTO transaction VALUES("784","7","132","107","Visa Payable","0","15800","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV109,Pax :USMAN PORKKALAM, PassNo :J6960023/ visaFor:90 DAYS DXB SETTING,By:Safuvan");
INSERT INTO transaction VALUES("785","154","4","19","Airline Receivable","0","18500","Airline Receivable: 19","2016-09-10","2016-09-10","6","2","2","0","INV#IA19, Pax:CHEMMUKKAN/MOHAMED SHEHEER, Passport No:L5482407, Sector:CCJ/SHJ, Date:20-09-2016, Tkt:098-9165446755, PNR:0, By:Manu");
INSERT INTO transaction VALUES("786","4","154","19","Airline Receivable","18500","0","Airline Receivable: 19","2016-09-10","2016-09-10","6","2","2","0","INV#IA19, Pax:CHEMMUKKAN/MOHAMED SHEHEER, Passport No:L5482407, Sector:CCJ/SHJ, Date:20-09-2016, Tkt:098-9165446755, PNR:0, By:Manu");
INSERT INTO transaction VALUES("787","83","5","19","Airline Payable","17540","0","","2016-09-10","2016-09-10","6","2","2","0","Details:INV#IA19, Pax:CHEMMUKKAN/MOHAMED SHEHEER, Passport No:L5482407, Sector:CCJ/SHJ, Date:20-09-2016, Tkt:098-9165446755, PNR:0, By:Manu");
INSERT INTO transaction VALUES("788","5","83","19","Airline Payable","0","17540","","2016-09-10","2016-09-10","6","2","2","0","Details:INV#IA19, Pax:CHEMMUKKAN/MOHAMED SHEHEER, Passport No:L5482407, Sector:CCJ/SHJ, Date:20-09-2016, Tkt:098-9165446755, PNR:0, By:Manu");
INSERT INTO transaction VALUES("789","96","133","10","Bank Receipts","26300","0","","2016-09-10","2016-09-09","33","1","2","0","VouNo#BR10,INV#IV4,IV105,IV106 Received By :Faizal,Details:INV#IV106,Pax :KADAMBU NEBISA, PassNo :L6156737.P2778220/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("790","133","96","10","Bank Receipts","0","26300","","2016-09-10","2016-09-09","33","1","2","0","VouNo#BR10,INV#IV4,IV105,IV106 Received By :Faizal,Details:INV#IV106,Pax :KADAMBU NEBISA, PassNo :L6156737.P2778220/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("791","156","4","20","Airline Receivable","0","15500","Airline Receivable: 20","2016-09-10","2016-09-10","6","2","2","0","INV#IA20, Pax:CHEMMUKKIL/MOHAMED HANEEFA, Passport No:L1182314, Sector:BOM/AUH, Date:20-09-2016/20-09-2016, Tkt:098-9165446759/098-9165446759, PNR:34DZAZ, By:Manu");
INSERT INTO transaction VALUES("792","4","156","20","Airline Receivable","15500","0","Airline Receivable: 20","2016-09-10","2016-09-10","6","2","2","0","INV#IA20, Pax:CHEMMUKKIL/MOHAMED HANEEFA, Passport No:L1182314, Sector:BOM/AUH, Date:20-09-2016/20-09-2016, Tkt:098-9165446759/098-9165446759, PNR:34DZAZ, By:Manu");
INSERT INTO transaction VALUES("793","83","5","20","Airline Payable","14750","0","","2016-09-10","2016-09-10","6","2","2","0","Details:INV#IA20, Pax:CHEMMUKKIL/MOHAMED HANEEFA, Passport No:L1182314, Sector:BOM/AUH, Date:20-09-2016/20-09-2016, Tkt:098-9165446759/098-9165446759, PNR:34DZAZ, By:Manu");
INSERT INTO transaction VALUES("794","5","83","20","Airline Payable","0","14750","","2016-09-10","2016-09-10","6","2","2","0","Details:INV#IA20, Pax:CHEMMUKKIL/MOHAMED HANEEFA, Passport No:L1182314, Sector:BOM/AUH, Date:20-09-2016/20-09-2016, Tkt:098-9165446759/098-9165446759, PNR:34DZAZ, By:Manu");
INSERT INTO transaction VALUES("795","156","2","10","Cash Receipts","15500","0","","2016-09-14","2016-09-14","6","2","2","0","VouNo#CR 10,INV#IA20 Received By :Manu,Details:INV#IA20, Pax:CHEMMUKKIL/MOHAMED HANEEFA, Passport No:L1182314, Sector:BOM/AUH, Date:20-09-2016/20-09-2016, Tkt:098-9165446759/098-9165446759, PNR:34DZAZ, By:Manu");
INSERT INTO transaction VALUES("796","2","156","10","Cash Receipts","0","15500","","2016-09-14","2016-09-14","6","2","2","0","VouNo#CR 10,INV#IA20 Received By :Manu,Details:INV#IA20, Pax:CHEMMUKKIL/MOHAMED HANEEFA, Passport No:L1182314, Sector:BOM/AUH, Date:20-09-2016/20-09-2016, Tkt:098-9165446759/098-9165446759, PNR:34DZAZ, By:Manu");
INSERT INTO transaction VALUES("797","96","6","110","Visa Receivable","0","14700","Visa Receivable : 110","2016-09-14","2016-09-14","8","1","2","0","INV#IV110,Pax :SEBEER PANEER, PassNo :L1356529/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("798","6","96","110","Visa Receivable","14700","0","Visa Receivable : 110","2016-09-14","2016-09-14","8","1","2","0","INV#IV110,Pax :SEBEER PANEER, PassNo :L1356529/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("799","94","7","108","Visa Payable","14200","0","","2016-09-14","2016-09-14","8","1","2","0","Details:INV#IV110,Pax :SEBEER PANEER, PassNo :L1356529/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("800","7","94","108","Visa Payable","0","14200","","2016-09-14","2016-09-14","8","1","2","0","Details:INV#IV110,Pax :SEBEER PANEER, PassNo :L1356529/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("801","92","6","3","Visa Receivable","0","17200","Visa Receivable : 3","2016-09-14","2016-09-14","6","2","2","0","INV#IV3,Pax :POOVATIL/JUHANA , PassNo :M2664637/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("802","6","92","3","Visa Receivable","17200","0","Visa Receivable : 3","2016-09-14","2016-09-14","6","2","2","0","INV#IV3,Pax :POOVATIL/JUHANA , PassNo :M2664637/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("803","157","7","3","Visa Payable","15700","0","","2016-09-14","2016-09-14","6","2","2","0","Details:INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("804","7","157","3","Visa Payable","0","15700","","2016-09-14","2016-09-14","6","2","2","0","Details:INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("805","92","2","11","Cash Receipts","46500","0","","2016-09-14","2016-09-14","6","2","2","0","VouNo#CR 11,INV#IA7,IA8,IV3 Received By :Manu,Details:INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("806","2","92","11","Cash Receipts","0","46500","","2016-09-14","2016-09-14","6","2","2","0","VouNo#CR 11,INV#IA7,IA8,IV3 Received By :Manu,Details:INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("807","150","2","12","Cash Receipts","68000","0","","2016-09-14","2016-09-14","6","2","2","0","VouNo#CR 12,INV#IA18 Received By :Manu,Details:INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("808","2","150","12","Cash Receipts","0","68000","","2016-09-14","2016-09-14","6","2","2","0","VouNo#CR 12,INV#IA18 Received By :Manu,Details:INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("809","154","2","13","Cash Receipts","18500","0","","2016-09-15","2016-09-15","6","2","2","0","VouNo#CR 13,INV#IA19 Received By :Manu,Details:INV#IA19, Pax:CHEMMUKKAN/MOHAMED SHEHEER, Passport No:L5482407, Sector:CCJ/SHJ, Date:20-09-2016, Tkt:098-9165446755, PNR:0, By:Manu");
INSERT INTO transaction VALUES("810","2","154","13","Cash Receipts","0","18500","","2016-09-15","2016-09-15","6","2","2","0","VouNo#CR 13,INV#IA19 Received By :Manu,Details:INV#IA19, Pax:CHEMMUKKAN/MOHAMED SHEHEER, Passport No:L5482407, Sector:CCJ/SHJ, Date:20-09-2016, Tkt:098-9165446755, PNR:0, By:Manu");
INSERT INTO transaction VALUES("813","128","4","21","Airline Receivable","0","29400","Airline Receivable: 21","2016-09-15","2016-09-15","6","2","2","0","INV#IA21, Pax:POOKAYIL/SIDHEEK, Passport No:J2072600, Sector:CCJ/SHJ, Date:16-09-2016, Tkt:BSZ6TZ, PNR:0, By:Manu");
INSERT INTO transaction VALUES("814","4","128","21","Airline Receivable","29400","0","Airline Receivable: 21","2016-09-15","2016-09-15","6","2","2","0","INV#IA21, Pax:POOKAYIL/SIDHEEK, Passport No:J2072600, Sector:CCJ/SHJ, Date:16-09-2016, Tkt:BSZ6TZ, PNR:0, By:Manu");
INSERT INTO transaction VALUES("815","81","5","21","Airline Payable","29260","0","","2016-09-15","2016-09-15","6","2","2","0","Details:INV#IA21, Pax:POOKAYIL/SIDHEEK, Passport No:J2072600, Sector:CCJ/SHJ, Date:16-09-2016, Tkt:BSZ6TZ, PNR:0, By:Manu");
INSERT INTO transaction VALUES("816","5","81","21","Airline Payable","0","29260","","2016-09-15","2016-09-15","6","2","2","0","Details:INV#IA21, Pax:POOKAYIL/SIDHEEK, Passport No:J2072600, Sector:CCJ/SHJ, Date:16-09-2016, Tkt:BSZ6TZ, PNR:0, By:Manu");
INSERT INTO transaction VALUES("827","162","4","1","Airline Receivable","0","13100","Airline Receivable: 1","2016-09-15","2016-09-15","35","3","2","0","INV#IA1, Pax:ALI ASKAR, Passport No:N2455497, Sector:COK/AUH, Date:20-09-2016, Tkt:098 9165446922, PNR:0, By:Shamshad Ali");
INSERT INTO transaction VALUES("828","4","162","1","Airline Receivable","13100","0","Airline Receivable: 1","2016-09-15","2016-09-15","35","3","2","0","INV#IA1, Pax:ALI ASKAR, Passport No:N2455497, Sector:COK/AUH, Date:20-09-2016, Tkt:098 9165446922, PNR:0, By:Shamshad Ali");
INSERT INTO transaction VALUES("829","159","5","1","Airline Payable","12600","0","","2016-09-15","2016-09-15","35","3","2","0","Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("830","5","159","1","Airline Payable","0","12600","","2016-09-15","2016-09-15","35","3","2","0","Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("835","162","2","1","Cash Receipts","13100","0","","2016-09-15","2016-09-15","35","3","2","0","VouNo#CR 1,INV#IA1 Received By :Shamshad Ali,Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("836","2","162","1","Cash Receipts","0","13100","","2016-09-15","2016-09-15","35","3","2","0","VouNo#CR 1,INV#IA1 Received By :Shamshad Ali,Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("837","83","2","5","Cash Payments","0","61405","Old  21830","2016-09-15","2016-09-15","6","2","2","0","VouNo#CP5,INV#PA14,PA16,PA17 By :Manu To : ");
INSERT INTO transaction VALUES("838","2","83","5","Cash Payments","61405","0","Old  21830","2016-09-15","2016-09-15","6","2","2","0","VouNo#CP5,INV#PA14,PA16,PA17 By :Manu To : ");
INSERT INTO transaction VALUES("839","95","6","111","Visa Receivable","0","1200","Visa Receivable : 111","2016-09-15","2016-09-15","10","1","2","0","INV#IV111,Pax :ANU MOLE, PassNo :k000000/Pax :SABEERA, PassNo :K000000/Pax :REMAKUMARI, PassNo :K000000/Pax :SHAHARAS, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("840","6","95","111","Visa Receivable","1200","0","Visa Receivable : 111","2016-09-15","2016-09-15","10","1","2","0","INV#IV111,Pax :ANU MOLE, PassNo :k000000/Pax :SABEERA, PassNo :K000000/Pax :REMAKUMARI, PassNo :K000000/Pax :SHAHARAS, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("841","94","7","109","Visa Payable","1200","0","","2016-09-15","2016-09-15","10","1","2","0","Details:INV#IV111,Pax :ANU MOLE, PassNo :k000000/Pax :SABEERA, PassNo :K000000/Pax :REMAKUMARI, PassNo :K000000/Pax :SHAHARAS, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("842","7","94","109","Visa Payable","0","1200","","2016-09-15","2016-09-15","10","1","2","0","Details:INV#IV111,Pax :ANU MOLE, PassNo :k000000/Pax :SABEERA, PassNo :K000000/Pax :REMAKUMARI, PassNo :K000000/Pax :SHAHARAS, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("845","88","10","1","Hotel Receivable","0","8","Hotel Receivable : 1","2016-09-16","2016-09-16","6","2","2","0","INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("846","10","88","1","Hotel Receivable","8","0","Hotel Receivable : 1","2016-09-16","2016-09-16","6","2","2","0","INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("847","16","88","1","Hotel Receivable","0","2","Hotel Receivable : 1","2016-09-16","2016-09-16","6","2","2","0","INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("848","88","16","1","Hotel Receivable","2","0","Hotel Receivable : 1","2016-09-16","2016-09-16","6","2","2","0","INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("849","88","19","1","Hotel Receivable","0","1","Hotel Receivable:Tds#1","2016-09-16","2016-09-16","6","2","2","0","INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("850","19","88","1","Hotel Receivable","1","0","Hotel Receivable:Tds#1","2016-09-16","2016-09-16","6","2","2","0","INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("851","88","18","1","Hotel Receivable","0","3","Hotel Receivable:Tax#1","2016-09-16","2016-09-16","6","2","2","0","INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("852","18","88","1","Hotel Receivable","3","0","Hotel Receivable:Tax#1","2016-09-16","2016-09-16","6","2","2","0","INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("853","81","11","1","Hotel Payable","6","0","","2016-09-16","2016-09-16","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("854","11","81","1","Hotel Payable","0","6","","2016-09-16","2016-09-16","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("855","81","16","1","Hotel Payable","2","0","Discount Given:inv#1","2016-09-16","2016-09-16","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("856","16","81","1","Hotel Payable","0","2","Discount Given:inv#1","2016-09-16","2016-09-16","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("857","81","20","1","Hotel Payable","0","2","Tac:inv#1","2016-09-16","2016-09-16","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("858","20","81","1","Hotel Payable","2","0","Tac:inv#1","2016-09-16","2016-09-16","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("859","164","6","112","Visa Receivable","0","30600","Visa Receivable : 112","2016-09-16","2016-09-16","9","1","2","0","INV#IV112,Pax :RIYAS, PassNo :P1018115/Pax :RAHINA THOUFEEK, PassNo :P0412729/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("860","6","164","112","Visa Receivable","30600","0","Visa Receivable : 112","2016-09-16","2016-09-16","9","1","2","0","INV#IV112,Pax :RIYAS, PassNo :P1018115/Pax :RAHINA THOUFEEK, PassNo :P0412729/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("861","132","7","110","Visa Payable","29600","0","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV112,Pax :RIYAS, PassNo :P1018115/Pax :RAHINA THOUFEEK, PassNo :P0412729/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("862","7","132","110","Visa Payable","0","29600","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV112,Pax :RIYAS, PassNo :P1018115/Pax :RAHINA THOUFEEK, PassNo :P0412729/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("863","95","6","113","Visa Receivable","0","29200","Visa Receivable : 113","2016-09-16","2016-09-16","9","1","2","0","INV#IV113,Pax :KUNJHU MOIDEEN, PassNo :K0385278/Pax :AYISHA, PassNo :K0385277/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("864","6","95","113","Visa Receivable","29200","0","Visa Receivable : 113","2016-09-16","2016-09-16","9","1","2","0","INV#IV113,Pax :KUNJHU MOIDEEN, PassNo :K0385278/Pax :AYISHA, PassNo :K0385277/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("865","94","7","111","Visa Payable","28200","0","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV113,Pax :KUNJHU MOIDEEN, PassNo :K0385278/Pax :AYISHA, PassNo :K0385277/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("866","7","94","111","Visa Payable","0","28200","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV113,Pax :KUNJHU MOIDEEN, PassNo :K0385278/Pax :AYISHA, PassNo :K0385277/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("867","99","6","114","Visa Receivable","0","14600","Visa Receivable : 114","2016-09-16","2016-09-16","9","1","2","0","INV#IV114,Pax :NITHEESH, PassNo :M2502907/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("868","6","99","114","Visa Receivable","14600","0","Visa Receivable : 114","2016-09-16","2016-09-16","9","1","2","0","INV#IV114,Pax :NITHEESH, PassNo :M2502907/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("869","94","7","112","Visa Payable","14100","0","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV114,Pax :NITHEESH, PassNo :M2502907/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("870","7","94","112","Visa Payable","0","14100","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV114,Pax :NITHEESH, PassNo :M2502907/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("871","95","6","115","Visa Receivable","0","14600","Visa Receivable : 115","2016-09-16","2016-09-16","9","1","2","0","INV#IV115,Pax :DAVIES MANJALY, PassNo :H9993108/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("872","6","95","115","Visa Receivable","14600","0","Visa Receivable : 115","2016-09-16","2016-09-16","9","1","2","0","INV#IV115,Pax :DAVIES MANJALY, PassNo :H9993108/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("873","94","7","113","Visa Payable","14100","0","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV115,Pax :DAVIES MANJALY, PassNo :H9993108/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("874","7","94","113","Visa Payable","0","14100","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV115,Pax :DAVIES MANJALY, PassNo :H9993108/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("875","95","6","116","Visa Receivable","0","14700","Visa Receivable : 116","2016-09-16","2016-09-16","9","1","2","0","INV#IV116,Pax :NIRMALA, PassNo :N9068639/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("876","6","95","116","Visa Receivable","14700","0","Visa Receivable : 116","2016-09-16","2016-09-16","9","1","2","0","INV#IV116,Pax :NIRMALA, PassNo :N9068639/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("877","132","7","114","Visa Payable","14800","0","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV116,Pax :NIRMALA, PassNo :N9068639/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("878","7","132","114","Visa Payable","0","14800","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV116,Pax :NIRMALA, PassNo :N9068639/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("879","95","6","117","Visa Receivable","0","14600","Visa Receivable : 117","2016-09-16","2016-09-16","9","1","2","0","INV#IV117,Pax :SHANAZ, PassNo :P4074758/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("880","6","95","117","Visa Receivable","14600","0","Visa Receivable : 117","2016-09-16","2016-09-16","9","1","2","0","INV#IV117,Pax :SHANAZ, PassNo :P4074758/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("881","132","7","115","Visa Payable","14800","0","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV117,Pax :SHANAZ, PassNo :P4074758/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("882","7","132","115","Visa Payable","0","14800","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV117,Pax :SHANAZ, PassNo :P4074758/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("883","101","6","118","Visa Receivable","0","14800","Visa Receivable : 118","2016-09-16","2016-09-16","9","1","2","0","INV#IV118,Pax :DISHA ABISHEK, PassNo :P2352218/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("884","6","101","118","Visa Receivable","14800","0","Visa Receivable : 118","2016-09-16","2016-09-16","9","1","2","0","INV#IV118,Pax :DISHA ABISHEK, PassNo :P2352218/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("885","132","7","116","Visa Payable","14800","0","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV118,Pax :DISHA ABISHEK, PassNo :P2352218/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("886","7","132","116","Visa Payable","0","14800","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV118,Pax :DISHA ABISHEK, PassNo :P2352218/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("887","165","6","119","Visa Receivable","0","14600","Visa Receivable : 119","2016-09-16","2016-09-16","9","1","2","0","INV#IV119,Pax :YOONUS, PassNo :N554446/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("888","6","165","119","Visa Receivable","14600","0","Visa Receivable : 119","2016-09-16","2016-09-16","9","1","2","0","INV#IV119,Pax :YOONUS, PassNo :N554446/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("889","132","7","117","Visa Payable","14800","0","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV119,Pax :YOONUS, PassNo :N554446/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("890","7","132","117","Visa Payable","0","14800","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV119,Pax :YOONUS, PassNo :N554446/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("891","165","6","120","Visa Receivable","0","14600","Visa Receivable : 120","2016-09-16","2016-09-16","9","1","2","0","INV#IV120,Pax :EBRAHIM, PassNo :M1043706/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("892","6","165","120","Visa Receivable","14600","0","Visa Receivable : 120","2016-09-16","2016-09-16","9","1","2","0","INV#IV120,Pax :EBRAHIM, PassNo :M1043706/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("893","94","7","118","Visa Payable","14100","0","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV120,Pax :EBRAHIM, PassNo :M1043706/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("894","7","94","118","Visa Payable","0","14100","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV120,Pax :EBRAHIM, PassNo :M1043706/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("895","165","6","121","Visa Receivable","0","14600","Visa Receivable : 121","2016-09-16","2016-09-16","9","1","2","0","INV#IV121,Pax :AKBAR SHAJAHAN, PassNo :P3482267/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("896","6","165","121","Visa Receivable","14600","0","Visa Receivable : 121","2016-09-16","2016-09-16","9","1","2","0","INV#IV121,Pax :AKBAR SHAJAHAN, PassNo :P3482267/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("897","132","7","119","Visa Payable","14800","0","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV121,Pax :AKBAR SHAJAHAN, PassNo :P3482267/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("898","7","132","119","Visa Payable","0","14800","","2016-09-16","2016-09-16","9","1","2","0","Details:INV#IV121,Pax :AKBAR SHAJAHAN, PassNo :P3482267/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("907","81","5","0","Airline Payable","13170","0","","2016-09-17","0000-00-00","6","2","2","0","INV#,sup:81,:against:,Details:");
INSERT INTO transaction VALUES("908","5","81","0","Airline Payable","0","13170","","2016-09-17","0000-00-00","6","2","2","0","INV#,sup:81,:against:,Details:");
INSERT INTO transaction VALUES("909","166","4","22","Airline Receivable","0","5300","Airline Receivable: 22","2016-09-17","2016-09-17","6","2","2","0","INV#IA22, Pax:ANWAR PALLIKKALAKATH, Passport No:M6141348, Sector:CCJ/DXB, Date:18-10-2016, Tkt:I6073692, PNR:0, By:Manu");
INSERT INTO transaction VALUES("910","4","166","22","Airline Receivable","5300","0","Airline Receivable: 22","2016-09-17","2016-09-17","6","2","2","0","INV#IA22, Pax:ANWAR PALLIKKALAKATH, Passport No:M6141348, Sector:CCJ/DXB, Date:18-10-2016, Tkt:I6073692, PNR:0, By:Manu");
INSERT INTO transaction VALUES("911","84","5","22","Airline Payable","5100","0","","2016-09-17","2016-09-17","6","2","2","0","Details:INV#IA22, Pax:ANWAR PALLIKKALAKATH, Passport No:M6141348, Sector:CCJ/DXB, Date:18-10-2016, Tkt:I6073692, PNR:0, By:Manu");
INSERT INTO transaction VALUES("912","5","84","22","Airline Payable","0","5100","","2016-09-17","2016-09-17","6","2","2","0","Details:INV#IA22, Pax:ANWAR PALLIKKALAKATH, Passport No:M6141348, Sector:CCJ/DXB, Date:18-10-2016, Tkt:I6073692, PNR:0, By:Manu");
INSERT INTO transaction VALUES("913","167","4","23","Airline Receivable","0","14000","Airline Receivable: 23","2016-09-17","2016-09-17","6","2","2","0","INV#IA23, Pax:RASHEED NARUNGALI , NEENAN MARAKKAR, Passport No:F9251602, Sector:DXB/CCJ, Date:21-09-2016, Tkt:UPQB5X, PNR:0, By:Manu");
INSERT INTO transaction VALUES("914","4","167","23","Airline Receivable","14000","0","Airline Receivable: 23","2016-09-17","2016-09-17","6","2","2","0","INV#IA23, Pax:RASHEED NARUNGALI , NEENAN MARAKKAR, Passport No:F9251602, Sector:DXB/CCJ, Date:21-09-2016, Tkt:UPQB5X, PNR:0, By:Manu");
INSERT INTO transaction VALUES("915","81","5","23","Airline Payable","13170","0","","2016-09-17","2016-09-17","6","2","2","0","Details:INV#IA23, Pax:RASHEED NARUNGALI , NEENAN MARAKKAR, Passport No:F9251602, Sector:DXB/CCJ, Date:21-09-2016, Tkt:UPQB5X, PNR:0, By:Manu");
INSERT INTO transaction VALUES("916","5","81","23","Airline Payable","0","13170","","2016-09-17","2016-09-17","6","2","2","0","Details:INV#IA23, Pax:RASHEED NARUNGALI , NEENAN MARAKKAR, Passport No:F9251602, Sector:DXB/CCJ, Date:21-09-2016, Tkt:UPQB5X, PNR:0, By:Manu");
INSERT INTO transaction VALUES("917","166","2","14","Cash Receipts","2000","0","","2016-09-17","2016-09-17","6","2","2","0","VouNo#CR 14,INV#IA22 Received By :Manu,Details:INV#IA22, Pax:ANWAR PALLIKKALAKATH, Passport No:M6141348, Sector:CCJ/DXB, Date:18-10-2016, Tkt:I6073692, PNR:0, By:Manu");
INSERT INTO transaction VALUES("918","2","166","14","Cash Receipts","0","2000","","2016-09-17","2016-09-17","6","2","2","0","VouNo#CR 14,INV#IA22 Received By :Manu,Details:INV#IA22, Pax:ANWAR PALLIKKALAKATH, Passport No:M6141348, Sector:CCJ/DXB, Date:18-10-2016, Tkt:I6073692, PNR:0, By:Manu");
INSERT INTO transaction VALUES("919","168","4","24","Airline Receivable","0","10600","Airline Receivable: 24","2016-09-17","2016-09-17","6","2","2","0","INV#IA24, Pax:MUHAMMED AFSAL CHEMMALA, Passport No:G4251068, Sector:CCJ/AUH, Date:24-09-2016, Tkt:74LUSH, PNR:0, By:Manu");
INSERT INTO transaction VALUES("920","4","168","24","Airline Receivable","10600","0","Airline Receivable: 24","2016-09-17","2016-09-17","6","2","2","0","INV#IA24, Pax:MUHAMMED AFSAL CHEMMALA, Passport No:G4251068, Sector:CCJ/AUH, Date:24-09-2016, Tkt:74LUSH, PNR:0, By:Manu");
INSERT INTO transaction VALUES("921","81","5","24","Airline Payable","10200","0","Cash payed in IX CCJ office","2016-09-17","2016-09-17","6","2","2","0","Details:INV#IA24, Pax:MUHAMMED AFSAL CHEMMALA, Passport No:G4251068, Sector:CCJ/AUH, Date:24-09-2016, Tkt:74LUSH, PNR:0, By:Manu");
INSERT INTO transaction VALUES("922","5","81","24","Airline Payable","0","10200","Cash payed in IX CCJ office","2016-09-17","2016-09-17","6","2","2","0","Details:INV#IA24, Pax:MUHAMMED AFSAL CHEMMALA, Passport No:G4251068, Sector:CCJ/AUH, Date:24-09-2016, Tkt:74LUSH, PNR:0, By:Manu");
INSERT INTO transaction VALUES("923","165","6","122","Visa Receivable","0","14600","Visa Receivable : 122","2016-09-17","2016-09-17","9","1","2","0","INV#IV122,Pax :ASKAR, PassNo :N0925550/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("924","6","165","122","Visa Receivable","14600","0","Visa Receivable : 122","2016-09-17","2016-09-17","9","1","2","0","INV#IV122,Pax :ASKAR, PassNo :N0925550/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("925","94","7","120","Visa Payable","14100","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV122,Pax :ASKAR, PassNo :N0925550/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("926","7","94","120","Visa Payable","0","14100","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV122,Pax :ASKAR, PassNo :N0925550/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("927","99","6","123","Visa Receivable","0","14800","Visa Receivable : 123","2016-09-17","2016-09-17","9","1","2","0","INV#IV123,Pax :TREESA JOSE, PassNo :K7236815/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("928","6","99","123","Visa Receivable","14800","0","Visa Receivable : 123","2016-09-17","2016-09-17","9","1","2","0","INV#IV123,Pax :TREESA JOSE, PassNo :K7236815/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("929","132","7","121","Visa Payable","14600","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV123,Pax :TREESA JOSE, PassNo :K7236815/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("930","7","132","121","Visa Payable","0","14600","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV123,Pax :TREESA JOSE, PassNo :K7236815/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("931","95","6","124","Visa Receivable","0","14600","Visa Receivable : 124","2016-09-17","2016-09-17","9","1","2","0","INV#IV124,Pax :AMIN SHERIF, PassNo :J8570711/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("932","6","95","124","Visa Receivable","14600","0","Visa Receivable : 124","2016-09-17","2016-09-17","9","1","2","0","INV#IV124,Pax :AMIN SHERIF, PassNo :J8570711/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("933","94","7","122","Visa Payable","14100","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV124,Pax :AMIN SHERIF, PassNo :J8570711/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("934","7","94","122","Visa Payable","0","14100","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV124,Pax :AMIN SHERIF, PassNo :J8570711/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("935","130","6","125","Visa Receivable","0","5100","Visa Receivable : 125","2016-09-17","2016-09-17","9","1","2","0","INV#IV125,Pax :JAFER, PassNo :L6355058/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("936","6","130","125","Visa Receivable","5100","0","Visa Receivable : 125","2016-09-17","2016-09-17","9","1","2","0","INV#IV125,Pax :JAFER, PassNo :L6355058/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("937","94","7","123","Visa Payable","5100","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV125,Pax :JAFER, PassNo :L6355058/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("938","7","94","123","Visa Payable","0","5100","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV125,Pax :JAFER, PassNo :L6355058/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("939","95","6","126","Visa Receivable","0","29200","Visa Receivable : 126","2016-09-17","2016-09-17","9","1","2","0","INV#IV126,Pax :ABDUL SHERIF, PassNo :L7528787/Pax :SARFUNNIZA, PassNo :N2147570/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("940","6","95","126","Visa Receivable","29200","0","Visa Receivable : 126","2016-09-17","2016-09-17","9","1","2","0","INV#IV126,Pax :ABDUL SHERIF, PassNo :L7528787/Pax :SARFUNNIZA, PassNo :N2147570/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("941","94","7","124","Visa Payable","28200","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV126,Pax :ABDUL SHERIF, PassNo :L7528787/Pax :SARFUNNIZA, PassNo :N2147570/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("942","7","94","124","Visa Payable","0","28200","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV126,Pax :ABDUL SHERIF, PassNo :L7528787/Pax :SARFUNNIZA, PassNo :N2147570/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("943","169","6","127","Visa Receivable","0","29000","Visa Receivable : 127","2016-09-17","2016-09-17","9","1","2","0","INV#IV127,Pax :SARA IBRAHIM, PassNo :G3509227/Pax :ATIFUL DULFIYA, PassNo :P4000338/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("944","6","169","127","Visa Receivable","29000","0","Visa Receivable : 127","2016-09-17","2016-09-17","9","1","2","0","INV#IV127,Pax :SARA IBRAHIM, PassNo :G3509227/Pax :ATIFUL DULFIYA, PassNo :P4000338/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("945","132","7","125","Visa Payable","29200","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV127,Pax :SARA IBRAHIM, PassNo :G3509227/Pax :ATIFUL DULFIYA, PassNo :P4000338/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("946","7","132","125","Visa Payable","0","29200","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV127,Pax :SARA IBRAHIM, PassNo :G3509227/Pax :ATIFUL DULFIYA, PassNo :P4000338/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("947","169","6","128","Visa Receivable","0","14500","Visa Receivable : 128","2016-09-17","2016-09-17","9","1","2","0","INV#IV128,Pax :ABDULLA, PassNo :G9716233/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("948","6","169","128","Visa Receivable","14500","0","Visa Receivable : 128","2016-09-17","2016-09-17","9","1","2","0","INV#IV128,Pax :ABDULLA, PassNo :G9716233/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("949","94","7","126","Visa Payable","14100","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV128,Pax :ABDULLA, PassNo :G9716233/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("950","7","94","126","Visa Payable","0","14100","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV128,Pax :ABDULLA, PassNo :G9716233/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("951","169","6","129","Visa Receivable","0","58000","Visa Receivable : 129","2016-09-17","2016-09-17","9","1","2","0","INV#IV129,Pax :NUSRATH, PassNo :M2789304/Pax :MINHA FATHIMA, PassNo :M4145622/Pax :MALLIK KUMARSWAMY, PassNo :J8582907/Pax :MARIYA, PassNo :L1316251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("952","6","169","129","Visa Receivable","58000","0","Visa Receivable : 129","2016-09-17","2016-09-17","9","1","2","0","INV#IV129,Pax :NUSRATH, PassNo :M2789304/Pax :MINHA FATHIMA, PassNo :M4145622/Pax :MALLIK KUMARSWAMY, PassNo :J8582907/Pax :MARIYA, PassNo :L1316251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("953","132","7","127","Visa Payable","58400","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV129,Pax :NUSRATH, PassNo :M2789304/Pax :MINHA FATHIMA, PassNo :M4145622/Pax :MALLIK KUMARSWAMY, PassNo :J8582907/Pax :MARIYA, PassNo :L1316251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("954","7","132","127","Visa Payable","0","58400","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV129,Pax :NUSRATH, PassNo :M2789304/Pax :MINHA FATHIMA, PassNo :M4145622/Pax :MALLIK KUMARSWAMY, PassNo :J8582907/Pax :MARIYA, PassNo :L1316251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("955","169","6","130","Visa Receivable","0","29000","Visa Receivable : 130","2016-09-17","2016-09-17","9","1","2","0","INV#IV130,Pax :SAIF ABDUL AZEEZ, PassNo :H7400492/Pax :ABDUL RAHIMAN, PassNo :L3684474/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("956","6","169","130","Visa Receivable","29000","0","Visa Receivable : 130","2016-09-17","2016-09-17","9","1","2","0","INV#IV130,Pax :SAIF ABDUL AZEEZ, PassNo :H7400492/Pax :ABDUL RAHIMAN, PassNo :L3684474/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("957","94","7","128","Visa Payable","28200","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV130,Pax :SAIF ABDUL AZEEZ, PassNo :H7400492/Pax :ABDUL RAHIMAN, PassNo :L3684474/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("958","7","94","128","Visa Payable","0","28200","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV130,Pax :SAIF ABDUL AZEEZ, PassNo :H7400492/Pax :ABDUL RAHIMAN, PassNo :L3684474/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("959","169","6","131","Visa Receivable","0","29000","Visa Receivable : 131","2016-09-17","2016-09-17","9","1","2","0","INV#IV131,Pax :MURSINA, PassNo :P0092227/Pax :PRAJITHA, PassNo :N3122093/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("960","6","169","131","Visa Receivable","29000","0","Visa Receivable : 131","2016-09-17","2016-09-17","9","1","2","0","INV#IV131,Pax :MURSINA, PassNo :P0092227/Pax :PRAJITHA, PassNo :N3122093/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("961","132","7","129","Visa Payable","29200","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV131,Pax :MURSINA, PassNo :P0092227/Pax :PRAJITHA, PassNo :N3122093/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("962","7","132","129","Visa Payable","0","29200","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV131,Pax :MURSINA, PassNo :P0092227/Pax :PRAJITHA, PassNo :N3122093/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("963","169","6","132","Visa Receivable","0","145000","Visa Receivable : 132","2016-09-17","2016-09-17","9","1","2","0","INV#IV132,Pax :RASHIDH , PassNo :P4059622/Pax :MANZOOR, PassNo :N9960716/Pax :FIJAS, PassNo :P1282872/Pax :VIPIN, PassNo :L4827297/Pax :SALIH, PassNo :K4635001/Pax :SHANAVAS, PassNo :L9686169/Pax :SAHID MON, PassNo :H6480490/Pax :SANIDH, PassNo :M5654047/Pax :DILEEP, PassNo :K4038159/Pax :BAJEESH, PassNo :N4336784/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("964","6","169","132","Visa Receivable","145000","0","Visa Receivable : 132","2016-09-17","2016-09-17","9","1","2","0","INV#IV132,Pax :RASHIDH , PassNo :P4059622/Pax :MANZOOR, PassNo :N9960716/Pax :FIJAS, PassNo :P1282872/Pax :VIPIN, PassNo :L4827297/Pax :SALIH, PassNo :K4635001/Pax :SHANAVAS, PassNo :L9686169/Pax :SAHID MON, PassNo :H6480490/Pax :SANIDH, PassNo :M5654047/Pax :DILEEP, PassNo :K4038159/Pax :BAJEESH, PassNo :N4336784/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("965","94","7","130","Visa Payable","141000","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV132,Pax :RASHIDH , PassNo :P4059622/Pax :MANZOOR, PassNo :N9960716/Pax :FIJAS, PassNo :P1282872/Pax :VIPIN, PassNo :L4827297/Pax :SALIH, PassNo :K4635001/Pax :SHANAVAS, PassNo :L9686169/Pax :SAHID MON, PassNo :H6480490/Pax :SANIDH, PassNo :M5654047/Pax :DILEEP, PassNo :K4038159/Pax :BAJEESH, PassNo :N4336784/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("966","7","94","130","Visa Payable","0","141000","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV132,Pax :RASHIDH , PassNo :P4059622/Pax :MANZOOR, PassNo :N9960716/Pax :FIJAS, PassNo :P1282872/Pax :VIPIN, PassNo :L4827297/Pax :SALIH, PassNo :K4635001/Pax :SHANAVAS, PassNo :L9686169/Pax :SAHID MON, PassNo :H6480490/Pax :SANIDH, PassNo :M5654047/Pax :DILEEP, PassNo :K4038159/Pax :BAJEESH, PassNo :N4336784/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("967","169","6","133","Visa Receivable","0","29000","Visa Receivable : 133","2016-09-17","2016-09-17","9","1","2","0","INV#IV133,Pax :SANOOF, PassNo :N8234508/Pax :SHABEENA, PassNo :M8557330/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("968","6","169","133","Visa Receivable","29000","0","Visa Receivable : 133","2016-09-17","2016-09-17","9","1","2","0","INV#IV133,Pax :SANOOF, PassNo :N8234508/Pax :SHABEENA, PassNo :M8557330/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("969","132","7","131","Visa Payable","29200","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV133,Pax :SANOOF, PassNo :N8234508/Pax :SHABEENA, PassNo :M8557330/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("970","7","132","131","Visa Payable","0","29200","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV133,Pax :SANOOF, PassNo :N8234508/Pax :SHABEENA, PassNo :M8557330/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("971","170","6","134","Visa Receivable","0","16200","Visa Receivable : 134","2016-09-17","2016-09-17","9","1","2","0","INV#IV134,Pax :NOORJAHAN, PassNo :K9052971/Pax :MOHAMMED ISMAIL, PassNo :Z2209883/Pax :ARIFA MAJEED, PassNo :K2442805/ visaFor:30 DAYS DXB WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("972","6","170","134","Visa Receivable","16200","0","Visa Receivable : 134","2016-09-17","2016-09-17","9","1","2","0","INV#IV134,Pax :NOORJAHAN, PassNo :K9052971/Pax :MOHAMMED ISMAIL, PassNo :Z2209883/Pax :ARIFA MAJEED, PassNo :K2442805/ visaFor:30 DAYS DXB WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("973","94","7","132","Visa Payable","15300","0","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV134,Pax :NOORJAHAN, PassNo :K9052971/Pax :MOHAMMED ISMAIL, PassNo :Z2209883/Pax :ARIFA MAJEED, PassNo :K2442805/ visaFor:30 DAYS DXB WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("974","7","94","132","Visa Payable","0","15300","","2016-09-17","2016-09-17","9","1","2","0","Details:INV#IV134,Pax :NOORJAHAN, PassNo :K9052971/Pax :MOHAMMED ISMAIL, PassNo :Z2209883/Pax :ARIFA MAJEED, PassNo :K2442805/ visaFor:30 DAYS DXB WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("991","169","6","135","Visa Receivable","0","29000","Visa Receivable : 135","2016-09-19","2016-09-19","9","1","2","0","INV#IV135,Pax :MUBASHIRA, PassNo :N5210758/Pax :USHA PADMANABAN, PassNo :H4651646/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("992","6","169","135","Visa Receivable","29000","0","Visa Receivable : 135","2016-09-19","2016-09-19","9","1","2","0","INV#IV135,Pax :MUBASHIRA, PassNo :N5210758/Pax :USHA PADMANABAN, PassNo :H4651646/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("993","132","7","133","Visa Payable","29200","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV135,Pax :MUBASHIRA, PassNo :N5210758/Pax :USHA PADMANABAN, PassNo :H4651646/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("994","7","132","133","Visa Payable","0","29200","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV135,Pax :MUBASHIRA, PassNo :N5210758/Pax :USHA PADMANABAN, PassNo :H4651646/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("995","169","6","136","Visa Receivable","0","101500","Visa Receivable : 136","2016-09-19","2016-09-19","9","1","2","0","INV#IV136,Pax :AFSAL ALI, PassNo :J8837024/Pax :VINI JOSE, PassNo :H9689436/Pax :RASHEED, PassNo :N9138711/Pax :MIDHLAJ, PassNo :L7920538/Pax :MOHAMMED LIBASIN, PassNo :H7232734/Pax :HYDERALI, PassNo :P4052692/Pax :ABDUL LATHEEF, PassNo :M2403126/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("996","6","169","136","Visa Receivable","101500","0","Visa Receivable : 136","2016-09-19","2016-09-19","9","1","2","0","INV#IV136,Pax :AFSAL ALI, PassNo :J8837024/Pax :VINI JOSE, PassNo :H9689436/Pax :RASHEED, PassNo :N9138711/Pax :MIDHLAJ, PassNo :L7920538/Pax :MOHAMMED LIBASIN, PassNo :H7232734/Pax :HYDERALI, PassNo :P4052692/Pax :ABDUL LATHEEF, PassNo :M2403126/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("997","94","7","134","Visa Payable","98700","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV136,Pax :AFSAL ALI, PassNo :J8837024/Pax :VINI JOSE, PassNo :H9689436/Pax :RASHEED, PassNo :N9138711/Pax :MIDHLAJ, PassNo :L7920538/Pax :MOHAMMED LIBASIN, PassNo :H7232734/Pax :HYDERALI, PassNo :P4052692/Pax :ABDUL LATHEEF, PassNo :M2403126/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("998","7","94","134","Visa Payable","0","98700","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV136,Pax :AFSAL ALI, PassNo :J8837024/Pax :VINI JOSE, PassNo :H9689436/Pax :RASHEED, PassNo :N9138711/Pax :MIDHLAJ, PassNo :L7920538/Pax :MOHAMMED LIBASIN, PassNo :H7232734/Pax :HYDERALI, PassNo :P4052692/Pax :ABDUL LATHEEF, PassNo :M2403126/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("999","169","6","137","Visa Receivable","0","58000","Visa Receivable : 137","2016-09-19","2016-09-19","9","1","2","0","INV#IV137,Pax :NIHAS, PassNo :M6039206/Pax :AZMIN FARAS, PassNo :L9706041/Pax :PHARISHA, PassNo :L9642701/Pax :GREESHMA, PassNo :N9499762/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1000","6","169","137","Visa Receivable","58000","0","Visa Receivable : 137","2016-09-19","2016-09-19","9","1","2","0","INV#IV137,Pax :NIHAS, PassNo :M6039206/Pax :AZMIN FARAS, PassNo :L9706041/Pax :PHARISHA, PassNo :L9642701/Pax :GREESHMA, PassNo :N9499762/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1001","132","7","135","Visa Payable","58400","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV137,Pax :NIHAS, PassNo :M6039206/Pax :AZMIN FARAS, PassNo :L9706041/Pax :PHARISHA, PassNo :L9642701/Pax :GREESHMA, PassNo :N9499762/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1002","7","132","135","Visa Payable","0","58400","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV137,Pax :NIHAS, PassNo :M6039206/Pax :AZMIN FARAS, PassNo :L9706041/Pax :PHARISHA, PassNo :L9642701/Pax :GREESHMA, PassNo :N9499762/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1003","169","6","138","Visa Receivable","0","72500","Visa Receivable : 138","2016-09-19","2016-09-19","9","1","2","0","INV#IV138,Pax :THANZEER, PassNo :G5437199/Pax :VIDESH, PassNo :G4842975/Pax :MUSTHAFA, PassNo :K0825228/Pax :MOHAMMED BASHEER, PassNo :J5523932/Pax :JAYA GOPALAN, PassNo :H9597787/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1004","6","169","138","Visa Receivable","72500","0","Visa Receivable : 138","2016-09-19","2016-09-19","9","1","2","0","INV#IV138,Pax :THANZEER, PassNo :G5437199/Pax :VIDESH, PassNo :G4842975/Pax :MUSTHAFA, PassNo :K0825228/Pax :MOHAMMED BASHEER, PassNo :J5523932/Pax :JAYA GOPALAN, PassNo :H9597787/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1005","94","7","136","Visa Payable","70500","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV138,Pax :THANZEER, PassNo :G5437199/Pax :VIDESH, PassNo :G4842975/Pax :MUSTHAFA, PassNo :K0825228/Pax :MOHAMMED BASHEER, PassNo :J5523932/Pax :JAYA GOPALAN, PassNo :H9597787/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1006","7","94","136","Visa Payable","0","70500","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV138,Pax :THANZEER, PassNo :G5437199/Pax :VIDESH, PassNo :G4842975/Pax :MUSTHAFA, PassNo :K0825228/Pax :MOHAMMED BASHEER, PassNo :J5523932/Pax :JAYA GOPALAN, PassNo :H9597787/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1007","114","6","139","Visa Receivable","0","16000","Visa Receivable : 139","2016-09-19","2016-09-19","9","1","2","0","INV#IV139,Pax :UMAIR SHAMSUDHEEN, PassNo :M2434982/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1008","6","114","139","Visa Receivable","16000","0","Visa Receivable : 139","2016-09-19","2016-09-19","9","1","2","0","INV#IV139,Pax :UMAIR SHAMSUDHEEN, PassNo :M2434982/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1009","132","7","137","Visa Payable","15600","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV139,Pax :UMAIR SHAMSUDHEEN, PassNo :M2434982/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1010","7","132","137","Visa Payable","0","15600","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV139,Pax :UMAIR SHAMSUDHEEN, PassNo :M2434982/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1011","169","6","140","Visa Receivable","0","87000","Visa Receivable : 140","2016-09-19","2016-09-19","9","1","2","0","INV#IV140,Pax :FASALURAHMAN, PassNo :N2555523/Pax :ANWAR, PassNo :K9738262/Pax :FASEELA MUJEEB, PassNo :K1586353/Pax :MINHA MARIYAM, PassNo :K53600143/Pax :AYISHA THARAKKAL, PassNo :K3056624/Pax :SHAN ADAM, PassNo :M9113339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1012","6","169","140","Visa Receivable","87000","0","Visa Receivable : 140","2016-09-19","2016-09-19","9","1","2","0","INV#IV140,Pax :FASALURAHMAN, PassNo :N2555523/Pax :ANWAR, PassNo :K9738262/Pax :FASEELA MUJEEB, PassNo :K1586353/Pax :MINHA MARIYAM, PassNo :K53600143/Pax :AYISHA THARAKKAL, PassNo :K3056624/Pax :SHAN ADAM, PassNo :M9113339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1013","132","7","138","Visa Payable","87600","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV140,Pax :FASALURAHMAN, PassNo :N2555523/Pax :ANWAR, PassNo :K9738262/Pax :FASEELA MUJEEB, PassNo :K1586353/Pax :MINHA MARIYAM, PassNo :K53600143/Pax :AYISHA THARAKKAL, PassNo :K3056624/Pax :SHAN ADAM, PassNo :M9113339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1014","7","132","138","Visa Payable","0","87600","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV140,Pax :FASALURAHMAN, PassNo :N2555523/Pax :ANWAR, PassNo :K9738262/Pax :FASEELA MUJEEB, PassNo :K1586353/Pax :MINHA MARIYAM, PassNo :K53600143/Pax :AYISHA THARAKKAL, PassNo :K3056624/Pax :SHAN ADAM, PassNo :M9113339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1015","130","6","141","Visa Receivable","0","14700","Visa Receivable : 141","2016-09-19","2016-09-19","9","1","2","0","INV#IV141,Pax :ABDUL NASEER, PassNo :K9823040/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1016","6","130","141","Visa Receivable","14700","0","Visa Receivable : 141","2016-09-19","2016-09-19","9","1","2","0","INV#IV141,Pax :ABDUL NASEER, PassNo :K9823040/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1017","94","7","139","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV141,Pax :ABDUL NASEER, PassNo :K9823040/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1018","7","94","139","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV141,Pax :ABDUL NASEER, PassNo :K9823040/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1019","114","6","142","Visa Receivable","0","14600","Visa Receivable : 142","2016-09-19","2016-09-19","9","1","2","0","INV#IV142,Pax :ASTHER LOURD, PassNo :J1943467/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1020","6","114","142","Visa Receivable","14600","0","Visa Receivable : 142","2016-09-19","2016-09-19","9","1","2","0","INV#IV142,Pax :ASTHER LOURD, PassNo :J1943467/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1021","132","7","140","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV142,Pax :ASTHER LOURD, PassNo :J1943467/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1022","7","132","140","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV142,Pax :ASTHER LOURD, PassNo :J1943467/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1023","171","6","143","Visa Receivable","0","5500","Visa Receivable : 143","2016-09-19","2016-09-19","9","1","2","0","INV#IV143,Pax :SIAHUDHEEN, PassNo :K9057849/ visaFor:30 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1024","6","171","143","Visa Receivable","5500","0","Visa Receivable : 143","2016-09-19","2016-09-19","9","1","2","0","INV#IV143,Pax :SIAHUDHEEN, PassNo :K9057849/ visaFor:30 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1025","94","7","141","Visa Payable","5100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV143,Pax :SIAHUDHEEN, PassNo :K9057849/ visaFor:30 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1026","7","94","141","Visa Payable","0","5100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV143,Pax :SIAHUDHEEN, PassNo :K9057849/ visaFor:30 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1027","169","6","144","Visa Receivable","0","29000","Visa Receivable : 144","2016-09-19","2016-09-19","9","1","2","0","INV#IV144,Pax :KANZA, PassNo :N7026492/Pax :NAZIYA, PassNo :N8159169/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1028","6","169","144","Visa Receivable","29000","0","Visa Receivable : 144","2016-09-19","2016-09-19","9","1","2","0","INV#IV144,Pax :KANZA, PassNo :N7026492/Pax :NAZIYA, PassNo :N8159169/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1029","132","7","142","Visa Payable","29200","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV144,Pax :KANZA, PassNo :N7026492/Pax :NAZIYA, PassNo :N8159169/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1030","7","132","142","Visa Payable","0","29200","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV144,Pax :KANZA, PassNo :N7026492/Pax :NAZIYA, PassNo :N8159169/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1031","114","6","145","Visa Receivable","0","58800","Visa Receivable : 145","2016-09-19","2016-09-19","9","1","2","0","INV#IV145,Pax :BEEGUM, PassNo :M4585433/Pax :MUKTHAR MOHAMMED, PassNo :P0621596/Pax :ANSI BADAR, PassNo :L9385374/Pax :SWALIHA, PassNo :N3176850/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1032","6","114","145","Visa Receivable","58800","0","Visa Receivable : 145","2016-09-19","2016-09-19","9","1","2","0","INV#IV145,Pax :BEEGUM, PassNo :M4585433/Pax :MUKTHAR MOHAMMED, PassNo :P0621596/Pax :ANSI BADAR, PassNo :L9385374/Pax :SWALIHA, PassNo :N3176850/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1033","132","7","143","Visa Payable","58400","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV145,Pax :BEEGUM, PassNo :M4585433/Pax :MUKTHAR MOHAMMED, PassNo :P0621596/Pax :ANSI BADAR, PassNo :L9385374/Pax :SWALIHA, PassNo :N3176850/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1034","7","132","143","Visa Payable","0","58400","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV145,Pax :BEEGUM, PassNo :M4585433/Pax :MUKTHAR MOHAMMED, PassNo :P0621596/Pax :ANSI BADAR, PassNo :L9385374/Pax :SWALIHA, PassNo :N3176850/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1035","169","6","146","Visa Receivable","0","29000","Visa Receivable : 146","2016-09-19","2016-09-19","9","1","2","0","INV#IV146,Pax :SREEJITH, PassNo :L8535331/Pax :NOORUDHEEN, PassNo :K7515194/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1036","6","169","146","Visa Receivable","29000","0","Visa Receivable : 146","2016-09-19","2016-09-19","9","1","2","0","INV#IV146,Pax :SREEJITH, PassNo :L8535331/Pax :NOORUDHEEN, PassNo :K7515194/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1037","94","7","144","Visa Payable","28200","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV146,Pax :SREEJITH, PassNo :L8535331/Pax :NOORUDHEEN, PassNo :K7515194/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1038","7","94","144","Visa Payable","0","28200","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV146,Pax :SREEJITH, PassNo :L8535331/Pax :NOORUDHEEN, PassNo :K7515194/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1039","99","6","147","Visa Receivable","0","29200","Visa Receivable : 147","2016-09-19","2016-09-19","9","1","2","0","INV#IV147,Pax :SEBASTIAN, PassNo :H4429316/Pax :ANU CHETTU, PassNo :K1061340/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1040","6","99","147","Visa Receivable","29200","0","Visa Receivable : 147","2016-09-19","2016-09-19","9","1","2","0","INV#IV147,Pax :SEBASTIAN, PassNo :H4429316/Pax :ANU CHETTU, PassNo :K1061340/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1041","94","7","145","Visa Payable","28200","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV147,Pax :SEBASTIAN, PassNo :H4429316/Pax :ANU CHETTU, PassNo :K1061340/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1042","7","94","145","Visa Payable","0","28200","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV147,Pax :SEBASTIAN, PassNo :H4429316/Pax :ANU CHETTU, PassNo :K1061340/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1043","169","6","148","Visa Receivable","0","25625","Visa Receivable : 148","2016-09-19","2016-09-19","9","1","2","0","INV#IV148,Pax :AZAAN, PassNo :M2529672/Pax :SAIMA, PassNo :M2236990/Pax :SAKEENA, PassNo :L9018447/Pax :MOHAMMED ABID, PassNo :Z1910668/Pax :FEMINA, PassNo :K1583268/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1044","6","169","148","Visa Receivable","25625","0","Visa Receivable : 148","2016-09-19","2016-09-19","9","1","2","0","INV#IV148,Pax :AZAAN, PassNo :M2529672/Pax :SAIMA, PassNo :M2236990/Pax :SAKEENA, PassNo :L9018447/Pax :MOHAMMED ABID, PassNo :Z1910668/Pax :FEMINA, PassNo :K1583268/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1045","94","7","146","Visa Payable","25500","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV148,Pax :AZAAN, PassNo :M2529672/Pax :SAIMA, PassNo :M2236990/Pax :SAKEENA, PassNo :L9018447/Pax :MOHAMMED ABID, PassNo :Z1910668/Pax :FEMINA, PassNo :K1583268/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1046","7","94","146","Visa Payable","0","25500","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV148,Pax :AZAAN, PassNo :M2529672/Pax :SAIMA, PassNo :M2236990/Pax :SAKEENA, PassNo :L9018447/Pax :MOHAMMED ABID, PassNo :Z1910668/Pax :FEMINA, PassNo :K1583268/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1047","169","6","149","Visa Receivable","0","15375","Visa Receivable : 149","2016-09-19","2016-09-19","9","1","2","0","INV#IV149,Pax :JAKIR ALI, PassNo :J8710589/Pax :HARIHARA, PassNo :J8732899/Pax :SHEIKH, PassNo :J8711078/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1048","6","169","149","Visa Receivable","15375","0","Visa Receivable : 149","2016-09-19","2016-09-19","9","1","2","0","INV#IV149,Pax :JAKIR ALI, PassNo :J8710589/Pax :HARIHARA, PassNo :J8732899/Pax :SHEIKH, PassNo :J8711078/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1049","94","7","147","Visa Payable","15300","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV149,Pax :JAKIR ALI, PassNo :J8710589/Pax :HARIHARA, PassNo :J8732899/Pax :SHEIKH, PassNo :J8711078/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1050","7","94","147","Visa Payable","0","15300","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV149,Pax :JAKIR ALI, PassNo :J8710589/Pax :HARIHARA, PassNo :J8732899/Pax :SHEIKH, PassNo :J8711078/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1051","169","6","150","Visa Receivable","0","14500","Visa Receivable : 150","2016-09-19","2016-09-19","9","1","2","0","INV#IV150,Pax :UMMER ISMAYIL, PassNo :P4073615/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1052","6","169","150","Visa Receivable","14500","0","Visa Receivable : 150","2016-09-19","2016-09-19","9","1","2","0","INV#IV150,Pax :UMMER ISMAYIL, PassNo :P4073615/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1053","94","7","148","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV150,Pax :UMMER ISMAYIL, PassNo :P4073615/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1054","7","94","148","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV150,Pax :UMMER ISMAYIL, PassNo :P4073615/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1055","130","6","151","Visa Receivable","0","14700","Visa Receivable : 151","2016-09-19","2016-09-19","9","1","2","0","INV#IV151,Pax :TITTU, PassNo :K6437253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1056","6","130","151","Visa Receivable","14700","0","Visa Receivable : 151","2016-09-19","2016-09-19","9","1","2","0","INV#IV151,Pax :TITTU, PassNo :K6437253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1057","94","7","149","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV151,Pax :TITTU, PassNo :K6437253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1058","7","94","149","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV151,Pax :TITTU, PassNo :K6437253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1059","129","6","152","Visa Receivable","0","30200","Visa Receivable : 152","2016-09-19","2016-09-19","9","1","2","0","INV#IV152,Pax :SUHAIB, PassNo :L2822761/Pax :LALITHA, PassNo :J89747590/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("1060","6","129","152","Visa Receivable","30200","0","Visa Receivable : 152","2016-09-19","2016-09-19","9","1","2","0","INV#IV152,Pax :SUHAIB, PassNo :L2822761/Pax :LALITHA, PassNo :J89747590/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("1063","132","7","150","Visa Payable","29200","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV152,Pax :SUHAIB, PassNo :L2822761/Pax :LALITHA, PassNo :J89747590/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("1064","7","132","150","Visa Payable","0","29200","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV152,Pax :SUHAIB, PassNo :L2822761/Pax :LALITHA, PassNo :J89747590/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("1065","171","6","153","Visa Receivable","0","10500","Visa Receivable : 153","2016-09-19","2016-09-19","9","1","2","0","INV#IV153,Pax :BASHEER, PassNo :G2821855/ visaFor:30 DAYS DXB LSSUER,By:Sahad");
INSERT INTO transaction VALUES("1066","6","171","153","Visa Receivable","10500","0","Visa Receivable : 153","2016-09-19","2016-09-19","9","1","2","0","INV#IV153,Pax :BASHEER, PassNo :G2821855/ visaFor:30 DAYS DXB LSSUER,By:Sahad");
INSERT INTO transaction VALUES("1067","132","7","151","Visa Payable","10250","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV153,Pax :BASHEER, PassNo :G2821855/ visaFor:30 DAYS DXB LSSUER,By:Sahad");
INSERT INTO transaction VALUES("1068","7","132","151","Visa Payable","0","10250","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV153,Pax :BASHEER, PassNo :G2821855/ visaFor:30 DAYS DXB LSSUER,By:Sahad");
INSERT INTO transaction VALUES("1075","95","6","154","Visa Receivable","0","900","Visa Receivable : 154","2016-09-19","2016-09-19","10","1","2","0","INV#IV154,Pax :MADAVAS, PassNo :k000000/Pax :ASHRAF, PassNo :K000000/Pax :KAVITHA, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1076","6","95","154","Visa Receivable","900","0","Visa Receivable : 154","2016-09-19","2016-09-19","10","1","2","0","INV#IV154,Pax :MADAVAS, PassNo :k000000/Pax :ASHRAF, PassNo :K000000/Pax :KAVITHA, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1077","94","7","152","Visa Payable","900","0","","2016-09-19","2016-09-19","10","1","2","0","Details:INV#IV154,Pax :MADAVAS, PassNo :k000000/Pax :ASHRAF, PassNo :K000000/Pax :KAVITHA, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1078","7","94","152","Visa Payable","0","900","","2016-09-19","2016-09-19","10","1","2","0","Details:INV#IV154,Pax :MADAVAS, PassNo :k000000/Pax :ASHRAF, PassNo :K000000/Pax :KAVITHA, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1079","170","6","155","Visa Receivable","0","31400","Visa Receivable : 155","2016-09-19","2016-09-19","10","1","2","0","INV#IV155,Pax :SAHIR &ABDUL JALEEL, PassNo :L2371853/	N7113488	/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("1080","6","170","155","Visa Receivable","31400","0","Visa Receivable : 155","2016-09-19","2016-09-19","10","1","2","0","INV#IV155,Pax :SAHIR &ABDUL JALEEL, PassNo :L2371853/	N7113488	/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("1081","125","7","153","Visa Payable","31400","0","","2016-09-19","2016-09-19","10","1","2","0","Details:INV#IV155,Pax :SAHIR &ABDUL JALEEL, PassNo :L2371853/	N7113488	/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("1082","7","125","153","Visa Payable","0","31400","","2016-09-19","2016-09-19","10","1","2","0","Details:INV#IV155,Pax :SAHIR &ABDUL JALEEL, PassNo :L2371853/	N7113488	/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("1083","130","6","156","Visa Receivable","0","300","Visa Receivable : 156","2016-09-19","2016-09-19","10","1","2","0","INV#IV156,Pax :MATHUKUTTY, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1084","6","130","156","Visa Receivable","300","0","Visa Receivable : 156","2016-09-19","2016-09-19","10","1","2","0","INV#IV156,Pax :MATHUKUTTY, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1085","94","7","154","Visa Payable","300","0","","2016-09-19","2016-09-19","10","1","2","0","Details:INV#IV156,Pax :MATHUKUTTY, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1086","7","94","154","Visa Payable","0","300","","2016-09-19","2016-09-19","10","1","2","0","Details:INV#IV156,Pax :MATHUKUTTY, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1087","96","6","157","Visa Receivable","0","600","Visa Receivable : 157","2016-09-19","2016-09-19","10","1","2","0","INV#IV157,Pax :KADER, PassNo :k000000/Pax :AYISHA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1088","6","96","157","Visa Receivable","600","0","Visa Receivable : 157","2016-09-19","2016-09-19","10","1","2","0","INV#IV157,Pax :KADER, PassNo :k000000/Pax :AYISHA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1089","94","7","155","Visa Payable","600","0","","2016-09-19","2016-09-19","10","1","2","0","Details:INV#IV157,Pax :KADER, PassNo :k000000/Pax :AYISHA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1090","7","94","155","Visa Payable","0","600","","2016-09-19","2016-09-19","10","1","2","0","Details:INV#IV157,Pax :KADER, PassNo :k000000/Pax :AYISHA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1099","126","10","1","Hotel Receivable","0","50","Hotel Receivable : 1","2016-09-19","2016-09-19","6","2","2","0","INV#IH1,Hotel Name :Sample,City :,Contact No :,CheckIn Date:19-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1100","10","126","1","Hotel Receivable","50","0","Hotel Receivable : 1","2016-09-19","2016-09-19","6","2","2","0","INV#IH1,Hotel Name :Sample,City :,Contact No :,CheckIn Date:19-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1101","85","11","2","Hotel Payable","57","0","","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1102","11","85","2","Hotel Payable","0","57","","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1103","85","16","2","Hotel Payable","2","0","Discount Given:inv#2","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1104","16","85","2","Hotel Payable","0","2","Discount Given:inv#2","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1105","85","19","2","Hotel Payable","2","0","Tds:inv#2","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1106","19","85","2","Hotel Payable","0","2","Tds:inv#2","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1107","85","18","2","Hotel Payable","2","0","Tax:inv#2","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1108","18","85","2","Hotel Payable","0","2","Tax:inv#2","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1109","85","20","2","Hotel Payable","0","2","Tac:inv#2","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1110","20","85","2","Hotel Payable","2","0","Tac:inv#2","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IH1,Hotel Name :Alisha Hotel,City :,Contact No :,CheckIn Date:16-09-2016, No.of Rooms: 2,No.of Nights: 1,By:Manu ");
INSERT INTO transaction VALUES("1111","173","4","25","Airline Receivable","0","14800","Airline Receivable: 25","2016-09-19","2016-09-19","6","2","2","0","INV#IA25, Pax:AYAPPALLI MOIDEEN, Passport No:L3813768, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774873, PNR:4IYCPH, By:Manu");
INSERT INTO transaction VALUES("1112","4","173","25","Airline Receivable","14800","0","Airline Receivable: 25","2016-09-19","2016-09-19","6","2","2","0","INV#IA25, Pax:AYAPPALLI MOIDEEN, Passport No:L3813768, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774873, PNR:4IYCPH, By:Manu");
INSERT INTO transaction VALUES("1113","83","5","25","Airline Payable","13535","0","","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IA25, Pax:AYAPPALLI MOIDEEN, Passport No:L3813768, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774873, PNR:4IYCPH, By:Manu");
INSERT INTO transaction VALUES("1114","5","83","25","Airline Payable","0","13535","","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IA25, Pax:AYAPPALLI MOIDEEN, Passport No:L3813768, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774873, PNR:4IYCPH, By:Manu");
INSERT INTO transaction VALUES("1115","174","4","26","Airline Receivable","0","25000","Airline Receivable: 26","2016-09-19","2016-09-19","6","2","2","0","INV#IA26, Pax:PANDARATHIL JURAIGE, Passport No:G5721484, Sector:CCJ/SHJ, Date:22-09-2016/24-09-2016, Tkt:AUKPRG/AUKPRG, PNR:AUKPRG, By:Manu");
INSERT INTO transaction VALUES("1116","4","174","26","Airline Receivable","25000","0","Airline Receivable: 26","2016-09-19","2016-09-19","6","2","2","0","INV#IA26, Pax:PANDARATHIL JURAIGE, Passport No:G5721484, Sector:CCJ/SHJ, Date:22-09-2016/24-09-2016, Tkt:AUKPRG/AUKPRG, PNR:AUKPRG, By:Manu");
INSERT INTO transaction VALUES("1117","81","5","26","Airline Payable","23980","0","","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IA26, Pax:PANDARATHIL JURAIGE, Passport No:G5721484, Sector:CCJ/SHJ, Date:22-09-2016/24-09-2016, Tkt:AUKPRG/AUKPRG, PNR:AUKPRG, By:Manu");
INSERT INTO transaction VALUES("1118","5","81","26","Airline Payable","0","23980","","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IA26, Pax:PANDARATHIL JURAIGE, Passport No:G5721484, Sector:CCJ/SHJ, Date:22-09-2016/24-09-2016, Tkt:AUKPRG/AUKPRG, PNR:AUKPRG, By:Manu");
INSERT INTO transaction VALUES("1119","87","4","27","Airline Receivable","0","16500","Airline Receivable: 27","2016-09-19","2016-09-19","6","2","2","0","INV#IA27, Pax:THALAKOTTUPURAM/ABDUL LATHEEF, Passport No:, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774899, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1120","4","87","27","Airline Receivable","16500","0","Airline Receivable: 27","2016-09-19","2016-09-19","6","2","2","0","INV#IA27, Pax:THALAKOTTUPURAM/ABDUL LATHEEF, Passport No:, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774899, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1121","83","5","27","Airline Payable","15565","0","","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IA27, Pax:THALAKOTTUPURAM/ABDUL LATHEEF, Passport No:, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774899, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1122","5","83","27","Airline Payable","0","15565","","2016-09-19","2016-09-19","6","2","2","0","Details:INV#IA27, Pax:THALAKOTTUPURAM/ABDUL LATHEEF, Passport No:, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774899, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1123","167","2","15","Cash Receipts","14000","0","","2016-09-19","2016-09-19","6","2","2","0","VouNo#CR 15,INV#IA23 Received By :Manu,Details:INV#IA23, Pax:RASHEED NARUNGALI , NEENAN MARAKKAR, Passport No:F9251602, Sector:DXB/CCJ, Date:21-09-2016, Tkt:UPQB5X, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1124","2","167","15","Cash Receipts","0","14000","","2016-09-19","2016-09-19","6","2","2","0","VouNo#CR 15,INV#IA23 Received By :Manu,Details:INV#IA23, Pax:RASHEED NARUNGALI , NEENAN MARAKKAR, Passport No:F9251602, Sector:DXB/CCJ, Date:21-09-2016, Tkt:UPQB5X, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1125","102","2","8","Cash Payments","0","3300","PAID PAYMENT","2016-09-19","2016-09-19","15","1","2","0","VouNo#CP8,INV#PV9 Paid By :Akhil To : TRAVEL PARTNER PURCHASE,Details:INV#IV9,Pax :ALLY JOY, PassNo :J0885006/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("1126","2","102","8","Cash Payments","3300","0","PAID PAYMENT","2016-09-19","2016-09-19","15","1","2","0","VouNo#CP8,INV#PV9 Paid By :Akhil To : TRAVEL PARTNER PURCHASE,Details:INV#IV9,Pax :ALLY JOY, PassNo :J0885006/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("1129","145","2","16","Cash Receipts","5000","0","RECEIVED CASH FRM RIYAS","2016-09-19","2016-09-19","15","1","2","0","VouNo#CR 16,INV#IV86 Received By :Akhil,Details:INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1130","2","145","16","Cash Receipts","0","5000","RECEIVED CASH FRM RIYAS","2016-09-19","2016-09-19","15","1","2","0","VouNo#CR 16,INV#IV86 Received By :Akhil,Details:INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1131","95","6","158","Visa Receivable","0","600","Visa Receivable : 158","2016-09-19","2016-09-19","10","1","2","0","INV#IV158,Pax :PHILIP, PassNo :k000000/Pax :MSARIYAMMA, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1132","6","95","158","Visa Receivable","600","0","Visa Receivable : 158","2016-09-19","2016-09-19","10","1","2","0","INV#IV158,Pax :PHILIP, PassNo :k000000/Pax :MSARIYAMMA, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1135","94","7","156","Visa Payable","600","0","","2016-09-19","2016-09-19","10","1","2","0","Details:INV#IV158,Pax :PHILIP, PassNo :k000000/Pax :MSARIYAMMA, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1136","7","94","156","Visa Payable","0","600","","2016-09-19","2016-09-19","10","1","2","0","Details:INV#IV158,Pax :PHILIP, PassNo :k000000/Pax :MSARIYAMMA, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1137","101","6","159","Visa Receivable","0","29600","Visa Receivable : 159","2016-09-19","2016-09-19","9","1","2","0","INV#IV159,Pax :ISTHIAQ, PassNo :L2498982/Pax :SMITHA, PassNo :K4201402/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1138","6","101","159","Visa Receivable","29600","0","Visa Receivable : 159","2016-09-19","2016-09-19","9","1","2","0","INV#IV159,Pax :ISTHIAQ, PassNo :L2498982/Pax :SMITHA, PassNo :K4201402/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1139","132","7","157","Visa Payable","29200","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV159,Pax :ISTHIAQ, PassNo :L2498982/Pax :SMITHA, PassNo :K4201402/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1140","7","132","157","Visa Payable","0","29200","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV159,Pax :ISTHIAQ, PassNo :L2498982/Pax :SMITHA, PassNo :K4201402/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1143","95","6","160","Visa Receivable","0","14600","Visa Receivable : 160","2016-09-19","2016-09-19","9","1","2","0","INV#IV160,Pax :MUHAMMED MUSTHAFA, PassNo :L1466281/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1144","6","95","160","Visa Receivable","14600","0","Visa Receivable : 160","2016-09-19","2016-09-19","9","1","2","0","INV#IV160,Pax :MUHAMMED MUSTHAFA, PassNo :L1466281/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1145","132","7","158","Visa Payable","14600","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV160,Pax :MUHAMMED MUSTHAFA, PassNo :L1466281/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1146","7","132","158","Visa Payable","0","14600","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV160,Pax :MUHAMMED MUSTHAFA, PassNo :L1466281/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1147","165","6","161","Visa Receivable","0","14600","Visa Receivable : 161","2016-09-19","2016-09-19","9","1","2","0","INV#IV161,Pax :JOBIN, PassNo :K6230924/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1148","6","165","161","Visa Receivable","14600","0","Visa Receivable : 161","2016-09-19","2016-09-19","9","1","2","0","INV#IV161,Pax :JOBIN, PassNo :K6230924/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1149","94","7","159","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV161,Pax :JOBIN, PassNo :K6230924/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1150","7","94","159","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV161,Pax :JOBIN, PassNo :K6230924/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1151","130","6","162","Visa Receivable","0","14700","Visa Receivable : 162","2016-09-19","2016-09-19","9","1","2","0","INV#IV162,Pax :AKHIL, PassNo :K5367262/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1152","6","130","162","Visa Receivable","14700","0","Visa Receivable : 162","2016-09-19","2016-09-19","9","1","2","0","INV#IV162,Pax :AKHIL, PassNo :K5367262/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1153","94","7","160","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV162,Pax :AKHIL, PassNo :K5367262/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1154","7","94","160","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV162,Pax :AKHIL, PassNo :K5367262/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1155","95","6","163","Visa Receivable","0","14600","Visa Receivable : 163","2016-09-19","2016-09-19","9","1","2","0","INV#IV163,Pax :SJAJAHAN, PassNo :J5958608/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1156","6","95","163","Visa Receivable","14600","0","Visa Receivable : 163","2016-09-19","2016-09-19","9","1","2","0","INV#IV163,Pax :SJAJAHAN, PassNo :J5958608/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1157","94","7","161","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV163,Pax :SJAJAHAN, PassNo :J5958608/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1158","7","94","161","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV163,Pax :SJAJAHAN, PassNo :J5958608/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1159","99","6","164","Visa Receivable","0","14800","Visa Receivable : 164","2016-09-19","2016-09-19","9","1","2","0","INV#IV164,Pax :SOOSAMMA CHANDY, PassNo :N8816782/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1160","6","99","164","Visa Receivable","14800","0","Visa Receivable : 164","2016-09-19","2016-09-19","9","1","2","0","INV#IV164,Pax :SOOSAMMA CHANDY, PassNo :N8816782/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1161","132","7","162","Visa Payable","14600","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV164,Pax :SOOSAMMA CHANDY, PassNo :N8816782/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1162","7","132","162","Visa Payable","0","14600","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV164,Pax :SOOSAMMA CHANDY, PassNo :N8816782/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1163","95","6","165","Visa Receivable","0","14600","Visa Receivable : 165","2016-09-19","2016-09-19","9","1","2","0","INV#IV165,Pax :FIROS, PassNo :L5907266/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1164","6","95","165","Visa Receivable","14600","0","Visa Receivable : 165","2016-09-19","2016-09-19","9","1","2","0","INV#IV165,Pax :FIROS, PassNo :L5907266/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1165","94","7","163","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV165,Pax :FIROS, PassNo :L5907266/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1166","7","94","163","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV165,Pax :FIROS, PassNo :L5907266/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1169","99","6","166","Visa Receivable","0","29200","Visa Receivable : 166","2016-09-19","2016-09-19","9","1","2","0","INV#IV166,Pax :CERIN XAVIER, PassNo :K1079157/Pax :RONI MOOKAYIL, PassNo :M0651353/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1170","6","99","166","Visa Receivable","29200","0","Visa Receivable : 166","2016-09-19","2016-09-19","9","1","2","0","INV#IV166,Pax :CERIN XAVIER, PassNo :K1079157/Pax :RONI MOOKAYIL, PassNo :M0651353/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1171","94","7","164","Visa Payable","28200","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV166,Pax :CERIN XAVIER, PassNo :K1079157/Pax :RONI MOOKAYIL, PassNo :M0651353/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1172","7","94","164","Visa Payable","0","28200","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV166,Pax :CERIN XAVIER, PassNo :K1079157/Pax :RONI MOOKAYIL, PassNo :M0651353/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1173","95","6","167","Visa Receivable","0","14600","Visa Receivable : 167","2016-09-19","2016-09-19","9","1","2","0","INV#IV167,Pax :ANISH KUMAR, PassNo :H4653637/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1174","6","95","167","Visa Receivable","14600","0","Visa Receivable : 167","2016-09-19","2016-09-19","9","1","2","0","INV#IV167,Pax :ANISH KUMAR, PassNo :H4653637/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1175","94","7","165","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV167,Pax :ANISH KUMAR, PassNo :H4653637/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1176","7","94","165","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV167,Pax :ANISH KUMAR, PassNo :H4653637/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1177","99","6","168","Visa Receivable","0","14800","Visa Receivable : 168","2016-09-19","2016-09-19","9","1","2","0","INV#IV168,Pax :TOMY MOL, PassNo :P4063035/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1178","6","99","168","Visa Receivable","14800","0","Visa Receivable : 168","2016-09-19","2016-09-19","9","1","2","0","INV#IV168,Pax :TOMY MOL, PassNo :P4063035/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1181","132","7","166","Visa Payable","14600","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV168,Pax :TOMY MOL, PassNo :P4063035/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1182","7","132","166","Visa Payable","0","14600","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV168,Pax :TOMY MOL, PassNo :P4063035/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1183","169","6","169","Visa Receivable","0","40800","Visa Receivable : 169","2016-09-19","2016-09-19","9","1","2","0","INV#IV169,Pax :MOHAMMED REHAN, PassNo :N0482654/Pax :AFSAL, PassNo :Z2953998/Pax :MUHAMMED NOUFAL, PassNo :M2830526/Pax :PARATTU SAIDU, PassNo :H1113535/Pax :FATHIMA, PassNo :G6290489/Pax :SAYYAF, PassNo :K3979073/Pax :TAJUDHENN, PassNo :J2040267/Pax :ABDU RAHIMAN, PassNo :N0161120/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1184","6","169","169","Visa Receivable","40800","0","Visa Receivable : 169","2016-09-19","2016-09-19","9","1","2","0","INV#IV169,Pax :MOHAMMED REHAN, PassNo :N0482654/Pax :AFSAL, PassNo :Z2953998/Pax :MUHAMMED NOUFAL, PassNo :M2830526/Pax :PARATTU SAIDU, PassNo :H1113535/Pax :FATHIMA, PassNo :G6290489/Pax :SAYYAF, PassNo :K3979073/Pax :TAJUDHENN, PassNo :J2040267/Pax :ABDU RAHIMAN, PassNo :N0161120/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1187","94","7","167","Visa Payable","40600","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV169,Pax :MOHAMMED REHAN, PassNo :N0482654/Pax :AFSAL, PassNo :Z2953998/Pax :MUHAMMED NOUFAL, PassNo :M2830526/Pax :PARATTU SAIDU, PassNo :H1113535/Pax :FATHIMA, PassNo :G6290489/Pax :SAYYAF, PassNo :K3979073/Pax :TAJUDHENN, PassNo :J2040267/Pax :ABDU RAHIMAN, PassNo :N0161120/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1188","7","94","167","Visa Payable","0","40600","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV169,Pax :MOHAMMED REHAN, PassNo :N0482654/Pax :AFSAL, PassNo :Z2953998/Pax :MUHAMMED NOUFAL, PassNo :M2830526/Pax :PARATTU SAIDU, PassNo :H1113535/Pax :FATHIMA, PassNo :G6290489/Pax :SAYYAF, PassNo :K3979073/Pax :TAJUDHENN, PassNo :J2040267/Pax :ABDU RAHIMAN, PassNo :N0161120/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1189","169","6","170","Visa Receivable","0","45900","Visa Receivable : 170","2016-09-19","2016-09-19","9","1","2","0","INV#IV170,Pax :MOHAMMED JIHAD, PassNo :N1597279/Pax :FAHIS, PassNo :P2332064/Pax :FARSANA, PassNo :H5552428/Pax :HANA FATHIMA, PassNo :L3650539/Pax :ZAMEEL, PassNo :L8267861/Pax :KHADEEJA, PassNo :J2671347/Pax :JUSTIN, PassNo :N2088987/Pax :RASHEED, PassNo :M3712539/Pax :SUBAIDA, PassNo :L2355344/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1190","6","169","170","Visa Receivable","45900","0","Visa Receivable : 170","2016-09-19","2016-09-19","9","1","2","0","INV#IV170,Pax :MOHAMMED JIHAD, PassNo :N1597279/Pax :FAHIS, PassNo :P2332064/Pax :FARSANA, PassNo :H5552428/Pax :HANA FATHIMA, PassNo :L3650539/Pax :ZAMEEL, PassNo :L8267861/Pax :KHADEEJA, PassNo :J2671347/Pax :JUSTIN, PassNo :N2088987/Pax :RASHEED, PassNo :M3712539/Pax :SUBAIDA, PassNo :L2355344/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1191","94","7","168","Visa Payable","45675","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV170,Pax :MOHAMMED JIHAD, PassNo :N1597279/Pax :FAHIS, PassNo :P2332064/Pax :FARSANA, PassNo :H5552428/Pax :HANA FATHIMA, PassNo :L3650539/Pax :ZAMEEL, PassNo :L8267861/Pax :KHADEEJA, PassNo :J2671347/Pax :JUSTIN, PassNo :N2088987/Pax :RASHEED, PassNo :M3712539/Pax :SUBAIDA, PassNo :L2355344/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1192","7","94","168","Visa Payable","0","45675","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV170,Pax :MOHAMMED JIHAD, PassNo :N1597279/Pax :FAHIS, PassNo :P2332064/Pax :FARSANA, PassNo :H5552428/Pax :HANA FATHIMA, PassNo :L3650539/Pax :ZAMEEL, PassNo :L8267861/Pax :KHADEEJA, PassNo :J2671347/Pax :JUSTIN, PassNo :N2088987/Pax :RASHEED, PassNo :M3712539/Pax :SUBAIDA, PassNo :L2355344/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1193","178","4","2","Airline Receivable","0","14600","Airline Receivable: 2","2016-09-19","2016-09-19","35","3","2","0","INV#IA2, Pax:VAKAYIL MUHAMMED/ALI AKBER , Passport No:, Sector:COK/MCT, Date:29-09-2016, Tkt:589-9165774925, PNR:0, By:Shamshad Ali");
INSERT INTO transaction VALUES("1194","4","178","2","Airline Receivable","14600","0","Airline Receivable: 2","2016-09-19","2016-09-19","35","3","2","0","INV#IA2, Pax:VAKAYIL MUHAMMED/ALI AKBER , Passport No:, Sector:COK/MCT, Date:29-09-2016, Tkt:589-9165774925, PNR:0, By:Shamshad Ali");
INSERT INTO transaction VALUES("1195","160","5","2","Airline Payable","14340","0","","2016-09-19","2016-09-19","35","3","2","0","Details:INV#IA2, Pax:VAKAYIL MUHAMMED/ALI AKBER , Passport No:, Sector:COK/MCT, Date:29-09-2016, Tkt:589-9165774925, PNR:0, By:Shamshad Ali");
INSERT INTO transaction VALUES("1196","5","160","2","Airline Payable","0","14340","","2016-09-19","2016-09-19","35","3","2","0","Details:INV#IA2, Pax:VAKAYIL MUHAMMED/ALI AKBER , Passport No:, Sector:COK/MCT, Date:29-09-2016, Tkt:589-9165774925, PNR:0, By:Shamshad Ali");
INSERT INTO transaction VALUES("1197","178","2","2","Cash Receipts","14600","0","","2016-09-19","2016-09-19","35","3","2","0","VouNo#CR 2,INV#IA2 Received By :Shamshad Ali,Details:INV#IA2, Pax:THALE KUNNATH/JASEEM, Passport No:, Sector:DOH/CCJ, Date:07-12-2016, Tkt:607-9164761434, PNR:3I53TT, By:Manu");
INSERT INTO transaction VALUES("1198","2","178","2","Cash Receipts","0","14600","","2016-09-19","2016-09-19","35","3","2","0","VouNo#CR 2,INV#IA2 Received By :Shamshad Ali,Details:INV#IA2, Pax:THALE KUNNATH/JASEEM, Passport No:, Sector:DOH/CCJ, Date:07-12-2016, Tkt:607-9164761434, PNR:3I53TT, By:Manu");
INSERT INTO transaction VALUES("1199","177","8","1","Package Receivable","0","125000","Invoice Package: 1","2016-09-19","2016-09-19","36","1","2","1","INV#IP1,Pac:Umra ,Pax:MUHAMMED KOYA CHEELANGATTU M 63 PassNo-N9062177,AYISHABI PALLITHAZHATU F 62 PassNo-N9063084 ,By:Siraj Saquafi");
INSERT INTO transaction VALUES("1200","8","177","1","Package Receivable","125000","0","Invoice Package: 1","2016-09-19","2016-09-19","36","1","2","1","INV#IP1,Pac:Umra ,Pax:MUHAMMED KOYA CHEELANGATTU M 63 PassNo-N9062177,AYISHABI PALLITHAZHATU F 62 PassNo-N9063084 ,By:Siraj Saquafi");
INSERT INTO transaction VALUES("1201","177","2","17","Cash Receipts","25000","0","Amount Not Conform","2016-09-19","2016-09-19","36","1","2","0","VouNo#CR 17,INV#IP1 Received By :Siraj Saquafi,Details:INV#IP1,Pac:Umra ,Pax:MUHAMMED KOYA CHEELANGATTU M 63 PassNo-N9062177,AYISHABI PALLITHAZHATU F 62 PassNo-N9063084 ,By:Siraj Saquafi");
INSERT INTO transaction VALUES("1202","2","177","17","Cash Receipts","0","25000","Amount Not Conform","2016-09-19","2016-09-19","36","1","2","0","VouNo#CR 17,INV#IP1 Received By :Siraj Saquafi,Details:INV#IP1,Pac:Umra ,Pax:MUHAMMED KOYA CHEELANGATTU M 63 PassNo-N9062177,AYISHABI PALLITHAZHATU F 62 PassNo-N9063084 ,By:Siraj Saquafi");
INSERT INTO transaction VALUES("1203","169","6","171","Visa Receivable","0","66300","Visa Receivable : 171","2016-09-19","2016-09-19","9","1","2","0","INV#IV171,Pax :JANSI BANU, PassNo :K3509049/Pax :MOHAMMED, PassNo :K4048589/Pax :AHAMMED AFREED, PassNo :M9386753/Pax :ANEESA, PassNo :M9867590/Pax :HANA FATHIMA, PassNo :N2757430/Pax :MUHAMMED HAMNA, PassNo :N2757718/Pax :SURENDRAN, PassNo :L7060373/Pax :SAIDALI, PassNo :L1222266/Pax :AMUL KOYA, PassNo :H9381424/Pax :SARA, PassNo :H2809593/Pax :SUMAYA MOL, PassNo :G6157755/Pax :RIFA, PassNo :N8086443/Pax :ABDUL RAOOF, PassNo :Z2105213/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1204","6","169","171","Visa Receivable","66300","0","Visa Receivable : 171","2016-09-19","2016-09-19","9","1","2","0","INV#IV171,Pax :JANSI BANU, PassNo :K3509049/Pax :MOHAMMED, PassNo :K4048589/Pax :AHAMMED AFREED, PassNo :M9386753/Pax :ANEESA, PassNo :M9867590/Pax :HANA FATHIMA, PassNo :N2757430/Pax :MUHAMMED HAMNA, PassNo :N2757718/Pax :SURENDRAN, PassNo :L7060373/Pax :SAIDALI, PassNo :L1222266/Pax :AMUL KOYA, PassNo :H9381424/Pax :SARA, PassNo :H2809593/Pax :SUMAYA MOL, PassNo :G6157755/Pax :RIFA, PassNo :N8086443/Pax :ABDUL RAOOF, PassNo :Z2105213/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1205","94","7","169","Visa Payable","65975","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV171,Pax :JANSI BANU, PassNo :K3509049/Pax :MOHAMMED, PassNo :K4048589/Pax :AHAMMED AFREED, PassNo :M9386753/Pax :ANEESA, PassNo :M9867590/Pax :HANA FATHIMA, PassNo :N2757430/Pax :MUHAMMED HAMNA, PassNo :N2757718/Pax :SURENDRAN, PassNo :L7060373/Pax :SAIDALI, PassNo :L1222266/Pax :AMUL KOYA, PassNo :H9381424/Pax :SARA, PassNo :H2809593/Pax :SUMAYA MOL, PassNo :G6157755/Pax :RIFA, PassNo :N8086443/Pax :ABDUL RAOOF, PassNo :Z2105213/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1206","7","94","169","Visa Payable","0","65975","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV171,Pax :JANSI BANU, PassNo :K3509049/Pax :MOHAMMED, PassNo :K4048589/Pax :AHAMMED AFREED, PassNo :M9386753/Pax :ANEESA, PassNo :M9867590/Pax :HANA FATHIMA, PassNo :N2757430/Pax :MUHAMMED HAMNA, PassNo :N2757718/Pax :SURENDRAN, PassNo :L7060373/Pax :SAIDALI, PassNo :L1222266/Pax :AMUL KOYA, PassNo :H9381424/Pax :SARA, PassNo :H2809593/Pax :SUMAYA MOL, PassNo :G6157755/Pax :RIFA, PassNo :N8086443/Pax :ABDUL RAOOF, PassNo :Z2105213/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1207","169","6","172","Visa Receivable","0","20400","Visa Receivable : 172","2016-09-19","2016-09-19","9","1","2","0","INV#IV172,Pax :VIJAYAMMA, PassNo :P0248026/Pax :MUHAMMED SHAFI, PassNo :H1477969/Pax :KAMARU, PassNo :Z3403231/Pax :ABDUL JABBAR, PassNo :M0236862/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1208","6","169","172","Visa Receivable","20400","0","Visa Receivable : 172","2016-09-19","2016-09-19","9","1","2","0","INV#IV172,Pax :VIJAYAMMA, PassNo :P0248026/Pax :MUHAMMED SHAFI, PassNo :H1477969/Pax :KAMARU, PassNo :Z3403231/Pax :ABDUL JABBAR, PassNo :M0236862/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1209","94","7","170","Visa Payable","20300","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV172,Pax :VIJAYAMMA, PassNo :P0248026/Pax :MUHAMMED SHAFI, PassNo :H1477969/Pax :KAMARU, PassNo :Z3403231/Pax :ABDUL JABBAR, PassNo :M0236862/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1210","7","94","170","Visa Payable","0","20300","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV172,Pax :VIJAYAMMA, PassNo :P0248026/Pax :MUHAMMED SHAFI, PassNo :H1477969/Pax :KAMARU, PassNo :Z3403231/Pax :ABDUL JABBAR, PassNo :M0236862/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1211","99","6","173","Visa Receivable","0","14600","Visa Receivable : 173","2016-09-19","2016-09-19","9","1","2","0","INV#IV173,Pax :SAJU RAJ, PassNo :K6463555/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1212","6","99","173","Visa Receivable","14600","0","Visa Receivable : 173","2016-09-19","2016-09-19","9","1","2","0","INV#IV173,Pax :SAJU RAJ, PassNo :K6463555/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1213","94","7","171","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV173,Pax :SAJU RAJ, PassNo :K6463555/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1214","7","94","171","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV173,Pax :SAJU RAJ, PassNo :K6463555/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1215","95","6","174","Visa Receivable","0","14700","Visa Receivable : 174","2016-09-19","2016-09-19","9","1","2","0","INV#IV174,Pax :FATHIMA NASEER, PassNo :N2809487/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1216","6","95","174","Visa Receivable","14700","0","Visa Receivable : 174","2016-09-19","2016-09-19","9","1","2","0","INV#IV174,Pax :FATHIMA NASEER, PassNo :N2809487/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1217","132","7","172","Visa Payable","14600","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV174,Pax :FATHIMA NASEER, PassNo :N2809487/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1218","7","132","172","Visa Payable","0","14600","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV174,Pax :FATHIMA NASEER, PassNo :N2809487/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1219","95","6","175","Visa Receivable","0","14600","Visa Receivable : 175","2016-09-19","2016-09-19","9","1","2","0","INV#IV175,Pax :JOBIN JOSE MARY, PassNo :N4351083/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("1220","6","95","175","Visa Receivable","14600","0","Visa Receivable : 175","2016-09-19","2016-09-19","9","1","2","0","INV#IV175,Pax :JOBIN JOSE MARY, PassNo :N4351083/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("1221","94","7","173","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV175,Pax :JOBIN JOSE MARY, PassNo :N4351083/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("1222","7","94","173","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV175,Pax :JOBIN JOSE MARY, PassNo :N4351083/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("1223","130","6","176","Visa Receivable","0","14700","Visa Receivable : 176","2016-09-19","2016-09-19","9","1","2","0","INV#IV176,Pax :PARVATHI, PassNo :K9986602/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1224","6","130","176","Visa Receivable","14700","0","Visa Receivable : 176","2016-09-19","2016-09-19","9","1","2","0","INV#IV176,Pax :PARVATHI, PassNo :K9986602/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1225","132","7","174","Visa Payable","14600","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV176,Pax :PARVATHI, PassNo :K9986602/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1226","7","132","174","Visa Payable","0","14600","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV176,Pax :PARVATHI, PassNo :K9986602/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1227","165","6","177","Visa Receivable","0","14600","Visa Receivable : 177","2016-09-19","2016-09-19","9","1","2","0","INV#IV177,Pax :LITHAS, PassNo :L8303251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1228","6","165","177","Visa Receivable","14600","0","Visa Receivable : 177","2016-09-19","2016-09-19","9","1","2","0","INV#IV177,Pax :LITHAS, PassNo :L8303251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1229","94","7","175","Visa Payable","14100","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV177,Pax :LITHAS, PassNo :L8303251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1230","7","94","175","Visa Payable","0","14100","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV177,Pax :LITHAS, PassNo :L8303251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1231","96","6","178","Visa Receivable","0","10400","Visa Receivable : 178","2016-09-19","2016-09-19","9","1","2","0","INV#IV178,Pax :MUNAVVAR, PassNo :J6244054/Pax :DANIS, PassNo :K7417702/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1232","6","96","178","Visa Receivable","10400","0","Visa Receivable : 178","2016-09-19","2016-09-19","9","1","2","0","INV#IV178,Pax :MUNAVVAR, PassNo :J6244054/Pax :DANIS, PassNo :K7417702/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1233","94","7","176","Visa Payable","10150","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV178,Pax :MUNAVVAR, PassNo :J6244054/Pax :DANIS, PassNo :K7417702/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1234","7","94","176","Visa Payable","0","10150","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV178,Pax :MUNAVVAR, PassNo :J6244054/Pax :DANIS, PassNo :K7417702/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1235","130","6","179","Visa Receivable","0","5100","Visa Receivable : 179","2016-09-19","2016-09-19","9","1","2","0","INV#IV179,Pax :MOHAMMED ISHAK, PassNo :N3264522/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1236","6","130","179","Visa Receivable","5100","0","Visa Receivable : 179","2016-09-19","2016-09-19","9","1","2","0","INV#IV179,Pax :MOHAMMED ISHAK, PassNo :N3264522/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1237","130","6","180","Visa Receivable","0","10200","Visa Receivable : 180","2016-09-19","2016-09-19","9","1","2","0","INV#IV180,Pax :MOHAMMED ISHAK, PassNo :N3264522/Pax :THAHIRA, PassNo :J5239951/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1238","6","130","180","Visa Receivable","10200","0","Visa Receivable : 180","2016-09-19","2016-09-19","9","1","2","0","INV#IV180,Pax :MOHAMMED ISHAK, PassNo :N3264522/Pax :THAHIRA, PassNo :J5239951/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1239","94","7","177","Visa Payable","10150","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV180,Pax :MOHAMMED ISHAK, PassNo :N3264522/Pax :THAHIRA, PassNo :J5239951/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1240","7","94","177","Visa Payable","0","10150","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV180,Pax :MOHAMMED ISHAK, PassNo :N3264522/Pax :THAHIRA, PassNo :J5239951/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1241","169","6","181","Visa Receivable","0","10200","Visa Receivable : 181","2016-09-19","2016-09-19","9","1","2","0","INV#IV181,Pax :ASHA JHON, PassNo :P2812641/Pax :JUSTIN, PassNo :G7715071/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1242","6","169","181","Visa Receivable","10200","0","Visa Receivable : 181","2016-09-19","2016-09-19","9","1","2","0","INV#IV181,Pax :ASHA JHON, PassNo :P2812641/Pax :JUSTIN, PassNo :G7715071/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1243","94","7","178","Visa Payable","10150","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV181,Pax :ASHA JHON, PassNo :P2812641/Pax :JUSTIN, PassNo :G7715071/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1244","7","94","178","Visa Payable","0","10150","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV181,Pax :ASHA JHON, PassNo :P2812641/Pax :JUSTIN, PassNo :G7715071/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1245","101","6","182","Visa Receivable","0","14800","Visa Receivable : 182","2016-09-19","2016-09-19","9","1","2","0","INV#IV182,Pax :SOMA BANERGI, PassNo :H4493704/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1246","6","101","182","Visa Receivable","14800","0","Visa Receivable : 182","2016-09-19","2016-09-19","9","1","2","0","INV#IV182,Pax :SOMA BANERGI, PassNo :H4493704/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1247","132","7","179","Visa Payable","14600","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV182,Pax :SOMA BANERGI, PassNo :H4493704/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1248","7","132","179","Visa Payable","0","14600","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV182,Pax :SOMA BANERGI, PassNo :H4493704/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1249","142","6","183","Visa Receivable","0","15100","Visa Receivable : 183","2016-09-19","2016-09-19","9","1","2","0","INV#IV183,Pax :FATHIMA SHIFANATH, PassNo :P2512185/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1250","6","142","183","Visa Receivable","15100","0","Visa Receivable : 183","2016-09-19","2016-09-19","9","1","2","0","INV#IV183,Pax :FATHIMA SHIFANATH, PassNo :P2512185/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1251","132","7","180","Visa Payable","14600","0","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV183,Pax :FATHIMA SHIFANATH, PassNo :P2512185/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1252","7","132","180","Visa Payable","0","14600","","2016-09-19","2016-09-19","9","1","2","0","Details:INV#IV183,Pax :FATHIMA SHIFANATH, PassNo :P2512185/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1257","169","2","18","Cash Receipts","50000","0","RECEIVED CASH FRM NOORUL HAYATH","2016-09-19","2016-09-19","15","1","2","0","VouNo#CR 18,INV#IV129 Received By :Akhil,Details:INV#IV129,Pax :NUSRATH, PassNo :M2789304/Pax :MINHA FATHIMA, PassNo :M4145622/Pax :MALLIK KUMARSWAMY, PassNo :J8582907/Pax :MARIYA, PassNo :L1316251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1258","2","169","18","Cash Receipts","0","50000","RECEIVED CASH FRM NOORUL HAYATH","2016-09-19","2016-09-19","15","1","2","0","VouNo#CR 18,INV#IV129 Received By :Akhil,Details:INV#IV129,Pax :NUSRATH, PassNo :M2789304/Pax :MINHA FATHIMA, PassNo :M4145622/Pax :MALLIK KUMARSWAMY, PassNo :J8582907/Pax :MARIYA, PassNo :L1316251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1259","181","2","9","Cash Payments","0","200","BOM COURIER","2016-09-19","2016-09-19","33","1","2","0","VouNo#CP9,INV# Paid By :Faizal To : COURIER EXP,Details:");
INSERT INTO transaction VALUES("1260","2","181","9","Cash Payments","200","0","BOM COURIER","2016-09-19","2016-09-19","33","1","2","0","VouNo#CP9,INV# Paid By :Faizal To : COURIER EXP,Details:");
INSERT INTO transaction VALUES("1261","181","2","10","Cash Payments","0","1900","FLIGHT COURIER TO VISA TO SALIL BOM","2016-09-19","2016-09-16","33","1","2","0","VouNo#CP9,INV# Paid By :Faizal To : COURIER EXP,Details:");
INSERT INTO transaction VALUES("1262","2","181","10","Cash Payments","1900","0","FLIGHT COURIER TO VISA TO SALIL BOM","2016-09-19","2016-09-16","33","1","2","0","VouNo#CP9,INV# Paid By :Faizal To : COURIER EXP,Details:");
INSERT INTO transaction VALUES("1263","182","2","11","Cash Payments","0","20000","NEW OFFICE EXP","2016-09-19","2016-09-19","33","1","2","0","VouNo#CP9,INV# Paid By :Faizal To : DELHI NEW OFFICE EXP,Details:");
INSERT INTO transaction VALUES("1264","2","182","11","Cash Payments","20000","0","NEW OFFICE EXP","2016-09-19","2016-09-19","33","1","2","0","VouNo#CP9,INV# Paid By :Faizal To : DELHI NEW OFFICE EXP,Details:");
INSERT INTO transaction VALUES("1265","133","2","12","Cash Payments","0","99000","CASH DEPPOSIT","2016-09-19","2016-09-19","33","1","2","0","VouNo#CP9,INV# Paid By :Faizal To : FEDERAL BANK,Details:");
INSERT INTO transaction VALUES("1266","2","133","12","Cash Payments","99000","0","CASH DEPPOSIT","2016-09-19","2016-09-19","33","1","2","0","VouNo#CP9,INV# Paid By :Faizal To : FEDERAL BANK,Details:");
INSERT INTO transaction VALUES("1271","146","134","16","Bank Receipts","300000","0","PAID CHEQUE TO MTM VISA","2016-09-20","2016-09-07","15","1","2","0","VouNo#BR16,INV# Received By :Akhil,Details:");
INSERT INTO transaction VALUES("1272","134","146","16","Bank Receipts","0","300000","PAID CHEQUE TO MTM VISA","2016-09-20","2016-09-07","15","1","2","0","VouNo#BR16,INV# Received By :Akhil,Details:");
INSERT INTO transaction VALUES("1275","173","2","16","Cash Receipts","14800","0","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 16,INV#IA25 Received By :Manu,Details:INV#IA25, Pax:AYAPPALLI MOIDEEN, Passport No:L3813768, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774873, PNR:4IYCPH, By:Manu");
INSERT INTO transaction VALUES("1276","2","173","16","Cash Receipts","0","14800","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 16,INV#IA25 Received By :Manu,Details:INV#IA25, Pax:AYAPPALLI MOIDEEN, Passport No:L3813768, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774873, PNR:4IYCPH, By:Manu");
INSERT INTO transaction VALUES("1277","128","2","17","Cash Receipts","25000","0","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 17,INV#IA21 Received By :Manu,Details:INV#IA21, Pax:POOKAYIL/SIDHEEK, Passport No:J2072600, Sector:CCJ/SHJ, Date:16-09-2016, Tkt:BSZ6TZ, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1278","2","128","17","Cash Receipts","0","25000","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 17,INV#IA21 Received By :Manu,Details:INV#IA21, Pax:POOKAYIL/SIDHEEK, Passport No:J2072600, Sector:CCJ/SHJ, Date:16-09-2016, Tkt:BSZ6TZ, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1279","183","4","28","Airline Receivable","0","15800","Airline Receivable: 28","2016-09-20","2016-09-20","6","2","2","0","INV#IA28, Pax:KATTIL/HUSSAIN, Passport No:M5493217, Sector:CCJ/DOH, Date:05-09-2016, Tkt:589-9165774938, PNR:4JHTIV, By:Manu");
INSERT INTO transaction VALUES("1280","4","183","28","Airline Receivable","15800","0","Airline Receivable: 28","2016-09-20","2016-09-20","6","2","2","0","INV#IA28, Pax:KATTIL/HUSSAIN, Passport No:M5493217, Sector:CCJ/DOH, Date:05-09-2016, Tkt:589-9165774938, PNR:4JHTIV, By:Manu");
INSERT INTO transaction VALUES("1281","83","5","28","Airline Payable","15475","0","","2016-09-20","2016-09-20","6","2","2","0","Details:INV#IA28, Pax:KATTIL/HUSSAIN, Passport No:M5493217, Sector:CCJ/DOH, Date:05-09-2016, Tkt:589-9165774938, PNR:4JHTIV, By:Manu");
INSERT INTO transaction VALUES("1282","5","83","28","Airline Payable","0","15475","","2016-09-20","2016-09-20","6","2","2","0","Details:INV#IA28, Pax:KATTIL/HUSSAIN, Passport No:M5493217, Sector:CCJ/DOH, Date:05-09-2016, Tkt:589-9165774938, PNR:4JHTIV, By:Manu");
INSERT INTO transaction VALUES("1287","151","2","19","Cash Receipts","5800","0","paid cash","2016-09-20","2016-09-20","15","1","2","0","VouNo#CR 19,INV#IV185 Received By :Akhil,Details:INV#IV185,Pax :SHAMEEM, PassNo :H0047499/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1288","2","151","19","Cash Receipts","0","5800","paid cash","2016-09-20","2016-09-20","15","1","2","0","VouNo#CR 19,INV#IV185 Received By :Akhil,Details:INV#IV185,Pax :SHAMEEM, PassNo :H0047499/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1289","184","4","29","Airline Receivable","0","24000","Airline Receivable: 29","2016-09-20","2016-09-20","6","2","2","0","INV#IA29, Pax:MOHAMMED CHOORAPPULAKKAL, Passport No:, Sector:CCJ/KWI, Date:21-09-2016, Tkt:3XAZ47, PNR:3XAZ47, By:Manu");
INSERT INTO transaction VALUES("1290","4","184","29","Airline Receivable","24000","0","Airline Receivable: 29","2016-09-20","2016-09-20","6","2","2","0","INV#IA29, Pax:MOHAMMED CHOORAPPULAKKAL, Passport No:, Sector:CCJ/KWI, Date:21-09-2016, Tkt:3XAZ47, PNR:3XAZ47, By:Manu");
INSERT INTO transaction VALUES("1291","81","5","29","Airline Payable","23810","0","","2016-09-20","2016-09-20","6","2","2","0","Details:INV#IA29, Pax:MOHAMMED CHOORAPPULAKKAL, Passport No:, Sector:CCJ/KWI, Date:21-09-2016, Tkt:3XAZ47, PNR:3XAZ47, By:Manu");
INSERT INTO transaction VALUES("1292","5","81","29","Airline Payable","0","23810","","2016-09-20","2016-09-20","6","2","2","0","Details:INV#IA29, Pax:MOHAMMED CHOORAPPULAKKAL, Passport No:, Sector:CCJ/KWI, Date:21-09-2016, Tkt:3XAZ47, PNR:3XAZ47, By:Manu");
INSERT INTO transaction VALUES("1293","87","2","18","Cash Receipts","10000","0","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 18,INV#IA16 Received By :Manu,Details:INV#IA16, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977454/157-9164977454, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1294","2","87","18","Cash Receipts","0","10000","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 18,INV#IA16 Received By :Manu,Details:INV#IA16, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977454/157-9164977454, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1295","87","2","19","Cash Receipts","15000","0","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 19,INV#IA27 Received By :Manu,Details:INV#IA27, Pax:THALAKOTTUPURAM/ABDUL LATHEEF, Passport No:, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774899, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1296","2","87","19","Cash Receipts","0","15000","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 19,INV#IA27 Received By :Manu,Details:INV#IA27, Pax:THALAKOTTUPURAM/ABDUL LATHEEF, Passport No:, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774899, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1297","87","2","20","Cash Receipts","300","0","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 20,INV#IA27 Received By :Manu,Details:INV#IA27, Pax:THALAKOTTUPURAM/ABDUL LATHEEF, Passport No:, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774899, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1298","2","87","20","Cash Receipts","0","300","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 20,INV#IA27 Received By :Manu,Details:INV#IA27, Pax:THALAKOTTUPURAM/ABDUL LATHEEF, Passport No:, Sector:CCJ/DXB, Date:21-09-2016, Tkt:098-9165774899, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1299","99","185","15","Bank Receipts","161200","0","DEPOSITED IN FEDERAL  BANK","2016-09-20","2016-09-19","15","1","2","0","VouNo#BR15,INV#IV5,IV15,IV43,IV46,IV48,IV55,IV114,IV123,IV147 Received By :Akhil,Details:INV#IV147,Pax :SEBASTIAN, PassNo :H4429316/Pax :ANU CHETTU, PassNo :K1061340/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1300","185","99","15","Bank Receipts","0","161200","DEPOSITED IN FEDERAL  BANK","2016-09-20","2016-09-19","15","1","2","0","VouNo#BR15,INV#IV5,IV15,IV43,IV46,IV48,IV55,IV114,IV123,IV147 Received By :Akhil,Details:INV#IV147,Pax :SEBASTIAN, PassNo :H4429316/Pax :ANU CHETTU, PassNo :K1061340/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1303","114","185","13","Bank Receipts","45600","0","DEPOSITED CASH IN FEDERAL BANK","2016-09-20","2016-09-17","15","1","2","0","VouNo#BR13,INV#IV39,IV40 Received By :Akhil,Details:INV#IV40,Pax :NILAA NIKHIL, PassNo :P0422439/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("1304","185","114","13","Bank Receipts","0","45600","DEPOSITED CASH IN FEDERAL BANK","2016-09-20","2016-09-17","15","1","2","0","VouNo#BR13,INV#IV39,IV40 Received By :Akhil,Details:INV#IV40,Pax :NILAA NIKHIL, PassNo :P0422439/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("1305","93","185","12","Bank Receipts","50000","0","CASH DEPOSITED IN FEDERAL BANK","2016-09-20","2016-09-15","15","1","2","0","VouNo#BR12,INV#IV33,IV35,IV58 Received By :Akhil,Details:INV#IV58,Pax :NAFSIYA KOLAMBIL, PassNo :G4454840/Pax :RSHAN AHAMMED, PassNo :P0352855/Pax :TANIA ISRA, PassNo :K5269033/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1306","185","93","12","Bank Receipts","0","50000","CASH DEPOSITED IN FEDERAL BANK","2016-09-20","2016-09-15","15","1","2","0","VouNo#BR12,INV#IV33,IV35,IV58 Received By :Akhil,Details:INV#IV58,Pax :NAFSIYA KOLAMBIL, PassNo :G4454840/Pax :RSHAN AHAMMED, PassNo :P0352855/Pax :TANIA ISRA, PassNo :K5269033/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1307","95","185","11","Bank Receipts","100000","0","DEPOSITED IN FEDERAL BANK","2016-09-20","2016-09-09","15","1","2","0","VouNo#BR11,INV#IV16,IV26,IV34,IV38,IV42,IV44 Received By :Akhil,Details:INV#IV44,Pax :SHAMEEM, PassNo :K3072306/Pax :SRIMATHY, PassNo :P4071810/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1308","185","95","11","Bank Receipts","0","100000","DEPOSITED IN FEDERAL BANK","2016-09-20","2016-09-09","15","1","2","0","VouNo#BR11,INV#IV16,IV26,IV34,IV38,IV42,IV44 Received By :Akhil,Details:INV#IV44,Pax :SHAMEEM, PassNo :K3072306/Pax :SRIMATHY, PassNo :P4071810/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1309","130","185","14","Bank Receipts","25000","0","CASH DEPOSITED IN FEDREAL BANK","2016-09-20","2016-09-19","15","1","2","0","VouNo#BR14,INV#IV32 Received By :Akhil,Details:INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1310","185","130","14","Bank Receipts","0","25000","CASH DEPOSITED IN FEDREAL BANK","2016-09-20","2016-09-19","15","1","2","0","VouNo#BR14,INV#IV32 Received By :Akhil,Details:INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1311","151","6","186","Visa Receivable","0","5800","Visa Receivable : 186","2016-09-20","2016-09-20","8","1","2","0","INV#IV186,Pax :MUSTHAK ALI AHAMMED, PassNo :M1904121/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1312","6","151","186","Visa Receivable","5800","0","Visa Receivable : 186","2016-09-20","2016-09-20","8","1","2","0","INV#IV186,Pax :MUSTHAK ALI AHAMMED, PassNo :M1904121/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1313","94","7","183","Visa Payable","5500","0","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV186,Pax :MUSTHAK ALI AHAMMED, PassNo :M1904121/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1314","7","94","183","Visa Payable","0","5500","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV186,Pax :MUSTHAK ALI AHAMMED, PassNo :M1904121/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1315","151","2","20","Cash Receipts","5800","0","received cash","2016-09-20","2016-09-20","15","1","2","0","VouNo#CR 20,INV#IV186 Received By :Akhil,Details:INV#IV186,Pax :MUSTHAK ALI AHAMMED, PassNo :M1904121/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1316","2","151","20","Cash Receipts","0","5800","received cash","2016-09-20","2016-09-20","15","1","2","0","VouNo#CR 20,INV#IV186 Received By :Akhil,Details:INV#IV186,Pax :MUSTHAK ALI AHAMMED, PassNo :M1904121/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1317","186","6","187","Visa Receivable","0","18500","Visa Receivable : 187","2016-09-20","2016-09-20","10","1","2","0","INV#IV187,Pax :AZEEZ NARACHUMMANTAKATH, PassNo :K8764291/ visaFor:90 DYAS DXB LESSUER,By:Irfad");
INSERT INTO transaction VALUES("1318","6","186","187","Visa Receivable","18500","0","Visa Receivable : 187","2016-09-20","2016-09-20","10","1","2","0","INV#IV187,Pax :AZEEZ NARACHUMMANTAKATH, PassNo :K8764291/ visaFor:90 DYAS DXB LESSUER,By:Irfad");
INSERT INTO transaction VALUES("1319","97","7","184","Visa Payable","17200","0","","2016-09-20","2016-09-20","10","1","2","0","Details:INV#IV187,Pax :AZEEZ NARACHUMMANTAKATH, PassNo :K8764291/ visaFor:90 DYAS DXB LESSUER,By:Irfad");
INSERT INTO transaction VALUES("1320","7","97","184","Visa Payable","0","17200","","2016-09-20","2016-09-20","10","1","2","0","Details:INV#IV187,Pax :AZEEZ NARACHUMMANTAKATH, PassNo :K8764291/ visaFor:90 DYAS DXB LESSUER,By:Irfad");
INSERT INTO transaction VALUES("1321","186","2","21","Cash Receipts","10000","0","","2016-09-20","2016-09-20","10","1","2","0","VouNo#CR 21,INV#IV187 Received By :Irfad,Details:INV#IV187,Pax :AZEEZ NARACHUMMANTAKATH, PassNo :K8764291/ visaFor:90 DYAS DXB LESSUER,By:Irfad");
INSERT INTO transaction VALUES("1322","2","186","21","Cash Receipts","0","10000","","2016-09-20","2016-09-20","10","1","2","0","VouNo#CR 21,INV#IV187 Received By :Irfad,Details:INV#IV187,Pax :AZEEZ NARACHUMMANTAKATH, PassNo :K8764291/ visaFor:90 DYAS DXB LESSUER,By:Irfad");
INSERT INTO transaction VALUES("1323","166","2","21","Cash Receipts","2000","0","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 21,INV#IA22 Received By :Manu,Details:INV#IA22, Pax:ANWAR PALLIKKALAKATH, Passport No:M6141348, Sector:CCJ/DXB, Date:18-10-2016, Tkt:I6073692, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1324","2","166","21","Cash Receipts","0","2000","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CR 21,INV#IA22 Received By :Manu,Details:INV#IA22, Pax:ANWAR PALLIKKALAKATH, Passport No:M6141348, Sector:CCJ/DXB, Date:18-10-2016, Tkt:I6073692, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1325","151","6","185","Visa Receivable","0","6800","Visa Receivable : 185","2016-09-20","2016-09-20","8","1","2","0","INV#IV185,Pax :SHAMEEM, PassNo :H0047499/ visaFor:30 DAYS DXB WITH OTB, By:Safuvan");
INSERT INTO transaction VALUES("1326","6","151","185","Visa Receivable","6800","0","Visa Receivable : 185","2016-09-20","2016-09-20","8","1","2","0","INV#IV185,Pax :SHAMEEM, PassNo :H0047499/ visaFor:30 DAYS DXB WITH OTB, By:Safuvan");
INSERT INTO transaction VALUES("1327","94","7","182","Visa Payable","6100","0","","2016-09-20","2016-09-20","8","1","2","0","INV#182,sup:94,:against:185,Details:INV#IV185,Pax :SHAMEEM, PassNo :H0047499/ visaFor:30 DAYS DXB WITH OTB, By:Safuvan");
INSERT INTO transaction VALUES("1328","7","94","182","Visa Payable","0","6100","","2016-09-20","2016-09-20","8","1","2","0","INV#182,sup:94,:against:185,Details:INV#IV185,Pax :SHAMEEM, PassNo :H0047499/ visaFor:30 DAYS DXB WITH OTB, By:Safuvan");
INSERT INTO transaction VALUES("1329","164","2","22","Cash Receipts","30600","0","","2016-09-20","2016-09-20","33","1","2","0","VouNo#CR 22,INV#IV112 Received By :Faizal,Details:INV#IV112,Pax :RIYAS, PassNo :P1018115/Pax :RAHINA THOUFEEK, PassNo :P0412729/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("1330","2","164","22","Cash Receipts","0","30600","","2016-09-20","2016-09-20","33","1","2","0","VouNo#CR 22,INV#IV112 Received By :Faizal,Details:INV#IV112,Pax :RIYAS, PassNo :P1018115/Pax :RAHINA THOUFEEK, PassNo :P0412729/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad");
INSERT INTO transaction VALUES("1331","187","6","188","Visa Receivable","0","7800","Visa Receivable : 188","2016-09-20","2016-09-20","10","1","2","0","INV#IV188,Pax :MUHAMMED IMRAN/MTM571 KHADEEJA PALLIPURAY, PassNo :N2014312/ N8496095/ visaFor:30 DAYS DXB WITH OTB,By:Irfad");
INSERT INTO transaction VALUES("1332","6","187","188","Visa Receivable","7800","0","Visa Receivable : 188","2016-09-20","2016-09-20","10","1","2","0","INV#IV188,Pax :MUHAMMED IMRAN/MTM571 KHADEEJA PALLIPURAY, PassNo :N2014312/ N8496095/ visaFor:30 DAYS DXB WITH OTB,By:Irfad");
INSERT INTO transaction VALUES("1333","125","7","185","Visa Payable","7800","0","","2016-09-20","2016-09-20","10","1","2","0","Details:INV#IV188,Pax :MUHAMMED IMRAN/MTM571 KHADEEJA PALLIPURAY, PassNo :N2014312/ N8496095/ visaFor:30 DAYS DXB WITH OTB,By:Irfad");
INSERT INTO transaction VALUES("1334","7","125","185","Visa Payable","0","7800","","2016-09-20","2016-09-20","10","1","2","0","Details:INV#IV188,Pax :MUHAMMED IMRAN/MTM571 KHADEEJA PALLIPURAY, PassNo :N2014312/ N8496095/ visaFor:30 DAYS DXB WITH OTB,By:Irfad");
INSERT INTO transaction VALUES("1335","99","185","9","Bank Receipts","78325","0","COUNTER16 cash deposited in federal bank","2016-09-20","2016-09-09","15","1","2","0","VouNo#BR9,INV#IV104 Received By :Akhil,Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("1336","185","99","9","Bank Receipts","0","78325","COUNTER16 cash deposited in federal bank","2016-09-20","2016-09-09","15","1","2","0","VouNo#BR9,INV#IV104 Received By :Akhil,Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("1337","129","185","8","Bank Receipts","50000","0","COUNTER 16 cash deposited in federal bank","2016-09-20","2016-09-07","15","1","2","0","VouNo#BR8,INV#IV92 Received By :Akhil,Details:INV#IV92,Pax :1245    AYSHANA RAMEES, PassNo :M5856848/Pax :1245	NASREENA, PassNo :M0882717/Pax :1198   VALSALA, PassNo :N2189073/Pax :1197	SAREENA, PassNo :K9858317/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("1338","185","129","8","Bank Receipts","0","50000","COUNTER 16 cash deposited in federal bank","2016-09-20","2016-09-07","15","1","2","0","VouNo#BR8,INV#IV92 Received By :Akhil,Details:INV#IV92,Pax :1245    AYSHANA RAMEES, PassNo :M5856848/Pax :1245	NASREENA, PassNo :M0882717/Pax :1198   VALSALA, PassNo :N2189073/Pax :1197	SAREENA, PassNo :K9858317/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("1339","95","185","7","Bank Receipts","150000","0","cash deposited in federal bank","2016-09-20","2016-09-09","15","1","2","0","VouNo#BR7,INV#IV2,IV7,IV8,IV10,IV11,IV12,IV13,IV17,IV26,IV29 Received By :Akhil,Details:INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("1340","185","95","7","Bank Receipts","0","150000","cash deposited in federal bank","2016-09-20","2016-09-09","15","1","2","0","VouNo#BR7,INV#IV2,IV7,IV8,IV10,IV11,IV12,IV13,IV17,IV26,IV29 Received By :Akhil,Details:INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("1341","136","185","6","Bank Receipts","15418","0","Deposited in south indian bank","2016-09-20","2016-09-06","15","1","2","0","VouNo#BR6,INV#IV70 Received By :Akhil,Details:INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1342","185","136","6","Bank Receipts","0","15418","Deposited in south indian bank","2016-09-20","2016-09-06","15","1","2","0","VouNo#BR6,INV#IV70 Received By :Akhil,Details:INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1343","130","185","4","Bank Receipts","50000","0","cash deposited in federal bank","2016-09-20","2016-09-08","15","1","2","0","VouNo#BR4,INV#IV28,IV30,IV32 Received By :Akhil,Details:INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1344","185","130","4","Bank Receipts","0","50000","cash deposited in federal bank","2016-09-20","2016-09-08","15","1","2","0","VouNo#BR4,INV#IV28,IV30,IV32 Received By :Akhil,Details:INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1345","81","2","8","Cash Payments","0","126500","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CP8,INV#PA15 Paid By :Manu To : MTM  CCJ,Details:INV#IA15, Pax:MACHINCHERY THOOMBIL, MOHAMED, Passport No:, Sector:CCJ/DXB, Date:16-12-2016/07-01-2017, Tkt:0B134I/0B134I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1346","2","81","8","Cash Payments","126500","0","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CP8,INV#PA15 Paid By :Manu To : MTM  CCJ,Details:INV#IA15, Pax:MACHINCHERY THOOMBIL, MOHAMED, Passport No:, Sector:CCJ/DXB, Date:16-12-2016/07-01-2017, Tkt:0B134I/0B134I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1347","142","185","3","Bank Receipts","15000","0","cash deposited in federal bank","2016-09-20","2016-09-08","15","1","2","0","VouNo#BR3,INV#IV81 Received By :Akhil,Details:INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1348","185","142","3","Bank Receipts","0","15000","cash deposited in federal bank","2016-09-20","2016-09-08","15","1","2","0","VouNo#BR3,INV#IV81 Received By :Akhil,Details:INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1349","81","2","9","Cash Payments","0","67030","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CP9,INV#PA18 Paid By :Manu To : MTM  CCJ,Details:INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1350","2","81","9","Cash Payments","67030","0","","2016-09-20","2016-09-20","6","2","2","0","VouNo#CP9,INV#PA18 Paid By :Manu To : MTM  CCJ,Details:INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1351","95","185","2","Bank Receipts","137700","0","COUNTER 16  deposited in federal bank","2016-09-20","2016-09-05","15","1","2","0","VouNo#BR2,INV#IV62,IV63,IV64,IV65,IV66,IV71,IV72,IV74,IV75,IV76 Received By :Akhil,Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("1352","185","95","2","Bank Receipts","0","137700","COUNTER 16  deposited in federal bank","2016-09-20","2016-09-05","15","1","2","0","VouNo#BR2,INV#IV62,IV63,IV64,IV65,IV66,IV71,IV72,IV74,IV75,IV76 Received By :Akhil,Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("1353","136","185","1","Bank Receipts","46123","0","DEPOSITED BY MUHAMMED KOTTATHAYIL  IN SOUTH INDIAN bank","2016-09-20","2016-09-07","15","1","2","0","VouNo#BR1,INV#IV49 Received By :Akhil,Details:INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1354","185","136","1","Bank Receipts","0","46123","DEPOSITED BY MUHAMMED KOTTATHAYIL  IN SOUTH INDIAN bank","2016-09-20","2016-09-07","15","1","2","0","VouNo#BR1,INV#IV49 Received By :Akhil,Details:INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1355","132","185","7","Bank Payments","0","100000","PAID CHEQUE TO federal  BANK","2016-09-20","2016-09-19","15","1","2","0","VouNo#BP7,INV#PV117,PV119,PV121,PV125,PV127 Paid By :Akhil To : ALPHA TOURS AND TRAVELS,Details:INV#IV129,Pax :NUSRATH, PassNo :M2789304/Pax :MINHA FATHIMA, PassNo :M4145622/Pax :MALLIK KUMARSWAMY, PassNo :J8582907/Pax :MARIYA, PassNo :L1316251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1356","185","132","7","Bank Payments","100000","0","PAID CHEQUE TO federal  BANK","2016-09-20","2016-09-19","15","1","2","0","VouNo#BP7,INV#PV117,PV119,PV121,PV125,PV127 Paid By :Akhil To : ALPHA TOURS AND TRAVELS,Details:INV#IV129,Pax :NUSRATH, PassNo :M2789304/Pax :MINHA FATHIMA, PassNo :M4145622/Pax :MALLIK KUMARSWAMY, PassNo :J8582907/Pax :MARIYA, PassNo :L1316251/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1361","132","185","6","Bank Payments","0","500000","PAID CHEQUE TO federal BANK","2016-09-20","2016-09-19","15","1","2","0","VouNo#BP6,INV#PV37,PV39,PV40,PV41,PV42,PV43,PV44,PV45,PV48,PV50,PV52,PV53,PV57,PV69,PV84,PV100,PV101,PV107,PV110,PV114,PV115,PV116,PV117 Paid By :Akhil To : ALPHA TOURS AND TRAVELS,Details:INV#IV119,Pax :YOONUS, PassNo :N554446/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1362","185","132","6","Bank Payments","500000","0","PAID CHEQUE TO federal BANK","2016-09-20","2016-09-19","15","1","2","0","VouNo#BP6,INV#PV37,PV39,PV40,PV41,PV42,PV43,PV44,PV45,PV48,PV50,PV52,PV53,PV57,PV69,PV84,PV100,PV101,PV107,PV110,PV114,PV115,PV116,PV117 Paid By :Akhil To : ALPHA TOURS AND TRAVELS,Details:INV#IV119,Pax :YOONUS, PassNo :N554446/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1363","94","185","5","Bank Payments","0","100000","paid cheque to federal bank","2016-09-20","2016-09-17","15","1","2","0","VouNo#BP5,INV#PV68,PV79,PV81,PV82,PV83,PV103,PV104,PV105,PV108,PV109,PV111 Paid By :Akhil To : SALIL PURCHASE,Details:INV#IV113,Pax :KUNJHU MOIDEEN, PassNo :K0385278/Pax :AYISHA, PassNo :K0385277/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1364","185","94","5","Bank Payments","100000","0","paid cheque to federal bank","2016-09-20","2016-09-17","15","1","2","0","VouNo#BP5,INV#PV68,PV79,PV81,PV82,PV83,PV103,PV104,PV105,PV108,PV109,PV111 Paid By :Akhil To : SALIL PURCHASE,Details:INV#IV113,Pax :KUNJHU MOIDEEN, PassNo :K0385278/Pax :AYISHA, PassNo :K0385277/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1365","94","185","4","Bank Payments","0","300000","paid cheque to federal bank","2016-09-20","2016-09-09","15","1","2","0","VouNo#BP4,INV#PV46,PV49,PV51,PV54,PV55,PV56,PV66,PV67,PV68 Paid By :Akhil To : SALIL PURCHASE,Details:INV#IV69,Pax :SUSHANTH, PassNo :K5230981/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1366","185","94","4","Bank Payments","300000","0","paid cheque to federal bank","2016-09-20","2016-09-09","15","1","2","0","VouNo#BP4,INV#PV46,PV49,PV51,PV54,PV55,PV56,PV66,PV67,PV68 Paid By :Akhil To : SALIL PURCHASE,Details:INV#IV69,Pax :SUSHANTH, PassNo :K5230981/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1367","94","185","3","Bank Payments","0","300000","paid cheque to south indian bank","2016-09-20","2016-09-07","15","1","2","0","VouNo#BP3,INV#PV2,PV3,PV5,PV6,PV7,PV8,PV10,PV13,PV19,PV25,PV26,PV27,PV28,PV31,PV32,PV35,PV38,PV47,PV49 Paid By :Akhil To : SALIL PURCHASE,Details:INV#IV50,Pax :ARIF, PassNo :M9418291/Pax :BONEY JOSEPH, PassNo :G8586116/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1368","185","94","3","Bank Payments","300000","0","paid cheque to south indian bank","2016-09-20","2016-09-07","15","1","2","0","VouNo#BP3,INV#PV2,PV3,PV5,PV6,PV7,PV8,PV10,PV13,PV19,PV25,PV26,PV27,PV28,PV31,PV32,PV35,PV38,PV47,PV49 Paid By :Akhil To : SALIL PURCHASE,Details:INV#IV50,Pax :ARIF, PassNo :M9418291/Pax :BONEY JOSEPH, PassNo :G8586116/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1369","125","185","1","Bank Payments","0","78325","paid cheque  to federal bank","2016-09-20","2016-09-09","15","1","2","0","VouNo#BP1,INV#PV102 Paid By :Akhil To : counter16,Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("1370","185","125","1","Bank Payments","78325","0","paid cheque  to federal bank","2016-09-20","2016-09-09","15","1","2","0","VouNo#BP1,INV#PV102 Paid By :Akhil To : counter16,Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("1375","125","185","2","Bank Payments","0","110900","paid cheque for against  federal bank","2016-09-20","2016-09-09","15","1","2","0","VouNo#BP2,INV#PV62,PV63,PV64,PV65,PV70,PV72,PV73,PV74 Paid By :Akhil To : counter16,Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("1376","185","125","2","Bank Payments","110900","0","paid cheque for against  federal bank","2016-09-20","2016-09-09","15","1","2","0","VouNo#BP2,INV#PV62,PV63,PV64,PV65,PV70,PV72,PV73,PV74 Paid By :Akhil To : counter16,Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("1377","189","6","191","Visa Receivable","0","17400","Visa Receivable : 191","2016-09-20","2016-09-20","10","1","2","0","INV#IV191,Pax :counter :156 332 429 955, PassNo :k000000/ visaFor:Dubai- 30 days,By:Irfad");
INSERT INTO transaction VALUES("1378","6","189","191","Visa Receivable","17400","0","Visa Receivable : 191","2016-09-20","2016-09-20","10","1","2","0","INV#IV191,Pax :counter :156 332 429 955, PassNo :k000000/ visaFor:Dubai- 30 days,By:Irfad");
INSERT INTO transaction VALUES("1379","125","7","188","Visa Payable","17400","0","","2016-09-20","2016-09-20","10","1","2","0","Details:INV#IV191,Pax :counter :156 332 429 955, PassNo :k000000/ visaFor:Dubai- 30 days,By:Irfad");
INSERT INTO transaction VALUES("1380","7","125","188","Visa Payable","0","17400","","2016-09-20","2016-09-20","10","1","2","0","Details:INV#IV191,Pax :counter :156 332 429 955, PassNo :k000000/ visaFor:Dubai- 30 days,By:Irfad");
INSERT INTO transaction VALUES("1381","95","185","16","Bank Receipts","300000","0","deposited in federal bank","2016-09-20","2016-09-20","15","1","2","0","VouNo#BR16,INV#IV44,IV47,IV50,IV51,IV56,IV68 Received By :Akhil,Details:INV#IV68,Pax :THAJUDHEEN, PassNo :K1076374/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1382","185","95","16","Bank Receipts","0","300000","deposited in federal bank","2016-09-20","2016-09-20","15","1","2","0","VouNo#BR16,INV#IV44,IV47,IV50,IV51,IV56,IV68 Received By :Akhil,Details:INV#IV68,Pax :THAJUDHEEN, PassNo :K1076374/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("1383","187","2","23","Cash Receipts","7800","0","cash received","2016-09-20","2016-09-20","15","1","2","0","VouNo#CR 23,INV#IV188 Received By :Akhil,Details:INV#IV188,Pax :MUHAMMED IMRAN/MTM571 KHADEEJA PALLIPURAY, PassNo :N2014312/ N8496095/ visaFor:30 DAYS DXB WITH OTB,By:Irfad");
INSERT INTO transaction VALUES("1384","2","187","23","Cash Receipts","0","7800","cash received","2016-09-20","2016-09-20","15","1","2","0","VouNo#CR 23,INV#IV188 Received By :Akhil,Details:INV#IV188,Pax :MUHAMMED IMRAN/MTM571 KHADEEJA PALLIPURAY, PassNo :N2014312/ N8496095/ visaFor:30 DAYS DXB WITH OTB,By:Irfad");
INSERT INTO transaction VALUES("1387","188","6","190","Visa Receivable","0","54700","Visa Receivable : 190","2016-09-20","2016-09-20","10","1","2","0","INV#IV190,Pax :COUNTER :128 129 358 390 522 1231, PassNo :k000000/ visaFor:Dubai- 30 days, By:Irfad");
INSERT INTO transaction VALUES("1388","6","188","190","Visa Receivable","54700","0","Visa Receivable : 190","2016-09-20","2016-09-20","10","1","2","0","INV#IV190,Pax :COUNTER :128 129 358 390 522 1231, PassNo :k000000/ visaFor:Dubai- 30 days, By:Irfad");
INSERT INTO transaction VALUES("1389","125","7","187","Visa Payable","54700","0","","2016-09-20","2016-09-20","10","1","2","0","INV#187,sup:125,:against:190,Details:INV#IV190,Pax :COUNTER :128 129 358 390 522 1231, PassNo :k000000/ visaFor:Dubai- 30 days, By:Irfad");
INSERT INTO transaction VALUES("1390","7","125","187","Visa Payable","0","54700","","2016-09-20","2016-09-20","10","1","2","0","INV#187,sup:125,:against:190,Details:INV#IV190,Pax :COUNTER :128 129 358 390 522 1231, PassNo :k000000/ visaFor:Dubai- 30 days, By:Irfad");
INSERT INTO transaction VALUES("1395","87","4","30","Airline Receivable","0","24100","Airline Receivable: 30","2016-09-20","2016-09-20","6","2","2","0","INV#IA30, Pax:MANAKKADVAKKATHODI/ABDULAZEEZ, Passport No:K2511923, Sector:COK/DXB, Date:21-09-2016, Tkt:176-9165944010, PNR:4LWZD7, By:Manu");
INSERT INTO transaction VALUES("1396","4","87","30","Airline Receivable","24100","0","Airline Receivable: 30","2016-09-20","2016-09-20","6","2","2","0","INV#IA30, Pax:MANAKKADVAKKATHODI/ABDULAZEEZ, Passport No:K2511923, Sector:COK/DXB, Date:21-09-2016, Tkt:176-9165944010, PNR:4LWZD7, By:Manu");
INSERT INTO transaction VALUES("1397","83","5","30","Airline Payable","23900","0","","2016-09-20","2016-09-20","6","2","2","0","Details:INV#IA30, Pax:MANAKKADVAKKATHODI/ABDULAZEEZ, Passport No:K2511923, Sector:COK/DXB, Date:21-09-2016, Tkt:176-9165944010, PNR:4LWZD7, By:Manu");
INSERT INTO transaction VALUES("1398","5","83","30","Airline Payable","0","23900","","2016-09-20","2016-09-20","6","2","2","0","Details:INV#IA30, Pax:MANAKKADVAKKATHODI/ABDULAZEEZ, Passport No:K2511923, Sector:COK/DXB, Date:21-09-2016, Tkt:176-9165944010, PNR:4LWZD7, By:Manu");
INSERT INTO transaction VALUES("1403","188","6","192","Visa Receivable","0","18000","Visa Receivable : 192","2016-09-20","2016-09-20","10","1","2","0","INV#IV192,Pax :RAHMATH ALI, PassNo :K1301847/Pax :MTM603 AMEERUDHEEN , PassNo :J8840991/Pax :  MOHAMMED AL, PassNo :K4044842/ visaFor:SAUDI VISA STAMPING, By:Irfad");
INSERT INTO transaction VALUES("1404","6","188","192","Visa Receivable","18000","0","Visa Receivable : 192","2016-09-20","2016-09-20","10","1","2","0","INV#IV192,Pax :RAHMATH ALI, PassNo :K1301847/Pax :MTM603 AMEERUDHEEN , PassNo :J8840991/Pax :  MOHAMMED AL, PassNo :K4044842/ visaFor:SAUDI VISA STAMPING, By:Irfad");
INSERT INTO transaction VALUES("1405","190","7","189","Visa Payable","15000","0","","2016-09-20","2016-09-20","10","1","2","0","INV#189,sup:190,:against:192,Details:INV#IV192,Pax :RAHMATH ALI, PassNo :K1301847/Pax :MTM603 AMEERUDHEEN , PassNo :J8840991/Pax :  MOHAMMED AL, PassNo :K4044842/ visaFor:SAUDI VISA STAMPING, By:Irfad");
INSERT INTO transaction VALUES("1406","7","190","189","Visa Payable","0","15000","","2016-09-20","2016-09-20","10","1","2","0","INV#189,sup:190,:against:192,Details:INV#IV192,Pax :RAHMATH ALI, PassNo :K1301847/Pax :MTM603 AMEERUDHEEN , PassNo :J8840991/Pax :  MOHAMMED AL, PassNo :K4044842/ visaFor:SAUDI VISA STAMPING, By:Irfad");
INSERT INTO transaction VALUES("1407","188","2","24","Cash Receipts","18000","0","","2016-09-20","2016-09-20","33","1","2","0","VouNo#CR 24,INV#IV192 Received By :Faizal,Details:INV#IV192,Pax :RAHMATH ALI, PassNo :K1301847/Pax :MTM603 AMEERUDHEEN , PassNo :J8840991/Pax :  MOHAMMED AL, PassNo :K4044842/ visaFor:SAUDI VISA STAMPING, By:Irfad");
INSERT INTO transaction VALUES("1408","2","188","24","Cash Receipts","0","18000","","2016-09-20","2016-09-20","33","1","2","0","VouNo#CR 24,INV#IV192 Received By :Faizal,Details:INV#IV192,Pax :RAHMATH ALI, PassNo :K1301847/Pax :MTM603 AMEERUDHEEN , PassNo :J8840991/Pax :  MOHAMMED AL, PassNo :K4044842/ visaFor:SAUDI VISA STAMPING, By:Irfad");
INSERT INTO transaction VALUES("1409","190","2","13","Cash Payments","0","15000","","2016-09-20","2016-09-20","33","1","2","0","VouNo#CP9,INV#PV189 Paid By :Faizal To : INDO ARAB CCJ,Details:INV#IV192,Pax :RAHMATH ALI, PassNo :K1301847/Pax :MTM603 AMEERUDHEEN , PassNo :J8840991/Pax :  MOHAMMED AL, PassNo :K4044842/ visaFor:SAUDI VISA STAMPING, By:Irfad");
INSERT INTO transaction VALUES("1410","2","190","13","Cash Payments","15000","0","","2016-09-20","2016-09-20","33","1","2","0","VouNo#CP9,INV#PV189 Paid By :Faizal To : INDO ARAB CCJ,Details:INV#IV192,Pax :RAHMATH ALI, PassNo :K1301847/Pax :MTM603 AMEERUDHEEN , PassNo :J8840991/Pax :  MOHAMMED AL, PassNo :K4044842/ visaFor:SAUDI VISA STAMPING, By:Irfad");
INSERT INTO transaction VALUES("1411","95","6","184","Visa Receivable","0","14600","Visa Receivable : 184","2016-09-20","2016-09-19","9","1","2","0","INV#IV184,Pax :LINTO, PassNo :M2693421/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Sahad");
INSERT INTO transaction VALUES("1412","6","95","184","Visa Receivable","14600","0","Visa Receivable : 184","2016-09-20","2016-09-19","9","1","2","0","INV#IV184,Pax :LINTO, PassNo :M2693421/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Sahad");
INSERT INTO transaction VALUES("1413","132","7","181","Visa Payable","14600","0","","2016-09-20","2016-09-19","9","1","2","0","INV#181,sup:132,:against:184,Details:INV#IV184,Pax :LINTO, PassNo :M2693421/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Sahad");
INSERT INTO transaction VALUES("1414","7","132","181","Visa Payable","0","14600","","2016-09-20","2016-09-19","9","1","2","0","INV#181,sup:132,:against:184,Details:INV#IV184,Pax :LINTO, PassNo :M2693421/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Sahad");
INSERT INTO transaction VALUES("1419","95","6","194","Visa Receivable","0","14600","Visa Receivable : 194","2016-09-20","2016-09-20","9","1","2","0","INV#IV194,Pax :KRISHANANUNNY, PassNo :N2509254/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1420","6","95","194","Visa Receivable","14600","0","Visa Receivable : 194","2016-09-20","2016-09-20","9","1","2","0","INV#IV194,Pax :KRISHANANUNNY, PassNo :N2509254/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1421","94","7","191","Visa Payable","14100","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV194,Pax :KRISHANANUNNY, PassNo :N2509254/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1422","7","94","191","Visa Payable","0","14100","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV194,Pax :KRISHANANUNNY, PassNo :N2509254/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1423","165","6","195","Visa Receivable","0","29200","Visa Receivable : 195","2016-09-20","2016-09-20","9","1","2","0","INV#IV195,Pax :NOUSHAD, PassNo :L9056502/Pax :ASKAR, PassNo :N2124342/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1424","6","165","195","Visa Receivable","29200","0","Visa Receivable : 195","2016-09-20","2016-09-20","9","1","2","0","INV#IV195,Pax :NOUSHAD, PassNo :L9056502/Pax :ASKAR, PassNo :N2124342/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1425","94","7","192","Visa Payable","28200","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV195,Pax :NOUSHAD, PassNo :L9056502/Pax :ASKAR, PassNo :N2124342/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1426","7","94","192","Visa Payable","0","28200","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV195,Pax :NOUSHAD, PassNo :L9056502/Pax :ASKAR, PassNo :N2124342/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1427","142","6","196","Visa Receivable","0","14800","Visa Receivable : 196","2016-09-20","2016-09-20","9","1","2","0","INV#IV196,Pax :NITHIN VARGHESE, PassNo :K5969564/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1428","6","142","196","Visa Receivable","14800","0","Visa Receivable : 196","2016-09-20","2016-09-20","9","1","2","0","INV#IV196,Pax :NITHIN VARGHESE, PassNo :K5969564/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1429","94","7","193","Visa Payable","14100","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV196,Pax :NITHIN VARGHESE, PassNo :K5969564/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1430","7","94","193","Visa Payable","0","14100","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV196,Pax :NITHIN VARGHESE, PassNo :K5969564/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1431","101","6","197","Visa Receivable","0","10000","Visa Receivable : 197","2016-09-20","2016-09-20","9","1","2","0","INV#IV197,Pax :PRINCE, PassNo :N1725019/ visaFor:10 DAYS OMAN,By:Sahad");
INSERT INTO transaction VALUES("1432","6","101","197","Visa Receivable","10000","0","Visa Receivable : 197","2016-09-20","2016-09-20","9","1","2","0","INV#IV197,Pax :PRINCE, PassNo :N1725019/ visaFor:10 DAYS OMAN,By:Sahad");
INSERT INTO transaction VALUES("1433","97","7","194","Visa Payable","8000","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV197,Pax :PRINCE, PassNo :N1725019/ visaFor:10 DAYS OMAN,By:Sahad");
INSERT INTO transaction VALUES("1434","7","97","194","Visa Payable","0","8000","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV197,Pax :PRINCE, PassNo :N1725019/ visaFor:10 DAYS OMAN,By:Sahad");
INSERT INTO transaction VALUES("1435","192","2","1","Cash Receipts","3389390","0","1000*25*100+(500*16*100)+(79*1000)+(68*100)+(70*50)+90","2016-09-20","2016-09-20","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");
INSERT INTO transaction VALUES("1436","2","192","1","Cash Receipts","0","3389390","1000*25*100+(500*16*100)+(79*1000)+(68*100)+(70*50)+90","2016-09-20","2016-09-20","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");
INSERT INTO transaction VALUES("1437","99","6","198","Visa Receivable","0","14600","Visa Receivable : 198","2016-09-20","2016-09-20","9","1","2","0","INV#IV198,Pax :MUHAMMED MUSTHAFA, PassNo :G7682506/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1438","6","99","198","Visa Receivable","14600","0","Visa Receivable : 198","2016-09-20","2016-09-20","9","1","2","0","INV#IV198,Pax :MUHAMMED MUSTHAFA, PassNo :G7682506/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1439","94","7","195","Visa Payable","14100","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV198,Pax :MUHAMMED MUSTHAFA, PassNo :G7682506/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1440","7","94","195","Visa Payable","0","14100","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV198,Pax :MUHAMMED MUSTHAFA, PassNo :G7682506/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1441","169","6","199","Visa Receivable","0","14500","Visa Receivable : 199","2016-09-20","2016-09-20","9","1","2","0","INV#IV199,Pax :SHAROOK, PassNo :N0479933/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1442","6","169","199","Visa Receivable","14500","0","Visa Receivable : 199","2016-09-20","2016-09-20","9","1","2","0","INV#IV199,Pax :SHAROOK, PassNo :N0479933/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1443","132","7","196","Visa Payable","14600","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV199,Pax :SHAROOK, PassNo :N0479933/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1444","7","132","196","Visa Payable","0","14600","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV199,Pax :SHAROOK, PassNo :N0479933/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1445","192","2","1","Cash Receipts","2000","0","old balnce of bid101","2016-09-20","2016-09-20","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");
INSERT INTO transaction VALUES("1446","2","192","1","Cash Receipts","0","2000","old balnce of bid101","2016-09-20","2016-09-20","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");
INSERT INTO transaction VALUES("1447","169","6","200","Visa Receivable","0","43500","Visa Receivable : 200","2016-09-20","2016-09-20","9","1","2","0","INV#IV200,Pax :NOUFAL, PassNo :L4528400/Pax :MOHAMMED AFLAH, PassNo :L421240/Pax :SHAFEEK, PassNo :H3389846/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1448","6","169","200","Visa Receivable","43500","0","Visa Receivable : 200","2016-09-20","2016-09-20","9","1","2","0","INV#IV200,Pax :NOUFAL, PassNo :L4528400/Pax :MOHAMMED AFLAH, PassNo :L421240/Pax :SHAFEEK, PassNo :H3389846/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1449","94","7","197","Visa Payable","42300","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV200,Pax :NOUFAL, PassNo :L4528400/Pax :MOHAMMED AFLAH, PassNo :L421240/Pax :SHAFEEK, PassNo :H3389846/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1450","7","94","197","Visa Payable","0","42300","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV200,Pax :NOUFAL, PassNo :L4528400/Pax :MOHAMMED AFLAH, PassNo :L421240/Pax :SHAFEEK, PassNo :H3389846/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1451","169","6","201","Visa Receivable","0","29000","Visa Receivable : 201","2016-09-20","2016-09-20","9","1","2","0","INV#IV201,Pax :MATHAI, PassNo :H5378488/Pax :ACHAMMA, PassNo :H7575435/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1452","6","169","201","Visa Receivable","29000","0","Visa Receivable : 201","2016-09-20","2016-09-20","9","1","2","0","INV#IV201,Pax :MATHAI, PassNo :H5378488/Pax :ACHAMMA, PassNo :H7575435/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1453","132","7","198","Visa Payable","29200","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV201,Pax :MATHAI, PassNo :H5378488/Pax :ACHAMMA, PassNo :H7575435/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1454","7","132","198","Visa Payable","0","29200","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV201,Pax :MATHAI, PassNo :H5378488/Pax :ACHAMMA, PassNo :H7575435/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1455","87","4","31","Airline Receivable","0","11500","Airline Receivable: 31","2016-09-20","2016-09-20","6","2","2","0","INV#IA31, Pax:MOHAMMED SHAJI PONNANAGATHODI  , Passport No:, Sector:CCJ/COK, Date:20-09-2016, Tkt:74YNNB, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1456","4","87","31","Airline Receivable","11500","0","Airline Receivable: 31","2016-09-20","2016-09-20","6","2","2","0","INV#IA31, Pax:MOHAMMED SHAJI PONNANAGATHODI  , Passport No:, Sector:CCJ/COK, Date:20-09-2016, Tkt:74YNNB, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1457","81","5","31","Airline Payable","11210","0","","2016-09-20","2016-09-20","6","2","2","0","Details:INV#IA31, Pax:MOHAMMED SHAJI PONNANAGATHODI  , Passport No:, Sector:CCJ/COK, Date:20-09-2016, Tkt:74YNNB, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1458","5","81","31","Airline Payable","0","11210","","2016-09-20","2016-09-20","6","2","2","0","Details:INV#IA31, Pax:MOHAMMED SHAJI PONNANAGATHODI  , Passport No:, Sector:CCJ/COK, Date:20-09-2016, Tkt:74YNNB, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1459","169","6","202","Visa Receivable","0","87000","Visa Receivable : 202","2016-09-20","2016-09-20","9","1","2","0","INV#IV202,Pax :RASHID ALUNGAL, PassNo :K9765697/Pax :MOHAMMED RASHID, PassNo :K9744697/Pax :SAMEER, PassNo :G8235703/Pax :SHAMEEM, PassNo :M6015145/Pax :ANOOP, PassNo :M49736009/Pax :MUHAMMED SABIR, PassNo :N7803090/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1460","6","169","202","Visa Receivable","87000","0","Visa Receivable : 202","2016-09-20","2016-09-20","9","1","2","0","INV#IV202,Pax :RASHID ALUNGAL, PassNo :K9765697/Pax :MOHAMMED RASHID, PassNo :K9744697/Pax :SAMEER, PassNo :G8235703/Pax :SHAMEEM, PassNo :M6015145/Pax :ANOOP, PassNo :M49736009/Pax :MUHAMMED SABIR, PassNo :N7803090/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1461","94","7","199","Visa Payable","84600","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV202,Pax :RASHID ALUNGAL, PassNo :K9765697/Pax :MOHAMMED RASHID, PassNo :K9744697/Pax :SAMEER, PassNo :G8235703/Pax :SHAMEEM, PassNo :M6015145/Pax :ANOOP, PassNo :M49736009/Pax :MUHAMMED SABIR, PassNo :N7803090/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1462","7","94","199","Visa Payable","0","84600","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV202,Pax :RASHID ALUNGAL, PassNo :K9765697/Pax :MOHAMMED RASHID, PassNo :K9744697/Pax :SAMEER, PassNo :G8235703/Pax :SHAMEEM, PassNo :M6015145/Pax :ANOOP, PassNo :M49736009/Pax :MUHAMMED SABIR, PassNo :N7803090/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1463","169","6","203","Visa Receivable","0","43500","Visa Receivable : 203","2016-09-20","2016-09-20","9","1","2","0","INV#IV203,Pax :BUSHARA, PassNo :K1586814/Pax :ABDUL BASITH, PassNo :N7631192/Pax :SIRAJ, PassNo :M8148005/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1464","6","169","203","Visa Receivable","43500","0","Visa Receivable : 203","2016-09-20","2016-09-20","9","1","2","0","INV#IV203,Pax :BUSHARA, PassNo :K1586814/Pax :ABDUL BASITH, PassNo :N7631192/Pax :SIRAJ, PassNo :M8148005/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1465","132","7","200","Visa Payable","43800","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV203,Pax :BUSHARA, PassNo :K1586814/Pax :ABDUL BASITH, PassNo :N7631192/Pax :SIRAJ, PassNo :M8148005/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1466","7","132","200","Visa Payable","0","43800","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV203,Pax :BUSHARA, PassNo :K1586814/Pax :ABDUL BASITH, PassNo :N7631192/Pax :SIRAJ, PassNo :M8148005/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1467","146","2","14","Cash Payments","0","15000","PAID CASH TO MARKAZ TRAVEL MART","2016-09-20","2016-09-20","15","1","2","0","VouNo#CP10,INV#PM2 Paid By :Akhil To : MARKAZ TRAVEL MART,Details:INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("1468","2","146","14","Cash Payments","15000","0","PAID CASH TO MARKAZ TRAVEL MART","2016-09-20","2016-09-20","15","1","2","0","VouNo#CP10,INV#PM2 Paid By :Akhil To : MARKAZ TRAVEL MART,Details:INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("1469","114","6","204","Visa Receivable","0","14500","Visa Receivable : 204","2016-09-20","2016-09-20","9","1","2","0","INV#IV204,Pax :RAJESH ANAND, PassNo :H3990335/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1470","6","114","204","Visa Receivable","14500","0","Visa Receivable : 204","2016-09-20","2016-09-20","9","1","2","0","INV#IV204,Pax :RAJESH ANAND, PassNo :H3990335/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1471","94","7","201","Visa Payable","14100","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV204,Pax :RAJESH ANAND, PassNo :H3990335/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1472","7","94","201","Visa Payable","0","14100","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV204,Pax :RAJESH ANAND, PassNo :H3990335/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1473","99","6","205","Visa Receivable","0","14800","Visa Receivable : 205","2016-09-20","2016-09-20","9","1","2","0","INV#IV205,Pax :SHANLY SIMON, PassNo :H6455482/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1474","6","99","205","Visa Receivable","14800","0","Visa Receivable : 205","2016-09-20","2016-09-20","9","1","2","0","INV#IV205,Pax :SHANLY SIMON, PassNo :H6455482/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1475","132","7","202","Visa Payable","14600","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV205,Pax :SHANLY SIMON, PassNo :H6455482/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1476","7","132","202","Visa Payable","0","14600","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV205,Pax :SHANLY SIMON, PassNo :H6455482/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1477","114","6","206","Visa Receivable","0","14500","Visa Receivable : 206","2016-09-20","2016-09-20","9","1","2","0","INV#IV206,Pax :MANSOOR, PassNo :G4551263/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1478","6","114","206","Visa Receivable","14500","0","Visa Receivable : 206","2016-09-20","2016-09-20","9","1","2","0","INV#IV206,Pax :MANSOOR, PassNo :G4551263/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1479","94","7","203","Visa Payable","14100","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV206,Pax :MANSOOR, PassNo :G4551263/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1480","7","94","203","Visa Payable","0","14100","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV206,Pax :MANSOOR, PassNo :G4551263/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1481","169","6","207","Visa Receivable","0","5100","Visa Receivable : 207","2016-09-20","2016-09-20","9","1","2","0","INV#IV207,Pax :SHANAS, PassNo :G6369762/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1482","6","169","207","Visa Receivable","5100","0","Visa Receivable : 207","2016-09-20","2016-09-20","9","1","2","0","INV#IV207,Pax :SHANAS, PassNo :G6369762/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1483","94","7","204","Visa Payable","5075","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV207,Pax :SHANAS, PassNo :G6369762/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1484","7","94","204","Visa Payable","0","5075","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV207,Pax :SHANAS, PassNo :G6369762/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1485","96","6","208","Visa Receivable","0","14700","Visa Receivable : 208","2016-09-20","2016-09-20","8","1","2","0","INV#IV208,Pax :RICHARD PINTO, PassNo :H4603974/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1486","6","96","208","Visa Receivable","14700","0","Visa Receivable : 208","2016-09-20","2016-09-20","8","1","2","0","INV#IV208,Pax :RICHARD PINTO, PassNo :H4603974/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1487","94","7","205","Visa Payable","14200","0","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV208,Pax :RICHARD PINTO, PassNo :H4603974/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1488","7","94","205","Visa Payable","0","14200","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV208,Pax :RICHARD PINTO, PassNo :H4603974/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1489","194","6","209","Visa Receivable","0","1000","Visa Receivable : 209","2016-09-20","2016-09-20","10","1","2","0","INV#IV209,Pax :MOOSA NEYYAN, PassNo :k000000/ visaFor:30 DAYS SAUDI,By:Irfad");
INSERT INTO transaction VALUES("1490","6","194","209","Visa Receivable","1000","0","Visa Receivable : 209","2016-09-20","2016-09-20","10","1","2","0","INV#IV209,Pax :MOOSA NEYYAN, PassNo :k000000/ visaFor:30 DAYS SAUDI,By:Irfad");
INSERT INTO transaction VALUES("1491","125","7","206","Visa Payable","1000","0","","2016-09-20","2016-09-20","10","1","2","0","Details:INV#IV209,Pax :MOOSA NEYYAN, PassNo :k000000/ visaFor:30 DAYS SAUDI,By:Irfad");
INSERT INTO transaction VALUES("1492","7","125","206","Visa Payable","0","1000","","2016-09-20","2016-09-20","10","1","2","0","Details:INV#IV209,Pax :MOOSA NEYYAN, PassNo :k000000/ visaFor:30 DAYS SAUDI,By:Irfad");
INSERT INTO transaction VALUES("1495","194","2","25","Cash Receipts","1000","0","","2016-09-20","2016-09-20","33","1","2","0","VouNo#CR 25,INV#IV209 Received By :Faizal,Details:INV#IV209,Pax :MOOSA NEYYAN, PassNo :k000000/ visaFor:30 DAYS SAUDI,By:Irfad");
INSERT INTO transaction VALUES("1496","2","194","25","Cash Receipts","0","1000","","2016-09-20","2016-09-20","33","1","2","0","VouNo#CR 25,INV#IV209 Received By :Faizal,Details:INV#IV209,Pax :MOOSA NEYYAN, PassNo :k000000/ visaFor:30 DAYS SAUDI,By:Irfad");
INSERT INTO transaction VALUES("1499","195","6","212","Visa Receivable","0","16000","Visa Receivable : 212","2016-09-20","2016-09-20","8","1","2","0","INV#IV212,Pax :, PassNo :/ visaFor:90 DAYS DXB  WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1500","6","195","212","Visa Receivable","16000","0","Visa Receivable : 212","2016-09-20","2016-09-20","8","1","2","0","INV#IV212,Pax :, PassNo :/ visaFor:90 DAYS DXB  WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1501","132","7","207","Visa Payable","14200","0","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV212,Pax :, PassNo :/ visaFor:90 DAYS DXB  WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1502","7","132","207","Visa Payable","0","14200","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV212,Pax :, PassNo :/ visaFor:90 DAYS DXB  WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1503","96","6","213","Visa Receivable","0","10400","Visa Receivable : 213","2016-09-20","2016-09-20","8","1","2","0","INV#IV213,Pax :SMITA , PassNo :/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1504","6","96","213","Visa Receivable","10400","0","Visa Receivable : 213","2016-09-20","2016-09-20","8","1","2","0","INV#IV213,Pax :SMITA , PassNo :/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1505","127","7","208","Visa Payable","10300","0","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV213,Pax :SMITA , PassNo :/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1506","7","127","208","Visa Payable","0","10300","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV213,Pax :SMITA , PassNo :/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1507","169","6","214","Visa Receivable","0","45900","Visa Receivable : 214","2016-09-20","2016-09-20","9","1","2","0","INV#IV214,Pax :ANIL KUMAR, PassNo :M2296507/Pax :JAYASREE, PassNo :K0085069/Pax :SAYED MUHAMMED, PassNo :K9227945/Pax :SAYIDATHA JAFAR, PassNo :P1015108/Pax :LIYAKATH, PassNo :L1155076/Pax :MARY KLISTED, PassNo :H5041984/Pax :JHON, PassNo :Z1971834/Pax :NAFEESU KODOTH, PassNo :G2804883/Pax :LUKMNUL KAKEEM, PassNo :M8565689/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1508","6","169","214","Visa Receivable","45900","0","Visa Receivable : 214","2016-09-20","2016-09-20","9","1","2","0","INV#IV214,Pax :ANIL KUMAR, PassNo :M2296507/Pax :JAYASREE, PassNo :K0085069/Pax :SAYED MUHAMMED, PassNo :K9227945/Pax :SAYIDATHA JAFAR, PassNo :P1015108/Pax :LIYAKATH, PassNo :L1155076/Pax :MARY KLISTED, PassNo :H5041984/Pax :JHON, PassNo :Z1971834/Pax :NAFEESU KODOTH, PassNo :G2804883/Pax :LUKMNUL KAKEEM, PassNo :M8565689/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1509","94","7","209","Visa Payable","45675","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV214,Pax :ANIL KUMAR, PassNo :M2296507/Pax :JAYASREE, PassNo :K0085069/Pax :SAYED MUHAMMED, PassNo :K9227945/Pax :SAYIDATHA JAFAR, PassNo :P1015108/Pax :LIYAKATH, PassNo :L1155076/Pax :MARY KLISTED, PassNo :H5041984/Pax :JHON, PassNo :Z1971834/Pax :NAFEESU KODOTH, PassNo :G2804883/Pax :LUKMNUL KAKEEM, PassNo :M8565689/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1510","7","94","209","Visa Payable","0","45675","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV214,Pax :ANIL KUMAR, PassNo :M2296507/Pax :JAYASREE, PassNo :K0085069/Pax :SAYED MUHAMMED, PassNo :K9227945/Pax :SAYIDATHA JAFAR, PassNo :P1015108/Pax :LIYAKATH, PassNo :L1155076/Pax :MARY KLISTED, PassNo :H5041984/Pax :JHON, PassNo :Z1971834/Pax :NAFEESU KODOTH, PassNo :G2804883/Pax :LUKMNUL KAKEEM, PassNo :M8565689/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1511","114","6","215","Visa Receivable","0","14500","Visa Receivable : 215","2016-09-20","2016-09-20","9","1","2","0","INV#IV215,Pax :MANJU MURALI, PassNo :P2098797/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1512","6","114","215","Visa Receivable","14500","0","Visa Receivable : 215","2016-09-20","2016-09-20","9","1","2","0","INV#IV215,Pax :MANJU MURALI, PassNo :P2098797/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1513","132","7","210","Visa Payable","14600","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV215,Pax :MANJU MURALI, PassNo :P2098797/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1514","7","132","210","Visa Payable","0","14600","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV215,Pax :MANJU MURALI, PassNo :P2098797/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1515","99","6","216","Visa Receivable","0","14600","Visa Receivable : 216","2016-09-20","2016-09-20","9","1","2","0","INV#IV216,Pax :YOOSAF, PassNo :N9071534/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1516","6","99","216","Visa Receivable","14600","0","Visa Receivable : 216","2016-09-20","2016-09-20","9","1","2","0","INV#IV216,Pax :YOOSAF, PassNo :N9071534/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1517","94","7","211","Visa Payable","14100","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV216,Pax :YOOSAF, PassNo :N9071534/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1518","7","94","211","Visa Payable","0","14100","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV216,Pax :YOOSAF, PassNo :N9071534/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1519","114","6","217","Visa Receivable","0","14500","Visa Receivable : 217","2016-09-20","2016-09-20","9","1","2","0","INV#IV217,Pax :SAIDU ALI AKBAR, PassNo :K6559055/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1520","6","114","217","Visa Receivable","14500","0","Visa Receivable : 217","2016-09-20","2016-09-20","9","1","2","0","INV#IV217,Pax :SAIDU ALI AKBAR, PassNo :K6559055/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1521","94","7","212","Visa Payable","14100","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV217,Pax :SAIDU ALI AKBAR, PassNo :K6559055/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1522","7","94","212","Visa Payable","0","14100","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV217,Pax :SAIDU ALI AKBAR, PassNo :K6559055/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1527","96","6","219","Visa Receivable","0","10400","Visa Receivable : 219","2016-09-20","2016-09-20","8","1","2","0","INV#IV219,Pax :MUNAVAR , PassNo :J6244054 / visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1528","6","96","219","Visa Receivable","10400","0","Visa Receivable : 219","2016-09-20","2016-09-20","8","1","2","0","INV#IV219,Pax :MUNAVAR , PassNo :J6244054 / visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1529","129","6","220","Visa Receivable","0","14800","Visa Receivable : 220","2016-09-20","2016-09-20","9","1","2","0","INV#IV220,Pax :SAKIRA , PassNo :M5045102/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1530","6","129","220","Visa Receivable","14800","0","Visa Receivable : 220","2016-09-20","2016-09-20","9","1","2","0","INV#IV220,Pax :SAKIRA , PassNo :M5045102/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1531","94","7","214","Visa Payable","10200","0","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV219,Pax :MUNAVAR , PassNo :J6244054 / visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1532","7","94","214","Visa Payable","0","10200","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV219,Pax :MUNAVAR , PassNo :J6244054 / visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1533","132","7","215","Visa Payable","14600","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV220,Pax :SAKIRA , PassNo :M5045102/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1534","7","132","215","Visa Payable","0","14600","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV220,Pax :SAKIRA , PassNo :M5045102/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1535","142","6","221","Visa Receivable","0","14700","Visa Receivable : 221","2016-09-20","2016-09-20","9","1","2","0","INV#IV221,Pax :ABDUL JALEEL, PassNo :P2516541/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1536","6","142","221","Visa Receivable","14700","0","Visa Receivable : 221","2016-09-20","2016-09-20","9","1","2","0","INV#IV221,Pax :ABDUL JALEEL, PassNo :P2516541/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1537","94","7","216","Visa Payable","14100","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV221,Pax :ABDUL JALEEL, PassNo :P2516541/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1538","7","94","216","Visa Payable","0","14100","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV221,Pax :ABDUL JALEEL, PassNo :P2516541/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1539","96","6","222","Visa Receivable","0","14700","Visa Receivable : 222","2016-09-20","2016-09-20","8","1","2","0","INV#IV222,Pax :SALMAN KHAN, PassNo :N7724337/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1540","6","96","222","Visa Receivable","14700","0","Visa Receivable : 222","2016-09-20","2016-09-20","8","1","2","0","INV#IV222,Pax :SALMAN KHAN, PassNo :N7724337/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1541","94","7","217","Visa Payable","14200","0","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV222,Pax :SALMAN KHAN, PassNo :N7724337/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1542","7","94","217","Visa Payable","0","14200","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV222,Pax :SALMAN KHAN, PassNo :N7724337/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1543","96","6","223","Visa Receivable","0","14700","Visa Receivable : 223","2016-09-20","2016-09-20","8","1","2","0","INV#IV223,Pax :SHAJAHAN KUDUNGARA HALLI, PassNo :N2498971/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1544","6","96","223","Visa Receivable","14700","0","Visa Receivable : 223","2016-09-20","2016-09-20","8","1","2","0","INV#IV223,Pax :SHAJAHAN KUDUNGARA HALLI, PassNo :N2498971/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1545","94","7","218","Visa Payable","14200","0","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV223,Pax :SHAJAHAN KUDUNGARA HALLI, PassNo :N2498971/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1546","7","94","218","Visa Payable","0","14200","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV223,Pax :SHAJAHAN KUDUNGARA HALLI, PassNo :N2498971/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1547","101","6","224","Visa Receivable","0","59200","Visa Receivable : 224","2016-09-20","2016-09-20","9","1","2","0","INV#IV224,Pax :PRADISHA, PassNo :L7914814/Pax :AYDIN, PassNo :L8284123/Pax :MANIYAMMAL, PassNo :L8208398/Pax :YEMI ABAN, PassNo :N9206672/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1548","6","101","224","Visa Receivable","59200","0","Visa Receivable : 224","2016-09-20","2016-09-20","9","1","2","0","INV#IV224,Pax :PRADISHA, PassNo :L7914814/Pax :AYDIN, PassNo :L8284123/Pax :MANIYAMMAL, PassNo :L8208398/Pax :YEMI ABAN, PassNo :N9206672/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1549","132","7","219","Visa Payable","58400","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV224,Pax :PRADISHA, PassNo :L7914814/Pax :AYDIN, PassNo :L8284123/Pax :MANIYAMMAL, PassNo :L8208398/Pax :YEMI ABAN, PassNo :N9206672/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1550","7","132","219","Visa Payable","0","58400","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV224,Pax :PRADISHA, PassNo :L7914814/Pax :AYDIN, PassNo :L8284123/Pax :MANIYAMMAL, PassNo :L8208398/Pax :YEMI ABAN, PassNo :N9206672/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1551","96","6","225","Visa Receivable","0","34500","Visa Receivable : 225","2016-09-20","2016-09-20","8","1","2","0","INV#IV225,Pax :MOHAMED ASLAM, PassNo :Z1980263/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1552","6","96","225","Visa Receivable","34500","0","Visa Receivable : 225","2016-09-20","2016-09-20","8","1","2","0","INV#IV225,Pax :MOHAMED ASLAM, PassNo :Z1980263/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1553","95","6","226","Visa Receivable","0","14700","Visa Receivable : 226","2016-09-20","2016-09-20","9","1","2","0","INV#IV226,Pax :JESNI, PassNo :N4797473/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1554","6","95","226","Visa Receivable","14700","0","Visa Receivable : 226","2016-09-20","2016-09-20","9","1","2","0","INV#IV226,Pax :JESNI, PassNo :N4797473/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1555","132","7","220","Visa Payable","14600","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV226,Pax :JESNI, PassNo :N4797473/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1556","7","132","220","Visa Payable","0","14600","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV226,Pax :JESNI, PassNo :N4797473/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1557","94","7","221","Visa Payable","33000","0","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV225,Pax :MOHAMED ASLAM, PassNo :Z1980263/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1558","7","94","221","Visa Payable","0","33000","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV225,Pax :MOHAMED ASLAM, PassNo :Z1980263/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1559","96","6","227","Visa Receivable","0","14700","Visa Receivable : 227","2016-09-20","2016-09-20","8","1","2","0","INV#IV227,Pax :MOHAMMED ATIF, PassNo :L6242738/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1560","6","96","227","Visa Receivable","14700","0","Visa Receivable : 227","2016-09-20","2016-09-20","8","1","2","0","INV#IV227,Pax :MOHAMMED ATIF, PassNo :L6242738/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1561","94","7","222","Visa Payable","14200","0","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV227,Pax :MOHAMMED ATIF, PassNo :L6242738/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1562","7","94","222","Visa Payable","0","14200","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV227,Pax :MOHAMMED ATIF, PassNo :L6242738/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1563","114","6","228","Visa Receivable","0","14500","Visa Receivable : 228","2016-09-20","2016-09-20","9","1","2","0","INV#IV228,Pax :SHAMNAS SALIM, PassNo :J5808373/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1564","6","114","228","Visa Receivable","14500","0","Visa Receivable : 228","2016-09-20","2016-09-20","9","1","2","0","INV#IV228,Pax :SHAMNAS SALIM, PassNo :J5808373/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1565","94","7","223","Visa Payable","14100","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV228,Pax :SHAMNAS SALIM, PassNo :J5808373/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1566","7","94","223","Visa Payable","0","14100","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV228,Pax :SHAMNAS SALIM, PassNo :J5808373/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1567","169","6","229","Visa Receivable","0","29000","Visa Receivable : 229","2016-09-20","2016-09-20","9","1","2","0","INV#IV229,Pax :SHERSHAD, PassNo :H2289981/Pax :MUHAMMED ASHIK, PassNo :G9707646/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1568","6","169","229","Visa Receivable","29000","0","Visa Receivable : 229","2016-09-20","2016-09-20","9","1","2","0","INV#IV229,Pax :SHERSHAD, PassNo :H2289981/Pax :MUHAMMED ASHIK, PassNo :G9707646/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1569","94","7","224","Visa Payable","28200","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV229,Pax :SHERSHAD, PassNo :H2289981/Pax :MUHAMMED ASHIK, PassNo :G9707646/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1570","7","94","224","Visa Payable","0","28200","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV229,Pax :SHERSHAD, PassNo :H2289981/Pax :MUHAMMED ASHIK, PassNo :G9707646/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1573","169","6","230","Visa Receivable","0","29000","Visa Receivable : 230","2016-09-20","2016-09-20","9","1","2","0","INV#IV230,Pax :SANTHA, PassNo :M6047012/Pax :HASNA, PassNo :K2449588/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1574","6","169","230","Visa Receivable","29000","0","Visa Receivable : 230","2016-09-20","2016-09-20","9","1","2","0","INV#IV230,Pax :SANTHA, PassNo :M6047012/Pax :HASNA, PassNo :K2449588/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1575","132","7","225","Visa Payable","29200","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV230,Pax :SANTHA, PassNo :M6047012/Pax :HASNA, PassNo :K2449588/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1576","7","132","225","Visa Payable","0","29200","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV230,Pax :SANTHA, PassNo :M6047012/Pax :HASNA, PassNo :K2449588/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1577","95","6","231","Visa Receivable","0","14600","Visa Receivable : 231","2016-09-20","2016-09-20","9","1","2","0","INV#IV231,Pax :NOUSHERE, PassNo :M3925390/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1578","6","95","231","Visa Receivable","14600","0","Visa Receivable : 231","2016-09-20","2016-09-20","9","1","2","0","INV#IV231,Pax :NOUSHERE, PassNo :M3925390/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1579","132","7","226","Visa Payable","14600","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV231,Pax :NOUSHERE, PassNo :M3925390/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1580","7","132","226","Visa Payable","0","14600","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV231,Pax :NOUSHERE, PassNo :M3925390/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1581","130","6","232","Visa Receivable","0","5100","Visa Receivable : 232","2016-09-20","2016-09-20","9","1","2","0","INV#IV232,Pax :NOUFAL, PassNo :L5479760/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1582","6","130","232","Visa Receivable","5100","0","Visa Receivable : 232","2016-09-20","2016-09-20","9","1","2","0","INV#IV232,Pax :NOUFAL, PassNo :L5479760/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1583","132","7","227","Visa Payable","5250","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV232,Pax :NOUFAL, PassNo :L5479760/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1584","7","132","227","Visa Payable","0","5250","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV232,Pax :NOUFAL, PassNo :L5479760/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1585","196","6","233","Visa Receivable","0","45000","Visa Receivable : 233","2016-09-20","2016-09-20","8","1","2","0","INV#IV233,Pax :MAJIDA , PassNo :N1847699 / visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1586","6","196","233","Visa Receivable","45000","0","Visa Receivable : 233","2016-09-20","2016-09-20","8","1","2","0","INV#IV233,Pax :MAJIDA , PassNo :N1847699 / visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1587","94","7","228","Visa Payable","42600","0","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV233,Pax :MAJIDA , PassNo :N1847699 / visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1588","7","94","228","Visa Payable","0","42600","","2016-09-20","2016-09-20","8","1","2","0","Details:INV#IV233,Pax :MAJIDA , PassNo :N1847699 / visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1589","95","6","234","Visa Receivable","0","43500","Visa Receivable : 234","2016-09-20","2016-09-20","9","1","2","0","INV#IV234,Pax :AJMAL, PassNo :K1961060/Pax :ASGAR, PassNo :H6100414/Pax :ASIF, PassNo :K036848/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1590","6","95","234","Visa Receivable","43500","0","Visa Receivable : 234","2016-09-20","2016-09-20","9","1","2","0","INV#IV234,Pax :AJMAL, PassNo :K1961060/Pax :ASGAR, PassNo :H6100414/Pax :ASIF, PassNo :K036848/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1591","94","7","229","Visa Payable","42300","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV234,Pax :AJMAL, PassNo :K1961060/Pax :ASGAR, PassNo :H6100414/Pax :ASIF, PassNo :K036848/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1592","7","94","229","Visa Payable","0","42300","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV234,Pax :AJMAL, PassNo :K1961060/Pax :ASGAR, PassNo :H6100414/Pax :ASIF, PassNo :K036848/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1593","114","6","235","Visa Receivable","0","14500","Visa Receivable : 235","2016-09-20","2016-09-20","9","1","2","0","INV#IV235,Pax :ANAS ABDUL MAJEED, PassNo :L7677299/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1594","6","114","235","Visa Receivable","14500","0","Visa Receivable : 235","2016-09-20","2016-09-20","9","1","2","0","INV#IV235,Pax :ANAS ABDUL MAJEED, PassNo :L7677299/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1595","132","7","230","Visa Payable","14600","0","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV235,Pax :ANAS ABDUL MAJEED, PassNo :L7677299/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1596","7","132","230","Visa Payable","0","14600","","2016-09-20","2016-09-20","9","1","2","0","Details:INV#IV235,Pax :ANAS ABDUL MAJEED, PassNo :L7677299/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1597","127","2","15","Cash Payments","0","750","INV#1, acc:ALTA CCJ,Opening:750","2016-09-20","2016-09-09","5","1","2","0","VouNo#CP11,INV# Paid By :Sidheeq To : ALTA CCJ,Details:");
INSERT INTO transaction VALUES("1598","2","127","15","Cash Payments","750","0","INV#1, acc:ALTA CCJ,Opening:750","2016-09-20","2016-09-09","5","1","2","0","VouNo#CP11,INV# Paid By :Sidheeq To : ALTA CCJ,Details:");
INSERT INTO transaction VALUES("1599","193","2","1","Cash Receipts","225000","0","Cash received from  Ticket account","2016-09-20","2016-09-20","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");
INSERT INTO transaction VALUES("1600","2","193","1","Cash Receipts","0","225000","Cash received from  Ticket account","2016-09-20","2016-09-20","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");
INSERT INTO transaction VALUES("1601","197","2","1","Cash Receipts","100000","0","visa recipt","2016-09-20","2016-09-20","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");
INSERT INTO transaction VALUES("1602","2","197","1","Cash Receipts","0","100000","visa recipt","2016-09-20","2016-09-20","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");
INSERT INTO transaction VALUES("1603","184","2","22","Cash Receipts","24000","0","","2016-09-21","2016-09-21","6","2","2","0","VouNo#CR 22,INV#IA29 Received By :Manu,Details:INV#IA29, Pax:MOHAMMED CHOORAPPULAKKAL, Passport No:, Sector:CCJ/KWI, Date:21-09-2016, Tkt:3XAZ47, PNR:3XAZ47, By:Manu");
INSERT INTO transaction VALUES("1604","2","184","22","Cash Receipts","0","24000","","2016-09-21","2016-09-21","6","2","2","0","VouNo#CR 22,INV#IA29 Received By :Manu,Details:INV#IA29, Pax:MOHAMMED CHOORAPPULAKKAL, Passport No:, Sector:CCJ/KWI, Date:21-09-2016, Tkt:3XAZ47, PNR:3XAZ47, By:Manu");
INSERT INTO transaction VALUES("1605","199","14","1","Misc Receivable","0","1800","Misc Receivable : 1","2016-09-21","2016-09-21","6","2","2","0","INV#IM1,Pax:NOUFIYA SHIRIN KARINGAPPARA,Narration:Passport Ser,By:Manu ");
INSERT INTO transaction VALUES("1606","14","199","1","Misc Receivable","1800","0","Misc Receivable : 1","2016-09-21","2016-09-21","6","2","2","0","INV#IM1,Pax:NOUFIYA SHIRIN KARINGAPPARA,Narration:Passport Ser,By:Manu ");
INSERT INTO transaction VALUES("1607","198","15","1","Misc Payable","1650","0","","2016-09-21","2016-09-21","6","2","2","0","Details:INV#IM1,Pax:NOUFIYA SHIRIN KARINGAPPARA,Narration:Passport Ser,By:Manu ");
INSERT INTO transaction VALUES("1608","15","198","1","Misc Payable","0","1650","","2016-09-21","2016-09-21","6","2","2","0","Details:INV#IM1,Pax:NOUFIYA SHIRIN KARINGAPPARA,Narration:Passport Ser,By:Manu ");
INSERT INTO transaction VALUES("1609","200","2","10","Cash Payments","0","870","","2016-09-21","2016-09-21","6","2","2","0","VouNo#CP10,INV# Paid By :Manu To : KSEB,Details:");
INSERT INTO transaction VALUES("1610","2","200","10","Cash Payments","870","0","","2016-09-21","2016-09-21","6","2","2","0","VouNo#CP10,INV# Paid By :Manu To : KSEB,Details:");
INSERT INTO transaction VALUES("1611","202","2","16","Cash Payments","0","14500","LESS 1 LEAV","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : sahad salary,Details:");
INSERT INTO transaction VALUES("1612","2","202","16","Cash Payments","14500","0","LESS 1 LEAV","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : sahad salary,Details:");
INSERT INTO transaction VALUES("1613","201","2","17","Cash Payments","0","13500","LESS 3 LEAV","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : safuvan salary,Details:");
INSERT INTO transaction VALUES("1614","2","201","17","Cash Payments","13500","0","LESS 3 LEAV","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : safuvan salary,Details:");
INSERT INTO transaction VALUES("1615","203","2","18","Cash Payments","0","8600","LESS 1.5 LEAV","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : IRFAD SALERY,Details:");
INSERT INTO transaction VALUES("1616","2","203","18","Cash Payments","8600","0","LESS 1.5 LEAV","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : IRFAD SALERY,Details:");
INSERT INTO transaction VALUES("1617","202","2","19","Cash Payments","0","7500","BAKREED BONES","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : sahad salary,Details:");
INSERT INTO transaction VALUES("1618","2","202","19","Cash Payments","7500","0","BAKREED BONES","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : sahad salary,Details:");
INSERT INTO transaction VALUES("1619","201","2","20","Cash Payments","0","7500","BAKREED BONES","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : safuvan salary,Details:");
INSERT INTO transaction VALUES("1620","2","201","20","Cash Payments","7500","0","BAKREED BONES","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : safuvan salary,Details:");
INSERT INTO transaction VALUES("1621","203","2","21","Cash Payments","0","4500","BAKREED BONES","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : IRFAD SALERY,Details:");
INSERT INTO transaction VALUES("1622","2","203","21","Cash Payments","4500","0","BAKREED BONES","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : IRFAD SALERY,Details:");
INSERT INTO transaction VALUES("1623","204","2","22","Cash Payments","0","8000","","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : BODHI SOLUTIION,Details:");
INSERT INTO transaction VALUES("1624","2","204","22","Cash Payments","8000","0","","2016-09-21","2016-09-21","33","1","2","0","VouNo#CP11,INV# Paid By :Faizal To : BODHI SOLUTIION,Details:");
INSERT INTO transaction VALUES("1625","205","2","11","Cash Payments","0","580","ARVADI SIYARA PKG","2016-09-21","2016-09-21","6","2","2","0","VouNo#CP10,INV# Paid By :Manu To : SIRAJ  Newspaper,Details:");
INSERT INTO transaction VALUES("1626","2","205","11","Cash Payments","580","0","ARVADI SIYARA PKG","2016-09-21","2016-09-21","6","2","2","0","VouNo#CP10,INV# Paid By :Manu To : SIRAJ  Newspaper,Details:");
INSERT INTO transaction VALUES("1627","148","6","94","Visa Receivable","0","6300","Visa Receivable : 94","2016-09-21","2016-09-08","8","1","2","0","INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA, By:Safuvan");
INSERT INTO transaction VALUES("1628","6","148","94","Visa Receivable","6300","0","Visa Receivable : 94","2016-09-21","2016-09-08","8","1","2","0","INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA, By:Safuvan");
INSERT INTO transaction VALUES("1629","97","7","91","Visa Payable","6000","0","","2016-09-21","2016-09-08","8","1","2","0","INV#91,sup:97,:against:94,Details:INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA, By:Safuvan");
INSERT INTO transaction VALUES("1630","7","97","91","Visa Payable","0","6000","","2016-09-21","2016-09-08","8","1","2","0","INV#91,sup:97,:against:94,Details:INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA, By:Safuvan");
INSERT INTO transaction VALUES("1631","199","2","23","Cash Receipts","1800","0","","2016-09-21","2016-09-21","6","2","2","0","VouNo#CR 23,INV#IM1 Received By :Manu,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("1632","2","199","23","Cash Receipts","0","1800","","2016-09-21","2016-09-21","6","2","2","0","VouNo#CR 23,INV#IM1 Received By :Manu,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("1633","151","6","236","Visa Receivable","0","6200","Visa Receivable : 236","2016-09-21","2016-09-21","8","1","2","0","INV#IV236,Pax :NAVAB, PassNo :P2198094/ visaFor:30 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1634","6","151","236","Visa Receivable","6200","0","Visa Receivable : 236","2016-09-21","2016-09-21","8","1","2","0","INV#IV236,Pax :NAVAB, PassNo :P2198094/ visaFor:30 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1635","97","7","231","Visa Payable","5500","0","","2016-09-21","2016-09-21","8","1","2","0","Details:INV#IV236,Pax :NAVAB, PassNo :P2198094/ visaFor:30 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1636","7","97","231","Visa Payable","0","5500","","2016-09-21","2016-09-21","8","1","2","0","Details:INV#IV236,Pax :NAVAB, PassNo :P2198094/ visaFor:30 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1639","151","2","26","Cash Receipts","3000","0","ADVANCE PAYMENT","2016-09-21","2016-09-21","15","1","2","0","VouNo#CR 26,INV#IV236 Received By :Akhil,Details:INV#IV236,Pax :NAVAB, PassNo :P2198094/ visaFor:30 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1640","2","151","26","Cash Receipts","0","3000","ADVANCE PAYMENT","2016-09-21","2016-09-21","15","1","2","0","VouNo#CR 26,INV#IV236 Received By :Akhil,Details:INV#IV236,Pax :NAVAB, PassNo :P2198094/ visaFor:30 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1641","198","2","12","Cash Payments","0","1650","","2016-09-21","2016-09-21","6","2","2","0","VouNo#CP10,INV#PM1 Paid By :Manu To : AKSHAYA PUTHANATHANI,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("1642","2","198","12","Cash Payments","1650","0","","2016-09-21","2016-09-21","6","2","2","0","VouNo#CP10,INV#PM1 Paid By :Manu To : AKSHAYA PUTHANATHANI,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("1643","148","2","27","Cash Receipts","2500","0","","2016-09-21","2016-09-21","33","1","2","0","VouNo#CR 27,INV#IV94 Received By :Faizal,Details:INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA, By:Safuvan");
INSERT INTO transaction VALUES("1644","2","148","27","Cash Receipts","0","2500","","2016-09-21","2016-09-21","33","1","2","0","VouNo#CR 27,INV#IV94 Received By :Faizal,Details:INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA, By:Safuvan");
INSERT INTO transaction VALUES("1645","206","2","13","Cash Payments","0","750","","2016-09-21","2016-09-21","6","2","2","0","VouNo#CP11,INV# Paid By :Manu To : BSNL,Details:");
INSERT INTO transaction VALUES("1646","2","206","13","Cash Payments","750","0","","2016-09-21","2016-09-21","6","2","2","0","VouNo#CP11,INV# Paid By :Manu To : BSNL,Details:");
INSERT INTO transaction VALUES("1647","129","6","90","Visa Receivable","0","11100","Visa Receivable : 90","2016-09-21","2016-09-08","10","1","2","0","INV#IV90,Pax :1196	MUHAMMED ALI, PassNo :K6459585/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("1648","6","129","90","Visa Receivable","11100","0","Visa Receivable : 90","2016-09-21","2016-09-08","10","1","2","0","INV#IV90,Pax :1196	MUHAMMED ALI, PassNo :K6459585/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("1649","125","7","88","Visa Payable","11100","0","","2016-09-21","2016-09-08","10","1","2","0","INV#88,sup:125,:against:90,Details:INV#IV90,Pax :1196	MUHAMMED ALI, PassNo :K6459585/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("1650","7","125","88","Visa Payable","0","11100","","2016-09-21","2016-09-08","10","1","2","0","INV#88,sup:125,:against:90,Details:INV#IV90,Pax :1196	MUHAMMED ALI, PassNo :K6459585/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("1653","129","185","17","Bank Receipts","45500","0","","2016-09-21","2016-09-15","33","1","2","0","VouNo#BR17,INV#IV25,IV27,IV36,IV91,IV97 Received By :Faizal,Details:INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1654","185","129","17","Bank Receipts","0","45500","","2016-09-21","2016-09-15","33","1","2","0","VouNo#BR17,INV#IV25,IV27,IV36,IV91,IV97 Received By :Faizal,Details:INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1655","177","2","28","Cash Receipts","25000","0","Advance Sakeena, Subaida","2016-09-21","2016-09-21","36","1","2","0","VouNo#CR 28,INV#IP1 Received By :Siraj Saquafi,Details:INV#IP1,Pac:Umra ,Pax:MUHAMMED KOYA CHEELANGATTU M 63 PassNo-N9062177,AYISHABI PALLITHAZHATU F 62 PassNo-N9063084 ,By:Siraj Saquafi");
INSERT INTO transaction VALUES("1656","2","177","28","Cash Receipts","0","25000","Advance Sakeena, Subaida","2016-09-21","2016-09-21","36","1","2","0","VouNo#CR 28,INV#IP1 Received By :Siraj Saquafi,Details:INV#IP1,Pac:Umra ,Pax:MUHAMMED KOYA CHEELANGATTU M 63 PassNo-N9062177,AYISHABI PALLITHAZHATU F 62 PassNo-N9063084 ,By:Siraj Saquafi");
INSERT INTO transaction VALUES("1657","94","185","8","Bank Payments","0","800000","","2016-09-21","2016-09-21","33","1","2","0","VouNo#BP8,INV#PV111,PV112,PV113,PV118,PV120,PV122,PV123,PV124,PV126,PV128,PV130,PV132,PV134,PV136,PV139,PV141,PV144,PV145,PV146,PV147,PV148,PV149,PV152,PV154,PV155,PV156,PV159,PV160,PV161,PV163,PV164,PV165,PV167,PV168 Paid By :Faizal To : SALIL PURCHASE,Details:INV#IV170,Pax :MOHAMMED JIHAD, PassNo :N1597279/Pax :FAHIS, PassNo :P2332064/Pax :FARSANA, PassNo :H5552428/Pax :HANA FATHIMA, PassNo :L3650539/Pax :ZAMEEL, PassNo :L8267861/Pax :KHADEEJA, PassNo :J2671347/Pax :JUSTIN, PassNo :N2088987/Pax :RASHEED, PassNo :M3712539/Pax :SUBAIDA, PassNo :L2355344/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1658","185","94","8","Bank Payments","800000","0","","2016-09-21","2016-09-21","33","1","2","0","VouNo#BP8,INV#PV111,PV112,PV113,PV118,PV120,PV122,PV123,PV124,PV126,PV128,PV130,PV132,PV134,PV136,PV139,PV141,PV144,PV145,PV146,PV147,PV148,PV149,PV152,PV154,PV155,PV156,PV159,PV160,PV161,PV163,PV164,PV165,PV167,PV168 Paid By :Faizal To : SALIL PURCHASE,Details:INV#IV170,Pax :MOHAMMED JIHAD, PassNo :N1597279/Pax :FAHIS, PassNo :P2332064/Pax :FARSANA, PassNo :H5552428/Pax :HANA FATHIMA, PassNo :L3650539/Pax :ZAMEEL, PassNo :L8267861/Pax :KHADEEJA, PassNo :J2671347/Pax :JUSTIN, PassNo :N2088987/Pax :RASHEED, PassNo :M3712539/Pax :SUBAIDA, PassNo :L2355344/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1659","130","185","18","Bank Receipts","50000","0","","2016-09-21","2016-09-20","33","1","2","0","VouNo#BR18,INV#IV32,IV41,IV57,IV83,IV84,IV125,IV141 Received By :Faizal,Details:INV#IV141,Pax :ABDUL NASEER, PassNo :K9823040/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1660","185","130","18","Bank Receipts","0","50000","","2016-09-21","2016-09-20","33","1","2","0","VouNo#BR18,INV#IV32,IV41,IV57,IV83,IV84,IV125,IV141 Received By :Faizal,Details:INV#IV141,Pax :ABDUL NASEER, PassNo :K9823040/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1661","151","6","237","Visa Receivable","0","17400","Visa Receivable : 237","2016-09-21","2016-09-21","8","1","2","0","INV#IV237,Pax :khalid , PassNo :K2266285 / visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1662","6","151","237","Visa Receivable","17400","0","Visa Receivable : 237","2016-09-21","2016-09-21","8","1","2","0","INV#IV237,Pax :khalid , PassNo :K2266285 / visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1663","94","7","232","Visa Payable","15600","0","","2016-09-21","2016-09-21","8","1","2","0","Details:INV#IV237,Pax :khalid , PassNo :K2266285 / visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1664","7","94","232","Visa Payable","0","15600","","2016-09-21","2016-09-21","8","1","2","0","Details:INV#IV237,Pax :khalid , PassNo :K2266285 / visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1665","151","2","29","Cash Receipts","17400","0","","2016-09-21","2016-09-21","33","1","2","0","VouNo#CR 29,INV#IV237 Received By :Faizal,Details:INV#IV237,Pax :khalid , PassNo :K2266285 / visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1666","2","151","29","Cash Receipts","0","17400","","2016-09-21","2016-09-21","33","1","2","0","VouNo#CR 29,INV#IV237 Received By :Faizal,Details:INV#IV237,Pax :khalid , PassNo :K2266285 / visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("1667","169","185","19","Bank Receipts","300000","0","","2016-09-21","2016-09-21","33","1","2","0","VouNo#BR19,INV#IV127,IV128,IV129,IV130,IV131,IV132,IV133,IV135,IV136 Received By :Faizal,Details:INV#IV136,Pax :AFSAL ALI, PassNo :J8837024/Pax :VINI JOSE, PassNo :H9689436/Pax :RASHEED, PassNo :N9138711/Pax :MIDHLAJ, PassNo :L7920538/Pax :MOHAMMED LIBASIN, PassNo :H7232734/Pax :HYDERALI, PassNo :P4052692/Pax :ABDUL LATHEEF, PassNo :M2403126/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1668","185","169","19","Bank Receipts","0","300000","","2016-09-21","2016-09-21","33","1","2","0","VouNo#BR19,INV#IV127,IV128,IV129,IV130,IV131,IV132,IV133,IV135,IV136 Received By :Faizal,Details:INV#IV136,Pax :AFSAL ALI, PassNo :J8837024/Pax :VINI JOSE, PassNo :H9689436/Pax :RASHEED, PassNo :N9138711/Pax :MIDHLAJ, PassNo :L7920538/Pax :MOHAMMED LIBASIN, PassNo :H7232734/Pax :HYDERALI, PassNo :P4052692/Pax :ABDUL LATHEEF, PassNo :M2403126/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1679","208","6","239","Visa Receivable","0","300","Visa Receivable : 239","2016-09-21","2016-09-21","10","1","2","0","INV#IV239,Pax :HAMEED, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1680","6","208","239","Visa Receivable","300","0","Visa Receivable : 239","2016-09-21","2016-09-21","10","1","2","0","INV#IV239,Pax :HAMEED, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1681","97","7","234","Visa Payable","300","0","","2016-09-21","2016-09-21","10","1","2","0","Details:INV#IV239,Pax :HAMEED, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1682","7","97","234","Visa Payable","0","300","","2016-09-21","2016-09-21","10","1","2","0","Details:INV#IV239,Pax :HAMEED, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("1683","208","2","30","Cash Receipts","2500","0","ADVANCE PAYMENT","2016-09-21","2016-09-21","15","1","2","0","VouNo#CR 30,INV#IV238 Received By :Akhil,Details:INV#IV238,Pax :HAMEED OTTAPILAVULLATHIL, PassNo :L28333806/ visaFor:30 DAYS DXB WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("1684","2","208","30","Cash Receipts","0","2500","ADVANCE PAYMENT","2016-09-21","2016-09-21","15","1","2","0","VouNo#CR 30,INV#IV238 Received By :Akhil,Details:INV#IV238,Pax :HAMEED OTTAPILAVULLATHIL, PassNo :L28333806/ visaFor:30 DAYS DXB WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("1685","208","6","238","Visa Receivable","0","5200","Visa Receivable : 238","2016-09-21","2016-09-21","10","1","2","0","INV#IV238,Pax :HAMEED OTTAPILAVULLATHIL, PassNo :L28333806/ visaFor:30 DAYS DXB LSSUER, By:Irfad");
INSERT INTO transaction VALUES("1686","6","208","238","Visa Receivable","5200","0","Visa Receivable : 238","2016-09-21","2016-09-21","10","1","2","0","INV#IV238,Pax :HAMEED OTTAPILAVULLATHIL, PassNo :L28333806/ visaFor:30 DAYS DXB LSSUER, By:Irfad");
INSERT INTO transaction VALUES("1687","94","7","233","Visa Payable","5100","0","","2016-09-21","2016-09-21","10","1","2","0","INV#233,sup:94,:against:238,Details:INV#IV238,Pax :HAMEED OTTAPILAVULLATHIL, PassNo :L28333806/ visaFor:30 DAYS DXB LSSUER, By:Irfad");
INSERT INTO transaction VALUES("1688","7","94","233","Visa Payable","0","5100","","2016-09-21","2016-09-21","10","1","2","0","INV#233,sup:94,:against:238,Details:INV#IV238,Pax :HAMEED OTTAPILAVULLATHIL, PassNo :L28333806/ visaFor:30 DAYS DXB LSSUER, By:Irfad");
INSERT INTO transaction VALUES("1689","114","6","240","Visa Receivable","0","14500","Visa Receivable : 240","2016-09-22","2016-09-22","9","1","2","0","INV#IV240,Pax :MUHAMMED MALIK, PassNo :N0175503/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1690","6","114","240","Visa Receivable","14500","0","Visa Receivable : 240","2016-09-22","2016-09-22","9","1","2","0","INV#IV240,Pax :MUHAMMED MALIK, PassNo :N0175503/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1691","132","7","235","Visa Payable","14600","0","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV240,Pax :MUHAMMED MALIK, PassNo :N0175503/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1692","7","132","235","Visa Payable","0","14600","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV240,Pax :MUHAMMED MALIK, PassNo :N0175503/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1693","114","6","241","Visa Receivable","0","14500","Visa Receivable : 241","2016-09-22","2016-09-22","9","1","2","0","INV#IV241,Pax :SREEJA, PassNo :P0625401/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1694","6","114","241","Visa Receivable","14500","0","Visa Receivable : 241","2016-09-22","2016-09-22","9","1","2","0","INV#IV241,Pax :SREEJA, PassNo :P0625401/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1695","132","7","236","Visa Payable","14600","0","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV241,Pax :SREEJA, PassNo :P0625401/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1696","7","132","236","Visa Payable","0","14600","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV241,Pax :SREEJA, PassNo :P0625401/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1697","169","6","242","Visa Receivable","0","15300","Visa Receivable : 242","2016-09-22","2016-09-22","9","1","2","0","INV#IV242,Pax :SAID HUMAM, PassNo :L8905859/Pax :SHAFEER, PassNo :L1309942/Pax :ABIN THOMAS, PassNo :N9065681/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1698","6","169","242","Visa Receivable","15300","0","Visa Receivable : 242","2016-09-22","2016-09-22","9","1","2","0","INV#IV242,Pax :SAID HUMAM, PassNo :L8905859/Pax :SHAFEER, PassNo :L1309942/Pax :ABIN THOMAS, PassNo :N9065681/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1699","94","7","237","Visa Payable","15225","0","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV242,Pax :SAID HUMAM, PassNo :L8905859/Pax :SHAFEER, PassNo :L1309942/Pax :ABIN THOMAS, PassNo :N9065681/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1700","7","94","237","Visa Payable","0","15225","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV242,Pax :SAID HUMAM, PassNo :L8905859/Pax :SHAFEER, PassNo :L1309942/Pax :ABIN THOMAS, PassNo :N9065681/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1701","101","6","243","Visa Receivable","0","14800","Visa Receivable : 243","2016-09-22","2016-09-22","9","1","2","0","INV#IV243,Pax :SHAMSUDHEEN, PassNo :L9685743/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1702","6","101","243","Visa Receivable","14800","0","Visa Receivable : 243","2016-09-22","2016-09-22","9","1","2","0","INV#IV243,Pax :SHAMSUDHEEN, PassNo :L9685743/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1703","132","7","238","Visa Payable","14600","0","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV243,Pax :SHAMSUDHEEN, PassNo :L9685743/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1704","7","132","238","Visa Payable","0","14600","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV243,Pax :SHAMSUDHEEN, PassNo :L9685743/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1705","129","6","244","Visa Receivable","0","14800","Visa Receivable : 244","2016-09-22","2016-09-22","9","1","2","0","INV#IV244,Pax :SEENATH, PassNo :P1506921/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1706","6","129","244","Visa Receivable","14800","0","Visa Receivable : 244","2016-09-22","2016-09-22","9","1","2","0","INV#IV244,Pax :SEENATH, PassNo :P1506921/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1707","132","7","239","Visa Payable","14600","0","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV244,Pax :SEENATH, PassNo :P1506921/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1708","7","132","239","Visa Payable","0","14600","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV244,Pax :SEENATH, PassNo :P1506921/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1709","142","6","245","Visa Receivable","0","14700","Visa Receivable : 245","2016-09-22","2016-09-22","9","1","2","0","INV#IV245,Pax :DADU, PassNo :K5287361/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1710","6","142","245","Visa Receivable","14700","0","Visa Receivable : 245","2016-09-22","2016-09-22","9","1","2","0","INV#IV245,Pax :DADU, PassNo :K5287361/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1711","94","7","240","Visa Payable","14100","0","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV245,Pax :DADU, PassNo :K5287361/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1712","7","94","240","Visa Payable","0","14100","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV245,Pax :DADU, PassNo :K5287361/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1713","209","6","246","Visa Receivable","0","15800","Visa Receivable : 246","2016-09-22","2016-09-22","9","1","2","0","INV#IV246,Pax :NAJEEB, PassNo :H0537872/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1714","6","209","246","Visa Receivable","15800","0","Visa Receivable : 246","2016-09-22","2016-09-22","9","1","2","0","INV#IV246,Pax :NAJEEB, PassNo :H0537872/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1715","132","7","241","Visa Payable","15600","0","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV246,Pax :NAJEEB, PassNo :H0537872/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1716","7","132","241","Visa Payable","0","15600","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV246,Pax :NAJEEB, PassNo :H0537872/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("1717","209","6","247","Visa Receivable","0","14600","Visa Receivable : 247","2016-09-22","2016-09-22","9","1","2","0","INV#IV247,Pax :SAYYID MUHAMMED JIFRI, PassNo :G3872348/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1718","6","209","247","Visa Receivable","14600","0","Visa Receivable : 247","2016-09-22","2016-09-22","9","1","2","0","INV#IV247,Pax :SAYYID MUHAMMED JIFRI, PassNo :G3872348/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1719","94","7","242","Visa Payable","14100","0","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV247,Pax :SAYYID MUHAMMED JIFRI, PassNo :G3872348/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1720","7","94","242","Visa Payable","0","14100","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV247,Pax :SAYYID MUHAMMED JIFRI, PassNo :G3872348/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1721","95","6","248","Visa Receivable","0","29200","Visa Receivable : 248","2016-09-22","2016-09-22","9","1","2","0","INV#IV248,Pax :SHABIN, PassNo :N6825621/Pax :NAHAS, PassNo :H6098882/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1722","6","95","248","Visa Receivable","29200","0","Visa Receivable : 248","2016-09-22","2016-09-22","9","1","2","0","INV#IV248,Pax :SHABIN, PassNo :N6825621/Pax :NAHAS, PassNo :H6098882/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1723","94","7","243","Visa Payable","28200","0","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV248,Pax :SHABIN, PassNo :N6825621/Pax :NAHAS, PassNo :H6098882/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1724","7","94","243","Visa Payable","0","28200","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV248,Pax :SHABIN, PassNo :N6825621/Pax :NAHAS, PassNo :H6098882/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1725","210","4","32","Airline Receivable","0","4200","Airline Receivable: 32","2016-09-22","2016-09-22","6","2","2","0","INV#IA32, Pax:CHEMMUKKAN, NOUFIR, Passport No:, Sector:CCJ/AUH, Date:13-10-2016, Tkt:QSTU3I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1726","4","210","32","Airline Receivable","4200","0","Airline Receivable: 32","2016-09-22","2016-09-22","6","2","2","0","INV#IA32, Pax:CHEMMUKKAN, NOUFIR, Passport No:, Sector:CCJ/AUH, Date:13-10-2016, Tkt:QSTU3I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1727","81","5","32","Airline Payable","3980","0","","2016-09-22","2016-09-22","6","2","2","0","Details:INV#IA32, Pax:CHEMMUKKAN, NOUFIR, Passport No:, Sector:CCJ/AUH, Date:13-10-2016, Tkt:QSTU3I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1728","5","81","32","Airline Payable","0","3980","","2016-09-22","2016-09-22","6","2","2","0","Details:INV#IA32, Pax:CHEMMUKKAN, NOUFIR, Passport No:, Sector:CCJ/AUH, Date:13-10-2016, Tkt:QSTU3I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1729","165","6","249","Visa Receivable","0","14600","Visa Receivable : 249","2016-09-22","2016-09-22","9","1","2","0","INV#IV249,Pax :AMAL PARATH, PassNo :P039069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1730","6","165","249","Visa Receivable","14600","0","Visa Receivable : 249","2016-09-22","2016-09-22","9","1","2","0","INV#IV249,Pax :AMAL PARATH, PassNo :P039069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1731","132","7","244","Visa Payable","14600","0","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV249,Pax :AMAL PARATH, PassNo :P039069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1732","7","132","244","Visa Payable","0","14600","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV249,Pax :AMAL PARATH, PassNo :P039069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1733","210","4","33","Airline Receivable","0","12700","Airline Receivable: 33","2016-09-22","2016-09-22","6","2","2","0","INV#IA33, Pax:SHAMSIYA KADAKKADAN &  INF , Passport No:, Sector:CCJ/AUH, Date:13-10-2016, Tkt:4KR1B7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1734","4","210","33","Airline Receivable","12700","0","Airline Receivable: 33","2016-09-22","2016-09-22","6","2","2","0","INV#IA33, Pax:SHAMSIYA KADAKKADAN &  INF , Passport No:, Sector:CCJ/AUH, Date:13-10-2016, Tkt:4KR1B7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1735","81","5","33","Airline Payable","12250","0","","2016-09-22","2016-09-22","6","2","2","0","Details:INV#IA33, Pax:SHAMSIYA KADAKKADAN &  INF , Passport No:, Sector:CCJ/AUH, Date:13-10-2016, Tkt:4KR1B7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1736","5","81","33","Airline Payable","0","12250","","2016-09-22","2016-09-22","6","2","2","0","Details:INV#IA33, Pax:SHAMSIYA KADAKKADAN &  INF , Passport No:, Sector:CCJ/AUH, Date:13-10-2016, Tkt:4KR1B7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1737","210","4","34","Airline Receivable","0","15200","Airline Receivable: 34","2016-09-22","2016-09-22","6","2","2","0","INV#IA34, Pax:MOHAMED SAIFE KALODI, Passport No:, Sector:CCJ/DXB, Date:27-09-2016, Tkt:VH1BIE, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1738","4","210","34","Airline Receivable","15200","0","Airline Receivable: 34","2016-09-22","2016-09-22","6","2","2","0","INV#IA34, Pax:MOHAMED SAIFE KALODI, Passport No:, Sector:CCJ/DXB, Date:27-09-2016, Tkt:VH1BIE, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1739","81","5","34","Airline Payable","14950","0","","2016-09-22","2016-09-22","6","2","2","0","Details:INV#IA34, Pax:MOHAMED SAIFE KALODI, Passport No:, Sector:CCJ/DXB, Date:27-09-2016, Tkt:VH1BIE, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1740","5","81","34","Airline Payable","0","14950","","2016-09-22","2016-09-22","6","2","2","0","Details:INV#IA34, Pax:MOHAMED SAIFE KALODI, Passport No:, Sector:CCJ/DXB, Date:27-09-2016, Tkt:VH1BIE, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1741","211","4","35","Airline Receivable","0","15600","Airline Receivable: 35","2016-09-22","2016-09-22","6","2","2","0","INV#IA35, Pax:ISMAIL PARAMBATH, Passport No:, Sector:CCJ/SHJ, Date:27-09-2016, Tkt:S1LUMS, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1742","4","211","35","Airline Receivable","15600","0","Airline Receivable: 35","2016-09-22","2016-09-22","6","2","2","0","INV#IA35, Pax:ISMAIL PARAMBATH, Passport No:, Sector:CCJ/SHJ, Date:27-09-2016, Tkt:S1LUMS, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1743","81","5","35","Airline Payable","15470","0","","2016-09-22","2016-09-22","6","2","2","0","Details:INV#IA35, Pax:ISMAIL PARAMBATH, Passport No:, Sector:CCJ/SHJ, Date:27-09-2016, Tkt:S1LUMS, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1744","5","81","35","Airline Payable","0","15470","","2016-09-22","2016-09-22","6","2","2","0","Details:INV#IA35, Pax:ISMAIL PARAMBATH, Passport No:, Sector:CCJ/SHJ, Date:27-09-2016, Tkt:S1LUMS, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1745","129","6","250","Visa Receivable","0","14700","Visa Receivable : 250","2016-09-22","2016-09-22","9","1","2","0","INV#IV250,Pax :MUHABIR, PassNo :M6486378/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1746","6","129","250","Visa Receivable","14700","0","Visa Receivable : 250","2016-09-22","2016-09-22","9","1","2","0","INV#IV250,Pax :MUHABIR, PassNo :M6486378/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1747","94","7","245","Visa Payable","14100","0","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV250,Pax :MUHABIR, PassNo :M6486378/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1748","7","94","245","Visa Payable","0","14100","","2016-09-22","2016-09-22","9","1","2","0","Details:INV#IV250,Pax :MUHABIR, PassNo :M6486378/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1749","210","2","24","Cash Receipts","32100","0","","2016-09-22","2016-09-22","6","2","2","0","VouNo#CR 24,INV#IA32,IA33,IA34 Received By :Manu,Details:INV#IA34, Pax:MOHAMED SAIFE KALODI, Passport No:, Sector:CCJ/DXB, Date:27-09-2016, Tkt:VH1BIE, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1750","2","210","24","Cash Receipts","0","32100","","2016-09-22","2016-09-22","6","2","2","0","VouNo#CR 24,INV#IA32,IA33,IA34 Received By :Manu,Details:INV#IA34, Pax:MOHAMED SAIFE KALODI, Passport No:, Sector:CCJ/DXB, Date:27-09-2016, Tkt:VH1BIE, PNR:0, By:Manu");
INSERT INTO transaction VALUES("1751","142","6","189","Visa Receivable","0","70200","Visa Receivable : 189","2016-09-22","2016-09-20","10","1","2","0","INV#IV189,Pax :COUNTER:65 78 79 91 94 95 100 119 138 226 228 333 806 878 1162 1265, PassNo :k000000/ visaFor:Dubai- 30 days, By:Irfad");
INSERT INTO transaction VALUES("1752","6","142","189","Visa Receivable","70200","0","Visa Receivable : 189","2016-09-22","2016-09-20","10","1","2","0","INV#IV189,Pax :COUNTER:65 78 79 91 94 95 100 119 138 226 228 333 806 878 1162 1265, PassNo :k000000/ visaFor:Dubai- 30 days, By:Irfad");
INSERT INTO transaction VALUES("1753","125","7","186","Visa Payable","70200","0","","2016-09-22","2016-09-20","10","1","2","0","INV#186,sup:125,:against:189,Details:INV#IV189,Pax :COUNTER:65 78 79 91 94 95 100 119 138 226 228 333 806 878 1162 1265, PassNo :k000000/ visaFor:Dubai- 30 days, By:Irfad");
INSERT INTO transaction VALUES("1754","7","125","186","Visa Payable","0","70200","","2016-09-22","2016-09-20","10","1","2","0","INV#186,sup:125,:against:189,Details:INV#IV189,Pax :COUNTER:65 78 79 91 94 95 100 119 138 226 228 333 806 878 1162 1265, PassNo :k000000/ visaFor:Dubai- 30 days, By:Irfad");
INSERT INTO transaction VALUES("1755","177","8","2","Package Receivable","0","125000","Invoice Package: 2","2016-09-22","2016-09-22","36","1","2","2","INV#IP2,Pac:Umra ,Pax:SAKEENA NALUKUDI PARAMBIL F 42 PassNo-L7056798,SUBAIDA IBRAHIM F 54 PassNo-N9467657 ,By:Siraj Saquafi");
INSERT INTO transaction VALUES("1756","8","177","2","Package Receivable","125000","0","Invoice Package: 2","2016-09-22","2016-09-22","36","1","2","2","INV#IP2,Pac:Umra ,Pax:SAKEENA NALUKUDI PARAMBIL F 42 PassNo-L7056798,SUBAIDA IBRAHIM F 54 PassNo-N9467657 ,By:Siraj Saquafi");
INSERT INTO transaction VALUES("1757","151","6","251","Visa Receivable","0","22000","Visa Receivable : 251","2016-09-22","2016-09-22","8","1","2","0","INV#IV251,Pax :HASSAINAR, PassNo :H63896913/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1758","6","151","251","Visa Receivable","22000","0","Visa Receivable : 251","2016-09-22","2016-09-22","8","1","2","0","INV#IV251,Pax :HASSAINAR, PassNo :H63896913/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1763","97","7","247","Visa Payable","19000","0","","2016-09-22","2016-09-22","8","1","2","0","Details:INV#IV252,Pax :HASSAINAR, PassNo :H63896913/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1764","7","97","247","Visa Payable","0","19000","","2016-09-22","2016-09-22","8","1","2","0","Details:INV#IV252,Pax :HASSAINAR, PassNo :H63896913/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1765","151","2","31","Cash Receipts","2000","0","ADV ON  QATAR VISA","2016-09-22","2016-09-22","33","1","2","0","VouNo#CR 31,INV#IV252 Received By :Faizal,Details:INV#IV252,Pax :HASSAINAR, PassNo :H63896913/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1766","2","151","31","Cash Receipts","0","2000","ADV ON  QATAR VISA","2016-09-22","2016-09-22","33","1","2","0","VouNo#CR 31,INV#IV252 Received By :Faizal,Details:INV#IV252,Pax :HASSAINAR, PassNo :H63896913/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan");
INSERT INTO transaction VALUES("1767","151","6","252","Visa Receivable","0","22000","Visa Receivable : 252","2016-09-22","2016-09-22","8","1","2","0","INV#IV252,Pax :HASSAINAR, PassNo :H63896913/ visaFor:90 DAYS QATAR BUSINESS, By:Safuvan");
INSERT INTO transaction VALUES("1768","6","151","252","Visa Receivable","22000","0","Visa Receivable : 252","2016-09-22","2016-09-22","8","1","2","0","INV#IV252,Pax :HASSAINAR, PassNo :H63896913/ visaFor:90 DAYS QATAR BUSINESS, By:Safuvan");
INSERT INTO transaction VALUES("1769","99","6","218","Visa Receivable","0","6200","Visa Receivable : 218","2016-09-22","2016-09-20","9","1","2","0","INV#IV218,Pax :RAVEENDRAN, PassNo :K9336948/ visaFor:30 DAYS DXB SETTING, By:Sahad");
INSERT INTO transaction VALUES("1770","6","99","218","Visa Receivable","6200","0","Visa Receivable : 218","2016-09-22","2016-09-20","9","1","2","0","INV#IV218,Pax :RAVEENDRAN, PassNo :K9336948/ visaFor:30 DAYS DXB SETTING, By:Sahad");
INSERT INTO transaction VALUES("1771","94","7","213","Visa Payable","5800","0","","2016-09-22","2016-09-20","9","1","2","0","INV#213,sup:94,:against:218,Details:INV#IV218,Pax :RAVEENDRAN, PassNo :K9336948/ visaFor:30 DAYS DXB SETTING, By:Sahad");
INSERT INTO transaction VALUES("1772","7","94","213","Visa Payable","0","5800","","2016-09-22","2016-09-20","9","1","2","0","INV#213,sup:94,:against:218,Details:INV#IV218,Pax :RAVEENDRAN, PassNo :K9336948/ visaFor:30 DAYS DXB SETTING, By:Sahad");
INSERT INTO transaction VALUES("1773","99","6","193","Visa Receivable","0","14600","Visa Receivable : 193","2016-09-22","2016-09-20","9","1","2","0","INV#IV193,Pax :MUHAMMED ANAS, PassNo :L1312268/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Sahad");
INSERT INTO transaction VALUES("1774","6","99","193","Visa Receivable","14600","0","Visa Receivable : 193","2016-09-22","2016-09-20","9","1","2","0","INV#IV193,Pax :MUHAMMED ANAS, PassNo :L1312268/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Sahad");
INSERT INTO transaction VALUES("1775","132","7","190","Visa Payable","14600","0","","2016-09-22","2016-09-20","9","1","2","0","INV#190,sup:132,:against:193,Details:INV#IV193,Pax :MUHAMMED ANAS, PassNo :L1312268/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Sahad");
INSERT INTO transaction VALUES("1776","7","132","190","Visa Payable","0","14600","","2016-09-22","2016-09-20","9","1","2","0","INV#190,sup:132,:against:193,Details:INV#IV193,Pax :MUHAMMED ANAS, PassNo :L1312268/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Sahad");
INSERT INTO transaction VALUES("1777","97","7","246","Visa Payable","18900","0","","2016-09-22","2016-09-22","8","1","2","0","INV#246,sup:97,:against:252,Details:INV#IV252,Pax :HASSAINAR, PassNo :H63896913/ visaFor:90 DAYS QATAR BUSINESS, By:Safuvan");
INSERT INTO transaction VALUES("1778","7","97","246","Visa Payable","0","18900","","2016-09-22","2016-09-22","8","1","2","0","INV#246,sup:97,:against:252,Details:INV#IV252,Pax :HASSAINAR, PassNo :H63896913/ visaFor:90 DAYS QATAR BUSINESS, By:Safuvan");
INSERT INTO transaction VALUES("1779","96","185","20","Bank Receipts","14700","0","CASH RECIVED IN FED BANK","2016-09-22","2016-09-22","33","1","2","0","VouNo#BR20,INV#IV208 Received By :Faizal,Details:INV#IV208,Pax :RICHARD PINTO, PassNo :H4603974/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1780","185","96","20","Bank Receipts","0","14700","CASH RECIVED IN FED BANK","2016-09-22","2016-09-22","33","1","2","0","VouNo#BR20,INV#IV208 Received By :Faizal,Details:INV#IV208,Pax :RICHARD PINTO, PassNo :H4603974/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Safuvan");
INSERT INTO transaction VALUES("1781","174","2","25","Cash Receipts","25000","0","","2016-09-22","2016-09-22","6","2","2","0","VouNo#CR 25,INV#IA26 Received By :Manu,Details:INV#IA26, Pax:PANDARATHIL JURAIGE, Passport No:G5721484, Sector:CCJ/SHJ, Date:22-09-2016/24-09-2016, Tkt:AUKPRG/AUKPRG, PNR:AUKPRG, By:Manu");
INSERT INTO transaction VALUES("1782","2","174","25","Cash Receipts","0","25000","","2016-09-22","2016-09-22","6","2","2","0","VouNo#CR 25,INV#IA26 Received By :Manu,Details:INV#IA26, Pax:PANDARATHIL JURAIGE, Passport No:G5721484, Sector:CCJ/SHJ, Date:22-09-2016/24-09-2016, Tkt:AUKPRG/AUKPRG, PNR:AUKPRG, By:Manu");
INSERT INTO transaction VALUES("1783","95","185","21","Bank Receipts","197600","0","cash deposited in federal bank","2016-09-22","2016-09-22","15","1","2","0","VouNo#BR21,INV#IV68,IV76,IV77,IV111,IV113,IV115,IV116,IV117 Received By :Akhil,Details:INV#IV117,Pax :SHANAZ, PassNo :P4074758/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1784","185","95","21","Bank Receipts","0","197600","cash deposited in federal bank","2016-09-22","2016-09-22","15","1","2","0","VouNo#BR21,INV#IV68,IV76,IV77,IV111,IV113,IV115,IV116,IV117 Received By :Akhil,Details:INV#IV117,Pax :SHANAZ, PassNo :P4074758/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad");
INSERT INTO transaction VALUES("1785","125","2","23","Cash Payments","0","10000","counter 16 refund inv no 1179    muhammed kutty","2016-09-22","2016-09-22","33","1","2","0","VouNo#CP23,INV# Paid By :Faizal To : counter16,Details:");
INSERT INTO transaction VALUES("1786","2","125","23","Cash Payments","10000","0","counter 16 refund inv no 1179    muhammed kutty","2016-09-22","2016-09-22","33","1","2","0","VouNo#CP23,INV# Paid By :Faizal To : counter16,Details:");
INSERT INTO transaction VALUES("1787","99","185","22","Bank Receipts","138200","0","cash deposited in federal bank","2016-09-22","2016-09-22","15","1","2","0","VouNo#BR22,INV#IV164,IV166,IV168,IV173,IV193,IV198,IV205,IV216,IV218 Received By :Akhil,Details:INV#IV218,Pax :RAVEENDRAN, PassNo :K9336948/ visaFor:30 DAYS DXB SETTING, By:Sahad");
INSERT INTO transaction VALUES("1788","185","99","22","Bank Receipts","0","138200","cash deposited in federal bank","2016-09-22","2016-09-22","15","1","2","0","VouNo#BR22,INV#IV164,IV166,IV168,IV173,IV193,IV198,IV205,IV216,IV218 Received By :Akhil,Details:INV#IV218,Pax :RAVEENDRAN, PassNo :K9336948/ visaFor:30 DAYS DXB SETTING, By:Sahad");
INSERT INTO transaction VALUES("1789","213","2","24","Cash Payments","0","1000","couter feit notes","2016-09-22","2016-09-22","33","1","2","0","VouNo#CP24,INV# Paid By :Faizal To : fake note,Details:");
INSERT INTO transaction VALUES("1790","2","213","24","Cash Payments","1000","0","couter feit notes","2016-09-22","2016-09-22","33","1","2","0","VouNo#CP24,INV# Paid By :Faizal To : fake note,Details:");
INSERT INTO transaction VALUES("1791","96","6","253","Visa Receivable","0","71800","Visa Receivable : 253","2016-09-22","2016-09-22","10","1","2","0","INV#IV253,Pax :COUNTER :383 541 739 742 749 750 764 805 842 853 987 1059, PassNo :k000000/ visaFor:Dubai- 30 days,By:Irfad");
INSERT INTO transaction VALUES("1792","6","96","253","Visa Receivable","71800","0","Visa Receivable : 253","2016-09-22","2016-09-22","10","1","2","0","INV#IV253,Pax :COUNTER :383 541 739 742 749 750 764 805 842 853 987 1059, PassNo :k000000/ visaFor:Dubai- 30 days,By:Irfad");
INSERT INTO transaction VALUES("1793","125","7","248","Visa Payable","71800","0","","2016-09-22","2016-09-22","10","1","2","0","Details:INV#IV253,Pax :COUNTER :383 541 739 742 749 750 764 805 842 853 987 1059, PassNo :k000000/ visaFor:Dubai- 30 days,By:Irfad");
INSERT INTO transaction VALUES("1794","7","125","248","Visa Payable","0","71800","","2016-09-22","2016-09-22","10","1","2","0","Details:INV#IV253,Pax :COUNTER :383 541 739 742 749 750 764 805 842 853 987 1059, PassNo :k000000/ visaFor:Dubai- 30 days,By:Irfad");
INSERT INTO transaction VALUES("1795","181","2","25","Cash Payments","0","200","paid cash for aramex corior","2016-09-22","2016-09-22","15","1","2","0","VouNo#CP25,INV# Paid By :Akhil To : COURIER EXP,Details:");
INSERT INTO transaction VALUES("1796","2","181","25","Cash Payments","200","0","paid cash for aramex corior","2016-09-22","2016-09-22","15","1","2","0","VouNo#CP25,INV# Paid By :Akhil To : COURIER EXP,Details:");
INSERT INTO transaction VALUES("1797","214","2","26","Cash Payments","0","100000","Paid daily cash to manager","2016-09-22","2016-09-20","5","1","2","0","VouNo#CP26,INV# Paid By :Sidheeq To : Cash In Hand with Manager,Details:");
INSERT INTO transaction VALUES("1798","2","214","26","Cash Payments","100000","0","Paid daily cash to manager","2016-09-22","2016-09-20","5","1","2","0","VouNo#CP26,INV# Paid By :Sidheeq To : Cash In Hand with Manager,Details:");
INSERT INTO transaction VALUES("1799","214","2","27","Cash Payments","0","1750","paid daily cash","2016-09-22","2016-09-22","5","1","2","0","VouNo#CP27,INV# Paid By :Sidheeq To : Cash In Hand with Manager,Details:");
INSERT INTO transaction VALUES("1800","2","214","27","Cash Payments","1750","0","paid daily cash","2016-09-22","2016-09-22","5","1","2","0","VouNo#CP27,INV# Paid By :Sidheeq To : Cash In Hand with Manager,Details:");
INSERT INTO transaction VALUES("1801","197","2","1","Cash Receipts","1750","0","daily collection","2016-09-22","2016-09-22","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");
INSERT INTO transaction VALUES("1802","2","197","1","Cash Receipts","0","1750","daily collection","2016-09-22","2016-09-22","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");
INSERT INTO transaction VALUES("1803","193","2","1","Cash Receipts","175000","0","daily cash collection","2016-09-22","2016-09-22","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");
INSERT INTO transaction VALUES("1804","2","193","1","Cash Receipts","0","175000","daily cash collection","2016-09-22","2016-09-22","38","11","2","0","VouNo#CR 1,INV# Received By :Sidheeqts,Details:");

DROP TABLE IF EXISTS transport_provider;
CREATE TABLE `transport_provider` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `providerName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO transport_provider VALUES("1","Ujoor","MAKKA","00000000","4","19","2");

DROP TABLE IF EXISTS user_permission;
CREATE TABLE `user_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NOT NULL,
  `basicSetting` int(11) NOT NULL DEFAULT '0',
  `basicSettingEdit` int(11) NOT NULL DEFAULT '0',
  `package` int(11) NOT NULL DEFAULT '0',
  `packageEdit` int(11) NOT NULL DEFAULT '0',
  `account` int(11) NOT NULL DEFAULT '0',
  `accountEdit` int(11) NOT NULL DEFAULT '0',
  `cashBank` int(11) NOT NULL DEFAULT '0',
  `cashBankEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceAirline` int(11) NOT NULL DEFAULT '0',
  `invoiceAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceHotel` int(11) NOT NULL DEFAULT '0',
  `invoiceHotelEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceVisa` int(11) NOT NULL DEFAULT '0',
  `invoiceVisaEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceMisc` int(11) NOT NULL DEFAULT '0',
  `invoiceMiscEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceTransport` int(11) NOT NULL DEFAULT '0',
  `invoiceTransportEdit` int(11) NOT NULL DEFAULT '0',
  `invoicePackage` int(11) NOT NULL DEFAULT '0',
  `invoicePackageEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseAirline` int(11) NOT NULL DEFAULT '0',
  `purchaseAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseHotel` int(11) NOT NULL DEFAULT '0',
  `purchaseHotelEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseVisa` int(11) NOT NULL DEFAULT '0',
  `purchaseVisaEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseMisc` int(11) NOT NULL DEFAULT '0',
  `purchaseMiscEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseTransport` int(11) NOT NULL DEFAULT '0',
  `purchaseTransportEdit` int(11) NOT NULL DEFAULT '0',
  `purchasePackage` int(11) NOT NULL DEFAULT '0',
  `purchasePackageEdit` int(11) NOT NULL DEFAULT '0',
  `accountReport` int(11) NOT NULL DEFAULT '0',
  `otherReport` int(11) NOT NULL DEFAULT '0',
  `user` int(11) NOT NULL DEFAULT '0',
  `userEdit` int(11) NOT NULL DEFAULT '0',
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `staffId` (`staffId`),
  CONSTRAINT `user_permission_ibfk_1` FOREIGN KEY (`staffId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
INSERT INTO user_permission VALUES("1","1","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("2","2","1","1","0","0","1","1","1","1","1","1","1","0","1","1","1","0","1","1","1","0","1","1","1","0","1","1","1","0","1","1","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("3","3","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("4","6","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("5","5","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("6","4","1","0","0","0","1","0","1","0","1","0","1","0","1","1","1","0","1","0","1","0","1","0","1","0","1","1","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("9","9","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("11","11","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("13","12","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("15","13","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("16","16","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("17","14","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("18","17","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("19","19","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("20","18","1","0","0","0","1","0","1","0","1","0","0","0","1","0","1","0","1","0","0","0","1","0","0","0","1","0","1","0","1","0","0","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("21","22","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("22","23","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("23","24","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("24","25","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("25","26","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");

DROP TABLE IF EXISTS vehicle_types;
CREATE TABLE `vehicle_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `vehicleType` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO vehicle_types VALUES("1","Bus","4","19","2");

DROP TABLE IF EXISTS visa_passenger;
CREATE TABLE `visa_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visaId` int(11) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(20) NOT NULL,
  `passportNo` varchar(50) NOT NULL,
  `visaFor` varchar(50) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `tokenNo` varchar(20) NOT NULL,
  `visaFee` float NOT NULL,
  `vfs` float NOT NULL,
  `ddCharge` float NOT NULL,
  `serviceCharge` float NOT NULL,
  `otherCharge` float NOT NULL,
  `courierCharge` float NOT NULL,
  `subTotal` float NOT NULL,
  `travelDate` date NOT NULL,
  `visaMessage` varchar(20) NOT NULL,
  `msgChngBy` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=517 DEFAULT CHARSET=utf8;
INSERT INTO visa_passenger VALUES("1","1","MAIMOONA MANJAPILLI AHAMMAD","0000-00-00","F","N1595786","3","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","1500","0","0","16500","0000-00-00","","0","","2","6","2");
INSERT INTO visa_passenger VALUES("2","2","MUFEED ABDUL AZIZ","0000-00-00","M","Z3668844","6","0000-00-00","0000-00-00","0000-00-00","","5800","0","0","400","0","0","6200","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("3","3","VISAKH","0000-00-00","M","N1140524","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","14600","0","0","28700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("4","4","ABDUL GAFOOR","0000-00-00","M","H2626002","5","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","700","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("5","5","ABDUL GAFOOR","0000-00-00","M","H2626002","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("6","6","SHAHUNL ABDULNASAR","0000-00-00","M","G8487574","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("7","7","SHAJAHAN PALLILATH","0000-00-00","M","H5103542","5","0000-00-00","0000-00-00","0000-00-00","","5100","0","0","100","0","0","5200","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("8","8","SIMON","0000-00-00","M","J1485230","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("9","8","ELSY","0000-00-00","F","J1485339","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("10","9","AMEER","0000-00-00","M","M0314613","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("11","10","ALLY JOY","0000-00-00","M","J0885006","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","300","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("12","11","DENIAL","0000-00-00","M","M9715530","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("13","12","SUSY","0000-00-00","M","N9538535","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","200","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("14","13","RAHMATHUNNISA","0000-00-00","F","L9256080","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","200","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("15","14","SHANU MON","0000-00-00","M","J2491039","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("16","15","MAIMOONA MANJAPPILLY","0000-00-00","F","M159786","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","200","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("17","16","MEENA HILARY","0000-00-00","F","M1342981","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","300","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("211","17","MISRIYA","0000-00-00","M","P1902660","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","200","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("19","18","AGHIL","0000-00-00","M","L9642581","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","100","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("20","19","ABDUL SALEEM","0000-00-00","M","K1233711","5","0000-00-00","0000-00-00","0000-00-00","","5100","0","0","100","0","0","5200","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("21","20","ABDUL SALEEM","0000-00-00","M","K1233711","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("22","21","MOHAMMED ISMAIL","0000-00-00","M","L715982","25","0000-00-00","0000-00-00","0000-00-00","","3300","0","0","500","0","0","3800","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("23","22","MOHAMMED ISMAIL","0000-00-00","M","L715982","25","0000-00-00","0000-00-00","0000-00-00","","3300","0","0","500","0","0","3800","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("24","23","ABDUL GAFOOR","0000-00-00","M","H2626002","9","2016-09-06","0000-00-00","0000-00-00","","5300","0","0","0","0","0","5300","0000-00-00","","0","","1","5","2");
INSERT INTO visa_passenger VALUES("213","24","FATHIMA HASHIFA","0000-00-00","F","N7809054","11","0000-00-00","0000-00-00","0000-00-00","","15900","0","0","1500","0","0","17400","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("28","25","626SHIRIN SHAHANA","0000-00-00","M","P1438308","11","0000-00-00","0000-00-00","0000-00-00","","15900","0","0","500","0","0","16400","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("30","26","MUHAMMED ANSAB","0000-00-00","M","N1900984","5","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","300","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("31","27","ANSAR","0000-00-00","M","P2698527","10","2016-09-06","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("32","28","SAFNAS","0000-00-00","M","H6710271","9","2016-09-06","0000-00-00","0000-00-00","","14100","0","0","900","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("33","29","HASSAN PUTHIYATI","0000-00-00","M","G9210752","10","2016-09-06","0000-00-00","0000-00-00","","14100","0","0","600","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("34","30","ANEEM","0000-00-00","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","2","2");
INSERT INTO visa_passenger VALUES("35","31","HEZZA","0000-00-00","F","N8801988","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("36","31","ISMATHUL HAMRA","0000-00-00","F","N8239724","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("37","32","MTM570 JAMAL YASAR","0000-00-00","M","H3365841","23","0000-00-00","0000-00-00","0000-00-00","","15500","0","0","1000","0","0","16500","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("38","33","MANZOOR","0000-00-00","M","P0914263","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("39","33","NOUFAL","0000-00-00","M","J5706307","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("40","33","PERAMAYIAN","0000-00-00","M","L9319426","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("41","33","ARAVINDAN","0000-00-00","M","M2761361","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("42","33","SAINUDHEEN","0000-00-00","M","Z2993148","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("43","33","RUKIYA","0000-00-00","F","K3086549","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("44","33","HAMNA","0000-00-00","F","N9974878","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("45","33","IDHAYATHUL HAMDAN","0000-00-00","M","N9974878","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("46","34","ANSIL","0000-00-00","M","L1756857","9","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","700","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("47","35","ZAINABA","0000-00-00","M","J8564596","10","2016-09-07","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("48","36","IBRAHIM KUTTY","0000-00-00","M","P2813905","11","2016-09-07","0000-00-00","0000-00-00","","15800","0","0","400","0","0","16200","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("49","37","MUHAMMED","0000-00-00","M","N5120158","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","900","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("50","38","MANU BHAI","0000-00-00","M","N7456851","5","2016-09-07","0000-00-00","0000-00-00","","5150","0","0","50","0","0","5200","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("51","39","SAROJA","0000-00-00","F","K6272814","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("52","39","JAFIN","0000-00-00","F","M3827069","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("53","40","CHANDRAN PULIKKATT","0000-00-00","M","J5352156","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","700","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("54","40","SHEEJA","0000-00-00","F","N4524757","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","700","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("55","41","NILAA NIKHIL","0000-00-00","M","P0422439","11","2016-09-07","0000-00-00","0000-00-00","","16000","0","0","0","0","0","16000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("56","42","JAMES CLEETUS","0000-00-00","M","N5427392","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("57","43","MERLIN BABU","0000-00-00","F","M0477973","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("58","44","ASHNA","0000-00-00","F","M6093308","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("59","45","SHAMEEM","0000-00-00","M","K3072306","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("60","45","SRIMATHY","0000-00-00","F","P4071810","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("61","46","SHEEBA","0000-00-00","F","N5159159","9","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","300","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("62","47","JUSTIN JAMES","0000-00-00","M","L5914253","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-200","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("63","48","ASOK KUMAR","0000-00-00","M","M1674522","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("64","48","DILEEP","0000-00-00","M","G6067478","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("65","48","HASHIM","0000-00-00","M","H6275220","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("66","48","RAMAKRISHNAN","0000-00-00","M","G6160411","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("67","48","JERIN JAMES","0000-00-00","M","K5427677","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("68","48","SABEEH","0000-00-00","M","H3049393","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("69","48","FAISAL","0000-00-00","M","K3033987","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("70","48","UMMER","0000-00-00","M","M4950208","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("71","48","HAFIZ","0000-00-00","M","J2006760","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("72","48","KUNJHU MON","0000-00-00","M","M7807535","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("73","48","MADAVADAS","0000-00-00","M","J5254307","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("74","48","VENUGOPALAN","0000-00-00","M","H5709904","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("75","48","SAROJINI","0000-00-00","F","H5708579","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("76","49","VARGHESE","0000-00-00","M","M832608","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("77","49","SUDHEESH","0000-00-00","M","N6950516","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("78","50","RAJA RAM","0000-00-00","M","H7437321","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","600","0","0","15400","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("79","50","VINOD","0000-00-00","M","N9184559","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","600","0","0","15400","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("80","50","MANOJ KUMAR","0000-00-00","M","K5356456","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","600","0","0","15400","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("81","51","ARIF","0000-00-00","M","M9418291","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("82","51","BONEY JOSEPH","0000-00-00","M","G8586116","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("83","52","SALIHA","0000-00-00","M","M0174249","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("84","52","NIMMY","0000-00-00","M","L2844381","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("85","53","SARATH KRISHNA","0000-00-00","M","K6243609","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","900","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("86","54","HABEEBA SHAFEEEK","0000-00-00","M","P2642443","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("87","55","HASNA","0000-00-00","M","P1163918","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("88","55","ZAHIDA","0000-00-00","M","M9909492","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("89","56","DAISAN DAVID","0000-00-00","M","N1142416","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("90","57","SIJO JOSEPH","0000-00-00","M","N8591047","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("91","57","AJAY KRISHNA","0000-00-00","M","G9194546","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("92","58","ABDUL AZEEZ","0000-00-00","M","L8040492","5","2016-09-07","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("93","59","NAFSIYA KOLAMBIL","0000-00-00","F","G4454840","9","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","300","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("94","59","RSHAN AHAMMED","0000-00-00","M","P0352855","9","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","300","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("95","59","TANIA ISRA","0000-00-00","F","K5269033","9","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","300","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("100","60","NAHIM","0000-00-00","M","K2828090","10","0000-00-00","0000-00-00","0000-00-00","","400","0","0","0","0","0","400","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("97","61","ABDU RAHIMAN","0000-00-00","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("98","61","SREEJITH","0000-00-00","M","K8893537","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("99","62","JAMES","0000-00-00","M","J4059812","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("101","63","ANSAR","0000-00-00","M","M5169954","10","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("102","64","ANILA","0000-00-00","F","N7681273","12","0000-00-00","0000-00-00","0000-00-00","","2300","0","0","0","0","0","2300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("103","64","PABRI CK","0000-00-00","F","N7686960","12","0000-00-00","0000-00-00","0000-00-00","","2300","0","0","0","0","0","2300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("104","64","PATRICIA ANJELIN","0000-00-00","F","P2488318","12","0000-00-00","0000-00-00","0000-00-00","","2300","0","0","0","0","0","2300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("105","65","REMA KUMARI","0000-00-00","F","N9101342","12","0000-00-00","0000-00-00","0000-00-00","","2300","0","0","0","0","0","2300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("106","65","KAVITHA","0000-00-00","F","K6437126","12","0000-00-00","0000-00-00","0000-00-00","","2300","0","0","0","0","0","2300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("107","66","RAMANATHAN","0000-00-00","M","M6237420","12","0000-00-00","0000-00-00","0000-00-00","","2600","0","0","0","0","0","2600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("108","67","Fathima Hanna","0000-00-00","F","L7896144","10","0000-00-00","0000-00-00","0000-00-00","","14900","0","0","0","0","0","14900","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("109","68","RICHARD","0000-00-00","M","H4603974","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","1000","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("110","69","THAJUDHEEN","0000-00-00","M","K1076374","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("111","70","SUSHANTH","0000-00-00","M","K5230981","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","900","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("112","71","SAJEEVAN","0000-00-00","M","M0569663","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","600","0","0","15400","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("113","72","AYISHA KUNNATH","0000-00-00","F","P0915315","2","2016-09-07","0000-00-00","0000-00-00","","5500","0","0","300","0","0","5800","0000-00-00","","0","","2","6","2");
INSERT INTO visa_passenger VALUES("114","73","SABEENA 1118","0000-00-00","F","J5242293","10","0000-00-00","0000-00-00","0000-00-00","","7700","0","0","0","0","0","7700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("207","107","UDAYA MOOLYA","0000-00-00","M","J7091327","4","0000-00-00","0000-00-00","0000-00-00","","5100","0","0","100","0","0","5200","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("216","74","FERVEES 1199","0000-00-00","M","J5802083","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("215","74","VIJESH 1222","0000-00-00","M","K7027177","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("214","74","ANEEM 1223","0000-00-00","M","K5283027","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("191","94","1197	SAREENA","0000-00-00","F","K9858317","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("121","76","KAVERI","0000-00-00","M","K7027177","10","0000-00-00","0000-00-00","0000-00-00","","14900","0","0","0","0","0","14900","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("122","77","AKHILA 12200","0000-00-00","F","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("123","77","ISHAK 1224","0000-00-00","M","K00000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("124","77","SREESHMA 1226","0000-00-00","F","K00000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("125","78","VYSHAKH 1233","0000-00-00","M","n9553909","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("126","78","AHAMEDKUTTY 1258","0000-00-00","M","J2077389","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("127","78","THARUN 1259","0000-00-00","M","P0246374","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("128","78","BINOY 1261","0000-00-00","M","L6725925","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("129","79","NOORUNNISA 1234","0000-00-00","F","M7276307","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("130","79","MUHAMMED RASHID 1234","0000-00-00","M","P3999764","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("131","79","ASHA MATHARI 1240","0000-00-00","F","J5238983","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("132","79","GEETHA 1250","0000-00-00","F","N5577038","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("133","79","1262	ANU MOLE","0000-00-00","F","N7798897","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("134","79","1264	VIJITHA","0000-00-00","F","P0428557","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("135","80","VIKAS VINOD","0000-00-00","M","J3924121","18","2016-09-08","0000-00-00","0000-00-00","","8000","0","0","2000","0","0","10000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("136","81","VIKAS VINOD","0000-00-00","M","J3924121","15","2016-09-08","0000-00-00","0000-00-00","","5500","0","0","300","0","0","5800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("137","82","AMAL PRABHAKAR","0000-00-00","M","J66565291","16","2016-09-08","0000-00-00","0000-00-00","","6500","0","0","100","0","0","6600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("138","83","MUHAMMED SHAMMAS","0000-00-00","M","L9931582","9","2016-09-08","0000-00-00","0000-00-00","","14100","0","0","900","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("139","84","MTM564 AHMED ORMUNDAKKAL","0000-00-00","M","J4887754","19","0000-00-00","0000-00-00","0000-00-00","","16500","0","0","0","0","0","16500","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("140","85","RAJAN KOCHU","0000-00-00","M","K3751349","5","2016-09-08","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("141","86","MATHUKUTTY","0000-00-00","M","L1747043","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","600","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("142","87","AYISHA","0000-00-00","F","P0615315","4","2016-09-08","0000-00-00","0000-00-00","","5100","0","0","400","0","0","5500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("143","88","RAHILA","0000-00-00","F","L4220391","10","2016-09-08","0000-00-00","0000-00-00","","14800","0","0","200","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("144","88","FATHIMA MIHRA","0000-00-00","F","M3684407","10","2016-09-08","0000-00-00","0000-00-00","","14800","0","0","200","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("145","88","MUHAMMED MIRSHAD","0000-00-00","F","N5538499","10","2016-09-08","0000-00-00","0000-00-00","","14800","0","0","200","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("146","89","HIDAYATHULLA","0000-00-00","M","J8843095","17","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","1500","0","0","16000","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("147","90","MTM615 HIDAYATHULLA","0000-00-00","M","J8843095","17","0000-00-00","0000-00-00","0000-00-00","","11000","0","0","0","0","0","11000","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("148","91","AYISHA","0000-00-00","F","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("149","91","FATHIMA","0000-00-00","F","K000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("150","91","FAHEENA","0000-00-00","M","K000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("489","92","1196	MUHAMMED ALI","0000-00-00","M","K6459585","10","0000-00-00","0000-00-00","0000-00-00","","11100","0","0","0","0","0","11100","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("183","93","1196	MUHAMMED ALI","0000-00-00","M","0","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("195","97","AYSHANA RAMEES","0000-00-00","F","K00000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("190","94","1198   VALSALA","0000-00-00","F","N2189073","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("189","94","1245	NASREENA","0000-00-00","F","M0882717","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("188","94","1245    AYSHANA RAMEES","0000-00-00","F","M5856848","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("197","106","COUNTER BALANCE,","0000-00-00","M","k000000","5","0000-00-00","0000-00-00","0000-00-00","","78325","0","0","0","0","0","78325","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("208","108","KADAMBU NEBISA,ANNATH,MAHAMMAD SHAFIK,MAHAMMAD NAWASHAD","0000-00-00","M","L6156737.P2778220,P2574005,P2887976","4","0000-00-00","0000-00-00","0000-00-00","","20400","0","0","400","0","0","20800","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("487","96","mohammed","0000-00-00","M","k8000829","25","0000-00-00","0000-00-00","0000-00-00","","6000","0","0","300","0","0","6300","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("194","97","NASREENA","0000-00-00","F","K00000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("193","97","VALSALA","0000-00-00","F","K000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("192","97","SAREENA VALSALA NASREENA /AYSHANA RAMEES","0000-00-00","F","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("168","98","1247	HISHAM MASHOOD","0000-00-00","M","J5440840","11","0000-00-00","0000-00-00","0000-00-00","","15900","0","0","0","0","0","15900","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("167","99","1247	HISHAM MASHOOD","0000-00-00","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("169","100","1254	ASHRAF PURATH PARAMBATH","0000-00-00","M","H5686487","11","0000-00-00","0000-00-00","0000-00-00","","15900","0","0","0","0","0","15900","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("170","101","1254	ASHRAF PURATH PARAMBATH","0000-00-00","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("209","109","SHAFJAN PATTILLATH CHANDERA","0000-00-00","M","H5103542","4","0000-00-00","0000-00-00","0000-00-00","","5100","0","0","100","0","0","5200","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("179","102","SABAD MUHAMMED","0000-00-00","M","M4747049","37","0000-00-00","0000-00-00","0000-00-00","","7000","0","0","750","0","0","7750","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("180","103","MUHAMMED ALI MUHAMMED","0000-00-00","M","H5110153","37","0000-00-00","0000-00-00","0000-00-00","","7000","0","0","750","0","0","7750","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("181","104","HAMDAN HAMEED","0000-00-00","M","P2203649","11","2016-09-09","0000-00-00","0000-00-00","","15800","0","0","700","0","0","16500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("182","105","JAMSHAD","0000-00-00","M","H1608955","11","2016-09-09","0000-00-00","0000-00-00","","15800","0","0","400","0","0","16200","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("210","110","","0000-00-00","","","38","0000-00-00","0000-00-00","0000-00-00","","11000","0","0","500","0","0","11500","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("217","111","USMAN PORKKALAM","0000-00-00","M","J6960023","11","0000-00-00","0000-00-00","0000-00-00","","15800","0","0","500","0","0","16300","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("218","112","SEBEER PANEER","0000-00-00","M","L1356529","10","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("219","113","POOVATIL/JUHANA","0000-00-00","F","M2664637","3","0000-00-00","0000-00-00","0000-00-00","","15700","0","0","1500","0","0","17200","0000-00-00","","0","","2","6","2");
INSERT INTO visa_passenger VALUES("220","114","ANU MOLE","0000-00-00","F","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("221","114","SABEERA","0000-00-00","F","K000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("222","114","REMAKUMARI","0000-00-00","F","K000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("223","114","SHAHARAS","0000-00-00","M","K00000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("224","115","RIYAS","0000-00-00","M","P1018115","9","2016-09-16","0000-00-00","0000-00-00","","14800","0","0","500","0","0","15300","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("225","115","RAHINA THOUFEEK","0000-00-00","F","P0412729","9","2016-09-16","0000-00-00","0000-00-00","","14800","0","0","500","0","0","15300","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("226","116","KUNJHU MOIDEEN","0000-00-00","M","K0385278","10","2016-09-16","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("227","116","AYISHA","0000-00-00","F","K0385277","10","2016-09-16","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("228","117","NITHEESH","0000-00-00","M","M2502907","10","2016-09-16","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("229","118","DAVIES MANJALY","0000-00-00","M","H9993108","10","2016-09-16","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("230","119","NIRMALA","0000-00-00","F","N9068639","10","2016-09-16","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("231","120","SHANAZ","0000-00-00","M","P4074758","10","2016-09-16","0000-00-00","0000-00-00","","14800","0","0","-200","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("232","121","DISHA ABISHEK","0000-00-00","F","P2352218","10","2016-09-16","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("233","122","YOONUS","0000-00-00","M","N554446","10","2016-09-16","0000-00-00","0000-00-00","","14800","0","0","-200","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("234","123","EBRAHIM","0000-00-00","M","M1043706","10","2016-09-16","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("235","124","AKBAR SHAJAHAN","0000-00-00","M","P3482267","10","2016-09-16","0000-00-00","0000-00-00","","14800","0","0","-200","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("236","125","ASKAR","2016-09-17","M","N0925550","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("237","126","TREESA JOSE","2016-09-17","F","K7236815","10","2016-09-17","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("238","127","AMIN SHERIF","2016-09-17","M","J8570711","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("239","128","JAFER","0000-00-00","M","L6355058","5","2016-09-17","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("240","129","ABDUL SHERIF","2016-09-17","M","L7528787","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("241","129","SARFUNNIZA","2016-09-17","M","N2147570","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("242","130","SARA IBRAHIM","2016-09-17","F","G3509227","10","2016-09-17","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("243","130","ATIFUL DULFIYA","2016-09-17","F","P4000338","10","2016-09-17","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("244","131","ABDULLA","2016-09-17","M","G9716233","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("245","132","NUSRATH","2016-09-17","F","M2789304","10","2016-09-17","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("246","132","MINHA FATHIMA","2016-09-17","F","M4145622","10","2016-09-17","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("247","132","MALLIK KUMARSWAMY","2016-09-17","F","J8582907","10","2016-09-17","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("248","132","MARIYA","2016-09-17","F","L1316251","10","2016-09-17","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("249","133","SAIF ABDUL AZEEZ","2016-09-17","M","H7400492","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("250","133","ABDUL RAHIMAN","2016-09-17","M","L3684474","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("251","134","MURSINA","2016-09-17","F","P0092227","10","2016-09-17","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("252","134","PRAJITHA","2016-09-17","F","N3122093","10","2016-09-17","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("253","135","RASHIDH","2016-09-17","M","P4059622","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("254","135","MANZOOR","2016-09-17","M","N9960716","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("255","135","FIJAS","2016-09-17","M","P1282872","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("256","135","VIPIN","2016-09-17","M","L4827297","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("257","135","SALIH","2016-09-17","M","K4635001","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("258","135","SHANAVAS","2016-09-17","M","L9686169","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("259","135","SAHID MON","2016-09-17","M","H6480490","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("260","135","SANIDH","2016-09-17","M","M5654047","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("261","135","DILEEP","2016-09-17","M","K4038159","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("262","135","BAJEESH","2016-09-17","M","N4336784","10","2016-09-17","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("263","136","SANOOF","2016-09-17","M","N8234508","10","2016-09-17","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("264","136","SHABEENA","2016-09-17","F","M8557330","10","2016-09-17","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("265","137","NOORJAHAN","2016-09-17","F","K9052971","4","2016-09-17","0000-00-00","0000-00-00","","5100","0","0","300","0","0","5400","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("266","137","MOHAMMED ISMAIL","2016-09-17","M","Z2209883","4","2016-09-17","0000-00-00","0000-00-00","","5100","0","0","300","0","0","5400","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("267","137","ARIFA MAJEED","2016-09-17","M","K2442805","4","2016-09-17","0000-00-00","0000-00-00","","5100","0","0","300","0","0","5400","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("268","138","MUBASHIRA","2016-09-19","F","N5210758","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("269","138","USHA PADMANABAN","2016-09-19","F","H4651646","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("270","139","AFSAL ALI","2016-09-19","M","J8837024","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("271","139","VINI JOSE","2016-09-19","M","H9689436","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("272","139","RASHEED","2016-09-19","M","N9138711","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("273","139","MIDHLAJ","2016-09-19","M","L7920538","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("274","139","MOHAMMED LIBASIN","2016-09-19","M","H7232734","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("275","139","HYDERALI","2016-09-19","M","P4052692","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("276","139","ABDUL LATHEEF","2016-09-19","M","M2403126","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("277","140","NIHAS","2016-09-19","M","M6039206","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("278","140","AZMIN FARAS","2016-09-19","F","L9706041","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("279","140","PHARISHA","2016-09-19","F","L9642701","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("280","140","GREESHMA","2016-09-19","F","N9499762","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("281","141","THANZEER","2016-09-19","M","G5437199","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("282","141","VIDESH","2016-09-19","M","G4842975","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("283","141","MUSTHAFA","2016-09-19","M","K0825228","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("284","141","MOHAMMED BASHEER","2016-09-19","M","J5523932","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("285","141","JAYA GOPALAN","2016-09-19","M","H9597787","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("286","142","UMAIR SHAMSUDHEEN","2016-09-19","M","M2434982","11","2016-09-19","0000-00-00","0000-00-00","","15600","0","0","400","0","0","16000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("287","143","FASALURAHMAN","2016-09-19","M","N2555523","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("288","143","ANWAR","2016-09-19","M","K9738262","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("289","143","FASEELA MUJEEB","2016-09-19","F","K1586353","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("290","143","MINHA MARIYAM","2016-09-19","F","K53600143","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("291","143","AYISHA THARAKKAL","2016-09-19","F","K3056624","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("292","143","SHAN ADAM","2016-09-19","F","M9113339","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("293","144","ABDUL NASEER","2016-09-19","M","K9823040","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","600","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("294","145","ASTHER LOURD","2016-09-19","M","J1943467","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("295","146","SIAHUDHEEN","2016-09-19","M","K9057849","6","2016-09-19","0000-00-00","0000-00-00","","5100","0","0","400","0","0","5500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("296","147","KANZA","2016-09-19","F","N7026492","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("297","147","NAZIYA","2016-09-19","F","N8159169","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("298","148","BEEGUM","2016-09-19","F","M4585433","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("299","148","MUKTHAR MOHAMMED","2016-09-19","M","P0621596","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("300","148","ANSI BADAR","2016-09-19","F","L9385374","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("301","148","SWALIHA","2016-09-19","F","N3176850","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("302","149","SREEJITH","2016-09-19","M","L8535331","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("303","149","NOORUDHEEN","2016-09-19","M","K7515194","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("304","150","SEBASTIAN","2016-09-19","M","H4429316","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("305","150","ANU CHETTU","2016-09-19","M","K1061340","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("306","151","AZAAN","2016-09-19","M","M2529672","5","2016-09-19","0000-00-00","0000-00-00","","5100","0","0","25","0","0","5125","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("307","151","SAIMA","2016-09-19","F","M2236990","5","2016-09-19","0000-00-00","0000-00-00","","5100","0","0","25","0","0","5125","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("308","151","SAKEENA","2016-09-19","F","L9018447","5","2016-09-19","0000-00-00","0000-00-00","","5100","0","0","25","0","0","5125","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("309","151","MOHAMMED ABID","2016-09-19","M","Z1910668","5","2016-09-19","0000-00-00","0000-00-00","","5100","0","0","25","0","0","5125","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("310","151","FEMINA","2016-09-19","F","K1583268","5","2016-09-19","0000-00-00","0000-00-00","","5100","0","0","25","0","0","5125","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("311","152","JAKIR ALI","2016-09-19","M","J8710589","5","2016-09-19","0000-00-00","0000-00-00","","5100","0","0","25","0","0","5125","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("312","152","HARIHARA","2016-09-19","M","J8732899","5","2016-09-19","0000-00-00","0000-00-00","","5100","0","0","25","0","0","5125","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("313","152","SHEIKH","2016-09-19","M","J8711078","5","2016-09-19","0000-00-00","0000-00-00","","5100","0","0","25","0","0","5125","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("314","153","UMMER ISMAYIL","2016-09-19","M","P4073615","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("315","154","TITTU","2016-09-19","M","K6437253","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","600","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("316","155","SUHAIB","2016-09-19","M","L2822761","9","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","500","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("317","155","LALITHA","2016-09-19","F","J89747590","9","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","500","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("318","156","BASHEER","2016-09-19","M","G2821855","7","2016-09-19","0000-00-00","0000-00-00","","10250","0","0","250","0","0","10500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("319","157","MADAVAS","2016-09-01","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("320","157","ASHRAF","2016-09-01","M","K000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("321","157","KAVITHA","2016-09-01","M","K00000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("322","158","SAHIR &ABDUL JALEEL","1983-04-13","M","L2371853/	N7113488","10","0000-00-00","0000-00-00","0000-00-00","","31400","0","0","0","0","0","31400","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("323","159","MATHUKUTTY","0000-00-00","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("324","160","KADER","2016-09-01","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("325","160","AYISHA","2016-09-04","F","K000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("326","161","PHILIP","2016-09-01","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("327","161","MSARIYAMMA","2016-09-01","F","K00000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("328","162","ISTHIAQ","2016-09-19","M","L2498982","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("329","162","SMITHA","2016-09-19","F","K4201402","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("330","163","MUHAMMED MUSTHAFA","2016-09-19","M","L1466281","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("331","164","JOBIN","2016-09-19","M","K6230924","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("332","165","AKHIL","2016-09-19","M","K5367262","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","600","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("333","166","SJAJAHAN","2016-09-19","M","J5958608","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("334","167","SOOSAMMA CHANDY","2016-09-19","F","N8816782","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("335","168","FIROS","2016-09-19","M","L5907266","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("336","169","CERIN XAVIER","2016-09-19","M","K1079157","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("337","169","RONI MOOKAYIL","2016-09-19","M","M0651353","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("338","170","ANISH KUMAR","2016-09-19","M","H4653637","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("339","171","TOMY MOL","2016-09-19","F","P4063035","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("340","172","MOHAMMED REHAN","2016-09-19","M","N0482654","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("341","172","AFSAL","2016-09-19","M","Z2953998","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("342","172","MUHAMMED NOUFAL","2016-09-19","M","M2830526","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("343","172","PARATTU SAIDU","2016-09-19","M","H1113535","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("344","172","FATHIMA","2016-09-19","F","G6290489","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("345","172","SAYYAF","2016-09-19","M","K3979073","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("346","172","TAJUDHENN","2016-09-19","M","J2040267","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("347","172","ABDU RAHIMAN","2016-09-19","M","N0161120","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("348","173","MOHAMMED JIHAD","2016-09-19","M","N1597279","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("349","173","FAHIS","2016-09-19","M","P2332064","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("350","173","FARSANA","2016-09-19","F","H5552428","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("351","173","HANA FATHIMA","2016-09-19","F","L3650539","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("352","173","ZAMEEL","2016-09-19","M","L8267861","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("353","173","KHADEEJA","2016-09-19","F","J2671347","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("354","173","JUSTIN","2016-09-19","M","N2088987","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("355","173","RASHEED","2016-09-19","M","M3712539","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("356","173","SUBAIDA","2016-09-19","F","L2355344","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("357","174","JANSI BANU","2016-09-19","F","K3509049","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("358","174","MOHAMMED","2016-09-19","M","K4048589","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("359","174","AHAMMED AFREED","2016-09-19","M","M9386753","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("360","174","ANEESA","2016-09-19","F","M9867590","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("361","174","HANA FATHIMA","2016-09-19","F","N2757430","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("362","174","MUHAMMED HAMNA","2016-09-19","F","N2757718","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("363","174","SURENDRAN","2016-09-19","M","L7060373","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("364","174","SAIDALI","2016-09-19","M","L1222266","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("365","174","AMUL KOYA","2016-09-19","M","H9381424","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("366","174","SARA","2016-09-19","F","H2809593","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("367","174","SUMAYA MOL","2016-09-19","F","G6157755","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("368","174","RIFA","2016-09-19","F","N8086443","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("369","174","ABDUL RAOOF","2016-09-19","M","Z2105213","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("370","175","VIJAYAMMA","2016-09-19","F","P0248026","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("371","175","MUHAMMED SHAFI","2016-09-19","M","H1477969","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("372","175","KAMARU","2016-09-19","M","Z3403231","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("373","175","ABDUL JABBAR","2016-09-19","M","M0236862","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("374","176","SAJU RAJ","2016-09-19","M","K6463555","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("375","177","FATHIMA NASEER","2016-09-19","F","N2809487","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("376","178","JOBIN JOSE MARY","2016-09-19","M","N4351083","9","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("377","179","PARVATHI","2016-09-19","F","K9986602","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("378","180","LITHAS","2016-09-19","M","L8303251","10","2016-09-19","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("379","181","MUNAVVAR","2016-09-19","M","J6244054","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","125","0","0","5200","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("380","181","DANIS","2016-09-19","M","K7417702","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","125","0","0","5200","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("381","182","MOHAMMED ISHAK","2016-09-19","M","N3264522","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("382","183","MOHAMMED ISHAK","2016-09-19","M","N3264522","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("383","183","THAHIRA","2016-09-19","F","J5239951","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("384","184","ASHA JHON","2016-09-19","F","P2812641","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("385","184","JUSTIN","2016-09-19","M","G7715071","5","2016-09-19","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("386","185","SOMA BANERGI","2016-09-19","F","H4493704","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("387","186","FATHIMA SHIFANATH","2016-09-19","F","P2512185","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","500","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("408","187","LINTO","2016-09-19","M","M2693421","10","2016-09-19","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("392","188","SHAMEEM","0000-00-00","M","H0047499","4","0000-00-00","0000-00-00","0000-00-00","","6100","0","0","400","300","0","6800","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("390","189","MUSTHAK ALI AHAMMED","0000-00-00","M","M1904121","4","0000-00-00","0000-00-00","0000-00-00","","5500","0","0","0","300","0","5800","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("391","190","AZEEZ NARACHUMMANTAKATH","0000-00-00","M","K8764291","12","0000-00-00","0000-00-00","0000-00-00","","17200","0","0","1300","0","0","18500","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("393","191","MUHAMMED IMRAN/MTM571 KHADEEJA PALLIPURAY","0000-00-00","M","N2014312/ N8496095","4","0000-00-00","0000-00-00","0000-00-00","","7800","0","0","0","0","0","7800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("510","192","COUNTER:65 78 79 91 94 95 100 119 138 226 228 333 806 878 1162 1265","0000-00-00","M","k000000","1","0000-00-00","0000-00-00","0000-00-00","","70200","0","0","0","0","0","70200","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("398","193","COUNTER :128 129 358 390 522 1231","0000-00-00","M","k000000","1","0000-00-00","0000-00-00","0000-00-00","","54700","0","0","0","0","0","54700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("396","194","counter :156 332 429 955","0000-00-00","M","k000000","1","0000-00-00","0000-00-00","0000-00-00","","17400","0","0","0","0","0","17400","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("406","195","MTM603 AMEERUDHEEN ,  MOHAMMED AL","2016-09-01","M","J8840991","20","0000-00-00","0000-00-00","0000-00-00","","5000","0","0","1000","0","0","6000","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("407","195","MOHAMMED AL","2016-09-01","M","K4044842","20","0000-00-00","0000-00-00","0000-00-00","","5000","0","0","1000","0","0","6000","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("405","195","RAHMATH ALI","2016-09-01","M","K1301847","20","0000-00-00","0000-00-00","0000-00-00","","5000","0","0","1000","0","0","6000","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("515","196","MUHAMMED ANAS","2016-09-20","M","L1312268","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("410","197","KRISHANANUNNY","2016-09-20","M","N2509254","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("411","198","NOUSHAD","2016-09-20","M","L9056502","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("412","198","ASKAR","2016-09-20","M","N2124342","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("413","199","NITHIN VARGHESE","2016-09-20","M","K5969564","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","700","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("414","200","PRINCE","2016-09-20","M","N1725019","18","2016-09-20","0000-00-00","0000-00-00","","8000","0","0","2000","0","0","10000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("415","201","MUHAMMED MUSTHAFA","2016-09-20","M","G7682506","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("416","202","SHAROOK","2016-09-20","M","N0479933","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("417","203","NOUFAL","2016-09-20","M","L4528400","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("418","203","MOHAMMED AFLAH","2016-09-20","M","L421240","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("419","203","SHAFEEK","2016-09-20","M","H3389846","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("420","204","MATHAI","2016-09-20","M","H5378488","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("421","204","ACHAMMA","2016-09-20","F","H7575435","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("422","205","RASHID ALUNGAL","2016-09-20","M","K9765697","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("423","205","MOHAMMED RASHID","2016-09-20","M","K9744697","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("424","205","SAMEER","2016-09-20","M","G8235703","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("425","205","SHAMEEM","2016-09-20","M","M6015145","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("426","205","ANOOP","2016-09-20","M","M49736009","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("427","205","MUHAMMED SABIR","2016-09-20","M","N7803090","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("428","206","BUSHARA","2016-09-20","F","K1586814","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("429","206","ABDUL BASITH","2016-09-20","M","N7631192","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("430","206","SIRAJ","2016-09-20","M","M8148005","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("431","207","RAJESH ANAND","2016-09-20","M","H3990335","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("432","208","SHANLY SIMON","2016-09-20","F","H6455482","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("433","209","MANSOOR","2016-09-20","M","G4551263","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("434","210","SHANAS","2016-09-20","M","G6369762","5","2016-09-20","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("435","211","RICHARD PINTO","0000-00-00","M","H4603974","10","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("436","212","MOOSA NEYYAN","0000-00-00","M","k000000","23","0000-00-00","0000-00-00","0000-00-00","","1000","0","0","0","0","0","1000","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("497","244","SREEJA","2016-09-22","M","P0625401","10","2016-09-22","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("496","243","MUHAMMED MALIK","2016-09-22","M","N0175503","10","2016-09-22","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("495","241","HAMEED OTTAPILAVULLATHIL","0000-00-00","M","L28333806","7","0000-00-00","0000-00-00","0000-00-00","","5100","0","0","100","0","0","5200","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("494","242","HAMEED","0000-00-00","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("490","240","khalid , NAJEEB , ALI AKBAR","0000-00-00","M","K2266285 , N4044387 , M7748105","4","0000-00-00","0000-00-00","0000-00-00","","15600","0","0","900","900","0","17400","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("488","239","NAVAB","0000-00-00","M","P2198094","16","0000-00-00","0000-00-00","0000-00-00","","5500","0","0","700","0","0","6200","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("486","238","ANAS ABDUL MAJEED","2016-09-20","M","L7677299","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("447","215","","0000-00-00","","","9","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","800","1000","0","16000","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("448","216","SMITA , SAIESHA","0000-00-00","M","","4","0000-00-00","0000-00-00","0000-00-00","","10300","0","0","100","0","0","10400","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("449","217","ANIL KUMAR","2016-09-20","M","M2296507","5","2016-09-20","0000-00-00","2016-09-20","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("450","217","JAYASREE","2016-09-20","F","K0085069","5","2016-09-20","0000-00-00","2016-09-20","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("451","217","SAYED MUHAMMED","2016-09-20","M","K9227945","5","2016-09-20","0000-00-00","2016-09-20","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("452","217","SAYIDATHA JAFAR","2016-09-20","F","P1015108","5","2016-09-20","0000-00-00","2016-09-20","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("453","217","LIYAKATH","2016-09-20","M","L1155076","5","2016-09-20","0000-00-00","2016-09-20","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("454","217","MARY KLISTED","2016-09-20","F","H5041984","5","2016-09-20","0000-00-00","2016-09-20","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("455","217","JHON","2016-09-20","F","Z1971834","5","2016-09-20","0000-00-00","2016-09-20","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("456","217","NAFEESU KODOTH","2016-09-20","M","G2804883","5","2016-09-20","0000-00-00","2016-09-20","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("457","217","LUKMNUL KAKEEM","2016-09-20","M","M8565689","5","2016-09-20","0000-00-00","2016-09-20","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("458","218","MANJU MURALI","2016-09-20","F","P2098797","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("459","219","YOOSAF","2016-09-20","M","N9071534","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("460","220","SAIDU ALI AKBAR","2016-09-20","M","K6559055","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("516","256","COUNTER :383 541 739 742 749 750 764 805 842 853 987 1059","0000-00-00","M","k000000","1","0000-00-00","0000-00-00","0000-00-00","","71800","0","0","0","0","0","71800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("514","221","RAVEENDRAN","2016-09-20","M","K9336948","6","2016-09-20","0000-00-00","0000-00-00","","5800","0","0","400","0","0","6200","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("463","222","MUNAVAR , DANIS","0000-00-00","M","J6244054 , K7417702","4","0000-00-00","0000-00-00","0000-00-00","","10200","0","0","200","0","0","10400","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("464","223","SAKIRA","2016-09-20","F","M5045102","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("465","224","ABDUL JALEEL","2016-09-20","M","P2516541","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","600","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("466","225","SALMAN KHAN","0000-00-00","M","N7724337","10","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("467","226","SHAJAHAN KUDUNGARA HALLI","0000-00-00","M","N2498971","10","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("468","227","PRADISHA","2016-09-20","F","L7914814","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("469","227","AYDIN","2016-09-20","F","L8284123","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("470","227","MANIYAMMAL","2016-09-20","F","L8208398","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("471","227","YEMI ABAN","2016-09-20","F","N9206672","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("472","228","MOHAMED ASLAM","0000-00-00","M","Z1980263","10","0000-00-00","0000-00-00","0000-00-00","","33000","0","0","1500","0","0","34500","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("473","229","JESNI","2016-09-20","F","N4797473","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("474","230","MOHAMMED ATIF","0000-00-00","M","L6242738","10","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("475","231","SHAMNAS SALIM","2016-09-20","M","J5808373","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("476","232","SHERSHAD","2016-09-20","M","H2289981","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("477","232","MUHAMMED ASHIK","2016-09-20","M","G9707646","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("478","233","SANTHA","2016-09-20","F","M6047012","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("479","233","HASNA","2016-09-20","F","K2449588","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","-100","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("480","234","NOUSHERE","2016-09-20","M","M3925390","10","2016-09-20","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("481","235","NOUFAL","2016-09-20","M","L5479760","5","2016-09-20","0000-00-00","0000-00-00","","5250","0","0","-150","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("482","236","MAJIDA , LASIN , LAIBA","0000-00-00","M","N1847699 , N2128473 , N2368777","5","0000-00-00","0000-00-00","0000-00-00","","42600","0","0","2400","0","0","45000","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("483","237","AJMAL","2016-09-20","M","K1961060","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("484","237","ASGAR","2016-09-20","M","H6100414","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("485","237","ASIF","2016-09-20","M","K036848","10","2016-09-20","0000-00-00","0000-00-00","","14100","0","0","400","0","0","14500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("498","245","SAID HUMAM","2016-09-22","M","L8905859","5","2016-09-22","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("499","245","SHAFEER","2016-09-22","M","L1309942","5","2016-09-22","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("500","245","ABIN THOMAS","2016-09-22","M","N9065681","5","2016-09-22","0000-00-00","0000-00-00","","5075","0","0","25","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("501","246","SHAMSUDHEEN","2016-09-22","M","L9685743","10","2016-09-22","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("502","247","SEENATH","2016-09-22","M","P1506921","10","2016-09-22","0000-00-00","0000-00-00","","14600","0","0","200","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("503","248","DADU","2016-09-22","M","K5287361","10","2016-09-22","0000-00-00","0000-00-00","","14100","0","0","600","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("504","249","NAJEEB","2016-09-22","M","H0537872","11","2016-09-22","0000-00-00","0000-00-00","","15600","0","0","200","0","0","15800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("505","250","SAYYID MUHAMMED JIFRI","2016-09-22","M","G3872348","10","2016-09-22","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("506","251","SHABIN","2016-09-22","M","N6825621","10","2016-09-22","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("507","251","NAHAS","2016-09-22","M","H6098882","10","2016-09-22","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("508","252","AMAL PARATH","2016-09-22","M","P039069","10","2016-09-22","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("509","253","MUHABIR","2016-09-22","M","M6486378","10","2016-09-22","0000-00-00","0000-00-00","","14100","0","0","600","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("511","254","HASSAINAR","0000-00-00","M","H63896913","17","0000-00-00","0000-00-00","0000-00-00","","0","0","0","22000","0","0","22000","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("513","255","HASSAINAR","0000-00-00","M","H63896913","17","0000-00-00","0000-00-00","0000-00-00","","18900","0","0","3100","0","0","22000","0000-00-00","","0","","1","8","2");

DROP TABLE IF EXISTS visafor;
CREATE TABLE `visafor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visafor` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
INSERT INTO visafor VALUES("1","Dubai- 30 days","1","5","2");
INSERT INTO visafor VALUES("2","UAE  30 Days","2","6","2");
INSERT INTO visafor VALUES("3","UAE 90 Days","2","6","2");
INSERT INTO visafor VALUES("4","30 DAYS DXB WITH OTB","1","10","2");
INSERT INTO visafor VALUES("5","30 DAYS DXB WITHOUT OTB","1","10","2");
INSERT INTO visafor VALUES("6","30 DAYS DXB SETTING","1","10","2");
INSERT INTO visafor VALUES("7","30 DAYS DXB LSSUER","1","10","2");
INSERT INTO visafor VALUES("8","30 DAYS DXB MULTIPLE","1","10","2");
INSERT INTO visafor VALUES("9","90 DAYS DXB  WITH OTB","1","10","2");
INSERT INTO visafor VALUES("10","90 DAYS DXB  WITHOUT OTB","1","10","2");
INSERT INTO visafor VALUES("11","90 DAYS DXB SETTING","1","10","2");
INSERT INTO visafor VALUES("12","90 DYAS DXB LESSUER","1","10","2");
INSERT INTO visafor VALUES("14","90 DAYS DXB  MULTIPLE","1","10","2");
INSERT INTO visafor VALUES("15","30 DAYS QATAR TOURIST","1","10","2");
INSERT INTO visafor VALUES("16","30 DAYS QATAR BUSINESS","1","10","2");
INSERT INTO visafor VALUES("17","90 DAYS QATAR BUSINESS","1","10","2");
INSERT INTO visafor VALUES("18","10 DAYS OMAN","1","10","2");
INSERT INTO visafor VALUES("19","30 DAYS OMAN","1","10","2");
INSERT INTO visafor VALUES("20","SAUDI VISA STAMPING","1","10","2");
INSERT INTO visafor VALUES("21","SAUDI VISA STAMPING FAMILY","1","10","2");
INSERT INTO visafor VALUES("22","SAUDI VISA STAMPING RESIDENTIAL","1","10","2");
INSERT INTO visafor VALUES("23","30 DAYS SAUDI","1","10","2");
INSERT INTO visafor VALUES("24","90 DAYS SAUDI MULTIPLE","1","10","2");
INSERT INTO visafor VALUES("25","30 DAYS MALASIA","1","10","2");
INSERT INTO visafor VALUES("26","30 DAYS SREELANKA","1","10","2");
INSERT INTO visafor VALUES("27","UAE ATTASTATION","1","10","2");
INSERT INTO visafor VALUES("28","QATAR ATTASTATION","1","10","2");
INSERT INTO visafor VALUES("29","SAUDI ATTASTATION","1","10","2");
INSERT INTO visafor VALUES("30","14 DAYS BAHARAIN","1","10","2");
INSERT INTO visafor VALUES("31","30 DAYS BAHARAIN","1","10","2");
INSERT INTO visafor VALUES("32","OTB NORMAL","1","10","2");
INSERT INTO visafor VALUES("33","OTB URGENT","1","10","2");
INSERT INTO visafor VALUES("34","UMRA","4","19","2");
INSERT INTO visafor VALUES("35","DUBAI","4","23","2");
INSERT INTO visafor VALUES("36","SAUDI","4","23","2");
INSERT INTO visafor VALUES("37","OMAN EMIGRATION","1","8","2");
INSERT INTO visafor VALUES("38","KUWAIT STAMPING","1","8","2");
INSERT INTO visafor VALUES("39","DUBAI ONE MONTH","3","34","2");
INSERT INTO visafor VALUES("40","DUBAI THREE MONTH","3","34","2");
INSERT INTO visafor VALUES("41","UMRA","3","34","2");
INSERT INTO visafor VALUES("42","Al Ojoor","1","36","2");



								/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
								/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
								/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
								/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
								/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
								/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
								/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
								
						