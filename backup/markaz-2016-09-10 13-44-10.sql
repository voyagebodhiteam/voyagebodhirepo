/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
								/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
								/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
								/*!40101 SET NAMES utf8 */;
								/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
								/*!40103 SET TIME_ZONE='+00:00' */;
								/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
								/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
								/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
								/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
								DROP TABLE IF EXISTS academicyear;
CREATE TABLE `academicyear` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `fromYear` varchar(20) NOT NULL,
  `toYear` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
INSERT INTO academicyear VALUES("2","new year2","2016","2017","YES");

DROP TABLE IF EXISTS account_group;
CREATE TABLE `account_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `acGroup` text NOT NULL,
  `parent` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
INSERT INTO account_group VALUES("1","Account","0","0","0");
INSERT INTO account_group VALUES("2","Balance Sheet","1","0","0");
INSERT INTO account_group VALUES("3","Profit & Loss Account","1","0","0");
INSERT INTO account_group VALUES("4","Assets","2","0","0");
INSERT INTO account_group VALUES("5","Liabilities","2","0","0");
INSERT INTO account_group VALUES("6","Current Assets","4","0","0");
INSERT INTO account_group VALUES("7","Fixed Assets","4","0","0");
INSERT INTO account_group VALUES("8","Cash In Hand","6","0","0");
INSERT INTO account_group VALUES("9","Deposits","6","0","0");
INSERT INTO account_group VALUES("10","Investments","6","0","0");
INSERT INTO account_group VALUES("11","Loans & Advances (asset)","6","0","0");
INSERT INTO account_group VALUES("12","Bank A/c","6","0","0");
INSERT INTO account_group VALUES("13","Expenses","3","0","0");
INSERT INTO account_group VALUES("14","Income","3","0","0");
INSERT INTO account_group VALUES("15","Cash","8","0","0");
INSERT INTO account_group VALUES("16","Capital","5","0","0");
INSERT INTO account_group VALUES("17","Current Liabilities","5","0","0");
INSERT INTO account_group VALUES("18","Loans (liability)","5","0","0");
INSERT INTO account_group VALUES("19","Suspense Accounts","5","0","0");
INSERT INTO account_group VALUES("20","Bank OD A/C","17","0","0");
INSERT INTO account_group VALUES("21","Duties & Taxes","17","0","0");
INSERT INTO account_group VALUES("22","Journal Accounts","17","0","0");
INSERT INTO account_group VALUES("23","Provisions","17","0","0");
INSERT INTO account_group VALUES("24","Direct Expense","13","0","0");
INSERT INTO account_group VALUES("25","Indirect Expense","13","0","0");
INSERT INTO account_group VALUES("26","Direct Income","14","0","0");
INSERT INTO account_group VALUES("27","Indirect Income","14","0","0");
INSERT INTO account_group VALUES("28","Fittings","7","0","0");
INSERT INTO account_group VALUES("29","Advertisement","25","0","0");
INSERT INTO account_group VALUES("30","Donation Given","25","0","0");
INSERT INTO account_group VALUES("31","Electricity Bills","25","0","0");
INSERT INTO account_group VALUES("32","Miscellaneous Expense","25","0","0");
INSERT INTO account_group VALUES("33","Office Expense","25","0","0");
INSERT INTO account_group VALUES("34","Other Expenses","25","0","0");
INSERT INTO account_group VALUES("35","Telephone Bills","25","0","0");
INSERT INTO account_group VALUES("36","Other Income","27","0","0");
INSERT INTO account_group VALUES("37","Customer/ Sundry Debtors","6","0","0");
INSERT INTO account_group VALUES("38","Shipping Agents","17","0","0");
INSERT INTO account_group VALUES("39","Staff Deposits","17","0","0");
INSERT INTO account_group VALUES("40","Supplier/ Sundry Creditors","17","0","0");
INSERT INTO account_group VALUES("41","Discount Given ","24","0","0");
INSERT INTO account_group VALUES("42","Job Works","24","0","0");
INSERT INTO account_group VALUES("43","Purchases","24","0","0");
INSERT INTO account_group VALUES("44","Courier","26","0","0");
INSERT INTO account_group VALUES("45","Discount Recieved","26","0","0");
INSERT INTO account_group VALUES("46","Sales","26","0","0");
INSERT INTO account_group VALUES("47","Services","26","0","0");
INSERT INTO account_group VALUES("48","Commission Agent","25","0","0");
INSERT INTO account_group VALUES("49","Incentives Given","25","0","0");
INSERT INTO account_group VALUES("50","Printing Expense","25","0","0");
INSERT INTO account_group VALUES("51","Rent Given","25","0","0");
INSERT INTO account_group VALUES("52","Salaries","25","0","0");
INSERT INTO account_group VALUES("53","Sales Expenses","25","0","0");
INSERT INTO account_group VALUES("54","Tax Paid","25","0","0");
INSERT INTO account_group VALUES("55","Tax Recieved","25","0","0");
INSERT INTO account_group VALUES("56","Incentives Recieved","27","0","0");
INSERT INTO account_group VALUES("57","Branches","6","0","0");
INSERT INTO account_group VALUES("58","Main Branches","17","0","0");

DROP TABLE IF EXISTS accounts;
CREATE TABLE `accounts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `agentId` int(11) NOT NULL,
  `accountGroupId` int(11) NOT NULL,
  `accountName` varchar(200) NOT NULL,
  `openingBal` double NOT NULL,
  `payType` varchar(20) NOT NULL,
  `masterLedger` int(11) NOT NULL,
  `addDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `description` text NOT NULL,
  `contactPerson` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address2` text NOT NULL,
  `pin2` varchar(20) NOT NULL,
  `city2` varchar(200) NOT NULL,
  `customerKey` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `accountGroupId` (`accountGroupId`),
  CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`accountGroupId`) REFERENCES `account_group` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=latin1;
INSERT INTO accounts VALUES("1","0","19","Suspense","0","Credit","1","2016-03-22","1","0","0","Suspense Account","","","","","","","","","","","");
INSERT INTO accounts VALUES("2","0","8","Cash Account","0","Credit","1","2016-03-22","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("3","0","24","Salary Payments","0","Credit","1","2016-03-22","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("4","0","14","Airline Receivable","0","Debit","1","2016-04-05","1","0","0","Airline charge received","","","","","","","","","","","");
INSERT INTO accounts VALUES("5","0","13","Airline Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("6","0","14","Visa Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("7","0","13","Visa Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("8","0","14","Package Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("9","0","13","Package Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("10","0","14","Hotel Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("11","0","13","Hotel Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("12","0","14","Transport Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("13","0","13","Transport Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("14","0","14","Misc Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("15","0","13","Misc Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("16","0","25","Discount Given","0","Credit","1","2016-05-25","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("17","0","27","Discount Received","0","Credit","1","2016-05-25","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("18","0","25","Tax","0","Credit","1","2016-06-24","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("19","0","25","Tds","0","Credit","1","2016-06-24","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("20","0","27","Tac","0","Credit","1","2016-06-24","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("21","0","25","Package Extra Chrg","0","Credit","1","2016-06-29","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("22","0","13","Other Purchase","0","Credit","0","2016-08-16","1","0","0","","","","","","","","","","","","");
INSERT INTO accounts VALUES("79","0","48","WC","0","Credit","0","2016-08-31","2","1","2","","WC","0","","","","0","","","","","");
INSERT INTO accounts VALUES("80","0","48","None","0","Credit","0","2016-09-01","6","2","2","","None","0","","","","0","","","","","");
INSERT INTO accounts VALUES("81","80","40","MTM  CCJ","0","Credit","0","2016-09-01","6","2","2","","MR FAISAL","CALICUT","","CALICUT","0495 4011414","9605198598","faizaltravelmart@gmail.com","","","","");
INSERT INTO accounts VALUES("82","80","40","MTM VISA","0","Credit","0","2016-09-01","6","2","2","","MR SAHAD","MARKAZ COMPLES ANNEX\nMAVOOR ROAD CALICUT","673004","CALICUT","0495 4011414","7736538597","mtmsahad@gmail.com","","","","");
INSERT INTO accounts VALUES("83","80","40","Creative Tours & Travels - KODUNGALLUR","0","Credit","0","2016-09-01","6","2","2","","Ms SABITHA","Creative Tours & Travels (India) Pvt. Ltd.\n PRABHA BUILDING, NORTH NADA,\n KODUNGALLUR - 680664\n DIST. - TRISSUR\n TEL : 0480 3005013","680664","KODUNGALLUR","04803005013","9947610703","sabithakm2010@gmail.com","","","","");
INSERT INTO accounts VALUES("84","80","40","LAWSON TRAVEL","0","Credit","0","2016-09-01","6","2","2","SG Ticket  shintu.v@lawson.travel","MR AZEEZ MP","Mangaden center, OPP. Asma tower , Mavoor road, Calicut - 673 004\n\nazeez.mp@lawson.travel\nSG Ticket  shintu.v@lawson.travel","673004","CALICUT","495-4093013","9847759510","","","","","");
INSERT INTO accounts VALUES("85","80","40","FLYHIND CCJ","0","Credit","0","2016-09-01","6","2","2","","Nikhil Shyam","1st Floor, Shobha Tower, Mavoor Road, Calicut - 673 004\n\nnikhil@flyhind.in","673004","CALICUT","0495 4025555","9746744717","nikhilsyam59@gmail.com","","","","");
INSERT INTO accounts VALUES("86","80","37","ABOOBACKAR  E P","0","Credit","0","2016-09-01","6","2","2","","ABOOBACKAR","CHAMRAVATTAM","000000","CHAMRAVATTAM","00971555013564","00971555013564","cholayilparambil@gmail.com","NON","000000","NON","");
INSERT INTO accounts VALUES("87","80","37","ASAD KC (BAVUTTY)","0","Credit","0","2016-09-01","6","2","2","","ASAD","RANDATHANI\nPOVANCHINA","676510","RANDATHANI","9020570402","920739224","asad.randathani@gmail.com","RANDATHANI","676510","RANDATHANI","");
INSERT INTO accounts VALUES("88","80","37","AHAMED KUTTY - KOTTAKKAL -KUTTIPPURAM","0","Credit","0","2016-09-08","6","2","2","","AHAMED KUTTY","KOTTAKKAL \nKUTTIPPURAM","676503","KOTTAKKAL","9947039616","00971559278031","mtmvettichira@gmail.com","KALLANGATTU KUZHIYIL","676503","KOTTAKKAL","");
INSERT INTO accounts VALUES("89","80","37","MOHAMED ALI MT","0","Credit","0","2016-09-03","6","2","2","","MOHAMED ALI MT","MACHINCHERY THOOMBIL","676510","RANDATHANI","00971508403056","00971508403056","cholayilparambil@gmail.com","RANDATHANI","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("90","80","37","RAHEEM HAJI  CV","0","Credit","0","2016-09-03","6","2","2","","MANU","VATTA PARAMBU","676510","RANDATHANI","7034637861","00971504959957","cholayilparambil@gmail.com","VATTA PARAMBU","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("91","80","37","RASHEED PARAMBATH","0","Credit","0","2016-09-03","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","00971551777134","00971551777134","mateenasm134@gmail.com","RANDATHANI","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("92","80","37","MUHAMMED MUKTHAR  MALIYEKKAL","0","Credit","0","2016-09-05","6","2","2","","HANEEFA","ABU DHABI","676510","RANDATHANI","9526187272","00971558918388","haneefa237@gmail.com","RANDATHANI","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("93","79","37","GAMA HOLIDAYS CCJ","35900","Debit","0","2016-09-05","10","1","2","","ARSHAD","KOZHIKODE","000000","KOZHIKODE","+91 903 7777 019 | 9","+91 903 7777 019 | 9","markaztravelmart@gmail.com","KOZHIKODE","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("94","79","40","SALIL PURCHASE","83925","Debit","0","2016-09-05","10","1","2","","SALIL","SHOP NO.2, Millenium Society\n\nRoad No.3, Liberty Garden\n\nMalad (west), Mumbai-400064","400064","MUMBAI","91-22-28892250 / 225","9892519222/998724422","salilcs84@gmail.com","SHOP NO.2, Millenium Society\n\nRoad No.3, Liberty Garden\n\nMalad (west), Mumbai-400064","400064","MUMBAI","");
INSERT INTO accounts VALUES("95","79","37","CREATIVE KODUNGALLUR","0","Debit","0","2016-09-05","10","1","2","","SHANMUGAN","VISA CONSULTANT \n101,Prabha Building,Opp, Municipal Bus stand,\nKodungallur-680 664,Thrissur Dist.Kerala ,","000000","KODUNGALLUR","9349803028","0480-3005047, 300500","markaztravelmart@gmail.com","TRISSUR","000000","TRISSUR","");
INSERT INTO accounts VALUES("96","79","37","SAFUVAN STAFF","0","Debit","0","2016-09-08","10","1","2","","SAFUVAN","AIKARAPADI","000000","MALAPPUARAM","9995959734","9995959734","safuvan.m4@gmail.com","AIKARAPPADI","000000","MALAPPURAM","");
INSERT INTO accounts VALUES("97","79","40","UNKNWON SUPPLIER","0","Credit","0","2016-09-05","10","1","2","","0","0","000000","0","0","8547116314","markaztravelmart@gmail.com","0","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("98","80","37","JAYAFER AINIKUNNAN","0","Credit","0","2016-09-05","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","9895936252","9895936252","cholayilparambil@gmail.com","AAA","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("99","79","37","ALHIND COK","0","Debit","0","2016-09-06","10","1","2","","AMOOLYA","Alhind Tours & Travels Pvt Ltd\nElmar Square\nAbove HDFC BANK ,M.G.Road,Ravipuram .Cochin-16","000000","COCHIN","0484-3082025,3082024","9496094990, 94960180","helpdesk2.cok@alhindonline.com","COCHIN","000000","COCHIN","");
INSERT INTO accounts VALUES("100","80","37","ABOOBACKER KARINGAPPARA","0","Credit","0","2016-09-05","6","2","2","","ABOOBACKER","VETTICHIRA","676510","VETTICHIRA","9846338837","9846338837","mtmvettichira@gmail.com","vettichira","676510","vettichira","");
INSERT INTO accounts VALUES("101","79","37","SALIL SALES","0","Debit","0","2016-09-05","10","1","2","","SALIL","SHOP NO.2, Millenium Society\n\nRoad No.3, Liberty Garden\n\nMalad (west), Mumbai-400064\n\nPh: +91-22-28892250 / 2255","000000","MALAD,MUMBAI","9892519222","9892519222/998724422","salilcs84@gmail.com","SHOP NO.2, Millenium Society\n\nRoad No.3, Liberty Garden\n\nMalad (west), Mumbai-400064\n\nPh: +91-22-28892250 / 2255","000000","MUMBAI","");
INSERT INTO accounts VALUES("102","79","40","TRAVEL PARTNER PURCHASE","90100","Debit","0","2016-09-05","10","1","2","","SHAMNAS","Sky Tower, Near OmanAir...ways,\nBank Rd, Mavoor Road Jn Calicut-1","000000","KOZHIKODE","495 4016677","9544053344","CCJ@travelpartnerholidays.com","Sky Tower, Near OmanAir...ways,\nBank Rd, Mavoor Road Jn Calicut-1","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("103","79","37","MTM VETTICHIRA","2500","Debit","0","2016-09-05","10","1","2","","MANU","VETTICHIRA","000000","MALAPPURAM","04942617140","04942617140","mtmvettichira@gmail.com","VETTICHIRA","000000","MALAPPURAM","");
INSERT INTO accounts VALUES("104","0","48","KKH","0","Credit","0","2016-09-05","8","1","2","","KKH","WAYANAD","","","","0000","","","","","");
INSERT INTO accounts VALUES("105","104","37","HARIS","0","Credit","0","2016-09-05","8","1","2","","HARIS","WAYANAD","673637","WAYANAD","000","00","safuvan.m4@gmail.com","WAY","637637","WAY","");
INSERT INTO accounts VALUES("106","0","48","FAIZAL","0","Credit","0","2016-09-05","17","4","2","","FAIZAL","...","","","","9605198598","","","","","");
INSERT INTO accounts VALUES("107","0","48","SIRAJ SAQUAFI","0","Credit","0","2016-09-05","17","4","2","","SIRAJ SAQUAFI","CHERAPURAM","","","","9961953870","","","","","");
INSERT INTO accounts VALUES("108","106","37","FAIZEEE","0","Debit","0","2016-09-05","17","4","2","","AKHIL","NN","673004","CCJ","9988774455","665544778899","FAIZALTRAVELMART@GMAIL.COM","MMM","673004","673004","");
INSERT INTO accounts VALUES("109","0","48","arshak","0","Credit","0","2016-09-05","21","4","2","","arshak","mele edakkat house","","","","8089684701","","","","","");
INSERT INTO accounts VALUES("110","0","48","AKI","0","Credit","0","2016-09-05","17","4","2","","AKI","GUDALUR","","","","9486747270","","","","","");
INSERT INTO accounts VALUES("111","106","40","IX","0","Credit","0","2016-09-05","17","4","2","","","","","","","8855223366","","","","","");
INSERT INTO accounts VALUES("112","110","40","AL HIND","1000","Debit","0","2016-09-05","17","4","2","","","","","","","1","","","","","");
INSERT INTO accounts VALUES("113","0","48","ZIYARA","0","Credit","0","2016-09-05","17","4","2","","ZIYARA","WAYANAD","","","","9999009990","","","","","");
INSERT INTO accounts VALUES("114","79","37","CREATIVE THIRUVANANDHAPURAM","0","Debit","0","2016-09-09","10","1","2","","Shahina Afsa","Shahina Afsal\n VISA CONSULTANT\n101,Prabha Building,Opp, Municipal Bus stand,\nKodungallur-680 664,Thrissur Dist.Kerala ,\n0480-3005047, 3005000(60 lines) 3006000(40 lines) 9349803028","000000","KODUNGALLUR","0480-3005047,","9349803028","visakdr@creativeholidaysindia.com","KODUNGLLUR","000000","0","");
INSERT INTO accounts VALUES("115","107","37","SIRAJ CHERAPURAM","0","Credit","0","2016-09-05","19","4","2","NOT","SIRAJ","KOZHIKODE\nMAVOOR ROAD","673004","KOZHIKODE","0","9845488545","mtmrecruits@gmail.com","KOZHIKODE\nMAVOOR ROAD","673004","KOZHIKODE","");
INSERT INTO accounts VALUES("116","107","40","AL UJOOR","0","Credit","0","2016-09-05","19","4","2","UMRA VISA","ABBAS","MAKKA","","","","00966000000000","","","","","");
INSERT INTO accounts VALUES("117","0","48","MARVA TRAVELS","0","Credit","0","2016-09-05","18","4","2","","MARVA TRAVELS","KERAla","","","","99999999","","","","","");
INSERT INTO accounts VALUES("118","110","37","AKASH","0","Credit","0","2016-09-05","22","4","2","","AAA","AA","123456","0","123","123","AKHILKP496@GMAIL.COM","AAA\n\nASS","123456","AA","");
INSERT INTO accounts VALUES("119","117","37","HYDER","0","Credit","0","2016-09-05","18","4","2","","HYDER","KERALA","673638","CALICUT","9999999","0","hydermkm@gmail.com","KERALA","673638","CALICUT","");
INSERT INTO accounts VALUES("123","79","37","MOHAMMED ISMAIL","0","Debit","0","2016-09-06","8","1","2","","MOHAMMED ISMAIL","ABDUL HAFEEZE","000000","0","7598109313","07418443464","MTMVISA@GMAIL.COM","WC","000000","WC","");
INSERT INTO accounts VALUES("124","79","37","MUHAMMED KUTTIKKA E","0","Credit","0","2016-09-06","5","1","2","","Muhammed Kuttika","edavannapara","673004","edavannapara","9446768538","9446768538","mtmrecruits@gmail.com","Edavannapara","673004","malappuram","");
INSERT INTO accounts VALUES("125","79","40","counter16","0","Credit","0","2016-09-06","5","1","2","","","","","","04952723831","04952723831","","","","","");
INSERT INTO accounts VALUES("126","80","37","SALEEM AAPARAMBIL","0","Credit","0","2016-09-06","6","2","2","","MANU","MTM","676510","RANDATHANI","00971563883556","00971563883556","seliapm@gmail.com","Abu dhabi","000000","Abudhabi","");
INSERT INTO accounts VALUES("127","79","40","ALTA CCJ","750","Credit","0","2016-09-09","10","1","2","","JAMSHAD","CD TOWER,KOZHIKODE","000000","KOZHIKODE","0495405052","9746084777","info@altatravel.in","CD TOWER,KOZHIKODE","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("128","80","37","SIDHEEQUE PUTHUPARAMBIL","0","Credit","0","2016-09-06","6","2","2","","MANU","RANDATHANI","676510","RANDATHANI","00971558848321","00971558848321","sidheeque321@gmail.com","RANDATHANI","676510","RANDAHTNAI","");
INSERT INTO accounts VALUES("129","79","37","FLYHIND THALASSERY","0","Debit","0","2016-09-06","9","1","2","","LANCHIMA","Sajad Tower , \nOpp: Old Bus Stand,\n\nHospital road , \n\nThalassery 673 101\n\nKerala, India.","673004","THALASSERY","9746744748","9746744746","flyhindtly@gmail.cpm","Sajad Tower , \nOpp: Old Bus Stand,\n\nHospital road , \n\nThalassery 673 101\n\nKerala, India.","673004","thalssery","");
INSERT INTO accounts VALUES("130","79","37","MAUSIM TRAVELS TIRUR","0","Debit","0","2016-09-06","9","1","2","","RUMANA","Mausim Travel and Tours Pvt Ltd |\n\nCity Tower | City Jn |  Tirur | Malappuram Dt | Kerala - INDIA\n\nTel + 91 494 654 1666 | +91 494 654 2666 | Mob  + 91 9288241666 | + 91 9497221666 |","673005","TIRUR","9497221666","9497221666","visa@mausim.in","Mausim Travel and Tours Pvt Ltd |\n\nCity Tower | City Jn |  Tirur | Malappuram Dt | Kerala - INDIA\n\nTel + 91 494 654 1666 | +91 494 654 2666 | Mob  + 91 9288241666 | + 91 9497221666 |","673005","tirur","");
INSERT INTO accounts VALUES("131","0","48","SAFUVAN","0","Credit","0","2016-09-06","2","1","2","","SAFUVAN","AIKARAPPADI","","","","9995959734","","","","","");
INSERT INTO accounts VALUES("132","79","40","ALPHA TOURS AND TRAVELS","0","Debit","0","2016-09-07","9","1","2","","SALAMKKA","Alpha Travel & Tours\n\nMob – +919400655946 / office – +914952435946\nE-Mail – visa@alphaindia.biz sales@alphaindia.biz alphatravels222@gmail.com","673005","CALICUT","9400655946","9400655946","","","","","");
INSERT INTO accounts VALUES("133","79","12","FEDERAL BANK","0","Debit","0","2016-09-07","2","1","2","","MUHAMMED","regd.ofc:Aluva Branch:Puthiyara IFSC:FDRL0001306","673004","KOZHIKODE","04952720888","9895925503","pya@fderalbank.co.in","regd.ofc:Aluva Branch:Puthiyara IFSC:FDRL0001306","673004","KOZHIKODE","12#AC1133");
INSERT INTO accounts VALUES("134","79","12","SOUTH INDIAN BANK","0","Debit","0","2016-09-07","2","1","2","","BOBAN V.J.","Kozhikode (nri) Branch,5 3410h4,galleria Trade Centre,near Markaz Complex, Kozhikode, Kerala, Pin-673004","673004","KOZHIKODE","495-2720588","495-2720588","CUSTOMERCARE@SIB.CO.IN","Kozhikode (nri) Branch,5 3410h4,galleria Trade Centre,near Markaz Complex, Kozhikode, Kerala, Pin-673004","673004","KOZHIKODE","12#AC1134");
INSERT INTO accounts VALUES("135","79","12","STATE BANK","0","Debit","0","2016-09-07","2","1","2","","","","","","","","","","","","12#AC1135");
INSERT INTO accounts VALUES("136","79","37","HANEEFA","0","Debit","0","2016-09-07","9","1","2","","MUHAMMED HANEEF","BLUE BERRY INTERIOR DESIGNS","673004","DUBAI","971527433307","971 4 2640240","haneefa@blueberryinterior.com","dubai","673001","DUBAI","");
INSERT INTO accounts VALUES("137","80","40","KSEB","0","Credit","0","2016-09-07","6","2","2","","","","","PUTHANATHANI","","0494 2615115","","","","","");
INSERT INTO accounts VALUES("138","79","37","MTT TRAVELS","0","Debit","0","2016-09-07","10","1","2","","X","MTT Holidays Calicut\n\n1st Floor, Vyaparabhavan,Bank Road, Calicut- 673001","000000","KOZHIKODE","0495-3062656","9349384858/702506002","info@mttholidays.com","MTT Holidays Calicut\n\n1st Floor, Vyaparabhavan,Bank Road, Calicut- 67300","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("139","80","37","ABOOBACKER KOORIPPARAMBIL","0","Credit","0","2016-09-07","6","2","2","","MANU","AA","676510","ABU DHBAI","00971503287504","00971503287504","a.backer125@gmail.com","ABUDHABI","000000","ABUDHABI","");
INSERT INTO accounts VALUES("140","79","40","GULF INDIA TRAVELS","0","Debit","0","2016-09-08","9","1","2","","NAVAS","Muhammed Navas | +974 70301006 DOHA | +91 70 255 666 00\n\nGulf Air Travels | Pattambi, Palakkad, Kerala +91 466 2213430 | Cochin, Kerala +91 484 4070430","673004","","","484 4070430","","","","","");
INSERT INTO accounts VALUES("141","79","37","LAWASON TRAVELS COCHIN","0","Debit","0","2016-09-08","9","1","2","","SHIJI","SHIJI\nVisa Consultant\n\n39/6842,Lawson Plaza, Ravipuram, M.G.Road, Kochi - 682015.\n\nT  :  91-484-4091000","673305","KOCHIN","04844091000","04844091000","visa.cok@lawsononline.com","visa.cok@lawsononline.com","673004","cochin","");
INSERT INTO accounts VALUES("142","79","37","SAHAD STAFF","0","Debit","0","2016-09-08","9","1","2","","SAHD","ALIYAMINTAKATH HOUSE \nPALAPPETTY","679598","PALAPPETTY","9567656784","7736538597","mtmsahad@gmail.com","vadakkepurath house","689579","palappetty","");
INSERT INTO accounts VALUES("143","79","37","ALI PATTANMARU VALAPPIL","0","Debit","0","2016-09-08","10","1","2","","ALIL","AMBALA VAYAYAL","673593","KOZHIKODE","00","000","MUHAMMEDUNAISPM007@GMAIL.COM","AMBALA VAYAYAL,WAYANDU","673593","WAYANAD","");
INSERT INTO accounts VALUES("144","79","37","AHMED ORMUNDAKKA","0","Debit","0","2016-09-08","10","1","2","","AHMED ORMUNDAKKA","THARUVANA,KARANTHUR,KUNNAMANGALAM,KOZHIKODE","673571","KOZHIKODE","9400501168","9400501168","markaztravelmart@gmail.com","THARUVANA,KARANTHUR,KUNNAMANGALAM,KOZHIKODE","673571","KOZHIKODE","");
INSERT INTO accounts VALUES("145","79","37","RIYAZ MKC","0","Debit","0","2016-09-08","9","1","2","","RIYAZ","Mohamed Riyas A.A\nCEO Office Manager\nMarkaz Knowledge City\nwww.markazcity.com\n09544712226","673004","CALICUT","9544712226","9544712226","riyascalicut2@gmail.com","riyascalicut2@gmail.com","673004","CALICUT","");
INSERT INTO accounts VALUES("146","79","40","MARKAZ TRAVEL MART","0","Debit","0","2016-09-08","10","1","2","","SIDHEEK","MARKAZ COMPLEX ,KOZHIKODE","000000","KOZHIKODE","04954011515","04954011414","markaztravelmart@gmail.com","MARKAZ COMPLEX ,KOZHIKODE","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("147","79","40","SHAMNAS PURCHASE","1450","Debit","0","2016-09-08","10","1","2","","SHAMNAS","SHANAS","000000","KOZHIKODE","0495 4050502","9895090917","anawallahtravel@gmail.com","SHAMNAS","000000","KOZHIKODE","");
INSERT INTO accounts VALUES("148","79","37","mohammed","0","Debit","0","2016-09-08","8","1","2","","mohammed","dervekshan","682551","lakshadeep","9447235706","9447235706","MTMVISA@GMAIL.COM","lakshadeep","000000","lakshadeep","");
INSERT INTO accounts VALUES("149","131","37","fathima travels","0","Debit","0","2016-09-08","8","1","2","","GAFOOR","THAMARASSERY","000000","THAMARASSERY","9447275477","9447275477","MTMVISA@GMAIL.COM","THAMARASSERY","000000","THAMARASSERY","");
INSERT INTO accounts VALUES("150","80","37","ABDUL KAREEM VALIYA VALAPPIL","0","Credit","0","2016-09-09","6","2","2","","MANU","PAVITTAPPURAM","000000","ABU DHBAI","00971504913624","9995122177","kareem_va@yahoo.in","ABUDHABI","000000","ABUDHABI","");
INSERT INTO accounts VALUES("151","79","37","Walking Passenger","0","Credit","0","2016-09-09","5","1","2","","Walking","walking","673004","kozhikode","0495","0495","tssidheeq@gmail.com","walking","673004","kozhkode","");
INSERT INTO accounts VALUES("152","131","37","SALALAH TRAVELS","0","Debit","0","2016-09-10","8","1","2","","HANEEFA","HANEEFA","000000","HANEEFA","9844840707","9535500007","MTMVISA@GMAIL.COM","HANEEFA","000000","HANEEFA","");

DROP TABLE IF EXISTS airline_flight;
CREATE TABLE `airline_flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlinePassId` int(11) NOT NULL,
  `airlineName` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `journeyDate` date NOT NULL,
  `sectorFrom` varchar(100) NOT NULL,
  `sectorTo` varchar(100) NOT NULL,
  `classType` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `tax` varchar(30) NOT NULL,
  `yq` float NOT NULL,
  `procCharge` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlinePassId` (`airlinePassId`),
  CONSTRAINT `airline_flight_ibfk_2` FOREIGN KEY (`airlinePassId`) REFERENCES `airline_passenger` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
INSERT INTO airline_flight VALUES("1","1","2","IX - 348","NHI1RD","2016-09-16","AUH","CCJ","Y","1000","0","0","0","2","6","2");
INSERT INTO airline_flight VALUES("2","2","3","EY398/254","607-9164761434","2016-12-07","DOH","CCJ","Y","11776","2239","0","35","2","6","2");
INSERT INTO airline_flight VALUES("3","3","2","IX346","AEDKL6","2016-09-05","DXB","CCJ","Y","14730","0","0","270","2","6","2");
INSERT INTO airline_flight VALUES("4","4","1","AI993","098-9164977278","2016-12-22","CCJ","DXB","Y","1471","834","6775","420","2","6","2");
INSERT INTO airline_flight VALUES("7","7","13","FD171","CW3YVB","2016-10-17","COK","DMK","Y","8870","0","0","730","2","6","2");
INSERT INTO airline_flight VALUES("8","7","13","FD170","CW3YVB","2016-10-19","DMK","COK","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("10","9","1","AI998","098-9164977299","2016-09-05","SHJ","CCJ","Y","2370","732","6028","570","2","6","2");
INSERT INTO airline_flight VALUES("12","11","3","EY257","607-9164977330","2016-10-04","CCJ","AUH","Y","5006","1019","4025","250","2","6","2");
INSERT INTO airline_flight VALUES("13","12","3","EY257","607-9164977328","2016-10-04","CCJ","AUH","Y","5006","1019","4025","250","2","6","2");
INSERT INTO airline_flight VALUES("14","13","12","9W207","589-9164977329","2017-01-01","SHJ","CCJ","Y","2191","2175","4019","315","2","6","2");
INSERT INTO airline_flight VALUES("29","23","1","AI582","098-9164977343","2016-09-04","CCJ","BOM","Y","11024","3092","7714","470","2","6","2");
INSERT INTO airline_flight VALUES("30","23","1","AI921","098-9164977343","2016-09-04","BOM","RUH","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("31","24","1","AI582","098-9164977344","2016-09-04","CCJ","BOM","Y","11024","3092","7714","470","2","6","2");
INSERT INTO airline_flight VALUES("32","24","1","AI921","098-9164977344","2016-09-04","BOM","RUH","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("33","25","1","AI582","098 9164977342","2016-09-04","CCJ","BOM","Y","12866","3205","7714","715","2","6","2");
INSERT INTO airline_flight VALUES("34","25","1","AI921","098 9164977342","2016-09-04","BOM","RUH","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("35","26","1","AI582","098-9164977341","2016-09-04","CCJ","BOM","Y","12979","3092","7714","715","2","6","2");
INSERT INTO airline_flight VALUES("36","26","1","AI921","098-9164977341","2016-09-04","BOM","RUH","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("38","28","2","IX350","X7IPF7","2016-10-13","CCJ","MCT","Y","1300","0","0","500","2","6","2");
INSERT INTO airline_flight VALUES("39","29","","SG53","06665879224","2016-09-08","CCJ","DXB","Y","500","","0","0","4","18","2");
INSERT INTO airline_flight VALUES("40","30","","EY","456456","2016-09-06","CCJ","DXB","","15000","","0","100","4","22","2");
INSERT INTO airline_flight VALUES("41","31","2","IX - 538","P1YKWK","2016-09-06","AUH","TRV","Y","20830","0","0","170","2","6","2");
INSERT INTO airline_flight VALUES("42","31","2","IX - 537","WP4IO2","2016-09-25","TRV","AUH","Y","11390","0","0","110","2","6","2");
INSERT INTO airline_flight VALUES("43","32","1","AI998","098-9164977437","2016-09-07","SHJ","CCJ","Y","12426","2092","6727","1255","2","6","2");
INSERT INTO airline_flight VALUES("44","33","2","IX344","0B134I","2016-12-16","DXB","CCJ","Y","126500","","0","2500","2","6","2");
INSERT INTO airline_flight VALUES("45","33","2","IX343","0B134I","2017-01-07","CCJ","DXB","Y","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("46","34","7","QR539","157-9164977454","2016-09-08","CCJ","DOH","N","12097","1661","4172","470","2","6","2");
INSERT INTO airline_flight VALUES("47","34","7","QR1046","157-9164977454","2016-09-08","DOH","AUH","N","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("48","35","7","","157-9164977450","2016-09-08","CCJ","DOH","S","400","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("49","35","7","","","2016-09-08","DOH","AUH","S","0","","0","0","2","6","2");
INSERT INTO airline_flight VALUES("50","36","2","IX373","M7AS4L","2016-09-11","CCJ","DOH","Y","24520","0","0","280","2","6","2");
INSERT INTO airline_flight VALUES("51","37","2","IX363","CCSTFD","2016-09-16","CCJ","AUH","Y","67030","0","0","970","2","6","2");
INSERT INTO airline_flight VALUES("52","38","2","IX363","","2016-09-16","CCJ","AUH","Y","0","","0","0","2","6","2");

DROP TABLE IF EXISTS airline_passenger;
CREATE TABLE `airline_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlineId` int(11) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationship` varchar(30) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `prefix` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlineId` (`airlineId`),
  CONSTRAINT `airline_passenger_ibfk_1` FOREIGN KEY (`airlineId`) REFERENCES `invoice_airline` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
INSERT INTO airline_passenger VALUES("1","1","Ittikaparambil Palakkal, Aboobackar","1978-01-10","39","M","CHAMRAVATTAM","","Indian","","G1284698","2008-02-10","2018-02-09","ABU DHABI","2","6","2","");
INSERT INTO airline_passenger VALUES("2","2","Thale Kunnath/jaseem","0000-00-00","0","M","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("3","3","Machincherithoombil/mohammed Sakeeb","1996-05-26","20","M","TIRUR","","Indian","","M8018076","2015-04-13","2025-04-12","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("4","4","Cheruvakkath/abdul Raheem","1960-01-23","57","M","VATTAPARAMBU","","Indian","","H0109737","2009-05-26","2019-05-25","DUBAI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("7","5","Cheruvakkath/abdul Raheem","1960-01-23","57","M","VATTAPARAMBU","","Indian","","H0109737","2009-05-26","2019-05-25","DUBAI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("9","7","Oduvil/ayyoob Mr","1979-03-10","38","M","10-03-1979","","Indian","","M7851287","2015-04-10","2025-04-09","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("11","8","Maliyekkal/muhammed Mukthar","1988-03-20","28","M","EDARIKODE","","Indian","","H0356591","2008-08-26","2018-08-25","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("12","8","Poovatil/juhana","1996-04-06","20","F","FEROKE","","Indian","","M2664637","2014-10-08","2024-10-07","MALAPPURAM","2","6","2","Mrs");
INSERT INTO airline_passenger VALUES("13","9","Poovatil/juhana","1996-04-06","20","F","FEROKE","","Indian","","M2664637","2014-10-08","2024-10-07","MALAPPURAM","2","6","2","Mrs");
INSERT INTO airline_passenger VALUES("23","11","Alqahtani/albandari Faisal","0000-00-00","0","F","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Ms");
INSERT INTO airline_passenger VALUES("24","11","Alqahtani/norah Faisal","0000-00-00","0","F","","","SAUDI","","","0000-00-00","0000-00-00","","2","6","2","Ms");
INSERT INTO airline_passenger VALUES("25","10","Alqahtani/renad Faisal","0000-00-00","0","","","","SAUDI","","","0000-00-00","0000-00-00","","2","6","2","Ms");
INSERT INTO airline_passenger VALUES("26","10","Alqahtani/qamraa Saad","0000-00-00","0","F","","","SAUDI","","","0000-00-00","0000-00-00","","2","6","2","Mrs");
INSERT INTO airline_passenger VALUES("28","13","Aboobacker Karingappara","0000-00-00","0","M","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("29","14","Riyas","0000-00-00","0","","","","Indian","","0","0000-00-00","0000-00-00","","4","18","2","Mr");
INSERT INTO airline_passenger VALUES("30","15","Aruncut","0000-00-00","0","","","","Indian","","AAAA","2016-09-13","2016-09-30","CALI","4","22","2","");
INSERT INTO airline_passenger VALUES("31","16","Suneer Majeed","0000-00-00","0","M","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("32","17","Sidheek Pookayil","1971-06-30","45","M","CHERUSHOLA","","Indian","","J2072600","2010-05-04","2020-05-03","ABUDHABI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("33","18","Machinchery Thoombil, Mohamed","0000-00-00","0","","","","Indian","","","0000-00-00","0000-00-00","","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("34","19","Kunnathodi/abdul Jaleel","1979-01-01","38","M","POOVANCHINA","","Indian","","G7230920","2008-02-08","2018-02-07","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("35","20","Kunnathodi/abdul Jaleel","1979-01-01","38","M","POOVANCHINA","","Indian","","G7230920","2008-02-08","2018-02-07","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("36","12","Ainikkunnan Jayafer","1992-08-02","24","M","VETTICHIRA","","Indian","","M7297991","2015-03-19","2025-03-18","MALAPPURAM","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("37","21","Valiya Valappil Ali Abdul Kareem","1967-05-01","49","M","PAVITTAPPURAM","","Indian","","K4621024","2012-03-19","2022-03-18","ABU DHABI","2","6","2","Mr");
INSERT INTO airline_passenger VALUES("38","21","Nadeera Abdul Kareem","1970-05-15","46","F","MANNALAMKUNNU","","Indian","","G5039690","2008-12-03","2018-12-02","ABUDHABI","2","6","2","Mrs");

DROP TABLE IF EXISTS cash_payment;
CREATE TABLE `cash_payment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `discount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
INSERT INTO cash_payment VALUES("3","1","visa","22","15900","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("4","1","visa","23","15900","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("5","1","airline","1","1000","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("6","1","airline","3","14730","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("14","2","airline","2","14015","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("15","2","airline","4","9080","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("16","2","airline","5","8870","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("17","2","airline","6","9130","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("18","2","airline","7","20100","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("19","2","airline","8","8385","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("20","2","airline","9","47570","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("21","2","airline","10","43660","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("22","3","visa","1","15000","0","","0","CASH","2","6","2");
INSERT INTO cash_payment VALUES("25","2","visa","86","8500","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("26","3","visa","36","5150","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("27","4","visa","18","5100","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("28","4","visa","19","100","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("29","5","visa","87","900","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("33","6","visa","77","5500","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("34","6","visa","78","7000","0","","0","CASH","1","15","2");
INSERT INTO cash_payment VALUES("35","4","airline","11","24520","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("36","4","airline","12","1300","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("37","4","airline","13","32220","0","","0","CASH","2","6","0");
INSERT INTO cash_payment VALUES("38","1","visa","102","78325","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("39","2","visa","62","6900","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("40","2","visa","63","4600","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("41","2","visa","64","2600","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("42","2","visa","65","14900","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("43","2","visa","70","7700","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("44","2","visa","72","14900","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("45","2","visa","73","900","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("46","2","visa","74","58400","0","","0","BANK","1","15","2");
INSERT INTO cash_payment VALUES("47","7","miscellaneous","1","1500","0","","0","CASH","1","15","2");

DROP TABLE IF EXISTS cash_receipt;
CREATE TABLE `cash_receipt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `discount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;
INSERT INTO cash_receipt VALUES("1","1","airline","4","9500","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("2","1","airline","5","9600","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("3","2","airline","12","1800","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("4","3","airline","11","24800","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("5","1","visa","22","5300","0","","0","CASH","1","5","2");
INSERT INTO cash_receipt VALUES("6","2","visa","18","5200","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("7","2","visa","19","300","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("10","5","visa","31","16500","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("11","6","visa","21","3700","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("12","7","visa","59","400","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("13","7","visa","61","14600","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("14","4","airline","2","14050","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("15","4","airline","9","49000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("16","4","airline","10","44600","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("18","6","visa","1","10000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("19","7","airline","1","1000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("20","5","airline","14","15000","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("21","1","visa","49","46123","0","DEPOSITED BY MUHAMMED KOTTATHAYIL  IN SOUTH INDIAN","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("22","2","visa","62","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("23","2","visa","63","6900","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("24","2","visa","64","4600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("25","2","visa","65","2600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("26","2","visa","66","14900","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("27","2","visa","71","7700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("28","2","visa","72","43800","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("29","2","visa","74","14900","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("30","2","visa","75","900","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("31","2","visa","76","26700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("32","8","airline","14","7500","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("33","8","miscellaneous","1","1700","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("34","9","visa","82","16000","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("35","3","visa","81","15000","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("36","10","visa","86","40000","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("37","4","visa","28","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("38","4","visa","30","29600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("39","4","visa","32","5700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("40","11","visa","94","3800","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("41","12","visa","37","5200","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("42","5","visa","89","900","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("44","13","visa","100","7750","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("45","13","visa","101","7750","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("46","9","airline","13","32500","0","","0","CASH","2","6","2");
INSERT INTO cash_receipt VALUES("47","6","visa","70","15418","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("48","7","visa","2","28700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("49","7","visa","7","29200","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("50","7","visa","8","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("51","7","visa","10","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("52","7","visa","11","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("53","7","visa","12","14700","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("54","7","visa","13","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("55","7","visa","17","14600","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("56","7","visa","26","4000","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("57","7","visa","29","300","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("59","8","visa","92","50000","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("60","9","visa","104","78325","0","","0","BANK","1","15","2");
INSERT INTO cash_receipt VALUES("61","14","miscellaneous","2","24750","0","","0","CASH","1","5","2");
INSERT INTO cash_receipt VALUES("62","3","visa","23","5000","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("63","4","visa","24","16000","0","","0","CASH","1","15","2");
INSERT INTO cash_receipt VALUES("64","15","visa","23","12400","0","","0","CASH","1","15","2");

DROP TABLE IF EXISTS company;
CREATE TABLE `company` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `tds` int(11) NOT NULL,
  `circle` varchar(100) NOT NULL,
  `resPerson` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `remark` text NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
INSERT INTO company VALUES("1","Mtm Travel Mart Kzkd","","0","KOZHIKODE","0495 4","07736538597","mtmvisa@gmail.com","","0","","Sa ad","Manager","","2");
INSERT INTO company VALUES("2","Mtm Travel Mart - Vettichira","-VETTICHIRA,CHUNGAM- MALAPPURAM","0","VETTICHIRA","04942617140","09497141053","mtmvettichira@gmail.com","","0","","MANU MUHAMMED","MANAGER","","2");
INSERT INTO company VALUES("3","Mtm Travel Mart Ptb","","0","PATTAMBI","0","9846414143","mtmpattambi@gmail.com","","0","","SAIFUDHEEN","MANAGER","","2");
INSERT INTO company VALUES("4","Test 1","","000000","0","0","8547116314","markaztravelmart@gmail.com","","0","","","","","2");

DROP TABLE IF EXISTS flight;
CREATE TABLE `flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `flightNo` varchar(50) NOT NULL,
  `flightName` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
INSERT INTO flight VALUES("1","0","Air India","","2","6","2");
INSERT INTO flight VALUES("2","0","Air India Express","","2","6","2");
INSERT INTO flight VALUES("3","0","Etihad Airways","","2","6","2");
INSERT INTO flight VALUES("4","0","Emirates  Airlines","","2","6","2");
INSERT INTO flight VALUES("5","0","Air Arabia","","2","6","2");
INSERT INTO flight VALUES("6","0","Saudi Arabian Airlines","","2","6","2");
INSERT INTO flight VALUES("7","0","Qatar Airways","","2","6","2");
INSERT INTO flight VALUES("8","0","Oman Air","","2","6","2");
INSERT INTO flight VALUES("9","0","Kuwait Airways","","2","6","2");
INSERT INTO flight VALUES("10","0","Spicejet","","2","6","2");
INSERT INTO flight VALUES("11","0","Indigo","","2","6","2");
INSERT INTO flight VALUES("12","0","Jet Airways","","2","6","2");
INSERT INTO flight VALUES("13","0","Thai  Airasia","","2","6","2");
INSERT INTO flight VALUES("14","0","Sv","","4","19","2");

DROP TABLE IF EXISTS hotel;
CREATE TABLE `hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(10) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO hotel VALUES("1","Janadriyya","","000000","Makka","Soudi","Soudi","00000","00000000","","","4","19","2");

DROP TABLE IF EXISTS hotel_customer;
CREATE TABLE `hotel_customer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelInvoiceId` int(11) NOT NULL,
  `roomType` varchar(20) NOT NULL,
  `noOfRooms` int(11) NOT NULL,
  `noOfPersons` int(11) NOT NULL,
  `rate` float NOT NULL,
  `subTax` varchar(30) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_airline;
CREATE TABLE `invoice_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float NOT NULL,
  `mainProcCharge` float NOT NULL,
  `mainOtherCharge` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `mainTax` varchar(30) NOT NULL,
  `discount` float NOT NULL,
  `pnr` varchar(20) NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `ticketDetails` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
INSERT INTO invoice_airline VALUES("1","1","2016-09-01","86","0","0","0","Reservation change fee collected","","0","0","NHI1RD","1000","Pending","document/20160901125156.doc","","2","6","2");
INSERT INTO invoice_airline VALUES("2","2","2016-09-01","87","0","0","0","EY PNR HTBDPY","","0","0","3I53TT","14050","Pending","document/20160901125119.docx","","2","6","2");
INSERT INTO invoice_airline VALUES("3","3","2016-09-03","89","0","0","0","","","0","0","AEDKL6","15000","Pending","document/20160903150903.doc","","2","6","2");
INSERT INTO invoice_airline VALUES("4","4","2016-09-03","90","0","0","0","","","0","0","3OEJL9","9500","Pending","document/20160903150745.doc","","2","6","2");
INSERT INTO invoice_airline VALUES("5","5","2016-09-03","90","0","0","0","PAYMENT REF * NAM8CG","","0","0","CW3YVB","9600","Pending","document/20160903150551.pdf","","2","6","2");
INSERT INTO invoice_airline VALUES("7","6","2016-09-03","91","0","0","0","","","0","0","0","9700","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("8","7","2016-09-05","92","0","0","0","","","0","0","3OX2YM","20600","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("9","8","2016-09-05","92","0","0","0","","","0","0","3OX2YM","8700","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("10","9","2016-09-05","87","0","0","0","","","0","0","3OYGIE","49000","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("11","10","2016-09-05","87","0","0","0","","","0","0","3OYEHF","44600","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("12","11","2016-09-05","98","0","0","0","","","0","0","M7AS4L","24800","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("13","12","2016-09-05","100","0","0","0","Ticket ree issue from IX CCJ Office","","0","0","0","1800","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("14","1","2016-09-05","119","65","500","0","","","0","0","0","1065","Pending","","","4","18","2");
INSERT INTO invoice_airline VALUES("15","2","2016-09-05","118","0","0","0","","","0","0","0","15100","Pending","","","4","22","2");
INSERT INTO invoice_airline VALUES("16","13","2016-09-06","126","0","0","0","","","0","0","0","32500","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("17","14","2016-09-06","128","0","0","0","","","0","0","0","22500","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("18","15","2016-09-06","89","0","0","0","MACHINCHERY THOOMBIL/MOHAMED MR \nEDATHADATHIL/SOUBHANATH MRS\nMACHINCHERY THOOMBIL/AFRAH MISS(CHD/22AUG10)\n.MACHINCHERY THOOMBIL/ASHDA MISS(CHD/27MAY07)","","0","0","0","129000","Pending","document/20160907173332.doc","","2","6","2");
INSERT INTO invoice_airline VALUES("19","16","2016-09-06","87","0","0","0","","","0","0","0","18400","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("20","17","2016-09-06","87","0","0","0","Void ticket","","0","0","0","400","Pending","","","2","6","2");
INSERT INTO invoice_airline VALUES("21","18","2016-09-09","150","0","0","0","","","0","0","0","68000","Pending","document/20160910133905.docx","<p>F</p>","2","6","2");

DROP TABLE IF EXISTS invoice_hotel;
CREATE TABLE `invoice_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `hotelId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `noOfNight` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `checkInDate` date NOT NULL,
  `checkInTime` varchar(20) NOT NULL,
  `checkOutDate` date NOT NULL,
  `checkOutTime` varchar(20) NOT NULL,
  `billingIns` varchar(100) NOT NULL,
  `reference` varchar(100) NOT NULL,
  `tax` varchar(30) NOT NULL,
  `tds` float NOT NULL,
  `otherCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_misc;
CREATE TABLE `invoice_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `basicAmount` float NOT NULL,
  `tds` float NOT NULL,
  `tax` varchar(20) NOT NULL,
  `otherCharge` float NOT NULL,
  `procCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `narration` int(11) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
INSERT INTO invoice_misc VALUES("1","1","143","2016-09-08","2016-09-08","ALI PATTNMARU VAL","","1500","0","0","0","200","0","1700","Pending","1","","1","10","2");
INSERT INTO invoice_misc VALUES("2","2","151","2016-09-09","2016-09-09","Aslam and wife","","15000","0","0","9750","0","0","24750","Pending","2","","1","5","2");

DROP TABLE IF EXISTS invoice_package;
CREATE TABLE `invoice_package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `packageId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `proCharge` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `discount` float DEFAULT '0',
  `netAmount` float NOT NULL DEFAULT '0',
  `description` varchar(200) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_airline;
CREATE TABLE `invoice_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_hotel;
CREATE TABLE `invoice_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `roomType` varchar(100) NOT NULL,
  `bedStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_passenger;
CREATE TABLE `invoice_package_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `packageId` int(11) NOT NULL,
  `prefix` varchar(20) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` date DEFAULT NULL,
  `age` int(11) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `birthPlace` varchar(100) NOT NULL,
  `relation` int(11) NOT NULL DEFAULT '0',
  `mobile` varchar(50) NOT NULL DEFAULT 'NA',
  `passportNo` varchar(50) NOT NULL,
  `dateOfIssue` date DEFAULT NULL,
  `validUpTo` date DEFAULT NULL,
  `issuePlace` varchar(100) NOT NULL,
  `mofa` varchar(50) NOT NULL,
  `emofa` varchar(50) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_transportation;
CREATE TABLE `invoice_package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `seatStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_visa;
CREATE TABLE `invoice_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_transport;
CREATE TABLE `invoice_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `serviceProviderId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `totalDays` int(11) NOT NULL,
  `vehicleType` varchar(100) NOT NULL,
  `kmAllowed` float NOT NULL,
  `arrivalDetails` text NOT NULL,
  `pickUpFrom` varchar(100) NOT NULL,
  `pickUpTime` varchar(20) NOT NULL,
  `DepartureDetails` text NOT NULL,
  `dropAt` varchar(100) NOT NULL,
  `dropTime` varchar(20) NOT NULL,
  `route` text NOT NULL,
  `inclusive` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_visa;
CREATE TABLE `invoice_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `mainOtherCharge` double NOT NULL,
  `discount` varchar(50) NOT NULL,
  `netAmount` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `customerKey` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;
INSERT INTO invoice_visa VALUES("1","1","2016-09-03","88","0","0","16500","","","Pending","","2","6","2","1IV#188");
INSERT INTO invoice_visa VALUES("2","1","2016-09-05","93","0","0","6200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("3","2","2016-09-05","95","0","0","28700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("4","3","2016-09-05","96","0","0","14800","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("5","4","2016-09-05","96","0","0","300","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("6","5","2016-09-05","99","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("7","6","2016-09-05","96","0","0","5200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("8","7","2016-09-05","95","0","0","29200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("9","8","2016-09-05","95","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("10","9","2016-09-05","101","0","0","14800","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("11","10","2016-09-05","95","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("12","11","2016-09-05","95","0","0","14700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("13","12","2016-09-05","95","0","0","14700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("14","13","2016-09-05","95","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("15","14","2016-09-05","103","0","0","14700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("16","15","2016-09-05","99","0","0","14800","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("17","16","2016-09-05","95","0","0","14700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("18","17","2016-09-05","95","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("19","18","2016-09-06","96","0","0","5200","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("20","19","2016-09-06","96","0","0","300","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("21","20","2016-09-06","123","0","0","3800","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("22","21","2016-09-06","123","0","100","3700","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("23","22","2016-09-06","124","0","0","5300","","","Pending","","1","5","2","");
INSERT INTO invoice_visa VALUES("24","23","2016-09-06","96","0","0","17400","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("25","24","2016-09-06","96","0","0","16400","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("26","25","2016-09-06","129","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("27","26","2016-09-06","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("28","27","2016-09-06","129","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("29","28","2016-09-06","130","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("30","29","2016-09-06","95","0","0","300","","","Pending","","1","2","2","");
INSERT INTO invoice_visa VALUES("31","30","2016-09-06","130","0","0","29600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("32","31","2016-09-06","96","0","0","16500","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("33","32","2016-09-06","130","0","0","40800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("34","33","2016-09-07","93","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("35","34","2016-09-07","95","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("36","35","2016-09-07","93","0","0","16200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("37","36","2016-09-07","129","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("38","37","2016-09-07","101","0","0","5200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("39","38","2016-09-07","95","0","0","29600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("40","39","2016-09-07","114","0","0","29600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("41","40","2016-09-07","114","0","0","16000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("42","41","2016-09-07","130","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("43","42","2016-09-07","95","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("44","43","2016-09-07","99","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("45","44","2016-09-07","95","0","0","29400","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("46","45","2016-09-07","129","0","0","15100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("47","46","2016-09-07","99","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("48","47","2016-09-07","95","0","0","189800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("49","48","2016-09-07","99","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("50","49","2016-09-07","136","0","0","46200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("51","50","2016-09-07","95","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("52","51","2016-09-07","95","0","0","29400","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("53","52","2016-09-07","129","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("54","53","2016-09-07","101","0","0","14800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("55","54","2016-09-07","101","0","0","29600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("56","55","2016-09-07","99","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("57","56","2016-09-07","95","0","0","29200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("58","57","2016-09-07","130","0","0","5100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("59","58","2016-09-07","93","0","0","45300","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("60","59","2016-09-07","138","0","0","400","OLD BALANCE UPDATED 14600","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("61","60","2016-09-07","138","0","0","600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("62","61","2016-09-07","138","0","0","14600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("63","62","2016-09-07","95","0","0","14700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("64","63","2016-09-07","95","0","0","6900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("65","64","2016-09-07","95","0","0","4600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("66","65","2016-09-07","95","0","0","2600","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("67","66","2016-09-07","95","0","0","14900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("68","67","2016-09-07","96","0","0","15100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("69","68","2016-09-07","95","0","0","14600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("70","69","2016-09-07","129","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("71","70","2016-09-07","136","0","0","15400","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("72","2","2016-09-07","139","0","0","5800","","","Pending","","2","6","2","");
INSERT INTO invoice_visa VALUES("73","71","2016-09-07","95","0","0","7700","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("74","72","2016-09-07","95","0","0","43800","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("107","105","2016-09-10","96","0","0","5200","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("76","74","2016-09-07","95","0","0","14900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("77","75","2016-09-07","95","0","0","900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("78","76","2016-09-07","95","0","0","58400","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("79","77","2016-09-07","95","0","0","88200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("80","78","2016-09-08","101","0","0","10000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("81","79","2016-09-08","101","0","0","5800","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("82","80","2016-09-08","141","0","0","6600","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("83","81","2016-09-08","142","0","0","15000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("84","82","2016-09-08","144","0","500","16000","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("85","83","2016-09-08","130","0","0","5100","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("86","84","2016-09-08","130","0","0","14700","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("87","85","2016-09-08","103","0","0","5500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("88","86","2016-09-08","145","0","0","45000","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("89","87","2016-09-08","96","0","0","16000","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("90","88","2016-09-08","96","0","0","11000","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("91","89","2016-09-08","129","0","0","900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("92","90","2016-09-08","129","0","0","11400","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("93","91","2016-09-08","129","0","0","300","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("94","92","2016-09-08","129","0","0","59200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("106","104","2016-09-09","99","0","0","78325","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("96","94","2016-09-08","148","0","0","3800","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("97","95","2016-09-08","129","0","0","1200","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("98","96","2016-09-08","129","0","0","15900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("99","97","2016-09-08","129","0","0","300","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("100","98","2016-09-08","129","0","0","15900","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("101","99","2016-09-08","129","0","0","300","","","Pending","","1","10","2","");
INSERT INTO invoice_visa VALUES("102","100","2016-09-08","149","0","0","7750","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("103","101","2016-09-08","149","0","0","7750","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("104","102","2016-09-09","114","0","0","16500","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("105","103","2016-09-09","93","0","0","16200","","","Pending","","1","9","2","");
INSERT INTO invoice_visa VALUES("108","106","2016-09-10","96","0","0","20800","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("109","107","2016-09-10","152","0","0","5200","","","Pending","","1","8","2","");
INSERT INTO invoice_visa VALUES("110","108","2016-09-10","96","0","0","11500","","","Pending","","1","8","2","");

DROP TABLE IF EXISTS log;
CREATE TABLE `log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(50) NOT NULL,
  `tableId` int(11) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `changeDate` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `oldAmt` float NOT NULL,
  `newAmt` float NOT NULL,
  `description` text NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
INSERT INTO log VALUES("1","Invoice Airline","5","5","2016-09-03","Edit","9600","9600","NetAmt:9600.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("2","Invoice Airline","6","6","2016-09-03","Edit","9700","9700","NetAmt:9700,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("3","Invoice Airline","6","6","2016-09-03","Delete","9700","0","InvoiceAmt:9700,PurchaseVoucherNo:6,PurchaseAmt:9130,PaidAmt:0,ReceivedAmt:0","6","2","2");
INSERT INTO log VALUES("4","Invoice Airline","8","7","2016-09-05","Edit","10300","20600","NetAmt:20600.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("5","Invoice Airline","10","9","2016-09-05","Edit","24500","49000","NetAmt:49000.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("6","Invoice Airline","10","9","2016-09-05","Edit","49000","49000","NetAmt:49000.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("7","Invoice Airline","10","9","2016-09-05","Edit","49000","49000","NetAmt:49000.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("8","Invoice Airline","11","10","2016-09-05","Edit","22300","44600","NetAmt:44600.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("9","Invoice Airline","10","9","2016-09-05","Edit","49000","49000","NetAmt:49000.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("10","Invoice Visa","25","24","2016-09-06","Edit","16400","16400","NetAmt:16400,Discount:0","8","1","2");
INSERT INTO log VALUES("11","Invoice Visa","25","24","2016-09-06","Edit","16400","16400","NetAmt:16400,Discount:0","8","1","2");
INSERT INTO log VALUES("12","Invoice Visa","24","23","2016-09-06","Edit","16400","16400","NetAmt:16400,Discount:0","8","1","2");
INSERT INTO log VALUES("13","Invoice Visa","60","59","2016-09-07","Edit","400","400","NetAmt:400,Discount:0","10","1","2");
INSERT INTO log VALUES("14","Invoice Airline","12","11","2016-09-07","Edit","24800","24800","NetAmt:24800.00,Discount:0,Tds:0,MainTax:0","6","2","2");
INSERT INTO log VALUES("15","Invoice Miscellaneous","1","1","2016-09-08","Edit","1700","1700","NetAmt:1700.00,Discount:0,Tds:0,MainTax:0","10","1","2");
INSERT INTO log VALUES("16","Invoice Miscellaneous","1","1","2016-09-08","Edit","1700","1700","NetAmt:1700,Discount:0,Tds:0,MainTax:0","10","1","2");
INSERT INTO log VALUES("17","Invoice Miscellaneous","1","1","2016-09-08","Edit","1700","1700","NetAmt:1700,Discount:0,Tds:0,MainTax:0","10","1","2");
INSERT INTO log VALUES("18","Invoice Visa","98","96","2016-09-08","Edit","16400","15900","NetAmt:15900,Discount:0","10","1","2");
INSERT INTO log VALUES("19","Invoice Visa","95","93","2016-09-08","Edit","59200","59200","NetAmt:59200,Discount:0","10","1","2");
INSERT INTO log VALUES("20","Invoice Visa","95","93","2016-09-08","Edit","59200","59200","NetAmt:59200,Discount:0","10","1","2");
INSERT INTO log VALUES("21","Invoice Visa","75","73","2016-09-09","Delete","43800","0","InvoiceAmt:43800,PurchaseVoucherNo:71,PurchaseAmt:43800,PaidAmt:0,ReceivedAmt:0","10","1","2");
INSERT INTO log VALUES("22","Invoice Visa","93","91","2016-09-09","Edit","300","300","NetAmt:300,Discount:0","10","1","2");
INSERT INTO log VALUES("23","Invoice Visa","94","92","2016-09-09","Edit","59200","59200","NetAmt:59200,Discount:0","10","1","2");
INSERT INTO log VALUES("24","Invoice Visa","94","92","2016-09-09","Edit","59200","59200","NetAmt:59200,Discount:0","10","1","2");
INSERT INTO log VALUES("25","Invoice Visa","97","95","2016-09-09","Edit","1200","1200","NetAmt:1200,Discount:0","10","1","2");
INSERT INTO log VALUES("26","Invoice Visa","95","93","2016-09-09","Delete","59200","0","InvoiceAmt:59200,PurchaseVoucherNo:90,PurchaseAmt:59200,PaidAmt:0,ReceivedAmt:49100","10","1","2");
INSERT INTO log VALUES("27","Invoice Visa","92","90","2016-09-09","Edit","14700","11400","NetAmt:11400,Discount:0","10","1","2");
INSERT INTO log VALUES("28","Invoice Visa","74","72","2016-09-09","Edit","43800","43800","NetAmt:43800,Discount:0","10","1","2");
INSERT INTO log VALUES("29","Invoice Visa","74","72","2016-09-09","Edit","43800","43800","NetAmt:43800,Discount:0","10","1","2");
INSERT INTO log VALUES("30","Invoice Miscellaneous","2","2","2016-09-09","Edit","24750","24750","NetAmt:24750,Discount:0,Tds:0,MainTax:0","5","1","2");
INSERT INTO log VALUES("31","Invoice Visa","74","72","2016-09-10","Edit","43800","43800","NetAmt:43800,Discount:0","10","1","2");
INSERT INTO log VALUES("32","Invoice Visa","17","16","2016-09-10","Edit","14700","14700","NetAmt:14700,Discount:0","10","1","2");
INSERT INTO log VALUES("33","Invoice Visa","24","23","2016-09-10","Edit","16400","17400","NetAmt:17400,Discount:0","8","1","2");
INSERT INTO log VALUES("34","Invoice Visa","24","23","2016-09-10","Edit","17400","17400","NetAmt:17400,Discount:0","8","1","2");
INSERT INTO log VALUES("35","Invoice Visa","74","72","2016-09-10","Edit","43800","43800","NetAmt:43800,Discount:0","10","1","2");

DROP TABLE IF EXISTS login;
CREATE TABLE `login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
INSERT INTO login VALUES("1","0","admin","81dc9bdb52d04dc20036dbd8313ed055","Admin","Active","0");
INSERT INTO login VALUES("2","1","mtmkzkd","ad8dc86031cc2b133b014e4887a649a5","Branch","Active","2");
INSERT INTO login VALUES("3","2","MTMVTRA","01b08dc6d86c837d2057e82c1bcc32d5","Branch","Active","2");
INSERT INTO login VALUES("4","3","MTMPTB","d29e62f0d944da737e90b336a035ee3b","Branch","Active","2");
INSERT INTO login VALUES("5","1","sidheeq","ba856b0417652289251c866c3e22824e","Staff","Active","2");
INSERT INTO login VALUES("6","2","MANU","540fdbdc9912ae5d8ed5fb0a50bc3578","Staff","Active","2");
INSERT INTO login VALUES("7","3","NASAR","73459038a6d3b1f36a8ac676f6973fff","Staff","Active","2");
INSERT INTO login VALUES("8","4","SAFUVAN","e47c53c3c74edf7d10c8f2a24917a67f","Staff","Active","2");
INSERT INTO login VALUES("9","5","SAHAD","bbb97d09796c1836e464439d690a82da","Staff","Active","2");
INSERT INTO login VALUES("10","6","IRFAD","b4fbb06cf89e72c88e8f77937160f8b6","Staff","Active","2");
INSERT INTO login VALUES("15","11","AKHIL","8724aa758c2f662d79952870ef486ea6","Staff","Active","2");
INSERT INTO login VALUES("16","12","HYDERALI","07d175602dcd88d08ec9637f02b0be6d","Staff","Active","2");
INSERT INTO login VALUES("17","4","TEST","033bd94b1168d7e4f0d644c3c95e35bf","Branch","Active","2");
INSERT INTO login VALUES("18","13","HYDER","bd02b22685e2122f2a4b800fc5856c95","Staff","Active","2");
INSERT INTO login VALUES("19","14","MARKAZ","43f92d15dd9e3940a7441c69248239ac","Staff","Active","2");
INSERT INTO login VALUES("20","15","FAIZY","e5fc0be25dd312817792969b6d984a80","Staff","Active","2");
INSERT INTO login VALUES("21","16","sha1","74a89174426b40307102e165374ab8ab","Staff","Active","2");
INSERT INTO login VALUES("22","17","AKHIL K P","8724aa758c2f662d79952870ef486ea6","Staff","Active","2");
INSERT INTO login VALUES("23","18","SADHAM","3c2ef6af516eb7b7e369531cdac2f98e","Staff","Active","2");
INSERT INTO login VALUES("24","19","FAIZYY","d5e3e8c37821a6a25fa6b60c65382ddd","Staff","Active","2");
INSERT INTO login VALUES("33","22","FAIZAL","c64fd02bb7f1cb6f7d81771d3c0c86d8","Staff","Active","2");

DROP TABLE IF EXISTS misc_narration;
CREATE TABLE `misc_narration` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `narration` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO misc_narration VALUES("1","Passport Service","1","10","2");
INSERT INTO misc_narration VALUES("2","Flight Date Change","1","5","2");

DROP TABLE IF EXISTS other_purchase;
CREATE TABLE `other_purchase` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package;
CREATE TABLE `package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageName` varchar(200) NOT NULL,
  `staff` int(11) NOT NULL,
  `leader` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `place` varchar(100) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `tds` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `remark` varchar(100) NOT NULL,
  `services` varchar(100) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `confirm` int(11) NOT NULL,
  `discount` float NOT NULL,
  `extra` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package_airline;
CREATE TABLE `package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `yq` float NOT NULL DEFAULT '0',
  `procCharge` float NOT NULL DEFAULT '0',
  `tax` float NOT NULL DEFAULT '0',
  `adultCharge` float NOT NULL,
  `childCharge` float NOT NULL,
  `infantCharge` float NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `packageId` (`packageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package_airline_flights;
CREATE TABLE `package_airline_flights` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_airlineId` int(11) NOT NULL,
  `airline` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `sectorFrom` varchar(200) NOT NULL,
  `sectorTo` varchar(200) NOT NULL,
  `jDate` date NOT NULL,
  `airTime` varchar(20) NOT NULL,
  `aDate` date NOT NULL,
  `aTime` varchar(20) NOT NULL,
  `class` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package_hotel;
CREATE TABLE `package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `hotelId` int(11) NOT NULL,
  `checkinDate` date DEFAULT NULL,
  `checkoutDate` date DEFAULT NULL,
  `noOfDays` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package_room_type;
CREATE TABLE `package_room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_hotelId` int(11) NOT NULL,
  `roomId` int(11) NOT NULL,
  `rate` float NOT NULL,
  `otherCharge` float NOT NULL,
  `total` float NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package_transportation;
CREATE TABLE `package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `vehicle` int(11) NOT NULL,
  `seatCharge` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package_visa;
CREATE TABLE `package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `visaFor` varchar(100) NOT NULL,
  `visaFee` float NOT NULL DEFAULT '0',
  `vfs` float NOT NULL DEFAULT '0',
  `ddCharge` float NOT NULL DEFAULT '0',
  `serviceCharge` float NOT NULL DEFAULT '0',
  `otherCharge` float NOT NULL DEFAULT '0',
  `courierCharge` float NOT NULL DEFAULT '0',
  `total` float NOT NULL DEFAULT '0',
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS passenger;
CREATE TABLE `passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `prefix` varchar(20) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationshipId` int(11) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `mofa` varchar(50) NOT NULL,
  `emofa` varchar(50) NOT NULL,
  `passPhoto` varchar(200) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=latin1;
INSERT INTO passenger VALUES("1","86","","ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR","1978-01-10","39","M","CHAMRAVATTAM","0","Indian","","G1284698","2008-02-10","2018-02-09","ABU DHABI","","","","2","6","2");
INSERT INTO passenger VALUES("2","87","Mr","THALE KUNNATH/JASEEM","0000-00-00","0","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("3","88","","MAIMOONA MANJAPILLI AHAMMAD ","1971-05-22","0","F","","0","","","N1595786","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("4","89","Mr","MACHINCHERITHOOMBIL/MOHAMMED SAKEEB","1996-05-26","20","M","TIRUR","0","Indian","","M8018076","2015-04-13","2025-04-12","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("5","90","Mr","CHERUVAKKATH/ABDUL RAHEEM","1960-01-23","57","M","VATTAPARAMBU","0","Indian","","H0109737","2009-05-26","2019-05-25","DUBAI","","","","2","6","2");
INSERT INTO passenger VALUES("6","91","Mr","ODUVIL/AYYOOB","1979-03-10","38","M","THAZHEKODE","0","Indian","","M7851287","2015-04-10","2025-04-09","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("7","90","Mr","Cheruvakkath/abdul Raheem","1960-01-23","57","M","VATTAPARAMBU","0","Indian","","H0109737","2009-05-26","2019-05-25","DUBAI","","","","2","6","2");
INSERT INTO passenger VALUES("8","91","Mr","ODUVIL/AYYOOB MR","1979-03-10","38","M","10-03-1979","0","Indian","","M7851287","2015-04-10","2025-04-09","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("9","92","Mr","MALIYEKKAL/MUHAMMED MUKTHAR","1988-03-20","28","M","EDARIKODE","0","Indian","","H0356591","2008-08-26","2018-08-25","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("10","92","Mrs","POOVATIL/JUHANA ","1996-04-06","20","F","FEROKE","0","Indian","","M2664637","2014-10-08","2024-10-07","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("11","87","Mrs","ALQAHTANI/QAMRAA SAAD","0000-00-00","0","","","0","SAUDI","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("12","87","Mrs","ALQAHTANI/QAMRAA SAAD ","0000-00-00","0","","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("13","87","Ms","ALQAHTANI/RENAD FAISAL ","0000-00-00","0","","","0","SAUDI","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("14","87","","","0000-00-00","0","","","0","","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("15","87","Ms","ALQAHTANI/ALBANDARI FAISAL","0000-00-00","0","F","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("16","87","Ms","ALQAHTANI/NORAH FAISAL ","0000-00-00","0","F","","0","SAUDI","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("17","87","Mrs","ALQAHTANI/QAMRAA SAAD ","0000-00-00","0","F","","0","SAUDI","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("18","93","","MUFEED ABDUL AZIZ","0000-00-00","0","M","","0","","","Z3668844","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("19","95","","VISAKH","0000-00-00","0","M","","0","","","N1140524","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("20","96","","ABDUL GAFOOR","0000-00-00","0","M","","0","","","H2626002","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("21","96","","ABDUL GAFOOR","0000-00-00","0","M","","0","","","H2626002","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("22","99","","SHAHUNL ABDULNASAR","0000-00-00","0","M","","0","","","G8487574","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("23","96","","SHAJAHAN PALLILATH","0000-00-00","0","M","","0","","","H5103542","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("24","95","","SIMON","2000-09-05","0","M","","0","","","J1485230","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("25","95","","ELSY","2000-09-05","0","F","","0","","","J1485339","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("26","95","","AMEER","0000-00-00","0","M","","0","","","M0314613","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("27","98","Mr","AINIKKUNNAN JAYAFER","1992-08-02","24","M","VETTICHIRA","0","Indian","","M7297991","2015-03-19","2025-03-18","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("28","100","Mr","ABOOBACKER KARINGAPPARA","0000-00-00","0","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("29","101","","ALLY JOY","0000-00-00","0","M","","0","","","J0885006","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("30","95","","DENIAL","0000-00-00","0","M","","0","","","M9715530","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("31","95","","SUSY","0000-00-00","0","M","","0","","","N9538535","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("32","95","","RAHMATHUNNISA ","0000-00-00","0","F","","0","","","L9256080","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("33","95","","SHANU MON","0000-00-00","0","M","","0","","","J2491039","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("34","103","","MAIMOONA MANJAPPILLY","0000-00-00","0","F","","0","","","M159786","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("35","99","","MEENA HILARY","0000-00-00","0","F","","0","","","M1342981","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("36","114","","MISRIYA","0000-00-00","0","M","","0","","","P1902660","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("37","95","","AGHIL","0000-00-00","0","M","","0","","","L9642581","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("38","119","Mr","RIYAS ","0000-00-00","0","","","0","Indian","","0","0000-00-00","0000-00-00","","","","","4","18","2");
INSERT INTO passenger VALUES("39","118","","ARUNCUT","0000-00-00","0","","","0","Indian","","AAAA","2016-09-13","2016-09-30","CALI","","","","4","22","2");
INSERT INTO passenger VALUES("40","96","","ABDUL SALEEM","0000-00-00","0","M","","0","","","K1233711","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("41","96","","ABDUL SALEEM","0000-00-00","0","M","","0","","","K1233711","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("42","123","","MOHAMMED ISMAIL","0000-00-00","0","M","","0","","","L715982","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("43","123","","MOHAMMED ISMAIL","0000-00-00","0","M","","0","","","L715982","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("44","124","","ABDUL GAFOOR","1976-09-06","0","M","","0","","","H2626002","0000-00-00","0000-00-00","","","","","1","5","2");
INSERT INTO passenger VALUES("45","96","","FATHIMA HASHIFA","0000-00-00","0","F","","0","","","N7809054","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("46","96","","626SHIRIN SHAHANA","0000-00-00","0","M","","0","","","P1438308","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("47","126","Mr","SUNEER MAJEED","0000-00-00","0","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("48","96","","626SHIRIN SHAHANA","0000-00-00","0","M","","0","","","P1438308","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("49","96","","626SHIRIN SHAHANA","0000-00-00","0","M","","0","","","P1438308","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("50","96","","FATHIMA HASHIFA","0000-00-00","0","F","","0","","","N7809054","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("51","128","Mr","SIDHEEK POOKAYIL","1971-06-30","45","M","CHERUSHOLA","0","Indian","","J2072600","2010-05-04","2020-05-03","ABUDHABI","","","","2","6","2");
INSERT INTO passenger VALUES("52","89","Mr","MACHINCHERY THOOMBIL, MOHAMED","0000-00-00","0","","","0","Indian","","","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("53","87","Mr","KUNNATHODI/ABDUL JALEEL","1979-01-01","38","M","POOVANCHINA","0","Indian","","G7230920","2008-02-08","2018-02-07","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("54","129","","MUHAMMED ANSAB","0000-00-00","0","M","","0","","","N1900984","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("55","95","","ANSAR","0000-00-00","0","M","","0","","","P2698527","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("56","129","","SAFNAS","0000-00-00","0","M","","0","","","H6710271","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("57","130","","HASSAN PUTHIYATI","0000-00-00","0","M","","0","","","G9210752","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("58","95","","ANEEM","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","2","2");
INSERT INTO passenger VALUES("59","130","","HEZZA","2016-09-06","0","F","","0","","","N8801988","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("60","130","","ISMATHUL HAMRA","2016-09-06","0","F","","0","","","N8239724","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("61","96","","MTM570 JAMAL YASAR","0000-00-00","0","M","","0","","","H3365841","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("62","130","","MANZOOR","2016-09-06","0","M","","0","","","P0914263","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("63","130","","NOUFAL","2016-09-06","0","M","","0","","","J5706307","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("64","130","","PERAMAYIAN","2016-09-06","0","M","","0","","","L9319426","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("65","130","","ARAVINDAN","2016-09-06","0","M","","0","","","M2761361","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("66","130","","SAINUDHEEN","2016-09-06","0","M","","0","","","Z2993148","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("67","130","","RUKIYA","2016-09-06","0","F","","0","","","K3086549","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("68","130","","HAMNA","2016-09-06","0","F","","0","","","N9974878","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("69","130","","IDHAYATHUL HAMDAN","2016-09-06","0","M","","0","","","N9974878","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("70","93","","ANSIL","0000-00-00","0","M","","0","","","L1756857","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("71","95","","ZAINABA","0000-00-00","0","M","","0","","","J8564596","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("72","93","","IBRAHIM KUTTY","0000-00-00","0","M","","0","","","P2813905","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("73","129","","MUHAMMED","0000-00-00","0","M","","0","","","N5120158","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("74","101","","MANU BHAI","0000-00-00","0","M","","0","","","N7456851","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("75","95","","SAROJA","2016-09-07","0","F","","0","","","K6272814","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("76","95","","JAFIN","2016-09-07","0","F","","0","","","M3827069","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("77","114","","CHANDRAN PULIKKATT","2016-09-07","0","M","","0","","","J5352156","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("78","114","","SHEEJA","2016-09-07","0","F","","0","","","N4524757","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("79","114","","NILAA NIKHIL","0000-00-00","0","M","","0","","","P0422439","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("80","130","","JAMES CLEETUS","0000-00-00","0","M","","0","","","N5427392","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("81","95","","MERLIN BABU","0000-00-00","0","F","","0","","","M0477973","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("82","99","","ASHNA","0000-00-00","0","F","","0","","","M6093308","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("83","95","","SHAMEEM","2016-09-07","0","M","","0","","","K3072306","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("84","95","","SRIMATHY","2016-09-07","0","F","","0","","","P4071810","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("85","129","","SHEEBA","2016-09-07","0","F","","0","","","N5159159","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("86","99","","JUSTIN JAMES","2016-09-07","0","M","","0","","","L5914253","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("87","95","","ASOK KUMAR","2016-09-07","0","M","","0","","","M1674522","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("88","95","","DILEEP","2016-09-07","0","M","","0","","","G6067478","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("89","95","","HASHIM","2016-09-07","0","M","","0","","","H6275220","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("90","95","","RAMAKRISHNAN","2016-09-07","0","M","","0","","","G6160411","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("91","95","","JERIN JAMES","2016-09-07","0","M","","0","","","K5427677","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("92","95","","SABEEH","2016-09-07","0","M","","0","","","H3049393","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("93","95","","FAISAL","2016-09-07","0","M","","0","","","K3033987","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("94","95","","UMMER","2016-09-07","0","M","","0","","","M4950208","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("95","95","","HAFIZ","2016-09-07","0","M","","0","","","J2006760","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("96","95","","KUNJHU MON","2016-09-07","0","M","","0","","","M7807535","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("97","95","","MADAVADAS","2016-09-07","0","M","","0","","","J5254307","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("98","95","","VENUGOPALAN","2016-09-07","0","M","","0","","","H5709904","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("99","95","","SAROJINI","2016-09-07","0","F","","0","","","H5708579","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("100","99","","VARGHESE","2016-09-07","0","M","","0","","","M832608","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("101","99","","SUDHEESH","2016-09-07","0","M","","0","","","N6950516","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("102","136","","RAJA RAM","2016-09-07","0","M","","0","","","H7437321","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("103","136","","VINOD","2016-09-07","0","M","","0","","","N9184559","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("104","136","","MANOJ KUMAR","2016-09-07","0","M","","0","","","K5356456","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("105","95","","ARIF","2016-09-07","0","M","","0","","","M9418291","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("106","95","","BONEY JOSEPH","2016-09-07","0","M","","0","","","G8586116","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("107","95","","SALIHA","2016-09-07","0","M","","0","","","M0174249","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("108","95","","NIMMY","2016-09-07","0","M","","0","","","L2844381","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("109","129","","SARATH KRISHNA","2016-09-07","0","M","","0","","","K6243609","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("110","101","","HABEEBA SHAFEEEK","2016-09-07","0","M","","0","","","P2642443","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("111","101","","HASNA","2016-09-07","0","M","","0","","","P1163918","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("112","101","","ZAHIDA","2016-09-07","0","M","","0","","","M9909492","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("113","99","","DAISAN DAVID","2016-09-07","0","M","","0","","","N1142416","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("114","95","","SIJO JOSEPH","2016-09-07","0","M","","0","","","N8591047","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("115","95","","AJAY KRISHNA","2016-09-07","0","M","","0","","","G9194546","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("116","130","","ABDUL AZEEZ","2016-09-07","0","M","","0","","","L8040492","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("117","93","","NAFSIYA KOLAMBIL","2016-09-07","0","F","","0","","","G4454840","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("118","93","","RSHAN AHAMMED","2016-09-07","0","M","","0","","","P0352855","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("119","93","","TANIA ISRA","2016-09-07","0","F","","0","","","K5269033","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("120","138","","NAHIM","0000-00-00","0","M","","0","","","K2828090","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("121","138","","ABDU RAHIMAN ","2000-09-01","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("122","138","","SREEJITH","2000-09-01","0","M","","0","","","K8893537","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("123","138","","JAMES ","0000-00-00","0","M","","0","","","J4059812","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("124","138","","NAHIM","0000-00-00","0","M","","0","","","K2828090","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("125","98","Mr","Ainikkunnan Jayafer","1992-08-02","24","M","VETTICHIRA","0","Indian","","M7297991","2015-03-19","2025-03-18","MALAPPURAM","","","","2","6","2");
INSERT INTO passenger VALUES("126","95","","ANSAR","0000-00-00","0","M","","0","","","M5169954","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("127","95","","ANILA","2000-09-01","0","F","","0","","","N7681273","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("128","95","","PABRI CK","2000-09-01","0","F","","0","","","N7686960","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("129","95","","PATRICIA ANJELIN","2000-09-01","0","F","","0","","","P2488318","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("130","95","","REMA KUMARI","2000-09-01","0","F","","0","","","N9101342","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("131","95","","KAVITHA","2000-09-01","0","F","","0","","","K6437126","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("132","95","","RAMANATHAN","0000-00-00","0","M","","0","","","M6237420","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("133","95","","Fathima Hanna","0000-00-00","0","F","","0","","","L7896144","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("134","96","","RICHARD","2016-09-07","0","M","","0","","","H4603974","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("135","95","","THAJUDHEEN","2016-09-07","0","M","","0","","","K1076374","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("136","129","","SUSHANTH","2016-09-07","0","M","","0","","","K5230981","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("137","136","","SAJEEVAN","2016-09-07","0","M","","0","","","M0569663","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("138","139","","AYISHA KUNNATH","1978-01-02","0","F","","0","","","P0915315","0000-00-00","0000-00-00","","","","","2","6","2");
INSERT INTO passenger VALUES("139","95","","SABEENA 1118","0000-00-00","0","F","","0","","","	J5242293","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("140","95","","FERVEES 1199","2000-09-05","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("141","95","","VIJESH 1222","2000-09-01","0","M","","0","","","	K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("142","95","","ANEEM 1223","2000-09-01","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("143","95","","FERVEES 1199","2000-09-05","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("144","95","","VIJESH 1222","2000-09-01","0","M","","0","","","	K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("145","95","","ANEEM 1223","2000-09-01","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("146","95","","KAVERI","0000-00-00","0","M","","0","","","K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("147","95","","AKHILA 12200","2016-09-01","0","F","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("148","95","","ISHAK 1224","2000-09-01","0","M","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("149","95","","SREESHMA 1226","2000-09-01","0","F","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("150","95","","VYSHAKH 1233","2000-09-06","0","M","","0","","","n9553909 ","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("151","95","","AHAMEDKUTTY 1258","2000-09-01","0","M","","0","","","J2077389","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("152","95","","THARUN 1259","2000-09-01","0","M","","0","","","P0246374","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("153","95","","BINOY 1261","2000-09-01","0","M","","0","","","L6725925","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("154","95","","NOORUNNISA 1234","2000-09-01","0","F","","0","","","M7276307","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("155","95","","MUHAMMED RASHID 1234","2000-09-01","0","M","","0","","","P3999764","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("156","95","","ASHA MATHARI 1240","2000-09-01","0","F","","0","","","J5238983","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("157","95","","GEETHA 1250","2000-09-01","0","F","","0","","","N5577038","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("158","95","","1262	ANU MOLE","2000-09-01","0","F","","0","","","N7798897","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("159","95","","1264	VIJITHA","2000-09-01","0","F","","0","","","	P0428557","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("160","101","","VIKAS VINOD","2016-09-08","0","M","","0","","","J3924121","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("161","101","","VIKAS VINOD","2016-09-08","0","M","","0","","","J3924121","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("162","141","","AMAL PRABHAKAR","2016-09-08","0","M","","0","","","J66565291","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("163","142","","MUHAMMED SHAMMAS","2016-09-08","0","M","","0","","","L9931582","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("164","144","","MTM564 AHMED ORMUNDAKKAL","0000-00-00","0","M","","0","","","J4887754","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("165","130","","RAJAN KOCHU","2016-09-08","0","M","","0","","","K3751349","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("166","130","","MATHUKUTTY","2016-09-08","0","M","","0","","","L1747043","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("167","103","","AYISHA","0000-00-00","0","F","","0","","","P0615315","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("168","145","","RAHILA","2016-09-08","0","F","","0","","","L4220391","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("169","145","","FATHIMA MIHRA","2016-09-08","0","F","","0","","","M3684407","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("170","145","","MUHAMMED MIRSHAD","2016-09-08","0","F","","0","","","N5538499","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("171","96","","HIDAYATHULLA","0000-00-00","0","M","","0","","","J8843095","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("172","96","","MTM615 HIDAYATHULLA","0000-00-00","0","M","","0","","","J8843095","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("173","129","","AYISHA","2000-09-01","0","F","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("174","129","","FATHIMA","2000-09-01","0","F","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("175","129","","FAHEENA","2000-09-01","0","M","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("176","129","","1196	MUHAMMED ALI","0000-00-00","0","M","","0","","","K6459585","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("177","95","","1196	MUHAMMED ALI","0000-00-00","0","M","","0","","","0","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("178","95","","1197	SAREENA","2000-09-01","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("179","95","","1198   VALSALA","2000-09-01","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("180","95","","1245	NASREENA ","2000-09-01","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("181","95","","1245    AYSHANA RAMEES","2010-09-01","0","F","","0","","","M5856848	","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("182","95","","1197	SAREENA","2000-09-01","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("183","95","","1198   VALSALA","2000-09-01","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("184","95","","1245	NASREENA ","2000-09-01","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("185","95","","1245    AYSHANA RAMEES","2010-09-01","0","F","","0","","","M5856848	","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("186","148","","mohammed","0000-00-00","0","M","","0","","","k8000829","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("187","95","","SAREENA VALSALA NASREENA /AYSHANA RAMEES","2010-09-01","0","F","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("188","95","","VALSALA","2010-09-01","0","F","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("189","95","","NASREENA","2000-09-01","0","F","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("190","95","","AYSHANA RAMEES","2000-09-01","0","F","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("191","129","","1247	HISHAM MASHOOD","0000-00-00","0","M","","0","","","J5440840","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("192","129","","1247	HISHAM MASHOOD","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("193","129","","1247	HISHAM MASHOOD","0000-00-00","0","M","","0","","","J5440840","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("194","129","","1254	ASHRAF PURATH PARAMBATH","0000-00-00","0","M","","0","","","H5686487","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("195","129","","1254	ASHRAF PURATH PARAMBATH","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("196","129","","1197	SAREENA","0000-00-00","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("197","129","","1198   VALSALA","0000-00-00","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("198","129","","1245	NASREENA","0000-00-00","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("199","129","","1245    AYSHANA RAMEES","0000-00-00","0","F","","0","","","M5856848","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("200","129","","1197	SAREENA","0000-00-00","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("201","129","","1198   VALSALA","0000-00-00","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("202","129","","1245	NASREENA","0000-00-00","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("203","129","","1245    AYSHANA RAMEES","0000-00-00","0","F","","0","","","M5856848","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("204","149","","SABAD MUHAMMED","0000-00-00","0","M","","0","","","M4747049","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("205","149","","MUHAMMED ALI MUHAMMED","0000-00-00","0","M","","0","","","H5110153","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("206","114","","HAMDAN HAMEED","2016-09-09","0","M","","0","","","P2203649","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("207","93","","JAMSHAD","2016-09-09","0","M","","0","","","H1608955","0000-00-00","0000-00-00","","","","","1","9","2");
INSERT INTO passenger VALUES("208","129","","1196	MUHAMMED ALI","0000-00-00","0","M","","0","","","0","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("209","129","","1197	SAREENA","0000-00-00","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("210","129","","1198   VALSALA","0000-00-00","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("211","129","","1245	NASREENA","0000-00-00","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("212","129","","1245    AYSHANA RAMEES","0000-00-00","0","F","","0","","","M5856848","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("213","129","","1245    AYSHANA RAMEES","0000-00-00","0","F","","0","","","M5856848","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("214","129","","1245	NASREENA","0000-00-00","0","F","","0","","","M0882717","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("215","129","","1198   VALSALA","0000-00-00","0","F","","0","","","N2189073","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("216","129","","1197	SAREENA","0000-00-00","0","F","","0","","","K9858317","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("217","129","","SAREENA VALSALA NASREENA /AYSHANA RAMEES","0000-00-00","0","F","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("218","129","","VALSALA","0000-00-00","0","F","","0","","","K000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("219","129","","NASREENA","0000-00-00","0","F","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("220","129","","AYSHANA RAMEES","0000-00-00","0","F","","0","","","K00000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("221","129","","1196	MUHAMMED ALI","0000-00-00","0","M","","0","","","K6459585","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("222","99","","COUNTER BALANCE,","0000-00-00","0","M","","0","","","k000000","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("223","150","Mr","VALIYA VALAPPIL ALI ABDUL KAREEM","1967-05-01","49","M","PAVITTAPPURAM","0","Indian","","K4621024","2012-03-19","2022-03-18","ABU DHABI","","","","2","6","2");
INSERT INTO passenger VALUES("224","150","Mrs","NADEERA ABDUL KAREEM","1970-05-15","46","F","MANNALAMKUNNU","0","Indian","","G5039690","2008-12-03","2018-12-02","ABUDHABI","","","","2","6","2");
INSERT INTO passenger VALUES("225","95","","FERVEES 1199","0000-00-00","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("226","95","","VIJESH 1222","0000-00-00","0","M","","0","","","K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("227","95","","ANEEM 1223","0000-00-00","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("228","95","","FERVEES 1199","0000-00-00","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("229","95","","VIJESH 1222","0000-00-00","0","M","","0","","","K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("230","95","","ANEEM 1223","0000-00-00","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("231","95","","FERVEES 1199","0000-00-00","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("232","95","","VIJESH 1222","0000-00-00","0","M","","0","","","K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("233","95","","ANEEM 1223","0000-00-00","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("234","96","","UDAYA MOOLYA","0000-00-00","0","M","","0","","","J7091327","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("235","96","","KADAMBU NEBISA,ANNATH,MAHAMMAD SHAFIK,MAHAMMAD NAW","0000-00-00","0","M","","0","","","L6156737.P2778220,P2","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("236","152","","SHAFJAN PATTILLATH CHANDERA","0000-00-00","0","M","","0","","","H5103542","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("237","96","","","0000-00-00","0","","","0","","","","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("238","95","","MISRIYA","0000-00-00","0","M","","0","","","P1902660","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("239","96","","FATHIMA HASHIFA","0000-00-00","0","F","","0","","","N7809054","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("240","96","","FATHIMA HASHIFA","0000-00-00","0","F","","0","","","N7809054","0000-00-00","0000-00-00","","","","","1","8","2");
INSERT INTO passenger VALUES("241","95","","ANEEM 1223","0000-00-00","0","M","","0","","","K5283027","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("242","95","","VIJESH 1222","0000-00-00","0","M","","0","","","K7027177","0000-00-00","0000-00-00","","","","","1","10","2");
INSERT INTO passenger VALUES("243","95","","FERVEES 1199","0000-00-00","0","M","","0","","","J5802083","0000-00-00","0000-00-00","","","","","1","10","2");

DROP TABLE IF EXISTS purchase_airline;
CREATE TABLE `purchase_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
INSERT INTO purchase_airline VALUES("1","1","2016-09-01","2016-09-01","81","5","1","1000","0","0","0","0","0","0","0","1000","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("2","2","2016-09-01","2016-09-01","83","5","2","14015","0","0","0","0","0","0","0","14015","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("3","3","2016-09-03","2016-09-03","81","5","3","14730","0","0","0","0","0","0","0","14730","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("4","4","2016-09-03","2016-09-03","83","5","4","9080","0","0","0","0","0","0","0","9080","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("5","5","2016-09-03","2016-09-03","83","5","5","8870","0","0","0","0","0","0","0","8870","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("7","6","2016-09-03","2016-09-03","83","5","6","9130","0","0","0","0","0","0","0","9130","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("8","7","2016-09-05","2016-09-05","83","5","7","20100","0","0","0","0","0","0","0","20100","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("9","8","2016-09-05","2016-09-05","83","5","8","8385","0","0","0","0","0","0","0","8385","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("10","9","2016-09-05","2016-09-05","83","5","9","47570","0","0","0","0","0","0","0","47570","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("11","10","2016-09-05","2016-09-05","83","5","10","43660","0","0","0","0","0","0","0","43660","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("12","11","2016-09-05","2016-09-07","81","5","11","24520","0","0","0","0","0","0","0","24520","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("13","12","2016-09-05","2016-09-05","81","5","12","1300","0","0","0","0","0","0","0","1300","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("14","13","2016-09-06","2016-09-06","81","5","13","32220","0","0","0","0","0","0","0","32220","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("15","14","2016-09-06","2016-09-06","83","5","14","21245","0","0","0","0","0","0","0","21245","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("16","15","2016-09-06","2016-09-06","81","5","15","126500","0","0","0","0","0","0","0","126500","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("17","16","2016-09-06","2016-09-06","83","5","16","17930","0","0","0","0","0","0","0","17930","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("18","17","2016-09-06","2016-09-06","83","5","17","400","0","0","0","0","0","0","0","400","","","0","0","2","6","2");
INSERT INTO purchase_airline VALUES("19","18","2016-09-09","2016-09-09","81","5","18","67030","0","0","0","0","0","0","0","67030","","","0","0","2","6","2");

DROP TABLE IF EXISTS purchase_hotel;
CREATE TABLE `purchase_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_misc;
CREATE TABLE `purchase_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO purchase_misc VALUES("1","1","2016-09-08","2016-09-08","146","15","1","1500","0","0","0","0","0","0","0","1500","","","0","0","1","10","2");
INSERT INTO purchase_misc VALUES("2","2","2016-09-09","2016-09-09","146","15","2","15000","0","0","0","0","0","0","0","15000","Fly Hind haj ticket date change","","0","0","1","5","2");

DROP TABLE IF EXISTS purchase_pac_airline;
CREATE TABLE `purchase_pac_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_airline;
CREATE TABLE `purchase_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date DEFAULT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL DEFAULT '0',
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_hotel;
CREATE TABLE `purchase_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_transport;
CREATE TABLE `purchase_package_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_visa;
CREATE TABLE `purchase_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_transport;
CREATE TABLE `purchase_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_visa;
CREATE TABLE `purchase_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;
INSERT INTO purchase_visa VALUES("45","1","2016-09-03","2016-09-03","82","7","1","15000","0","0","0","0","0","0","0","15000","","","0","0","2","6","2");
INSERT INTO purchase_visa VALUES("46","1","2016-09-05","2016-09-05","94","7","1","5800","0","0","0","0","0","0","0","5800","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("47","2","2016-09-05","2016-09-05","94","7","2","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("48","3","2016-09-05","2016-09-05","94","7","3","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("49","4","2016-09-05","2016-09-05","97","7","4","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("50","5","2016-09-05","2016-09-05","94","7","5","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("51","6","2016-09-05","2016-09-05","94","7","6","5100","0","0","0","0","0","0","0","5100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("52","7","2016-09-05","2016-09-05","94","7","7","28200","0","0","0","0","0","0","0","28200","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("53","8","2016-09-05","2016-09-05","94","7","8","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("54","9","2016-09-05","2016-09-05","102","7","9","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("55","10","2016-09-05","2016-09-05","94","7","10","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("56","11","2016-09-05","2016-09-05","102","7","11","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("57","12","2016-09-05","2016-09-05","102","7","12","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("58","13","2016-09-05","2016-09-05","94","7","13","14100","0","0","0","0","0","0","0","14100","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("59","14","2016-09-05","2016-09-05","102","7","14","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("60","15","2016-09-05","2016-09-05","102","7","15","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("61","16","2016-09-05","2016-09-10","102","7","16","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("62","17","2016-09-05","2016-09-05","102","7","17","14500","0","0","0","0","0","0","0","14500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("63","18","2016-09-06","2016-09-06","94","7","18","5100","0","0","0","0","0","0","0","5100","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("64","19","2016-09-06","2016-09-06","94","7","19","300","0","0","0","0","0","0","0","300","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("65","20","2016-09-06","2016-09-06","97","7","21","3300","0","0","0","0","0","0","0","3300","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("66","21","2016-09-06","2016-09-06","125","7","22","5300","0","0","0","0","0","0","0","5300","","","0","0","1","5","2");
INSERT INTO purchase_visa VALUES("67","22","2016-09-06","2016-09-10","127","7","23","15900","0","0","0","0","0","0","0","15900","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("68","23","2016-09-06","2016-09-06","127","7","24","15900","0","0","0","0","0","0","0","15900","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("69","24","2016-09-06","2016-09-06","102","7","25","14700","0","0","0","0","0","0","0","14700","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("70","25","2016-09-06","2016-09-06","94","7","26","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("71","26","2016-09-06","2016-09-06","94","7","27","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("72","27","2016-09-06","2016-09-06","94","7","28","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("73","28","2016-09-06","2016-09-06","94","7","29","300","0","0","0","0","0","0","0","300","","","0","0","1","2","2");
INSERT INTO purchase_visa VALUES("74","29","2016-09-06","2016-09-06","97","7","30","29600","0","0","0","0","0","0","0","29600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("75","30","2016-09-06","2016-09-06","125","7","31","15500","0","0","0","0","0","0","0","15500","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("76","31","2016-09-06","2016-09-06","94","7","32","40800","0","0","0","0","0","0","0","40800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("77","32","2016-09-07","2016-09-07","94","7","33","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("78","33","2016-09-07","2016-09-07","102","7","34","14700","0","0","0","0","0","0","0","14700","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("79","34","2016-09-07","2016-09-07","97","7","35","15800","0","0","0","0","0","0","0","15800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("80","35","2016-09-07","2016-09-07","94","7","36","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("81","36","2016-09-07","2016-09-07","127","7","37","5150","0","0","0","0","0","0","0","5150","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("82","37","2016-09-07","2016-09-07","132","7","38","29600","0","0","0","0","0","0","0","29600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("83","38","2016-09-07","2016-09-07","94","7","39","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("84","39","2016-09-07","2016-09-07","132","7","40","16000","0","0","0","0","0","0","0","16000","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("85","40","2016-09-07","2016-09-07","132","7","41","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("86","41","2016-09-07","2016-09-07","132","7","42","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("87","42","2016-09-07","2016-09-07","132","7","43","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("88","43","2016-09-07","2016-09-07","132","7","44","29600","0","0","0","0","0","0","0","29600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("89","44","2016-09-07","2016-09-07","132","7","45","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("90","45","2016-09-07","2016-09-07","132","7","46","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("91","46","2016-09-07","2016-09-07","94","7","47","183300","0","0","0","0","0","0","0","183300","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("92","47","2016-09-07","2016-09-07","94","7","48","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("93","48","2016-09-07","2016-09-07","132","7","49","44400","0","0","0","0","0","0","0","44400","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("94","49","2016-09-07","2016-09-07","94","7","50","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("95","50","2016-09-07","2016-09-07","132","7","51","29600","0","0","0","0","0","0","0","29600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("96","51","2016-09-07","2016-09-07","94","7","52","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("97","52","2016-09-07","2016-09-07","132","7","53","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("98","53","2016-09-07","2016-09-07","132","7","54","29600","0","0","0","0","0","0","0","29600","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("99","54","2016-09-07","2016-09-07","94","7","55","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("100","55","2016-09-07","2016-09-07","94","7","56","28200","0","0","0","0","0","0","0","28200","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("101","56","2016-09-07","2016-09-07","94","7","57","5100","0","0","0","0","0","0","0","5100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("102","57","2016-09-07","2016-09-07","132","7","58","44400","0","0","0","0","0","0","0","44400","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("103","58","2016-09-07","2016-09-07","125","7","59","400","0","0","0","0","0","0","0","400","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("104","59","2016-09-07","2016-09-07","125","7","60","600","0","0","0","0","0","0","0","600","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("105","60","2016-09-07","2016-09-07","125","7","61","14600","0","0","0","0","0","0","0","14600","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("106","61","2016-09-07","2016-09-07","125","7","62","14200","0","0","0","0","0","0","0","14200","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("107","62","2016-09-07","2016-09-07","125","7","63","6900","0","0","0","0","0","0","0","6900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("108","63","2016-09-07","2016-09-07","125","7","64","4600","0","0","0","0","0","0","0","4600","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("109","64","2016-09-07","2016-09-07","125","7","65","2600","0","0","0","0","0","0","0","2600","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("110","65","2016-09-07","2016-09-07","125","7","66","14900","0","0","0","0","0","0","0","14900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("111","66","2016-09-07","2016-09-07","94","7","67","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("112","67","2016-09-07","2016-09-07","94","7","68","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("113","68","2016-09-07","2016-09-07","94","7","69","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("114","69","2016-09-07","2016-09-07","132","7","70","14800","0","0","0","0","0","0","0","14800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("115","2","2016-09-07","2016-09-07","82","7","2","5500","0","0","0","0","0","0","0","5500","","","0","0","2","6","2");
INSERT INTO purchase_visa VALUES("116","70","2016-09-07","2016-09-07","125","7","71","7700","0","0","0","0","0","0","0","7700","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("118","72","2016-09-07","2016-09-07","125","7","74","14900","0","0","0","0","0","0","0","14900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("119","73","2016-09-07","2016-09-07","125","7","75","900","0","0","0","0","0","0","0","900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("120","74","2016-09-07","2016-09-07","125","7","76","58400","0","0","0","0","0","0","0","58400","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("121","75","2016-09-07","2016-09-07","125","7","77","88200","0","0","0","0","0","0","0","88200","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("122","76","2016-09-08","2016-09-08","97","7","78","8000","0","0","0","0","0","0","0","8000","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("123","77","2016-09-08","2016-09-08","140","7","79","5500","0","0","0","0","0","0","0","5500","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("124","78","2016-09-08","2016-09-08","140","7","80","6500","0","0","0","0","0","0","0","6500","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("125","79","2016-09-08","2016-09-08","94","7","81","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("126","80","2016-09-08","2016-09-08","125","7","82","16500","0","0","0","0","0","0","0","16500","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("127","81","2016-09-08","2016-09-08","94","7","83","5100","0","0","0","0","0","0","0","5100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("128","82","2016-09-08","2016-09-08","94","7","84","14100","0","0","0","0","0","0","0","14100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("129","83","2016-09-08","2016-09-08","94","7","85","5100","0","0","0","0","0","0","0","5100","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("130","84","2016-09-08","2016-09-08","132","7","86","44400","0","0","0","0","0","0","0","44400","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("131","85","2016-09-08","2016-09-08","125","7","87","14500","0","0","0","0","0","0","0","14500","COUNTER 1204","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("132","86","2016-09-08","2016-09-08","125","7","88","11000","0","0","0","0","0","0","0","11000","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("133","87","2016-09-08","2016-09-08","147","7","89","900","0","0","0","0","0","0","0","900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("134","88","2016-09-08","2016-09-09","125","7","90","11400","0","0","0","0","0","0","0","11400","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("135","89","2016-09-08","2016-09-09","97","7","91","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("137","91","2016-09-08","2016-09-08","97","7","94","3300","0","0","0","0","0","0","0","3300","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("138","92","2016-09-08","2016-09-09","97","7","95","1200","0","0","0","0","0","0","0","1200","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("139","93","2016-09-08","2016-09-08","125","7","96","15900","0","0","0","0","0","0","0","15900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("140","94","2016-09-08","2016-09-08","97","7","97","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("141","95","2016-09-08","2016-09-08","97","7","97","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("142","96","2016-09-08","2016-09-08","125","7","98","15900","0","0","0","0","0","0","0","15900","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("143","97","2016-09-08","2016-09-08","97","7","99","300","0","0","0","0","0","0","0","300","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("144","98","2016-09-08","2016-09-08","125","7","100","7000","0","0","0","0","0","0","0","7000","COUNTER 1180","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("145","99","2016-09-08","2016-09-08","125","7","101","7000","0","0","0","0","0","0","0","7000","COUNTER 1182","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("146","100","2016-09-09","2016-09-09","132","7","102","15800","0","0","0","0","0","0","0","15800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("147","101","2016-09-09","2016-09-09","132","7","103","15800","0","0","0","0","0","0","0","15800","","","0","0","1","9","2");
INSERT INTO purchase_visa VALUES("148","102","2016-09-09","2016-09-09","125","7","104","78325","0","0","0","0","0","0","0","78325","","","0","0","1","10","2");
INSERT INTO purchase_visa VALUES("149","103","2016-09-10","2016-09-10","94","7","105","5100","0","0","0","0","0","0","0","5100","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("150","104","2016-09-10","2016-09-10","94","7","106","20400","0","0","0","0","0","0","0","20400","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("151","105","2016-09-10","2016-09-10","94","7","107","5100","0","0","0","0","0","0","0","5100","","","0","0","1","8","2");
INSERT INTO purchase_visa VALUES("152","106","2016-09-10","2016-09-10","97","7","108","11000","0","0","0","0","0","0","0","11000","","","0","0","1","8","2");

DROP TABLE IF EXISTS relationship;
CREATE TABLE `relationship` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `relationship` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS room_type;
CREATE TABLE `room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `roomType` varchar(30) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
INSERT INTO room_type VALUES("1","Sharing","0","1","0");
INSERT INTO room_type VALUES("2","Twin","0","1","0");
INSERT INTO room_type VALUES("3","Triple","0","1","0");
INSERT INTO room_type VALUES("4","Four Bed","0","1","0");
INSERT INTO room_type VALUES("5","Five Bed","0","1","0");
INSERT INTO room_type VALUES("6","Six Bed","0","1","0");
INSERT INTO room_type VALUES("7","Child With Bed","0","1","0");
INSERT INTO room_type VALUES("8","Child Without Bed","0","1","0");

DROP TABLE IF EXISTS staff;
CREATE TABLE `staff` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffName` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `userType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
INSERT INTO staff VALUES("1","Sidheeq","8547116314","tssidheeq@gmail.com","staf","1","2","2");
INSERT INTO staff VALUES("2","Manu","9544306950","mtmvettichira@gmail.com","","2","3","2");
INSERT INTO staff VALUES("3","Abdul Nasar","8281757040","mtmvettichira@gmail.com","","2","3","2");
INSERT INTO staff VALUES("4","Safuvan","9995959734","mtmsafuvan@gmail.com","VISA STAFF","1","2","2");
INSERT INTO staff VALUES("5","Sahad","9567656784","mtmsahad@gmail.com","VISA STAFF","1","2","2");
INSERT INTO staff VALUES("6","Irfad","9746915105","irfadamanath1@gmail.com","VISA STAFF","1","2","2");
INSERT INTO staff VALUES("7","Hyderali Staff","9562706022","hydermkm@gmail.com","TICKETING STAFF","1","2","2");
INSERT INTO staff VALUES("8","Siraj Saquafi","9961953870","sirajsaquafimtm@gmail.com","Umra","1","2","2");
INSERT INTO staff VALUES("9","Muhammedsha","8089684701","muhammedshamtm@gmail.com","umrah","1","2","2");
INSERT INTO staff VALUES("11","Akhil","9486747270","akhilkpmtm@gmail.com","ACCOUNTS","1","2","2");
INSERT INTO staff VALUES("12","Hyder","9562706022","hydermkm@gmail.com","","1","2","2");
INSERT INTO staff VALUES("13","Hyderali","9846529001","hydermkm@gmail.com","","4","17","2");
INSERT INTO staff VALUES("14","Siraj Saquafi","9961953870","sirajsaquafimtm@gmail.com","Umra","4","17","2");
INSERT INTO staff VALUES("15","Faizal M","9605198598","","","4","17","2");
INSERT INTO staff VALUES("16","Muhammedsha","8089684701","muhammedsha87@gmail.com","umrah","4","17","2");
INSERT INTO staff VALUES("17","Akhil K P","9486747270","AKHILKP496@GMAIL.COM","ACCOUNTS","4","17","2");
INSERT INTO staff VALUES("18","Sadham","9999009999","","","4","17","2");
INSERT INTO staff VALUES("19","Faizyyyyy","9605198598","","","4","17","2");
INSERT INTO staff VALUES("22","Faizal","9605198598","FAIZAL@TRAVELMART.BIZ","ACCOUNTS","1","2","2");

DROP TABLE IF EXISTS transaction;
CREATE TABLE `transaction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `fromLedger` int(11) NOT NULL,
  `toLedger` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(100) NOT NULL,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `remark` text NOT NULL,
  `addedDate` date NOT NULL,
  `transactionDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `referenceNo` int(11) NOT NULL,
  `details` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=779 DEFAULT CHARSET=latin1;
INSERT INTO transaction VALUES("1","86","4","1","Airline Receivable","0","1000","Airline Receivable: 1","2016-09-01","2016-09-01","6","2","2","0","INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("2","4","86","1","Airline Receivable","1000","0","Airline Receivable: 1","2016-09-01","2016-09-01","6","2","2","0","INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("3","81","5","1","Airline Payable","1000","0","","2016-09-01","2016-09-01","6","2","2","0","Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("4","5","81","1","Airline Payable","0","1000","","2016-09-01","2016-09-01","6","2","2","0","Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("5","87","4","2","Airline Receivable","0","14050","Airline Receivable: 2","2016-09-01","2016-09-01","6","2","2","0","INV#IA2, Pax:THALE KUNNATH/JASEEM, Passport No:, Sector:DOH/CCJ, Date:07-12-2016, Tkt:607-9164761434, PNR:3I53TT, By:Manu");
INSERT INTO transaction VALUES("6","4","87","2","Airline Receivable","14050","0","Airline Receivable: 2","2016-09-01","2016-09-01","6","2","2","0","INV#IA2, Pax:THALE KUNNATH/JASEEM, Passport No:, Sector:DOH/CCJ, Date:07-12-2016, Tkt:607-9164761434, PNR:3I53TT, By:Manu");
INSERT INTO transaction VALUES("7","83","5","2","Airline Payable","14015","0","","2016-09-01","2016-09-01","6","2","2","0","Details:INV#IA2, Pax:THALE KUNNATH/JASEEM, Passport No:, Sector:DOH/CCJ, Date:07-12-2016, Tkt:607-9164761434, PNR:3I53TT, By:Manu");
INSERT INTO transaction VALUES("8","5","83","2","Airline Payable","0","14015","","2016-09-01","2016-09-01","6","2","2","0","Details:INV#IA2, Pax:THALE KUNNATH/JASEEM, Passport No:, Sector:DOH/CCJ, Date:07-12-2016, Tkt:607-9164761434, PNR:3I53TT, By:Manu");
INSERT INTO transaction VALUES("9","88","6","1","Visa Receivable","0","16500","Visa Receivable : 1","2016-09-03","2016-09-03","6","2","2","0","INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("10","6","88","1","Visa Receivable","16500","0","Visa Receivable : 1","2016-09-03","2016-09-03","6","2","2","0","INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("11","82","7","1","Visa Payable","15000","0","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("12","7","82","1","Visa Payable","0","15000","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("13","89","4","3","Airline Receivable","0","15000","Airline Receivable: 3","2016-09-03","2016-09-03","6","2","2","0","INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("14","4","89","3","Airline Receivable","15000","0","Airline Receivable: 3","2016-09-03","2016-09-03","6","2","2","0","INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("15","81","5","3","Airline Payable","14730","0","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("16","5","81","3","Airline Payable","0","14730","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("17","90","4","4","Airline Receivable","0","9500","Airline Receivable: 4","2016-09-03","2016-09-03","6","2","2","0","INV#IA4, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:CCJ/DXB, Date:22-12-2016, Tkt:098-9164977278, PNR:3OEJL9, By:Manu");
INSERT INTO transaction VALUES("18","4","90","4","Airline Receivable","9500","0","Airline Receivable: 4","2016-09-03","2016-09-03","6","2","2","0","INV#IA4, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:CCJ/DXB, Date:22-12-2016, Tkt:098-9164977278, PNR:3OEJL9, By:Manu");
INSERT INTO transaction VALUES("19","83","5","4","Airline Payable","9080","0","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA4, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:CCJ/DXB, Date:22-12-2016, Tkt:098-9164977278, PNR:3OEJL9, By:Manu");
INSERT INTO transaction VALUES("20","5","83","4","Airline Payable","0","9080","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA4, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:CCJ/DXB, Date:22-12-2016, Tkt:098-9164977278, PNR:3OEJL9, By:Manu");
INSERT INTO transaction VALUES("29","90","2","1","Cash Receipts","19100","0","","2016-09-03","2016-09-03","6","2","2","0","VouNo#CR 1,INV#IA4,IA5 Received By :Manu,Details:INV#IA5, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:COK/DMK, Date:17-10-2016, Tkt:CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("30","2","90","1","Cash Receipts","0","19100","","2016-09-03","2016-09-03","6","2","2","0","VouNo#CR 1,INV#IA4,IA5 Received By :Manu,Details:INV#IA5, Pax:CHERUVAKKATH/ABDUL RAHEEM, Passport No:H0109737, Sector:COK/DMK, Date:17-10-2016, Tkt:CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("31","90","4","5","Airline Receivable","0","9600","Airline Receivable: 5","2016-09-03","2016-09-03","6","2","2","0","INV#IA5, Pax:Cheruvakkath/abdul Raheem, Passport No:H0109737, Sector:DMK/COK, Date:17-10-2016/19-10-2016, Tkt:CW3YVB/CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("32","4","90","5","Airline Receivable","9600","0","Airline Receivable: 5","2016-09-03","2016-09-03","6","2","2","0","INV#IA5, Pax:Cheruvakkath/abdul Raheem, Passport No:H0109737, Sector:DMK/COK, Date:17-10-2016/19-10-2016, Tkt:CW3YVB/CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("33","83","5","5","Airline Payable","8870","0","","2016-09-03","2016-09-03","6","2","2","0","INV#5,sup:83,:against:5,Details:INV#IA5, Pax:Cheruvakkath/abdul Raheem, Passport No:H0109737, Sector:DMK/COK, Date:17-10-2016/19-10-2016, Tkt:CW3YVB/CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("34","5","83","5","Airline Payable","0","8870","","2016-09-03","2016-09-03","6","2","2","0","INV#5,sup:83,:against:5,Details:INV#IA5, Pax:Cheruvakkath/abdul Raheem, Passport No:H0109737, Sector:DMK/COK, Date:17-10-2016/19-10-2016, Tkt:CW3YVB/CW3YVB, PNR:CW3YVB, By:Manu");
INSERT INTO transaction VALUES("39","91","4","6","Airline Receivable","0","9700","Airline Receivable: 6","2016-09-03","2016-09-03","6","2","2","0","INV#IA6, Pax:ODUVIL/AYYOOB MR, Passport No:M7851287, Sector:SHJ/CCJ, Date:05-09-2016, Tkt:098-9164977299, PNR:0, By:Manu");
INSERT INTO transaction VALUES("40","4","91","6","Airline Receivable","9700","0","Airline Receivable: 6","2016-09-03","2016-09-03","6","2","2","0","INV#IA6, Pax:ODUVIL/AYYOOB MR, Passport No:M7851287, Sector:SHJ/CCJ, Date:05-09-2016, Tkt:098-9164977299, PNR:0, By:Manu");
INSERT INTO transaction VALUES("41","83","5","6","Airline Payable","9130","0","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA6, Pax:ODUVIL/AYYOOB MR, Passport No:M7851287, Sector:SHJ/CCJ, Date:05-09-2016, Tkt:098-9164977299, PNR:0, By:Manu");
INSERT INTO transaction VALUES("42","5","83","6","Airline Payable","0","9130","","2016-09-03","2016-09-03","6","2","2","0","Details:INV#IA6, Pax:ODUVIL/AYYOOB MR, Passport No:M7851287, Sector:SHJ/CCJ, Date:05-09-2016, Tkt:098-9164977299, PNR:0, By:Manu");
INSERT INTO transaction VALUES("47","92","4","7","Airline Receivable","0","20600","Airline Receivable: 7","2016-09-05","2016-09-05","6","2","2","0","INV#IA7, Pax:Maliyekkal/muhammed Mukthar/POOVATIL/JUHANA , Passport No:M2664637, Sector:CCJ/AUH, Date:04-10-2016/04-10-2016, Tkt:607-9164977330/607-9164977328, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("48","4","92","7","Airline Receivable","20600","0","Airline Receivable: 7","2016-09-05","2016-09-05","6","2","2","0","INV#IA7, Pax:Maliyekkal/muhammed Mukthar/POOVATIL/JUHANA , Passport No:M2664637, Sector:CCJ/AUH, Date:04-10-2016/04-10-2016, Tkt:607-9164977330/607-9164977328, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("49","83","5","7","Airline Payable","20100","0","","2016-09-05","2016-09-05","6","2","2","0","INV#7,sup:83,:against:7,Details:INV#IA7, Pax:Maliyekkal/muhammed Mukthar/POOVATIL/JUHANA , Passport No:M2664637, Sector:CCJ/AUH, Date:04-10-2016/04-10-2016, Tkt:607-9164977330/607-9164977328, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("50","5","83","7","Airline Payable","0","20100","","2016-09-05","2016-09-05","6","2","2","0","INV#7,sup:83,:against:7,Details:INV#IA7, Pax:Maliyekkal/muhammed Mukthar/POOVATIL/JUHANA , Passport No:M2664637, Sector:CCJ/AUH, Date:04-10-2016/04-10-2016, Tkt:607-9164977330/607-9164977328, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("51","92","4","8","Airline Receivable","0","8700","Airline Receivable: 8","2016-09-05","2016-09-05","6","2","2","0","INV#IA8, Pax:POOVATIL/JUHANA , Passport No:M2664637, Sector:SHJ/CCJ, Date:01-01-2017, Tkt:589-9164977329, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("52","4","92","8","Airline Receivable","8700","0","Airline Receivable: 8","2016-09-05","2016-09-05","6","2","2","0","INV#IA8, Pax:POOVATIL/JUHANA , Passport No:M2664637, Sector:SHJ/CCJ, Date:01-01-2017, Tkt:589-9164977329, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("53","83","5","8","Airline Payable","8385","0","","2016-09-05","2016-09-05","6","2","2","0","Details:INV#IA8, Pax:POOVATIL/JUHANA , Passport No:M2664637, Sector:SHJ/CCJ, Date:01-01-2017, Tkt:589-9164977329, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("54","5","83","8","Airline Payable","0","8385","","2016-09-05","2016-09-05","6","2","2","0","Details:INV#IA8, Pax:POOVATIL/JUHANA , Passport No:M2664637, Sector:SHJ/CCJ, Date:01-01-2017, Tkt:589-9164977329, PNR:3OX2YM, By:Manu");
INSERT INTO transaction VALUES("73","87","4","10","Airline Receivable","0","44600","Airline Receivable: 10","2016-09-05","2016-09-05","6","2","2","0","INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("74","4","87","10","Airline Receivable","44600","0","Airline Receivable: 10","2016-09-05","2016-09-05","6","2","2","0","INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("75","83","5","10","Airline Payable","43660","0","","2016-09-05","2016-09-05","6","2","2","0","INV#10,sup:83,:against:10,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("76","5","83","10","Airline Payable","0","43660","","2016-09-05","2016-09-05","6","2","2","0","INV#10,sup:83,:against:10,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("77","87","4","9","Airline Receivable","0","49000","Airline Receivable: 9","2016-09-05","2016-09-05","6","2","2","0","INV#IA9, Pax:Alqahtani/renad Faisal/ALQAHTANI/QAMRAA SAAD , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098 9164977342/098 9164977342/098-9164977341/098-9164977341, PNR:3OYGIE, By:Manu");
INSERT INTO transaction VALUES("78","4","87","9","Airline Receivable","49000","0","Airline Receivable: 9","2016-09-05","2016-09-05","6","2","2","0","INV#IA9, Pax:Alqahtani/renad Faisal/ALQAHTANI/QAMRAA SAAD , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098 9164977342/098 9164977342/098-9164977341/098-9164977341, PNR:3OYGIE, By:Manu");
INSERT INTO transaction VALUES("79","83","5","9","Airline Payable","47570","0","","2016-09-05","2016-09-05","6","2","2","0","INV#9,sup:83,:against:9,Details:INV#IA9, Pax:Alqahtani/renad Faisal/ALQAHTANI/QAMRAA SAAD , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098 9164977342/098 9164977342/098-9164977341/098-9164977341, PNR:3OYGIE, By:Manu");
INSERT INTO transaction VALUES("80","5","83","9","Airline Payable","0","47570","","2016-09-05","2016-09-05","6","2","2","0","INV#9,sup:83,:against:9,Details:INV#IA9, Pax:Alqahtani/renad Faisal/ALQAHTANI/QAMRAA SAAD , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098 9164977342/098 9164977342/098-9164977341/098-9164977341, PNR:3OYGIE, By:Manu");
INSERT INTO transaction VALUES("81","93","93","1","Opening Balance","0","35900","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#1, acc:GAMA HOLIDAYS CCJ,Opening:35900");
INSERT INTO transaction VALUES("82","1","93","1","Opening Balance","35900","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#1,acc:GAMA HOLIDAYS CCJ,Opening:35900");
INSERT INTO transaction VALUES("83","94","94","2","Opening Balance","83925","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#2, acc:SALIL PURCHASE,Opening:83925");
INSERT INTO transaction VALUES("84","1","94","2","Opening Balance","0","83925","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#2, acc:SALIL PURCHASE,Opening:83925");
INSERT INTO transaction VALUES("85","93","6","1","Visa Receivable","0","6200","Visa Receivable : 1","2016-09-05","2016-09-05","10","1","2","0","INV#IV1,Pax :MUFEED ABDUL AZIZ, PassNo :Z3668844/ visaFor:30 DAYS DXB SETTING,By:Irfad Staff");
INSERT INTO transaction VALUES("86","6","93","1","Visa Receivable","6200","0","Visa Receivable : 1","2016-09-05","2016-09-05","10","1","2","0","INV#IV1,Pax :MUFEED ABDUL AZIZ, PassNo :Z3668844/ visaFor:30 DAYS DXB SETTING,By:Irfad Staff");
INSERT INTO transaction VALUES("87","94","7","1","Visa Payable","5800","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("88","7","94","1","Visa Payable","0","5800","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("89","95","6","2","Visa Receivable","0","28700","Visa Receivable : 2","2016-09-05","2016-09-05","10","1","2","0","INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("90","6","95","2","Visa Receivable","28700","0","Visa Receivable : 2","2016-09-05","2016-09-05","10","1","2","0","INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("91","94","7","2","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("92","7","94","2","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("94","1","96","3","Opening Balance","130500","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#3,acc:SAFUVAN STAFF,Opening:130500");
INSERT INTO transaction VALUES("95","96","6","3","Visa Receivable","0","14800","Visa Receivable : 3","2016-09-05","2016-09-05","10","1","2","0","INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("96","6","96","3","Visa Receivable","14800","0","Visa Receivable : 3","2016-09-05","2016-09-05","10","1","2","0","INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("97","94","7","3","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("98","7","94","3","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV3,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("99","96","6","4","Visa Receivable","0","300","Visa Receivable : 4","2016-09-05","2016-09-05","10","1","2","0","INV#IV4,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("100","6","96","4","Visa Receivable","300","0","Visa Receivable : 4","2016-09-05","2016-09-05","10","1","2","0","INV#IV4,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("101","97","7","4","Visa Payable","300","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV4,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("102","7","97","4","Visa Payable","0","300","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV4,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("104","1","99","4","Opening Balance","94225","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#4,acc:ALHIND COK,Opening:94225");
INSERT INTO transaction VALUES("105","99","6","5","Visa Receivable","0","14600","Visa Receivable : 5","2016-09-05","2016-09-05","10","1","2","0","INV#IV5,Pax :SHAHUNL ABDULNASAR, PassNo :G8487574/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("106","6","99","5","Visa Receivable","14600","0","Visa Receivable : 5","2016-09-05","2016-09-05","10","1","2","0","INV#IV5,Pax :SHAHUNL ABDULNASAR, PassNo :G8487574/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("107","94","7","5","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV5,Pax :SHAHUNL ABDULNASAR, PassNo :G8487574/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("108","7","94","5","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV5,Pax :SHAHUNL ABDULNASAR, PassNo :G8487574/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("109","96","6","6","Visa Receivable","0","5200","Visa Receivable : 6","2016-09-05","2016-09-05","10","1","2","0","INV#IV6,Pax :SHAJAHAN PALLILATH, PassNo :H5103542/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("110","6","96","6","Visa Receivable","5200","0","Visa Receivable : 6","2016-09-05","2016-09-05","10","1","2","0","INV#IV6,Pax :SHAJAHAN PALLILATH, PassNo :H5103542/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("111","94","7","6","Visa Payable","5100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV6,Pax :SHAJAHAN PALLILATH, PassNo :H5103542/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("112","7","94","6","Visa Payable","0","5100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV6,Pax :SHAJAHAN PALLILATH, PassNo :H5103542/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("113","95","6","7","Visa Receivable","0","29200","Visa Receivable : 7","2016-09-05","2016-09-05","10","1","2","0","INV#IV7,Pax :SIMON, PassNo :J1485230/Pax :ELSY, PassNo :J1485339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("114","6","95","7","Visa Receivable","29200","0","Visa Receivable : 7","2016-09-05","2016-09-05","10","1","2","0","INV#IV7,Pax :SIMON, PassNo :J1485230/Pax :ELSY, PassNo :J1485339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("115","94","7","7","Visa Payable","28200","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV7,Pax :SIMON, PassNo :J1485230/Pax :ELSY, PassNo :J1485339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("116","7","94","7","Visa Payable","0","28200","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV7,Pax :SIMON, PassNo :J1485230/Pax :ELSY, PassNo :J1485339/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("117","95","6","8","Visa Receivable","0","14600","Visa Receivable : 8","2016-09-05","2016-09-05","10","1","2","0","INV#IV8,Pax :AMEER, PassNo :M0314613/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("118","6","95","8","Visa Receivable","14600","0","Visa Receivable : 8","2016-09-05","2016-09-05","10","1","2","0","INV#IV8,Pax :AMEER, PassNo :M0314613/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("119","94","7","8","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV8,Pax :AMEER, PassNo :M0314613/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("120","7","94","8","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV8,Pax :AMEER, PassNo :M0314613/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("121","94","94","0","Opening Balance","0","83925","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#1, acc:SALIL PURCHASE,Opening:83925");
INSERT INTO transaction VALUES("122","1","94","0","Opening Balance","83925","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#1, acc:SALIL PURCHASE,Opening:83925");
INSERT INTO transaction VALUES("127","100","4","12","Airline Receivable","0","1800","Airline Receivable: 12","2016-09-05","2016-09-05","6","2","2","0","INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("128","4","100","12","Airline Receivable","1800","0","Airline Receivable: 12","2016-09-05","2016-09-05","6","2","2","0","INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("129","81","5","12","Airline Payable","1300","0","","2016-09-05","2016-09-05","6","2","2","0","Details:INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("130","5","81","12","Airline Payable","0","1300","","2016-09-05","2016-09-05","6","2","2","0","Details:INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("131","102","102","5","Opening Balance","0","90100","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#5, acc:TRAVEL PARTNER PURCHASE,Opening:90100");
INSERT INTO transaction VALUES("132","1","102","5","Opening Balance","90100","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#5, acc:TRAVEL PARTNER PURCHASE,Opening:90100");
INSERT INTO transaction VALUES("133","101","6","9","Visa Receivable","0","14800","Visa Receivable : 9","2016-09-05","2016-09-05","10","1","2","0","INV#IV9,Pax :ALLY JOY, PassNo :J0885006/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("134","6","101","9","Visa Receivable","14800","0","Visa Receivable : 9","2016-09-05","2016-09-05","10","1","2","0","INV#IV9,Pax :ALLY JOY, PassNo :J0885006/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("135","102","7","9","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV9,Pax :ALLY JOY, PassNo :J0885006/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("136","7","102","9","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV9,Pax :ALLY JOY, PassNo :J0885006/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("137","95","6","10","Visa Receivable","0","14600","Visa Receivable : 10","2016-09-05","2016-09-05","10","1","2","0","INV#IV10,Pax :DENIAL, PassNo :M9715530/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("138","6","95","10","Visa Receivable","14600","0","Visa Receivable : 10","2016-09-05","2016-09-05","10","1","2","0","INV#IV10,Pax :DENIAL, PassNo :M9715530/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("139","94","7","10","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV10,Pax :DENIAL, PassNo :M9715530/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("140","7","94","10","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV10,Pax :DENIAL, PassNo :M9715530/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("141","95","6","11","Visa Receivable","0","14700","Visa Receivable : 11","2016-09-05","2016-09-05","10","1","2","0","INV#IV11,Pax :SUSY, PassNo :N9538535/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("142","6","95","11","Visa Receivable","14700","0","Visa Receivable : 11","2016-09-05","2016-09-05","10","1","2","0","INV#IV11,Pax :SUSY, PassNo :N9538535/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("143","102","7","11","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV11,Pax :SUSY, PassNo :N9538535/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("144","7","102","11","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV11,Pax :SUSY, PassNo :N9538535/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("145","95","6","12","Visa Receivable","0","14700","Visa Receivable : 12","2016-09-05","2016-09-05","10","1","2","0","INV#IV12,Pax :RAHMATHUNNISA , PassNo :L9256080/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("146","6","95","12","Visa Receivable","14700","0","Visa Receivable : 12","2016-09-05","2016-09-05","10","1","2","0","INV#IV12,Pax :RAHMATHUNNISA , PassNo :L9256080/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("147","102","7","12","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV12,Pax :RAHMATHUNNISA , PassNo :L9256080/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("148","7","102","12","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV12,Pax :RAHMATHUNNISA , PassNo :L9256080/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("149","95","6","13","Visa Receivable","0","14600","Visa Receivable : 13","2016-09-05","2016-09-05","10","1","2","0","INV#IV13,Pax :SHANU MON, PassNo :J2491039/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("150","6","95","13","Visa Receivable","14600","0","Visa Receivable : 13","2016-09-05","2016-09-05","10","1","2","0","INV#IV13,Pax :SHANU MON, PassNo :J2491039/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("151","94","7","13","Visa Payable","14100","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV13,Pax :SHANU MON, PassNo :J2491039/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("152","7","94","13","Visa Payable","0","14100","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV13,Pax :SHANU MON, PassNo :J2491039/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("153","103","103","6","Opening Balance","0","2500","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#6, acc:MTM VETTICHIRA,Opening:2500");
INSERT INTO transaction VALUES("154","1","103","6","Opening Balance","2500","0","Opening Balance","2016-09-05","2016-09-05","10","1","2","0","INV#6,acc:MTM VETTICHIRA,Opening:2500");
INSERT INTO transaction VALUES("155","103","6","14","Visa Receivable","0","14700","Visa Receivable : 14","2016-09-05","2016-09-05","10","1","2","0","INV#IV14,Pax :MAIMOONA MANJAPPILLY, PassNo :M159786/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("156","6","103","14","Visa Receivable","14700","0","Visa Receivable : 14","2016-09-05","2016-09-05","10","1","2","0","INV#IV14,Pax :MAIMOONA MANJAPPILLY, PassNo :M159786/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("157","102","7","14","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV14,Pax :MAIMOONA MANJAPPILLY, PassNo :M159786/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("158","7","102","14","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV14,Pax :MAIMOONA MANJAPPILLY, PassNo :M159786/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("159","100","2","2","Cash Receipts","1800","0","","2016-09-05","2016-09-05","6","2","2","0","VouNo#CR 2,INV#IA12 Received By :Manu,Details:INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("160","2","100","2","Cash Receipts","0","1800","","2016-09-05","2016-09-05","6","2","2","0","VouNo#CR 2,INV#IA12 Received By :Manu,Details:INV#IA12, Pax:ABOOBACKER KARINGAPPARA, Passport No:, Sector:CCJ/MCT, Date:13-10-2016, Tkt:X7IPF7, PNR:0, By:Manu");
INSERT INTO transaction VALUES("161","98","2","3","Cash Receipts","24800","0","","2016-09-05","2016-09-05","6","2","2","0","VouNo#CR 3,INV#IA11 Received By :Manu,Details:INV#IA11, Pax:AINIKKUNNAN JAYAFER, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("162","2","98","3","Cash Receipts","0","24800","","2016-09-05","2016-09-05","6","2","2","0","VouNo#CR 3,INV#IA11 Received By :Manu,Details:INV#IA11, Pax:AINIKKUNNAN JAYAFER, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("163","99","6","15","Visa Receivable","0","14800","Visa Receivable : 15","2016-09-05","2016-09-05","10","1","2","0","INV#IV15,Pax :MEENA HILARY, PassNo :M1342981/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("164","6","99","15","Visa Receivable","14800","0","Visa Receivable : 15","2016-09-05","2016-09-05","10","1","2","0","INV#IV15,Pax :MEENA HILARY, PassNo :M1342981/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("165","102","7","15","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV15,Pax :MEENA HILARY, PassNo :M1342981/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("166","7","102","15","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV15,Pax :MEENA HILARY, PassNo :M1342981/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("167","112","112","1","Opening Balance","0","1000","Opening Balance","2016-09-05","2016-09-05","17","4","2","0","INV#1, acc:AL HIND,Opening:1000");
INSERT INTO transaction VALUES("168","1","112","1","Opening Balance","1000","0","Opening Balance","2016-09-05","2016-09-05","17","4","2","0","INV#1, acc:AL HIND,Opening:1000");
INSERT INTO transaction VALUES("173","95","6","17","Visa Receivable","0","14600","Visa Receivable : 17","2016-09-05","2016-09-05","10","1","2","0","INV#IV17,Pax :AGHIL, PassNo :L9642581/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("174","6","95","17","Visa Receivable","14600","0","Visa Receivable : 17","2016-09-05","2016-09-05","10","1","2","0","INV#IV17,Pax :AGHIL, PassNo :L9642581/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("175","102","7","17","Visa Payable","14500","0","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV17,Pax :AGHIL, PassNo :L9642581/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("176","7","102","17","Visa Payable","0","14500","","2016-09-05","2016-09-05","10","1","2","0","Details:INV#IV17,Pax :AGHIL, PassNo :L9642581/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("177","119","4","1","Airline Receivable","0","1000","Airline Receivable: 1","2016-09-05","2016-09-05","18","4","2","0","INV#IA1, Pax:RIYAS , Passport No:0, Sector:CCJ/DXB, Date:08-09-2016, Tkt:06665879224, PNR:0, By:Hyderali");
INSERT INTO transaction VALUES("178","4","119","1","Airline Receivable","1000","0","Airline Receivable: 1","2016-09-05","2016-09-05","18","4","2","0","INV#IA1, Pax:RIYAS , Passport No:0, Sector:CCJ/DXB, Date:08-09-2016, Tkt:06665879224, PNR:0, By:Hyderali");
INSERT INTO transaction VALUES("179","119","19","1","Airline Receivable","0","65","Airline Receivable:Tds#1","2016-09-05","2016-09-05","18","4","2","0","INV#IA1, Pax:RIYAS , Passport No:0, Sector:CCJ/DXB, Date:08-09-2016, Tkt:06665879224, PNR:0, By:Hyderali");
INSERT INTO transaction VALUES("180","19","119","1","Airline Receivable","65","0","Airline Receivable:Tds#1","2016-09-05","2016-09-05","18","4","2","0","INV#IA1, Pax:RIYAS , Passport No:0, Sector:CCJ/DXB, Date:08-09-2016, Tkt:06665879224, PNR:0, By:Hyderali");
INSERT INTO transaction VALUES("181","118","4","2","Airline Receivable","0","15100","Airline Receivable: 2","2016-09-05","2016-09-05","22","4","2","0","INV#IA2, Pax:ARUNCUT, Passport No:AAAA, Sector:CCJ/DXB, Date:06-09-2016, Tkt:456456, PNR:0, By:Akhil K P");
INSERT INTO transaction VALUES("182","4","118","2","Airline Receivable","15100","0","Airline Receivable: 2","2016-09-05","2016-09-05","22","4","2","0","INV#IA2, Pax:ARUNCUT, Passport No:AAAA, Sector:CCJ/DXB, Date:06-09-2016, Tkt:456456, PNR:0, By:Akhil K P");
INSERT INTO transaction VALUES("183","96","6","18","Visa Receivable","0","5200","Visa Receivable : 18","2016-09-06","2016-09-06","8","1","2","0","INV#IV18,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan Staff");
INSERT INTO transaction VALUES("184","6","96","18","Visa Receivable","5200","0","Visa Receivable : 18","2016-09-06","2016-09-06","8","1","2","0","INV#IV18,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan Staff");
INSERT INTO transaction VALUES("185","94","7","18","Visa Payable","5100","0","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV18,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan Staff");
INSERT INTO transaction VALUES("186","7","94","18","Visa Payable","0","5100","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV18,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:30 DAYS DXB WITHOUT OTB,By:Safuvan Staff");
INSERT INTO transaction VALUES("187","96","6","19","Visa Receivable","0","300","Visa Receivable : 19","2016-09-06","2016-09-06","8","1","2","0","INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("188","6","96","19","Visa Receivable","300","0","Visa Receivable : 19","2016-09-06","2016-09-06","8","1","2","0","INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("189","94","7","19","Visa Payable","300","0","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("190","7","94","19","Visa Payable","0","300","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("191","123","6","20","Visa Receivable","0","3800","Visa Receivable : 20","2016-09-06","2016-09-06","8","1","2","0","INV#IV20,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("192","6","123","20","Visa Receivable","3800","0","Visa Receivable : 20","2016-09-06","2016-09-06","8","1","2","0","INV#IV20,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("193","123","6","21","Visa Receivable","0","3800","Visa Receivable : 21","2016-09-06","2016-09-06","8","1","2","0","INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("194","6","123","21","Visa Receivable","3800","0","Visa Receivable : 21","2016-09-06","2016-09-06","8","1","2","0","INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("195","16","123","21","Visa Receivable","0","100","Visa Receivable : 21","2016-09-06","2016-09-06","8","1","2","0","INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("196","123","16","21","Visa Receivable","100","0","Visa Receivable : 21","2016-09-06","2016-09-06","8","1","2","0","INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("197","97","7","20","Visa Payable","3300","0","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("198","7","97","20","Visa Payable","0","3300","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("203","124","6","22","Visa Receivable","0","5300","Visa Receivable : 22","2016-09-06","2016-09-06","5","1","2","0","INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("204","6","124","22","Visa Receivable","5300","0","Visa Receivable : 22","2016-09-06","2016-09-06","5","1","2","0","INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("205","125","7","21","Visa Payable","5300","0","","2016-09-06","2016-09-06","5","1","2","0","Details:INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("206","7","125","21","Visa Payable","0","5300","","2016-09-06","2016-09-06","5","1","2","0","Details:INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("207","124","2","1","Cash Receipts","5300","0","receipt for couter 16","2016-09-06","2016-09-06","5","1","2","0","VouNo#CR 1,INV#IV22 Received By :Sidheeq,Details:INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("208","2","124","1","Cash Receipts","0","5300","receipt for couter 16","2016-09-06","2016-09-06","5","1","2","0","VouNo#CR 1,INV#IV22 Received By :Sidheeq,Details:INV#IV22,Pax :ABDUL GAFOOR, PassNo :H2626002/ visaFor:90 DAYS DXB  WITH OTB,By:Sidheeq");
INSERT INTO transaction VALUES("217","96","2","2","Cash Receipts","5500","0","","2016-09-06","2016-09-06","15","1","2","0","VouNo#CR 2,INV#IV18,IV19 Received By :Akhil,Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("218","2","96","2","Cash Receipts","0","5500","","2016-09-06","2016-09-06","15","1","2","0","VouNo#CR 2,INV#IV18,IV19 Received By :Akhil,Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("223","126","4","13","Airline Receivable","0","32500","Airline Receivable: 13","2016-09-06","2016-09-06","6","2","2","0","INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("224","4","126","13","Airline Receivable","32500","0","Airline Receivable: 13","2016-09-06","2016-09-06","6","2","2","0","INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("225","81","5","13","Airline Payable","32220","0","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("226","5","81","13","Airline Payable","0","32220","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("231","96","6","24","Visa Receivable","0","16400","Visa Receivable : 24","2016-09-06","2016-09-06","8","1","2","0","INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("232","6","96","24","Visa Receivable","16400","0","Visa Receivable : 24","2016-09-06","2016-09-06","8","1","2","0","INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("233","127","7","23","Visa Payable","15900","0","","2016-09-06","2016-09-06","8","1","2","0","INV#23,sup:127,:against:24,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("234","7","127","23","Visa Payable","0","15900","","2016-09-06","2016-09-06","8","1","2","0","INV#23,sup:127,:against:24,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("239","127","2","1","Cash Payments","0","31800","","2016-09-06","2016-09-06","15","1","2","0","VouNo#CP1,INV#PV22,PV23 Paid By :Akhil To : ALTA CCJ,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("240","2","127","1","Cash Payments","31800","0","","2016-09-06","2016-09-06","15","1","2","0","VouNo#CP1,INV#PV22,PV23 Paid By :Akhil To : ALTA CCJ,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("241","128","4","14","Airline Receivable","0","22500","Airline Receivable: 14","2016-09-06","2016-09-06","6","2","2","0","INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("242","4","128","14","Airline Receivable","22500","0","Airline Receivable: 14","2016-09-06","2016-09-06","6","2","2","0","INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("243","83","5","14","Airline Payable","21245","0","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("244","5","83","14","Airline Payable","0","21245","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("245","89","4","15","Airline Receivable","0","129000","Airline Receivable: 15","2016-09-06","2016-09-06","6","2","2","0","INV#IA15, Pax:MACHINCHERY THOOMBIL, MOHAMED, Passport No:, Sector:CCJ/DXB, Date:16-12-2016/07-01-2017, Tkt:0B134I/0B134I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("246","4","89","15","Airline Receivable","129000","0","Airline Receivable: 15","2016-09-06","2016-09-06","6","2","2","0","INV#IA15, Pax:MACHINCHERY THOOMBIL, MOHAMED, Passport No:, Sector:CCJ/DXB, Date:16-12-2016/07-01-2017, Tkt:0B134I/0B134I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("247","81","5","15","Airline Payable","126500","0","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA15, Pax:MACHINCHERY THOOMBIL, MOHAMED, Passport No:, Sector:CCJ/DXB, Date:16-12-2016/07-01-2017, Tkt:0B134I/0B134I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("248","5","81","15","Airline Payable","0","126500","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA15, Pax:MACHINCHERY THOOMBIL, MOHAMED, Passport No:, Sector:CCJ/DXB, Date:16-12-2016/07-01-2017, Tkt:0B134I/0B134I, PNR:0, By:Manu");
INSERT INTO transaction VALUES("249","87","4","16","Airline Receivable","0","18400","Airline Receivable: 16","2016-09-06","2016-09-06","6","2","2","0","INV#IA16, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977454/157-9164977454, PNR:0, By:Manu");
INSERT INTO transaction VALUES("250","4","87","16","Airline Receivable","18400","0","Airline Receivable: 16","2016-09-06","2016-09-06","6","2","2","0","INV#IA16, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977454/157-9164977454, PNR:0, By:Manu");
INSERT INTO transaction VALUES("251","83","5","16","Airline Payable","17930","0","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA16, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977454/157-9164977454, PNR:0, By:Manu");
INSERT INTO transaction VALUES("252","5","83","16","Airline Payable","0","17930","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA16, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977454/157-9164977454, PNR:0, By:Manu");
INSERT INTO transaction VALUES("253","87","4","17","Airline Receivable","0","400","Airline Receivable: 17","2016-09-06","2016-09-06","6","2","2","0","INV#IA17, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977450/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("254","4","87","17","Airline Receivable","400","0","Airline Receivable: 17","2016-09-06","2016-09-06","6","2","2","0","INV#IA17, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977450/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("255","83","5","17","Airline Payable","400","0","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA17, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977450/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("256","5","83","17","Airline Payable","0","400","","2016-09-06","2016-09-06","6","2","2","0","Details:INV#IA17, Pax:KUNNATHODI/ABDUL JALEEL, Passport No:G7230920, Sector:DOH/AUH, Date:08-09-2016/08-09-2016, Tkt:157-9164977450/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("257","129","6","25","Visa Receivable","0","15000","Visa Receivable : 25","2016-09-06","2016-09-06","9","1","2","0","INV#IV25,Pax :MUHAMMED ANSAB, PassNo :N1900984/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("258","6","129","25","Visa Receivable","15000","0","Visa Receivable : 25","2016-09-06","2016-09-06","9","1","2","0","INV#IV25,Pax :MUHAMMED ANSAB, PassNo :N1900984/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("259","102","7","24","Visa Payable","14700","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV25,Pax :MUHAMMED ANSAB, PassNo :N1900984/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("260","7","102","24","Visa Payable","0","14700","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV25,Pax :MUHAMMED ANSAB, PassNo :N1900984/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("261","95","6","26","Visa Receivable","0","14600","Visa Receivable : 26","2016-09-06","2016-09-06","9","1","2","0","INV#IV26,Pax :ANSAR, PassNo :P2698527/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("262","6","95","26","Visa Receivable","14600","0","Visa Receivable : 26","2016-09-06","2016-09-06","9","1","2","0","INV#IV26,Pax :ANSAR, PassNo :P2698527/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("263","94","7","25","Visa Payable","14100","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV26,Pax :ANSAR, PassNo :P2698527/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("264","7","94","25","Visa Payable","0","14100","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV26,Pax :ANSAR, PassNo :P2698527/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("265","129","6","27","Visa Receivable","0","15000","Visa Receivable : 27","2016-09-06","2016-09-06","9","1","2","0","INV#IV27,Pax :SAFNAS, PassNo :H6710271/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("266","6","129","27","Visa Receivable","15000","0","Visa Receivable : 27","2016-09-06","2016-09-06","9","1","2","0","INV#IV27,Pax :SAFNAS, PassNo :H6710271/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("267","94","7","26","Visa Payable","14100","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV27,Pax :SAFNAS, PassNo :H6710271/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("268","7","94","26","Visa Payable","0","14100","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV27,Pax :SAFNAS, PassNo :H6710271/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("269","130","6","28","Visa Receivable","0","14700","Visa Receivable : 28","2016-09-06","2016-09-06","9","1","2","0","INV#IV28,Pax :HASSAN PUTHIYATI, PassNo :G9210752/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("270","6","130","28","Visa Receivable","14700","0","Visa Receivable : 28","2016-09-06","2016-09-06","9","1","2","0","INV#IV28,Pax :HASSAN PUTHIYATI, PassNo :G9210752/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("271","94","7","27","Visa Payable","14100","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV28,Pax :HASSAN PUTHIYATI, PassNo :G9210752/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("272","7","94","27","Visa Payable","0","14100","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV28,Pax :HASSAN PUTHIYATI, PassNo :G9210752/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("273","95","6","29","Visa Receivable","0","300","Visa Receivable : 29","2016-09-06","2016-09-06","2","1","2","0","INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("274","6","95","29","Visa Receivable","300","0","Visa Receivable : 29","2016-09-06","2016-09-06","2","1","2","0","INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("275","94","7","28","Visa Payable","300","0","","2016-09-06","2016-09-06","2","1","2","0","Details:INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("276","7","94","28","Visa Payable","0","300","","2016-09-06","2016-09-06","2","1","2","0","Details:INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("277","130","6","30","Visa Receivable","0","29600","Visa Receivable : 30","2016-09-06","2016-09-06","9","1","2","0","INV#IV30,Pax :HEZZA, PassNo :N8801988/Pax :ISMATHUL HAMRA, PassNo :N8239724/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("278","6","130","30","Visa Receivable","29600","0","Visa Receivable : 30","2016-09-06","2016-09-06","9","1","2","0","INV#IV30,Pax :HEZZA, PassNo :N8801988/Pax :ISMATHUL HAMRA, PassNo :N8239724/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("279","97","7","29","Visa Payable","29600","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV30,Pax :HEZZA, PassNo :N8801988/Pax :ISMATHUL HAMRA, PassNo :N8239724/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("280","7","97","29","Visa Payable","0","29600","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV30,Pax :HEZZA, PassNo :N8801988/Pax :ISMATHUL HAMRA, PassNo :N8239724/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("281","96","6","31","Visa Receivable","0","16500","Visa Receivable : 31","2016-09-06","2016-09-06","8","1","2","0","INV#IV31,Pax :MTM570 JAMAL YASAR, PassNo :H3365841/ visaFor:30 DAYS SAUDI,By:Safuvan Staff");
INSERT INTO transaction VALUES("282","6","96","31","Visa Receivable","16500","0","Visa Receivable : 31","2016-09-06","2016-09-06","8","1","2","0","INV#IV31,Pax :MTM570 JAMAL YASAR, PassNo :H3365841/ visaFor:30 DAYS SAUDI,By:Safuvan Staff");
INSERT INTO transaction VALUES("283","125","7","30","Visa Payable","15500","0","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV31,Pax :MTM570 JAMAL YASAR, PassNo :H3365841/ visaFor:30 DAYS SAUDI,By:Safuvan Staff");
INSERT INTO transaction VALUES("284","7","125","30","Visa Payable","0","15500","","2016-09-06","2016-09-06","8","1","2","0","Details:INV#IV31,Pax :MTM570 JAMAL YASAR, PassNo :H3365841/ visaFor:30 DAYS SAUDI,By:Safuvan Staff");
INSERT INTO transaction VALUES("287","130","6","32","Visa Receivable","0","40800","Visa Receivable : 32","2016-09-06","2016-09-06","9","1","2","0","INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("288","6","130","32","Visa Receivable","40800","0","Visa Receivable : 32","2016-09-06","2016-09-06","9","1","2","0","INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("289","94","7","31","Visa Payable","40800","0","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("290","7","94","31","Visa Payable","0","40800","","2016-09-06","2016-09-06","9","1","2","0","Details:INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("291","123","2","6","Cash Receipts","3700","0","vist visa","2016-09-06","2016-09-06","15","1","2","0","VouNo#CR 6,INV#IV21 Received By :Akhil,Details:INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("292","2","123","6","Cash Receipts","0","3700","vist visa","2016-09-06","2016-09-06","15","1","2","0","VouNo#CR 6,INV#IV21 Received By :Akhil,Details:INV#IV21,Pax :MOHAMMED ISMAIL, PassNo :L715982/ visaFor:30 DAYS MALASIA,By:Safuvan Staff");
INSERT INTO transaction VALUES("293","93","6","33","Visa Receivable","0","14800","Visa Receivable : 33","2016-09-07","2016-09-07","9","1","2","0","INV#IV33,Pax :ANSIL, PassNo :L1756857/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("294","6","93","33","Visa Receivable","14800","0","Visa Receivable : 33","2016-09-07","2016-09-07","9","1","2","0","INV#IV33,Pax :ANSIL, PassNo :L1756857/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("295","94","7","32","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV33,Pax :ANSIL, PassNo :L1756857/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("296","7","94","32","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV33,Pax :ANSIL, PassNo :L1756857/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("297","95","6","34","Visa Receivable","0","14700","Visa Receivable : 34","2016-09-07","2016-09-07","9","1","2","0","INV#IV34,Pax :ZAINABA, PassNo :J8564596/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("298","6","95","34","Visa Receivable","14700","0","Visa Receivable : 34","2016-09-07","2016-09-07","9","1","2","0","INV#IV34,Pax :ZAINABA, PassNo :J8564596/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("299","102","7","33","Visa Payable","14700","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV34,Pax :ZAINABA, PassNo :J8564596/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("300","7","102","33","Visa Payable","0","14700","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV34,Pax :ZAINABA, PassNo :J8564596/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("301","93","6","35","Visa Receivable","0","16200","Visa Receivable : 35","2016-09-07","2016-09-07","9","1","2","0","INV#IV35,Pax :IBRAHIM KUTTY, PassNo :P2813905/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("302","6","93","35","Visa Receivable","16200","0","Visa Receivable : 35","2016-09-07","2016-09-07","9","1","2","0","INV#IV35,Pax :IBRAHIM KUTTY, PassNo :P2813905/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("303","97","7","34","Visa Payable","15800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV35,Pax :IBRAHIM KUTTY, PassNo :P2813905/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("304","7","97","34","Visa Payable","0","15800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV35,Pax :IBRAHIM KUTTY, PassNo :P2813905/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("305","129","6","36","Visa Receivable","0","15000","Visa Receivable : 36","2016-09-07","2016-09-07","9","1","2","0","INV#IV36,Pax :MUHAMMED, PassNo :N5120158/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("306","6","129","36","Visa Receivable","15000","0","Visa Receivable : 36","2016-09-07","2016-09-07","9","1","2","0","INV#IV36,Pax :MUHAMMED, PassNo :N5120158/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("307","94","7","35","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV36,Pax :MUHAMMED, PassNo :N5120158/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("308","7","94","35","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV36,Pax :MUHAMMED, PassNo :N5120158/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("309","101","6","37","Visa Receivable","0","5200","Visa Receivable : 37","2016-09-07","2016-09-07","9","1","2","0","INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("310","6","101","37","Visa Receivable","5200","0","Visa Receivable : 37","2016-09-07","2016-09-07","9","1","2","0","INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("311","127","7","36","Visa Payable","5150","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("312","7","127","36","Visa Payable","0","5150","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("313","95","6","38","Visa Receivable","0","29600","Visa Receivable : 38","2016-09-07","2016-09-07","9","1","2","0","INV#IV38,Pax :SAROJA, PassNo :K6272814/Pax :JAFIN, PassNo :M3827069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("314","6","95","38","Visa Receivable","29600","0","Visa Receivable : 38","2016-09-07","2016-09-07","9","1","2","0","INV#IV38,Pax :SAROJA, PassNo :K6272814/Pax :JAFIN, PassNo :M3827069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("315","132","7","37","Visa Payable","29600","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV38,Pax :SAROJA, PassNo :K6272814/Pax :JAFIN, PassNo :M3827069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("316","7","132","37","Visa Payable","0","29600","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV38,Pax :SAROJA, PassNo :K6272814/Pax :JAFIN, PassNo :M3827069/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("317","114","6","39","Visa Receivable","0","29600","Visa Receivable : 39","2016-09-07","2016-09-07","9","1","2","0","INV#IV39,Pax :CHANDRAN PULIKKATT, PassNo :J5352156/Pax :SHEEJA, PassNo :N4524757/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("318","6","114","39","Visa Receivable","29600","0","Visa Receivable : 39","2016-09-07","2016-09-07","9","1","2","0","INV#IV39,Pax :CHANDRAN PULIKKATT, PassNo :J5352156/Pax :SHEEJA, PassNo :N4524757/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("319","94","7","38","Visa Payable","28200","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV39,Pax :CHANDRAN PULIKKATT, PassNo :J5352156/Pax :SHEEJA, PassNo :N4524757/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("320","7","94","38","Visa Payable","0","28200","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV39,Pax :CHANDRAN PULIKKATT, PassNo :J5352156/Pax :SHEEJA, PassNo :N4524757/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("321","114","6","40","Visa Receivable","0","16000","Visa Receivable : 40","2016-09-07","2016-09-07","9","1","2","0","INV#IV40,Pax :NILAA NIKHIL, PassNo :P0422439/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("322","6","114","40","Visa Receivable","16000","0","Visa Receivable : 40","2016-09-07","2016-09-07","9","1","2","0","INV#IV40,Pax :NILAA NIKHIL, PassNo :P0422439/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("323","132","7","39","Visa Payable","16000","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV40,Pax :NILAA NIKHIL, PassNo :P0422439/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("324","7","132","39","Visa Payable","0","16000","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV40,Pax :NILAA NIKHIL, PassNo :P0422439/ visaFor:90 DAYS DXB SETTING,By:Sahad Staff");
INSERT INTO transaction VALUES("325","130","6","41","Visa Receivable","0","14700","Visa Receivable : 41","2016-09-07","2016-09-07","9","1","2","0","INV#IV41,Pax :JAMES CLEETUS, PassNo :N5427392/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("326","6","130","41","Visa Receivable","14700","0","Visa Receivable : 41","2016-09-07","2016-09-07","9","1","2","0","INV#IV41,Pax :JAMES CLEETUS, PassNo :N5427392/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("327","132","7","40","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV41,Pax :JAMES CLEETUS, PassNo :N5427392/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("328","7","132","40","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV41,Pax :JAMES CLEETUS, PassNo :N5427392/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("329","95","6","42","Visa Receivable","0","14700","Visa Receivable : 42","2016-09-07","2016-09-07","9","1","2","0","INV#IV42,Pax :MERLIN BABU, PassNo :M0477973/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("330","6","95","42","Visa Receivable","14700","0","Visa Receivable : 42","2016-09-07","2016-09-07","9","1","2","0","INV#IV42,Pax :MERLIN BABU, PassNo :M0477973/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("331","132","7","41","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV42,Pax :MERLIN BABU, PassNo :M0477973/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("332","7","132","41","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV42,Pax :MERLIN BABU, PassNo :M0477973/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("333","99","6","43","Visa Receivable","0","14800","Visa Receivable : 43","2016-09-07","2016-09-07","9","1","2","0","INV#IV43,Pax :ASHNA, PassNo :M6093308/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("334","6","99","43","Visa Receivable","14800","0","Visa Receivable : 43","2016-09-07","2016-09-07","9","1","2","0","INV#IV43,Pax :ASHNA, PassNo :M6093308/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("335","132","7","42","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV43,Pax :ASHNA, PassNo :M6093308/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("336","7","132","42","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV43,Pax :ASHNA, PassNo :M6093308/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("337","95","6","44","Visa Receivable","0","29400","Visa Receivable : 44","2016-09-07","2016-09-07","9","1","2","0","INV#IV44,Pax :SHAMEEM, PassNo :K3072306/Pax :SRIMATHY, PassNo :P4071810/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("338","6","95","44","Visa Receivable","29400","0","Visa Receivable : 44","2016-09-07","2016-09-07","9","1","2","0","INV#IV44,Pax :SHAMEEM, PassNo :K3072306/Pax :SRIMATHY, PassNo :P4071810/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("339","132","7","43","Visa Payable","29600","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV44,Pax :SHAMEEM, PassNo :K3072306/Pax :SRIMATHY, PassNo :P4071810/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("340","7","132","43","Visa Payable","0","29600","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV44,Pax :SHAMEEM, PassNo :K3072306/Pax :SRIMATHY, PassNo :P4071810/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("341","129","6","45","Visa Receivable","0","15100","Visa Receivable : 45","2016-09-07","2016-09-07","9","1","2","0","INV#IV45,Pax :SHEEBA, PassNo :N5159159/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("342","6","129","45","Visa Receivable","15100","0","Visa Receivable : 45","2016-09-07","2016-09-07","9","1","2","0","INV#IV45,Pax :SHEEBA, PassNo :N5159159/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("343","132","7","44","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV45,Pax :SHEEBA, PassNo :N5159159/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("344","7","132","44","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV45,Pax :SHEEBA, PassNo :N5159159/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("345","99","6","46","Visa Receivable","0","14600","Visa Receivable : 46","2016-09-07","2016-09-07","9","1","2","0","INV#IV46,Pax :JUSTIN JAMES, PassNo :L5914253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("346","6","99","46","Visa Receivable","14600","0","Visa Receivable : 46","2016-09-07","2016-09-07","9","1","2","0","INV#IV46,Pax :JUSTIN JAMES, PassNo :L5914253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("347","132","7","45","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV46,Pax :JUSTIN JAMES, PassNo :L5914253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("348","7","132","45","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV46,Pax :JUSTIN JAMES, PassNo :L5914253/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("349","95","6","47","Visa Receivable","0","189800","Visa Receivable : 47","2016-09-07","2016-09-07","9","1","2","0","INV#IV47,Pax :ASOK KUMAR, PassNo :M1674522/Pax :DILEEP, PassNo :G6067478/Pax :HASHIM, PassNo :H6275220/Pax :RAMAKRISHNAN, PassNo :G6160411/Pax :JERIN JAMES, PassNo :K5427677/Pax :SABEEH, PassNo :H3049393/Pax :FAISAL, PassNo :K3033987/Pax :UMMER, PassNo :M4950208/Pax :HAFIZ, PassNo :J2006760/Pax :KUNJHU MON, PassNo :M7807535/Pax :MADAVADAS, PassNo :J5254307/Pax :VENUGOPALAN, PassNo :H5709904/Pax :SAROJINI, PassNo :H5708579/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("350","6","95","47","Visa Receivable","189800","0","Visa Receivable : 47","2016-09-07","2016-09-07","9","1","2","0","INV#IV47,Pax :ASOK KUMAR, PassNo :M1674522/Pax :DILEEP, PassNo :G6067478/Pax :HASHIM, PassNo :H6275220/Pax :RAMAKRISHNAN, PassNo :G6160411/Pax :JERIN JAMES, PassNo :K5427677/Pax :SABEEH, PassNo :H3049393/Pax :FAISAL, PassNo :K3033987/Pax :UMMER, PassNo :M4950208/Pax :HAFIZ, PassNo :J2006760/Pax :KUNJHU MON, PassNo :M7807535/Pax :MADAVADAS, PassNo :J5254307/Pax :VENUGOPALAN, PassNo :H5709904/Pax :SAROJINI, PassNo :H5708579/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("351","94","7","46","Visa Payable","183300","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV47,Pax :ASOK KUMAR, PassNo :M1674522/Pax :DILEEP, PassNo :G6067478/Pax :HASHIM, PassNo :H6275220/Pax :RAMAKRISHNAN, PassNo :G6160411/Pax :JERIN JAMES, PassNo :K5427677/Pax :SABEEH, PassNo :H3049393/Pax :FAISAL, PassNo :K3033987/Pax :UMMER, PassNo :M4950208/Pax :HAFIZ, PassNo :J2006760/Pax :KUNJHU MON, PassNo :M7807535/Pax :MADAVADAS, PassNo :J5254307/Pax :VENUGOPALAN, PassNo :H5709904/Pax :SAROJINI, PassNo :H5708579/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("352","7","94","46","Visa Payable","0","183300","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV47,Pax :ASOK KUMAR, PassNo :M1674522/Pax :DILEEP, PassNo :G6067478/Pax :HASHIM, PassNo :H6275220/Pax :RAMAKRISHNAN, PassNo :G6160411/Pax :JERIN JAMES, PassNo :K5427677/Pax :SABEEH, PassNo :H3049393/Pax :FAISAL, PassNo :K3033987/Pax :UMMER, PassNo :M4950208/Pax :HAFIZ, PassNo :J2006760/Pax :KUNJHU MON, PassNo :M7807535/Pax :MADAVADAS, PassNo :J5254307/Pax :VENUGOPALAN, PassNo :H5709904/Pax :SAROJINI, PassNo :H5708579/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("353","99","6","48","Visa Receivable","0","29200","Visa Receivable : 48","2016-09-07","2016-09-07","9","1","2","0","INV#IV48,Pax :VARGHESE, PassNo :M832608/Pax :SUDHEESH, PassNo :N6950516/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("354","6","99","48","Visa Receivable","29200","0","Visa Receivable : 48","2016-09-07","2016-09-07","9","1","2","0","INV#IV48,Pax :VARGHESE, PassNo :M832608/Pax :SUDHEESH, PassNo :N6950516/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("355","94","7","47","Visa Payable","28200","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV48,Pax :VARGHESE, PassNo :M832608/Pax :SUDHEESH, PassNo :N6950516/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("356","7","94","47","Visa Payable","0","28200","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV48,Pax :VARGHESE, PassNo :M832608/Pax :SUDHEESH, PassNo :N6950516/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("357","136","6","49","Visa Receivable","0","46200","Visa Receivable : 49","2016-09-07","2016-09-07","9","1","2","0","INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("358","6","136","49","Visa Receivable","46200","0","Visa Receivable : 49","2016-09-07","2016-09-07","9","1","2","0","INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("359","132","7","48","Visa Payable","44400","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("360","7","132","48","Visa Payable","0","44400","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("361","95","6","50","Visa Receivable","0","29200","Visa Receivable : 50","2016-09-07","2016-09-07","9","1","2","0","INV#IV50,Pax :ARIF, PassNo :M9418291/Pax :BONEY JOSEPH, PassNo :G8586116/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("362","6","95","50","Visa Receivable","29200","0","Visa Receivable : 50","2016-09-07","2016-09-07","9","1","2","0","INV#IV50,Pax :ARIF, PassNo :M9418291/Pax :BONEY JOSEPH, PassNo :G8586116/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("363","94","7","49","Visa Payable","28200","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV50,Pax :ARIF, PassNo :M9418291/Pax :BONEY JOSEPH, PassNo :G8586116/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("364","7","94","49","Visa Payable","0","28200","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV50,Pax :ARIF, PassNo :M9418291/Pax :BONEY JOSEPH, PassNo :G8586116/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("365","95","6","51","Visa Receivable","0","29400","Visa Receivable : 51","2016-09-07","2016-09-07","9","1","2","0","INV#IV51,Pax :SALIHA, PassNo :M0174249/Pax :NIMMY, PassNo :L2844381/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("366","6","95","51","Visa Receivable","29400","0","Visa Receivable : 51","2016-09-07","2016-09-07","9","1","2","0","INV#IV51,Pax :SALIHA, PassNo :M0174249/Pax :NIMMY, PassNo :L2844381/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("367","132","7","50","Visa Payable","29600","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV51,Pax :SALIHA, PassNo :M0174249/Pax :NIMMY, PassNo :L2844381/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("368","7","132","50","Visa Payable","0","29600","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV51,Pax :SALIHA, PassNo :M0174249/Pax :NIMMY, PassNo :L2844381/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("369","81","2","1","Cash Payments","0","15730","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP1,INV#PA1,PA3 Paid By :Manu To : MTM  CCJ,Details:INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("370","2","81","1","Cash Payments","15730","0","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP1,INV#PA1,PA3 Paid By :Manu To : MTM  CCJ,Details:INV#IA3, Pax:MACHINCHERITHOOMBIL/MOHAMMED SAKEEB, Passport No:M8018076, Sector:DXB/CCJ, Date:05-09-2016, Tkt:AEDKL6, PNR:AEDKL6, By:Manu");
INSERT INTO transaction VALUES("371","129","6","52","Visa Receivable","0","15000","Visa Receivable : 52","2016-09-07","2016-09-07","9","1","2","0","INV#IV52,Pax :SARATH KRISHNA, PassNo :K6243609/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("372","6","129","52","Visa Receivable","15000","0","Visa Receivable : 52","2016-09-07","2016-09-07","9","1","2","0","INV#IV52,Pax :SARATH KRISHNA, PassNo :K6243609/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("373","94","7","51","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV52,Pax :SARATH KRISHNA, PassNo :K6243609/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("374","7","94","51","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV52,Pax :SARATH KRISHNA, PassNo :K6243609/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("375","101","6","53","Visa Receivable","0","14800","Visa Receivable : 53","2016-09-07","2016-09-07","9","1","2","0","INV#IV53,Pax :HABEEBA SHAFEEEK, PassNo :P2642443/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("376","6","101","53","Visa Receivable","14800","0","Visa Receivable : 53","2016-09-07","2016-09-07","9","1","2","0","INV#IV53,Pax :HABEEBA SHAFEEEK, PassNo :P2642443/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("377","132","7","52","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV53,Pax :HABEEBA SHAFEEEK, PassNo :P2642443/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("378","7","132","52","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV53,Pax :HABEEBA SHAFEEEK, PassNo :P2642443/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("379","101","6","54","Visa Receivable","0","29600","Visa Receivable : 54","2016-09-07","2016-09-07","9","1","2","0","INV#IV54,Pax :HASNA, PassNo :P1163918/Pax :ZAHIDA, PassNo :M9909492/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("380","6","101","54","Visa Receivable","29600","0","Visa Receivable : 54","2016-09-07","2016-09-07","9","1","2","0","INV#IV54,Pax :HASNA, PassNo :P1163918/Pax :ZAHIDA, PassNo :M9909492/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("381","132","7","53","Visa Payable","29600","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV54,Pax :HASNA, PassNo :P1163918/Pax :ZAHIDA, PassNo :M9909492/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("382","7","132","53","Visa Payable","0","29600","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV54,Pax :HASNA, PassNo :P1163918/Pax :ZAHIDA, PassNo :M9909492/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("385","99","6","55","Visa Receivable","0","14600","Visa Receivable : 55","2016-09-07","2016-09-07","9","1","2","0","INV#IV55,Pax :DAISAN DAVID, PassNo :N1142416/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("386","6","99","55","Visa Receivable","14600","0","Visa Receivable : 55","2016-09-07","2016-09-07","9","1","2","0","INV#IV55,Pax :DAISAN DAVID, PassNo :N1142416/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("387","94","7","54","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV55,Pax :DAISAN DAVID, PassNo :N1142416/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("388","7","94","54","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV55,Pax :DAISAN DAVID, PassNo :N1142416/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("389","95","6","56","Visa Receivable","0","29200","Visa Receivable : 56","2016-09-07","2016-09-07","9","1","2","0","INV#IV56,Pax :SIJO JOSEPH, PassNo :N8591047/Pax :AJAY KRISHNA, PassNo :G9194546/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("390","6","95","56","Visa Receivable","29200","0","Visa Receivable : 56","2016-09-07","2016-09-07","9","1","2","0","INV#IV56,Pax :SIJO JOSEPH, PassNo :N8591047/Pax :AJAY KRISHNA, PassNo :G9194546/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("391","94","7","55","Visa Payable","28200","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV56,Pax :SIJO JOSEPH, PassNo :N8591047/Pax :AJAY KRISHNA, PassNo :G9194546/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("392","7","94","55","Visa Payable","0","28200","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV56,Pax :SIJO JOSEPH, PassNo :N8591047/Pax :AJAY KRISHNA, PassNo :G9194546/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("393","130","6","57","Visa Receivable","0","5100","Visa Receivable : 57","2016-09-07","2016-09-07","9","1","2","0","INV#IV57,Pax :ABDUL AZEEZ, PassNo :L8040492/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("394","6","130","57","Visa Receivable","5100","0","Visa Receivable : 57","2016-09-07","2016-09-07","9","1","2","0","INV#IV57,Pax :ABDUL AZEEZ, PassNo :L8040492/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("395","94","7","56","Visa Payable","5100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV57,Pax :ABDUL AZEEZ, PassNo :L8040492/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("396","7","94","56","Visa Payable","0","5100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV57,Pax :ABDUL AZEEZ, PassNo :L8040492/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("397","93","6","58","Visa Receivable","0","45300","Visa Receivable : 58","2016-09-07","2016-09-07","9","1","2","0","INV#IV58,Pax :NAFSIYA KOLAMBIL, PassNo :G4454840/Pax :RSHAN AHAMMED, PassNo :P0352855/Pax :TANIA ISRA, PassNo :K5269033/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("398","6","93","58","Visa Receivable","45300","0","Visa Receivable : 58","2016-09-07","2016-09-07","9","1","2","0","INV#IV58,Pax :NAFSIYA KOLAMBIL, PassNo :G4454840/Pax :RSHAN AHAMMED, PassNo :P0352855/Pax :TANIA ISRA, PassNo :K5269033/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("399","132","7","57","Visa Payable","44400","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV58,Pax :NAFSIYA KOLAMBIL, PassNo :G4454840/Pax :RSHAN AHAMMED, PassNo :P0352855/Pax :TANIA ISRA, PassNo :K5269033/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("400","7","132","57","Visa Payable","0","44400","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV58,Pax :NAFSIYA KOLAMBIL, PassNo :G4454840/Pax :RSHAN AHAMMED, PassNo :P0352855/Pax :TANIA ISRA, PassNo :K5269033/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("401","83","2","2","Cash Payments","0","138980","160810 - 21830 = 138980","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP2,INV#PA2,PA4,PA5,PA6,PA7,PA8,PA9,PA10 Paid By :Manu To : Creative Tours & Travels - KODUNGALLUR,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("402","2","83","2","Cash Payments","138980","0","160810 - 21830 = 138980","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP2,INV#PA2,PA4,PA5,PA6,PA7,PA8,PA9,PA10 Paid By :Manu To : Creative Tours & Travels - KODUNGALLUR,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("407","138","6","60","Visa Receivable","0","600","Visa Receivable : 60","2016-09-07","2016-09-07","10","1","2","0","INV#IV60,Pax :ABDU RAHIMAN , PassNo :k000000/Pax :SREEJITH, PassNo :K8893537/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("408","6","138","60","Visa Receivable","600","0","Visa Receivable : 60","2016-09-07","2016-09-07","10","1","2","0","INV#IV60,Pax :ABDU RAHIMAN , PassNo :k000000/Pax :SREEJITH, PassNo :K8893537/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("409","125","7","59","Visa Payable","600","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV60,Pax :ABDU RAHIMAN , PassNo :k000000/Pax :SREEJITH, PassNo :K8893537/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("410","7","125","59","Visa Payable","0","600","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV60,Pax :ABDU RAHIMAN , PassNo :k000000/Pax :SREEJITH, PassNo :K8893537/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("411","138","6","61","Visa Receivable","0","14600","Visa Receivable : 61","2016-09-07","2016-09-07","10","1","2","0","INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("412","6","138","61","Visa Receivable","14600","0","Visa Receivable : 61","2016-09-07","2016-09-07","10","1","2","0","INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("413","125","7","60","Visa Payable","14600","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("414","7","125","60","Visa Payable","0","14600","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("415","138","2","7","Cash Receipts","15000","0","COUNTER 16","2016-09-07","2016-09-07","15","1","2","0","VouNo#CR 7,INV#IV59,IV61 Received By :Akhil,Details:INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("416","2","138","7","Cash Receipts","0","15000","COUNTER 16","2016-09-07","2016-09-07","15","1","2","0","VouNo#CR 7,INV#IV59,IV61 Received By :Akhil,Details:INV#IV61,Pax :JAMES , PassNo :J4059812/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("417","87","2","4","Cash Receipts","107650","0","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 4,INV#IA2,IA9,IA10 Received By :Manu,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("418","2","87","4","Cash Receipts","0","107650","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 4,INV#IA2,IA9,IA10 Received By :Manu,Details:INV#IA10, Pax:Alqahtani/albandari Faisal/ALQAHTANI/NORAH FAISAL , Passport No:, Sector:BOM/RUH, Date:04-09-2016/04-09-2016/04-09-2016/04-09-2016, Tkt:098-9164977343/098-9164977343/098-9164977344/098-9164977344, PNR:3OYEHF, By:Manu");
INSERT INTO transaction VALUES("421","138","6","59","Visa Receivable","0","400","Visa Receivable : 59","2016-09-07","2016-09-07","10","1","2","0","INV#IV59,Pax :NAHIM, PassNo :K2828090/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad Staff");
INSERT INTO transaction VALUES("422","6","138","59","Visa Receivable","400","0","Visa Receivable : 59","2016-09-07","2016-09-07","10","1","2","0","INV#IV59,Pax :NAHIM, PassNo :K2828090/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad Staff");
INSERT INTO transaction VALUES("423","125","7","58","Visa Payable","400","0","","2016-09-07","2016-09-07","10","1","2","0","INV#58,sup:125,:against:59,Details:INV#IV59,Pax :NAHIM, PassNo :K2828090/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad Staff");
INSERT INTO transaction VALUES("424","7","125","58","Visa Payable","0","400","","2016-09-07","2016-09-07","10","1","2","0","INV#58,sup:125,:against:59,Details:INV#IV59,Pax :NAHIM, PassNo :K2828090/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad Staff");
INSERT INTO transaction VALUES("425","82","2","3","Cash Payments","0","15000","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP3,INV#PV1 Paid By :Manu To : MTM VISA,Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("426","2","82","3","Cash Payments","15000","0","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CP3,INV#PV1 Paid By :Manu To : MTM VISA,Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("427","88","2","6","Cash Receipts","10000","0","6500 balance","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 6,INV#IV1 Received By :Manu,Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("428","2","88","6","Cash Receipts","0","10000","6500 balance","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 6,INV#IV1 Received By :Manu,Details:INV#IV1,Pax :MAIMOONA MANJAPILLI AHAMMAD , PassNo :N1595786/ visaFor:UAE 90 Days,By:Manu");
INSERT INTO transaction VALUES("429","86","2","7","Cash Receipts","1000","0","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 7,INV#IA1 Received By :Manu,Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("430","2","86","7","Cash Receipts","0","1000","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 7,INV#IA1 Received By :Manu,Details:INV#IA1, Pax:ITTIKAPARAMBIL PALAKKAL, ABOOBACKAR, Passport No:G1284698, Sector:AUH/CCJ, Date:16-09-2016, Tkt:NHI1RD, PNR:NHI1RD, By:Manu");
INSERT INTO transaction VALUES("431","128","2","5","Cash Receipts","15000","0","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 5,INV#IA14 Received By :Manu,Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("432","2","128","5","Cash Receipts","0","15000","","2016-09-07","2016-09-07","6","2","2","0","VouNo#CR 5,INV#IA14 Received By :Manu,Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("433","98","4","11","Airline Receivable","0","24800","Airline Receivable: 11","2016-09-07","2016-09-05","6","2","2","0","INV#IA11, Pax:Ainikkunnan Jayafer, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("434","4","98","11","Airline Receivable","24800","0","Airline Receivable: 11","2016-09-07","2016-09-05","6","2","2","0","INV#IA11, Pax:Ainikkunnan Jayafer, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("435","81","5","11","Airline Payable","24520","0","","2016-09-07","2016-09-05","6","2","2","0","INV#11,sup:81,:against:11,Details:INV#IA11, Pax:Ainikkunnan Jayafer, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("436","5","81","11","Airline Payable","0","24520","","2016-09-07","2016-09-05","6","2","2","0","INV#11,sup:81,:against:11,Details:INV#IA11, Pax:Ainikkunnan Jayafer, Passport No:M7297991, Sector:CCJ/DOH, Date:11-09-2016, Tkt:M7AS4L, PNR:M7AS4L, By:Manu");
INSERT INTO transaction VALUES("437","136","134","1","Bank Receipts","46123","0","DEPOSITED BY MUHAMMED KOTTATHAYIL  IN SOUTH INDIAN","2016-09-07","2016-09-07","15","1","2","0","VouNo#BR1,INV#IV49 Received By :Akhil,Details:INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("438","134","136","1","Bank Receipts","0","46123","DEPOSITED BY MUHAMMED KOTTATHAYIL  IN SOUTH INDIAN","2016-09-07","2016-09-07","15","1","2","0","VouNo#BR1,INV#IV49 Received By :Akhil,Details:INV#IV49,Pax :RAJA RAM, PassNo :H7437321/Pax :VINOD, PassNo :N9184559/Pax :MANOJ KUMAR, PassNo :K5356456/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("439","95","6","62","Visa Receivable","0","14700","Visa Receivable : 62","2016-09-07","2016-09-07","10","1","2","0","INV#IV62,Pax :ANSAR, PassNo :M5169954/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("440","6","95","62","Visa Receivable","14700","0","Visa Receivable : 62","2016-09-07","2016-09-07","10","1","2","0","INV#IV62,Pax :ANSAR, PassNo :M5169954/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("441","125","7","61","Visa Payable","14200","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV62,Pax :ANSAR, PassNo :M5169954/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("442","7","125","61","Visa Payable","0","14200","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV62,Pax :ANSAR, PassNo :M5169954/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("443","95","6","63","Visa Receivable","0","6900","Visa Receivable : 63","2016-09-07","2016-09-07","10","1","2","0","INV#IV63,Pax :ANILA, PassNo :N7681273/Pax :PABRI CK, PassNo :N7686960/Pax :PATRICIA ANJELIN, PassNo :P2488318/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("444","6","95","63","Visa Receivable","6900","0","Visa Receivable : 63","2016-09-07","2016-09-07","10","1","2","0","INV#IV63,Pax :ANILA, PassNo :N7681273/Pax :PABRI CK, PassNo :N7686960/Pax :PATRICIA ANJELIN, PassNo :P2488318/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("445","125","7","62","Visa Payable","6900","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV63,Pax :ANILA, PassNo :N7681273/Pax :PABRI CK, PassNo :N7686960/Pax :PATRICIA ANJELIN, PassNo :P2488318/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("446","7","125","62","Visa Payable","0","6900","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV63,Pax :ANILA, PassNo :N7681273/Pax :PABRI CK, PassNo :N7686960/Pax :PATRICIA ANJELIN, PassNo :P2488318/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("447","95","6","64","Visa Receivable","0","4600","Visa Receivable : 64","2016-09-07","2016-09-07","10","1","2","0","INV#IV64,Pax :REMA KUMARI, PassNo :N9101342/Pax :KAVITHA, PassNo :K6437126/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("448","6","95","64","Visa Receivable","4600","0","Visa Receivable : 64","2016-09-07","2016-09-07","10","1","2","0","INV#IV64,Pax :REMA KUMARI, PassNo :N9101342/Pax :KAVITHA, PassNo :K6437126/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("449","125","7","63","Visa Payable","4600","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV64,Pax :REMA KUMARI, PassNo :N9101342/Pax :KAVITHA, PassNo :K6437126/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("450","7","125","63","Visa Payable","0","4600","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV64,Pax :REMA KUMARI, PassNo :N9101342/Pax :KAVITHA, PassNo :K6437126/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("451","95","6","65","Visa Receivable","0","2600","Visa Receivable : 65","2016-09-07","2016-09-07","10","1","2","0","INV#IV65,Pax :RAMANATHAN, PassNo :M6237420/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("452","6","95","65","Visa Receivable","2600","0","Visa Receivable : 65","2016-09-07","2016-09-07","10","1","2","0","INV#IV65,Pax :RAMANATHAN, PassNo :M6237420/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("453","125","7","64","Visa Payable","2600","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV65,Pax :RAMANATHAN, PassNo :M6237420/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("454","7","125","64","Visa Payable","0","2600","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV65,Pax :RAMANATHAN, PassNo :M6237420/ visaFor:90 DYAS DXB LESSUER,By:Irfad Staff");
INSERT INTO transaction VALUES("455","95","6","66","Visa Receivable","0","14900","Visa Receivable : 66","2016-09-07","2016-09-07","10","1","2","0","INV#IV66,Pax :Fathima Hanna, PassNo :L7896144/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("456","6","95","66","Visa Receivable","14900","0","Visa Receivable : 66","2016-09-07","2016-09-07","10","1","2","0","INV#IV66,Pax :Fathima Hanna, PassNo :L7896144/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("457","125","7","65","Visa Payable","14900","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV66,Pax :Fathima Hanna, PassNo :L7896144/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("458","7","125","65","Visa Payable","0","14900","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV66,Pax :Fathima Hanna, PassNo :L7896144/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("459","96","6","67","Visa Receivable","0","15100","Visa Receivable : 67","2016-09-07","2016-09-07","9","1","2","0","INV#IV67,Pax :RICHARD, PassNo :H4603974/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("460","6","96","67","Visa Receivable","15100","0","Visa Receivable : 67","2016-09-07","2016-09-07","9","1","2","0","INV#IV67,Pax :RICHARD, PassNo :H4603974/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("461","94","7","66","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV67,Pax :RICHARD, PassNo :H4603974/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("462","7","94","66","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV67,Pax :RICHARD, PassNo :H4603974/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("463","95","6","68","Visa Receivable","0","14600","Visa Receivable : 68","2016-09-07","2016-09-07","9","1","2","0","INV#IV68,Pax :THAJUDHEEN, PassNo :K1076374/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("464","6","95","68","Visa Receivable","14600","0","Visa Receivable : 68","2016-09-07","2016-09-07","9","1","2","0","INV#IV68,Pax :THAJUDHEEN, PassNo :K1076374/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("465","94","7","67","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV68,Pax :THAJUDHEEN, PassNo :K1076374/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("466","7","94","67","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV68,Pax :THAJUDHEEN, PassNo :K1076374/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("467","129","6","69","Visa Receivable","0","15000","Visa Receivable : 69","2016-09-07","2016-09-07","9","1","2","0","INV#IV69,Pax :SUSHANTH, PassNo :K5230981/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("468","6","129","69","Visa Receivable","15000","0","Visa Receivable : 69","2016-09-07","2016-09-07","9","1","2","0","INV#IV69,Pax :SUSHANTH, PassNo :K5230981/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("469","94","7","68","Visa Payable","14100","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV69,Pax :SUSHANTH, PassNo :K5230981/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("470","7","94","68","Visa Payable","0","14100","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV69,Pax :SUSHANTH, PassNo :K5230981/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("471","136","6","70","Visa Receivable","0","15400","Visa Receivable : 70","2016-09-07","2016-09-07","9","1","2","0","INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("472","6","136","70","Visa Receivable","15400","0","Visa Receivable : 70","2016-09-07","2016-09-07","9","1","2","0","INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("473","132","7","69","Visa Payable","14800","0","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("474","7","132","69","Visa Payable","0","14800","","2016-09-07","2016-09-07","9","1","2","0","Details:INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("475","139","6","2","Visa Receivable","0","5800","Visa Receivable : 2","2016-09-07","2016-09-07","6","2","2","0","INV#IV2,Pax :AYISHA KUNNATH, PassNo :P0915315/ visaFor:UAE  30 Days,By:Manu");
INSERT INTO transaction VALUES("476","6","139","2","Visa Receivable","5800","0","Visa Receivable : 2","2016-09-07","2016-09-07","6","2","2","0","INV#IV2,Pax :AYISHA KUNNATH, PassNo :P0915315/ visaFor:UAE  30 Days,By:Manu");
INSERT INTO transaction VALUES("477","82","7","2","Visa Payable","5500","0","","2016-09-07","2016-09-07","6","2","2","0","Details:INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("478","7","82","2","Visa Payable","0","5500","","2016-09-07","2016-09-07","6","2","2","0","Details:INV#IV2,Pax :VISAKH, PassNo :N1140524/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("479","95","6","71","Visa Receivable","0","7700","Visa Receivable : 71","2016-09-07","2016-09-07","10","1","2","0","INV#IV71,Pax :SABEENA 1118, PassNo :	J5242293/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("480","6","95","71","Visa Receivable","7700","0","Visa Receivable : 71","2016-09-07","2016-09-07","10","1","2","0","INV#IV71,Pax :SABEENA 1118, PassNo :	J5242293/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("481","125","7","70","Visa Payable","7700","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV71,Pax :SABEENA 1118, PassNo :	J5242293/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("482","7","125","70","Visa Payable","0","7700","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV71,Pax :SABEENA 1118, PassNo :	J5242293/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("489","95","6","74","Visa Receivable","0","14900","Visa Receivable : 74","2016-09-07","2016-09-07","10","1","2","0","INV#IV74,Pax :KAVERI, PassNo :K7027177/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("490","6","95","74","Visa Receivable","14900","0","Visa Receivable : 74","2016-09-07","2016-09-07","10","1","2","0","INV#IV74,Pax :KAVERI, PassNo :K7027177/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("491","125","7","72","Visa Payable","14900","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV74,Pax :KAVERI, PassNo :K7027177/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("492","7","125","72","Visa Payable","0","14900","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV74,Pax :KAVERI, PassNo :K7027177/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("493","95","6","75","Visa Receivable","0","900","Visa Receivable : 75","2016-09-07","2016-09-07","10","1","2","0","INV#IV75,Pax :AKHILA 12200, PassNo :k000000/Pax :ISHAK 1224, PassNo :K00000/Pax :SREESHMA 1226, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("494","6","95","75","Visa Receivable","900","0","Visa Receivable : 75","2016-09-07","2016-09-07","10","1","2","0","INV#IV75,Pax :AKHILA 12200, PassNo :k000000/Pax :ISHAK 1224, PassNo :K00000/Pax :SREESHMA 1226, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("495","125","7","73","Visa Payable","900","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV75,Pax :AKHILA 12200, PassNo :k000000/Pax :ISHAK 1224, PassNo :K00000/Pax :SREESHMA 1226, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("496","7","125","73","Visa Payable","0","900","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV75,Pax :AKHILA 12200, PassNo :k000000/Pax :ISHAK 1224, PassNo :K00000/Pax :SREESHMA 1226, PassNo :K00000/ visaFor:OTB NORMAL,By:Irfad Staff");
INSERT INTO transaction VALUES("497","95","6","76","Visa Receivable","0","58400","Visa Receivable : 76","2016-09-07","2016-09-07","10","1","2","0","INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("498","6","95","76","Visa Receivable","58400","0","Visa Receivable : 76","2016-09-07","2016-09-07","10","1","2","0","INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("499","125","7","74","Visa Payable","58400","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("500","7","125","74","Visa Payable","0","58400","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("501","95","6","77","Visa Receivable","0","88200","Visa Receivable : 77","2016-09-07","2016-09-07","10","1","2","0","INV#IV77,Pax :NOORUNNISA 1234, PassNo :M7276307/Pax :MUHAMMED RASHID 1234, PassNo :P3999764/Pax :ASHA MATHARI 1240, PassNo :J5238983/Pax :GEETHA 1250, PassNo :N5577038/Pax :1262	ANU MOLE, PassNo :N7798897/Pax :1264	VIJITHA, PassNo :	P0428557/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("502","6","95","77","Visa Receivable","88200","0","Visa Receivable : 77","2016-09-07","2016-09-07","10","1","2","0","INV#IV77,Pax :NOORUNNISA 1234, PassNo :M7276307/Pax :MUHAMMED RASHID 1234, PassNo :P3999764/Pax :ASHA MATHARI 1240, PassNo :J5238983/Pax :GEETHA 1250, PassNo :N5577038/Pax :1262	ANU MOLE, PassNo :N7798897/Pax :1264	VIJITHA, PassNo :	P0428557/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("503","125","7","75","Visa Payable","88200","0","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV77,Pax :NOORUNNISA 1234, PassNo :M7276307/Pax :MUHAMMED RASHID 1234, PassNo :P3999764/Pax :ASHA MATHARI 1240, PassNo :J5238983/Pax :GEETHA 1250, PassNo :N5577038/Pax :1262	ANU MOLE, PassNo :N7798897/Pax :1264	VIJITHA, PassNo :	P0428557/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("504","7","125","75","Visa Payable","0","88200","","2016-09-07","2016-09-07","10","1","2","0","Details:INV#IV77,Pax :NOORUNNISA 1234, PassNo :M7276307/Pax :MUHAMMED RASHID 1234, PassNo :P3999764/Pax :ASHA MATHARI 1240, PassNo :J5238983/Pax :GEETHA 1250, PassNo :N5577038/Pax :1262	ANU MOLE, PassNo :N7798897/Pax :1264	VIJITHA, PassNo :	P0428557/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("505","95","133","2","Bank Receipts","137700","0","COUNTER 16 BALANCE AMT","2016-09-07","2016-09-05","15","1","2","0","VouNo#BR2,INV#IV62,IV63,IV64,IV65,IV66,IV71,IV72,IV74,IV75,IV76 Received By :Akhil,Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("506","133","95","2","Bank Receipts","0","137700","COUNTER 16 BALANCE AMT","2016-09-07","2016-09-05","15","1","2","0","VouNo#BR2,INV#IV62,IV63,IV64,IV65,IV66,IV71,IV72,IV74,IV75,IV76 Received By :Akhil,Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("507","101","6","78","Visa Receivable","0","10000","Visa Receivable : 78","2016-09-08","2016-09-08","9","1","2","0","INV#IV78,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:10 DAYS OMAN,By:Sahad Staff");
INSERT INTO transaction VALUES("508","6","101","78","Visa Receivable","10000","0","Visa Receivable : 78","2016-09-08","2016-09-08","9","1","2","0","INV#IV78,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:10 DAYS OMAN,By:Sahad Staff");
INSERT INTO transaction VALUES("509","97","7","76","Visa Payable","8000","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV78,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:10 DAYS OMAN,By:Sahad Staff");
INSERT INTO transaction VALUES("510","7","97","76","Visa Payable","0","8000","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV78,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:10 DAYS OMAN,By:Sahad Staff");
INSERT INTO transaction VALUES("511","101","6","79","Visa Receivable","0","5800","Visa Receivable : 79","2016-09-08","2016-09-08","9","1","2","0","INV#IV79,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:30 DAYS QATAR TOURIST,By:Sahad Staff");
INSERT INTO transaction VALUES("512","6","101","79","Visa Receivable","5800","0","Visa Receivable : 79","2016-09-08","2016-09-08","9","1","2","0","INV#IV79,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:30 DAYS QATAR TOURIST,By:Sahad Staff");
INSERT INTO transaction VALUES("513","140","7","77","Visa Payable","5500","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV79,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:30 DAYS QATAR TOURIST,By:Sahad Staff");
INSERT INTO transaction VALUES("514","7","140","77","Visa Payable","0","5500","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV79,Pax :VIKAS VINOD, PassNo :J3924121/ visaFor:30 DAYS QATAR TOURIST,By:Sahad Staff");
INSERT INTO transaction VALUES("515","128","2","8","Cash Receipts","7500","0","","2016-09-08","2016-09-08","6","2","2","0","VouNo#CR 8,INV#IA14 Received By :Manu,Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("516","2","128","8","Cash Receipts","0","7500","","2016-09-08","2016-09-08","6","2","2","0","VouNo#CR 8,INV#IA14 Received By :Manu,Details:INV#IA14, Pax:SIDHEEK POOKAYIL, Passport No:J2072600, Sector:SHJ/CCJ, Date:07-09-2016, Tkt:098-9164977437, PNR:0, By:Manu");
INSERT INTO transaction VALUES("517","141","6","80","Visa Receivable","0","6600","Visa Receivable : 80","2016-09-08","2016-09-08","9","1","2","0","INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("518","6","141","80","Visa Receivable","6600","0","Visa Receivable : 80","2016-09-08","2016-09-08","9","1","2","0","INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("519","140","7","78","Visa Payable","6500","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("520","7","140","78","Visa Payable","0","6500","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("521","142","6","81","Visa Receivable","0","15000","Visa Receivable : 81","2016-09-08","2016-09-08","9","1","2","0","INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("522","6","142","81","Visa Receivable","15000","0","Visa Receivable : 81","2016-09-08","2016-09-08","9","1","2","0","INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("523","94","7","79","Visa Payable","14100","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("524","7","94","79","Visa Payable","0","14100","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("533","143","2","8","Cash Receipts","1700","0","PASSPORT SERVICE","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 8,INV#IM1 Received By :Akhil,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("534","2","143","8","Cash Receipts","0","1700","PASSPORT SERVICE","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 8,INV#IM1 Received By :Akhil,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("535","144","6","82","Visa Receivable","0","16500","Visa Receivable : 82","2016-09-08","2016-09-08","10","1","2","0","INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("536","6","144","82","Visa Receivable","16500","0","Visa Receivable : 82","2016-09-08","2016-09-08","10","1","2","0","INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("537","16","144","82","Visa Receivable","0","500","Visa Receivable : 82","2016-09-08","2016-09-08","10","1","2","0","INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("538","144","16","82","Visa Receivable","500","0","Visa Receivable : 82","2016-09-08","2016-09-08","10","1","2","0","INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("539","125","7","80","Visa Payable","16500","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("540","7","125","80","Visa Payable","0","16500","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("541","144","2","9","Cash Receipts","16000","0","OMAN VISA","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 9,INV#IV82 Received By :Akhil,Details:INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("542","2","144","9","Cash Receipts","0","16000","OMAN VISA","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 9,INV#IV82 Received By :Akhil,Details:INV#IV82,Pax :MTM564 AHMED ORMUNDAKKAL, PassNo :J4887754/ visaFor:30 DAYS OMAN,By:Irfad Staff");
INSERT INTO transaction VALUES("545","130","6","83","Visa Receivable","0","5100","Visa Receivable : 83","2016-09-08","2016-09-08","9","1","2","0","INV#IV83,Pax :RAJAN KOCHU, PassNo :K3751349/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("546","6","130","83","Visa Receivable","5100","0","Visa Receivable : 83","2016-09-08","2016-09-08","9","1","2","0","INV#IV83,Pax :RAJAN KOCHU, PassNo :K3751349/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("547","94","7","81","Visa Payable","5100","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV83,Pax :RAJAN KOCHU, PassNo :K3751349/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("548","7","94","81","Visa Payable","0","5100","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV83,Pax :RAJAN KOCHU, PassNo :K3751349/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("549","142","133","3","Bank Receipts","15000","0","","2016-09-08","2016-09-08","15","1","2","0","VouNo#BR3,INV#IV81 Received By :Akhil,Details:INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("550","133","142","3","Bank Receipts","0","15000","","2016-09-08","2016-09-08","15","1","2","0","VouNo#BR3,INV#IV81 Received By :Akhil,Details:INV#IV81,Pax :MUHAMMED SHAMMAS, PassNo :L9931582/ visaFor:90 DAYS DXB  WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("551","130","6","84","Visa Receivable","0","14700","Visa Receivable : 84","2016-09-08","2016-09-08","9","1","2","0","INV#IV84,Pax :MATHUKUTTY, PassNo :L1747043/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("552","6","130","84","Visa Receivable","14700","0","Visa Receivable : 84","2016-09-08","2016-09-08","9","1","2","0","INV#IV84,Pax :MATHUKUTTY, PassNo :L1747043/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("553","94","7","82","Visa Payable","14100","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV84,Pax :MATHUKUTTY, PassNo :L1747043/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("554","7","94","82","Visa Payable","0","14100","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV84,Pax :MATHUKUTTY, PassNo :L1747043/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("555","103","6","85","Visa Receivable","0","5500","Visa Receivable : 85","2016-09-08","2016-09-08","9","1","2","0","INV#IV85,Pax :AYISHA, PassNo :P0615315/ visaFor:30 DAYS DXB WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("556","6","103","85","Visa Receivable","5500","0","Visa Receivable : 85","2016-09-08","2016-09-08","9","1","2","0","INV#IV85,Pax :AYISHA, PassNo :P0615315/ visaFor:30 DAYS DXB WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("557","94","7","83","Visa Payable","5100","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV85,Pax :AYISHA, PassNo :P0615315/ visaFor:30 DAYS DXB WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("558","7","94","83","Visa Payable","0","5100","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV85,Pax :AYISHA, PassNo :P0615315/ visaFor:30 DAYS DXB WITH OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("559","145","6","86","Visa Receivable","0","45000","Visa Receivable : 86","2016-09-08","2016-09-08","9","1","2","0","INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("560","6","145","86","Visa Receivable","45000","0","Visa Receivable : 86","2016-09-08","2016-09-08","9","1","2","0","INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("561","132","7","84","Visa Payable","44400","0","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("562","7","132","84","Visa Payable","0","44400","","2016-09-08","2016-09-08","9","1","2","0","Details:INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("563","145","2","10","Cash Receipts","40000","0","VISA FOR 3 MONTHS","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 10,INV#IV86 Received By :Akhil,Details:INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("564","2","145","10","Cash Receipts","0","40000","VISA FOR 3 MONTHS","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 10,INV#IV86 Received By :Akhil,Details:INV#IV86,Pax :RAHILA, PassNo :L4220391/Pax :FATHIMA MIHRA, PassNo :M3684407/Pax :MUHAMMED MIRSHAD, PassNo :N5538499/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("567","143","14","1","Misc Receivable","0","1700","Misc Receivable : 1","2016-09-08","2016-09-08","10","1","2","0","INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("568","14","143","1","Misc Receivable","1700","0","Misc Receivable : 1","2016-09-08","2016-09-08","10","1","2","0","INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("569","146","15","1","Misc Payable","1500","0","","2016-09-08","2016-09-08","10","1","2","0","INV#1,sup:146,:against:1,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("570","15","146","1","Misc Payable","0","1500","","2016-09-08","2016-09-08","10","1","2","0","INV#1,sup:146,:against:1,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("573","96","6","87","Visa Receivable","0","16000","Visa Receivable : 87","2016-09-08","2016-09-08","8","1","2","0","INV#IV87,Pax :HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("574","6","96","87","Visa Receivable","16000","0","Visa Receivable : 87","2016-09-08","2016-09-08","8","1","2","0","INV#IV87,Pax :HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("575","125","7","85","Visa Payable","14500","0","COUNTER 1204","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV87,Pax :HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("576","7","125","85","Visa Payable","0","14500","COUNTER 1204","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV87,Pax :HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("577","147","147","7","Opening Balance","0","1450","Opening Balance","2016-09-08","2016-09-08","10","1","2","0","INV#7, acc:SHAMNAS PURCHASE ,Opening:1450");
INSERT INTO transaction VALUES("578","1","147","7","Opening Balance","1450","0","Opening Balance","2016-09-08","2016-09-08","10","1","2","0","INV#7, acc:SHAMNAS PURCHASE ,Opening:1450");
INSERT INTO transaction VALUES("579","96","6","88","Visa Receivable","0","11000","Visa Receivable : 88","2016-09-08","2016-09-08","8","1","2","0","INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("580","6","96","88","Visa Receivable","11000","0","Visa Receivable : 88","2016-09-08","2016-09-08","8","1","2","0","INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("581","125","7","86","Visa Payable","11000","0","","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("582","7","125","86","Visa Payable","0","11000","","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("583","125","2","2","Cash Payments","0","8500","COUNTER 1204","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP2,INV#PV86 Paid By :Akhil To : counter16,Details:INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("584","2","125","2","Cash Payments","8500","0","COUNTER 1204","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP2,INV#PV86 Paid By :Akhil To : counter16,Details:INV#IV88,Pax :MTM615 HIDAYATHULLA, PassNo :J8843095/ visaFor:90 DAYS QATAR BUSINESS,By:Safuvan Staff");
INSERT INTO transaction VALUES("585","129","6","89","Visa Receivable","0","900","Visa Receivable : 89","2016-09-08","2016-09-08","10","1","2","0","INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("586","6","129","89","Visa Receivable","900","0","Visa Receivable : 89","2016-09-08","2016-09-08","10","1","2","0","INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("587","147","7","87","Visa Payable","900","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("588","7","147","87","Visa Payable","0","900","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("603","130","133","4","Bank Receipts","50000","0","","2016-09-08","2016-09-08","15","1","2","0","VouNo#BR4,INV#IV28,IV30,IV32 Received By :Akhil,Details:INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("604","133","130","4","Bank Receipts","0","50000","","2016-09-08","2016-09-08","15","1","2","0","VouNo#BR4,INV#IV28,IV30,IV32 Received By :Akhil,Details:INV#IV32,Pax :MANZOOR, PassNo :P0914263/Pax :NOUFAL, PassNo :J5706307/Pax :PERAMAYIAN, PassNo :L9319426/Pax :ARAVINDAN, PassNo :M2761361/Pax :SAINUDHEEN, PassNo :Z2993148/Pax :RUKIYA, PassNo :K3086549/Pax :HAMNA, PassNo :N9974878/Pax :IDHAYATHUL HAMDAN, PassNo :N9974878/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("605","148","6","94","Visa Receivable","0","3800","Visa Receivable : 94","2016-09-08","2016-09-08","8","1","2","0","INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA,By:Safuvan");
INSERT INTO transaction VALUES("606","6","148","94","Visa Receivable","3800","0","Visa Receivable : 94","2016-09-08","2016-09-08","8","1","2","0","INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA,By:Safuvan");
INSERT INTO transaction VALUES("607","97","7","91","Visa Payable","3300","0","","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA,By:Safuvan");
INSERT INTO transaction VALUES("608","7","97","91","Visa Payable","0","3300","","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA,By:Safuvan");
INSERT INTO transaction VALUES("613","148","2","11","Cash Receipts","3800","0","paid cash for visa amount","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 11,INV#IV94 Received By :Akhil,Details:INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA,By:Safuvan");
INSERT INTO transaction VALUES("614","2","148","11","Cash Receipts","0","3800","paid cash for visa amount","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 11,INV#IV94 Received By :Akhil,Details:INV#IV94,Pax :mohammed, PassNo :k8000829/ visaFor:30 DAYS MALASIA,By:Safuvan");
INSERT INTO transaction VALUES("619","129","6","97","Visa Receivable","0","300","Visa Receivable : 97","2016-09-08","2016-09-08","10","1","2","0","INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("620","6","129","97","Visa Receivable","300","0","Visa Receivable : 97","2016-09-08","2016-09-08","10","1","2","0","INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("621","97","7","94","Visa Payable","300","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("622","7","97","94","Visa Payable","0","300","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("623","97","7","95","Visa Payable","300","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("624","7","97","95","Visa Payable","0","300","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV97,Pax :1247	HISHAM MASHOOD, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("625","129","6","96","Visa Receivable","0","15900","Visa Receivable : 96","2016-09-08","2016-09-08","10","1","2","0","INV#IV96,Pax :1247	HISHAM MASHOOD, PassNo :J5440840/ visaFor:90 DAYS DXB SETTING, By:Irfad");
INSERT INTO transaction VALUES("626","6","129","96","Visa Receivable","15900","0","Visa Receivable : 96","2016-09-08","2016-09-08","10","1","2","0","INV#IV96,Pax :1247	HISHAM MASHOOD, PassNo :J5440840/ visaFor:90 DAYS DXB SETTING, By:Irfad");
INSERT INTO transaction VALUES("627","125","7","93","Visa Payable","15900","0","","2016-09-08","2016-09-08","10","1","2","0","INV#93,sup:125,:against:96,Details:INV#IV96,Pax :1247	HISHAM MASHOOD, PassNo :J5440840/ visaFor:90 DAYS DXB SETTING, By:Irfad");
INSERT INTO transaction VALUES("628","7","125","93","Visa Payable","0","15900","","2016-09-08","2016-09-08","10","1","2","0","INV#93,sup:125,:against:96,Details:INV#IV96,Pax :1247	HISHAM MASHOOD, PassNo :J5440840/ visaFor:90 DAYS DXB SETTING, By:Irfad");
INSERT INTO transaction VALUES("629","129","6","98","Visa Receivable","0","15900","Visa Receivable : 98","2016-09-08","2016-09-08","10","1","2","0","INV#IV98,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :H5686487/ visaFor:90 DAYS DXB SETTING,By:Irfad");
INSERT INTO transaction VALUES("630","6","129","98","Visa Receivable","15900","0","Visa Receivable : 98","2016-09-08","2016-09-08","10","1","2","0","INV#IV98,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :H5686487/ visaFor:90 DAYS DXB SETTING,By:Irfad");
INSERT INTO transaction VALUES("631","125","7","96","Visa Payable","15900","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV98,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :H5686487/ visaFor:90 DAYS DXB SETTING,By:Irfad");
INSERT INTO transaction VALUES("632","7","125","96","Visa Payable","0","15900","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV98,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :H5686487/ visaFor:90 DAYS DXB SETTING,By:Irfad");
INSERT INTO transaction VALUES("633","129","6","99","Visa Receivable","0","300","Visa Receivable : 99","2016-09-08","2016-09-08","10","1","2","0","INV#IV99,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("634","6","129","99","Visa Receivable","300","0","Visa Receivable : 99","2016-09-08","2016-09-08","10","1","2","0","INV#IV99,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("635","97","7","97","Visa Payable","300","0","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV99,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("636","7","97","97","Visa Payable","0","300","","2016-09-08","2016-09-08","10","1","2","0","Details:INV#IV99,Pax :1254	ASHRAF PURATH PARAMBATH, PassNo :k000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("643","101","2","12","Cash Receipts","5200","0","adjust salil a/c","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 12,INV#IV37 Received By :Akhil,Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("644","2","101","12","Cash Receipts","0","5200","adjust salil a/c","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 12,INV#IV37 Received By :Akhil,Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("645","127","2","3","Cash Payments","0","5150","adjust to salil a/c","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP3,INV#PV36 Paid By :Akhil To : ALTA CCJ,Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("646","2","127","3","Cash Payments","5150","0","adjust to salil a/c","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP3,INV#PV36 Paid By :Akhil To : ALTA CCJ,Details:INV#IV37,Pax :MANU BHAI, PassNo :N7456851/ visaFor:30 DAYS DXB WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("647","94","2","4","Cash Payments","0","5200","adjusted to IV37 manu bahi","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP4,INV#PV18,PV19 Paid By :Akhil To : SALIL PURCHASE,Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("648","2","94","4","Cash Payments","5200","0","adjusted to IV37 manu bahi","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP4,INV#PV18,PV19 Paid By :Akhil To : SALIL PURCHASE,Details:INV#IV19,Pax :ABDUL SALEEM, PassNo :K1233711/ visaFor:OTB NORMAL,By:Safuvan Staff");
INSERT INTO transaction VALUES("651","147","2","5","Cash Payments","0","900","paid cash","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP5,INV#PV87 Paid By :Akhil To : SHAMNAS PURCHASE,Details:INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("652","2","147","5","Cash Payments","900","0","paid cash","2016-09-08","2016-09-08","15","1","2","0","VouNo#CP5,INV#PV87 Paid By :Akhil To : SHAMNAS PURCHASE,Details:INV#IV89,Pax :AYISHA, PassNo :k000000/Pax :FATHIMA, PassNo :K000000/Pax :FAHEENA, PassNo :K000000/ visaFor:OTB NORMAL,By:Irfad");
INSERT INTO transaction VALUES("653","149","6","100","Visa Receivable","0","7750","Visa Receivable : 100","2016-09-08","2016-09-08","8","1","2","0","INV#IV100,Pax :SABAD MUHAMMED, PassNo :M4747049/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("654","6","149","100","Visa Receivable","7750","0","Visa Receivable : 100","2016-09-08","2016-09-08","8","1","2","0","INV#IV100,Pax :SABAD MUHAMMED, PassNo :M4747049/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("655","125","7","98","Visa Payable","7000","0","COUNTER 1180","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV100,Pax :SABAD MUHAMMED, PassNo :M4747049/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("656","7","125","98","Visa Payable","0","7000","COUNTER 1180","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV100,Pax :SABAD MUHAMMED, PassNo :M4747049/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("657","149","6","101","Visa Receivable","0","7750","Visa Receivable : 101","2016-09-08","2016-09-08","8","1","2","0","INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("658","6","149","101","Visa Receivable","7750","0","Visa Receivable : 101","2016-09-08","2016-09-08","8","1","2","0","INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("659","125","7","99","Visa Payable","7000","0","COUNTER 1182","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("660","7","125","99","Visa Payable","0","7000","COUNTER 1182","2016-09-08","2016-09-08","8","1","2","0","Details:INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("661","149","2","13","Cash Receipts","15500","0","counter 16","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 13,INV#IV100,IV101 Received By :Akhil,Details:INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("662","2","149","13","Cash Receipts","0","15500","counter 16","2016-09-08","2016-09-08","15","1","2","0","VouNo#CR 13,INV#IV100,IV101 Received By :Akhil,Details:INV#IV101,Pax :MUHAMMED ALI MUHAMMED, PassNo :H5110153/ visaFor:OMAN EMIGRATION,By:Safuvan");
INSERT INTO transaction VALUES("663","126","2","9","Cash Receipts","32500","0","","2016-09-09","2016-09-09","6","2","2","0","VouNo#CR 9,INV#IA13 Received By :Manu,Details:INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("664","2","126","9","Cash Receipts","0","32500","","2016-09-09","2016-09-09","6","2","2","0","VouNo#CR 9,INV#IA13 Received By :Manu,Details:INV#IA13, Pax:SUNEER MAJEED, Passport No:, Sector:TRV/AUH, Date:06-09-2016/25-09-2016, Tkt:P1YKWK/WP4IO2, PNR:0, By:Manu");
INSERT INTO transaction VALUES("665","114","6","102","Visa Receivable","0","16500","Visa Receivable : 102","2016-09-09","2016-09-09","9","1","2","0","INV#IV102,Pax :HAMDAN HAMEED, PassNo :P2203649/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("666","6","114","102","Visa Receivable","16500","0","Visa Receivable : 102","2016-09-09","2016-09-09","9","1","2","0","INV#IV102,Pax :HAMDAN HAMEED, PassNo :P2203649/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("667","132","7","100","Visa Payable","15800","0","","2016-09-09","2016-09-09","9","1","2","0","Details:INV#IV102,Pax :HAMDAN HAMEED, PassNo :P2203649/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("668","7","132","100","Visa Payable","0","15800","","2016-09-09","2016-09-09","9","1","2","0","Details:INV#IV102,Pax :HAMDAN HAMEED, PassNo :P2203649/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("669","93","6","103","Visa Receivable","0","16200","Visa Receivable : 103","2016-09-09","2016-09-09","9","1","2","0","INV#IV103,Pax :JAMSHAD, PassNo :H1608955/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("670","6","93","103","Visa Receivable","16200","0","Visa Receivable : 103","2016-09-09","2016-09-09","9","1","2","0","INV#IV103,Pax :JAMSHAD, PassNo :H1608955/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("671","132","7","101","Visa Payable","15800","0","","2016-09-09","2016-09-09","9","1","2","0","Details:INV#IV103,Pax :JAMSHAD, PassNo :H1608955/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("672","7","132","101","Visa Payable","0","15800","","2016-09-09","2016-09-09","9","1","2","0","Details:INV#IV103,Pax :JAMSHAD, PassNo :H1608955/ visaFor:90 DAYS DXB SETTING,By:Sahad");
INSERT INTO transaction VALUES("673","127","127","0","Opening Balance","750","0","Opening Balance","2016-09-09","2016-09-09","10","1","2","0","INV#1, acc:ALTA CCJ,Opening:750");
INSERT INTO transaction VALUES("674","1","127","0","Opening Balance","0","750","Opening Balance","2016-09-09","2016-09-09","10","1","2","0","INV#1, acc:ALTA CCJ,Opening:750");
INSERT INTO transaction VALUES("677","140","2","6","Cash Payments","0","12500","PAID CASH THROUGH FEDERAL BANK","2016-09-09","2016-09-09","15","1","2","0","VouNo#CP6,INV#PV77,PV78 Paid By :Akhil To : GULF INDIA TRAVELS,Details:INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("678","2","140","6","Cash Payments","12500","0","PAID CASH THROUGH FEDERAL BANK","2016-09-09","2016-09-09","15","1","2","0","VouNo#CP6,INV#PV77,PV78 Paid By :Akhil To : GULF INDIA TRAVELS,Details:INV#IV80,Pax :AMAL PRABHAKAR, PassNo :J66565291/ visaFor:30 DAYS QATAR BUSINESS,By:Sahad Staff");
INSERT INTO transaction VALUES("679","129","6","91","Visa Receivable","0","300","Visa Receivable : 91","2016-09-09","2016-09-08","10","1","2","0","INV#IV91,Pax :1196	MUHAMMED ALI, PassNo :0/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("680","6","129","91","Visa Receivable","300","0","Visa Receivable : 91","2016-09-09","2016-09-08","10","1","2","0","INV#IV91,Pax :1196	MUHAMMED ALI, PassNo :0/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("681","97","7","89","Visa Payable","300","0","","2016-09-09","2016-09-08","10","1","2","0","INV#89,sup:97,:against:91,Details:INV#IV91,Pax :1196	MUHAMMED ALI, PassNo :0/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("682","7","97","89","Visa Payable","0","300","","2016-09-09","2016-09-08","10","1","2","0","INV#89,sup:97,:against:91,Details:INV#IV91,Pax :1196	MUHAMMED ALI, PassNo :0/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("685","129","6","92","Visa Receivable","0","59200","Visa Receivable : 92","2016-09-09","2016-09-08","10","1","2","0","INV#IV92,Pax :1245    AYSHANA RAMEES, PassNo :M5856848/Pax :1245	NASREENA, PassNo :M0882717/Pax :1198   VALSALA, PassNo :N2189073/Pax :1197	SAREENA, PassNo :K9858317/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("686","6","129","92","Visa Receivable","59200","0","Visa Receivable : 92","2016-09-09","2016-09-08","10","1","2","0","INV#IV92,Pax :1245    AYSHANA RAMEES, PassNo :M5856848/Pax :1245	NASREENA, PassNo :M0882717/Pax :1198   VALSALA, PassNo :N2189073/Pax :1197	SAREENA, PassNo :K9858317/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("689","129","6","95","Visa Receivable","0","1200","Visa Receivable : 95","2016-09-09","2016-09-08","10","1","2","0","INV#IV95,Pax :SAREENA VALSALA NASREENA /AYSHANA RAMEES, PassNo :k000000/Pax :VALSALA, PassNo :K000000/Pax :NASREENA, PassNo :K00000/Pax :AYSHANA RAMEES, PassNo :K00000/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("690","6","129","95","Visa Receivable","1200","0","Visa Receivable : 95","2016-09-09","2016-09-08","10","1","2","0","INV#IV95,Pax :SAREENA VALSALA NASREENA /AYSHANA RAMEES, PassNo :k000000/Pax :VALSALA, PassNo :K000000/Pax :NASREENA, PassNo :K00000/Pax :AYSHANA RAMEES, PassNo :K00000/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("691","97","7","92","Visa Payable","1200","0","","2016-09-09","2016-09-08","10","1","2","0","INV#92,sup:97,:against:95,Details:INV#IV95,Pax :SAREENA VALSALA NASREENA /AYSHANA RAMEES, PassNo :k000000/Pax :VALSALA, PassNo :K000000/Pax :NASREENA, PassNo :K00000/Pax :AYSHANA RAMEES, PassNo :K00000/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("692","7","97","92","Visa Payable","0","1200","","2016-09-09","2016-09-08","10","1","2","0","INV#92,sup:97,:against:95,Details:INV#IV95,Pax :SAREENA VALSALA NASREENA /AYSHANA RAMEES, PassNo :k000000/Pax :VALSALA, PassNo :K000000/Pax :NASREENA, PassNo :K00000/Pax :AYSHANA RAMEES, PassNo :K00000/ visaFor:OTB NORMAL, By:Irfad");
INSERT INTO transaction VALUES("693","136","134","6","Bank Receipts","15418","0","Deposited in south indian bank","2016-09-09","2016-09-06","15","1","2","0","VouNo#BR6,INV#IV70 Received By :Akhil,Details:INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("694","134","136","6","Bank Receipts","0","15418","Deposited in south indian bank","2016-09-09","2016-09-06","15","1","2","0","VouNo#BR6,INV#IV70 Received By :Akhil,Details:INV#IV70,Pax :SAJEEVAN, PassNo :M0569663/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Sahad Staff");
INSERT INTO transaction VALUES("695","95","133","7","Bank Receipts","150000","0","","2016-09-09","2016-09-09","15","1","2","0","VouNo#BR7,INV#IV2,IV7,IV8,IV10,IV11,IV12,IV13,IV17,IV26,IV29 Received By :Akhil,Details:INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("696","133","95","7","Bank Receipts","0","150000","","2016-09-09","2016-09-09","15","1","2","0","VouNo#BR7,INV#IV2,IV7,IV8,IV10,IV11,IV12,IV13,IV17,IV26,IV29 Received By :Akhil,Details:INV#IV29,Pax :ANEEM, PassNo :k000000/ visaFor:OTB NORMAL,By:Mtm Travel Mart Kzkd");
INSERT INTO transaction VALUES("697","81","2","4","Cash Payments","0","39240","","2016-09-09","2016-09-09","6","2","2","0","VouNo#CP4,INV#PA11,PA12,PA13 By :Manu To : ");
INSERT INTO transaction VALUES("698","2","81","4","Cash Payments","39240","0","","2016-09-09","2016-09-09","6","2","2","0","VouNo#CP4,INV#PA11,PA12,PA13 By :Manu To : ");
INSERT INTO transaction VALUES("699","129","6","90","Visa Receivable","0","11400","Visa Receivable : 90","2016-09-09","2016-09-08","10","1","2","0","INV#IV90,Pax :1196	MUHAMMED ALI, PassNo :K6459585/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("700","6","129","90","Visa Receivable","11400","0","Visa Receivable : 90","2016-09-09","2016-09-08","10","1","2","0","INV#IV90,Pax :1196	MUHAMMED ALI, PassNo :K6459585/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("701","125","7","88","Visa Payable","11400","0","","2016-09-09","2016-09-08","10","1","2","0","INV#88,sup:125,:against:90,Details:INV#IV90,Pax :1196	MUHAMMED ALI, PassNo :K6459585/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("702","7","125","88","Visa Payable","0","11400","","2016-09-09","2016-09-08","10","1","2","0","INV#88,sup:125,:against:90,Details:INV#IV90,Pax :1196	MUHAMMED ALI, PassNo :K6459585/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("705","129","133","8","Bank Receipts","50000","0","COUNTER 16","2016-09-09","2016-09-07","15","1","2","0","VouNo#BR8,INV#IV92 Received By :Akhil,Details:INV#IV92,Pax :1245    AYSHANA RAMEES, PassNo :M5856848/Pax :1245	NASREENA, PassNo :M0882717/Pax :1198   VALSALA, PassNo :N2189073/Pax :1197	SAREENA, PassNo :K9858317/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("706","133","129","8","Bank Receipts","0","50000","COUNTER 16","2016-09-09","2016-09-07","15","1","2","0","VouNo#BR8,INV#IV92 Received By :Akhil,Details:INV#IV92,Pax :1245    AYSHANA RAMEES, PassNo :M5856848/Pax :1245	NASREENA, PassNo :M0882717/Pax :1198   VALSALA, PassNo :N2189073/Pax :1197	SAREENA, PassNo :K9858317/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("707","99","6","104","Visa Receivable","0","78325","Visa Receivable : 104","2016-09-09","2016-09-09","10","1","2","0","INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("708","6","99","104","Visa Receivable","78325","0","Visa Receivable : 104","2016-09-09","2016-09-09","10","1","2","0","INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("709","125","7","102","Visa Payable","78325","0","","2016-09-09","2016-09-09","10","1","2","0","Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("710","7","125","102","Visa Payable","0","78325","","2016-09-09","2016-09-09","10","1","2","0","Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("711","99","133","9","Bank Receipts","78325","0","COUNTER16","2016-09-09","2016-09-09","15","1","2","0","VouNo#BR9,INV#IV104 Received By :Akhil,Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("712","133","99","9","Bank Receipts","0","78325","COUNTER16","2016-09-09","2016-09-09","15","1","2","0","VouNo#BR9,INV#IV104 Received By :Akhil,Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("713","125","133","1","Bank Payments","0","78325","","2016-09-09","2016-09-09","15","1","2","0","VouNo#BP1,INV#PV102 Paid By :Akhil To : counter16,Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("714","133","125","1","Bank Payments","78325","0","","2016-09-09","2016-09-09","15","1","2","0","VouNo#BP1,INV#PV102 Paid By :Akhil To : counter16,Details:INV#IV104,Pax :COUNTER BALANCE, PassNo :k000000/ visaFor:30 DAYS DXB WITHOUT OTB,By:Irfad");
INSERT INTO transaction VALUES("715","150","4","18","Airline Receivable","0","68000","Airline Receivable: 18","2016-09-09","2016-09-09","6","2","2","0","INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("716","4","150","18","Airline Receivable","68000","0","Airline Receivable: 18","2016-09-09","2016-09-09","6","2","2","0","INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("717","81","5","18","Airline Payable","67030","0","","2016-09-09","2016-09-09","6","2","2","0","Details:INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("718","5","81","18","Airline Payable","0","67030","","2016-09-09","2016-09-09","6","2","2","0","Details:INV#IA18, Pax:VALIYA VALAPPIL ALI ABDUL KAREEM/NADEERA ABDUL KAREEM, Passport No:G5039690, Sector:CCJ/AUH, Date:16-09-2016/16-09-2016, Tkt:CCSTFD/, PNR:0, By:Manu");
INSERT INTO transaction VALUES("719","125","133","2","Bank Payments","0","110900","","2016-09-09","2016-09-09","15","1","2","0","VouNo#BP2,INV#PV62,PV63,PV64,PV65,PV70,PV72,PV73,PV74 Paid By :Akhil To : counter16,Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("720","133","125","2","Bank Payments","110900","0","","2016-09-09","2016-09-09","15","1","2","0","VouNo#BP2,INV#PV62,PV63,PV64,PV65,PV70,PV72,PV73,PV74 Paid By :Akhil To : counter16,Details:INV#IV76,Pax :VYSHAKH 1233, PassNo :n9553909 /Pax :AHAMEDKUTTY 1258, PassNo :J2077389/Pax :THARUN 1259, PassNo :P0246374/Pax :BINOY 1261, PassNo :L6725925/ visaFor:90 DAYS DXB  WITHOUT OTB,By:Irfad Staff");
INSERT INTO transaction VALUES("731","146","15","2","Misc Payable","15000","0","Fly Hind haj ticket date change","2016-09-09","2016-09-09","5","1","2","0","Details:INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("732","15","146","2","Misc Payable","0","15000","Fly Hind haj ticket date change","2016-09-09","2016-09-09","5","1","2","0","Details:INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("733","151","14","2","Misc Receivable","0","24750","Misc Receivable : 2","2016-09-09","2016-09-09","5","1","2","0","INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("734","14","151","2","Misc Receivable","24750","0","Misc Receivable : 2","2016-09-09","2016-09-09","5","1","2","0","INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("735","151","2","14","Cash Receipts","24750","0","Haj Ticket date change penalty","2016-09-09","2016-09-09","5","1","2","0","VouNo#CR 14,INV#IM2 Received By :Sidheeq,Details:INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("736","2","151","14","Cash Receipts","0","24750","Haj Ticket date change penalty","2016-09-09","2016-09-09","5","1","2","0","VouNo#CR 14,INV#IM2 Received By :Sidheeq,Details:INV#IM2,Pax:Aslam and wife,Narration:Passport Service,By:Sidheeq ");
INSERT INTO transaction VALUES("741","96","6","105","Visa Receivable","0","5200","Visa Receivable : 105","2016-09-10","2016-09-10","8","1","2","0","INV#IV105,Pax :UDAYA MOOLYA, PassNo :J7091327/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("742","6","96","105","Visa Receivable","5200","0","Visa Receivable : 105","2016-09-10","2016-09-10","8","1","2","0","INV#IV105,Pax :UDAYA MOOLYA, PassNo :J7091327/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("743","94","7","103","Visa Payable","5100","0","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV105,Pax :UDAYA MOOLYA, PassNo :J7091327/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("744","7","94","103","Visa Payable","0","5100","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV105,Pax :UDAYA MOOLYA, PassNo :J7091327/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("745","146","2","7","Cash Payments","0","1500","PAID CASH TO TRAVEL MART","2016-09-10","2016-09-10","15","1","2","0","VouNo#CP7,INV#PM1 Paid By :Akhil To : MARKAZ TRAVEL MART,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("746","2","146","7","Cash Payments","1500","0","PAID CASH TO TRAVEL MART","2016-09-10","2016-09-10","15","1","2","0","VouNo#CP7,INV#PM1 Paid By :Akhil To : MARKAZ TRAVEL MART,Details:INV#IM1,Pax:ALI PATTNMARU VAL,Narration:Passport Service,By:Irfad Staff ");
INSERT INTO transaction VALUES("747","96","6","106","Visa Receivable","0","20800","Visa Receivable : 106","2016-09-10","2016-09-10","8","1","2","0","INV#IV106,Pax :KADAMBU NEBISA, PassNo :L6156737.P2778220/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("748","6","96","106","Visa Receivable","20800","0","Visa Receivable : 106","2016-09-10","2016-09-10","8","1","2","0","INV#IV106,Pax :KADAMBU NEBISA, PassNo :L6156737.P2778220/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("749","94","7","104","Visa Payable","20400","0","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV106,Pax :KADAMBU NEBISA, PassNo :L6156737.P2778220/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("750","7","94","104","Visa Payable","0","20400","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV106,Pax :KADAMBU NEBISA, PassNo :L6156737.P2778220/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("751","152","6","107","Visa Receivable","0","5200","Visa Receivable : 107","2016-09-10","2016-09-10","8","1","2","0","INV#IV107,Pax :SHAFJAN PATTILLATH CHANDERA, PassNo :H5103542/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("752","6","152","107","Visa Receivable","5200","0","Visa Receivable : 107","2016-09-10","2016-09-10","8","1","2","0","INV#IV107,Pax :SHAFJAN PATTILLATH CHANDERA, PassNo :H5103542/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("753","94","7","105","Visa Payable","5100","0","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV107,Pax :SHAFJAN PATTILLATH CHANDERA, PassNo :H5103542/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("754","7","94","105","Visa Payable","0","5100","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV107,Pax :SHAFJAN PATTILLATH CHANDERA, PassNo :H5103542/ visaFor:30 DAYS DXB WITH OTB,By:Safuvan");
INSERT INTO transaction VALUES("755","96","6","108","Visa Receivable","0","11500","Visa Receivable : 108","2016-09-10","2016-09-10","8","1","2","0","INV#IV108,Pax :, PassNo :/ visaFor:KUWAIT STAMPING,By:Safuvan");
INSERT INTO transaction VALUES("756","6","96","108","Visa Receivable","11500","0","Visa Receivable : 108","2016-09-10","2016-09-10","8","1","2","0","INV#IV108,Pax :, PassNo :/ visaFor:KUWAIT STAMPING,By:Safuvan");
INSERT INTO transaction VALUES("757","97","7","106","Visa Payable","11000","0","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV108,Pax :, PassNo :/ visaFor:KUWAIT STAMPING,By:Safuvan");
INSERT INTO transaction VALUES("758","7","97","106","Visa Payable","0","11000","","2016-09-10","2016-09-10","8","1","2","0","Details:INV#IV108,Pax :, PassNo :/ visaFor:KUWAIT STAMPING,By:Safuvan");
INSERT INTO transaction VALUES("759","95","6","16","Visa Receivable","0","14700","Visa Receivable : 16","2016-09-10","2016-09-05","10","1","2","0","INV#IV16,Pax :MISRIYA, PassNo :P1902660/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("760","6","95","16","Visa Receivable","14700","0","Visa Receivable : 16","2016-09-10","2016-09-05","10","1","2","0","INV#IV16,Pax :MISRIYA, PassNo :P1902660/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("761","102","7","16","Visa Payable","14500","0","","2016-09-10","2016-09-05","10","1","2","0","INV#16,sup:102,:against:16,Details:INV#IV16,Pax :MISRIYA, PassNo :P1902660/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("762","7","102","16","Visa Payable","0","14500","","2016-09-10","2016-09-05","10","1","2","0","INV#16,sup:102,:against:16,Details:INV#IV16,Pax :MISRIYA, PassNo :P1902660/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("765","127","7","22","Visa Payable","15900","0","","2016-09-10","2016-09-06","8","1","2","0","INV#22,sup:127,:against:23,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("766","7","127","22","Visa Payable","0","15900","","2016-09-10","2016-09-06","8","1","2","0","INV#22,sup:127,:against:23,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("767","96","6","23","Visa Receivable","0","17400","Visa Receivable : 23","2016-09-10","2016-09-06","8","1","2","0","INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("768","6","96","23","Visa Receivable","17400","0","Visa Receivable : 23","2016-09-10","2016-09-06","8","1","2","0","INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("769","96","2","3","Cash Receipts","5000","0","","2016-09-10","2016-09-06","15","1","2","0","VouNo#CR 3,INV#IV23 Received By :Akhil,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("770","2","96","3","Cash Receipts","0","5000","","2016-09-10","2016-09-06","15","1","2","0","VouNo#CR 3,INV#IV23 Received By :Akhil,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("771","96","2","4","Cash Receipts","16000","0","","2016-09-10","2016-09-06","15","1","2","0","VouNo#CR 4,INV#IV24 Received By :Akhil,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("772","2","96","4","Cash Receipts","0","16000","","2016-09-10","2016-09-06","15","1","2","0","VouNo#CR 4,INV#IV24 Received By :Akhil,Details:INV#IV24,Pax :626SHIRIN SHAHANA, PassNo :P1438308/ visaFor:90 DAYS DXB SETTING, By:Safuvan Staff");
INSERT INTO transaction VALUES("773","96","2","15","Cash Receipts","12400","0","","2016-09-10","2016-09-10","15","1","2","0","VouNo#CR 15,INV#IV23 Received By :Akhil,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("774","2","96","15","Cash Receipts","0","12400","","2016-09-10","2016-09-10","15","1","2","0","VouNo#CR 15,INV#IV23 Received By :Akhil,Details:INV#IV23,Pax :FATHIMA HASHIFA, PassNo :N7809054/ visaFor:90 DAYS DXB SETTING, By:Safuvan");
INSERT INTO transaction VALUES("775","95","6","72","Visa Receivable","0","43800","Visa Receivable : 72","2016-09-10","2016-09-07","10","1","2","0","INV#IV72,Pax :ANEEM 1223, PassNo :K5283027/Pax :VIJESH 1222, PassNo :K7027177/Pax :FERVEES 1199, PassNo :J5802083/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("776","6","95","72","Visa Receivable","43800","0","Visa Receivable : 72","2016-09-10","2016-09-07","10","1","2","0","INV#IV72,Pax :ANEEM 1223, PassNo :K5283027/Pax :VIJESH 1222, PassNo :K7027177/Pax :FERVEES 1199, PassNo :J5802083/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Irfad");
INSERT INTO transaction VALUES("777","97","7","0","Visa Payable","43800","0","","2016-09-10","0000-00-00","10","1","2","0","INV#,sup:97,:against:,Details:");
INSERT INTO transaction VALUES("778","7","97","0","Visa Payable","0","43800","","2016-09-10","0000-00-00","10","1","2","0","INV#,sup:97,:against:,Details:");

DROP TABLE IF EXISTS transport_provider;
CREATE TABLE `transport_provider` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `providerName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO transport_provider VALUES("1","Ujoor","MAKKA","00000000","4","19","2");

DROP TABLE IF EXISTS user_permission;
CREATE TABLE `user_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NOT NULL,
  `basicSetting` int(11) NOT NULL DEFAULT '0',
  `basicSettingEdit` int(11) NOT NULL DEFAULT '0',
  `package` int(11) NOT NULL DEFAULT '0',
  `packageEdit` int(11) NOT NULL DEFAULT '0',
  `account` int(11) NOT NULL DEFAULT '0',
  `accountEdit` int(11) NOT NULL DEFAULT '0',
  `cashBank` int(11) NOT NULL DEFAULT '0',
  `cashBankEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceAirline` int(11) NOT NULL DEFAULT '0',
  `invoiceAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceHotel` int(11) NOT NULL DEFAULT '0',
  `invoiceHotelEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceVisa` int(11) NOT NULL DEFAULT '0',
  `invoiceVisaEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceMisc` int(11) NOT NULL DEFAULT '0',
  `invoiceMiscEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceTransport` int(11) NOT NULL DEFAULT '0',
  `invoiceTransportEdit` int(11) NOT NULL DEFAULT '0',
  `invoicePackage` int(11) NOT NULL DEFAULT '0',
  `invoicePackageEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseAirline` int(11) NOT NULL DEFAULT '0',
  `purchaseAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseHotel` int(11) NOT NULL DEFAULT '0',
  `purchaseHotelEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseVisa` int(11) NOT NULL DEFAULT '0',
  `purchaseVisaEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseMisc` int(11) NOT NULL DEFAULT '0',
  `purchaseMiscEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseTransport` int(11) NOT NULL DEFAULT '0',
  `purchaseTransportEdit` int(11) NOT NULL DEFAULT '0',
  `purchasePackage` int(11) NOT NULL DEFAULT '0',
  `purchasePackageEdit` int(11) NOT NULL DEFAULT '0',
  `accountReport` int(11) NOT NULL DEFAULT '0',
  `otherReport` int(11) NOT NULL DEFAULT '0',
  `user` int(11) NOT NULL DEFAULT '0',
  `userEdit` int(11) NOT NULL DEFAULT '0',
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `staffId` (`staffId`),
  CONSTRAINT `user_permission_ibfk_1` FOREIGN KEY (`staffId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
INSERT INTO user_permission VALUES("1","1","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("2","2","1","1","0","0","1","1","1","1","1","1","1","0","1","1","1","0","1","1","1","0","1","1","1","0","1","1","1","0","1","1","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("3","3","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("4","6","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("5","5","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("6","4","1","0","0","0","1","0","1","0","1","0","1","0","1","1","1","0","1","0","1","0","1","0","1","0","1","1","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("8","8","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("9","9","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("11","11","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("12","7","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("13","12","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("14","12","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("15","13","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("16","16","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("17","14","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("18","17","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("19","19","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("20","18","1","0","0","0","1","0","1","0","1","0","0","0","1","0","1","0","1","0","0","0","1","0","0","0","1","0","1","0","1","0","0","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("21","22","1","0","0","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0","0");

DROP TABLE IF EXISTS vehicle_types;
CREATE TABLE `vehicle_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `vehicleType` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO vehicle_types VALUES("1","Bus","4","19","2");

DROP TABLE IF EXISTS visa_passenger;
CREATE TABLE `visa_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visaId` int(11) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(20) NOT NULL,
  `passportNo` varchar(50) NOT NULL,
  `visaFor` varchar(50) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `tokenNo` varchar(20) NOT NULL,
  `visaFee` float NOT NULL,
  `vfs` float NOT NULL,
  `ddCharge` float NOT NULL,
  `serviceCharge` float NOT NULL,
  `otherCharge` float NOT NULL,
  `courierCharge` float NOT NULL,
  `subTotal` float NOT NULL,
  `travelDate` date NOT NULL,
  `visaMessage` varchar(20) NOT NULL,
  `msgChngBy` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=217 DEFAULT CHARSET=utf8;
INSERT INTO visa_passenger VALUES("1","1","MAIMOONA MANJAPILLI AHAMMAD","0000-00-00","F","N1595786","3","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","1500","0","0","16500","0000-00-00","","0","","2","6","2");
INSERT INTO visa_passenger VALUES("2","2","MUFEED ABDUL AZIZ","0000-00-00","M","Z3668844","6","0000-00-00","0000-00-00","0000-00-00","","5800","0","0","400","0","0","6200","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("3","3","VISAKH","0000-00-00","M","N1140524","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","14600","0","0","28700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("4","4","ABDUL GAFOOR","0000-00-00","M","H2626002","5","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","700","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("5","5","ABDUL GAFOOR","0000-00-00","M","H2626002","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("6","6","SHAHUNL ABDULNASAR","0000-00-00","M","G8487574","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("7","7","SHAJAHAN PALLILATH","0000-00-00","M","H5103542","5","0000-00-00","0000-00-00","0000-00-00","","5100","0","0","100","0","0","5200","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("8","8","SIMON","0000-00-00","M","J1485230","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("9","8","ELSY","0000-00-00","F","J1485339","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("10","9","AMEER","0000-00-00","M","M0314613","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("11","10","ALLY JOY","0000-00-00","M","J0885006","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","300","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("12","11","DENIAL","0000-00-00","M","M9715530","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("13","12","SUSY","0000-00-00","M","N9538535","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","200","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("14","13","RAHMATHUNNISA","0000-00-00","F","L9256080","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","200","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("15","14","SHANU MON","0000-00-00","M","J2491039","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("16","15","MAIMOONA MANJAPPILLY","0000-00-00","F","M159786","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","200","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("17","16","MEENA HILARY","0000-00-00","F","M1342981","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","300","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("211","17","MISRIYA","0000-00-00","M","P1902660","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","200","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("19","18","AGHIL","0000-00-00","M","L9642581","10","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","100","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("20","19","ABDUL SALEEM","0000-00-00","M","K1233711","5","0000-00-00","0000-00-00","0000-00-00","","5100","0","0","100","0","0","5200","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("21","20","ABDUL SALEEM","0000-00-00","M","K1233711","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("22","21","MOHAMMED ISMAIL","0000-00-00","M","L715982","25","0000-00-00","0000-00-00","0000-00-00","","3300","0","0","500","0","0","3800","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("23","22","MOHAMMED ISMAIL","0000-00-00","M","L715982","25","0000-00-00","0000-00-00","0000-00-00","","3300","0","0","500","0","0","3800","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("24","23","ABDUL GAFOOR","0000-00-00","M","H2626002","9","2016-09-06","0000-00-00","0000-00-00","","5300","0","0","0","0","0","5300","0000-00-00","","0","","1","5","2");
INSERT INTO visa_passenger VALUES("213","24","FATHIMA HASHIFA","0000-00-00","F","N7809054","11","0000-00-00","0000-00-00","0000-00-00","","15900","0","0","1500","0","0","17400","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("28","25","626SHIRIN SHAHANA","0000-00-00","M","P1438308","11","0000-00-00","0000-00-00","0000-00-00","","15900","0","0","500","0","0","16400","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("30","26","MUHAMMED ANSAB","0000-00-00","M","N1900984","5","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","300","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("31","27","ANSAR","0000-00-00","M","P2698527","10","2016-09-06","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("32","28","SAFNAS","0000-00-00","M","H6710271","9","2016-09-06","0000-00-00","0000-00-00","","14100","0","0","900","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("33","29","HASSAN PUTHIYATI","0000-00-00","M","G9210752","10","2016-09-06","0000-00-00","0000-00-00","","14100","0","0","600","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("34","30","ANEEM","0000-00-00","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","2","2");
INSERT INTO visa_passenger VALUES("35","31","HEZZA","0000-00-00","F","N8801988","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("36","31","ISMATHUL HAMRA","0000-00-00","F","N8239724","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("37","32","MTM570 JAMAL YASAR","0000-00-00","M","H3365841","23","0000-00-00","0000-00-00","0000-00-00","","15500","0","0","1000","0","0","16500","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("38","33","MANZOOR","0000-00-00","M","P0914263","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("39","33","NOUFAL","0000-00-00","M","J5706307","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("40","33","PERAMAYIAN","0000-00-00","M","L9319426","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("41","33","ARAVINDAN","0000-00-00","M","M2761361","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("42","33","SAINUDHEEN","0000-00-00","M","Z2993148","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("43","33","RUKIYA","0000-00-00","F","K3086549","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("44","33","HAMNA","0000-00-00","F","N9974878","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("45","33","IDHAYATHUL HAMDAN","0000-00-00","M","N9974878","5","2016-09-06","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("46","34","ANSIL","0000-00-00","M","L1756857","9","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","700","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("47","35","ZAINABA","0000-00-00","M","J8564596","10","2016-09-07","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("48","36","IBRAHIM KUTTY","0000-00-00","M","P2813905","11","2016-09-07","0000-00-00","0000-00-00","","15800","0","0","400","0","0","16200","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("49","37","MUHAMMED","0000-00-00","M","N5120158","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","900","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("50","38","MANU BHAI","0000-00-00","M","N7456851","5","2016-09-07","0000-00-00","0000-00-00","","5150","0","0","50","0","0","5200","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("51","39","SAROJA","0000-00-00","F","K6272814","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("52","39","JAFIN","0000-00-00","F","M3827069","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("53","40","CHANDRAN PULIKKATT","0000-00-00","M","J5352156","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","700","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("54","40","SHEEJA","0000-00-00","F","N4524757","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","700","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("55","41","NILAA NIKHIL","0000-00-00","M","P0422439","11","2016-09-07","0000-00-00","0000-00-00","","16000","0","0","0","0","0","16000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("56","42","JAMES CLEETUS","0000-00-00","M","N5427392","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("57","43","MERLIN BABU","0000-00-00","F","M0477973","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("58","44","ASHNA","0000-00-00","F","M6093308","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("59","45","SHAMEEM","0000-00-00","M","K3072306","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("60","45","SRIMATHY","0000-00-00","F","P4071810","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("61","46","SHEEBA","0000-00-00","F","N5159159","9","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","300","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("62","47","JUSTIN JAMES","0000-00-00","M","L5914253","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-200","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("63","48","ASOK KUMAR","0000-00-00","M","M1674522","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("64","48","DILEEP","0000-00-00","M","G6067478","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("65","48","HASHIM","0000-00-00","M","H6275220","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("66","48","RAMAKRISHNAN","0000-00-00","M","G6160411","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("67","48","JERIN JAMES","0000-00-00","M","K5427677","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("68","48","SABEEH","0000-00-00","M","H3049393","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("69","48","FAISAL","0000-00-00","M","K3033987","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("70","48","UMMER","0000-00-00","M","M4950208","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("71","48","HAFIZ","0000-00-00","M","J2006760","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("72","48","KUNJHU MON","0000-00-00","M","M7807535","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("73","48","MADAVADAS","0000-00-00","M","J5254307","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("74","48","VENUGOPALAN","0000-00-00","M","H5709904","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("75","48","SAROJINI","0000-00-00","F","H5708579","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("76","49","VARGHESE","0000-00-00","M","M832608","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("77","49","SUDHEESH","0000-00-00","M","N6950516","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("78","50","RAJA RAM","0000-00-00","M","H7437321","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","600","0","0","15400","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("79","50","VINOD","0000-00-00","M","N9184559","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","600","0","0","15400","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("80","50","MANOJ KUMAR","0000-00-00","M","K5356456","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","600","0","0","15400","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("81","51","ARIF","0000-00-00","M","M9418291","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("82","51","BONEY JOSEPH","0000-00-00","M","G8586116","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("83","52","SALIHA","0000-00-00","M","M0174249","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("84","52","NIMMY","0000-00-00","M","L2844381","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","-100","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("85","53","SARATH KRISHNA","0000-00-00","M","K6243609","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","900","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("86","54","HABEEBA SHAFEEEK","0000-00-00","M","P2642443","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("87","55","HASNA","0000-00-00","M","P1163918","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("88","55","ZAHIDA","0000-00-00","M","M9909492","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("89","56","DAISAN DAVID","0000-00-00","M","N1142416","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("90","57","SIJO JOSEPH","0000-00-00","M","N8591047","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("91","57","AJAY KRISHNA","0000-00-00","M","G9194546","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("92","58","ABDUL AZEEZ","0000-00-00","M","L8040492","5","2016-09-07","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("93","59","NAFSIYA KOLAMBIL","0000-00-00","F","G4454840","9","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","300","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("94","59","RSHAN AHAMMED","0000-00-00","M","P0352855","9","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","300","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("95","59","TANIA ISRA","0000-00-00","F","K5269033","9","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","300","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("100","60","NAHIM","0000-00-00","M","K2828090","10","0000-00-00","0000-00-00","0000-00-00","","400","0","0","0","0","0","400","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("97","61","ABDU RAHIMAN","0000-00-00","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("98","61","SREEJITH","0000-00-00","M","K8893537","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("99","62","JAMES","0000-00-00","M","J4059812","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("101","63","ANSAR","0000-00-00","M","M5169954","10","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("102","64","ANILA","0000-00-00","F","N7681273","12","0000-00-00","0000-00-00","0000-00-00","","2300","0","0","0","0","0","2300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("103","64","PABRI CK","0000-00-00","F","N7686960","12","0000-00-00","0000-00-00","0000-00-00","","2300","0","0","0","0","0","2300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("104","64","PATRICIA ANJELIN","0000-00-00","F","P2488318","12","0000-00-00","0000-00-00","0000-00-00","","2300","0","0","0","0","0","2300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("105","65","REMA KUMARI","0000-00-00","F","N9101342","12","0000-00-00","0000-00-00","0000-00-00","","2300","0","0","0","0","0","2300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("106","65","KAVITHA","0000-00-00","F","K6437126","12","0000-00-00","0000-00-00","0000-00-00","","2300","0","0","0","0","0","2300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("107","66","RAMANATHAN","0000-00-00","M","M6237420","12","0000-00-00","0000-00-00","0000-00-00","","2600","0","0","0","0","0","2600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("108","67","Fathima Hanna","0000-00-00","F","L7896144","10","0000-00-00","0000-00-00","0000-00-00","","14900","0","0","0","0","0","14900","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("109","68","RICHARD","0000-00-00","M","H4603974","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","1000","0","0","15100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("110","69","THAJUDHEEN","0000-00-00","M","K1076374","10","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","500","0","0","14600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("111","70","SUSHANTH","0000-00-00","M","K5230981","9","2016-09-07","0000-00-00","0000-00-00","","14100","0","0","900","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("112","71","SAJEEVAN","0000-00-00","M","M0569663","10","2016-09-07","0000-00-00","0000-00-00","","14800","0","0","600","0","0","15400","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("113","72","AYISHA KUNNATH","0000-00-00","F","P0915315","2","2016-09-07","0000-00-00","0000-00-00","","5500","0","0","300","0","0","5800","0000-00-00","","0","","2","6","2");
INSERT INTO visa_passenger VALUES("114","73","SABEENA 1118","0000-00-00","F","J5242293","10","0000-00-00","0000-00-00","0000-00-00","","7700","0","0","0","0","0","7700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("207","107","UDAYA MOOLYA","0000-00-00","M","J7091327","4","0000-00-00","0000-00-00","0000-00-00","","5100","0","0","100","0","0","5200","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("216","74","FERVEES 1199","0000-00-00","M","J5802083","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("215","74","VIJESH 1222","0000-00-00","M","K7027177","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("214","74","ANEEM 1223","0000-00-00","M","K5283027","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("191","94","1197	SAREENA","0000-00-00","F","K9858317","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("121","76","KAVERI","0000-00-00","M","K7027177","10","0000-00-00","0000-00-00","0000-00-00","","14900","0","0","0","0","0","14900","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("122","77","AKHILA 12200","0000-00-00","F","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("123","77","ISHAK 1224","0000-00-00","M","K00000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("124","77","SREESHMA 1226","0000-00-00","F","K00000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("125","78","VYSHAKH 1233","0000-00-00","M","n9553909","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("126","78","AHAMEDKUTTY 1258","0000-00-00","M","J2077389","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("127","78","THARUN 1259","0000-00-00","M","P0246374","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("128","78","BINOY 1261","0000-00-00","M","L6725925","10","0000-00-00","0000-00-00","0000-00-00","","14600","0","0","0","0","0","14600","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("129","79","NOORUNNISA 1234","0000-00-00","F","M7276307","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("130","79","MUHAMMED RASHID 1234","0000-00-00","M","P3999764","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("131","79","ASHA MATHARI 1240","0000-00-00","F","J5238983","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("132","79","GEETHA 1250","0000-00-00","F","N5577038","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("133","79","1262	ANU MOLE","0000-00-00","F","N7798897","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("134","79","1264	VIJITHA","0000-00-00","F","P0428557","10","0000-00-00","0000-00-00","0000-00-00","","14700","0","0","0","0","0","14700","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("135","80","VIKAS VINOD","0000-00-00","M","J3924121","18","2016-09-08","0000-00-00","0000-00-00","","8000","0","0","2000","0","0","10000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("136","81","VIKAS VINOD","0000-00-00","M","J3924121","15","2016-09-08","0000-00-00","0000-00-00","","5500","0","0","300","0","0","5800","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("137","82","AMAL PRABHAKAR","0000-00-00","M","J66565291","16","2016-09-08","0000-00-00","0000-00-00","","6500","0","0","100","0","0","6600","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("138","83","MUHAMMED SHAMMAS","0000-00-00","M","L9931582","9","2016-09-08","0000-00-00","0000-00-00","","14100","0","0","900","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("139","84","MTM564 AHMED ORMUNDAKKAL","0000-00-00","M","J4887754","19","0000-00-00","0000-00-00","0000-00-00","","16500","0","0","0","0","0","16500","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("140","85","RAJAN KOCHU","0000-00-00","M","K3751349","5","2016-09-08","0000-00-00","0000-00-00","","5100","0","0","0","0","0","5100","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("141","86","MATHUKUTTY","0000-00-00","M","L1747043","10","0000-00-00","0000-00-00","0000-00-00","","14100","0","0","600","0","0","14700","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("142","87","AYISHA","0000-00-00","F","P0615315","4","2016-09-08","0000-00-00","0000-00-00","","5100","0","0","400","0","0","5500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("143","88","RAHILA","0000-00-00","F","L4220391","10","2016-09-08","0000-00-00","0000-00-00","","14800","0","0","200","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("144","88","FATHIMA MIHRA","0000-00-00","F","M3684407","10","2016-09-08","0000-00-00","0000-00-00","","14800","0","0","200","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("145","88","MUHAMMED MIRSHAD","0000-00-00","F","N5538499","10","2016-09-08","0000-00-00","0000-00-00","","14800","0","0","200","0","0","15000","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("146","89","HIDAYATHULLA","0000-00-00","M","J8843095","17","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","1500","0","0","16000","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("147","90","MTM615 HIDAYATHULLA","0000-00-00","M","J8843095","17","0000-00-00","0000-00-00","0000-00-00","","11000","0","0","0","0","0","11000","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("148","91","AYISHA","0000-00-00","F","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("149","91","FATHIMA","0000-00-00","F","K000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("150","91","FAHEENA","0000-00-00","M","K000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("196","92","1196	MUHAMMED ALI","0000-00-00","M","K6459585","10","0000-00-00","0000-00-00","0000-00-00","","11400","0","0","0","0","0","11400","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("183","93","1196	MUHAMMED ALI","0000-00-00","M","0","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("195","97","AYSHANA RAMEES","0000-00-00","F","K00000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("190","94","1198   VALSALA","0000-00-00","F","N2189073","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("189","94","1245	NASREENA","0000-00-00","F","M0882717","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("188","94","1245    AYSHANA RAMEES","0000-00-00","F","M5856848","10","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","0","0","0","14800","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("197","106","COUNTER BALANCE,","0000-00-00","M","k000000","5","0000-00-00","0000-00-00","0000-00-00","","78325","0","0","0","0","0","78325","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("208","108","KADAMBU NEBISA,ANNATH,MAHAMMAD SHAFIK,MAHAMMAD NAWASHAD","0000-00-00","M","L6156737.P2778220,P2574005,P2887976","4","0000-00-00","0000-00-00","0000-00-00","","20400","0","0","400","0","0","20800","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("161","96","mohammed","0000-00-00","M","k8000829","25","0000-00-00","0000-00-00","0000-00-00","","3300","0","0","500","0","0","3800","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("194","97","NASREENA","0000-00-00","F","K00000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("193","97","VALSALA","0000-00-00","F","K000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("192","97","SAREENA VALSALA NASREENA /AYSHANA RAMEES","0000-00-00","F","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("168","98","1247	HISHAM MASHOOD","0000-00-00","M","J5440840","11","0000-00-00","0000-00-00","0000-00-00","","15900","0","0","0","0","0","15900","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("167","99","1247	HISHAM MASHOOD","0000-00-00","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("169","100","1254	ASHRAF PURATH PARAMBATH","0000-00-00","M","H5686487","11","0000-00-00","0000-00-00","0000-00-00","","15900","0","0","0","0","0","15900","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("170","101","1254	ASHRAF PURATH PARAMBATH","0000-00-00","M","k000000","32","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","1","10","2");
INSERT INTO visa_passenger VALUES("209","109","SHAFJAN PATTILLATH CHANDERA","0000-00-00","M","H5103542","4","0000-00-00","0000-00-00","0000-00-00","","5100","0","0","100","0","0","5200","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("179","102","SABAD MUHAMMED","0000-00-00","M","M4747049","37","0000-00-00","0000-00-00","0000-00-00","","7000","0","0","750","0","0","7750","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("180","103","MUHAMMED ALI MUHAMMED","0000-00-00","M","H5110153","37","0000-00-00","0000-00-00","0000-00-00","","7000","0","0","750","0","0","7750","0000-00-00","","0","","1","8","2");
INSERT INTO visa_passenger VALUES("181","104","HAMDAN HAMEED","0000-00-00","M","P2203649","11","2016-09-09","0000-00-00","0000-00-00","","15800","0","0","700","0","0","16500","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("182","105","JAMSHAD","0000-00-00","M","H1608955","11","2016-09-09","0000-00-00","0000-00-00","","15800","0","0","400","0","0","16200","0000-00-00","","0","","1","9","2");
INSERT INTO visa_passenger VALUES("210","110","","0000-00-00","","","38","0000-00-00","0000-00-00","0000-00-00","","11000","0","0","500","0","0","11500","0000-00-00","","0","","1","8","2");

DROP TABLE IF EXISTS visafor;
CREATE TABLE `visafor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visafor` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
INSERT INTO visafor VALUES("1","Dubai- 30 days","1","5","2");
INSERT INTO visafor VALUES("2","UAE  30 Days","2","6","2");
INSERT INTO visafor VALUES("3","UAE 90 Days","2","6","2");
INSERT INTO visafor VALUES("4","30 DAYS DXB WITH OTB","1","10","2");
INSERT INTO visafor VALUES("5","30 DAYS DXB WITHOUT OTB","1","10","2");
INSERT INTO visafor VALUES("6","30 DAYS DXB SETTING","1","10","2");
INSERT INTO visafor VALUES("7","30 DAYS DXB LSSUER","1","10","2");
INSERT INTO visafor VALUES("8","30 DAYS DXB MULTIPLE","1","10","2");
INSERT INTO visafor VALUES("9","90 DAYS DXB  WITH OTB","1","10","2");
INSERT INTO visafor VALUES("10","90 DAYS DXB  WITHOUT OTB","1","10","2");
INSERT INTO visafor VALUES("11","90 DAYS DXB SETTING","1","10","2");
INSERT INTO visafor VALUES("12","90 DYAS DXB LESSUER","1","10","2");
INSERT INTO visafor VALUES("14","90 DAYS DXB  MULTIPLE","1","10","2");
INSERT INTO visafor VALUES("15","30 DAYS QATAR TOURIST","1","10","2");
INSERT INTO visafor VALUES("16","30 DAYS QATAR BUSINESS","1","10","2");
INSERT INTO visafor VALUES("17","90 DAYS QATAR BUSINESS","1","10","2");
INSERT INTO visafor VALUES("18","10 DAYS OMAN","1","10","2");
INSERT INTO visafor VALUES("19","30 DAYS OMAN","1","10","2");
INSERT INTO visafor VALUES("20","SAUDI VISA STAMPING","1","10","2");
INSERT INTO visafor VALUES("21","SAUDI VISA STAMPING FAMILY","1","10","2");
INSERT INTO visafor VALUES("22","SAUDI VISA STAMPING RESIDENTIAL","1","10","2");
INSERT INTO visafor VALUES("23","30 DAYS SAUDI","1","10","2");
INSERT INTO visafor VALUES("24","90 DAYS SAUDI MULTIPLE","1","10","2");
INSERT INTO visafor VALUES("25","30 DAYS MALASIA","1","10","2");
INSERT INTO visafor VALUES("26","30 DAYS SREELANKA","1","10","2");
INSERT INTO visafor VALUES("27","UAE ATTASTATION","1","10","2");
INSERT INTO visafor VALUES("28","QATAR ATTASTATION","1","10","2");
INSERT INTO visafor VALUES("29","SAUDI ATTASTATION","1","10","2");
INSERT INTO visafor VALUES("30","14 DAYS BAHARAIN","1","10","2");
INSERT INTO visafor VALUES("31","30 DAYS BAHARAIN","1","10","2");
INSERT INTO visafor VALUES("32","OTB NORMAL","1","10","2");
INSERT INTO visafor VALUES("33","OTB URGENT","1","10","2");
INSERT INTO visafor VALUES("34","UMRA","4","19","2");
INSERT INTO visafor VALUES("35","DUBAI","4","23","2");
INSERT INTO visafor VALUES("36","SAUDI","4","23","2");
INSERT INTO visafor VALUES("37","OMAN EMIGRATION","1","8","2");
INSERT INTO visafor VALUES("38","KUWAIT STAMPING","1","8","2");



								/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
								/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
								/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
								/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
								/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
								/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
								/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
								
						