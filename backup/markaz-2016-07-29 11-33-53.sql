/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
								/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
								/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
								/*!40101 SET NAMES utf8 */;
								/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
								/*!40103 SET TIME_ZONE='+00:00' */;
								/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
								/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
								/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
								/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
								DROP TABLE IF EXISTS account_group;
CREATE TABLE `account_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `acGroup` text NOT NULL,
  `parent` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
INSERT INTO account_group VALUES("1","Account","0","0","0");
INSERT INTO account_group VALUES("2","Balance Sheet","1","0","0");
INSERT INTO account_group VALUES("3","Profit & Loss Account","1","0","0");
INSERT INTO account_group VALUES("4","Assets","2","0","0");
INSERT INTO account_group VALUES("5","Liabilities","2","0","0");
INSERT INTO account_group VALUES("6","Current Assets","4","0","0");
INSERT INTO account_group VALUES("7","Fixed Assets","4","0","0");
INSERT INTO account_group VALUES("8","Cash In Hand","6","0","0");
INSERT INTO account_group VALUES("9","Deposits","6","0","0");
INSERT INTO account_group VALUES("10","Investments","6","0","0");
INSERT INTO account_group VALUES("11","Loans & Advances (asset)","6","0","0");
INSERT INTO account_group VALUES("12","Bank A/c","6","0","0");
INSERT INTO account_group VALUES("13","Expenses","3","0","0");
INSERT INTO account_group VALUES("14","Income","3","0","0");
INSERT INTO account_group VALUES("15","Cash","8","0","0");
INSERT INTO account_group VALUES("16","Capital","5","0","0");
INSERT INTO account_group VALUES("17","Current Liabilities","5","0","0");
INSERT INTO account_group VALUES("18","Loans (liability)","5","0","0");
INSERT INTO account_group VALUES("19","Suspense Accounts","5","0","0");
INSERT INTO account_group VALUES("20","Bank OD A/C","17","0","0");
INSERT INTO account_group VALUES("21","Duties & Taxes","17","0","0");
INSERT INTO account_group VALUES("22","Journal Accounts","17","0","0");
INSERT INTO account_group VALUES("23","Provisions","17","0","0");
INSERT INTO account_group VALUES("24","Direct Expense","13","0","0");
INSERT INTO account_group VALUES("25","Indirect Expense","13","0","0");
INSERT INTO account_group VALUES("26","Direct Income","14","0","0");
INSERT INTO account_group VALUES("27","Indirect Income","14","0","0");
INSERT INTO account_group VALUES("28","Fittings","7","0","0");
INSERT INTO account_group VALUES("29","Advertisement","25","0","0");
INSERT INTO account_group VALUES("30","Donation Given","25","0","0");
INSERT INTO account_group VALUES("31","Electricity Bills","25","0","0");
INSERT INTO account_group VALUES("32","Miscellaneous Expense","25","0","0");
INSERT INTO account_group VALUES("33","Office Expense","25","0","0");
INSERT INTO account_group VALUES("34","Other Expenses","25","0","0");
INSERT INTO account_group VALUES("35","Telephone Bills","25","0","0");
INSERT INTO account_group VALUES("36","Other Income","27","0","0");
INSERT INTO account_group VALUES("37","Customer/ Sundry Debtors","6","0","0");
INSERT INTO account_group VALUES("38","Shipping Agents","17","0","0");
INSERT INTO account_group VALUES("39","Staff Deposits","17","0","0");
INSERT INTO account_group VALUES("40","Supplier/ Sundry Creditors","17","0","0");
INSERT INTO account_group VALUES("41","Discount Given ","24","0","0");
INSERT INTO account_group VALUES("42","Job Works","24","0","0");
INSERT INTO account_group VALUES("43","Purchases","24","0","0");
INSERT INTO account_group VALUES("44","Courier","26","0","0");
INSERT INTO account_group VALUES("45","Discount Recieved","26","0","0");
INSERT INTO account_group VALUES("46","Sales","26","0","0");
INSERT INTO account_group VALUES("47","Services","26","0","0");
INSERT INTO account_group VALUES("48","Commission Agent","25","0","0");
INSERT INTO account_group VALUES("49","Incentives Given","25","0","0");
INSERT INTO account_group VALUES("50","Printing Expense","25","0","0");
INSERT INTO account_group VALUES("51","Rent Given","25","0","0");
INSERT INTO account_group VALUES("52","Salaries","25","0","0");
INSERT INTO account_group VALUES("53","Sales Expenses","25","0","0");
INSERT INTO account_group VALUES("54","Tax Paid","25","0","0");
INSERT INTO account_group VALUES("55","Tax Recieved","25","0","0");
INSERT INTO account_group VALUES("56","Incentives Recieved","27","0","0");
INSERT INTO account_group VALUES("57","Branches","6","0","0");
INSERT INTO account_group VALUES("58","Main Branches","17","0","0");

DROP TABLE IF EXISTS accounts;
CREATE TABLE `accounts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `agentId` int(11) NOT NULL,
  `accountGroupId` int(11) NOT NULL,
  `accountName` varchar(200) NOT NULL,
  `openingBal` double NOT NULL,
  `payType` varchar(20) NOT NULL,
  `masterLedger` int(11) NOT NULL,
  `addDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `description` text NOT NULL,
  `contactPerson` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address2` text NOT NULL,
  `pin2` varchar(20) NOT NULL,
  `city2` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `accountGroupId` (`accountGroupId`),
  CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`accountGroupId`) REFERENCES `account_group` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
INSERT INTO accounts VALUES("1","0","19","Suspense","0","Credit","1","2016-03-22","1","0","Suspense Account","","","","","","","","","","");
INSERT INTO accounts VALUES("2","0","8","Cash Account","0","Credit","1","2016-03-22","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("3","0","24","Salary Payments","0","Credit","1","2016-03-22","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("4","0","14","Airline Receivable","0","Debit","1","2016-04-05","1","0","Airline charge received","","","","","","","","","","");
INSERT INTO accounts VALUES("5","0","13","Airline Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("6","0","14","Visa Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("7","0","13","Visa Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("8","0","14","Package Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("9","0","13","Package Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("10","0","14","Hotel Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("11","0","13","Hotel Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("12","0","14","Transport Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("13","0","13","Transport Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("14","0","14","Misc Receivable","0","Debit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("15","0","13","Misc Payable","0","Credit","1","2016-04-05","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("16","0","25","Discount Given","0","Credit","1","2016-05-25","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("17","0","27","Discount Received","0","Credit","1","2016-05-25","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("18","0","25","Tax","0","Credit","1","2016-06-24","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("19","0","25","Tds","0","Credit","1","2016-06-24","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("20","0","27","Tac","0","Credit","1","2016-06-24","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("21","0","25","Package Extra Chrg","0","Credit","1","2016-06-29","1","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("40","0","37","CREATIVE KDNGLR 2","0","Debit","0","2016-07-16","5","2","","SHANMUGAN","","","KODUNGALLUR","0480 3005058","0480 3005058","visakdr@creativeholidaysindia.com","","","");
INSERT INTO accounts VALUES("41","0","40","ALPHA CCJ","0","Credit","0","2016-07-16","5","2","","SALAM","","","KOZHIKODE","0495 2435946","9400655946","visa@alphaindia.biz","","","");
INSERT INTO accounts VALUES("42","0","40","ALTA CCJ","0","Credit","0","2016-07-16","5","2","","SHAMNAS","","","KOZHIKODE","0495 4050502","0495 4050503","visa@altatravel.in","","","");
INSERT INTO accounts VALUES("43","0","40","SALIL BOM","0","Credit","0","2016-07-16","5","2","","SALIL","","","BOMBAY","9892519222","9987244222","salilcs84@gmail.com","","","");
INSERT INTO accounts VALUES("44","0","40","TRAVEL PARTNER","0","Credit","0","2016-07-16","5","2","","RISANA","","","","0495 4016677","9544053344","CCJ@travelpartnerholidays.com","","","");
INSERT INTO accounts VALUES("45","0","37","sahad staff","0","Debit","0","2016-07-16","5","2","","SAHAD","","","TRISSUR","","9567656784","mtmsahad@gmail.com","","","");
INSERT INTO accounts VALUES("46","0","37","CREATIVE KDNGLR 1","0","Debit","0","2016-07-16","5","2","","SHANMUGAN","","","KODUNGALLUR","0480 3006000","9349231973","kdr@creative-india.com","","","");
INSERT INTO accounts VALUES("47","0","37","WALKING CUSTOMER","0","Credit","0","2016-07-16","5","2","","","","","","","0","","","","");
INSERT INTO accounts VALUES("48","0","12","FEDERAL BANK","0","Credit","0","2016-07-18","5","2","","","","","","","","","","","");
INSERT INTO accounts VALUES("49","0","12","SOUTH INDIAN BANK","0","Credit","0","2016-07-18","5","2","","","","","","","","","","","");
INSERT INTO accounts VALUES("50","0","37","MTM VETTICHIRA","0","Credit","0","2016-07-18","6","2","","","","","","04942617140","0","","","","");
INSERT INTO accounts VALUES("52","0","37","KUNHAYI PANNIKOTATH","0","Credit","0","2016-07-18","6","2","","","","","","","9946607862","","","","");
INSERT INTO accounts VALUES("53","0","37","NAFEESA MOYTHEENKOYA","0","Credit","0","2016-07-18","6","2","","","","","","","9447399396","","","","");
INSERT INTO accounts VALUES("54","0","37","SUHARA MAMBATH","0","Credit","0","2016-07-18","6","2","","","","","","","8891570223","","","","");
INSERT INTO accounts VALUES("55","0","37","PATHUMMA THURUTHIYIL MEETHAL","0","Credit","0","2016-07-18","6","2","","","","","","","8289843711","","","","");
INSERT INTO accounts VALUES("56","0","37","MAYIN KUTTY THONIKKADAVATH","0","Credit","0","2016-07-18","6","2","","","","","","","9447357815","","","","");
INSERT INTO accounts VALUES("57","0","37","Ibraheem Sqf Kottur","0","Credit","0","2016-07-18","6","2","","","","","","","9846345001","","","","");
INSERT INTO accounts VALUES("58","0","37","SUBAIDA VELLARATHODI","0","Credit","0","2016-07-18","6","2","","","","","","","9895925036","","","","");
INSERT INTO accounts VALUES("59","0","37","MUSTHAFA KAYANIKARA","0","Credit","0","2016-07-18","6","2","","","","","","","8606162786","","","","");
INSERT INTO accounts VALUES("60","0","37","FLYHIND TLY","0","Debit","0","2016-07-18","5","2","","LAJIMA","","","TALASSERY","0490 2344533","0","tlyflyhind@gmail.com","","","");
INSERT INTO accounts VALUES("61","0","37","AMEER","0","Credit","0","2016-07-18","6","2","","","","","","","9947809526","","","","");
INSERT INTO accounts VALUES("62","0","37","ABDUL RASHEED MANNARATTIL","0","Credit","0","2016-07-18","6","2","","","","","","","9847020540","","","","");
INSERT INTO accounts VALUES("63","0","37","PAIKAT TRAVELS","0","Debit","0","2016-07-18","5","2","","","","","","","04954012999","paikattravels@gmail.com","","","");
INSERT INTO accounts VALUES("64","0","37","SALIL BOM","0","Debit","0","2016-07-18","5","2","","","","","","","9892519222","salilcs84@gmail.com","","","");
INSERT INTO accounts VALUES("65","0","37","MOULAVI","0","Debit","0","2016-07-20","5","2","","","","","","","049540543332","","","","");
INSERT INTO accounts VALUES("66","0","37","GAMA HOLIDAYS CCJ","0","Debit","0","2016-07-22","5","2","","ARSHAD","","","CALICUT","903 7777 013","9037777019","gamaholidays@gmail.com","","","");
INSERT INTO accounts VALUES("67","0","40","AROOHA CCJ","0","Credit","0","2016-07-22","5","2","","RAJESH","","","","","9645313333","visa@aroohaholidays.in","","","");
INSERT INTO accounts VALUES("69","0","37","AKBAR VENGARA","0","Credit","0","2016-07-22","5","2","","LATHEEF","","","VENGARA","","974700791","akbartt_vga@yahoo.com","","","");
INSERT INTO accounts VALUES("70","0","37","FLYHIND CCJ","0","Debit","0","2016-07-23","5","2","","","","","","","0","","","","");
INSERT INTO accounts VALUES("71","0","40","SHAMNAS PERSONAL","0","Credit","0","2016-07-23","5","2","","","","","","","0","","","","");
INSERT INTO accounts VALUES("72","0","37","UMMER JOIPORE ABBASU","0","Credit","0","2016-07-26","6","2","","","","","","","9480083986","","","","");
INSERT INTO accounts VALUES("73","0","37","ABDURAHIMAN PUTHIYOTTIL","0","Credit","0","2016-07-26","6","2","","","","","","","9605457189","","","","");
INSERT INTO accounts VALUES("74","0","37","KOYA KAIKKOLAN PARAKKAL","0","Credit","0","2016-07-27","6","2","","","","","","","9497887086","","","","");
INSERT INTO accounts VALUES("75","0","37","ASLAM VENNEETTINDAVIDA MEETHAL","0","Credit","0","2016-07-27","6","2","","","","","","","8086994824","","","","");
INSERT INTO accounts VALUES("77","0","37","cus1","1","Credit","0","2016-07-27","33","28","","alisha","add","876787","kozhikode","0497564545","7867567676","ali@gmail.com","add2","342342","kozhikode");
INSERT INTO accounts VALUES("78","0","40","supplier1","1","Credit","0","2016-07-27","33","28","","new","sdf","987678","new","04562511564","6787656767","ali@gmail.com","sdf","2342","vwe");

DROP TABLE IF EXISTS airline_flight;
CREATE TABLE `airline_flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlinePassId` int(11) NOT NULL,
  `airlineName` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `journeyDate` date NOT NULL,
  `sectorFrom` varchar(100) NOT NULL,
  `sectorTo` varchar(100) NOT NULL,
  `classType` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `tax` varchar(30) NOT NULL,
  `yq` float NOT NULL,
  `procCharge` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlinePassId` (`airlinePassId`),
  CONSTRAINT `airline_flight_ibfk_2` FOREIGN KEY (`airlinePassId`) REFERENCES `airline_passenger` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
INSERT INTO airline_flight VALUES("1","1","","ix25322","8965754664","2016-07-27","ccj","jed","e","45600","1200","0","500","2","5");

DROP TABLE IF EXISTS airline_passenger;
CREATE TABLE `airline_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlineId` int(11) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationship` varchar(30) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlineId` (`airlineId`),
  CONSTRAINT `airline_passenger_ibfk_1` FOREIGN KEY (`airlineId`) REFERENCES `invoice_airline` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
INSERT INTO airline_passenger VALUES("1","1","Test","2016-07-19","0","M","kerala","","india","","E000200","2016-07-20","2016-07-26","kerala","2","5");

DROP TABLE IF EXISTS cash_payment;
CREATE TABLE `cash_payment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `discount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS cash_receipt;
CREATE TABLE `cash_receipt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `discount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
INSERT INTO cash_receipt VALUES("3","1","visa","1","14900","0","","ONLINE","BANK","2","5");
INSERT INTO cash_receipt VALUES("4","1","visa","2","44700","0","","ONLINE","BANK","2","5");
INSERT INTO cash_receipt VALUES("6","2","visa","4","0","0","","0","CASH","2","5");
INSERT INTO cash_receipt VALUES("8","2","visa","10","15500","0","","0","BANK","2","5");
INSERT INTO cash_receipt VALUES("11","3","visa","20","200","0","","0","CASH","2","5");
INSERT INTO cash_receipt VALUES("12","3","visa","23","29800","0","","0","CASH","2","5");
INSERT INTO cash_receipt VALUES("13","4","visa","44","14800","0","","0","CASH","2","5");
INSERT INTO cash_receipt VALUES("14","4","visa","49","14800","0","","0","CASH","2","5");
INSERT INTO cash_receipt VALUES("15","3","visa","11","15100","0","","0","BANK","2","5");
INSERT INTO cash_receipt VALUES("16","3","visa","22","5200","0","","0","BANK","2","5");
INSERT INTO cash_receipt VALUES("17","3","visa","25","5200","0","","0","BANK","2","5");
INSERT INTO cash_receipt VALUES("18","3","visa","26","5200","0","","0","BANK","2","5");

DROP TABLE IF EXISTS company;
CREATE TABLE `company` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `tds` int(11) NOT NULL,
  `circle` varchar(100) NOT NULL,
  `resPerson` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `remark` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
INSERT INTO company VALUES("2","Mtm Travel Service","","673004","KOZHIKODE","04954011515","8547116314","mtmvisa@gmail.com","","0","","","","");
INSERT INTO company VALUES("3","T","","","","","0","","","0","","","","");
INSERT INTO company VALUES("29","Mtm Travel Service-pattambi","NEAR COURT-PATTAMBI","0","PATTAMBI","","9349333888","MTMPATTAMBI@GMAIL.COM","","0","","","","");

DROP TABLE IF EXISTS flight;
CREATE TABLE `flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `flightNo` varchar(50) NOT NULL,
  `flightName` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO flight VALUES("1","0","Saudi Airlines","","2","6");
INSERT INTO flight VALUES("2","0","Air India","","2","6");

DROP TABLE IF EXISTS hotel;
CREATE TABLE `hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(10) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO hotel VALUES("1","Mubarak Plaza","","0","Makka","","Ksa","00","000","","","2","6");

DROP TABLE IF EXISTS hotel_customer;
CREATE TABLE `hotel_customer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelInvoiceId` int(11) NOT NULL,
  `roomType` varchar(20) NOT NULL,
  `noOfRooms` int(11) NOT NULL,
  `noOfPersons` int(11) NOT NULL,
  `rate` float NOT NULL,
  `subTax` varchar(30) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_airline;
CREATE TABLE `invoice_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float NOT NULL,
  `mainProcCharge` float NOT NULL,
  `mainOtherCharge` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `mainTax` varchar(30) NOT NULL,
  `discount` float NOT NULL,
  `pnr` varchar(20) NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
INSERT INTO invoice_airline VALUES("1","1","2016-07-19","47","0","0","0","","","0","0","","47300","Pending","2","5");

DROP TABLE IF EXISTS invoice_hotel;
CREATE TABLE `invoice_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `hotelId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `noOfNight` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `checkInDate` date NOT NULL,
  `checkInTime` varchar(20) NOT NULL,
  `checkOutDate` date NOT NULL,
  `checkOutTime` varchar(20) NOT NULL,
  `billingIns` varchar(100) NOT NULL,
  `reference` varchar(100) NOT NULL,
  `tax` varchar(30) NOT NULL,
  `tds` float NOT NULL,
  `otherCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_misc;
CREATE TABLE `invoice_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `basicAmount` float NOT NULL,
  `tds` float NOT NULL,
  `tax` varchar(20) NOT NULL,
  `otherCharge` float NOT NULL,
  `procCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `narration` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_package;
CREATE TABLE `invoice_package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `packageId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `proCharge` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `discount` float DEFAULT '0',
  `netAmount` float NOT NULL DEFAULT '0',
  `description` varchar(200) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package VALUES("1","1","2016-07-16","1","47","0","0","0","0","0","0","","2","5");
INSERT INTO invoice_package VALUES("21","14","2016-07-18","1","50","0","0","0","0","0","0","","2","6");
INSERT INTO invoice_package VALUES("9","3","2016-07-18","1","52","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("10","4","2016-07-18","1","53","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("11","5","2016-07-18","1","54","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("12","6","2016-07-18","1","55","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("13","7","2016-07-18","1","56","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("15","8","2016-07-18","1","57","0","0","0","0","0","0","","2","6");
INSERT INTO invoice_package VALUES("16","9","2016-07-18","1","58","0","0","0","0","0","700000","","2","6");
INSERT INTO invoice_package VALUES("20","13","2016-07-18","1","59","0","0","0","0","0","0","","2","6");
INSERT INTO invoice_package VALUES("18","11","2016-07-18","1","61","0","0","0","0","0","350000","","2","6");
INSERT INTO invoice_package VALUES("19","12","2016-07-18","1","62","0","0","0","0","0","700000","","2","6");
INSERT INTO invoice_package VALUES("24","15","2016-07-26","1","72","0","0","0","0","0","0","","2","6");
INSERT INTO invoice_package VALUES("26","16","2016-07-26","1","73","0","0","0","0","0","0","","2","6");
INSERT INTO invoice_package VALUES("27","17","2016-07-27","1","74","0","0","0","0","0","700000","","2","6");

DROP TABLE IF EXISTS invoice_package_airline;
CREATE TABLE `invoice_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_airline VALUES("1","1","1","","0","2","5");
INSERT INTO invoice_package_airline VALUES("42","21","42","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("41","21","41","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("40","21","40","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("39","21","39","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("38","21","38","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("21","9","21","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("22","10","22","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("23","11","23","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("24","12","24","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("25","13","25","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("27","15","27","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("28","15","28","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("29","16","29","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("30","16","30","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("37","20","37","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("36","20","36","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("33","18","33","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("34","19","34","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("35","19","35","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("43","21","43","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("46","24","45","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("47","24","46","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("49","26","48","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("50","26","49","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("51","27","50","","350000","2","6");
INSERT INTO invoice_package_airline VALUES("52","27","51","","350000","2","6");

DROP TABLE IF EXISTS invoice_package_hotel;
CREATE TABLE `invoice_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `roomType` varchar(100) NOT NULL,
  `bedStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_passenger;
CREATE TABLE `invoice_package_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `packageId` int(11) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` date DEFAULT NULL,
  `age` int(11) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `birthPlace` varchar(100) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(50) NOT NULL,
  `dateOfIssue` date DEFAULT NULL,
  `validUpTo` date DEFAULT NULL,
  `issuePlace` varchar(100) NOT NULL,
  `mofa` varchar(50) NOT NULL,
  `emofa` varchar(50) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_passenger VALUES("1","1","1","","","","0","","","","","","","","","","","0","2","5");
INSERT INTO invoice_package_passenger VALUES("42","21","1","KHADEEJA ORAVIL","F","1951-01-01","66","Indian","kerala","","","L6381798","2013-12-17","2023-12-16","MALAPPURAM","","","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("41","21","1","MARIYAKUTTY CHEMBALAKKARA","F","1963-06-01","53","Indian","kerala","","","K9735468","2013-03-13","2023-03-12","MALAPPURAM","","","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("40","21","1","MOOSSA KUMBALATH VALAPPIL","M","1965-05-02","51","Indian","kerala","","","G7880366","2009-03-18","2019-03-17","ABUDHABI","","","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("38","21","1","MUSTHAFA HASSAN","M","1962-05-16","54","Indian","kerala","","","K8746279","2013-01-06","","05-01-2023","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("39","21","1","NASEEMA MUSTHAFA","F","1974-03-12","42","Indian","kerala","HUSBAND","","L4214724","2013-10-09","2023-10-08","MALAPPURAM","","","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("21","9","1","KUNHAYI PANNIKOTATH","M","1955-11-10","61","indian","kerala","","","M5807886","2015-01-28","2025-01-27","KOZHIKODE","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("22","10","1","NAFEESA MOYTHEENKOYA","F","1943-01-01","74","indian","kerala","","","L1578471","2013-04-25","2023-04-24","KOZHIKODE","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("23","11","1","SUHARA MAMBATH","F","1950-01-01","67","indian","kerala","","","N2162499","","","","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("24","12","1","PATHUMMA THURUTHIYIL MEETHAL","F","1960-05-10","56","indian","kerala","","","J8212967","2011-06-09","2021-06-08","KOZHIKODE","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("25","13","1","MAYIN KUTTY THONIKKADAVATH","M","1958-10-21","58","indian","kerala","","","N8565859","2016-03-09","2026-03-08","MALAPPURAM","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("27","15","1","KADEEJA MADATHIL","F","1955-01-01","62","Indian","kerala","","","K6698357","2012-11-22","2022-11-21","KOZHIKODE","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("28","15","1","FATHIMA KUNJALAVI","F","1946-01-01","71","indian","kerala","","","K6473146","2013-04-05","2023-04-04","KOZHIKODE","","","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("29","16","1","SUBAIDA VELLARATHODI","F","1968-05-31","48","indian","kerala","","","P0919902","2016-06-15","2026-06-14","MALAPPURAM","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("30","16","1","KUNJEEVI VELLARATHODI","F","1946-01-01","71","indian","kerala","","","K5368916","2012-10-29","2022-10-28","MALAPPURAM","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("37","20","1","NILAKKADA VATH HYRUNNEESA CHEMMEKKOTTU","F","1976-05-23","40","Indian","kerala","HUSBAND","","N0419816","2015-12-15","2025-12-14","KUWAIT","","","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("36","20","1","MUSTHAFA KAYANIKARA ","M","1970-06-01","46","Indian","kerala","","","N0419931","2015-12-16","2025-12-15","MALAPPURAM","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("33","18","1","SULAIMAN CHAMAKKALAYI MUHAMMED KUTTY","M","1967-01-15","50","Indian","kerala","","","N7797851","2016-02-09","2026-02-08","KOZHIKODE","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("34","19","1","ABDUL RASHEED MANNARATTIL","M","1976-04-10","40","indian","kerala","","","G1782208","2007-04-04","2017-04-03","KOZHIKODE","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("35","19","1","NASREENA ABDUL RASHEED","F","1987-10-08","29","indian","kerala","","","K9864596","2013-03-06","2023-03-05","KOZHIKODE","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("43","21","1","KUNCHIMOHAMED HASSAN CHEMBALAKKARA","M","1958-05-10","58","indian","kerala","","","H6227189","2009-10-08","2019-10-07","MALAPPURAM","","","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("45","24","1","UMMER JOIPORE ABBASU","M","1953-03-22","63","Indian","KONDANGERI, KARNATAKA","","","J9342964","2011-10-21","2021-10-20","BANGALORE","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("46","24","1","AISAMMA JOIPORE UMMER","F","1962-05-15","54","indian","KONDANGERI, KARNATAKA","HUSBAND","","K1591962","2011-10-25","2021-10-24","BANGALORE","","","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("48","26","1","ABDURAHIMAN PUTHIYOTTIL","M","1964-02-02","53","Indian","KAKKOOR, KERALA","","","N9064334","2016-04-01","2026-03-31","KOZHIKODE","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("49","26","1","KHADEEJA MADATHIL KUNNUMMAL","F","1967-04-19","49","indian","EKAROOL, KERALA","HUSBAND","","J9653203","2011-10-19","2021-10-18","KOZHIKODE","","","700000","2","6");
INSERT INTO invoice_package_passenger VALUES("50","27","1","KOYA KAIKKOLAN PARAKKAL","M","1958-04-02","58","indian","VAKAYAD, KERALA","","","N9064739","2016-04-01","2026-03-31","KOZHIKODE","","","350000","2","6");
INSERT INTO invoice_package_passenger VALUES("51","27","1","SAFIYA PARAYULLATHIL MEETHAL","F","1970-05-20","46","indian","KOLATHUR, KERALA","HUSBAND","","L3666227","2013-10-14","2023-10-13","KOZHIKODE","","","350000","2","6");

DROP TABLE IF EXISTS invoice_package_transportation;
CREATE TABLE `invoice_package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `seatStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_package_visa;
CREATE TABLE `invoice_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS invoice_transport;
CREATE TABLE `invoice_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `serviceProviderId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `totalDays` int(11) NOT NULL,
  `vehicleType` varchar(100) NOT NULL,
  `kmAllowed` float NOT NULL,
  `arrivalDetails` text NOT NULL,
  `pickUpFrom` varchar(100) NOT NULL,
  `pickUpTime` varchar(20) NOT NULL,
  `DepartureDetails` text NOT NULL,
  `dropAt` varchar(100) NOT NULL,
  `dropTime` varchar(20) NOT NULL,
  `route` text NOT NULL,
  `inclusive` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS invoice_visa;
CREATE TABLE `invoice_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `mainOtherCharge` double NOT NULL,
  `discount` varchar(50) NOT NULL,
  `netAmount` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
INSERT INTO invoice_visa VALUES("1","1","2016-07-16","40","0","0","15300","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("2","2","2016-07-16","40","0","0","44700","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("3","3","2016-07-16","45","0","0","15200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("4","4","2016-07-16","40","0","0","14900","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("5","5","2016-07-16","40","0","0","14900","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("6","6","2016-07-16","46","0","0","5250","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("7","7","2016-07-18","40","0","0","14900","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("8","8","2016-07-18","40","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("10","9","2016-07-18","60","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("11","10","2016-07-18","45","0","0","15500","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("12","11","2016-07-18","63","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("13","12","2016-07-18","40","0","0","29800","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("14","13","2016-07-18","40","0","0","29800","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("15","14","2016-07-18","45","0","0","15200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("16","15","2016-07-18","40","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("17","16","2016-07-18","64","0","0","17400","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("19","17","2016-07-19","46","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("20","18","2016-07-20","60","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("21","19","2016-07-19","60","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("23","20","2016-07-19","65","0","0","5200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("24","21","2016-07-19","46","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("25","22","2016-07-20","63","0","0","5200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("26","23","2016-07-20","65","0","0","30200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("27","24","2016-07-20","63","0","0","11900","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("29","25","2016-07-20","63","0","0","5200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("30","26","2016-07-20","63","0","0","5200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("31","27","2016-07-20","40","0","0","28900","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("32","28","2016-07-19","40","0","0","300","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("33","29","2016-07-20","60","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("34","30","2016-07-22","63","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("35","31","2016-07-20","63","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("36","32","2016-07-22","40","0","0","14700","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("37","33","2016-07-20","40","0","0","14700","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("38","34","2016-07-20","40","0","0","15000","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("40","35","2016-07-20","46","0","0","18200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("41","36","2016-07-20","46","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("42","37","2016-07-20","63","0","0","5200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("43","38","2016-07-20","63","0","0","5200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("44","39","2016-07-22","40","0","0","14700","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("45","40","2016-07-21","40","0","0","14700","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("46","41","2016-07-22","45","0","0","16300","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("47","42","2016-07-21","45","0","0","16300","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("48","43","2016-07-21","40","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("49","44","2016-07-21","66","0","0","14800","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("50","45","2016-07-21","40","0","0","14700","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("51","46","2016-07-22","64","0","0","17400","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("54","47","2016-07-21","64","0","0","17400","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("53","48","2016-07-22","40","0","0","300","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("55","49","2016-07-22","66","0","0","14800","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("56","50","2016-07-22","69","0","0","15300","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("57","51","2016-07-22","45","0","0","5200","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("58","52","2016-07-22","66","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("59","53","2016-07-22","66","0","0","34400","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("60","54","2016-07-22","45","0","0","15000","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("61","55","2016-07-22","40","0","0","600","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("62","56","2016-07-22","40","0","0","14700","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("63","57","2016-07-22","40","0","0","15000","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("64","58","2016-07-23","65","0","0","14600","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("65","59","2016-07-23","65","0","0","14600","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("66","60","2016-07-23","65","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("67","61","2016-07-23","45","0","0","22500","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("68","62","2016-07-23","40","0","0","34600","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("69","63","2016-07-23","40","0","0","14700","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("70","64","2016-07-23","66","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("71","65","2016-07-23","40","0","0","14700","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("73","66","2016-07-23","70","0","0","15100","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("75","67","2016-07-23","63","0","0","300","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("76","68","2016-07-23","40","0","0","300","","","Pending","2","5");
INSERT INTO invoice_visa VALUES("77","69","2016-07-23","45","0","0","16100","","","Pending","2","5");

DROP TABLE IF EXISTS login;
CREATE TABLE `login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
INSERT INTO login VALUES("1","0","admin","81dc9bdb52d04dc20036dbd8313ed055","Admin","Active");
INSERT INTO login VALUES("3","2","sidheeq","ba856b0417652289251c866c3e22824e","Branch","Active");
INSERT INTO login VALUES("5","2","irfad","11d0a18c25c1f6b40791a75f6bde5a97","Staff","Active");
INSERT INTO login VALUES("6","3","sha","ca794fb2d950acf25c964ecc35f2d7e2","Staff","Active");
INSERT INTO login VALUES("7","3","a","0cc175b9c0f1b6a831c399e269772661","Branch","Blocked");
INSERT INTO login VALUES("35","29","PATTAMBI","dd567e84b397e1f04243fe6240e3cdf5","Branch","");
INSERT INTO login VALUES("36","6","SAIFU","efdd5286109aab77dd1e3260ff2d167b","Staff","");

DROP TABLE IF EXISTS misc_narration;
CREATE TABLE `misc_narration` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `narration` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS package;
CREATE TABLE `package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageName` varchar(200) NOT NULL,
  `staff` int(11) NOT NULL,
  `leader` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `place` varchar(100) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `tds` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `remark` varchar(100) NOT NULL,
  `services` varchar(100) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `confirm` int(11) NOT NULL,
  `discount` float NOT NULL,
  `extra` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO package VALUES("1","Haj","2","Ameer","0","2016-08-10","haj 2016","40","0","0","0","","airline","350000","350000","50000","2","3","0","0","0");
INSERT INTO package VALUES("2","Haj 2016 B","3","Ameer","0","2016-08-10","","","0","0","0","","airline,hotel,visa,transportation","230000","225000","145000","2","5","0","0","0");

DROP TABLE IF EXISTS package_airline;
CREATE TABLE `package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `yq` float NOT NULL DEFAULT '0',
  `procCharge` float NOT NULL DEFAULT '0',
  `tax` float NOT NULL DEFAULT '0',
  `adultCharge` float NOT NULL,
  `childCharge` float NOT NULL,
  `infantCharge` float NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `packageId` (`packageId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO package_airline VALUES("1","1","0","0","0","350000","350000","50000","350000","350000","50000","","2","3");
INSERT INTO package_airline VALUES("4","2","0","0","0","85000","80000","20000","85000","80000","20000","","2","5");

DROP TABLE IF EXISTS package_airline_flights;
CREATE TABLE `package_airline_flights` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_airlineId` int(11) NOT NULL,
  `airline` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `sectorFrom` varchar(200) NOT NULL,
  `sectorTo` varchar(200) NOT NULL,
  `jDate` date NOT NULL,
  `class` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO package_airline_flights VALUES("1","1","1","","000","ccj","jed","2016-08-10","","2","3");
INSERT INTO package_airline_flights VALUES("4","2","4","1","SV000","ccj","jed","2016-08-10","e","2","5");

DROP TABLE IF EXISTS package_hotel;
CREATE TABLE `package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `hotelId` int(11) NOT NULL,
  `checkinDate` date DEFAULT NULL,
  `checkoutDate` date DEFAULT NULL,
  `noOfDays` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO package_hotel VALUES("3","2","1","2016-08-05","2016-08-19","14","2","5","0");

DROP TABLE IF EXISTS package_room_type;
CREATE TABLE `package_room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_hotelId` int(11) NOT NULL,
  `roomId` int(11) NOT NULL,
  `rate` float NOT NULL,
  `otherCharge` float NOT NULL,
  `total` float NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
INSERT INTO package_room_type VALUES("1","2","3","1","0","0","0","0","2","5");
INSERT INTO package_room_type VALUES("2","2","3","2","0","0","0","0","2","5");
INSERT INTO package_room_type VALUES("3","2","3","3","0","0","0","0","2","5");
INSERT INTO package_room_type VALUES("4","2","3","4","0","0","0","0","2","5");
INSERT INTO package_room_type VALUES("5","2","3","5","0","0","0","0","2","5");
INSERT INTO package_room_type VALUES("6","2","3","6","0","0","0","0","2","5");
INSERT INTO package_room_type VALUES("7","2","3","7","0","0","0","0","2","5");
INSERT INTO package_room_type VALUES("8","2","3","8","0","0","0","0","2","5");

DROP TABLE IF EXISTS package_transportation;
CREATE TABLE `package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `vehicle` int(11) NOT NULL,
  `seatCharge` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO package_transportation VALUES("3","2","1","20000","0","0","0","2","5");

DROP TABLE IF EXISTS package_visa;
CREATE TABLE `package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `visaFor` varchar(100) NOT NULL,
  `visaFee` float NOT NULL DEFAULT '0',
  `vfs` float NOT NULL DEFAULT '0',
  `ddCharge` float NOT NULL DEFAULT '0',
  `serviceCharge` float NOT NULL DEFAULT '0',
  `otherCharge` float NOT NULL DEFAULT '0',
  `courierCharge` float NOT NULL DEFAULT '0',
  `total` float NOT NULL DEFAULT '0',
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO package_visa VALUES("3","2","2","125000","0","0","0","0","0","125000","0","0","2","5");

DROP TABLE IF EXISTS passenger;
CREATE TABLE `passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationship` varchar(30) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `mofa` varchar(50) NOT NULL,
  `emofa` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
INSERT INTO passenger VALUES("1","40","Bismi Ramshad","0000-00-00","0","F","","","Indian","","M2551864","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("2","40","Shamnas","0000-00-00","0","M","","","Indian","","P0917864","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("3","40","Shanaza","0000-00-00","0","F","","","Indian","","N9972437","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("4","40","Suhail","0000-00-00","0","M","","","Indian","","K5983739","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("5","40","Ashitha Moideen Kutty","0000-00-00","0","F","","","Indian","","M36831335","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("6","40","Fathima Isha","0000-00-00","0","F","","","Indian","","L6574162","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("7","40","Akhila Meenakshi","0000-00-00","0","F","","","Indian","","M8417567","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("8","40","Asain Kumar","0000-00-00","0","M","","","Indian","","K6166640","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("9","40","Sruthi","0000-00-00","0","M","","","Indian","","N6776784","0000-00-00","0000-00-00","","","","2","5");
INSERT INTO passenger VALUES("10","40","Princy Mol","2016-07-11","0","M","","","Indian","","L2985948","0000-00-00","0000-00-00","","","","2","6");

DROP TABLE IF EXISTS purchase_airline;
CREATE TABLE `purchase_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_hotel;
CREATE TABLE `purchase_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_misc;
CREATE TABLE `purchase_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_pac_airline;
CREATE TABLE `purchase_pac_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO purchase_pac_airline VALUES("1","1","0","0","0","0","2","3");
INSERT INTO purchase_pac_airline VALUES("4","2","0","0","0","0","2","5");

DROP TABLE IF EXISTS purchase_package_airline;
CREATE TABLE `purchase_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date DEFAULT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL DEFAULT '0',
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_hotel;
CREATE TABLE `purchase_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_transport;
CREATE TABLE `purchase_package_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_package_visa;
CREATE TABLE `purchase_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_transport;
CREATE TABLE `purchase_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS purchase_visa;
CREATE TABLE `purchase_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
INSERT INTO purchase_visa VALUES("3","3","2016-07-16","2016-07-16","43","7","4","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("4","4","2016-07-16","2016-07-16","43","7","5","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("5","5","2016-07-16","2016-07-16","42","7","6","5150","0","0","0","0","0","0","0","5150","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("6","6","2016-07-18","2016-07-18","43","7","7","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("7","7","2016-07-18","2016-07-18","41","7","8","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("8","8","2016-07-18","2016-07-18","43","7","9","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("9","9","2016-07-18","2016-07-18","43","7","10","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("10","10","2016-07-18","2016-07-18","43","7","11","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("11","11","2016-07-18","2016-07-18","43","7","12","29000","0","0","0","0","0","0","0","29000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("12","12","2016-07-18","2016-07-18","43","7","13","29000","0","0","0","0","0","0","0","29000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("13","13","2016-07-18","2016-07-18","43","7","14","14500","0","0","0","0","0","0","0","14500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("14","14","2016-07-18","2016-07-18","41","7","15","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("15","15","2016-07-18","2016-07-18","44","7","16","17000","0","0","0","0","0","0","0","17000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("16","16","2016-07-19","2016-07-20","41","7","17","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("17","17","2016-07-19","2016-07-20","41","7","19","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("18","18","2016-07-19","2016-07-20","42","7","20","5150","0","0","0","0","0","0","0","5150","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("19","19","2016-07-19","2016-07-20","41","7","21","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("20","20","2016-07-20","2016-07-20","42","7","22","5150","0","0","0","0","0","0","0","5150","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("21","21","2016-07-20","2016-07-21","43","7","24","5700","0","0","0","0","0","0","0","5700","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("22","22","2016-07-20","2016-07-22","42","7","25","5150","0","0","0","0","0","0","0","5150","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("23","23","2016-07-20","2016-07-22","42","7","26","5150","0","0","0","0","0","0","0","5150","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("24","24","2016-07-20","2016-07-22","43","7","27","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("25","25","2016-07-19","2016-07-22","43","7","28","300","0","0","0","0","0","0","0","300","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("26","26","2016-07-20","2016-07-22","43","7","29","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("27","27","2016-07-20","2016-07-22","43","7","31","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("28","28","2016-07-20","2016-07-22","43","7","33","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("29","29","2016-07-20","2016-07-22","43","7","34","14900","0","0","0","0","0","0","0","14900","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("30","30","2016-07-20","2016-07-22","44","7","35","17800","0","0","0","0","0","0","0","17800","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("31","31","2016-07-20","2016-07-22","41","7","36","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("32","32","2016-07-20","2016-07-22","42","7","37","5150","0","0","0","0","0","0","0","5150","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("33","33","2016-07-20","2016-07-22","42","7","38","5150","0","0","0","0","0","0","0","5150","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("34","34","2016-07-21","2016-07-22","43","7","40","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("35","35","2016-07-21","2016-07-22","42","7","42","15400","0","0","0","0","0","0","0","15400","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("36","36","2016-07-21","2016-07-22","41","7","43","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("37","37","2016-07-21","2016-07-22","43","7","44","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("38","38","2016-07-21","2016-07-22","43","7","45","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("39","39","2016-07-22","2016-07-22","67","7","47","16800","0","0","0","0","0","0","0","16800","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("40","40","2016-07-22","2016-07-22","42","7","48","250","0","0","0","0","0","0","0","250","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("41","41","2016-07-22","2016-07-22","43","7","49","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("42","42","2016-07-22","2016-07-22","43","7","50","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("43","43","2016-07-22","2016-07-22","42","7","51","5150","0","0","0","0","0","0","0","5150","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("44","44","2016-07-22","2016-07-22","41","7","52","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("45","45","2016-07-22","2016-07-22","67","7","53","17000","0","0","0","0","0","0","0","17000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("46","46","2016-07-22","2016-07-22","43","7","54","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("47","47","2016-07-22","2016-07-22","43","7","55","500","0","0","0","0","0","0","0","500","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("48","48","2016-07-22","2016-07-22","43","7","56","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("49","49","2016-07-22","2016-07-22","43","7","57","14900","0","0","0","0","0","0","0","14900","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("50","50","2016-07-23","2016-07-23","43","7","58","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("51","51","2016-07-23","2016-07-23","43","7","59","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("52","52","2016-07-23","2016-07-23","41","7","60","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("53","53","2016-07-23","2016-07-23","43","7","61","21000","0","0","0","0","0","0","0","21000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("54","54","2016-07-23","2016-07-23","44","7","62","34000","0","0","0","0","0","0","0","34000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("55","55","2016-07-23","2016-07-23","43","7","63","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("56","56","2016-07-23","2016-07-23","41","7","64","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("57","57","2016-07-23","2016-07-23","43","7","65","14200","0","0","0","0","0","0","0","14200","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("58","58","2016-07-23","2016-07-23","41","7","66","15000","0","0","0","0","0","0","0","15000","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("59","59","2016-07-23","2016-07-23","71","7","67","250","0","0","0","0","0","0","0","250","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("60","60","2016-07-23","2016-07-23","42","7","68","300","0","0","0","0","0","0","0","300","","","0","0","2","5");
INSERT INTO purchase_visa VALUES("61","61","2016-07-23","2016-07-23","42","7","69","15400","0","0","0","0","0","0","0","15400","","","0","0","2","5");

DROP TABLE IF EXISTS room_type;
CREATE TABLE `room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `roomType` varchar(30) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
INSERT INTO room_type VALUES("1","Sharing","0","1");
INSERT INTO room_type VALUES("2","Twin","0","1");
INSERT INTO room_type VALUES("3","Triple","0","1");
INSERT INTO room_type VALUES("4","Four Bed","0","1");
INSERT INTO room_type VALUES("5","Five Bed","0","1");
INSERT INTO room_type VALUES("6","Six Bed","0","1");
INSERT INTO room_type VALUES("7","Child With Bed","0","1");
INSERT INTO room_type VALUES("8","Child Without Bed","0","1");

DROP TABLE IF EXISTS staff;
CREATE TABLE `staff` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffName` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `userType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
INSERT INTO staff VALUES("2","Irfad","9746915105","irfadamanath1@gmail.","staf","2","3");
INSERT INTO staff VALUES("3","Arshak","8089684701","muhammedsha87@gmail.","","2","3");
INSERT INTO staff VALUES("6","Saifu","0","","","29","35");

DROP TABLE IF EXISTS transaction;
CREATE TABLE `transaction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `fromLedger` int(11) NOT NULL,
  `toLedger` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(100) NOT NULL,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `remark` text NOT NULL,
  `addedDate` date NOT NULL,
  `transactionDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `referenceNo` int(11) NOT NULL,
  `details` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=657 DEFAULT CHARSET=latin1;
INSERT INTO transaction VALUES("1","40","6","1","Visa Receivable","0","15300","Visa Receivable : 1","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV1, submissionDate:16-07-2016, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("2","6","40","1","Visa Receivable","15300","0","Visa Receivable : 1","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV1, submissionDate:16-07-2016, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("3","40","6","2","Visa Receivable","0","44700","Visa Receivable : 2","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV2, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("4","6","40","2","Visa Receivable","44700","0","Visa Receivable : 2","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV2, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("5","41","7","1","Visa Payable","44400","0","","2016-07-16","2016-07-16","5","2","0","INV#1,sup:41,:against:2");
INSERT INTO transaction VALUES("6","7","41","1","Visa Payable","0","44400","","2016-07-16","2016-07-16","5","2","0","INV#1,sup:41,:against:2");
INSERT INTO transaction VALUES("7","45","6","3","Visa Receivable","0","15200","Visa Receivable : 3","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV3, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("8","6","45","3","Visa Receivable","15200","0","Visa Receivable : 3","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV3, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("9","41","7","2","Visa Payable","15000","0","","2016-07-16","2016-07-16","5","2","0","INV#2,sup:41,:against:3");
INSERT INTO transaction VALUES("10","7","41","2","Visa Payable","0","15000","","2016-07-16","2016-07-16","5","2","0","INV#2,sup:41,:against:3");
INSERT INTO transaction VALUES("11","40","6","4","Visa Receivable","0","14900","Visa Receivable : 4","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV4, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("12","6","40","4","Visa Receivable","14900","0","Visa Receivable : 4","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV4, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("13","43","7","3","Visa Payable","14500","0","","2016-07-16","2016-07-16","5","2","0","INV#3,sup:43,:against:4");
INSERT INTO transaction VALUES("14","7","43","3","Visa Payable","0","14500","","2016-07-16","2016-07-16","5","2","0","INV#3,sup:43,:against:4");
INSERT INTO transaction VALUES("15","40","6","5","Visa Receivable","0","14900","Visa Receivable : 5","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV5, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("16","6","40","5","Visa Receivable","14900","0","Visa Receivable : 5","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV5, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("17","43","7","4","Visa Payable","14500","0","","2016-07-16","2016-07-16","5","2","0","INV#4,sup:43,:against:5");
INSERT INTO transaction VALUES("18","7","43","4","Visa Payable","0","14500","","2016-07-16","2016-07-16","5","2","0","INV#4,sup:43,:against:5");
INSERT INTO transaction VALUES("19","46","6","6","Visa Receivable","0","5250","Visa Receivable : 6","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV6, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("20","6","46","6","Visa Receivable","5250","0","Visa Receivable : 6","2016-07-16","2016-07-16","5","2","0","Visa Receivable (IV), INV#IV6, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("21","42","7","5","Visa Payable","5150","0","","2016-07-16","2016-07-16","5","2","0","INV#5,sup:42,:against:6");
INSERT INTO transaction VALUES("22","7","42","5","Visa Payable","0","5150","","2016-07-16","2016-07-16","5","2","0","INV#5,sup:42,:against:6");
INSERT INTO transaction VALUES("23","47","8","1","Package Receivable","0","0","Invoice Package: 1","2016-07-16","2016-07-16","5","2","1","INV#IP1,Pac:Haj ,Pax: ,By:Irfad");
INSERT INTO transaction VALUES("24","8","47","1","Package Receivable","0","0","Invoice Package: 1","2016-07-16","2016-07-16","5","2","1","INV#IP1,Pac:Haj ,Pax: ,By:Irfad");
INSERT INTO transaction VALUES("29","40","48","1","Bank Receipts","59600","0","","2016-07-18","2016-07-18","5","2","0","INV#BR1,by:Irfad,from:CREATIVE KDNGLR 2");
INSERT INTO transaction VALUES("30","48","40","1","Bank Receipts","0","59600","","2016-07-18","2016-07-18","5","2","0","INV#BR1,by:Irfad,from:CREATIVE KDNGLR 2");
INSERT INTO transaction VALUES("33","40","2","2","Cash Receipts","0","0","","2016-07-18","2016-07-18","5","2","0","Cash Receipts(CR), INV#CR, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("34","2","40","2","Cash Receipts","0","0","","2016-07-18","2016-07-18","5","2","0","Cash Receipts(CR), INV#CR, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("49","52","8","3","Package Receivable","0","350000","Invoice Package: 3","2016-07-18","2016-07-18","6","2","3","INV#IP3,Pac:Haj ,Pax:KUNHAYI PANNIKOTATH ,By:Arshak");
INSERT INTO transaction VALUES("50","8","52","3","Package Receivable","350000","0","Invoice Package: 3","2016-07-18","2016-07-18","6","2","3","INV#IP3,Pac:Haj ,Pax:KUNHAYI PANNIKOTATH ,By:Arshak");
INSERT INTO transaction VALUES("51","53","8","4","Package Receivable","0","350000","Invoice Package: 4","2016-07-18","2016-07-18","6","2","4","INV#IP4,Pac:Haj ,Pax:NAFEESA MOYTHEENKOYA ,By:Arshak");
INSERT INTO transaction VALUES("52","8","53","4","Package Receivable","350000","0","Invoice Package: 4","2016-07-18","2016-07-18","6","2","4","INV#IP4,Pac:Haj ,Pax:NAFEESA MOYTHEENKOYA ,By:Arshak");
INSERT INTO transaction VALUES("53","54","8","5","Package Receivable","0","350000","Invoice Package: 5","2016-07-18","2016-07-18","6","2","5","INV#IP5,Pac:Haj ,Pax:SUHARA MAMBATH ,By:Arshak");
INSERT INTO transaction VALUES("54","8","54","5","Package Receivable","350000","0","Invoice Package: 5","2016-07-18","2016-07-18","6","2","5","INV#IP5,Pac:Haj ,Pax:SUHARA MAMBATH ,By:Arshak");
INSERT INTO transaction VALUES("55","55","8","6","Package Receivable","0","350000","Invoice Package: 6","2016-07-18","2016-07-18","6","2","6","INV#IP6,Pac:Haj ,Pax:PATHUMMA THURUTHIYIL MEETHAL ,By:Arshak");
INSERT INTO transaction VALUES("56","8","55","6","Package Receivable","350000","0","Invoice Package: 6","2016-07-18","2016-07-18","6","2","6","INV#IP6,Pac:Haj ,Pax:PATHUMMA THURUTHIYIL MEETHAL ,By:Arshak");
INSERT INTO transaction VALUES("57","56","8","7","Package Receivable","0","350000","Invoice Package: 7","2016-07-18","2016-07-18","6","2","7","INV#IP7,Pac:Haj ,Pax:MAYIN KUTTY THONIKKADAVATH ,By:Arshak");
INSERT INTO transaction VALUES("58","8","56","7","Package Receivable","350000","0","Invoice Package: 7","2016-07-18","2016-07-18","6","2","7","INV#IP7,Pac:Haj ,Pax:MAYIN KUTTY THONIKKADAVATH ,By:Arshak");
INSERT INTO transaction VALUES("61","57","8","8","Package Receivable","0","0","Invoice Package: 8","2016-07-18","2016-07-18","6","2","8","INV#IP8,Pac:Haj ,Pax:KADEEJA MADATHIL,FATHIMA KUNJALAVI ,By:Arshak");
INSERT INTO transaction VALUES("62","8","57","8","Package Receivable","0","0","Invoice Package: 8","2016-07-18","2016-07-18","6","2","8","INV#IP8,Pac:Haj ,Pax:KADEEJA MADATHIL,FATHIMA KUNJALAVI ,By:Arshak");
INSERT INTO transaction VALUES("63","58","8","9","Package Receivable","0","700000","Invoice Package: 9","2016-07-18","2016-07-18","6","2","9","INV#IP9,Pac:Haj ,Pax:SUBAIDA VELLARATHODI,KUNJEEVI VELLARATHODI ,By:Arshak");
INSERT INTO transaction VALUES("64","8","58","9","Package Receivable","700000","0","Invoice Package: 9","2016-07-18","2016-07-18","6","2","9","INV#IP9,Pac:Haj ,Pax:SUBAIDA VELLARATHODI,KUNJEEVI VELLARATHODI ,By:Arshak");
INSERT INTO transaction VALUES("65","40","6","7","Visa Receivable","0","14900","Visa Receivable : 7","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV7, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("66","6","40","7","Visa Receivable","14900","0","Visa Receivable : 7","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV7, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("67","43","7","6","Visa Payable","14500","0","","2016-07-18","2016-07-18","5","2","0","INV#6,sup:43,:against:7");
INSERT INTO transaction VALUES("68","7","43","6","Visa Payable","0","14500","","2016-07-18","2016-07-18","5","2","0","INV#6,sup:43,:against:7");
INSERT INTO transaction VALUES("69","40","6","8","Visa Receivable","0","15100","Visa Receivable : 8","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV8, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("70","6","40","8","Visa Receivable","15100","0","Visa Receivable : 8","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV8, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("71","41","7","7","Visa Payable","15000","0","","2016-07-18","2016-07-18","5","2","0","INV#7,sup:41,:against:8");
INSERT INTO transaction VALUES("72","7","41","7","Visa Payable","0","15000","","2016-07-18","2016-07-18","5","2","0","INV#7,sup:41,:against:8");
INSERT INTO transaction VALUES("79","60","6","9","Visa Receivable","0","15100","Visa Receivable : 9","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV9, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("80","6","60","9","Visa Receivable","15100","0","Visa Receivable : 9","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV9, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("81","43","7","8","Visa Payable","14500","0","","2016-07-18","2016-07-18","5","2","0","INV#8,sup:43,:against:9");
INSERT INTO transaction VALUES("82","7","43","8","Visa Payable","0","14500","","2016-07-18","2016-07-18","5","2","0","INV#8,sup:43,:against:9");
INSERT INTO transaction VALUES("83","61","8","11","Package Receivable","0","350000","Invoice Package: 11","2016-07-18","2016-07-18","6","2","11","INV#IP11,Pac:Haj ,Pax:SULAIMAN CHAMAKKALAYI MUHAMMED KUTTY ,By:Arshak");
INSERT INTO transaction VALUES("84","8","61","11","Package Receivable","350000","0","Invoice Package: 11","2016-07-18","2016-07-18","6","2","11","INV#IP11,Pac:Haj ,Pax:SULAIMAN CHAMAKKALAYI MUHAMMED KUTTY ,By:Arshak");
INSERT INTO transaction VALUES("85","45","6","10","Visa Receivable","0","15500","Visa Receivable : 10","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV10, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("86","6","45","10","Visa Receivable","15500","0","Visa Receivable : 10","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV10, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("87","43","7","9","Visa Payable","14500","0","","2016-07-18","2016-07-18","5","2","0","INV#9,sup:43,:against:10");
INSERT INTO transaction VALUES("88","7","43","9","Visa Payable","0","14500","","2016-07-18","2016-07-18","5","2","0","INV#9,sup:43,:against:10");
INSERT INTO transaction VALUES("89","62","8","12","Package Receivable","0","700000","Invoice Package: 12","2016-07-18","2016-07-18","6","2","12","INV#IP12,Pac:Haj ,Pax:ABDUL RASHEED MANNARATTIL,NASREENA ABDUL RASHEED ,By:Arshak");
INSERT INTO transaction VALUES("90","8","62","12","Package Receivable","700000","0","Invoice Package: 12","2016-07-18","2016-07-18","6","2","12","INV#IP12,Pac:Haj ,Pax:ABDUL RASHEED MANNARATTIL,NASREENA ABDUL RASHEED ,By:Arshak");
INSERT INTO transaction VALUES("91","59","8","13","Package Receivable","0","0","Invoice Package: 13","2016-07-18","2016-07-18","6","2","13","INV#IP13,Pac:Haj ,Pax:MUSTHAFA KAYANIKARA ,NILAKKADA VATH HYRUNNEESA CHEMMEKKOTTU ,By:Arshak");
INSERT INTO transaction VALUES("92","8","59","13","Package Receivable","0","0","Invoice Package: 13","2016-07-18","2016-07-18","6","2","13","INV#IP13,Pac:Haj ,Pax:MUSTHAFA KAYANIKARA ,NILAKKADA VATH HYRUNNEESA CHEMMEKKOTTU ,By:Arshak");
INSERT INTO transaction VALUES("93","63","6","11","Visa Receivable","0","15100","Visa Receivable : 11","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV11, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("94","6","63","11","Visa Receivable","15100","0","Visa Receivable : 11","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV11, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("95","43","7","10","Visa Payable","14500","0","","2016-07-18","2016-07-18","5","2","0","INV#10,sup:43,:against:11");
INSERT INTO transaction VALUES("96","7","43","10","Visa Payable","0","14500","","2016-07-18","2016-07-18","5","2","0","INV#10,sup:43,:against:11");
INSERT INTO transaction VALUES("97","40","6","12","Visa Receivable","0","29800","Visa Receivable : 12","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV12, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("98","6","40","12","Visa Receivable","29800","0","Visa Receivable : 12","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV12, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("99","43","7","11","Visa Payable","29000","0","","2016-07-18","2016-07-18","5","2","0","INV#11,sup:43,:against:12");
INSERT INTO transaction VALUES("100","7","43","11","Visa Payable","0","29000","","2016-07-18","2016-07-18","5","2","0","INV#11,sup:43,:against:12");
INSERT INTO transaction VALUES("101","40","6","13","Visa Receivable","0","29800","Visa Receivable : 13","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV13, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("102","6","40","13","Visa Receivable","29800","0","Visa Receivable : 13","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV13, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("103","43","7","12","Visa Payable","29000","0","","2016-07-18","2016-07-18","5","2","0","INV#12,sup:43,:against:13");
INSERT INTO transaction VALUES("104","7","43","12","Visa Payable","0","29000","","2016-07-18","2016-07-18","5","2","0","INV#12,sup:43,:against:13");
INSERT INTO transaction VALUES("105","45","6","14","Visa Receivable","0","15200","Visa Receivable : 14","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV14, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("106","6","45","14","Visa Receivable","15200","0","Visa Receivable : 14","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV14, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("107","43","7","13","Visa Payable","14500","0","","2016-07-18","2016-07-18","5","2","0","INV#13,sup:43,:against:14");
INSERT INTO transaction VALUES("108","7","43","13","Visa Payable","0","14500","","2016-07-18","2016-07-18","5","2","0","INV#13,sup:43,:against:14");
INSERT INTO transaction VALUES("109","40","6","15","Visa Receivable","0","15100","Visa Receivable : 15","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV15, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("110","6","40","15","Visa Receivable","15100","0","Visa Receivable : 15","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV15, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("111","41","7","14","Visa Payable","15000","0","","2016-07-18","2016-07-18","5","2","0","INV#14,sup:41,:against:15");
INSERT INTO transaction VALUES("112","7","41","14","Visa Payable","0","15000","","2016-07-18","2016-07-18","5","2","0","INV#14,sup:41,:against:15");
INSERT INTO transaction VALUES("113","64","6","16","Visa Receivable","0","17400","Visa Receivable : 16","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV16, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("114","6","64","16","Visa Receivable","17400","0","Visa Receivable : 16","2016-07-18","2016-07-18","5","2","0","Visa Receivable (IV), INV#IV16, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("115","44","7","15","Visa Payable","17000","0","","2016-07-18","2016-07-18","5","2","0","INV#15,sup:44,:against:16");
INSERT INTO transaction VALUES("116","7","44","15","Visa Payable","0","17000","","2016-07-18","2016-07-18","5","2","0","INV#15,sup:44,:against:16");
INSERT INTO transaction VALUES("117","45","48","2","Bank Receipts","15500","0","","2016-07-19","2016-07-19","5","2","0","INV#BR2,by:Irfad,from:sahad staff");
INSERT INTO transaction VALUES("118","48","45","2","Bank Receipts","0","15500","","2016-07-19","2016-07-19","5","2","0","INV#BR2,by:Irfad,from:sahad staff");
INSERT INTO transaction VALUES("119","47","4","1","Airline Receivable","0","47300","Airline Receivable: 1","2016-07-19","2016-07-19","5","2","0","Airline Receivable(IA), INV#IA1, Pax:Test, Date:27-07-2016, Tkt:8965754664, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("120","4","47","1","Airline Receivable","47300","0","Airline Receivable: 1","2016-07-19","2016-07-19","5","2","0","Airline Receivable(IA), INV#IA1, Pax:Test, Date:27-07-2016, Tkt:8965754664, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("121","44","5","1","Airline Payable","46800","0","","2016-07-19","2016-07-19","5","2","0","INV#1,sup:44,:against:1");
INSERT INTO transaction VALUES("122","5","44","1","Airline Payable","0","46800","","2016-07-19","2016-07-19","5","2","0","INV#1,sup:44,:against:1");
INSERT INTO transaction VALUES("123","50","8","14","Package Receivable","0","0","Invoice Package: 14","2016-07-20","2016-07-18","6","2","14","INV#IP14,Pac:Haj ,Pax:MUSTHAFA HASSAN,NASEEMA MUSTHAFA,MOOSSA KUMBALATH VALAPPIL,MARIYAKUTTY CHEMBALAKKARA,KHADEEJA ORAVIL,KUNCHIMOHAMED HASSAN CHEMBALAKKARA ,By:Arshak");
INSERT INTO transaction VALUES("124","8","50","14","Package Receivable","0","0","Invoice Package: 14","2016-07-20","2016-07-18","6","2","14","INV#IP14,Pac:Haj ,Pax:MUSTHAFA HASSAN,NASEEMA MUSTHAFA,MOOSSA KUMBALATH VALAPPIL,MARIYAKUTTY CHEMBALAKKARA,KHADEEJA ORAVIL,KUNCHIMOHAMED HASSAN CHEMBALAKKARA ,By:Arshak");
INSERT INTO transaction VALUES("129","46","6","17","Visa Receivable","0","15100","Visa Receivable : 17","2016-07-20","2016-07-19","5","2","0","Visa Receivable (IV), INV#IV17, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("130","6","46","17","Visa Receivable","15100","0","Visa Receivable : 17","2016-07-20","2016-07-19","5","2","0","Visa Receivable (IV), INV#IV17, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("131","41","7","16","Visa Payable","15000","0","","2016-07-20","2016-07-19","5","2","0","INV#16,sup:41,:against:17");
INSERT INTO transaction VALUES("132","7","41","16","Visa Payable","0","15000","","2016-07-20","2016-07-19","5","2","0","INV#16,sup:41,:against:17");
INSERT INTO transaction VALUES("133","60","6","18","Visa Receivable","0","15100","Visa Receivable : 18","2016-07-20","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV18, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("134","6","60","18","Visa Receivable","15100","0","Visa Receivable : 18","2016-07-20","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV18, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("135","60","6","19","Visa Receivable","0","15100","Visa Receivable : 19","2016-07-20","2016-07-19","5","2","0","Visa Receivable (IV), INV#IV19, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("136","6","60","19","Visa Receivable","15100","0","Visa Receivable : 19","2016-07-20","2016-07-19","5","2","0","Visa Receivable (IV), INV#IV19, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("137","41","7","17","Visa Payable","15000","0","","2016-07-20","2016-07-19","5","2","0","INV#17,sup:41,:against:19");
INSERT INTO transaction VALUES("138","7","41","17","Visa Payable","0","15000","","2016-07-20","2016-07-19","5","2","0","INV#17,sup:41,:against:19");
INSERT INTO transaction VALUES("143","65","6","20","Visa Receivable","0","5200","Visa Receivable : 20","2016-07-20","2016-07-19","5","2","0","Visa Receivable (IV), INV#IV20, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("144","6","65","20","Visa Receivable","5200","0","Visa Receivable : 20","2016-07-20","2016-07-19","5","2","0","Visa Receivable (IV), INV#IV20, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("145","42","7","18","Visa Payable","5150","0","","2016-07-20","2016-07-19","5","2","0","INV#18,sup:42,:against:20");
INSERT INTO transaction VALUES("146","7","42","18","Visa Payable","0","5150","","2016-07-20","2016-07-19","5","2","0","INV#18,sup:42,:against:20");
INSERT INTO transaction VALUES("147","46","6","21","Visa Receivable","0","15100","Visa Receivable : 21","2016-07-20","2016-07-19","5","2","0","Visa Receivable (IV), INV#IV21, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("148","6","46","21","Visa Receivable","15100","0","Visa Receivable : 21","2016-07-20","2016-07-19","5","2","0","Visa Receivable (IV), INV#IV21, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("149","41","7","19","Visa Payable","15000","0","","2016-07-20","2016-07-19","5","2","0","INV#19,sup:41,:against:21");
INSERT INTO transaction VALUES("150","7","41","19","Visa Payable","0","15000","","2016-07-20","2016-07-19","5","2","0","INV#19,sup:41,:against:21");
INSERT INTO transaction VALUES("151","63","6","22","Visa Receivable","0","5200","Visa Receivable : 22","2016-07-20","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV22, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("152","6","63","22","Visa Receivable","5200","0","Visa Receivable : 22","2016-07-20","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV22, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("153","42","7","20","Visa Payable","5150","0","","2016-07-20","2016-07-20","5","2","0","INV#20,sup:42,:against:22");
INSERT INTO transaction VALUES("154","7","42","20","Visa Payable","0","5150","","2016-07-20","2016-07-20","5","2","0","INV#20,sup:42,:against:22");
INSERT INTO transaction VALUES("155","65","6","23","Visa Receivable","0","30200","Visa Receivable : 23","2016-07-20","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV23, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("156","6","65","23","Visa Receivable","30200","0","Visa Receivable : 23","2016-07-20","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV23, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("159","63","6","24","Visa Receivable","0","11900","Visa Receivable : 24","2016-07-21","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV24, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("160","6","63","24","Visa Receivable","11900","0","Visa Receivable : 24","2016-07-21","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV24, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("161","43","7","21","Visa Payable","5700","0","","2016-07-21","2016-07-20","5","2","0","INV#21,sup:43,:against:24");
INSERT INTO transaction VALUES("162","7","43","21","Visa Payable","0","5700","","2016-07-21","2016-07-20","5","2","0","INV#21,sup:43,:against:24");
INSERT INTO transaction VALUES("167","63","6","25","Visa Receivable","0","5200","Visa Receivable : 25","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV25, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("168","6","63","25","Visa Receivable","5200","0","Visa Receivable : 25","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV25, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("169","42","7","22","Visa Payable","5150","0","","2016-07-22","2016-07-20","5","2","0","INV#22,sup:42,:against:25");
INSERT INTO transaction VALUES("170","7","42","22","Visa Payable","0","5150","","2016-07-22","2016-07-20","5","2","0","INV#22,sup:42,:against:25");
INSERT INTO transaction VALUES("171","63","6","26","Visa Receivable","0","5200","Visa Receivable : 26","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV26, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("172","6","63","26","Visa Receivable","5200","0","Visa Receivable : 26","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV26, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("173","42","7","23","Visa Payable","5150","0","","2016-07-22","2016-07-20","5","2","0","INV#23,sup:42,:against:26");
INSERT INTO transaction VALUES("174","7","42","23","Visa Payable","0","5150","","2016-07-22","2016-07-20","5","2","0","INV#23,sup:42,:against:26");
INSERT INTO transaction VALUES("175","40","6","27","Visa Receivable","0","28900","Visa Receivable : 27","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV27, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("176","6","40","27","Visa Receivable","28900","0","Visa Receivable : 27","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV27, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("177","43","7","24","Visa Payable","14200","0","","2016-07-22","2016-07-20","5","2","0","INV#24,sup:43,:against:27");
INSERT INTO transaction VALUES("178","7","43","24","Visa Payable","0","14200","","2016-07-22","2016-07-20","5","2","0","INV#24,sup:43,:against:27");
INSERT INTO transaction VALUES("179","40","6","28","Visa Receivable","0","300","Visa Receivable : 28","2016-07-22","2016-07-19","5","2","0","Visa Receivable (IV), INV#IV28, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("180","6","40","28","Visa Receivable","300","0","Visa Receivable : 28","2016-07-22","2016-07-19","5","2","0","Visa Receivable (IV), INV#IV28, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("181","43","7","25","Visa Payable","300","0","","2016-07-22","2016-07-19","5","2","0","INV#25,sup:43,:against:28");
INSERT INTO transaction VALUES("182","7","43","25","Visa Payable","0","300","","2016-07-22","2016-07-19","5","2","0","INV#25,sup:43,:against:28");
INSERT INTO transaction VALUES("183","60","6","29","Visa Receivable","0","15100","Visa Receivable : 29","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV29, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("184","6","60","29","Visa Receivable","15100","0","Visa Receivable : 29","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV29, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("185","43","7","26","Visa Payable","14200","0","","2016-07-22","2016-07-20","5","2","0","INV#26,sup:43,:against:29");
INSERT INTO transaction VALUES("186","7","43","26","Visa Payable","0","14200","","2016-07-22","2016-07-20","5","2","0","INV#26,sup:43,:against:29");
INSERT INTO transaction VALUES("187","63","6","30","Visa Receivable","0","15100","Visa Receivable : 30","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV30, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("188","6","63","30","Visa Receivable","15100","0","Visa Receivable : 30","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV30, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("189","63","6","31","Visa Receivable","0","15100","Visa Receivable : 31","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV31, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("190","6","63","31","Visa Receivable","15100","0","Visa Receivable : 31","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV31, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("191","43","7","27","Visa Payable","14200","0","","2016-07-22","2016-07-20","5","2","0","INV#27,sup:43,:against:31");
INSERT INTO transaction VALUES("192","7","43","27","Visa Payable","0","14200","","2016-07-22","2016-07-20","5","2","0","INV#27,sup:43,:against:31");
INSERT INTO transaction VALUES("193","40","6","32","Visa Receivable","0","14700","Visa Receivable : 32","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV32, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("194","6","40","32","Visa Receivable","14700","0","Visa Receivable : 32","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV32, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("195","40","6","33","Visa Receivable","0","14700","Visa Receivable : 33","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV33, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("196","6","40","33","Visa Receivable","14700","0","Visa Receivable : 33","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV33, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("197","43","7","28","Visa Payable","14200","0","","2016-07-22","2016-07-20","5","2","0","INV#28,sup:43,:against:33");
INSERT INTO transaction VALUES("198","7","43","28","Visa Payable","0","14200","","2016-07-22","2016-07-20","5","2","0","INV#28,sup:43,:against:33");
INSERT INTO transaction VALUES("199","40","6","34","Visa Receivable","0","15000","Visa Receivable : 34","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV34, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("200","6","40","34","Visa Receivable","15000","0","Visa Receivable : 34","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV34, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("201","43","7","29","Visa Payable","14900","0","","2016-07-22","2016-07-20","5","2","0","INV#29,sup:43,:against:34");
INSERT INTO transaction VALUES("202","7","43","29","Visa Payable","0","14900","","2016-07-22","2016-07-20","5","2","0","INV#29,sup:43,:against:34");
INSERT INTO transaction VALUES("207","46","6","35","Visa Receivable","0","18200","Visa Receivable : 35","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV35, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("208","6","46","35","Visa Receivable","18200","0","Visa Receivable : 35","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV35, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("209","44","7","30","Visa Payable","17800","0","","2016-07-22","2016-07-20","5","2","0","INV#30,sup:44,:against:35");
INSERT INTO transaction VALUES("210","7","44","30","Visa Payable","0","17800","","2016-07-22","2016-07-20","5","2","0","INV#30,sup:44,:against:35");
INSERT INTO transaction VALUES("211","46","6","36","Visa Receivable","0","15100","Visa Receivable : 36","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV36, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("212","6","46","36","Visa Receivable","15100","0","Visa Receivable : 36","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV36, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("213","41","7","31","Visa Payable","15000","0","","2016-07-22","2016-07-20","5","2","0","INV#31,sup:41,:against:36");
INSERT INTO transaction VALUES("214","7","41","31","Visa Payable","0","15000","","2016-07-22","2016-07-20","5","2","0","INV#31,sup:41,:against:36");
INSERT INTO transaction VALUES("215","63","6","37","Visa Receivable","0","5200","Visa Receivable : 37","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV37, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("216","6","63","37","Visa Receivable","5200","0","Visa Receivable : 37","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV37, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("217","42","7","32","Visa Payable","5150","0","","2016-07-22","2016-07-20","5","2","0","INV#32,sup:42,:against:37");
INSERT INTO transaction VALUES("218","7","42","32","Visa Payable","0","5150","","2016-07-22","2016-07-20","5","2","0","INV#32,sup:42,:against:37");
INSERT INTO transaction VALUES("219","63","6","38","Visa Receivable","0","5200","Visa Receivable : 38","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV38, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("220","6","63","38","Visa Receivable","5200","0","Visa Receivable : 38","2016-07-22","2016-07-20","5","2","0","Visa Receivable (IV), INV#IV38, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("221","42","7","33","Visa Payable","5150","0","","2016-07-22","2016-07-20","5","2","0","INV#33,sup:42,:against:38");
INSERT INTO transaction VALUES("222","7","42","33","Visa Payable","0","5150","","2016-07-22","2016-07-20","5","2","0","INV#33,sup:42,:against:38");
INSERT INTO transaction VALUES("223","40","6","39","Visa Receivable","0","14700","Visa Receivable : 39","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV39, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("224","6","40","39","Visa Receivable","14700","0","Visa Receivable : 39","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV39, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("225","40","6","40","Visa Receivable","0","14700","Visa Receivable : 40","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV40, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("226","6","40","40","Visa Receivable","14700","0","Visa Receivable : 40","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV40, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("227","43","7","34","Visa Payable","14200","0","","2016-07-22","2016-07-21","5","2","0","INV#34,sup:43,:against:40");
INSERT INTO transaction VALUES("228","7","43","34","Visa Payable","0","14200","","2016-07-22","2016-07-21","5","2","0","INV#34,sup:43,:against:40");
INSERT INTO transaction VALUES("229","45","6","41","Visa Receivable","0","16300","Visa Receivable : 41","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV41, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("230","6","45","41","Visa Receivable","16300","0","Visa Receivable : 41","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV41, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("231","45","6","42","Visa Receivable","0","16300","Visa Receivable : 42","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV42, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("232","6","45","42","Visa Receivable","16300","0","Visa Receivable : 42","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV42, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("233","42","7","35","Visa Payable","15400","0","","2016-07-22","2016-07-21","5","2","0","INV#35,sup:42,:against:42");
INSERT INTO transaction VALUES("234","7","42","35","Visa Payable","0","15400","","2016-07-22","2016-07-21","5","2","0","INV#35,sup:42,:against:42");
INSERT INTO transaction VALUES("235","40","6","43","Visa Receivable","0","15100","Visa Receivable : 43","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV43, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("236","6","40","43","Visa Receivable","15100","0","Visa Receivable : 43","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV43, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("237","41","7","36","Visa Payable","15000","0","","2016-07-22","2016-07-21","5","2","0","INV#36,sup:41,:against:43");
INSERT INTO transaction VALUES("238","7","41","36","Visa Payable","0","15000","","2016-07-22","2016-07-21","5","2","0","INV#36,sup:41,:against:43");
INSERT INTO transaction VALUES("239","66","6","44","Visa Receivable","0","14800","Visa Receivable : 44","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV44, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("240","6","66","44","Visa Receivable","14800","0","Visa Receivable : 44","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV44, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("241","43","7","37","Visa Payable","14200","0","","2016-07-22","2016-07-21","5","2","0","INV#37,sup:43,:against:44");
INSERT INTO transaction VALUES("242","7","43","37","Visa Payable","0","14200","","2016-07-22","2016-07-21","5","2","0","INV#37,sup:43,:against:44");
INSERT INTO transaction VALUES("243","40","6","45","Visa Receivable","0","14700","Visa Receivable : 45","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV45, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("244","6","40","45","Visa Receivable","14700","0","Visa Receivable : 45","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV45, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("245","43","7","38","Visa Payable","14200","0","","2016-07-22","2016-07-21","5","2","0","INV#38,sup:43,:against:45");
INSERT INTO transaction VALUES("246","7","43","38","Visa Payable","0","14200","","2016-07-22","2016-07-21","5","2","0","INV#38,sup:43,:against:45");
INSERT INTO transaction VALUES("247","64","6","46","Visa Receivable","0","17400","Visa Receivable : 46","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV46, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("248","6","64","46","Visa Receivable","17400","0","Visa Receivable : 46","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV46, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("253","40","6","48","Visa Receivable","0","300","Visa Receivable : 48","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV48, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("254","6","40","48","Visa Receivable","300","0","Visa Receivable : 48","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV48, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("255","42","7","40","Visa Payable","250","0","","2016-07-22","2016-07-22","5","2","0","INV#40,sup:42,:against:48");
INSERT INTO transaction VALUES("256","7","42","40","Visa Payable","0","250","","2016-07-22","2016-07-22","5","2","0","INV#40,sup:42,:against:48");
INSERT INTO transaction VALUES("259","65","2","3","Cash Receipts","30000","0","21 JUL CHEQUE","2016-07-22","2016-07-22","5","2","0","Cash Receipts(CR), INV#CR,By:Mtm Travel Service-Irfad");
INSERT INTO transaction VALUES("260","2","65","3","Cash Receipts","0","30000","21 JUL CHEQUE","2016-07-22","2016-07-22","5","2","0","Cash Receipts(CR), INV#CR,By:Mtm Travel Service-Irfad");
INSERT INTO transaction VALUES("261","64","6","47","Visa Receivable","0","17400","Visa Receivable : 47","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV47, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("262","6","64","47","Visa Receivable","17400","0","Visa Receivable : 47","2016-07-22","2016-07-21","5","2","0","Visa Receivable (IV), INV#IV47, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("263","67","7","39","Visa Payable","16800","0","","2016-07-22","2016-07-22","5","2","0","INV#39,sup:67,:against:47");
INSERT INTO transaction VALUES("264","7","67","39","Visa Payable","0","16800","","2016-07-22","2016-07-22","5","2","0","INV#39,sup:67,:against:47");
INSERT INTO transaction VALUES("265","66","6","49","Visa Receivable","0","14800","Visa Receivable : 49","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV49, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("266","6","66","49","Visa Receivable","14800","0","Visa Receivable : 49","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV49, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("267","43","7","41","Visa Payable","14200","0","","2016-07-22","2016-07-22","5","2","0","INV#41,sup:43,:against:49");
INSERT INTO transaction VALUES("268","7","43","41","Visa Payable","0","14200","","2016-07-22","2016-07-22","5","2","0","INV#41,sup:43,:against:49");
INSERT INTO transaction VALUES("269","66","2","4","Cash Receipts","29600","0","","2016-07-22","2016-07-22","5","2","0","Cash Receipts(CR), INV#CR, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("270","2","66","4","Cash Receipts","0","29600","","2016-07-22","2016-07-22","5","2","0","Cash Receipts(CR), INV#CR, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("271","69","6","50","Visa Receivable","0","15300","Visa Receivable : 50","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV50, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("272","6","69","50","Visa Receivable","15300","0","Visa Receivable : 50","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV50, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("273","43","7","42","Visa Payable","14200","0","","2016-07-22","2016-07-22","5","2","0","INV#42,sup:43,:against:50");
INSERT INTO transaction VALUES("274","7","43","42","Visa Payable","0","14200","","2016-07-22","2016-07-22","5","2","0","INV#42,sup:43,:against:50");
INSERT INTO transaction VALUES("275","45","6","51","Visa Receivable","0","5200","Visa Receivable : 51","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV51, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("276","6","45","51","Visa Receivable","5200","0","Visa Receivable : 51","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV51, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("277","42","7","43","Visa Payable","5150","0","","2016-07-22","2016-07-22","5","2","0","INV#43,sup:42,:against:51");
INSERT INTO transaction VALUES("278","7","42","43","Visa Payable","0","5150","","2016-07-22","2016-07-22","5","2","0","INV#43,sup:42,:against:51");
INSERT INTO transaction VALUES("279","66","6","52","Visa Receivable","0","15100","Visa Receivable : 52","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV52, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("280","6","66","52","Visa Receivable","15100","0","Visa Receivable : 52","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV52, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("281","41","7","44","Visa Payable","15000","0","","2016-07-22","2016-07-22","5","2","0","INV#44,sup:41,:against:52");
INSERT INTO transaction VALUES("282","7","41","44","Visa Payable","0","15000","","2016-07-22","2016-07-22","5","2","0","INV#44,sup:41,:against:52");
INSERT INTO transaction VALUES("283","66","6","53","Visa Receivable","0","34400","Visa Receivable : 53","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV53, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("284","6","66","53","Visa Receivable","34400","0","Visa Receivable : 53","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV53, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("285","67","7","45","Visa Payable","17000","0","","2016-07-22","2016-07-22","5","2","0","INV#45,sup:67,:against:53");
INSERT INTO transaction VALUES("286","7","67","45","Visa Payable","0","17000","","2016-07-22","2016-07-22","5","2","0","INV#45,sup:67,:against:53");
INSERT INTO transaction VALUES("287","45","6","54","Visa Receivable","0","15000","Visa Receivable : 54","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV54, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("288","6","45","54","Visa Receivable","15000","0","Visa Receivable : 54","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV54, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("289","43","7","46","Visa Payable","14200","0","","2016-07-22","2016-07-22","5","2","0","INV#46,sup:43,:against:54");
INSERT INTO transaction VALUES("290","7","43","46","Visa Payable","0","14200","","2016-07-22","2016-07-22","5","2","0","INV#46,sup:43,:against:54");
INSERT INTO transaction VALUES("291","40","6","55","Visa Receivable","0","600","Visa Receivable : 55","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV55, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("292","6","40","55","Visa Receivable","600","0","Visa Receivable : 55","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV55, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("293","43","7","47","Visa Payable","500","0","","2016-07-22","2016-07-22","5","2","0","INV#47,sup:43,:against:55");
INSERT INTO transaction VALUES("294","7","43","47","Visa Payable","0","500","","2016-07-22","2016-07-22","5","2","0","INV#47,sup:43,:against:55");
INSERT INTO transaction VALUES("295","40","6","56","Visa Receivable","0","14700","Visa Receivable : 56","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV56, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("296","6","40","56","Visa Receivable","14700","0","Visa Receivable : 56","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV56, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("297","43","7","48","Visa Payable","14200","0","","2016-07-22","2016-07-22","5","2","0","INV#48,sup:43,:against:56");
INSERT INTO transaction VALUES("298","7","43","48","Visa Payable","0","14200","","2016-07-22","2016-07-22","5","2","0","INV#48,sup:43,:against:56");
INSERT INTO transaction VALUES("299","40","6","57","Visa Receivable","0","15000","Visa Receivable : 57","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV57, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("300","6","40","57","Visa Receivable","15000","0","Visa Receivable : 57","2016-07-22","2016-07-22","5","2","0","Visa Receivable (IV), INV#IV57, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("301","43","7","49","Visa Payable","14900","0","","2016-07-22","2016-07-22","5","2","0","INV#49,sup:43,:against:57");
INSERT INTO transaction VALUES("302","7","43","49","Visa Payable","0","14900","","2016-07-22","2016-07-22","5","2","0","INV#49,sup:43,:against:57");
INSERT INTO transaction VALUES("303","63","48","3","Bank Receipts","30700","0","FED CHEQUE 23 JUL","2016-07-23","2016-07-23","5","2","0","INV#BR3,by:Irfad,from:PAIKAT TRAVELS");
INSERT INTO transaction VALUES("304","48","63","3","Bank Receipts","0","30700","FED CHEQUE 23 JUL","2016-07-23","2016-07-23","5","2","0","INV#BR3,by:Irfad,from:PAIKAT TRAVELS");
INSERT INTO transaction VALUES("305","65","6","58","Visa Receivable","0","14600","Visa Receivable : 58","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV58, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("306","6","65","58","Visa Receivable","14600","0","Visa Receivable : 58","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV58, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("307","43","7","50","Visa Payable","14200","0","","2016-07-23","2016-07-23","5","2","0","INV#50,sup:43,:against:58");
INSERT INTO transaction VALUES("308","7","43","50","Visa Payable","0","14200","","2016-07-23","2016-07-23","5","2","0","INV#50,sup:43,:against:58");
INSERT INTO transaction VALUES("309","65","6","59","Visa Receivable","0","14600","Visa Receivable : 59","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV59, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("310","6","65","59","Visa Receivable","14600","0","Visa Receivable : 59","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV59, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("311","43","7","51","Visa Payable","14200","0","","2016-07-23","2016-07-23","5","2","0","INV#51,sup:43,:against:59");
INSERT INTO transaction VALUES("312","7","43","51","Visa Payable","0","14200","","2016-07-23","2016-07-23","5","2","0","INV#51,sup:43,:against:59");
INSERT INTO transaction VALUES("313","65","6","60","Visa Receivable","0","15100","Visa Receivable : 60","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV60, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("314","6","65","60","Visa Receivable","15100","0","Visa Receivable : 60","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV60, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("315","41","7","52","Visa Payable","15000","0","","2016-07-23","2016-07-23","5","2","0","INV#52,sup:41,:against:60");
INSERT INTO transaction VALUES("316","7","41","52","Visa Payable","0","15000","","2016-07-23","2016-07-23","5","2","0","INV#52,sup:41,:against:60");
INSERT INTO transaction VALUES("317","45","6","61","Visa Receivable","0","22500","Visa Receivable : 61","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV61, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("318","6","45","61","Visa Receivable","22500","0","Visa Receivable : 61","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV61, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("319","43","7","53","Visa Payable","21000","0","","2016-07-23","2016-07-23","5","2","0","INV#53,sup:43,:against:61");
INSERT INTO transaction VALUES("320","7","43","53","Visa Payable","0","21000","","2016-07-23","2016-07-23","5","2","0","INV#53,sup:43,:against:61");
INSERT INTO transaction VALUES("321","40","6","62","Visa Receivable","0","34600","Visa Receivable : 62","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV62, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("322","6","40","62","Visa Receivable","34600","0","Visa Receivable : 62","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV62, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("323","44","7","54","Visa Payable","34000","0","","2016-07-23","2016-07-23","5","2","0","INV#54,sup:44,:against:62");
INSERT INTO transaction VALUES("324","7","44","54","Visa Payable","0","34000","","2016-07-23","2016-07-23","5","2","0","INV#54,sup:44,:against:62");
INSERT INTO transaction VALUES("325","40","6","63","Visa Receivable","0","14700","Visa Receivable : 63","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV63, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("326","6","40","63","Visa Receivable","14700","0","Visa Receivable : 63","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV63, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("327","43","7","55","Visa Payable","14200","0","","2016-07-23","2016-07-23","5","2","0","INV#55,sup:43,:against:63");
INSERT INTO transaction VALUES("328","7","43","55","Visa Payable","0","14200","","2016-07-23","2016-07-23","5","2","0","INV#55,sup:43,:against:63");
INSERT INTO transaction VALUES("329","66","6","64","Visa Receivable","0","15100","Visa Receivable : 64","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV64, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("330","6","66","64","Visa Receivable","15100","0","Visa Receivable : 64","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV64, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("331","41","7","56","Visa Payable","15000","0","","2016-07-23","2016-07-23","5","2","0","INV#56,sup:41,:against:64");
INSERT INTO transaction VALUES("332","7","41","56","Visa Payable","0","15000","","2016-07-23","2016-07-23","5","2","0","INV#56,sup:41,:against:64");
INSERT INTO transaction VALUES("333","40","6","65","Visa Receivable","0","14700","Visa Receivable : 65","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV65, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("334","6","40","65","Visa Receivable","14700","0","Visa Receivable : 65","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV65, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("335","43","7","57","Visa Payable","14200","0","","2016-07-23","2016-07-23","5","2","0","INV#57,sup:43,:against:65");
INSERT INTO transaction VALUES("336","7","43","57","Visa Payable","0","14200","","2016-07-23","2016-07-23","5","2","0","INV#57,sup:43,:against:65");
INSERT INTO transaction VALUES("341","70","6","66","Visa Receivable","0","15100","Visa Receivable : 66","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV66, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("342","6","70","66","Visa Receivable","15100","0","Visa Receivable : 66","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV66, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("343","41","7","58","Visa Payable","15000","0","","2016-07-23","2016-07-23","5","2","0","INV#58,sup:41,:against:66");
INSERT INTO transaction VALUES("344","7","41","58","Visa Payable","0","15000","","2016-07-23","2016-07-23","5","2","0","INV#58,sup:41,:against:66");
INSERT INTO transaction VALUES("349","63","6","67","Visa Receivable","0","300","Visa Receivable : 67","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV67, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("350","6","63","67","Visa Receivable","300","0","Visa Receivable : 67","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV67, submissionDate:00-00-0000, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("351","71","7","59","Visa Payable","250","0","","2016-07-23","2016-07-23","5","2","0","INV#59,sup:71,:against:67");
INSERT INTO transaction VALUES("352","7","71","59","Visa Payable","0","250","","2016-07-23","2016-07-23","5","2","0","INV#59,sup:71,:against:67");
INSERT INTO transaction VALUES("353","40","6","68","Visa Receivable","0","300","Visa Receivable : 68","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV68, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("354","6","40","68","Visa Receivable","300","0","Visa Receivable : 68","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV68, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("355","42","7","60","Visa Payable","300","0","","2016-07-23","2016-07-23","5","2","0","INV#60,sup:42,:against:68");
INSERT INTO transaction VALUES("356","7","42","60","Visa Payable","0","300","","2016-07-23","2016-07-23","5","2","0","INV#60,sup:42,:against:68");
INSERT INTO transaction VALUES("357","45","6","69","Visa Receivable","0","16100","Visa Receivable : 69","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV69, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("358","6","45","69","Visa Receivable","16100","0","Visa Receivable : 69","2016-07-23","2016-07-23","5","2","0","Visa Receivable (IV), INV#IV69, submissionDate:, By:Mtm Travel Service-Irfad ");
INSERT INTO transaction VALUES("359","42","7","61","Visa Payable","15400","0","","2016-07-23","2016-07-23","5","2","0","INV#61,sup:42,:against:69");
INSERT INTO transaction VALUES("360","7","42","61","Visa Payable","0","15400","","2016-07-23","2016-07-23","5","2","0","INV#61,sup:42,:against:69");
INSERT INTO transaction VALUES("363","72","8","15","Package Receivable","0","0","Invoice Package: 15","2016-07-26","2016-07-26","6","2","15","INV#IP15,Pac:Haj ,Pax:UMMER JOIPORE ABBASU,AISAMMA JOIPORE UMMER ,By:Arshak");
INSERT INTO transaction VALUES("364","8","72","15","Package Receivable","0","0","Invoice Package: 15","2016-07-26","2016-07-26","6","2","15","INV#IP15,Pac:Haj ,Pax:UMMER JOIPORE ABBASU,AISAMMA JOIPORE UMMER ,By:Arshak");
INSERT INTO transaction VALUES("367","73","8","16","Package Receivable","0","0","Invoice Package: 16","2016-07-26","2016-07-26","6","2","16","INV#IP16,Pac:Haj ,Pax:ABDURAHIMAN PUTHIYOTTIL,KHADEEJA MADATHIL KUNNUMMAL ,By:Arshak");
INSERT INTO transaction VALUES("368","8","73","16","Package Receivable","0","0","Invoice Package: 16","2016-07-26","2016-07-26","6","2","16","INV#IP16,Pac:Haj ,Pax:ABDURAHIMAN PUTHIYOTTIL,KHADEEJA MADATHIL KUNNUMMAL ,By:Arshak");
INSERT INTO transaction VALUES("369","74","8","17","Package Receivable","0","700000","Invoice Package: 17","2016-07-27","2016-07-27","6","2","17","INV#IP17,Pac:Haj ,Pax:KOYA KAIKKOLAN PARAKKAL,SAFIYA PARAYULLATHIL MEETHAL ,By:Arshak");
INSERT INTO transaction VALUES("370","8","74","17","Package Receivable","700000","0","Invoice Package: 17","2016-07-27","2016-07-27","6","2","17","INV#IP17,Pac:Haj ,Pax:KOYA KAIKKOLAN PARAKKAL,SAFIYA PARAYULLATHIL MEETHAL ,By:Arshak");
INSERT INTO transaction VALUES("371","77","77","1","Opening Balance","1","0","Opening Balance","2016-07-27","2016-07-27","33","28","0","INV#1, acc:cus1,Opening:1");
INSERT INTO transaction VALUES("372","1","77","1","Opening Balance","0","1","Opening Balance","2016-07-27","2016-07-27","33","28","0","INV#1, acc:cus1,Opening:1");
INSERT INTO transaction VALUES("373","78","78","2","Opening Balance","1","0","Opening Balance","2016-07-27","2016-07-27","33","28","0","INV#2, acc:supplier1,Opening:1");
INSERT INTO transaction VALUES("374","1","78","2","Opening Balance","0","1","Opening Balance","2016-07-27","2016-07-27","33","28","0","INV#2, acc:supplier1,Opening:1");
INSERT INTO transaction VALUES("445","77","10","1","Hotel Receivable","0","11","Hotel Receivable : 1","2016-07-27","2016-07-27","33","28","0","Hotel Receivable (IH), INV#IH1,Customer Name:cus1,Hotel Name :Taj, CheckIn Date:28-07-2016, No.of Rooms: 1,No.of Nights: 3,By:Test-Tstaff1 ");
INSERT INTO transaction VALUES("446","10","77","1","Hotel Receivable","11","0","Hotel Receivable : 1","2016-07-27","2016-07-27","33","28","0","Hotel Receivable (IH), INV#IH1,Customer Name:cus1,Hotel Name :Taj, CheckIn Date:28-07-2016, No.of Rooms: 1,No.of Nights: 3,By:Test-Tstaff1 ");
INSERT INTO transaction VALUES("447","0","77","1","Hotel Receivable","0","2","Hotel Receivable : 1","2016-07-27","2016-07-27","33","28","0","Hotel Receivable (IH), INV#IH1,Customer Name:cus1,Hotel Name :Taj, CheckIn Date:28-07-2016, No.of Rooms: 1,No.of Nights: 3,By:Test-Tstaff1 ");
INSERT INTO transaction VALUES("448","77","0","1","Hotel Receivable","2","0","Hotel Receivable : 1","2016-07-27","2016-07-27","33","28","0","Hotel Receivable (IH), INV#IH1,Customer Name:cus1,Hotel Name :Taj, CheckIn Date:28-07-2016, No.of Rooms: 1,No.of Nights: 3,By:Test-Tstaff1 ");
INSERT INTO transaction VALUES("449","77","18","1","Hotel Receivable","0","3","Hotel Receivable:Tax#1","2016-07-27","2016-07-27","33","28","0","Hotel Receivable (IH), INV#IH1,Customer Name:cus1,Hotel Name :Taj, CheckIn Date:28-07-2016, No.of Rooms: 1,No.of Nights: 3,By:Test-Tstaff1 ");
INSERT INTO transaction VALUES("450","18","77","1","Hotel Receivable","3","0","Hotel Receivable:Tax#1","2016-07-27","2016-07-27","33","28","0","Hotel Receivable (IH), INV#IH1,Customer Name:cus1,Hotel Name :Taj, CheckIn Date:28-07-2016, No.of Rooms: 1,No.of Nights: 3,By:Test-Tstaff1 ");
INSERT INTO transaction VALUES("451","78","11","1","Hotel Payable","14","0","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("452","11","78","1","Hotel Payable","0","14","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("453","78","16","1","Hotel Payable","2","0","Discount Given:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("454","16","78","1","Hotel Payable","0","2","Discount Given:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("455","78","19","1","Hotel Payable","4","0","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("456","19","78","1","Hotel Payable","0","4","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("457","78","18","1","Hotel Payable","1","0","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("458","18","78","1","Hotel Payable","0","1","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("459","78","20","1","Hotel Payable","0","1","Tac:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("460","20","78","1","Hotel Payable","1","0","Tac:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("531","78","5","1","Airline Payable","11","0","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("532","5","78","1","Airline Payable","0","11","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("533","78","16","1","Airline Payable","1","0","Discount Given:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("534","16","78","1","Airline Payable","0","1","Discount Given:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("535","78","19","1","Airline Payable","3","0","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("536","19","78","1","Airline Payable","0","3","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("537","78","18","1","Airline Payable","1","0","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("538","18","78","1","Airline Payable","0","1","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("539","78","20","1","Airline Payable","0","2","Tac:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("540","20","78","1","Airline Payable","2","0","Tac:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("559","78","7","1","Visa Payable","17","0","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("560","7","78","1","Visa Payable","0","17","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("561","78","16","1","Visa Payable","1","0","Discount Given:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("562","16","78","1","Visa Payable","0","1","Discount Given:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("563","78","19","1","Visa Payable","1","0","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("564","19","78","1","Visa Payable","0","1","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("565","78","18","1","Visa Payable","1","0","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("566","18","78","1","Visa Payable","0","1","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("581","78","13","1","Transport Payable","5","0","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("582","13","78","1","Transport Payable","0","5","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("583","78","19","1","Transport Payable","1","0","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("584","19","78","1","Transport Payable","0","1","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("585","78","18","1","Transport Payable","1","0","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("586","18","78","1","Transport Payable","0","1","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("591","78","7","1","Visa Payable","4","0","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("592","7","78","1","Visa Payable","0","4","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("593","78","16","1","Visa Payable","2","0","Discount Given:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("594","16","78","1","Visa Payable","0","2","Discount Given:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("595","78","19","1","Visa Payable","1","0","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("596","19","78","1","Visa Payable","0","1","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("597","78","18","1","Visa Payable","2","0","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("598","18","78","1","Visa Payable","0","2","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("599","78","20","1","Visa Payable","0","1","Tac:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("600","20","78","1","Visa Payable","1","0","Tac:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("609","78","7","0","Visa Payable","10","0","","2016-07-27","2016-07-27","33","28","0","INV#,sup:78,:against:0");
INSERT INTO transaction VALUES("610","7","78","0","Visa Payable","0","10","","2016-07-27","2016-07-27","33","28","0","INV#,sup:78,:against:0");
INSERT INTO transaction VALUES("611","78","16","0","Visa Payable","1","0","Discount Given:inv#","2016-07-27","2016-07-27","33","28","0","INV#,sup:78,:against:0");
INSERT INTO transaction VALUES("612","16","78","0","Visa Payable","0","1","Discount Given:inv#","2016-07-27","2016-07-27","33","28","0","INV#,sup:78,:against:0");
INSERT INTO transaction VALUES("613","78","19","0","Visa Payable","1","0","Tds:inv#","2016-07-27","2016-07-27","33","28","0","INV#,sup:78,:against:0");
INSERT INTO transaction VALUES("614","19","78","0","Visa Payable","0","1","Tds:inv#","2016-07-27","2016-07-27","33","28","0","INV#,sup:78,:against:0");
INSERT INTO transaction VALUES("615","78","18","0","Visa Payable","3","0","Tax:inv#","2016-07-27","2016-07-27","33","28","0","INV#,sup:78,:against:0");
INSERT INTO transaction VALUES("616","18","78","0","Visa Payable","0","3","Tax:inv#","2016-07-27","2016-07-27","33","28","0","INV#,sup:78,:against:0");
INSERT INTO transaction VALUES("617","78","20","0","Visa Payable","0","2","Tac:inv#","2016-07-27","2016-07-27","33","28","0","INV#,sup:78,:against:0");
INSERT INTO transaction VALUES("618","20","78","0","Visa Payable","2","0","Tac:inv#","2016-07-27","2016-07-27","33","28","0","INV#,sup:78,:against:0");
INSERT INTO transaction VALUES("623","78","7","1","Visa Payable","5","0","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("624","7","78","1","Visa Payable","0","5","","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("625","78","16","1","Visa Payable","2","0","Discount Given:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("626","16","78","1","Visa Payable","0","2","Discount Given:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("627","78","19","1","Visa Payable","1","0","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("628","19","78","1","Visa Payable","0","1","Tds:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("629","78","18","1","Visa Payable","2","0","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("630","18","78","1","Visa Payable","0","2","Tax:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("631","78","20","1","Visa Payable","0","1","Tac:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("632","20","78","1","Visa Payable","1","0","Tac:inv#1","2016-07-27","2016-07-27","33","28","0","INV#1,sup:78,:against:1");
INSERT INTO transaction VALUES("651","78","7","2","Visa Payable","4","0","","2016-07-27","2016-07-27","33","28","0","INV#2,sup:78,:against:2");
INSERT INTO transaction VALUES("652","7","78","2","Visa Payable","0","4","","2016-07-27","2016-07-27","33","28","0","INV#2,sup:78,:against:2");
INSERT INTO transaction VALUES("653","78","19","2","Visa Payable","2","0","Tds:inv#2","2016-07-27","2016-07-27","33","28","0","INV#2,sup:78,:against:2");
INSERT INTO transaction VALUES("654","19","78","2","Visa Payable","0","2","Tds:inv#2","2016-07-27","2016-07-27","33","28","0","INV#2,sup:78,:against:2");
INSERT INTO transaction VALUES("655","78","18","2","Visa Payable","2","0","Tax:inv#2","2016-07-27","2016-07-27","33","28","0","INV#2,sup:78,:against:2");
INSERT INTO transaction VALUES("656","18","78","2","Visa Payable","0","2","Tax:inv#2","2016-07-27","2016-07-27","33","28","0","INV#2,sup:78,:against:2");

DROP TABLE IF EXISTS transport_provider;
CREATE TABLE `transport_provider` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `providerName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO transport_provider VALUES("1","Al Ojur Company","","","2","6");

DROP TABLE IF EXISTS user_permission;
CREATE TABLE `user_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NOT NULL,
  `basicSetting` int(11) NOT NULL DEFAULT '0',
  `basicSettingEdit` int(11) NOT NULL DEFAULT '0',
  `package` int(11) NOT NULL DEFAULT '0',
  `packageEdit` int(11) NOT NULL DEFAULT '0',
  `account` int(11) NOT NULL DEFAULT '0',
  `accountEdit` int(11) NOT NULL DEFAULT '0',
  `cashBank` int(11) NOT NULL DEFAULT '0',
  `cashBankEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceAirline` int(11) NOT NULL DEFAULT '0',
  `invoiceAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceHotel` int(11) NOT NULL DEFAULT '0',
  `invoiceHotelEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceVisa` int(11) NOT NULL DEFAULT '0',
  `invoiceVisaEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceMisc` int(11) NOT NULL DEFAULT '0',
  `invoiceMiscEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceTransport` int(11) NOT NULL DEFAULT '0',
  `invoiceTransportEdit` int(11) NOT NULL DEFAULT '0',
  `invoicePackage` int(11) NOT NULL DEFAULT '0',
  `invoicePackageEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseAirline` int(11) NOT NULL DEFAULT '0',
  `purchaseAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseHotel` int(11) NOT NULL DEFAULT '0',
  `purchaseHotelEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseVisa` int(11) NOT NULL DEFAULT '0',
  `purchaseVisaEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseMisc` int(11) NOT NULL DEFAULT '0',
  `purchaseMiscEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseTransport` int(11) NOT NULL DEFAULT '0',
  `purchaseTransportEdit` int(11) NOT NULL DEFAULT '0',
  `purchasePackage` int(11) NOT NULL DEFAULT '0',
  `purchasePackageEdit` int(11) NOT NULL DEFAULT '0',
  `accountReport` int(11) NOT NULL DEFAULT '0',
  `otherReport` int(11) NOT NULL DEFAULT '0',
  `user` int(11) NOT NULL DEFAULT '0',
  `userEdit` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `staffId` (`staffId`),
  CONSTRAINT `user_permission_ibfk_1` FOREIGN KEY (`staffId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
INSERT INTO user_permission VALUES("2","2","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0");
INSERT INTO user_permission VALUES("3","3","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1");
INSERT INTO user_permission VALUES("5","6","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1","1","0","0");

DROP TABLE IF EXISTS vehicle_types;
CREATE TABLE `vehicle_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `vehicleType` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
INSERT INTO vehicle_types VALUES("1","Bus","2","6");

DROP TABLE IF EXISTS visa_passenger;
CREATE TABLE `visa_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visaId` int(11) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(20) NOT NULL,
  `passportNo` varchar(50) NOT NULL,
  `visaFor` varchar(50) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `tokenNo` varchar(20) NOT NULL,
  `visaFee` float NOT NULL,
  `vfs` float NOT NULL,
  `ddCharge` float NOT NULL,
  `serviceCharge` float NOT NULL,
  `otherCharge` float NOT NULL,
  `courierCharge` float NOT NULL,
  `subTotal` float NOT NULL,
  `travelDate` date NOT NULL,
  `visaMessage` varchar(20) NOT NULL,
  `msgChngBy` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
INSERT INTO visa_passenger VALUES("1","1","Bismi Ramshad","0000-00-00","F","M2551864","2","2016-07-16","0000-00-00","0000-00-00","","15000","0","0","300","0","0","15300","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("2","2","Shamnas","0000-00-00","M","P0917864","2","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","100","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("3","2","Shanaza","0000-00-00","F","N9972437","2","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","100","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("4","2","Suhail","0000-00-00","M","K5983739","2","0000-00-00","0000-00-00","0000-00-00","","14800","0","0","100","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("5","3","SAKEER LASINA","0000-00-00","M","L4417087","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","200","0","0","15200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("6","4","ANAND","0000-00-00","M","J6643549","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","400","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("7","5","SAFAR","0000-00-00","M","H7376973","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","400","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("8","6","KUMARAN","0000-00-00","M","K0060995","4","0000-00-00","0000-00-00","0000-00-00","","5150","0","0","100","0","0","5250","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("9","7","DON","0000-00-00","M","K7228415","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","400","0","0","14900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("10","8","AYISHA","0000-00-00","M","M3617677","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("12","10","","0000-00-00","","","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","600","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("13","11","ANOOP MOONAKAPARMBIL","0000-00-00","M","F7229447","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","1000","0","0","15500","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("14","12","DURAI PANDYAN","0000-00-00","M","N2573742","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","600","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("15","13","JESINTHA & SAMEER THANALOTT","0000-00-00","M","K4277103 & H0862662","2","0000-00-00","0000-00-00","0000-00-00","","29000","0","0","800","0","0","29800","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("16","14","ANIL KUMAR & LATHA NATHAN","0000-00-00","M","K7816303  & Z3140980","2","0000-00-00","0000-00-00","0000-00-00","","29000","0","0","800","0","0","29800","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("17","15","NAVAS KOLAVAYAL","0000-00-00","M","J1054938","2","0000-00-00","0000-00-00","0000-00-00","","14500","0","0","700","0","0","15200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("18","16","LATHA MATTAN","0000-00-00","M","Z3140980","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("19","17","JISHNU MANIYANTHARA","0000-00-00","M","M6709343","2","0000-00-00","0000-00-00","0000-00-00","","17000","0","0","400","0","0","17400","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("21","19","","0000-00-00","","","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("22","20","SHEEJA","0000-00-00","F","L6729625","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("23","21","SHEEJA","0000-00-00","M","L6729625","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("25","23","","0000-00-00","","","4","0000-00-00","0000-00-00","0000-00-00","","5150","0","0","50","0","0","5200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("26","24","HOWATH ABU","0000-00-00","F","L3741646","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("27","25","MOHAMMED MOIDU AHMAD","0000-00-00","M","J7622760","4","0000-00-00","0000-00-00","0000-00-00","","5150","0","0","50","0","0","5200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("28","26","HANA ASHRAF ALI","0000-00-00","F","L2522470","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("29","26","AMINA FIDHA","0000-00-00","F","L2364571","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("30","27","JARAD MOIDU","0000-00-00","M","N9461175","3","0000-00-00","0000-00-00","0000-00-00","","5700","0","0","6200","0","0","11900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("32","29","","0000-00-00","","","4","0000-00-00","0000-00-00","0000-00-00","","5150","0","0","50","0","0","5200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("33","30","MANAF PUTHIYA VALAPPIL","0000-00-00","M","K3967752","4","0000-00-00","0000-00-00","0000-00-00","","5150","0","0","50","0","0","5200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("34","31","SUNNY STEPHAN","0000-00-00","M","P2690972","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","14700","0","0","28900","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("35","32","AKHILA","0000-00-00","M","0","12","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("36","33","SAHIL POOLERI","0000-00-00","M","M4864873","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","900","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("37","34","CHERIYAMON","0000-00-00","M","H3496894","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","900","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("38","35","CHERIYA MON","0000-00-00","M","H3496894","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","900","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("39","36","AFSAL","0000-00-00","M","N8312729","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("40","37","AFSAL","0000-00-00","M","N8312729","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("41","38","ANSEENA","0000-00-00","M","N5915766","2","0000-00-00","0000-00-00","0000-00-00","","14900","0","0","100","0","0","15000","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("43","40","","0000-00-00","","","13","0000-00-00","0000-00-00","0000-00-00","","17800","0","0","400","0","0","18200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("44","41","MIDHUN","0000-00-00","M","N1201156","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("45","42","SALAHUDHEEN","0000-00-00","M","G5456820","3","0000-00-00","0000-00-00","0000-00-00","","5150","0","0","50","0","0","5200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("46","43","VASANTHI RAMESH","0000-00-00","M","H4813688","4","0000-00-00","0000-00-00","0000-00-00","","5150","0","0","50","0","0","5200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("47","44","ABIDALI","0000-00-00","M","P2097152","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("48","45","ABIDALI","0000-00-00","M","P2097152","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("49","46","MOHAMMED NIZAM","0000-00-00","M","K8898707","14","0000-00-00","0000-00-00","0000-00-00","","15400","0","0","900","0","0","16300","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("50","47","MOHAMMED NIZAM","0000-00-00","M","K8898707","14","0000-00-00","0000-00-00","0000-00-00","","15400","0","0","900","0","0","16300","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("51","48","AYISHA MOIDEEN","0000-00-00","F","N4062756","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("52","49","YASAR","0000-00-00","M","H0522937","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","600","0","0","14800","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("53","50","SHAHRAS","0000-00-00","M","H0833397","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("54","51","BIBIN JHON","0000-00-00","M","M7052470","13","0000-00-00","0000-00-00","0000-00-00","","16800","0","0","600","0","0","17400","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("57","54","","0000-00-00","","","13","0000-00-00","0000-00-00","0000-00-00","","16800","0","0","600","0","0","17400","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("56","53","SAMEER","0000-00-00","M","0","12","0000-00-00","0000-00-00","0000-00-00","","250","0","0","50","0","0","300","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("58","55","DEEPU ARANGOTH","0000-00-00","M","H5549880","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","600","0","0","14800","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("59","56","SAIDALAVI KARIRATTATH","0000-00-00","M","J2735882","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","1100","0","0","15300","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("60","57","ANAS ASSBE","0000-00-00","M","G8684159","4","0000-00-00","0000-00-00","0000-00-00","","5150","0","0","50","0","0","5200","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("61","58","ANJALI SUNEESH","0000-00-00","F","P1371510","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("62","59","SUBAIR NADAVEKA","0000-00-00","M","G4265797","13","0000-00-00","0000-00-00","0000-00-00","","17000","0","0","17400","0","0","34400","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("63","60","SANAL VIJAYAN","0000-00-00","M","L6565994","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","800","0","0","15000","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("64","61","SRUTHI","0000-00-00","F","0","12","0000-00-00","0000-00-00","0000-00-00","","500","0","0","100","0","0","600","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("65","62","RAHUL","0000-00-00","M","J4742862","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("66","63","ALEEMA","0000-00-00","F","J5798946","2","0000-00-00","0000-00-00","0000-00-00","","14900","0","0","100","0","0","15000","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("67","64","HIJAS","0000-00-00","M","G6850518","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","400","0","0","14600","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("68","65","KHALEEL","0000-00-00","M","L4215643","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","400","0","0","14600","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("69","66","RISHANA ALAGADATH","0000-00-00","F","P0821481","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("70","67","SIVAPRASAD","0000-00-00","M","L8832970","3","0000-00-00","0000-00-00","0000-00-00","","7000","0","0","500","0","0","7500","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("71","67","UNNIKRISHNAN","0000-00-00","M","N2027345","3","0000-00-00","0000-00-00","0000-00-00","","7000","0","0","500","0","0","7500","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("72","67","UNNIKRISHNAN","0000-00-00","M","L1740323","3","0000-00-00","0000-00-00","0000-00-00","","7000","0","0","500","0","0","7500","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("73","68","SESU","0000-00-00","M","H0287717","13","0000-00-00","0000-00-00","0000-00-00","","17000","0","0","300","0","0","17300","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("74","68","SARAH","0000-00-00","M","L7532058","13","0000-00-00","0000-00-00","0000-00-00","","17000","0","0","300","0","0","17300","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("75","69","ASMAL","0000-00-00","M","L8013212","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("76","70","SABEEL","0000-00-00","M","N70534523","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("77","71","ANSAR","0000-00-00","M","M5169954","2","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","500","0","0","14700","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("79","73","","0000-00-00","","","2","0000-00-00","0000-00-00","0000-00-00","","15000","0","0","100","0","0","15100","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("81","75","","0000-00-00","","","12","0000-00-00","0000-00-00","0000-00-00","","250","0","0","50","0","0","300","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("82","76","ANAND","0000-00-00","M","0","12","0000-00-00","0000-00-00","0000-00-00","","300","0","0","0","0","0","300","0000-00-00","","0","","2","5");
INSERT INTO visa_passenger VALUES("83","77","YOUSUF PALAKKAN","0000-00-00","M","N9462891","14","0000-00-00","0000-00-00","0000-00-00","","15400","0","0","700","0","0","16100","0000-00-00","","0","","2","5");

DROP TABLE IF EXISTS visafor;
CREATE TABLE `visafor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visafor` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
INSERT INTO visafor VALUES("1","90 DAYS DXB  WITH OTB","2","5");
INSERT INTO visafor VALUES("2","90 DAYS DXB  WITHOUT OTB","2","5");
INSERT INTO visafor VALUES("3","30 DAYS DXB WITH OTB","2","5");
INSERT INTO visafor VALUES("4","30 DAYS DXB WITHOUT OTB","2","5");
INSERT INTO visafor VALUES("5","30 DAYS QATAR BUSINESS","2","5");
INSERT INTO visafor VALUES("6","30 DAYS QATAR TOURIST","2","5");
INSERT INTO visafor VALUES("7","90 DAYS QATAR","2","5");
INSERT INTO visafor VALUES("8","90 DAYS UAE MULTIPLE","2","5");
INSERT INTO visafor VALUES("9","30 DAYS UAE MULTIPLE","2","5");
INSERT INTO visafor VALUES("10","30 DAYS UAE  LESURE","2","5");
INSERT INTO visafor VALUES("11","90 DAYS UAE LESURE","2","5");
INSERT INTO visafor VALUES("12","OTB","2","5");
INSERT INTO visafor VALUES("13","90 DAYS DXB CONVERTABLE","2","5");
INSERT INTO visafor VALUES("14","90 DAYS DXB SETTING","2","5");



								/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
								/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
								/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
								/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
								/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
								/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
								/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
								
						