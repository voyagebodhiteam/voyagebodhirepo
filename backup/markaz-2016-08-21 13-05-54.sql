/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
								/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
								/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
								/*!40101 SET NAMES utf8 */;
								/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
								/*!40103 SET TIME_ZONE='+00:00' */;
								/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
								/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
								/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
								/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
								DROP TABLE IF EXISTS academicyear;
CREATE TABLE `academicyear` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `fromYear` varchar(20) NOT NULL,
  `toYear` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
INSERT INTO academicyear VALUES("2","New year","2014","2015","NO");
INSERT INTO academicyear VALUES("3","new year2","2016","2017","YES");
INSERT INTO academicyear VALUES("4","new year3","2018","2019","NO");
INSERT INTO academicyear VALUES("5","","2019","2020","NO");
INSERT INTO academicyear VALUES("6","year","2024","2025","NO");

DROP TABLE IF EXISTS account_group;
CREATE TABLE `account_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `acGroup` text NOT NULL,
  `parent` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
INSERT INTO account_group VALUES("1","Account","0","0","0");
INSERT INTO account_group VALUES("2","Balance Sheet","1","0","0");
INSERT INTO account_group VALUES("3","Profit & Loss Account","1","0","0");
INSERT INTO account_group VALUES("4","Assets","2","0","0");
INSERT INTO account_group VALUES("5","Liabilities","2","0","0");
INSERT INTO account_group VALUES("6","Current Assets","4","0","0");
INSERT INTO account_group VALUES("7","Fixed Assets","4","0","0");
INSERT INTO account_group VALUES("8","Cash In Hand","6","0","0");
INSERT INTO account_group VALUES("9","Deposits","6","0","0");
INSERT INTO account_group VALUES("10","Investments","6","0","0");
INSERT INTO account_group VALUES("11","Loans & Advances (asset)","6","0","0");
INSERT INTO account_group VALUES("12","Bank A/c","6","0","0");
INSERT INTO account_group VALUES("13","Expenses","3","0","0");
INSERT INTO account_group VALUES("14","Income","3","0","0");
INSERT INTO account_group VALUES("15","Cash","8","0","0");
INSERT INTO account_group VALUES("16","Capital","5","0","0");
INSERT INTO account_group VALUES("17","Current Liabilities","5","0","0");
INSERT INTO account_group VALUES("18","Loans (liability)","5","0","0");
INSERT INTO account_group VALUES("19","Suspense Accounts","5","0","0");
INSERT INTO account_group VALUES("20","Bank OD A/C","17","0","0");
INSERT INTO account_group VALUES("21","Duties & Taxes","17","0","0");
INSERT INTO account_group VALUES("22","Journal Accounts","17","0","0");
INSERT INTO account_group VALUES("23","Provisions","17","0","0");
INSERT INTO account_group VALUES("24","Direct Expense","13","0","0");
INSERT INTO account_group VALUES("25","Indirect Expense","13","0","0");
INSERT INTO account_group VALUES("26","Direct Income","14","0","0");
INSERT INTO account_group VALUES("27","Indirect Income","14","0","0");
INSERT INTO account_group VALUES("28","Fittings","7","0","0");
INSERT INTO account_group VALUES("29","Advertisement","25","0","0");
INSERT INTO account_group VALUES("30","Donation Given","25","0","0");
INSERT INTO account_group VALUES("31","Electricity Bills","25","0","0");
INSERT INTO account_group VALUES("32","Miscellaneous Expense","25","0","0");
INSERT INTO account_group VALUES("33","Office Expense","25","0","0");
INSERT INTO account_group VALUES("34","Other Expenses","25","0","0");
INSERT INTO account_group VALUES("35","Telephone Bills","25","0","0");
INSERT INTO account_group VALUES("36","Other Income","27","0","0");
INSERT INTO account_group VALUES("37","Customer/ Sundry Debtors","6","0","0");
INSERT INTO account_group VALUES("38","Shipping Agents","17","0","0");
INSERT INTO account_group VALUES("39","Staff Deposits","17","0","0");
INSERT INTO account_group VALUES("40","Supplier/ Sundry Creditors","17","0","0");
INSERT INTO account_group VALUES("41","Discount Given ","24","0","0");
INSERT INTO account_group VALUES("42","Job Works","24","0","0");
INSERT INTO account_group VALUES("43","Purchases","24","0","0");
INSERT INTO account_group VALUES("44","Courier","26","0","0");
INSERT INTO account_group VALUES("45","Discount Recieved","26","0","0");
INSERT INTO account_group VALUES("46","Sales","26","0","0");
INSERT INTO account_group VALUES("47","Services","26","0","0");
INSERT INTO account_group VALUES("48","Commission Agent","25","0","0");
INSERT INTO account_group VALUES("49","Incentives Given","25","0","0");
INSERT INTO account_group VALUES("50","Printing Expense","25","0","0");
INSERT INTO account_group VALUES("51","Rent Given","25","0","0");
INSERT INTO account_group VALUES("52","Salaries","25","0","0");
INSERT INTO account_group VALUES("53","Sales Expenses","25","0","0");
INSERT INTO account_group VALUES("54","Tax Paid","25","0","0");
INSERT INTO account_group VALUES("55","Tax Recieved","25","0","0");
INSERT INTO account_group VALUES("56","Incentives Recieved","27","0","0");
INSERT INTO account_group VALUES("57","Branches","6","0","0");
INSERT INTO account_group VALUES("58","Main Branches","17","0","0");

DROP TABLE IF EXISTS accounts;
CREATE TABLE `accounts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `agentId` int(11) NOT NULL,
  `accountGroupId` int(11) NOT NULL,
  `accountName` varchar(200) NOT NULL,
  `openingBal` double NOT NULL,
  `payType` varchar(20) NOT NULL,
  `masterLedger` int(11) NOT NULL,
  `addDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `description` text NOT NULL,
  `contactPerson` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address2` text NOT NULL,
  `pin2` varchar(20) NOT NULL,
  `city2` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `accountGroupId` (`accountGroupId`),
  CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`accountGroupId`) REFERENCES `account_group` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
INSERT INTO accounts VALUES("1","0","19","Suspense","0","Credit","1","2016-03-22","1","0","0","Suspense Account","","","","","","","","","","");
INSERT INTO accounts VALUES("2","0","8","Cash Account","0","Credit","1","2016-03-22","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("3","0","24","Salary Payments","0","Credit","1","2016-03-22","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("4","0","14","Airline Receivable","0","Debit","1","2016-04-05","1","0","0","Airline charge received","","","","","","","","","","");
INSERT INTO accounts VALUES("5","0","13","Airline Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("6","0","14","Visa Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("7","0","13","Visa Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("8","0","14","Package Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("9","0","13","Package Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("10","0","14","Hotel Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("11","0","13","Hotel Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("12","0","14","Transport Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("13","0","13","Transport Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("14","0","14","Misc Receivable","0","Debit","1","2016-04-05","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("15","0","13","Misc Payable","0","Credit","1","2016-04-05","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("16","0","25","Discount Given","0","Credit","1","2016-05-25","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("17","0","27","Discount Received","0","Credit","1","2016-05-25","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("18","0","25","Tax","0","Credit","1","2016-06-24","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("19","0","25","Tds","0","Credit","1","2016-06-24","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("20","0","27","Tac","0","Credit","1","2016-06-24","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("21","0","25","Package Extra Chrg","0","Credit","1","2016-06-29","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("22","0","13","Other Purchase","0","Credit","0","2016-08-16","1","0","0","","","","","","","","","","","");
INSERT INTO accounts VALUES("40","0","37","custphr","0","Credit","0","2016-08-16","62","13","3","","custphr","","786663","vb","68768","798787979","muhammee@gmaol.com","","","");
INSERT INTO accounts VALUES("41","0","37","cust2phr","0","Credit","0","2016-08-16","62","13","3","","cust2phr","","657574","vbnvnv","68687","68768","muhamme.@gmail.com","","","");
INSERT INTO accounts VALUES("42","0","40","razak","0","Credit","0","2016-08-16","62","13","3","","","","","","","","","","","");
INSERT INTO accounts VALUES("43","0","48","rkagents","0","Credit","0","2016-08-16","62","13","3","","rkagents","","","","","","","","","");
INSERT INTO accounts VALUES("44","43","37","rkCustomer","0","Credit","0","2016-08-17","61","13","3","","Rameez","dwew","673001","Calicut","0495 26565655","9874563211","abc@gmail.com","wrwwrw","673211","673266");
INSERT INTO accounts VALUES("45","0","48","sernamAgent1","0","Credit","0","2016-08-17","59","16","3","","sernamAgent1","abc","","","","995633","","","","");
INSERT INTO accounts VALUES("46","0","48","senamAgent2","0","Credit","0","2016-08-17","59","16","3","","senamAgent2","enthanu babuatta ith","","","","786552","","","","");
INSERT INTO accounts VALUES("47","45","12","SBIsernamAgnet1","0","Credit","0","2016-08-17","59","16","3","","","","","","","","","","","");
INSERT INTO accounts VALUES("48","45","37","sernamCust1","0","Credit","0","2016-08-17","59","16","3","","enthooooo","rfedf","788878","fwef","878545","788454","ytyurgey@fvf.com","fds","343524","sffs");
INSERT INTO accounts VALUES("49","46","37","sernamCust2","0","Credit","0","2016-08-17","59","16","3","","rameez","dbc","797954","bjdbf","654654","4564","mushh@hg.com","vgfb","645564","vrtg");
INSERT INTO accounts VALUES("50","45","40","nachka","0","Credit","0","2016-08-17","59","16","3","","","","","","","5656","","","","");
INSERT INTO accounts VALUES("51","45","40","majeedka","0","Credit","0","2016-08-17","59","16","3","","","","","","","4654","","","","");
INSERT INTO accounts VALUES("52","45","37","thajuCust1","0","Credit","0","2016-08-17","63","16","3","","ert","fdefdfddf","546223","errr","rrr","4434","ddfd@dff.com","dfdff","695623","dfddd");
INSERT INTO accounts VALUES("53","46","37","ThajuCust2","0","Credit","0","2016-08-17","63","16","3","","dc","wdw","232322","sds","242","232","wdsdw@ee.com","swdw","122111","scs");
INSERT INTO accounts VALUES("54","0","48","wc","0","Credit","0","2016-08-17","65","18","3","","wc","test","","","","0","","","","");
INSERT INTO accounts VALUES("55","54","37","IRFAD STAFF","0","Debit","0","2016-08-17","66","18","3","","MAJID","POONOOR","673574","POONOOR","9746915105","9895115314","IRFADAMANTH1@GMAIL.COM","CHEPPALA","673574","CALICUT");
INSERT INTO accounts VALUES("56","54","40","SALIL BOM","0","Credit","0","2016-08-17","66","18","3","","SALIL","","","","","9895519222","","","","");
INSERT INTO accounts VALUES("57","54","40","ALTA CCJ","0","Credit","0","2016-08-17","66","18","3","","","","","","","0495 4050503","","","","");
INSERT INTO accounts VALUES("58","54","37","FAISAL STAFF","0","Debit","0","2016-08-17","66","18","3","","FAISAL","TEST","673575","KOZHIKODE","04954011515","9999999","FAISAL@GMAIL.COM","TEST","673454","TEST");
INSERT INTO accounts VALUES("59","54","40","BSP","0","Credit","0","2016-08-17","66","18","3","","","","","","","0000","","","","");
INSERT INTO accounts VALUES("60","0","48","Agent Manu","0","Credit","0","2016-08-18","64","16","3","","Agent Manu","Kakkodi","","","","1234567899","","","","");
INSERT INTO accounts VALUES("61","60","37","Customer Unni","0","Credit","0","2016-08-18","64","16","3","","Unni","Kakkodi","673001","Calicut","0495 26565655","9874563211","unni@gmail.com","Kakkodi","673211","Calicut");
INSERT INTO accounts VALUES("62","60","40","Supplier Abi","0","Credit","0","2016-08-18","64","16","3","","Abi","Kakkodi","","","","9874563200","","","","");
INSERT INTO accounts VALUES("63","0","48","alisha agent","0","Credit","0","2016-08-20","70","19","3","","alisha agent","dfdg","","","","7898789878","","","","");
INSERT INTO accounts VALUES("64","63","37","alisha cus","0","Credit","0","2016-08-20","70","19","3","","alisha","dfgd","567656","kozhikode","0498787989","9876567656","alimolvavd@gmail.com","dgfd","456545","dfgd");
INSERT INTO accounts VALUES("65","63","40","alisha supplier","0","Credit","0","2016-08-20","70","19","3","","","","","","","5676567656","","","","");
INSERT INTO accounts VALUES("66","0","37","sundaycustomer1","0","Credit","0","2016-08-21","72","21","3","","cust","","234345","ert","123","234","fccfc@g.com","","","");
INSERT INTO accounts VALUES("67","0","40","sundaysuplier","0","Credit","0","2016-08-21","72","21","3","","","","","","","","","","","");

DROP TABLE IF EXISTS airline_flight;
CREATE TABLE `airline_flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlinePassId` int(11) NOT NULL,
  `airlineName` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `journeyDate` date NOT NULL,
  `sectorFrom` varchar(100) NOT NULL,
  `sectorTo` varchar(100) NOT NULL,
  `classType` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `tax` varchar(30) NOT NULL,
  `yq` float NOT NULL,
  `procCharge` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlinePassId` (`airlinePassId`),
  CONSTRAINT `airline_flight_ibfk_2` FOREIGN KEY (`airlinePassId`) REFERENCES `airline_passenger` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
INSERT INTO airline_flight VALUES("1","1","1","rf433","uy677","2016-08-19","trerr","hggfg","F","1000","100","10","10","16","63","3");
INSERT INTO airline_flight VALUES("2","2","1","1234A","1234A","2016-08-19","DL","KL","Business","500","10","100","100","16","64","3");
INSERT INTO airline_flight VALUES("3","3","2","er455","rt544","2016-08-19","fgf","kkk","F","2000","100","10","100","16","63","3");
INSERT INTO airline_flight VALUES("5","5","","222","02022222","2016-08-26","cok","maa","e","10000","100","0","1000","18","66","3");
INSERT INTO airline_flight VALUES("6","6","3","222","5665566","2016-08-31","CCJ","DMM","E","7000","100","0","500","18","66","3");
INSERT INTO airline_flight VALUES("7","7","1","1234az","123","2016-08-11","DL","KL","Bussiness  Class","500","","100","100","16","64","3");
INSERT INTO airline_flight VALUES("8","8","","3","4","2016-08-31","sdf","sdf","sd","34","34","34","3","19","70","3");
INSERT INTO airline_flight VALUES("9","9","4","er443","tckt566","2016-08-15","rt","ff","F","100","100","100","100","21","72","3");
INSERT INTO airline_flight VALUES("10","10","5","dfgd455","fdfg45","2016-08-16","ghh","yuuuu","R","100","10","10","10","21","72","3");

DROP TABLE IF EXISTS airline_passenger;
CREATE TABLE `airline_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `airlineId` int(11) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationship` varchar(30) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `prefix` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `airlineId` (`airlineId`),
  CONSTRAINT `airline_passenger_ibfk_1` FOREIGN KEY (`airlineId`) REFERENCES `invoice_airline` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
INSERT INTO airline_passenger VALUES("1","28","Sernamcust1pasngr1","2006-08-08","10","M","rty","father","Indian","yes","wer433","2011-08-17","2019-08-21","ert","16","63","3","Mrs");
INSERT INTO airline_passenger VALUES("2","29","Thajucust2pasngr1","1996-08-13","20","M","","husband","Indian","yes","gfh554","2016-08-01","2023-08-16","fhfhf","16","64","3","Mr");
INSERT INTO airline_passenger VALUES("3","30","Sernamcustpangr1","2016-08-01","0","M","","son","Indian","son","dd","2016-08-02","2025-08-27","rty","16","63","3","Mrs");
INSERT INTO airline_passenger VALUES("5","31","Siraj","1970-08-01","46","M","","","Indian","","k2333333","2016-08-18","2026-08-17","","18","66","3","Mr");
INSERT INTO airline_passenger VALUES("6","32","Basith","2006-08-18","10","M","","","Indian","","K255222","2016-08-01","2017-08-01","","18","66","3","Mr");
INSERT INTO airline_passenger VALUES("7","33","Beeran","1956-08-15","60","M","","husband","Indian","","1234BR","2016-08-10","2017-03-17","Kakkodi","16","64","3","Mr");
INSERT INTO airline_passenger VALUES("8","34","Alisha Pass","1986-08-14","30","M","","mother","Indian","","23","1998-08-14","2021-08-26","","19","70","3","Mrs");
INSERT INTO airline_passenger VALUES("9","35","Sundaypasngr1","1999-08-18","17","M","thretpasp123","brother","Indian","","ps45","2007-08-23","2026-08-12","ghrt","21","72","3","Mr");
INSERT INTO airline_passenger VALUES("10","36","Sundaypasngr1","1999-08-18","17","M","thretpasp123","brother","Indian","","ps45","2007-08-23","2026-08-12","ghrt","21","72","3","Ms");

DROP TABLE IF EXISTS cash_payment;
CREATE TABLE `cash_payment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `discount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
INSERT INTO cash_payment VALUES("1","1","visa","1","14200","0","","0","CASH","18","66","3");
INSERT INTO cash_payment VALUES("2","1","airline","1","1240","0","","0","CASH","16","63","3");
INSERT INTO cash_payment VALUES("3","1","hotel","1","660","0","","0","CASH","16","63","3");
INSERT INTO cash_payment VALUES("4","1","miscellaneous","1","110","0","","0","CASH","16","63","3");
INSERT INTO cash_payment VALUES("5","1","visa","3","1700","0","","0","CASH","16","63","3");
INSERT INTO cash_payment VALUES("6","1","airline","3","2500","0","","0","BANK","16","63","3");
INSERT INTO cash_payment VALUES("7","1","miscellaneous","2","110","0","","0","BANK","16","63","3");
INSERT INTO cash_payment VALUES("8","1","transportation","1","5500","0","","0","BANK","16","63","3");
INSERT INTO cash_payment VALUES("9","1","visa","2","1200","0","","0","BANK","16","63","3");

DROP TABLE IF EXISTS cash_receipt;
CREATE TABLE `cash_receipt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(30) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `payingAmount` float NOT NULL,
  `discount` float NOT NULL,
  `remark` text NOT NULL,
  `cheque` varchar(20) NOT NULL,
  `paymentType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
INSERT INTO cash_receipt VALUES("1","1","airline","1","11100","0","","0","CASH","18","66","3");
INSERT INTO cash_receipt VALUES("2","1","airline","3","2600","0","","0","CASH","16","63","3");
INSERT INTO cash_receipt VALUES("3","1","miscellaneous","1","200","0","","0","CASH","16","63","3");
INSERT INTO cash_receipt VALUES("4","1","visa","2","1300","0","","0","CASH","16","63","3");
INSERT INTO cash_receipt VALUES("5","2","transportation","1","6000","0","","0","CASH","16","63","3");
INSERT INTO cash_receipt VALUES("6","3","hotel","2","800","0","","0","CASH","16","63","3");
INSERT INTO cash_receipt VALUES("7","3","miscellaneous","2","200","0","","0","CASH","16","63","3");
INSERT INTO cash_receipt VALUES("8","4","airline","2","910","0","","0","CASH","16","63","3");
INSERT INTO cash_receipt VALUES("9","1","hotel","3","900","0","","0","BANK","16","63","3");
INSERT INTO cash_receipt VALUES("10","2","transportation","2","5000","0","","0","BANK","16","63","3");
INSERT INTO cash_receipt VALUES("11","5","package","1","10000","0","","0","CASH","16","63","3");

DROP TABLE IF EXISTS company;
CREATE TABLE `company` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(20) NOT NULL,
  `city` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `tds` int(11) NOT NULL,
  `circle` varchar(100) NOT NULL,
  `resPerson` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `remark` text NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
INSERT INTO company VALUES("2","Mtm Travel Service","","673004","KOZHIKODE","04954011515","8547116314","mtmvisa@gmail.com","","0","","","","","0");
INSERT INTO company VALUES("13","Rameez","","234433","","","2333","","","0","","","","","3");
INSERT INTO company VALUES("14","Ajnas","","232211","","","3323","","","0","","","","","3");
INSERT INTO company VALUES("16","Sernam","","223322","","","453367bb","","","0","","","","","3");
INSERT INTO company VALUES("17","Shuroof","","123233","","","433322","","","0","","","","","3");
INSERT INTO company VALUES("18","Yarmook","","","","","8","","","0","","","","","3");
INSERT INTO company VALUES("19","C1s1","","","","","456546456","","","0","","","","","3");
INSERT INTO company VALUES("20","New Company","","","","","","","","0","","","","","3");
INSERT INTO company VALUES("21","Sundaycompany","","342535","fdgre","04962560440","99956462","","","0","","","","","3");

DROP TABLE IF EXISTS flight;
CREATE TABLE `flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `flightNo` varchar(50) NOT NULL,
  `flightName` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
INSERT INTO flight VALUES("1","0","Indigo","","16","63","3");
INSERT INTO flight VALUES("2","0","Air Asia","","16","63","3");
INSERT INTO flight VALUES("3","0","Air India","../../uploads/flight/20160818184533.jpg","18","66","3");
INSERT INTO flight VALUES("4","0","Indigo Airline","","21","72","3");
INSERT INTO flight VALUES("5","0","Suv","","21","72","3");

DROP TABLE IF EXISTS hotel;
CREATE TABLE `hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `pin` varchar(10) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO hotel VALUES("1","Raviz Kadavu","","679588","Wert","State","India","0126","333665","","","16","63","3");
INSERT INTO hotel VALUES("2","Viavanta Taj","","","","","India","4545","","","","16","63","3");
INSERT INTO hotel VALUES("3","Rk","","748952","","","India","","","","","21","72","3");

DROP TABLE IF EXISTS hotel_customer;
CREATE TABLE `hotel_customer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hotelInvoiceId` int(11) NOT NULL,
  `roomType` varchar(20) NOT NULL,
  `noOfRooms` int(11) NOT NULL,
  `noOfPersons` int(11) NOT NULL,
  `rate` float NOT NULL,
  `subTax` varchar(30) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
INSERT INTO hotel_customer VALUES("1","1","1","1","1","750","10","760","16","64","3");
INSERT INTO hotel_customer VALUES("5","3","1","1","5","500","100","600","16","63","3");
INSERT INTO hotel_customer VALUES("6","2","5","1","1","500","100","600","16","63","3");

DROP TABLE IF EXISTS invoice_airline;
CREATE TABLE `invoice_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float NOT NULL,
  `mainProcCharge` float NOT NULL,
  `mainOtherCharge` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `mainTax` varchar(30) NOT NULL,
  `discount` float NOT NULL,
  `pnr` varchar(20) NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `ticketDetails` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `customerKey` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
INSERT INTO invoice_airline VALUES("28","1","2016-08-18","48","10","10","100","","","70","10","0","1300","Pending","","","16","63","3","28IA#148");
INSERT INTO invoice_airline VALUES("29","2","2016-08-18","53","100","100","100","","","0","100","0","910","Pending","","","16","64","3","29IA#253");
INSERT INTO invoice_airline VALUES("30","3","2016-08-18","48","100","100","100","","","100","10","0","2600","Pending","","","16","63","3","30IA#348");
INSERT INTO invoice_airline VALUES("31","1","2016-08-18","55","0","0","0","","","0","0","0","11100","Pending","","","18","66","3","31IA#155");
INSERT INTO invoice_airline VALUES("32","2","2016-08-18","58","0","0","0","","","0","0","0","7600","Pending","","","18","66","3","32IA#258");
INSERT INTO invoice_airline VALUES("33","4","2016-08-18","61","100","100","100","","","0","0","0","1000","Pending","","","16","64","3","33IA#461");
INSERT INTO invoice_airline VALUES("34","1","2016-08-20","64","3","3","0","","","3","4","3","110","Pending","","","19","70","3","34IA#164");
INSERT INTO invoice_airline VALUES("35","1","2016-08-21","66","0","0","0","","","0","0","0","400","Pending","","","21","72","3","35IA#166");
INSERT INTO invoice_airline VALUES("36","2","2016-08-21","66","100","100","100","","","100","100","0","430","Pending","","","21","72","3","36IA#266");

DROP TABLE IF EXISTS invoice_hotel;
CREATE TABLE `invoice_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `hotelId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `noOfNight` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `checkInDate` date NOT NULL,
  `checkInTime` varchar(20) NOT NULL,
  `checkOutDate` date NOT NULL,
  `checkOutTime` varchar(20) NOT NULL,
  `billingIns` varchar(100) NOT NULL,
  `reference` varchar(100) NOT NULL,
  `tax` varchar(30) NOT NULL,
  `tds` float NOT NULL,
  `otherCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
INSERT INTO invoice_hotel VALUES("1","1","2016-08-18","1","53","1","1","0","1","2016-08-19","","2016-08-20","","none","","0","100","100","0","960","Pending","","16","64","3");
INSERT INTO invoice_hotel VALUES("2","2","2016-08-18","2","52","1","2","0","2","2016-08-18","","2016-08-19","","none","","100","100","100","100","800","Pending","","16","63","3");
INSERT INTO invoice_hotel VALUES("3","3","2016-08-18","1","52","1","5","7","12","2016-08-18","","2016-08-19","","none","","100","200","100","100","900","Pending","","16","63","3");

DROP TABLE IF EXISTS invoice_misc;
CREATE TABLE `invoice_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `basicAmount` float NOT NULL,
  `tds` float NOT NULL,
  `tax` varchar(20) NOT NULL,
  `otherCharge` float NOT NULL,
  `procCharge` float NOT NULL,
  `discount` float NOT NULL,
  `netAmount` float NOT NULL,
  `status` varchar(20) NOT NULL,
  `narration` int(11) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
INSERT INTO invoice_misc VALUES("1","1","48","2016-08-18","2016-08-18","rameees","","100","10","40","30","20","0","200","Pending","1","","16","63","3");
INSERT INTO invoice_misc VALUES("2","2","52","2016-08-18","2016-08-18","rtt","","100","0","60","0","40","0","200","Pending","0","","16","63","3");

DROP TABLE IF EXISTS invoice_package;
CREATE TABLE `invoice_package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `packageId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `tds` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `proCharge` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `discount` float DEFAULT '0',
  `netAmount` float NOT NULL DEFAULT '0',
  `description` varchar(200) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package VALUES("1","1","2016-08-20","1","48","100","100","100","100","100","11500","","16","63","3");
INSERT INTO invoice_package VALUES("2","2","2016-08-20","2","52","100","200","600","1000","100","14000","","16","63","3");
INSERT INTO invoice_package VALUES("3","3","2016-08-20","1","61","100","100","100","100","100","12500","","16","63","3");
INSERT INTO invoice_package VALUES("4","4","2016-08-20","1","61","100","100","100","200","100","6500","","16","63","3");

DROP TABLE IF EXISTS invoice_package_airline;
CREATE TABLE `invoice_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `ticketNo` varchar(100) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_airline VALUES("1","1","9","","800","16","63","3");
INSERT INTO invoice_package_airline VALUES("2","1","10","","800","16","63","3");
INSERT INTO invoice_package_airline VALUES("3","2","11","","1300","16","63","3");
INSERT INTO invoice_package_airline VALUES("4","2","12","","1300","16","63","3");
INSERT INTO invoice_package_airline VALUES("5","3","13","","1300","16","63","3");
INSERT INTO invoice_package_airline VALUES("6","3","14","","1300","16","63","3");
INSERT INTO invoice_package_airline VALUES("7","4","15","","1300","16","63","3");

DROP TABLE IF EXISTS invoice_package_hotel;
CREATE TABLE `invoice_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `roomType` varchar(100) NOT NULL,
  `bedStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_hotel VALUES("1","1","9","2","0","1500","16","63","3");
INSERT INTO invoice_package_hotel VALUES("2","1","10","2","0","1500","16","63","3");
INSERT INTO invoice_package_hotel VALUES("3","2","11","2","0","1000","16","63","3");
INSERT INTO invoice_package_hotel VALUES("4","2","12","2","0","1000","16","63","3");
INSERT INTO invoice_package_hotel VALUES("5","3","13","2","0","1500","16","63","3");
INSERT INTO invoice_package_hotel VALUES("6","3","14","2","0","1500","16","63","3");
INSERT INTO invoice_package_hotel VALUES("7","4","15","2","0","1500","16","63","3");

DROP TABLE IF EXISTS invoice_package_passenger;
CREATE TABLE `invoice_package_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `packageId` int(11) NOT NULL,
  `prefix` varchar(20) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` date DEFAULT NULL,
  `age` int(11) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `birthPlace` varchar(100) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(50) NOT NULL,
  `dateOfIssue` date DEFAULT NULL,
  `validUpTo` date DEFAULT NULL,
  `issuePlace` varchar(100) NOT NULL,
  `mofa` varchar(50) NOT NULL,
  `emofa` varchar(50) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_passenger VALUES("1","1","1","","Sernamcust1pasngr1","M","2006-08-08","10","Indian","rty","10","","wer433","2011-08-17","2019-08-21","ert","","","5600","16","63","3");
INSERT INTO invoice_package_passenger VALUES("2","1","1","","Sernamcust1pasngr1","M","2006-08-08","10","Indian","rty","11","Sernamcust1pasngr1","wer433","2011-08-17","2019-08-21","ert","","","5600","16","63","3");
INSERT INTO invoice_package_passenger VALUES("3","2","2","Mrs","haseena","M","1988-08-10","28","Indian","kochi","","","rf332","2008-08-07","2019-08-17","clt","","","6100","16","63","3");
INSERT INTO invoice_package_passenger VALUES("4","2","2","","aisho","F","2000-08-17","16","Indian","df","11","haseena","df344","2013-08-07","2025-08-14","fgt","","","6100","16","63","3");
INSERT INTO invoice_package_passenger VALUES("5","3","1","Ms","jasim","M","1996-08-21","20","Indian","fkr","14","","fd33","2008-08-07","2026-08-13","dft","","","6100","16","63","3");
INSERT INTO invoice_package_passenger VALUES("6","3","1","Mrs","husna","F","1997-08-15","19","Indian","df","11","jasim","df333","2007-08-08","2026-08-06","ftg","","","6100","16","63","3");
INSERT INTO invoice_package_passenger VALUES("7","4","1","","A","M","1999-08-05","17","Indian","dd","","","df44","2007-08-14","2023-08-10","ffgg","","","6100","16","63","3");

DROP TABLE IF EXISTS invoice_package_transportation;
CREATE TABLE `invoice_package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `seatStatus` int(11) NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_transportation VALUES("1","1","9","1","1500","16","63","3");
INSERT INTO invoice_package_transportation VALUES("2","1","10","1","1500","16","63","3");
INSERT INTO invoice_package_transportation VALUES("3","2","11","1","1500","16","63","3");
INSERT INTO invoice_package_transportation VALUES("4","2","12","1","1500","16","63","3");
INSERT INTO invoice_package_transportation VALUES("5","3","13","1","1500","16","63","3");
INSERT INTO invoice_package_transportation VALUES("6","3","14","1","1500","16","63","3");
INSERT INTO invoice_package_transportation VALUES("7","4","15","1","1500","16","63","3");

DROP TABLE IF EXISTS invoice_package_visa;
CREATE TABLE `invoice_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `total` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
INSERT INTO invoice_package_visa VALUES("1","1","9","0000-00-00","0000-00-00","0000-00-00","1500","16","63","3");
INSERT INTO invoice_package_visa VALUES("2","1","10","0000-00-00","0000-00-00","0000-00-00","1500","16","63","3");
INSERT INTO invoice_package_visa VALUES("3","2","11","0000-00-00","0000-00-00","0000-00-00","2000","16","63","3");
INSERT INTO invoice_package_visa VALUES("4","2","12","0000-00-00","0000-00-00","0000-00-00","2000","16","63","3");
INSERT INTO invoice_package_visa VALUES("5","3","13","0000-00-00","0000-00-00","0000-00-00","1500","16","63","3");
INSERT INTO invoice_package_visa VALUES("6","3","14","0000-00-00","0000-00-00","0000-00-00","1500","16","63","3");
INSERT INTO invoice_package_visa VALUES("7","4","15","0000-00-00","0000-00-00","0000-00-00","1500","16","63","3");

DROP TABLE IF EXISTS invoice_transport;
CREATE TABLE `invoice_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `serviceProviderId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `adults` int(11) NOT NULL,
  `childrens` int(11) NOT NULL,
  `totalPersons` int(11) NOT NULL,
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `totalDays` int(11) NOT NULL,
  `vehicleType` varchar(100) NOT NULL,
  `kmAllowed` float NOT NULL,
  `arrivalDetails` text NOT NULL,
  `pickUpFrom` varchar(100) NOT NULL,
  `pickUpTime` varchar(20) NOT NULL,
  `DepartureDetails` text NOT NULL,
  `dropAt` varchar(100) NOT NULL,
  `dropTime` varchar(20) NOT NULL,
  `route` text NOT NULL,
  `inclusive` varchar(100) NOT NULL,
  `fare` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
INSERT INTO invoice_transport VALUES("1","1","2016-08-18","4","49","4","0","4","2016-08-18","2016-08-26","8","6","0","","","","","","","","","5000","100","900","6000","","Pending","","16","63","3");
INSERT INTO invoice_transport VALUES("2","2","2016-08-18","6","52","30","15","45","2016-08-18","2016-08-22","4","8","0","","","","","","","","","7500","500","1000","9000","","Pending","","16","63","3");

DROP TABLE IF EXISTS invoice_visa;
CREATE TABLE `invoice_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `customerId` int(11) NOT NULL,
  `mainOtherCharge` double NOT NULL,
  `discount` varchar(50) NOT NULL,
  `netAmount` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `document` text NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `customerKey` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO invoice_visa VALUES("1","1","2016-08-18","58","0","0","14800","","","Pending","","18","66","3","1IV#158");
INSERT INTO invoice_visa VALUES("2","1","2016-08-18","49","0","0","1300","","","Pending","","16","63","3","2IV#149");
INSERT INTO invoice_visa VALUES("3","2","2016-08-18","48","0","0","1300","","","Pending","","16","63","3","3IV#248");
INSERT INTO invoice_visa VALUES("4","3","2016-08-18","53","0","0","2000","","","Pending","","16","63","3","4IV#353");

DROP TABLE IF EXISTS log;
CREATE TABLE `log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(50) NOT NULL,
  `tableId` int(11) NOT NULL,
  `invoiceNo` int(11) NOT NULL,
  `changeDate` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `oldAmt` float NOT NULL,
  `newAmt` float NOT NULL,
  `description` text NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO log VALUES("1","Invoice Hotel","3","3","2016-08-20","Edit","900","900","NetAmt:900,Discount:100,Tds:200,MainTax:100","63","16","3");
INSERT INTO log VALUES("2","Invoice Hotel","2","2","2016-08-20","Edit","800","800","NetAmt:800,Discount:100,Tds:100,MainTax:100","63","16","3");

DROP TABLE IF EXISTS login;
CREATE TABLE `login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;
INSERT INTO login VALUES("1","0","admin","81dc9bdb52d04dc20036dbd8313ed055","Admin","Active","0");
INSERT INTO login VALUES("51","13","staff3","5e55c63055cb2dc905db1c9c689f2c16","Staff","","0");
INSERT INTO login VALUES("56","13","rameez","4a7d1ed414474e4033ac29ccb8653d9b","Branch","","0");
INSERT INTO login VALUES("57","14","ajnas","4a7d1ed414474e4033ac29ccb8653d9b","Branch","","0");
INSERT INTO login VALUES("59","16","sernam","4a7d1ed414474e4033ac29ccb8653d9b","Branch","","0");
INSERT INTO login VALUES("60","17","shuroof","1dfddcce785b47784b1e97090f4feafd","Branch","Active","0");
INSERT INTO login VALUES("61","14","rayeez","4a7d1ed414474e4033ac29ccb8653d9b","Staff","","0");
INSERT INTO login VALUES("62","15","pharvees","4a7d1ed414474e4033ac29ccb8653d9b","Staff","","0");
INSERT INTO login VALUES("63","16","thaju","4a7d1ed414474e4033ac29ccb8653d9b","Staff","Blocked","0");
INSERT INTO login VALUES("64","17","minnu","4a7d1ed414474e4033ac29ccb8653d9b","Staff","","0");
INSERT INTO login VALUES("65","18","test","098f6bcd4621d373cade4e832627b4f6","Branch","","0");
INSERT INTO login VALUES("66","18","test1","5a105e8b9d40e1329780d62ea2265d8a","Staff","","0");
INSERT INTO login VALUES("67","19","c1s1","9948808d4e268c04a6fededcd84b356f","Branch","Active","3");
INSERT INTO login VALUES("68","20","company","4a7d1ed414474e4033ac29ccb8653d9b","Branch","Active","3");
INSERT INTO login VALUES("69","19","staff","4a7d1ed414474e4033ac29ccb8653d9b","Staff","Blocked","3");
INSERT INTO login VALUES("70","20","alisha","dddf0ac818211584c38514bd95eca175","Staff","Active","3");
INSERT INTO login VALUES("71","21","sundaycompany","4a7d1ed414474e4033ac29ccb8653d9b","Branch","Active","3");
INSERT INTO login VALUES("72","21","sundaystaff1","4a7d1ed414474e4033ac29ccb8653d9b","Staff","Active","3");
INSERT INTO login VALUES("73","22","sundaystaff2","4a7d1ed414474e4033ac29ccb8653d9b","Staff","Active","3");

DROP TABLE IF EXISTS misc_narration;
CREATE TABLE `misc_narration` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `narration` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
INSERT INTO misc_narration VALUES("1","Travel","16","64","3");
INSERT INTO misc_narration VALUES("2","Travel1","16","64","3");
INSERT INTO misc_narration VALUES("3","Dancebar","21","72","3");
INSERT INTO misc_narration VALUES("4","Shopping","21","72","3");
INSERT INTO misc_narration VALUES("5","Suvier","21","73","3");
INSERT INTO misc_narration VALUES("6","Suv","21","73","3");

DROP TABLE IF EXISTS other_purchase;
CREATE TABLE `other_purchase` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO other_purchase VALUES("1","1","2016-08-18","50","1000","0","10","20","10","60","0","0","1100","","","0","16","63","3");
INSERT INTO other_purchase VALUES("2","1","2016-08-21","67","1000","0","100","100","100","1000","100","100","2400","","","100","21","72","3");

DROP TABLE IF EXISTS package;
CREATE TABLE `package` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageName` varchar(200) NOT NULL,
  `staff` int(11) NOT NULL,
  `leader` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `place` varchar(100) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `tds` float DEFAULT '0',
  `otherCharge` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `remark` varchar(100) NOT NULL,
  `services` varchar(100) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `confirm` int(11) NOT NULL,
  `discount` float NOT NULL,
  `extra` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO package VALUES("1","Singapore","16","Ravi","2323","2016-08-21","site seeing","5","100","100","100","","airline,hotel,visa,transportation","4600","4100","2200","16","59","3","1","0","0");
INSERT INTO package VALUES("2","Malasiya","16","Rahim","98767","2016-08-21","site seeing","5","100","100","100","","airline,hotel,visa,transportation","5100","4600","2700","16","59","3","1","0","0");
INSERT INTO package VALUES("3","Goa","16","Njan","98878","2016-08-21","trip","4","100","100","100","","airline,hotel,visa,transportation","4600","4100","2200","16","59","3","1","0","0");

DROP TABLE IF EXISTS package_airline;
CREATE TABLE `package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `yq` float NOT NULL DEFAULT '0',
  `procCharge` float NOT NULL DEFAULT '0',
  `tax` float NOT NULL DEFAULT '0',
  `adultCharge` float NOT NULL,
  `childCharge` float NOT NULL,
  `infantCharge` float NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `packageId` (`packageId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO package_airline VALUES("2","2","100","100","100","1000","500","100","1300","800","400","","16","59");
INSERT INTO package_airline VALUES("3","1","100","100","100","1000","500","100","1300","800","400","","16","59");
INSERT INTO package_airline VALUES("4","3","100","100","100","1000","500","100","1300","800","400","","16","59");

DROP TABLE IF EXISTS package_airline_flights;
CREATE TABLE `package_airline_flights` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_airlineId` int(11) NOT NULL,
  `airline` varchar(100) NOT NULL,
  `flightNo` varchar(100) NOT NULL,
  `sectorFrom` varchar(200) NOT NULL,
  `sectorTo` varchar(200) NOT NULL,
  `jDate` date NOT NULL,
  `airTime` varchar(20) NOT NULL,
  `class` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO package_airline_flights VALUES("3","1","3","1","fl23","kochi","dubai","2016-08-21","","F","16","59");
INSERT INTO package_airline_flights VALUES("2","2","2","2","AA122","chennai","malasiya","2016-08-21","","F","16","59");
INSERT INTO package_airline_flights VALUES("4","3","4","1","indg433","df","we","2016-08-21","","F","16","59");

DROP TABLE IF EXISTS package_hotel;
CREATE TABLE `package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `hotelId` int(11) NOT NULL,
  `checkinDate` date DEFAULT NULL,
  `checkoutDate` date DEFAULT NULL,
  `noOfDays` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO package_hotel VALUES("3","1","2","2016-08-22","2016-08-26","4","16","59","62");
INSERT INTO package_hotel VALUES("2","2","1","2016-08-23","2016-08-25","2","16","59","62");
INSERT INTO package_hotel VALUES("4","3","0","","","0","16","59","0");

DROP TABLE IF EXISTS package_room_type;
CREATE TABLE `package_room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `package_hotelId` int(11) NOT NULL,
  `roomId` int(11) NOT NULL,
  `rate` float NOT NULL,
  `otherCharge` float NOT NULL,
  `total` float NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
INSERT INTO package_room_type VALUES("24","1","3","8","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("23","1","3","7","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("22","1","3","6","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("21","1","3","5","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("20","1","3","4","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("19","1","3","3","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("18","1","3","2","1000","500","1500","1000","16","59");
INSERT INTO package_room_type VALUES("17","1","3","1","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("9","2","2","1","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("10","2","2","2","1000","0","1000","800","16","59");
INSERT INTO package_room_type VALUES("11","2","2","3","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("12","2","2","4","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("13","2","2","5","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("14","2","2","6","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("15","2","2","7","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("16","2","2","8","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("25","3","4","1","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("26","3","4","2","1500","0","1500","1000","16","59");
INSERT INTO package_room_type VALUES("27","3","4","3","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("28","3","4","4","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("29","3","4","5","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("30","3","4","6","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("31","3","4","7","0","0","0","0","16","59");
INSERT INTO package_room_type VALUES("32","3","4","8","0","0","0","0","16","59");

DROP TABLE IF EXISTS package_transportation;
CREATE TABLE `package_transportation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `vehicle` int(11) NOT NULL,
  `seatCharge` float NOT NULL,
  `remark` varchar(100) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO package_transportation VALUES("3","1","8","1500","","62","1000","16","59");
INSERT INTO package_transportation VALUES("2","2","8","1500","","62","1000","16","59");
INSERT INTO package_transportation VALUES("4","3","7","1500","","51","1000","16","59");

DROP TABLE IF EXISTS package_visa;
CREATE TABLE `package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `visaFor` varchar(100) NOT NULL,
  `visaFee` float NOT NULL DEFAULT '0',
  `vfs` float NOT NULL DEFAULT '0',
  `ddCharge` float NOT NULL DEFAULT '0',
  `serviceCharge` float NOT NULL DEFAULT '0',
  `otherCharge` float NOT NULL DEFAULT '0',
  `courierCharge` float NOT NULL DEFAULT '0',
  `total` float NOT NULL DEFAULT '0',
  `supplierId` int(11) NOT NULL,
  `purchaseAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO package_visa VALUES("3","1","5","1000","100","100","100","0","100","1500","62","1000","16","59");
INSERT INTO package_visa VALUES("2","2","6","1500","100","100","100","0","100","2000","62","1500","16","59");
INSERT INTO package_visa VALUES("4","3","6","1000","100","100","100","0","100","1500","0","1000","16","59");

DROP TABLE IF EXISTS passenger;
CREATE TABLE `passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `prefix` varchar(20) NOT NULL,
  `passengerName` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `placeOfBirth` varchar(30) NOT NULL,
  `relationshipId` int(11) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `passportNo` varchar(20) NOT NULL,
  `dateOfIssue` date NOT NULL,
  `validUpTo` date NOT NULL,
  `issuePlace` varchar(50) NOT NULL,
  `mofa` varchar(50) NOT NULL,
  `emofa` varchar(50) NOT NULL,
  `passPhoto` varchar(200) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
INSERT INTO passenger VALUES("1","40","Mr","Refwf","2007-08-07","9","F","","3","Indian","yes","sfs","0000-00-00","0000-00-00","","","","","13","62","3");
INSERT INTO passenger VALUES("2","41","","Pasngr1","1988-08-11","28","M","","0","Indian","","","0000-00-00","0000-00-00","","","","","13","62","3");
INSERT INTO passenger VALUES("3","48","Mr","Sernamcust1pasngr1","2006-08-08","10","M","rty","12","Indian","yes","wer433","2011-08-17","2019-08-21","ert","er","vbb","","16","63","3");
INSERT INTO passenger VALUES("4","53","Mrs","Thajucust2pasngr1","1996-08-13","20","M","","10","Indian","yes","gfh554","2016-08-01","2023-08-16","fhfhf","hfh","hddj","","16","63","3");
INSERT INTO passenger VALUES("5","48","","Sernamcustpangr1","2016-08-01","0","M","","13","Indian","son","dd","2016-08-02","2025-08-27","rty","fggf","ddf","","16","63","3");
INSERT INTO passenger VALUES("6","49","Ms","Sernamcust2psngr2","2006-08-06","10","F","cc","12","Indian","father","xx","2008-08-28","2025-08-19","xfdd","fgr","retf","","16","63","3");
INSERT INTO passenger VALUES("7","61","Mrs","Aamina","1966-08-04","50","F","","11","Indian","","1234AM","2016-08-09","2016-12-15","Kakkodi","","","","16","64","3");
INSERT INTO passenger VALUES("8","61","Mr","Beeran","1956-08-15","60","M","","10","Indian","","1234BR","2016-08-10","2017-03-17","Kakkodi","","","uploads/passenger/1234BR.jpg","16","64","3");
INSERT INTO passenger VALUES("9","48","","Sernamcust1pasngr1","2006-08-08","10","M","rty","10","Indian","","wer433","2011-08-17","2019-08-21","ert","","","","16","63","3");
INSERT INTO passenger VALUES("10","48","","Sernamcust1pasngr1","2006-08-08","10","M","rty","11","Indian","Sernamcust1pasngr1","wer433","2011-08-17","2019-08-21","ert","","","","16","63","3");
INSERT INTO passenger VALUES("11","52","Mrs","haseena","1988-08-10","28","M","kochi","0","Indian","","rf332","2008-08-07","2019-08-17","clt","","","","16","63","3");
INSERT INTO passenger VALUES("12","52","","aisho","2000-08-17","16","F","df","11","Indian","haseena","df344","2013-08-07","2025-08-14","fgt","","","","16","63","3");
INSERT INTO passenger VALUES("13","61","Ms","jasim","1996-08-21","20","M","fkr","14","Indian","","fd33","2008-08-07","2026-08-13","dft","","","","16","63","3");
INSERT INTO passenger VALUES("14","61","Mrs","husna","1997-08-15","19","F","df","11","Indian","jasim","df333","2007-08-08","2026-08-06","ftg","","","","16","63","3");
INSERT INTO passenger VALUES("15","61","","A","1999-08-05","17","M","dd","0","Indian","","df44","2007-08-14","2023-08-10","ffgg","","","","16","63","3");
INSERT INTO passenger VALUES("16","64","Mr","Alisha Pass","1986-08-14","30","M","","15","Indian","","23","0000-00-00","0000-00-00","","","","","19","70","3");
INSERT INTO passenger VALUES("17","64","Mrs","Alisha Pass","1986-08-14","30","M","","0","Indian","","23","1998-08-14","2021-08-26","","","","","19","70","3");
INSERT INTO passenger VALUES("18","66","Mr","Sundaypasngr1","1999-08-18","17","M","thretpasp123","16","Indian","","ps45","2007-08-23","2026-08-12","ghrt","ght45","hfhh555","","21","72","3");
INSERT INTO passenger VALUES("19","66","Mr","Sundaypasngr1","1999-08-18","17","M","thretpasp123","0","Indian","","ps45","2007-08-23","2026-08-12","ghrt","","","","21","72","3");
INSERT INTO passenger VALUES("20","66","Ms","Sundaypasngr1","1999-08-18","17","M","thretpasp123","0","Indian","","ps45","2007-08-23","2026-08-12","ghrt","","","","21","72","3");

DROP TABLE IF EXISTS purchase_airline;
CREATE TABLE `purchase_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
INSERT INTO purchase_airline VALUES("1","1","2016-08-18","2016-08-18","50","5","1","1110","0","100","10","100","10","10","0","1240","","","100","0","16","63","3");
INSERT INTO purchase_airline VALUES("2","2","2016-08-18","2016-08-18","50","5","2","610","0","0","0","0","0","0","0","610","","","0","0","16","64","3");
INSERT INTO purchase_airline VALUES("3","1","2016-08-18","2016-08-18","59","5","2","7100","0","0","0","0","0","0","0","7100","","","0","0","18","66","3");
INSERT INTO purchase_airline VALUES("4","3","2016-08-18","2016-08-18","51","5","3","2110","0","200","100","100","0","0","0","2500","","","10","0","16","63","3");
INSERT INTO purchase_airline VALUES("5","4","2016-08-18","2016-08-18","62","5","4","600","0","0","0","0","0","0","0","600","","","0","0","16","64","3");
INSERT INTO purchase_airline VALUES("6","1","2016-08-20","2016-08-20","65","5","1","102","3","3","3","4","0","0","0","89","","","23","0","19","70","3");
INSERT INTO purchase_airline VALUES("7","1","2016-08-21","2016-08-21","67","5","1","300","0","100","100","100","100","100","100","400","","","500","0","21","72","3");
INSERT INTO purchase_airline VALUES("8","2","2016-08-21","2016-08-21","67","5","1","300","0","100","100","100","100","100","100","400","","","500","0","21","72","3");
INSERT INTO purchase_airline VALUES("9","3","2016-08-21","2016-08-21","67","5","2","120","0","0","0","0","0","0","0","120","","","0","0","21","72","3");

DROP TABLE IF EXISTS purchase_hotel;
CREATE TABLE `purchase_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO purchase_hotel VALUES("1","1","2016-08-18","2016-08-18","50","11","1","760","0","0","0","0","0","0","0","660","","","100","0","16","64","3");
INSERT INTO purchase_hotel VALUES("2","2","2016-08-18","2016-08-20","50","11","2","600","0","100","100","100","100","0","0","700","","","300","0","16","63","3");
INSERT INTO purchase_hotel VALUES("3","3","2016-08-18","2016-08-20","50","11","3","600","0","100","10","10","50","0","30","700","","","100","0","16","63","3");

DROP TABLE IF EXISTS purchase_misc;
CREATE TABLE `purchase_misc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO purchase_misc VALUES("1","1","2016-08-18","2016-08-18","50","15","1","100","0","10","0","0","0","0","0","110","","","0","0","16","63","3");
INSERT INTO purchase_misc VALUES("2","2","2016-08-18","2016-08-18","51","15","2","100","0","10","0","0","0","0","0","110","","","0","0","16","63","3");

DROP TABLE IF EXISTS purchase_pac_airline;
CREATE TABLE `purchase_pac_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `adultTotal` float NOT NULL,
  `childTotal` float NOT NULL,
  `infantTotal` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO purchase_pac_airline VALUES("3","1","62","1000","500","100","16","59");
INSERT INTO purchase_pac_airline VALUES("2","2","62","1000","500","100","16","59");
INSERT INTO purchase_pac_airline VALUES("4","3","51","1000","500","100","16","59");

DROP TABLE IF EXISTS purchase_package_airline;
CREATE TABLE `purchase_package_airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date DEFAULT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL DEFAULT '0',
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
INSERT INTO purchase_package_airline VALUES("1","1","1","1","2016-08-20","62","500","16","59","0");
INSERT INTO purchase_package_airline VALUES("2","1","2","2","2016-08-20","62","500","16","59","0");
INSERT INTO purchase_package_airline VALUES("3","2","11","3","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_airline VALUES("4","2","12","4","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_airline VALUES("5","3","13","5","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_airline VALUES("6","3","14","6","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_airline VALUES("7","4","15","7","2016-08-20","62","1000","16","63","3");

DROP TABLE IF EXISTS purchase_package_hotel;
CREATE TABLE `purchase_package_hotel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
INSERT INTO purchase_package_hotel VALUES("1","2","11","1","2016-08-20","62","800","16","63","3");
INSERT INTO purchase_package_hotel VALUES("2","2","12","2","2016-08-20","62","800","16","63","3");
INSERT INTO purchase_package_hotel VALUES("3","3","13","3","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_hotel VALUES("4","3","14","4","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_hotel VALUES("5","4","15","5","2016-08-20","62","1000","16","63","3");

DROP TABLE IF EXISTS purchase_package_transport;
CREATE TABLE `purchase_package_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
INSERT INTO purchase_package_transport VALUES("1","2","11","1","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_transport VALUES("2","2","12","2","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_transport VALUES("3","3","13","3","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_transport VALUES("4","3","14","4","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_transport VALUES("5","4","15","5","2016-08-20","62","1000","16","63","3");

DROP TABLE IF EXISTS purchase_package_visa;
CREATE TABLE `purchase_package_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `in_packageId` int(11) NOT NULL,
  `passengerId` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `purchaseDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `netAmount` float NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
INSERT INTO purchase_package_visa VALUES("1","1","1","1","2016-08-20","62","1000","16","59","0");
INSERT INTO purchase_package_visa VALUES("2","1","2","2","2016-08-20","62","1000","16","59","0");
INSERT INTO purchase_package_visa VALUES("3","2","11","3","2016-08-20","62","1500","16","63","3");
INSERT INTO purchase_package_visa VALUES("4","2","12","4","2016-08-20","62","1500","16","63","3");
INSERT INTO purchase_package_visa VALUES("5","3","13","5","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_visa VALUES("6","3","14","6","2016-08-20","62","1000","16","63","3");
INSERT INTO purchase_package_visa VALUES("7","4","15","7","2016-08-20","62","1000","16","63","3");

DROP TABLE IF EXISTS purchase_transport;
CREATE TABLE `purchase_transport` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO purchase_transport VALUES("1","1","2016-08-18","2016-08-18","51","13","1","5000","0","500","500","500","0","0","0","5500","","","1000","0","16","63","3");
INSERT INTO purchase_transport VALUES("2","2","2016-08-18","2016-08-18","51","13","2","7500","0","300","200","100","0","0","0","8000","","","100","0","16","63","3");

DROP TABLE IF EXISTS purchase_visa;
CREATE TABLE `purchase_visa` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceNo` int(11) NOT NULL,
  `invoiceDate` date NOT NULL,
  `entryDate` date NOT NULL,
  `supplierId` int(11) NOT NULL,
  `expAccountId` int(11) NOT NULL,
  `ourInvoiceNo` int(11) NOT NULL,
  `basicAmount` float NOT NULL,
  `taxRegNo` float NOT NULL,
  `proCharge` float NOT NULL,
  `otherCharges` float NOT NULL,
  `tds` float NOT NULL,
  `refundable` float NOT NULL,
  `taxInPer` float NOT NULL,
  `roundAmount` float NOT NULL,
  `netAmount` float NOT NULL,
  `remark` text NOT NULL,
  `description` text NOT NULL,
  `tac` int(11) NOT NULL,
  `billNo` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO purchase_visa VALUES("1","1","2016-08-18","2016-08-18","57","7","1","14200","0","0","0","0","0","0","0","14200","","","0","0","18","66","3");
INSERT INTO purchase_visa VALUES("2","1","2016-08-18","2016-08-18","50","7","1","1000","0","10","20","30","40","0","0","1000","","","100","0","16","63","3");
INSERT INTO purchase_visa VALUES("3","2","2016-08-18","2016-08-18","51","7","2","900","0","100","200","200","200","0","100","1200","","","500","0","16","63","3");
INSERT INTO purchase_visa VALUES("4","3","2016-08-18","2016-08-18","50","7","3","1500","0","100","100","100","100","0","0","1700","","","200","0","16","63","3");

DROP TABLE IF EXISTS relationship;
CREATE TABLE `relationship` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `relationship` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
INSERT INTO relationship VALUES("1","brother","13","62","3");
INSERT INTO relationship VALUES("2","sister","13","62","3");
INSERT INTO relationship VALUES("3","mother","13","62","3");
INSERT INTO relationship VALUES("4","father","13","62","3");
INSERT INTO relationship VALUES("5","friend","13","62","3");
INSERT INTO relationship VALUES("6","foster sister","13","61","3");
INSERT INTO relationship VALUES("7","foster brother","13","61","3");
INSERT INTO relationship VALUES("8","step father","13","61","3");
INSERT INTO relationship VALUES("9","step mother","13","61","3");
INSERT INTO relationship VALUES("10","husband","16","63","3");
INSERT INTO relationship VALUES("11","wife","16","63","3");
INSERT INTO relationship VALUES("12","father","16","63","3");
INSERT INTO relationship VALUES("13","son","16","63","3");
INSERT INTO relationship VALUES("14","daughter","16","63","3");
INSERT INTO relationship VALUES("15","mother","19","70","3");
INSERT INTO relationship VALUES("16","brother","21","72","3");

DROP TABLE IF EXISTS room_type;
CREATE TABLE `room_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `roomType` varchar(30) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
INSERT INTO room_type VALUES("1","Sharing","0","1","0");
INSERT INTO room_type VALUES("2","Twin","0","1","0");
INSERT INTO room_type VALUES("3","Triple","0","1","0");
INSERT INTO room_type VALUES("4","Four Bed","0","1","0");
INSERT INTO room_type VALUES("5","Five Bed","0","1","0");
INSERT INTO room_type VALUES("6","Six Bed","0","1","0");
INSERT INTO room_type VALUES("7","Child With Bed","0","1","0");
INSERT INTO room_type VALUES("8","Child Without Bed","0","1","0");
INSERT INTO room_type VALUES("9","Suv","21","72","3");

DROP TABLE IF EXISTS staff;
CREATE TABLE `staff` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffName` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `userType` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
INSERT INTO staff VALUES("12","Test","9496568942","testnew@gmail.com","","8","48","0");
INSERT INTO staff VALUES("13","Staff3","8965142536","","","8","48","0");
INSERT INTO staff VALUES("14","Rayeez","80980","","","13","56","0");
INSERT INTO staff VALUES("15","Pharvees","3414","","","13","56","0");
INSERT INTO staff VALUES("16","Thaju","3655","","","16","59","3");
INSERT INTO staff VALUES("17","Minnu","8798654","","","16","59","3");
INSERT INTO staff VALUES("18","Test1","0","","","18","65","3");
INSERT INTO staff VALUES("19","Staff","098","","","20","68","3");
INSERT INTO staff VALUES("20","Alisha","6787678767","","","19","67","3");
INSERT INTO staff VALUES("21","Sundaystaff1","9995646254","","","21","71","3");
INSERT INTO staff VALUES("22","Sundaystaff2","9876","","","21","71","3");

DROP TABLE IF EXISTS transaction;
CREATE TABLE `transaction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `fromLedger` int(11) NOT NULL,
  `toLedger` int(11) NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `voucherType` varchar(100) NOT NULL,
  `credit` double NOT NULL,
  `debit` double NOT NULL,
  `remark` text NOT NULL,
  `addedDate` date NOT NULL,
  `transactionDate` date NOT NULL,
  `loginId` int(11) NOT NULL,
  `proId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  `referenceNo` int(11) NOT NULL,
  `details` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=345 DEFAULT CHARSET=latin1;
INSERT INTO transaction VALUES("1","50","22","1","Other Purchase","1090","0","","2016-08-18","2016-08-18","63","16","0","0","");
INSERT INTO transaction VALUES("2","22","50","1","Other Purchase","0","1090","","2016-08-18","2016-08-18","63","16","0","0","");
INSERT INTO transaction VALUES("3","50","19","1","Other Purchase","10","0","Tds:inv#1","2016-08-18","2016-08-18","63","16","0","0","");
INSERT INTO transaction VALUES("4","19","50","1","Other Purchase","0","10","Tds:inv#1","2016-08-18","2016-08-18","63","16","0","0","");
INSERT INTO transaction VALUES("5","48","4","1","Airline Receivable","0","1230","Airline Receivable: 1","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("6","4","48","1","Airline Receivable","1230","0","Airline Receivable: 1","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("7","16","48","1","Airline Receivable","0","10","Airline Receivable:Dis# 1","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("8","48","16","1","Airline Receivable","10","0","Airline Receivable:Dis#1","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("9","48","19","1","Airline Receivable","0","10","Airline Receivable:Tds#1","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("10","19","48","1","Airline Receivable","10","0","Airline Receivable:Tds#1","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("11","48","18","1","Airline Receivable","0","70","Airline Receivable:Tax#1","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("12","18","48","1","Airline Receivable","70","0","Airline Receivable:Tax#1","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("13","50","5","1","Airline Payable","1230","0","","2016-08-18","2016-08-18","63","16","3","0","PINV#PA1,sup:50,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("14","5","50","1","Airline Payable","0","1230","","2016-08-18","2016-08-18","63","16","3","0","PINV#PA1,sup:50,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("15","50","19","1","Airline Payable","100","0","Tds:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PA1,sup:50,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("16","19","50","1","Airline Payable","0","100","Tds:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PA1,sup:50,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("17","50","18","1","Airline Payable","10","0","Tax:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PA1,sup:50,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("18","18","50","1","Airline Payable","0","10","Tax:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PA1,sup:50,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("19","50","20","1","Airline Payable","0","100","Tac:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PA1,sup:50,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("20","20","50","1","Airline Payable","100","0","Tac:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PA1,sup:50,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("21","53","4","2","Airline Receivable","0","910","Airline Receivable: 2","2016-08-18","2016-08-18","64","16","3","0","Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("22","4","53","2","Airline Receivable","910","0","Airline Receivable: 2","2016-08-18","2016-08-18","64","16","3","0","Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("23","16","53","2","Airline Receivable","0","100","Airline Receivable:Dis# 2","2016-08-18","2016-08-18","64","16","3","0","Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("24","53","16","2","Airline Receivable","100","0","Airline Receivable:Dis#2","2016-08-18","2016-08-18","64","16","3","0","Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("25","53","19","2","Airline Receivable","0","100","Airline Receivable:Tds#2","2016-08-18","2016-08-18","64","16","3","0","Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("26","19","53","2","Airline Receivable","100","0","Airline Receivable:Tds#2","2016-08-18","2016-08-18","64","16","3","0","Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("27","50","5","2","Airline Payable","610","0","","2016-08-18","2016-08-18","64","16","3","0","PINV#PA2,sup:50,details:Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("28","5","50","2","Airline Payable","0","610","","2016-08-18","2016-08-18","64","16","3","0","PINV#PA2,sup:50,details:Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("29","48","4","3","Airline Receivable","0","2410","Airline Receivable: 3","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("30","4","48","3","Airline Receivable","2410","0","Airline Receivable: 3","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("31","16","48","3","Airline Receivable","0","10","Airline Receivable:Dis# 3","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("32","48","16","3","Airline Receivable","10","0","Airline Receivable:Dis#3","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("33","48","19","3","Airline Receivable","0","100","Airline Receivable:Tds#3","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("34","19","48","3","Airline Receivable","100","0","Airline Receivable:Tds#3","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("35","48","18","3","Airline Receivable","0","100","Airline Receivable:Tax#3","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("36","18","48","3","Airline Receivable","100","0","Airline Receivable:Tax#3","2016-08-18","2016-08-18","63","16","3","0","Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("37","53","10","1","Hotel Receivable","0","860","Hotel Receivable : 1","2016-08-18","2016-08-18","64","16","3","0","Hotel Receivable (IH), INV#IH1,Customer Name:ThajuCust2,Hotel Name :Raviz Kadavu, CheckIn Date:19-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Minnu ");
INSERT INTO transaction VALUES("38","10","53","1","Hotel Receivable","860","0","Hotel Receivable : 1","2016-08-18","2016-08-18","64","16","3","0","Hotel Receivable (IH), INV#IH1,Customer Name:ThajuCust2,Hotel Name :Raviz Kadavu, CheckIn Date:19-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Minnu ");
INSERT INTO transaction VALUES("39","50","11","1","Hotel Payable","760","0","","2016-08-18","2016-08-18","64","16","3","0","PINV#PH1,sup:50,details:Hotel Receivable (IH), INV#IH1,Customer Name:ThajuCust2,Hotel Name :Raviz Kadavu, CheckIn Date:19-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Minnu ");
INSERT INTO transaction VALUES("40","11","50","1","Hotel Payable","0","760","","2016-08-18","2016-08-18","64","16","3","0","PINV#PH1,sup:50,details:Hotel Receivable (IH), INV#IH1,Customer Name:ThajuCust2,Hotel Name :Raviz Kadavu, CheckIn Date:19-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Minnu ");
INSERT INTO transaction VALUES("41","50","20","1","Hotel Payable","0","100","Tac:inv#1","2016-08-18","2016-08-18","64","16","3","0","PINV#PH1,sup:50,details:Hotel Receivable (IH), INV#IH1,Customer Name:ThajuCust2,Hotel Name :Raviz Kadavu, CheckIn Date:19-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Minnu ");
INSERT INTO transaction VALUES("42","20","50","1","Hotel Payable","100","0","Tac:inv#1","2016-08-18","2016-08-18","64","16","3","0","PINV#PH1,sup:50,details:Hotel Receivable (IH), INV#IH1,Customer Name:ThajuCust2,Hotel Name :Raviz Kadavu, CheckIn Date:19-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Minnu ");
INSERT INTO transaction VALUES("45","58","6","1","Visa Receivable","0","14800","Visa Receivable : 1","2016-08-18","2016-08-18","66","18","3","0","Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("46","6","58","1","Visa Receivable","14800","0","Visa Receivable : 1","2016-08-18","2016-08-18","66","18","3","0","Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("47","57","7","1","Visa Payable","14200","0","","2016-08-18","2016-08-18","66","18","3","0","PINV#PV1,sup:57,details:Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("48","7","57","1","Visa Payable","0","14200","","2016-08-18","2016-08-18","66","18","3","0","PINV#PV1,sup:57,details:Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("49","55","2","1","Cash Receipts","11100","0","","2016-08-18","2016-08-18","66","18","3","0","VouNo#CR 1,INV#IA1 Received By :Test1,Details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("50","2","55","1","Cash Receipts","0","11100","","2016-08-18","2016-08-18","66","18","3","0","VouNo#CR 1,INV#IA1 Received By :Test1,Details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("51","57","2","1","Cash Payments","0","14200","","2016-08-18","2016-08-18","66","18","3","0","VouNo#CP1,INV#PV1 Paid By :Test1 To : ALTA CCJ,Details:Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("52","2","57","1","Cash Payments","14200","0","","2016-08-18","2016-08-18","66","18","3","0","VouNo#CP1,INV#PV1 Paid By :Test1 To : ALTA CCJ,Details:Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("53","55","4","1","Airline Receivable","0","11100","Airline Receivable: 1","2016-08-18","2016-08-18","66","18","3","0","Airline Receivable(IA), INV#IA1, Pax:Siraj, Passport No:k2333333, Sector:cok/maa, Date:26-08-2016, Tkt:02022222, PNR:0, By:Test1");
INSERT INTO transaction VALUES("54","4","55","1","Airline Receivable","11100","0","Airline Receivable: 1","2016-08-18","2016-08-18","66","18","3","0","Airline Receivable(IA), INV#IA1, Pax:Siraj, Passport No:k2333333, Sector:cok/maa, Date:26-08-2016, Tkt:02022222, PNR:0, By:Test1");
INSERT INTO transaction VALUES("55","58","4","2","Airline Receivable","0","7600","Airline Receivable: 2","2016-08-18","2016-08-18","66","18","3","0","Airline Receivable(IA), INV#IA2, Pax:BASITH, Passport No:K255222, Sector:CCJ/DMM, Date:31-08-2016, Tkt:5665566, PNR:0, By:Test1");
INSERT INTO transaction VALUES("56","4","58","2","Airline Receivable","7600","0","Airline Receivable: 2","2016-08-18","2016-08-18","66","18","3","0","Airline Receivable(IA), INV#IA2, Pax:BASITH, Passport No:K255222, Sector:CCJ/DMM, Date:31-08-2016, Tkt:5665566, PNR:0, By:Test1");
INSERT INTO transaction VALUES("57","59","5","1","Airline Payable","7100","0","","2016-08-18","2016-08-18","66","18","3","0","PINV#PA1,sup:59,details:Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("58","5","59","1","Airline Payable","0","7100","","2016-08-18","2016-08-18","66","18","3","0","PINV#PA1,sup:59,details:Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("59","51","5","3","Airline Payable","2410","0","","2016-08-18","2016-08-18","63","16","3","0","PINV#PA3,sup:51,details:Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("60","5","51","3","Airline Payable","0","2410","","2016-08-18","2016-08-18","63","16","3","0","PINV#PA3,sup:51,details:Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("61","51","19","3","Airline Payable","100","0","Tds:inv#3","2016-08-18","2016-08-18","63","16","3","0","PINV#PA3,sup:51,details:Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("62","19","51","3","Airline Payable","0","100","Tds:inv#3","2016-08-18","2016-08-18","63","16","3","0","PINV#PA3,sup:51,details:Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("63","51","20","3","Airline Payable","0","10","Tac:inv#3","2016-08-18","2016-08-18","63","16","3","0","PINV#PA3,sup:51,details:Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("64","20","51","3","Airline Payable","10","0","Tac:inv#3","2016-08-18","2016-08-18","63","16","3","0","PINV#PA3,sup:51,details:Airline Receivable(IA), INV#IA3, Pax:Sernamcustpangr1, Passport No:dd, Sector:fgf/kkk, Date:19-08-2016, Tkt:rt544, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("91","49","6","1","Visa Receivable","0","1300","Visa Receivable : 1","2016-08-18","2016-08-18","63","16","3","0","Visa Receivable (IV), INV#IV1,Customer Name:sernamCust2,Pax :Sernamcust2psngr2, PassNo :xx/ visaFor:malasiya, By:Sernam-Thaju");
INSERT INTO transaction VALUES("92","6","49","1","Visa Receivable","1300","0","Visa Receivable : 1","2016-08-18","2016-08-18","63","16","3","0","Visa Receivable (IV), INV#IV1,Customer Name:sernamCust2,Pax :Sernamcust2psngr2, PassNo :xx/ visaFor:malasiya, By:Sernam-Thaju");
INSERT INTO transaction VALUES("93","50","7","1","Visa Payable","1070","0","","2016-08-18","2016-08-18","63","16","3","0","PINV#PV1,sup:50,details:Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("94","7","50","1","Visa Payable","0","1070","","2016-08-18","2016-08-18","63","16","3","0","PINV#PV1,sup:50,details:Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("95","50","19","1","Visa Payable","30","0","Tds:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PV1,sup:50,details:Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("96","19","50","1","Visa Payable","0","30","Tds:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PV1,sup:50,details:Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("97","50","20","1","Visa Payable","0","100","Tac:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PV1,sup:50,details:Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("98","20","50","1","Visa Payable","100","0","Tac:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PV1,sup:50,details:Visa Receivable (IV), INV#IV1,Customer Name:FAISAL STAFF,Pax :AKHILA, PassNo :K0060995/ visaFor:90 DAYS DXB  WITHOUT OTB, By:Yarmook-Test1");
INSERT INTO transaction VALUES("99","48","6","2","Visa Receivable","0","1300","Visa Receivable : 2","2016-08-18","2016-08-18","63","16","3","0","Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("100","6","48","2","Visa Receivable","1300","0","Visa Receivable : 2","2016-08-18","2016-08-18","63","16","3","0","Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("101","51","7","2","Visa Payable","1400","0","","2016-08-18","2016-08-18","63","16","3","0","PINV#PV2,sup:51,details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("102","7","51","2","Visa Payable","0","1400","","2016-08-18","2016-08-18","63","16","3","0","PINV#PV2,sup:51,details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("103","51","16","2","Visa Payable","100","0","Discount Given:inv#2","2016-08-18","2016-08-18","63","16","3","0","PINV#PV2,sup:51,details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("104","16","51","2","Visa Payable","0","100","Discount Given:inv#2","2016-08-18","2016-08-18","63","16","3","0","PINV#PV2,sup:51,details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("105","51","19","2","Visa Payable","200","0","Tds:inv#2","2016-08-18","2016-08-18","63","16","3","0","PINV#PV2,sup:51,details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("106","19","51","2","Visa Payable","0","200","Tds:inv#2","2016-08-18","2016-08-18","63","16","3","0","PINV#PV2,sup:51,details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("107","51","20","2","Visa Payable","0","500","Tac:inv#2","2016-08-18","2016-08-18","63","16","3","0","PINV#PV2,sup:51,details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("108","20","51","2","Visa Payable","500","0","Tac:inv#2","2016-08-18","2016-08-18","63","16","3","0","PINV#PV2,sup:51,details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("109","53","6","3","Visa Receivable","0","2000","Visa Receivable : 3","2016-08-18","2016-08-18","63","16","3","0","Visa Receivable (IV), INV#IV3,Customer Name:ThajuCust2,Pax :Thajucust2pasngr1, PassNo :gfh554/ visaFor:dubai, By:Sernam-Thaju");
INSERT INTO transaction VALUES("110","6","53","3","Visa Receivable","2000","0","Visa Receivable : 3","2016-08-18","2016-08-18","63","16","3","0","Visa Receivable (IV), INV#IV3,Customer Name:ThajuCust2,Pax :Thajucust2pasngr1, PassNo :gfh554/ visaFor:dubai, By:Sernam-Thaju");
INSERT INTO transaction VALUES("111","50","7","3","Visa Payable","1800","0","","2016-08-18","2016-08-18","63","16","3","0","PINV#PV3,sup:50,details:Visa Receivable (IV), INV#IV3,Customer Name:ThajuCust2,Pax :Thajucust2pasngr1, PassNo :gfh554/ visaFor:dubai, By:Sernam-Thaju");
INSERT INTO transaction VALUES("112","7","50","3","Visa Payable","0","1800","","2016-08-18","2016-08-18","63","16","3","0","PINV#PV3,sup:50,details:Visa Receivable (IV), INV#IV3,Customer Name:ThajuCust2,Pax :Thajucust2pasngr1, PassNo :gfh554/ visaFor:dubai, By:Sernam-Thaju");
INSERT INTO transaction VALUES("113","50","19","3","Visa Payable","100","0","Tds:inv#3","2016-08-18","2016-08-18","63","16","3","0","PINV#PV3,sup:50,details:Visa Receivable (IV), INV#IV3,Customer Name:ThajuCust2,Pax :Thajucust2pasngr1, PassNo :gfh554/ visaFor:dubai, By:Sernam-Thaju");
INSERT INTO transaction VALUES("114","19","50","3","Visa Payable","0","100","Tds:inv#3","2016-08-18","2016-08-18","63","16","3","0","PINV#PV3,sup:50,details:Visa Receivable (IV), INV#IV3,Customer Name:ThajuCust2,Pax :Thajucust2pasngr1, PassNo :gfh554/ visaFor:dubai, By:Sernam-Thaju");
INSERT INTO transaction VALUES("115","50","20","3","Visa Payable","0","200","Tac:inv#3","2016-08-18","2016-08-18","63","16","3","0","PINV#PV3,sup:50,details:Visa Receivable (IV), INV#IV3,Customer Name:ThajuCust2,Pax :Thajucust2pasngr1, PassNo :gfh554/ visaFor:dubai, By:Sernam-Thaju");
INSERT INTO transaction VALUES("116","20","50","3","Visa Payable","200","0","Tac:inv#3","2016-08-18","2016-08-18","63","16","3","0","PINV#PV3,sup:50,details:Visa Receivable (IV), INV#IV3,Customer Name:ThajuCust2,Pax :Thajucust2pasngr1, PassNo :gfh554/ visaFor:dubai, By:Sernam-Thaju");
INSERT INTO transaction VALUES("117","48","14","1","Misc Receivable","0","150","Misc Receivable : 1","2016-08-18","2016-08-18","63","16","3","0","Misc Receivable (IM), INV#IM1,Pax:rameees,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("118","14","48","1","Misc Receivable","150","0","Misc Receivable : 1","2016-08-18","2016-08-18","63","16","3","0","Misc Receivable (IM), INV#IM1,Pax:rameees,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("119","48","19","1","Misc Receivable","0","10","Misc Receivable: 1","2016-08-18","2016-08-18","63","16","3","0","Misc Receivable (IM), INV#IM1,Pax:rameees,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("120","19","48","1","Misc Receivable","10","0","Misc Receivable: 1","2016-08-18","2016-08-18","63","16","3","0","Misc Receivable (IM), INV#IM1,Pax:rameees,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("121","48","18","1","Misc Receivable","0","40","Misc Receivable: 1","2016-08-18","2016-08-18","63","16","3","0","Misc Receivable (IM), INV#IM1,Pax:rameees,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("122","18","48","1","Misc Receivable","40","0","Misc Receivable: 1","2016-08-18","2016-08-18","63","16","3","0","Misc Receivable (IM), INV#IM1,Pax:rameees,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("123","50","15","1","Misc Payable","110","0","","2016-08-18","2016-08-18","63","16","3","0","PINV#PM1,sup:50,details:Misc Receivable (IM), INV#IM1,Pax:rameees,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("124","15","50","1","Misc Payable","0","110","","2016-08-18","2016-08-18","63","16","3","0","PINV#PM1,sup:50,details:Misc Receivable (IM), INV#IM1,Pax:rameees,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("125","52","14","2","Misc Receivable","0","140","Misc Receivable : 2","2016-08-18","2016-08-18","63","16","3","0","Misc Receivable (IM), INV#IM2,Pax:rtt,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("126","14","52","2","Misc Receivable","140","0","Misc Receivable : 2","2016-08-18","2016-08-18","63","16","3","0","Misc Receivable (IM), INV#IM2,Pax:rtt,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("127","52","18","2","Misc Receivable","0","60","Misc Receivable: 2","2016-08-18","2016-08-18","63","16","3","0","Misc Receivable (IM), INV#IM2,Pax:rtt,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("128","18","52","2","Misc Receivable","60","0","Misc Receivable: 2","2016-08-18","2016-08-18","63","16","3","0","Misc Receivable (IM), INV#IM2,Pax:rtt,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("129","51","15","2","Misc Payable","110","0","","2016-08-18","2016-08-18","63","16","3","0","PINV#PM2,sup:51,details:Misc Receivable (IM), INV#IM2,Pax:rtt,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("130","15","51","2","Misc Payable","0","110","","2016-08-18","2016-08-18","63","16","3","0","PINV#PM2,sup:51,details:Misc Receivable (IM), INV#IM2,Pax:rtt,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("131","49","12","1","Transport Receivable","0","6000","Transport Receivable : 1","2016-08-18","2016-08-18","63","16","3","0","Transport Receivable (IT), INV#IT1,Provider Name:Galaxy,No.of Adult&Children:4/0,Total Days:8,Vehicle:Suv,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("132","12","49","1","Transport Receivable","6000","0","Transport Receivable : 1","2016-08-18","2016-08-18","63","16","3","0","Transport Receivable (IT), INV#IT1,Provider Name:Galaxy,No.of Adult&Children:4/0,Total Days:8,Vehicle:Suv,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("133","51","13","1","Transport Payable","6000","0","","2016-08-18","2016-08-18","63","16","3","0","PINV#PT1,sup:51,details:Transport Receivable (IT), INV#IT1,Provider Name:Galaxy,No.of Adult&Children:4/0,Total Days:8,Vehicle:Suv,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("134","13","51","1","Transport Payable","0","6000","","2016-08-18","2016-08-18","63","16","3","0","PINV#PT1,sup:51,details:Transport Receivable (IT), INV#IT1,Provider Name:Galaxy,No.of Adult&Children:4/0,Total Days:8,Vehicle:Suv,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("135","51","19","1","Transport Payable","500","0","Tds:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PT1,sup:51,details:Transport Receivable (IT), INV#IT1,Provider Name:Galaxy,No.of Adult&Children:4/0,Total Days:8,Vehicle:Suv,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("136","19","51","1","Transport Payable","0","500","Tds:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PT1,sup:51,details:Transport Receivable (IT), INV#IT1,Provider Name:Galaxy,No.of Adult&Children:4/0,Total Days:8,Vehicle:Suv,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("137","51","20","1","Transport Payable","0","1000","Tac:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PT1,sup:51,details:Transport Receivable (IT), INV#IT1,Provider Name:Galaxy,No.of Adult&Children:4/0,Total Days:8,Vehicle:Suv,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("138","20","51","1","Transport Payable","1000","0","Tac:inv#1","2016-08-18","2016-08-18","63","16","3","0","PINV#PT1,sup:51,details:Transport Receivable (IT), INV#IT1,Provider Name:Galaxy,No.of Adult&Children:4/0,Total Days:8,Vehicle:Suv,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("139","52","12","2","Transport Receivable","0","9000","Transport Receivable : 2","2016-08-18","2016-08-18","63","16","3","0","Transport Receivable (IT), INV#IT2,Provider Name:Scania,No.of Adult&Children:30/15,Total Days:4,Vehicle:Volvo Bus,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("140","12","52","2","Transport Receivable","9000","0","Transport Receivable : 2","2016-08-18","2016-08-18","63","16","3","0","Transport Receivable (IT), INV#IT2,Provider Name:Scania,No.of Adult&Children:30/15,Total Days:4,Vehicle:Volvo Bus,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("141","51","13","2","Transport Payable","8000","0","","2016-08-18","2016-08-18","63","16","3","0","PINV#PT2,sup:51,details:Transport Receivable (IT), INV#IT2,Provider Name:Scania,No.of Adult&Children:30/15,Total Days:4,Vehicle:Volvo Bus,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("142","13","51","2","Transport Payable","0","8000","","2016-08-18","2016-08-18","63","16","3","0","PINV#PT2,sup:51,details:Transport Receivable (IT), INV#IT2,Provider Name:Scania,No.of Adult&Children:30/15,Total Days:4,Vehicle:Volvo Bus,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("143","51","19","2","Transport Payable","100","0","Tds:inv#2","2016-08-18","2016-08-18","63","16","3","0","PINV#PT2,sup:51,details:Transport Receivable (IT), INV#IT2,Provider Name:Scania,No.of Adult&Children:30/15,Total Days:4,Vehicle:Volvo Bus,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("144","19","51","2","Transport Payable","0","100","Tds:inv#2","2016-08-18","2016-08-18","63","16","3","0","PINV#PT2,sup:51,details:Transport Receivable (IT), INV#IT2,Provider Name:Scania,No.of Adult&Children:30/15,Total Days:4,Vehicle:Volvo Bus,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("145","51","20","2","Transport Payable","0","100","Tac:inv#2","2016-08-18","2016-08-18","63","16","3","0","PINV#PT2,sup:51,details:Transport Receivable (IT), INV#IT2,Provider Name:Scania,No.of Adult&Children:30/15,Total Days:4,Vehicle:Volvo Bus,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("146","20","51","2","Transport Payable","100","0","Tac:inv#2","2016-08-18","2016-08-18","63","16","3","0","PINV#PT2,sup:51,details:Transport Receivable (IT), INV#IT2,Provider Name:Scania,No.of Adult&Children:30/15,Total Days:4,Vehicle:Volvo Bus,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("147","50","2","1","Cash Payments","0","3710","","2016-08-18","2016-08-18","63","16","3","0","VouNo#CP1,INV#PA1,PH1,PM1,PV3 Paid By :Thaju To : nachka,Details:Visa Receivable (IV), INV#IV3,Customer Name:ThajuCust2,Pax :Thajucust2pasngr1, PassNo :gfh554/ visaFor:dubai, By:Sernam-Thaju");
INSERT INTO transaction VALUES("148","2","50","1","Cash Payments","3710","0","","2016-08-18","2016-08-18","63","16","3","0","VouNo#CP1,INV#PA1,PH1,PM1,PV3 Paid By :Thaju To : nachka,Details:Visa Receivable (IV), INV#IV3,Customer Name:ThajuCust2,Pax :Thajucust2pasngr1, PassNo :gfh554/ visaFor:dubai, By:Sernam-Thaju");
INSERT INTO transaction VALUES("149","51","47","1","Bank Payments","0","9310","","2016-08-18","2016-08-18","63","16","3","0","VouNo#BP1,INV#PA3,PM2,PT1,PV2 Paid By :Thaju To : majeedka,Details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("150","47","51","1","Bank Payments","9310","0","","2016-08-18","2016-08-18","63","16","3","0","VouNo#BP1,INV#PA3,PM2,PT1,PV2 Paid By :Thaju To : majeedka,Details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("151","48","2","1","Cash Receipts","4100","0","","2016-08-18","2016-08-18","63","16","3","0","VouNo#CR 1,INV#IA3,IM1,IV2 Received By :Thaju,Details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("152","2","48","1","Cash Receipts","0","4100","","2016-08-18","2016-08-18","63","16","3","0","VouNo#CR 1,INV#IA3,IM1,IV2 Received By :Thaju,Details:Visa Receivable (IV), INV#IV2,Customer Name:sernamCust1,Pax :Sernamcustpangr1, PassNo :dd/ visaFor:bahrain, By:Sernam-Thaju");
INSERT INTO transaction VALUES("153","49","2","2","Cash Receipts","6000","0","","2016-08-18","2016-08-18","63","16","3","0","VouNo#CR 2,INV#IT1 Received By :Thaju,Details:Transport Receivable (IT), INV#IT1,Provider Name:Galaxy,No.of Adult&Children:4/0,Total Days:8,Vehicle:Suv,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("154","2","49","2","Cash Receipts","0","6000","","2016-08-18","2016-08-18","63","16","3","0","VouNo#CR 2,INV#IT1 Received By :Thaju,Details:Transport Receivable (IT), INV#IT1,Provider Name:Galaxy,No.of Adult&Children:4/0,Total Days:8,Vehicle:Suv,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("155","52","2","3","Cash Receipts","1000","0","","2016-08-18","2016-08-18","63","16","3","0","VouNo#CR 3,INV#IH2,IM2 Received By :Thaju,Details:Misc Receivable (IM), INV#IM2,Pax:rtt,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("156","2","52","3","Cash Receipts","0","1000","","2016-08-18","2016-08-18","63","16","3","0","VouNo#CR 3,INV#IH2,IM2 Received By :Thaju,Details:Misc Receivable (IM), INV#IM2,Pax:rtt,Narration:Travel,By:Thaju ");
INSERT INTO transaction VALUES("157","53","2","4","Cash Receipts","910","0","","2016-08-18","2016-08-18","63","16","3","0","VouNo#CR 4,INV#IA2 Received By :Thaju,Details:Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("158","2","53","4","Cash Receipts","0","910","","2016-08-18","2016-08-18","63","16","3","0","VouNo#CR 4,INV#IA2 Received By :Thaju,Details:Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("159","52","47","1","Bank Receipts","900","0","","2016-08-18","2016-08-18","63","16","3","0","VouNo#BR1,INV#IH3 Received By :Thaju,Details:Hotel Receivable (IH), INV#IH3,Customer Name:thajuCust1,Hotel Name :Raviz Kadavu, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("160","47","52","1","Bank Receipts","0","900","","2016-08-18","2016-08-18","63","16","3","0","VouNo#BR1,INV#IH3 Received By :Thaju,Details:Hotel Receivable (IH), INV#IH3,Customer Name:thajuCust1,Hotel Name :Raviz Kadavu, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("161","52","47","2","Bank Receipts","5000","0","","2016-08-18","2016-08-18","63","16","3","0","VouNo#BR2,INV#IT2 Received By :Thaju,Details:Transport Receivable (IT), INV#IT2,Provider Name:Scania,No.of Adult&Children:30/15,Total Days:4,Vehicle:Volvo Bus,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("162","47","52","2","Bank Receipts","0","5000","","2016-08-18","2016-08-18","63","16","3","0","VouNo#BR2,INV#IT2 Received By :Thaju,Details:Transport Receivable (IT), INV#IT2,Provider Name:Scania,No.of Adult&Children:30/15,Total Days:4,Vehicle:Volvo Bus,fromDate:18-08-2016, By:Thaju ");
INSERT INTO transaction VALUES("163","61","4","4","Airline Receivable","0","900","Airline Receivable: 4","2016-08-18","2016-08-18","64","16","3","0","Airline Receivable(IA), INV#IA4, Pax:Beeran, Passport No:1234BR, Sector:DL/KL, Date:11-08-2016, Tkt:123, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("164","4","61","4","Airline Receivable","900","0","Airline Receivable: 4","2016-08-18","2016-08-18","64","16","3","0","Airline Receivable(IA), INV#IA4, Pax:Beeran, Passport No:1234BR, Sector:DL/KL, Date:11-08-2016, Tkt:123, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("165","61","19","4","Airline Receivable","0","100","Airline Receivable:Tds#4","2016-08-18","2016-08-18","64","16","3","0","Airline Receivable(IA), INV#IA4, Pax:Beeran, Passport No:1234BR, Sector:DL/KL, Date:11-08-2016, Tkt:123, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("166","19","61","4","Airline Receivable","100","0","Airline Receivable:Tds#4","2016-08-18","2016-08-18","64","16","3","0","Airline Receivable(IA), INV#IA4, Pax:Beeran, Passport No:1234BR, Sector:DL/KL, Date:11-08-2016, Tkt:123, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("167","62","5","4","Airline Payable","600","0","","2016-08-18","2016-08-18","64","16","3","0","PINV#PA4,sup:62,details:Airline Receivable(IA), INV#IA4, Pax:Beeran, Passport No:1234BR, Sector:DL/KL, Date:11-08-2016, Tkt:123, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("168","5","62","4","Airline Payable","0","600","","2016-08-18","2016-08-18","64","16","3","0","PINV#PA4,sup:62,details:Airline Receivable(IA), INV#IA4, Pax:Beeran, Passport No:1234BR, Sector:DL/KL, Date:11-08-2016, Tkt:123, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("169","48","8","1","Package Receivable","0","11600","Invoice Package: 1","2016-08-20","2016-08-20","63","16","3","1","INV#IP1,Pac:Singapore ,Pax:Sernamcust1pasngr1 M 10 PassNo-wer433,Sernamcust1pasngr1 M 10 PassNo-wer433 ,By:Thaju");
INSERT INTO transaction VALUES("170","8","48","1","Package Receivable","11600","0","Invoice Package: 1","2016-08-20","2016-08-20","63","16","3","1","INV#IP1,Pac:Singapore ,Pax:Sernamcust1pasngr1 M 10 PassNo-wer433,Sernamcust1pasngr1 M 10 PassNo-wer433 ,By:Thaju");
INSERT INTO transaction VALUES("171","16","48","1","Package Receivable","0","100","Invoice Package: 1","2016-08-20","2016-08-20","63","16","3","1","INV#IP1,Pac:Singapore ,By:Thaju,To:sernamCust1");
INSERT INTO transaction VALUES("172","48","16","1","Package Receivable","100","0","Invoice Package: 1","2016-08-20","2016-08-20","63","16","3","1","INV#IP1,Pac:Singapore ,By:Thaju,To:sernamCust1");
INSERT INTO transaction VALUES("173","48","19","1","Package Receivable","0","100","Package Receivable: 1","2016-08-20","2016-08-20","63","16","3","1","INV#IP1,Pac:Singapore ,By:Thaju,To:Tds");
INSERT INTO transaction VALUES("174","19","48","1","Package Receivable","100","0","Package Receivable: 1","2016-08-20","2016-08-20","63","16","3","1","INV#IP1,Pac:Singapore ,By:Thaju,To:Tds");
INSERT INTO transaction VALUES("175","48","18","1","Package Receivable","0","100","Package Receivable: 1","2016-08-20","2016-08-20","63","16","3","1","INV#IP1,Pac:Singapore ,By:Thaju,To:Tax");
INSERT INTO transaction VALUES("176","18","48","1","Package Receivable","100","0","Package Receivable: 1","2016-08-20","2016-08-20","63","16","3","1","INV#IP1,Pac:Singapore ,By:Thaju,To:Tax");
INSERT INTO transaction VALUES("177","48","2","5","Cash Receipts","10000","0","","2016-08-20","2016-08-20","63","16","3","0","VouNo#CR 5,INV#IP1 Received By :Thaju,Details:");
INSERT INTO transaction VALUES("178","2","48","5","Cash Receipts","0","10000","","2016-08-20","2016-08-20","63","16","3","0","VouNo#CR 5,INV#IP1 Received By :Thaju,Details:");
INSERT INTO transaction VALUES("179","5","62","1","Package Airline","0","500","Purchase Package: 1","2016-08-20","2016-08-20","59","16","0","1","INV#IP1 ,Pac:Singapore ,Pax:Sernamcust1pasngr1,By:Sernam");
INSERT INTO transaction VALUES("180","62","5","1","Package Airline","500","0","Purchase Package: 1","2016-08-20","2016-08-20","59","16","0","1","INV#IP1 ,Pac:Singapore ,Pax:Sernamcust1pasngr1,By:Sernam");
INSERT INTO transaction VALUES("181","7","62","1","Package Visa","0","1000","Purchase Package: 1","2016-08-20","2016-08-20","59","16","0","1","INV#IP1 ,Pac:Singapore ,Pax:Sernamcust1pasngr1 ,By:Sernam");
INSERT INTO transaction VALUES("182","62","7","1","Package Visa","1000","0","Purchase Package: 1","2016-08-20","2016-08-20","59","16","0","1","INV#IP1 ,Pac:Singapore ,Pax:Sernamcust1pasngr1 ,By:Sernam");
INSERT INTO transaction VALUES("183","5","62","2","Package Airline","0","500","Purchase Package: 1","2016-08-20","2016-08-20","59","16","0","1","INV#IP1 ,Pac:Singapore ,Pax:Sernamcust1pasngr1,By:Sernam");
INSERT INTO transaction VALUES("184","62","5","2","Package Airline","500","0","Purchase Package: 1","2016-08-20","2016-08-20","59","16","0","1","INV#IP1 ,Pac:Singapore ,Pax:Sernamcust1pasngr1,By:Sernam");
INSERT INTO transaction VALUES("185","7","62","2","Package Visa","0","1000","Purchase Package: 1","2016-08-20","2016-08-20","59","16","0","1","INV#IP1 ,Pac:Singapore ,Pax:Sernamcust1pasngr1 ,By:Sernam");
INSERT INTO transaction VALUES("186","62","7","2","Package Visa","1000","0","Purchase Package: 1","2016-08-20","2016-08-20","59","16","0","1","INV#IP1 ,Pac:Singapore ,Pax:Sernamcust1pasngr1 ,By:Sernam");
INSERT INTO transaction VALUES("187","5","62","3","Package Airline","0","1000","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:haseena,M,28 ,By:Thaju");
INSERT INTO transaction VALUES("188","62","5","3","Package Airline","1000","0","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:haseena,M,28 ,By:Thaju");
INSERT INTO transaction VALUES("189","11","62","1","Package Hotel","0","800","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:haseena,M,28,By:Thaju");
INSERT INTO transaction VALUES("190","62","11","1","Package Hotel","800","0","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:haseena,M,28,By:Thaju");
INSERT INTO transaction VALUES("191","7","62","3","Package Visa","0","1500","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:haseena ,M,28 ,By:Thaju");
INSERT INTO transaction VALUES("192","62","7","3","Package Visa","1500","0","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:haseena ,M,28 ,By:Thaju");
INSERT INTO transaction VALUES("193","13","62","1","Package Transportation","0","1000","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:haseena,M,28,By:Thaju");
INSERT INTO transaction VALUES("194","62","13","1","Package Transportation","1000","0","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:haseena,M,28,By:Thaju");
INSERT INTO transaction VALUES("195","5","62","4","Package Airline","0","1000","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:aisho,F,16 ,By:Thaju");
INSERT INTO transaction VALUES("196","62","5","4","Package Airline","1000","0","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:aisho,F,16 ,By:Thaju");
INSERT INTO transaction VALUES("197","11","62","2","Package Hotel","0","800","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:aisho,F,16,By:Thaju");
INSERT INTO transaction VALUES("198","62","11","2","Package Hotel","800","0","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:aisho,F,16,By:Thaju");
INSERT INTO transaction VALUES("199","7","62","4","Package Visa","0","1500","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:aisho ,F,16 ,By:Thaju");
INSERT INTO transaction VALUES("200","62","7","4","Package Visa","1500","0","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:aisho ,F,16 ,By:Thaju");
INSERT INTO transaction VALUES("201","13","62","2","Package Transportation","0","1000","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:aisho,F,16,By:Thaju");
INSERT INTO transaction VALUES("202","62","13","2","Package Transportation","1000","0","Purchase Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2 ,Pac:Malasiya ,Pax:aisho,F,16,By:Thaju");
INSERT INTO transaction VALUES("203","52","8","2","Package Receivable","0","14100","Invoice Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2,Pac:Malasiya ,Pax:haseena M 28 PassNo-rf332,aisho F 16 PassNo-df344 ,By:Thaju");
INSERT INTO transaction VALUES("204","8","52","2","Package Receivable","14100","0","Invoice Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2,Pac:Malasiya ,Pax:haseena M 28 PassNo-rf332,aisho F 16 PassNo-df344 ,By:Thaju");
INSERT INTO transaction VALUES("205","16","52","2","Package Receivable","0","100","Invoice Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2,Pac:Malasiya ,By:Thaju,To:thajuCust1");
INSERT INTO transaction VALUES("206","52","16","2","Package Receivable","100","0","Invoice Package: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2,Pac:Malasiya ,By:Thaju,To:thajuCust1");
INSERT INTO transaction VALUES("207","52","19","2","Package Receivable","0","100","Package Receivable: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2,Pac:Malasiya ,By:Thaju,To:Tds");
INSERT INTO transaction VALUES("208","19","52","2","Package Receivable","100","0","Package Receivable: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2,Pac:Malasiya ,By:Thaju,To:Tds");
INSERT INTO transaction VALUES("209","52","18","2","Package Receivable","0","200","Package Receivable: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2,Pac:Malasiya ,By:Thaju,To:Tax");
INSERT INTO transaction VALUES("210","18","52","2","Package Receivable","200","0","Package Receivable: 2","2016-08-20","2016-08-20","63","16","3","2","INV#IP2,Pac:Malasiya ,By:Thaju,To:Tax");
INSERT INTO transaction VALUES("219","52","10","3","Hotel Receivable","0","700","Hotel Receivable : 3","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH3,Customer Name:thajuCust1,Hotel Name :Raviz Kadavu, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("220","10","52","3","Hotel Receivable","700","0","Hotel Receivable : 3","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH3,Customer Name:thajuCust1,Hotel Name :Raviz Kadavu, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("221","0","52","3","Hotel Receivable","0","100","Hotel Receivable : 3","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH3,Customer Name:thajuCust1,Hotel Name :Raviz Kadavu, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("222","52","0","3","Hotel Receivable","100","0","Hotel Receivable : 3","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH3,Customer Name:thajuCust1,Hotel Name :Raviz Kadavu, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("223","52","19","3","Hotel Receivable","0","200","Hotel Receivable:Tds#3","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH3,Customer Name:thajuCust1,Hotel Name :Raviz Kadavu, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("224","19","52","3","Hotel Receivable","200","0","Hotel Receivable:Tds#3","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH3,Customer Name:thajuCust1,Hotel Name :Raviz Kadavu, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("225","52","18","3","Hotel Receivable","0","100","Hotel Receivable:Tax#3","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH3,Customer Name:thajuCust1,Hotel Name :Raviz Kadavu, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("226","18","52","3","Hotel Receivable","100","0","Hotel Receivable:Tax#3","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH3,Customer Name:thajuCust1,Hotel Name :Raviz Kadavu, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("227","50","11","3","Hotel Payable","760","0","","2016-08-20","2016-08-18","63","16","3","0","INV#3,sup:50,:against:3");
INSERT INTO transaction VALUES("228","11","50","3","Hotel Payable","0","760","","2016-08-20","2016-08-18","63","16","3","0","INV#3,sup:50,:against:3");
INSERT INTO transaction VALUES("229","50","16","3","Hotel Payable","30","0","Discount Given:inv#3","2016-08-20","2016-08-18","63","16","3","0","INV#3,sup:50,:against:3");
INSERT INTO transaction VALUES("230","16","50","3","Hotel Payable","0","30","Discount Given:inv#3","2016-08-20","2016-08-18","63","16","3","0","INV#3,sup:50,:against:3");
INSERT INTO transaction VALUES("231","50","19","3","Hotel Payable","10","0","Tds:inv#3","2016-08-20","2016-08-18","63","16","3","0","INV#3,sup:50,:against:3");
INSERT INTO transaction VALUES("232","19","50","3","Hotel Payable","0","10","Tds:inv#3","2016-08-20","2016-08-18","63","16","3","0","INV#3,sup:50,:against:3");
INSERT INTO transaction VALUES("233","50","20","3","Hotel Payable","0","100","Tac:inv#3","2016-08-20","2016-08-18","63","16","3","0","INV#3,sup:50,:against:3");
INSERT INTO transaction VALUES("234","20","50","3","Hotel Payable","100","0","Tac:inv#3","2016-08-20","2016-08-18","63","16","3","0","INV#3,sup:50,:against:3");
INSERT INTO transaction VALUES("235","52","10","2","Hotel Receivable","0","700","Hotel Receivable : 2","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH2,Customer Name:thajuCust1,Hotel Name :Viavanta Taj, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("236","10","52","2","Hotel Receivable","700","0","Hotel Receivable : 2","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH2,Customer Name:thajuCust1,Hotel Name :Viavanta Taj, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("237","0","52","2","Hotel Receivable","0","100","Hotel Receivable : 2","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH2,Customer Name:thajuCust1,Hotel Name :Viavanta Taj, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("238","52","0","2","Hotel Receivable","100","0","Hotel Receivable : 2","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH2,Customer Name:thajuCust1,Hotel Name :Viavanta Taj, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("239","52","19","2","Hotel Receivable","0","100","Hotel Receivable:Tds#2","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH2,Customer Name:thajuCust1,Hotel Name :Viavanta Taj, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("240","19","52","2","Hotel Receivable","100","0","Hotel Receivable:Tds#2","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH2,Customer Name:thajuCust1,Hotel Name :Viavanta Taj, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("241","52","18","2","Hotel Receivable","0","100","Hotel Receivable:Tax#2","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH2,Customer Name:thajuCust1,Hotel Name :Viavanta Taj, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("242","18","52","2","Hotel Receivable","100","0","Hotel Receivable:Tax#2","2016-08-20","2016-08-18","63","16","3","0","Hotel Receivable (IH), INV#IH2,Customer Name:thajuCust1,Hotel Name :Viavanta Taj, CheckIn Date:18-08-2016, No.of Rooms: 1,No.of Nights: 1,By:Thaju ");
INSERT INTO transaction VALUES("243","50","11","2","Hotel Payable","900","0","","2016-08-20","2016-08-18","63","16","3","0","INV#2,sup:50,:against:2");
INSERT INTO transaction VALUES("244","11","50","2","Hotel Payable","0","900","","2016-08-20","2016-08-18","63","16","3","0","INV#2,sup:50,:against:2");
INSERT INTO transaction VALUES("245","50","19","2","Hotel Payable","100","0","Tds:inv#2","2016-08-20","2016-08-18","63","16","3","0","INV#2,sup:50,:against:2");
INSERT INTO transaction VALUES("246","19","50","2","Hotel Payable","0","100","Tds:inv#2","2016-08-20","2016-08-18","63","16","3","0","INV#2,sup:50,:against:2");
INSERT INTO transaction VALUES("247","50","20","2","Hotel Payable","0","300","Tac:inv#2","2016-08-20","2016-08-18","63","16","3","0","INV#2,sup:50,:against:2");
INSERT INTO transaction VALUES("248","20","50","2","Hotel Payable","300","0","Tac:inv#2","2016-08-20","2016-08-18","63","16","3","0","INV#2,sup:50,:against:2");
INSERT INTO transaction VALUES("249","5","62","5","Package Airline","0","1000","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:jasim,M,20 ,By:Thaju");
INSERT INTO transaction VALUES("250","62","5","5","Package Airline","1000","0","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:jasim,M,20 ,By:Thaju");
INSERT INTO transaction VALUES("251","11","62","3","Package Hotel","0","1000","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:jasim,M,20,By:Thaju");
INSERT INTO transaction VALUES("252","62","11","3","Package Hotel","1000","0","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:jasim,M,20,By:Thaju");
INSERT INTO transaction VALUES("253","7","62","5","Package Visa","0","1000","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:jasim ,M,20 ,By:Thaju");
INSERT INTO transaction VALUES("254","62","7","5","Package Visa","1000","0","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:jasim ,M,20 ,By:Thaju");
INSERT INTO transaction VALUES("255","13","62","3","Package Transportation","0","1000","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:jasim,M,20,By:Thaju");
INSERT INTO transaction VALUES("256","62","13","3","Package Transportation","1000","0","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:jasim,M,20,By:Thaju");
INSERT INTO transaction VALUES("257","5","62","6","Package Airline","0","1000","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:husna,F,19 ,By:Thaju");
INSERT INTO transaction VALUES("258","62","5","6","Package Airline","1000","0","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:husna,F,19 ,By:Thaju");
INSERT INTO transaction VALUES("259","11","62","4","Package Hotel","0","1000","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:husna,F,19,By:Thaju");
INSERT INTO transaction VALUES("260","62","11","4","Package Hotel","1000","0","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:husna,F,19,By:Thaju");
INSERT INTO transaction VALUES("261","7","62","6","Package Visa","0","1000","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:husna ,F,19 ,By:Thaju");
INSERT INTO transaction VALUES("262","62","7","6","Package Visa","1000","0","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:husna ,F,19 ,By:Thaju");
INSERT INTO transaction VALUES("263","13","62","4","Package Transportation","0","1000","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:husna,F,19,By:Thaju");
INSERT INTO transaction VALUES("264","62","13","4","Package Transportation","1000","0","Purchase Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3 ,Pac:Singapore ,Pax:husna,F,19,By:Thaju");
INSERT INTO transaction VALUES("265","61","8","3","Package Receivable","0","12600","Invoice Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3,Pac:Singapore ,Pax:jasim M 20 PassNo-fd33,husna F 19 PassNo-df333 ,By:Thaju");
INSERT INTO transaction VALUES("266","8","61","3","Package Receivable","12600","0","Invoice Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3,Pac:Singapore ,Pax:jasim M 20 PassNo-fd33,husna F 19 PassNo-df333 ,By:Thaju");
INSERT INTO transaction VALUES("267","16","61","3","Package Receivable","0","100","Invoice Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3,Pac:Singapore ,By:Thaju,To:Customer Unni");
INSERT INTO transaction VALUES("268","61","16","3","Package Receivable","100","0","Invoice Package: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3,Pac:Singapore ,By:Thaju,To:Customer Unni");
INSERT INTO transaction VALUES("269","61","19","3","Package Receivable","0","100","Package Receivable: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3,Pac:Singapore ,By:Thaju,To:Tds");
INSERT INTO transaction VALUES("270","19","61","3","Package Receivable","100","0","Package Receivable: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3,Pac:Singapore ,By:Thaju,To:Tds");
INSERT INTO transaction VALUES("271","61","18","3","Package Receivable","0","100","Package Receivable: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3,Pac:Singapore ,By:Thaju,To:Tax");
INSERT INTO transaction VALUES("272","18","61","3","Package Receivable","100","0","Package Receivable: 3","2016-08-20","2016-08-20","63","16","3","3","INV#IP3,Pac:Singapore ,By:Thaju,To:Tax");
INSERT INTO transaction VALUES("273","5","62","7","Package Airline","0","1000","Purchase Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4 ,Pac:Singapore ,Pax:A,M,17 ,By:Thaju");
INSERT INTO transaction VALUES("274","62","5","7","Package Airline","1000","0","Purchase Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4 ,Pac:Singapore ,Pax:A,M,17 ,By:Thaju");
INSERT INTO transaction VALUES("275","11","62","5","Package Hotel","0","1000","Purchase Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4 ,Pac:Singapore ,Pax:A,M,17,By:Thaju");
INSERT INTO transaction VALUES("276","62","11","5","Package Hotel","1000","0","Purchase Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4 ,Pac:Singapore ,Pax:A,M,17,By:Thaju");
INSERT INTO transaction VALUES("277","7","62","7","Package Visa","0","1000","Purchase Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4 ,Pac:Singapore ,Pax:A ,M,17 ,By:Thaju");
INSERT INTO transaction VALUES("278","62","7","7","Package Visa","1000","0","Purchase Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4 ,Pac:Singapore ,Pax:A ,M,17 ,By:Thaju");
INSERT INTO transaction VALUES("279","13","62","5","Package Transportation","0","1000","Purchase Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4 ,Pac:Singapore ,Pax:A,M,17,By:Thaju");
INSERT INTO transaction VALUES("280","62","13","5","Package Transportation","1000","0","Purchase Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4 ,Pac:Singapore ,Pax:A,M,17,By:Thaju");
INSERT INTO transaction VALUES("281","61","8","4","Package Receivable","0","6600","Invoice Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4,Pac:Singapore ,Pax:A M 17 PassNo-df44 ,By:Thaju");
INSERT INTO transaction VALUES("282","8","61","4","Package Receivable","6600","0","Invoice Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4,Pac:Singapore ,Pax:A M 17 PassNo-df44 ,By:Thaju");
INSERT INTO transaction VALUES("283","16","61","4","Package Receivable","0","100","Invoice Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4,Pac:Singapore ,By:Thaju,To:Customer Unni");
INSERT INTO transaction VALUES("284","61","16","4","Package Receivable","100","0","Invoice Package: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4,Pac:Singapore ,By:Thaju,To:Customer Unni");
INSERT INTO transaction VALUES("285","61","19","4","Package Receivable","0","100","Package Receivable: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4,Pac:Singapore ,By:Thaju,To:Tds");
INSERT INTO transaction VALUES("286","19","61","4","Package Receivable","100","0","Package Receivable: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4,Pac:Singapore ,By:Thaju,To:Tds");
INSERT INTO transaction VALUES("287","61","18","4","Package Receivable","0","100","Package Receivable: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4,Pac:Singapore ,By:Thaju,To:Tax");
INSERT INTO transaction VALUES("288","18","61","4","Package Receivable","100","0","Package Receivable: 4","2016-08-20","2016-08-20","63","16","3","4","INV#IP4,Pac:Singapore ,By:Thaju,To:Tax");
INSERT INTO transaction VALUES("289","64","4","1","Airline Receivable","0","108","Airline Receivable: 1","2016-08-20","2016-08-20","70","19","3","0","Airline Receivable(IA), INV#IA1, Pax:Alisha Pass, Passport No:23, Sector:sdf/sdf, Date:31-08-2016, Tkt:4, PNR:3, By:Alisha");
INSERT INTO transaction VALUES("290","4","64","1","Airline Receivable","108","0","Airline Receivable: 1","2016-08-20","2016-08-20","70","19","3","0","Airline Receivable(IA), INV#IA1, Pax:Alisha Pass, Passport No:23, Sector:sdf/sdf, Date:31-08-2016, Tkt:4, PNR:3, By:Alisha");
INSERT INTO transaction VALUES("291","16","64","1","Airline Receivable","0","4","Airline Receivable:Dis# 1","2016-08-20","2016-08-20","70","19","3","0","Airline Receivable(IA), INV#IA1, Pax:Alisha Pass, Passport No:23, Sector:sdf/sdf, Date:31-08-2016, Tkt:4, PNR:3, By:Alisha");
INSERT INTO transaction VALUES("292","64","16","1","Airline Receivable","4","0","Airline Receivable:Dis#1","2016-08-20","2016-08-20","70","19","3","0","Airline Receivable(IA), INV#IA1, Pax:Alisha Pass, Passport No:23, Sector:sdf/sdf, Date:31-08-2016, Tkt:4, PNR:3, By:Alisha");
INSERT INTO transaction VALUES("293","64","19","1","Airline Receivable","0","3","Airline Receivable:Tds#1","2016-08-20","2016-08-20","70","19","3","0","Airline Receivable(IA), INV#IA1, Pax:Alisha Pass, Passport No:23, Sector:sdf/sdf, Date:31-08-2016, Tkt:4, PNR:3, By:Alisha");
INSERT INTO transaction VALUES("294","19","64","1","Airline Receivable","3","0","Airline Receivable:Tds#1","2016-08-20","2016-08-20","70","19","3","0","Airline Receivable(IA), INV#IA1, Pax:Alisha Pass, Passport No:23, Sector:sdf/sdf, Date:31-08-2016, Tkt:4, PNR:3, By:Alisha");
INSERT INTO transaction VALUES("295","64","18","1","Airline Receivable","0","3","Airline Receivable:Tax#1","2016-08-20","2016-08-20","70","19","3","0","Airline Receivable(IA), INV#IA1, Pax:Alisha Pass, Passport No:23, Sector:sdf/sdf, Date:31-08-2016, Tkt:4, PNR:3, By:Alisha");
INSERT INTO transaction VALUES("296","18","64","1","Airline Receivable","3","0","Airline Receivable:Tax#1","2016-08-20","2016-08-20","70","19","3","0","Airline Receivable(IA), INV#IA1, Pax:Alisha Pass, Passport No:23, Sector:sdf/sdf, Date:31-08-2016, Tkt:4, PNR:3, By:Alisha");
INSERT INTO transaction VALUES("297","65","5","1","Airline Payable","108","0","","2016-08-20","2016-08-20","70","19","3","0","PINV#PA1,sup:65,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("298","5","65","1","Airline Payable","0","108","","2016-08-20","2016-08-20","70","19","3","0","PINV#PA1,sup:65,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("299","65","19","1","Airline Payable","4","0","Tds:inv#1","2016-08-20","2016-08-20","70","19","3","0","PINV#PA1,sup:65,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("300","19","65","1","Airline Payable","0","4","Tds:inv#1","2016-08-20","2016-08-20","70","19","3","0","PINV#PA1,sup:65,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("301","65","20","1","Airline Payable","0","23","Tac:inv#1","2016-08-20","2016-08-20","70","19","3","0","PINV#PA1,sup:65,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("302","20","65","1","Airline Payable","23","0","Tac:inv#1","2016-08-20","2016-08-20","70","19","3","0","PINV#PA1,sup:65,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("303","67","22","1","Other Purchase","2200","0","","2016-08-21","2016-08-21","72","21","0","0","");
INSERT INTO transaction VALUES("304","22","67","1","Other Purchase","0","2200","","2016-08-21","2016-08-21","72","21","0","0","");
INSERT INTO transaction VALUES("305","67","16","1","Other Purchase","100","0","Discount Given:inv#1","2016-08-21","2016-08-21","72","21","0","0","");
INSERT INTO transaction VALUES("306","16","67","1","Other Purchase","0","100","Discount Given:inv#1","2016-08-21","2016-08-21","72","21","0","0","");
INSERT INTO transaction VALUES("307","67","19","1","Other Purchase","100","0","Tds:inv#1","2016-08-21","2016-08-21","72","21","0","0","");
INSERT INTO transaction VALUES("308","19","67","1","Other Purchase","0","100","Tds:inv#1","2016-08-21","2016-08-21","72","21","0","0","");
INSERT INTO transaction VALUES("309","67","18","1","Other Purchase","100","0","Tax:inv#1","2016-08-21","2016-08-21","72","21","0","0","");
INSERT INTO transaction VALUES("310","18","67","1","Other Purchase","0","100","Tax:inv#1","2016-08-21","2016-08-21","72","21","0","0","");
INSERT INTO transaction VALUES("311","67","20","1","Other Purchase","0","100","Tac:inv#1","2016-08-21","2016-08-21","72","21","0","0","");
INSERT INTO transaction VALUES("312","20","67","1","Other Purchase","100","0","Tac:inv#1","2016-08-21","2016-08-21","72","21","0","0","");
INSERT INTO transaction VALUES("313","66","4","1","Airline Receivable","0","400","Airline Receivable: 1","2016-08-21","2016-08-21","72","21","3","0","Airline Receivable(IA), INV#IA1, Pax:Sundaypasngr1, Passport No:ps45, Sector:rt/ff, Date:15-08-2016, Tkt:tckt566, PNR:0, By:Sundaystaff1");
INSERT INTO transaction VALUES("314","4","66","1","Airline Receivable","400","0","Airline Receivable: 1","2016-08-21","2016-08-21","72","21","3","0","Airline Receivable(IA), INV#IA1, Pax:Sundaypasngr1, Passport No:ps45, Sector:rt/ff, Date:15-08-2016, Tkt:tckt566, PNR:0, By:Sundaystaff1");
INSERT INTO transaction VALUES("315","67","5","1","Airline Payable","600","0","","2016-08-21","2016-08-21","72","21","3","0","PINV#PA1,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("316","5","67","1","Airline Payable","0","600","","2016-08-21","2016-08-21","72","21","3","0","PINV#PA1,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("317","67","16","1","Airline Payable","100","0","Discount Given:inv#1","2016-08-21","2016-08-21","72","21","3","0","PINV#PA1,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("318","16","67","1","Airline Payable","0","100","Discount Given:inv#1","2016-08-21","2016-08-21","72","21","3","0","PINV#PA1,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("319","67","19","1","Airline Payable","100","0","Tds:inv#1","2016-08-21","2016-08-21","72","21","3","0","PINV#PA1,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("320","19","67","1","Airline Payable","0","100","Tds:inv#1","2016-08-21","2016-08-21","72","21","3","0","PINV#PA1,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("321","67","18","1","Airline Payable","100","0","Tax:inv#1","2016-08-21","2016-08-21","72","21","3","0","PINV#PA1,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("322","18","67","1","Airline Payable","0","100","Tax:inv#1","2016-08-21","2016-08-21","72","21","3","0","PINV#PA1,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("323","67","20","1","Airline Payable","0","500","Tac:inv#1","2016-08-21","2016-08-21","72","21","3","0","PINV#PA1,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("324","20","67","1","Airline Payable","500","0","Tac:inv#1","2016-08-21","2016-08-21","72","21","3","0","PINV#PA1,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("325","67","5","2","Airline Payable","600","0","","2016-08-21","2016-08-21","72","21","3","0","PINV#PA2,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("326","5","67","2","Airline Payable","0","600","","2016-08-21","2016-08-21","72","21","3","0","PINV#PA2,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("327","67","16","2","Airline Payable","100","0","Discount Given:inv#2","2016-08-21","2016-08-21","72","21","3","0","PINV#PA2,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("328","16","67","2","Airline Payable","0","100","Discount Given:inv#2","2016-08-21","2016-08-21","72","21","3","0","PINV#PA2,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("329","67","19","2","Airline Payable","100","0","Tds:inv#2","2016-08-21","2016-08-21","72","21","3","0","PINV#PA2,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("330","19","67","2","Airline Payable","0","100","Tds:inv#2","2016-08-21","2016-08-21","72","21","3","0","PINV#PA2,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("331","67","18","2","Airline Payable","100","0","Tax:inv#2","2016-08-21","2016-08-21","72","21","3","0","PINV#PA2,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("332","18","67","2","Airline Payable","0","100","Tax:inv#2","2016-08-21","2016-08-21","72","21","3","0","PINV#PA2,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("333","67","20","2","Airline Payable","0","500","Tac:inv#2","2016-08-21","2016-08-21","72","21","3","0","PINV#PA2,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("334","20","67","2","Airline Payable","500","0","Tac:inv#2","2016-08-21","2016-08-21","72","21","3","0","PINV#PA2,sup:67,details:Airline Receivable(IA), INV#IA1, Pax:Sernamcust1pasngr1, Passport No:wer433, Sector:trerr/hggfg, Date:19-08-2016, Tkt:uy677, PNR:0, By:Thaju");
INSERT INTO transaction VALUES("335","66","4","2","Airline Receivable","0","330","Airline Receivable: 2","2016-08-21","2016-08-21","72","21","3","0","Airline Receivable(IA), INV#IA2, Pax:Sundaypasngr1, Passport No:ps45, Sector:ghh/yuuuu, Date:16-08-2016, Tkt:fdfg45, PNR:0, By:Sundaystaff1");
INSERT INTO transaction VALUES("336","4","66","2","Airline Receivable","330","0","Airline Receivable: 2","2016-08-21","2016-08-21","72","21","3","0","Airline Receivable(IA), INV#IA2, Pax:Sundaypasngr1, Passport No:ps45, Sector:ghh/yuuuu, Date:16-08-2016, Tkt:fdfg45, PNR:0, By:Sundaystaff1");
INSERT INTO transaction VALUES("337","16","66","2","Airline Receivable","0","100","Airline Receivable:Dis# 2","2016-08-21","2016-08-21","72","21","3","0","Airline Receivable(IA), INV#IA2, Pax:Sundaypasngr1, Passport No:ps45, Sector:ghh/yuuuu, Date:16-08-2016, Tkt:fdfg45, PNR:0, By:Sundaystaff1");
INSERT INTO transaction VALUES("338","66","16","2","Airline Receivable","100","0","Airline Receivable:Dis#2","2016-08-21","2016-08-21","72","21","3","0","Airline Receivable(IA), INV#IA2, Pax:Sundaypasngr1, Passport No:ps45, Sector:ghh/yuuuu, Date:16-08-2016, Tkt:fdfg45, PNR:0, By:Sundaystaff1");
INSERT INTO transaction VALUES("339","66","19","2","Airline Receivable","0","100","Airline Receivable:Tds#2","2016-08-21","2016-08-21","72","21","3","0","Airline Receivable(IA), INV#IA2, Pax:Sundaypasngr1, Passport No:ps45, Sector:ghh/yuuuu, Date:16-08-2016, Tkt:fdfg45, PNR:0, By:Sundaystaff1");
INSERT INTO transaction VALUES("340","19","66","2","Airline Receivable","100","0","Airline Receivable:Tds#2","2016-08-21","2016-08-21","72","21","3","0","Airline Receivable(IA), INV#IA2, Pax:Sundaypasngr1, Passport No:ps45, Sector:ghh/yuuuu, Date:16-08-2016, Tkt:fdfg45, PNR:0, By:Sundaystaff1");
INSERT INTO transaction VALUES("341","66","18","2","Airline Receivable","0","100","Airline Receivable:Tax#2","2016-08-21","2016-08-21","72","21","3","0","Airline Receivable(IA), INV#IA2, Pax:Sundaypasngr1, Passport No:ps45, Sector:ghh/yuuuu, Date:16-08-2016, Tkt:fdfg45, PNR:0, By:Sundaystaff1");
INSERT INTO transaction VALUES("342","18","66","2","Airline Receivable","100","0","Airline Receivable:Tax#2","2016-08-21","2016-08-21","72","21","3","0","Airline Receivable(IA), INV#IA2, Pax:Sundaypasngr1, Passport No:ps45, Sector:ghh/yuuuu, Date:16-08-2016, Tkt:fdfg45, PNR:0, By:Sundaystaff1");
INSERT INTO transaction VALUES("343","67","5","3","Airline Payable","120","0","","2016-08-21","2016-08-21","72","21","3","0","PINV#PA3,sup:67,details:Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");
INSERT INTO transaction VALUES("344","5","67","3","Airline Payable","0","120","","2016-08-21","2016-08-21","72","21","3","0","PINV#PA3,sup:67,details:Airline Receivable(IA), INV#IA2, Pax:Thajucust2pasngr1, Passport No:gfh554, Sector:DL/KL, Date:19-08-2016, Tkt:1234A, PNR:0, By:Minnu");

DROP TABLE IF EXISTS transport_provider;
CREATE TABLE `transport_provider` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `providerName` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
INSERT INTO transport_provider VALUES("1","Al Akbar","","","13","62","3");
INSERT INTO transport_provider VALUES("2","Galaxy","","","13","62","3");
INSERT INTO transport_provider VALUES("3","Kallada","","","13","62","3");
INSERT INTO transport_provider VALUES("4","Galaxy","abc","","16","63","3");
INSERT INTO transport_provider VALUES("5","Kallada","","","16","63","3");
INSERT INTO transport_provider VALUES("6","Scania","","","16","63","3");
INSERT INTO transport_provider VALUES("8","Galaxy","","","21","72","3");
INSERT INTO transport_provider VALUES("9","Kallada","","","21","72","3");
INSERT INTO transport_provider VALUES("10","Mba","","","21","72","3");
INSERT INTO transport_provider VALUES("11","Suv","","","21","72","3");

DROP TABLE IF EXISTS user_permission;
CREATE TABLE `user_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NOT NULL,
  `basicSetting` int(11) NOT NULL DEFAULT '0',
  `basicSettingEdit` int(11) NOT NULL DEFAULT '0',
  `package` int(11) NOT NULL DEFAULT '0',
  `packageEdit` int(11) NOT NULL DEFAULT '0',
  `account` int(11) NOT NULL DEFAULT '0',
  `accountEdit` int(11) NOT NULL DEFAULT '0',
  `cashBank` int(11) NOT NULL DEFAULT '0',
  `cashBankEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceAirline` int(11) NOT NULL DEFAULT '0',
  `invoiceAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceHotel` int(11) NOT NULL DEFAULT '0',
  `invoiceHotelEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceVisa` int(11) NOT NULL DEFAULT '0',
  `invoiceVisaEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceMisc` int(11) NOT NULL DEFAULT '0',
  `invoiceMiscEdit` int(11) NOT NULL DEFAULT '0',
  `invoiceTransport` int(11) NOT NULL DEFAULT '0',
  `invoiceTransportEdit` int(11) NOT NULL DEFAULT '0',
  `invoicePackage` int(11) NOT NULL DEFAULT '0',
  `invoicePackageEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseAirline` int(11) NOT NULL DEFAULT '0',
  `purchaseAirlineEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseHotel` int(11) NOT NULL DEFAULT '0',
  `purchaseHotelEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseVisa` int(11) NOT NULL DEFAULT '0',
  `purchaseVisaEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseMisc` int(11) NOT NULL DEFAULT '0',
  `purchaseMiscEdit` int(11) NOT NULL DEFAULT '0',
  `purchaseTransport` int(11) NOT NULL DEFAULT '0',
  `purchaseTransportEdit` int(11) NOT NULL DEFAULT '0',
  `purchasePackage` int(11) NOT NULL DEFAULT '0',
  `purchasePackageEdit` int(11) NOT NULL DEFAULT '0',
  `accountReport` int(11) NOT NULL DEFAULT '0',
  `otherReport` int(11) NOT NULL DEFAULT '0',
  `user` int(11) NOT NULL DEFAULT '0',
  `userEdit` int(11) NOT NULL DEFAULT '0',
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `staffId` (`staffId`),
  CONSTRAINT `user_permission_ibfk_1` FOREIGN KEY (`staffId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
INSERT INTO user_permission VALUES("2","12","1","1","0","0","1","0","0","0","1","1","0","0","1","1","0","0","1","1","0","0","1","1","0","0","1","1","0","0","1","1","0","0","0","0","0","0","0");
INSERT INTO user_permission VALUES("3","14","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("4","15","1","1","0","0","1","1","1","1","1","1","1","1","0","0","0","0","0","0","0","0","1","1","1","1","0","0","0","0","0","0","0","0","1","1","0","0","0");
INSERT INTO user_permission VALUES("5","16","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("6","17","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("7","18","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("8","19","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("9","20","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("10","21","1","1","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1","0","0","0");
INSERT INTO user_permission VALUES("11","22","1","1","0","0","1","1","1","1","1","1","1","1","0","0","1","1","1","1","0","0","1","1","1","1","0","0","1","1","1","1","0","0","1","1","0","0","0");

DROP TABLE IF EXISTS vehicle_types;
CREATE TABLE `vehicle_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `vehicleType` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
INSERT INTO vehicle_types VALUES("1","4 Wheeler","13","62","3");
INSERT INTO vehicle_types VALUES("2","3 Wheeler","13","62","3");
INSERT INTO vehicle_types VALUES("3","2 Wheeler","13","62","3");
INSERT INTO vehicle_types VALUES("4","Tempo Travel","13","62","3");
INSERT INTO vehicle_types VALUES("5","Car","16","63","3");
INSERT INTO vehicle_types VALUES("6","Suv","16","63","3");
INSERT INTO vehicle_types VALUES("7","Tempo Traveller","16","63","3");
INSERT INTO vehicle_types VALUES("8","Volvo Bus","16","63","3");
INSERT INTO vehicle_types VALUES("9","Suv","21","72","3");
INSERT INTO vehicle_types VALUES("10","Muv","21","72","3");
INSERT INTO vehicle_types VALUES("11","Kuv","21","72","3");
INSERT INTO vehicle_types VALUES("12","Duksa","21","72","3");
INSERT INTO vehicle_types VALUES("13","Dud","21","73","3");

DROP TABLE IF EXISTS visa_passenger;
CREATE TABLE `visa_passenger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visaId` int(11) NOT NULL,
  `passengerName` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(20) NOT NULL,
  `passportNo` varchar(50) NOT NULL,
  `visaFor` varchar(50) NOT NULL,
  `submissionDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `dispatchDate` date NOT NULL,
  `tokenNo` varchar(20) NOT NULL,
  `visaFee` float NOT NULL,
  `vfs` float NOT NULL,
  `ddCharge` float NOT NULL,
  `serviceCharge` float NOT NULL,
  `otherCharge` float NOT NULL,
  `courierCharge` float NOT NULL,
  `subTotal` float NOT NULL,
  `travelDate` date NOT NULL,
  `visaMessage` varchar(20) NOT NULL,
  `msgChngBy` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
INSERT INTO visa_passenger VALUES("1","1","AKHILA","0000-00-00","M","K0060995","11","0000-00-00","0000-00-00","0000-00-00","","14200","0","0","600","0","0","14800","0000-00-00","","0","","18","66","3");
INSERT INTO visa_passenger VALUES("2","2","Sernamcust2psngr2","2006-08-06","F","xx","6","2016-08-19","2016-08-18","2016-08-24","hg66","1000","10","20","70","100","100","1300","0000-00-00","","0","","16","63","3");
INSERT INTO visa_passenger VALUES("3","3","Sernamcustpangr1","2016-08-01","M","dd","9","2016-08-26","2016-08-18","2016-08-18","","900","10","20","200","100","70","1300","0000-00-00","","0","","16","63","3");
INSERT INTO visa_passenger VALUES("4","4","Thajucust2pasngr1","1996-08-13","M","gfh554","8","2016-08-19","2016-08-22","2016-08-30","","1500","100","100","200","100","0","2000","0000-00-00","","0","","16","63","3");

DROP TABLE IF EXISTS visafor;
CREATE TABLE `visafor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `visafor` varchar(100) NOT NULL,
  `proId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL,
  `finYear` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
INSERT INTO visafor VALUES("1","sigapore","13","62","3");
INSERT INTO visafor VALUES("2","dubai","13","62","3");
INSERT INTO visafor VALUES("3","qater","13","62","3");
INSERT INTO visafor VALUES("4","saudi             arabia","13","62","3");
INSERT INTO visafor VALUES("5","singapore","16","63","3");
INSERT INTO visafor VALUES("6","malasiya","16","63","3");
INSERT INTO visafor VALUES("7","qater","16","63","3");
INSERT INTO visafor VALUES("8","dubai","16","63","3");
INSERT INTO visafor VALUES("9","bahrain","16","63","3");
INSERT INTO visafor VALUES("10","90 DAYS DXB  WITH OTB","18","66","3");
INSERT INTO visafor VALUES("11","90 DAYS DXB  WITHOUT OTB","18","66","3");
INSERT INTO visafor VALUES("12","30 DAYS DXB WITH OTB","18","66","3");
INSERT INTO visafor VALUES("13","30 DAYS DXB WITHOUT OTB","18","66","3");
INSERT INTO visafor VALUES("14","30 DAYS QATAR BUSINESS","18","66","3");
INSERT INTO visafor VALUES("15","90 DAYS DXB SETTING","18","66","3");
INSERT INTO visafor VALUES("16","USA","21","72","3");



								/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
								/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
								/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
								/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
								/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
								/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
								/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
								
						