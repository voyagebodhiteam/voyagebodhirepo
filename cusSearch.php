<?php
require("config/config.inc.php"); 
require("config/Database.class.php");
require("config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();

?>
<form method="post">
	<input type="text" name="searchName">
	<input type="submit" name="ticket" value="Ticket details">
	<input type="submit" name="visa" value="Visa details">
</form>
<?php
$today= date('Y-m-d');
$date = strtotime("$today -1 year");
$startDate = date ( 'Y-m-d' , $date );
if(@$_REQUEST['ticket'])
{
	$key=$_REQUEST['searchName'];
	$sel="select ID from ".TABLE_ACCOUNTS." WHERE customerKey='$key'";
	$re=mysql_query($sel);
	$resTicket=mysql_fetch_array($re);
	$custId=$resTicket['ID'];//echo $custId;
	
	$select="select ID,netAmount,invoiceNo,proId from ".TABLE_INVOICE_AIRLINE." WHERE customerId='$custId' and invoiceDate>'$startDate'";
	$res=mysql_query($select);
	$num=mysql_num_rows($res);
	?>
	 <table class="table table-bordered table-striped show_table">
        <thead>
            <tr>
                <th rowspan="2">Sl No</th>
                <th rowspan="2">Passenger</th>
                <th colspan="5">Flight Details</th>
				<!--<th>Ticket No</th>
				<th>From</th>
				<th>To</th>                                            
				<th>FLT No</th>
				<th>Date</th>-->
            </tr>
            <tr>               
				<th>Ticket No</th>
				<th>From</th>
				<th>To</th>                                            
				<th>FLT No</th>
				<th>Date</th>
            </tr>
        </thead>
        <tbody>
        <?php
        if($num==0)
        {
			?>
			<th colspan="7">No data found</th>
			<?php  
		}
		else
		{ $i=0;
			while($result=mysql_fetch_array($res))
			{		$i++;		
				$airlineId=$result['ID'];
				$netAmount=$result['netAmount'];
				$invoiceNo=$result['invoiceNo'];	
				$proId=$result['proId'];
				$cashReceiptQry="SELECT sum(payingAmount) as paidAmt FROM `".TABLE_CASH_RECEIPT."` p 
								WHERE voucherType='airline' 
								and invoiceNo='$invoiceNo'
								and proId='$proId'								
							GROUP BY invoiceNo,voucherType"; 
				$cashRes= $db->query($cashReceiptQry);
				$cashNum=mysql_num_rows($cashRes);	
				if($cashNum==0)
				{
					$paidAmount=0;
				}
				else{
					$cashRow=mysql_fetch_array($cashRes);
					$paidAmount=$cashRow['paidAmt'];
				}
				if($paidAmount==$netAmount)
				{
					 $select2 = 	"select  * from ".TABLE_AIRLINE_PASSENGER." where airlineId='$airlineId'";
					$res2 	 = 	$db->query($select2);		
					while($result2	= 	mysql_fetch_array($res2))
					{
						$passId=$result2['ID'];
						$select3 = 	"select  * from ".TABLE_AIRLINE_FLIGHT." where airlinePassId='$passId'";
						$res3 = 	$db->query($select3);
						$num3	=	mysql_num_rows($res3);									
						?>																
						<td rowspan="<?php echo $num3; ?>"><?php echo  $i;$i++?></td>
						<td rowspan="<?php echo $num3; ?>"><?php echo $result2['passengerName']; ?></td>
						<td colspan="5">
							<table>
							<?php
							
							while($result3	= 	mysql_fetch_array($res3))
							{
							?>					
								<tr>
									<td><?php echo $result3['ticketNo']; ?></td>
									<td><?php echo $result3['sectorFrom']; ?></td>
									<td><?php echo $result3['sectorTo']; ?></td>
									<td><?php echo $result3['flightNo']; ?></td>
									<td><?php echo $result3['journeyDate']; ?></td>
								</tr>					
							<?php					
							}
							?>
							</table>
						</td>
						<?php
					}	
				 }                     				
			}
		}
		?>
        </tbody>
      </table>
	<?php	
}
if(@$_REQUEST['visa'])
{
	$key=$_REQUEST['searchName'];
	$sel="select ID from ".TABLE_ACCOUNTS." WHERE customerKey='$key'";
	$re=mysql_query($sel);
	$resul=mysql_fetch_array($re);
	$custId=$resul['ID'];//echo $sel;
	
	$select="select ID,netAmount,invoiceNo,proId from ".TABLE_INVOICE_VISA." WHERE customerId='$custId' and invoiceDate>'$startDate'"; //echo $select;
	$res=mysql_query($select);
	$num=mysql_num_rows($res);
	?>
	 <table class="table table-bordered table-striped show_table">
        <thead>
            <tr>
                <th>Sl No</th>
                <th>Passenger</th>
                <th>Passport No</th>
				<th>Visa For</th>
				<th>Travel Date</th>
				<th>status</th>                                            				
            </tr>           
        </thead>
        <tbody>
        <?php
        if($num==0)
        {
			?>
			<th colspan="6">No data found</th>
			<?php  
		}
		else
		{ $i=0;
			while($result=mysql_fetch_array($res))
			{		
				$i++;		
				$visaId=$result['ID'];	
				$netAmount=$result['netAmount'];
				$invoiceNo=$result['invoiceNo'];	
				$proId=$result['proId'];
				$cashReceiptQry="SELECT sum(payingAmount) as paidAmt FROM `".TABLE_CASH_RECEIPT."` p 
								WHERE voucherType='visa' 
								and invoiceNo='$invoiceNo'
								and proId='$proId'								
							GROUP BY invoiceNo,voucherType"; 
				$cashRes= $db->query($cashReceiptQry);
				$cashNum=mysql_num_rows($cashRes);	
				if($cashNum==0)
				{
					$paidAmount=0;
				}
				else{
					$cashRow=mysql_fetch_array($cashRes);
					$paidAmount=$cashRow['paidAmt'];
				}
				if($paidAmount==$netAmount)
				{
				                        
				$select2 = 	"select  * from ".TABLE_VISA_PASSENGER." where visaId='$visaId'";
				$res2 	 = 	$db->query($select2);		
				while($result2	= 	mysql_fetch_array($res2))
				{												
					?>																			
					<tr>
						<td><?php echo $i;$i++ ?></td>
						<td><?php echo $result2['passengerName']; ?></td>
						<td><?php echo $result2['passportNo']; ?></td>
						<td><?php echo $result2['visaFor']; ?></td>
						<td><?php echo $result2['travelDate']; ?></td>
						<td><?php echo $result2['status']; ?></td>
					</tr>										
				<?php
				}
			}
		}
	}
		?>
        </tbody>
      </table>
	<?php	
}
?>