<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
/*if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"ac.proId='$proId'";
}*/
// Connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$acResult = [];
$acDetails = [];
$staticID = $_REQUEST['accID'];
$apiKey = $_REQUEST['apiKey'];
if($apiKey == 'api123'){
    $acSelect = "select ac.ID as acID,ac.agentId as agID, ac.accountGroupId, ac.accountName, ac.openingBal, ac.payType, ac.addDate, ac.loginId, ac.description, ac.contactPerson, ac.address, ac.pin, ac.city, ac.phone, ac.mobile, ac.email, ac.address2, ac.pin2, ac.city2, acG.ID as acGId, acG.acGroup, log.ID as logID, log.userName from `".TABLE_ACCOUNTS."` ac, `".TABLE_ACCOUNT_GROUP."` acG, `".TABLE_LOGIN."` log where ac.ID = ".$staticID." and ac.accountGroupId = acG.ID and ac.loginId = log.ID ";
//echo $acSelect;
    $acQry = mysqli_query($connection, $acSelect);
    if(mysqli_num_rows($acQry) > 0)
    {
        $acRow = mysqli_fetch_array($acQry);
        //print_r($acRow);
        $acResult['status'] = "ok";
         if($acRow['agID'] != null)
        {
        	$aId=$acRow['agID']; 
			$editSelect2 = 	"select  accountName                          			
			from ".TABLE_ACCOUNTS." 
			where ID='$aId'";
			$editResult2 = 	mysqli_query($connection, $editSelect2);
			$editRow2	 = 	mysqli_fetch_array($editResult2);
            $acDetails['agentName'] = ['title' => 'Agent Name', 'dataValue' => $editRow2['accountName']];
            $acDetails['agentId'] = ['title' => 'Agent ID', 'dataValue' => $aId];
        }
        if($acRow['accountName'] != null)
        {
            $acDetails['accountName'] = ['title' => 'Account Name', 'dataValue' => $acRow['accountName']];
        }
        if($acRow['acGroup'] != null)
        {
            $acDetails['parent'] = ['title' => 'Parent Account', 'dataValue' => $acRow['acGroup']];
        }
        if($acRow['openingBal'] != null)
        {
            $acDetails['openingBalance'] = ['title' => 'Opening Balance', 'dataValue' => $acRow['openingBal']];
        }
        if($acRow['payType'] != null)
        {
            $acDetails['payType'] = ['title' => 'Pay Type', 'dataValue' => $acRow['payType']];
        }
        if($acRow['addDate'] != null)
        {
        	$invDateArray = explode('-', $acRow['addDate']);
			$tempArray = $invDateArray[2].'-'.$invDateArray[1].'-'.$invDateArray[0];
            $acDetails['addDate'] = ['title' => 'Added Date', 'dataValue' => $tempArray];
        }
        if($acRow['userName'] != null)
        {
            $acDetails['userName'] = ['title' => 'User Name', 'dataValue' => $acRow['userName']];
        }
        if($acRow['description'] != null)
        {
            $acDetails['description'] = ['title' => 'Description', 'dataValue' => $acRow['description']];
        }
        if($acRow['contactPerson'] != null)
        {
            $acDetails['contactPerson'] = ['title' => 'Contact Person', 'dataValue' => $acRow['contactPerson']];
        }
        if($acRow['address'] != null)
        {
            $acDetails['address'] = ['title' => 'Address', 'dataValue' => $acRow['address']];
        }
        if($acRow['pin'] != null)
        {
            $acDetails['pin'] = ['title' => 'Pin', 'dataValue' => $acRow['pin']];
        }
        if($acRow['city'] != null)
        {
            $acDetails['city'] = ['title' => 'City', 'dataValue' => $acRow['city']];
        }
        if($acRow['phone'] != null)
        {
            $acDetails['phone'] = ['title' => 'Phone', 'dataValue' => $acRow['phone']];
        }
        if($acRow['mobile'] != null)
        {
            $acDetails['mobile'] = ['title' => 'Mobile', 'dataValue' => $acRow['mobile']];
        }
        if($acRow['email'] != null)
        {
            $acDetails['email'] = ['title' => 'Email ID', 'dataValue' => $acRow['email']];
        }
        if($acRow['address2'] != null)
        {
            $acDetails['address2'] = ['title' => 'Address 2', 'dataValue' => $acRow['address2']];
        }
        if($acRow['pin2'] != null)
        {
            $acDetails['pin2'] = ['title' => 'Pin 2', 'dataValue' => $acRow['pin2']];
        }
        if($acRow['city2'] != null)
        {
            $acDetails['city2'] = ['title' => 'City 2', 'dataValue' => $acRow['city2']];
        }
        //print_r($acDetails);
        $acResult['details'] = $acDetails;
        //print_r($acResult);
        header('Content-type: application/json');
        echo json_encode($acResult);
    }
    else
    {
        $acResult['status'] = "failure";
        $acResult['error'] = "No data found";
        header('Content-type: application/json');
        echo json_encode($acResult);
    }
}
else
{
    $acResult['status'] = "failure";
    $acResult['error'] = "apiKey not valid";
    header('Content-type: application/json');
    echo json_encode($acResult);
}

