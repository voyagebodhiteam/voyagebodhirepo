<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
        $finYear	=	$_SESSION['finYear'];
	$check 	= 	"proId='$proId'";
}
// Connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$acResult = [];
$acDetails = [];
$ID = $_REQUEST['accID'];
$apiKey = $_REQUEST['apiKey'];
if($apiKey == 'api123'){
    $acSelect = "select * from ".TABLE_HOTEL." where ID='$ID' and $check and ".TABLE_HOTEL.".finYear=$finYear";
	//echo $acSelect;
    $acQry = mysqli_query($connection, $acSelect);
    if(mysqli_num_rows($acQry) > 0)
    {
        $acRow = mysqli_fetch_array($acQry);
        //print_r($acRow);
        $acResult['status'] = "ok";
        if($acRow['hotelName'] != null)
        {
            $acDetails['hotelName'] = ['title' => 'Hotel Name', 'dataValue' => $acRow['hotelName']];
        }
        if($acRow['address'] != null)
        {
            $acDetails['address'] = ['title' => 'Address', 'dataValue' => $acRow['address']];
        }
        if($acRow['pin'] != null)
        {
            $acDetails['pin'] = ['title' => 'Pin', 'dataValue' => $acRow['pin']];
        }
        if($acRow['city'] != null)
        {
            $acDetails['city'] = ['title' => 'City', 'dataValue' => $acRow['city']];
        }
        if($acRow['state'] != null)
        {
            $acDetails['state'] = ['title' => 'State', 'dataValue' => $acRow['state']];
        }
        if($acRow['country'] != null)
        {
            $acDetails['country'] = ['title' => 'Country', 'dataValue' => $acRow['country']];
        }
        if($acRow['phone'] != null)
        {
            $acDetails['phone'] = ['title' => 'Phone', 'dataValue' => $acRow['phone']];
        }
        if($acRow['mobile'] != null)
        {
            $acDetails['mobile'] = ['title' => 'Mobile', 'dataValue' => $acRow['mobile']];
        }
        if($acRow['email'] != null)
        {
            $acDetails['email'] = ['title' => 'Email', 'dataValue' => $acRow['email']];
        }
        if($acRow['website'] != null)
        {
            $acDetails['website'] = ['title' => 'Website', 'dataValue' => $acRow['website']];
        }
        //print_r($acDetails);
        $acResult['details'] = $acDetails;
        //print_r($acResult);
        header('Content-type: application/json');
        echo json_encode($acResult);
    }
    else
    {
        $acResult['status'] = "failure";
        $acResult['error'] = "No data found";
        header('Content-type: application/json');
        echo json_encode($acResult);
    }
}
else
{
    $acResult['status'] = "failure";
    $acResult['error'] = "apiKey not valid";
    header('Content-type: application/json');
    echo json_encode($acResult);
}