<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
/*if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"proId='$proId'";
}*/

// Connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$servicesArray = explode(',', $_REQUEST['services']);
$packageID = $_REQUEST['packageID'];
$extra = 0;
$discount = 0;
$confirmQry	=	mysqli_query($connection, "SELECT confirm,discount,extra FROM  ".TABLE_PACKAGE." WHERE ID=$packageID ");
$confirmRow	=	mysqli_fetch_array($confirmQry);
$confirm	=	$confirmRow['confirm'];
if ($confirm == 1 && $confirmRow['extra'] > 0) {
	$extra = $confirmRow['extra'];	
} 
if ($confirm == 1 && $confirmRow['discount'] > 0) {
	$discount = $confirmRow['discount'];
}
$rateResult = array("extra" => $extra, "discount" => $discount);
header('Content-type:application/json');
if (isset($_REQUEST['apiKey']) && $_REQUEST['apiKey'] == "api123") {
    $rateResult['status'] = "ok";
    if (in_array("airline", $servicesArray)) {
        $airlineRateQry = "SELECT adultTotal, childTotal, infantTotal FROM `".TABLE_PACKAGE_AIRLINE."` WHERE packageId = ".$packageID;
        $airlineRateRes = mysqli_query($connection, $airlineRateQry);
        if (mysqli_num_rows($airlineRateRes) > 0) {
            $airlineRateRow = mysqli_fetch_array($airlineRateRes);
            $rateResult['rates']['airline']['rate'] = array("adult_total" => $airlineRateRow['adultTotal'], "child_total" => $airlineRateRow['childTotal'], "infant_total" => $airlineRateRow['infantTotal']);
        }
        $airlinePurQry = "SELECT adultTotal, childTotal, infantTotal FROM `".TABLE_PURCHASE_PAC_AIRLINE."` WHERE  packageId = ".$packageID;
        $airlinePurRes = mysqli_query($connection, $airlinePurQry);
        if (mysqli_num_rows($airlinePurRes) > 0) {
            $airlinePurRow = mysqli_fetch_array($airlinePurRes);
            $rateResult['rates']['airline']['purchase'] = array("adult_total" => $airlinePurRow['adultTotal'], "child_total" => $airlinePurRow['childTotal'], "infant_total" => $airlinePurRow['infantTotal']);
        }
    }
    if (in_array("hotel", $servicesArray)) {
        $hotelRateQry = "SELECT p3.roomType, p3.ID, SUM(p2.total) AS roomInvTotal, SUM(p2.purchaseAmount) AS roomPurTotal FROM `".TABLE_PACKAGE_HOTEL."` AS p1 INNER JOIN `".TABLE_PACKAGE_HOTEL_ROOM_TYPE."` AS p2 ON p1.ID = p2.package_hotelId INNER JOIN `".TABLE_ROOM_TYPE."` AS p3 ON p3.ID = p2.roomId WHERE p1.packageId = ".$packageID."  GROUP BY p2.roomId";
        $hotelRateRes = mysqli_query($connection, $hotelRateQry);
        $roomArray = array();
        if (mysqli_num_rows($hotelRateRes) > 0) {
            while ($hotelRateRow = mysqli_fetch_array($hotelRateRes)) {
                //$rateResult['rates']['hotel']['rate'] = array("room_id" => $hotelRateRow['ID'], "room_inv_total" => $hotelRateRow['roomInvTotal'], "room_pur_total" => $hotelRateRow['roomPurTotal']);
                $tempArray = array("room_id" => $hotelRateRow['ID'], "room_type" => $hotelRateRow['roomType'], "room_inv_total" => $hotelRateRow['roomInvTotal'], "room_pur_total" => $hotelRateRow['roomPurTotal']);
                array_push($roomArray, $tempArray);
            }
            $rateResult['rates']['hotel'] = $roomArray;
        }
        $hotelRateNewQry =	"SELECT p3.roomType, p3.ID, sum(p2.total) AS roomInvTotal, sum(p2.purchaseAmount) AS roomPurTotal FROM `".TABLE_PACKAGE_HOTEL."` AS p1 INNER JOIN `".TABLE_PACKAGE_HOTEL_ROOM_TYPE."` AS p2 ON p1.ID = p2.package_hotelId INNER JOIN `".TABLE_ROOM_TYPE."` AS p3 ON p3.ID = p2.roomId WHERE p1.packageId = ".$packageID." and p2.roomId=1  GROUP BY p2.packageId";
        //echo $hotelRateNewQry;
        $hotelRateNewRes = mysqli_query($connection, $hotelRateNewQry);
       // $roomArray = array();
                   $rateResult['rates']['hotel_inv_rate_new'] = 0;

        if (mysqli_num_rows($hotelRateNewRes) > 0) {
            while ($hotelRateNewRow = mysqli_fetch_array($hotelRateNewRes)) {
            $rateResult['hotel_inv_rate_new'] = $hotelRateNewRow['roomInvTotal'];               
            }
        }
    }
    if (in_array("visa", $servicesArray)) {
        $visaRateQry = "SELECT SUM(total) AS invTotal, SUM(purchaseAmount) AS purTotal FROM `".TABLE_PACKAGE_VISA."` WHERE   packageId = ".$packageID." GROUP BY packageId";
        $visaRateRes = mysqli_query($connection, $visaRateQry);
        if (mysqli_num_rows($visaRateRes) > 0) {
            $visaRateRow = mysqli_fetch_array($visaRateRes);
            $rateResult['rates']['visa'] = array("inv_total" => $visaRateRow['invTotal'], "pur_total" => $visaRateRow['purTotal']);
        }
    }
    if (in_array("transportation", $servicesArray)) {
        $transportationRateQry = "SELECT SUM(seatCharge) AS invTotal, SUM(purchaseAmount) AS purTotal FROM `".TABLE_PACKAGE_TRANSPORTATION."` WHERE packageId = ".$packageID." GROUP BY packageId";
        $transportationRateRes = mysqli_query($connection, $transportationRateQry);
        if (mysqli_num_rows($transportationRateRes) > 0) {
            $transportationRateRow = mysqli_fetch_array($transportationRateRes);
            $rateResult['rates']['transportation'] = array("inv_total" => $transportationRateRow['invTotal'], "pur_total" => $transportationRateRow['purTotal']);
        }
    }
    $generalChargeQry = "SELECT tds, otherCharge, tax FROM `".TABLE_PACKAGE."` WHERE ID = ".$packageID;
    $generalChargeRes = mysqli_query($connection, $generalChargeQry);
    if (mysqli_num_rows($generalChargeRes) > 0) {
        $generalChargeRow = mysqli_fetch_array($generalChargeRes);
        $rateResult['general_charge'] = $generalChargeRow['tds'] + $generalChargeRow['otherCharge'] + $generalChargeRow['tax'];
    }
    echo json_encode($rateResult);
} else {
    $rateResult['status'] = "failure";
    $rateResult['error'] = "API key invalid";
}