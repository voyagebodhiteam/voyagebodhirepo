<?php
require ("../config/config.inc.php");
require("../config/Application.class.php");
if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	
	$check 	= 	" a.proId='$proId'";
}

$finYear	=	$_SESSION['finYear'];
$connection	=	mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_DATABASE);

$acResult	=	[];
$acDetails	=	[];
$staticID	=	$_REQUEST['accID'];
$apiKey		=	$_REQUEST['apiKey'];

if($apiKey 	==	'api123')
{
	$acSelect	=	"SELECT a.ID,a.in_packageId,a.passengerName,a.dob,a.age,a.nationality,a.birthPlace,
    					 	a.relation,a.mobile,a.passportNo,a.dateOfIssue,a.validUpTo,a.issuePlace,
    					 	b.ticketNo,e.submissionDate,e.collectionDate,e.dispatchDate,
    					 	c.bedStatus,d.seatStatus,f.roomType
                       FROM ".TABLE_INVOICE_PACKAGE_PASSENGER." a 
                  LEFT JOIN ".TABLE_INVOICE_PACKAGE_AIRLINE." b ON b.passengerId=a.ID 
                  LEFT JOIN ".TABLE_INVOICE_PACKAGE_VISA." e ON e.passengerId=a.ID
                  LEFT JOIN ".TABLE_INVOICE_PACKAGE_HOTEL." c ON c.passengerId=a.ID
                  LEFT JOIN ".TABLE_ROOM_TYPE." f ON f.ID=c.roomType
                  LEFT JOIN ".TABLE_INVOICE_PACKAGE_TRANSPORTATION." d ON d.passengerId=a.ID
  					  WHERE a.ID='$staticID' and a.finYear=$finYear";
	//echo $acSelect;die;
	$acQry		=	mysqli_query($connection,$acSelect);
	if(mysqli_num_rows($acQry)>0)
	{
		$acRow	=	mysqli_fetch_array($acQry);
		
		$acResult['status'] = "ok";
		
		if($acRow['in_packageId'] != null)
        {
            $acDetails['in_packageId'] = ['title' => 'Invoice No', 'dataValue' => $acRow['in_packageId']];
        }
        if($acRow['passengerName'] != null)
        {
            $acDetails['passengerName'] = ['title' => 'Passenger Name', 'dataValue' => $acRow['passengerName']];
        }
       
        if($acRow['dob'] != null)
        {
            $acDetails['dob'] = ['title' => 'DOB', 'dataValue' => $App->dbformat_date($acRow['dob'])];
        }
        if($acRow['age'] != null)
        {
            $acDetails['age'] = ['title' => 'Age', 'dataValue' => $acRow['age']];
        }
      
        if($acRow['birthPlace'] != null)
        {
            $acDetails['birthPlace'] = ['title' => 'Place Of Birth', 'dataValue' => $acRow['birthPlace']];
        }
        if($acRow['relation'] != null)
        {
            $acDetails['relation'] = ['title' => 'Relation', 'dataValue' => $acRow['relation']];
        }
        if($acRow['mobile'] != null)
        {
            $acDetails['mobile'] = ['title' => 'Mahram', 'dataValue' => $acRow['mobile']];
        }
        if($acRow['nationality'] != null)
        {
            $acDetails['nationality'] = ['title' => 'Nationality', 'dataValue' => $acRow['nationality']];
        }
        if($acRow['passportNo'] != null)
        {
            $acDetails['passportNo'] = ['title' => 'Passport No', 'dataValue' => $acRow['passportNo']];
        }
        if($acRow['dateOfIssue'] != null)
        {
            $acDetails['dateOfIssue'] = ['title' => 'Date Of Issue', 'dataValue' => $App->dbformat_date($acRow['dateOfIssue'])];
        }
        if($acRow['validUpTo'] != null)
        {
            $acDetails['validUpTo'] = ['title' => 'Valid UpTo', 'dataValue' => $App->dbformat_date($acRow['validUpTo'])];
        }
        if($acRow['issuePlace'] != null)
        {
            $acDetails['issuePlace'] = ['title' => 'Place of Issue', 'dataValue' => $acRow['issuePlace']];
        }  
        if($acRow['ticketNo'] != null)
        {
            $acDetails['ticketNo'] = ['title' => 'Ticket No', 'dataValue' => $acRow['ticketNo']];
        }  
         if($acRow['submissionDate'] != null)
        {
            $acDetails['submissionDate'] = ['title' => 'Submission Date', 'dataValue' => $acRow['submissionDate']];
        }
         if($acRow['collectionDate'] != null)
        {
            $acDetails['collectionDate'] = ['title' => 'Collection Date', 'dataValue' => $acRow['collectionDate']];
        } 
         if($acRow['dispatchDate'] != null)
        {
            $acDetails['dispatchDate'] = ['title' => 'Dispatch Date', 'dataValue' => $acRow['dispatchDate']];
        } 
        if($acRow['roomType'] != null)
        {
            $acDetails['roomType'] = ['title' => 'Room Type', 'dataValue' => $acRow['roomType']];
        } 
        if($acRow['seatStatus'] == 1)
        {
            $acDetails['seatStatus'] = ['title' => 'Seat Status', 'dataValue' => 'Yes'];
        } 
        
        
        //print_r($acDetails);
        $acResult['details'] = $acDetails;
        //print_r($acResult);
        header('Content-type: application/json');
        echo json_encode($acResult);
	}
	else
    {
        $acResult['status'] = "failure";
        $acResult['error'] = "No data found";
        header('Content-type: application/json');
        echo json_encode($acResult);
    }
}
else
{
    $acResult['status'] = "failure";
    $acResult['error'] = "apiKey not valid";
    header('Content-type: application/json');
    echo json_encode($acResult);
}

?>