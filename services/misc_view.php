<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"proId='$proId'";
}
// Connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$acResult = [];
$acDetails = [];
$ID = $_REQUEST['accID'];
$apiKey = $_REQUEST['apiKey'];
if($apiKey == 'api123'){
    $acSelect = "select * from ".TABLE_MISC_SERVICES." where ID='$ID' and $check";
	//echo $acSelect;
    $acQry = mysqli_query($connection, $acSelect);
    if(mysqli_num_rows($acQry) > 0)
    {
        $acRow = mysqli_fetch_array($acQry);
        //print_r($acRow);
        $acResult['status'] = "ok";
        if($acRow['narration'] != null)
        {
            $acDetails['narration'] = ['title' => 'Narration', 'dataValue' => $acRow['narration']];
        }
        if($acRow['customer'] != null)
        {
            $acDetails['customer'] = ['title' => 'Customer', 'dataValue' => $acRow['customer']];
        }
        if($acRow['address'] != null)
        {
            $acDetails['address'] = ['title' => 'Address', 'dataValue' => $acRow['address']];
        }
        if($acRow['details'] != null)
        {
            $acDetails['details'] = ['title' => 'Details', 'dataValue' => $acRow['details']];
        }
        if($acRow['amount'] != null)
        {
            $acDetails['amount'] = ['title' => 'Amount', 'dataValue' => $acRow['amount']];
        }
        if($acRow['tax'] != null)
        {
            $acDetails['tax'] = ['title' => 'Tax', 'dataValue' => $acRow['tax']];
        }
        if($acRow['tds'] != null)
        {
            $acDetails['tds'] = ['title' => 'TDS', 'dataValue' => $acRow['tds']];
        }
        if($acRow['charges'] != null)
        {
            $acDetails['charges'] = ['title' => 'Other Charges', 'dataValue' => $acRow['charges']];
        }
        if($acRow['total'] != null)
        {
            $acDetails['total'] = ['title' => 'Total', 'dataValue' => $acRow['total']];
        }
        
        //print_r($acDetails);
        $acResult['details'] = $acDetails;
        //print_r($acResult);
        header('Content-type: application/json');
        echo json_encode($acResult);
    }
    else
    {
        $acResult['status'] = "failure";
        $acResult['error'] = "No data found";
        header('Content-type: application/json');
        echo json_encode($acResult);
    }
}
else
{
    $acResult['status'] = "failure";
    $acResult['error'] = "apiKey not valid";
    header('Content-type: application/json');
    echo json_encode($acResult);
}