<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$finYear	=	$_SESSION['finYear'];
	$check 	= 	"a.proId='$proId'";
}
// Connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$passResult = [];
$passList = [];
$passDetails = [];
$apiKey = $_REQUEST['apiKey'];
if (isset($_REQUEST['passengerID'])) {
    $passID = $_REQUEST['passengerID'];
    if ($apiKey == "api123") {
        $passSelect = "SELECT a.ID,
        					  a.passengerName,
							  a.dob,
							  a.prefix,
							  a.age,
							  a.gender,
							  a.placeOfBirth,
							  a.relationshipId,
							  a.mobile,
							  a.nationality,
							  a.passportNo,
							  a.dateOfIssue,
							  a.validUpTo,
							  a.issuePlace,
							  a.mofa,
							  a.emofa,
							  b.relationship
        			     FROM ".TABLE_PASSENGER." a  
        			LEFT JOIN ".TABLE_RELATIONSHIP." b on a.relationshipId=b.ID
        			     WHERE a.ID = '".$passID."' and $check and a.finYear=$finYear";
        			     //echo $passSelect;die;
        $passQry = mysqli_query($connection, $passSelect);
        if (mysqli_num_rows($passQry) > 0) {
            $passResult['status'] = "ok";
            $passRow = mysqli_fetch_array($passQry);
            $passDetails['ID'] = $passRow['ID'];
            if ($passRow['passengerName'] != null) {
                $passDetails['passengerName'] = $passRow['passengerName'];
            }
            if ($passRow['dob'] != null) {
                $dobAr  = explode('-', $passRow['dob']);
                $passDetails['dob'] = $dobAr[2].'-'.$dobAr[1].'-'.$dobAr[0];
            }
            /*
            akhila
            */
          	if ($passRow['age'] != null) {
                $passDetails['age'] = $passRow['age'];
            }
            if ($passRow['gender'] != null) {
                $passDetails['gender'] = $passRow['gender'];
            }
            if ($passRow['mobile'] != null) {
                $passDetails['mobile'] = $passRow['mobile'];
            }
            if ($passRow['placeOfBirth'] != null) {
                $passDetails['placeOfBirth'] = $passRow['placeOfBirth'];
            }
            if ($passRow['relationshipId'] != null) {
                $passDetails['relationship'] = $passRow['relationship'];
            }
            /*
            akhila end
            */
            if ($passRow['nationality'] != null) {
                $passDetails['nationality'] = $passRow['nationality'];
            }            
            if ($passRow['mobile'] != null) {
                $passDetails['mobile'] = $passRow['mobile'];
            }            
            if ($passRow['passportNo'] != null) {
                $passDetails['passportNo'] = $passRow['passportNo'];
            }
            if ($passRow['dateOfIssue'] != null) {
                $issAr = explode('-', $passRow['dateOfIssue']);
                $passDetails['dateOfIssue'] = $issAr[2].'-'.$issAr[1].'-'.$issAr[0];
            }
            if ($passRow['validUpTo'] != null) {
                $validAr = explode('-', $passRow['validUpTo']);
                $passDetails['validUpTo'] = $validAr[2].'-'.$validAr[1].'-'.$validAr[0];
            }
            /*
            akhila
            */
            if ($passRow['issuePlace'] != null) {
                $passDetails['issuePlace'] = $passRow['issuePlace'];
            }
            /*
            akhila end
            */
            $passResult['passengerDetails'] = $passDetails;
            header('Content-type: application/json');
            echo json_encode($passResult);
        } else {
            $passResult['status'] = "failure";
            $passResult['error'] = "No data found";
            header('Content-type: application/json');
            echo json_encode($passResult);
        }
    } else {
        $passResult['status'] = "failure";
        $passResult['error'] = "API Key not valid";
        header('Content-type: application/json');
        echo json_encode($passResult);
    }
}
if (isset($_REQUEST['searchKey']) && isset($_REQUEST['parentId'])) {
    $searchKey = $_REQUEST['searchKey'];
    $parentId = $_REQUEST['parentId'];
    if ($apiKey == "api123") {
        $searchSelect = "SELECT ID, passengerName, mobile FROM `".TABLE_PASSENGER."` WHERE passengerName like '%".$searchKey."%' AND customerId = $parentId";
        $searchQry = mysqli_query($connection, $searchSelect);
        if (mysqli_num_rows($searchQry) > 0) {
            $passResult['status'] = "ok";
            while ($searchRow = mysqli_fetch_array($searchQry)) {
                //$passList['passengerName'] = $searchRow['passengerName'];
                //$passList['mobile'] = $searchRow['mobile'];
                $tempArray = array("id" => $searchRow['ID'],"passenger_name" => $searchRow['passengerName'], "mobile" => $searchRow['mobile']);
                array_push($passList, $tempArray);
            }
            $passResult['passengerNames'] = $passList;
            header('Content-type: application/json');
            echo json_encode($passResult);
        } else {
            $passResult['status'] = "failure";
            $passResult['error'] = "No data found";
            header('Content-type: application/json');
            echo json_encode($passResult);
        }
    } else {
        $passResult['status'] = "failure";
        $passResult['error'] = "API Key not valid";
        header('Content-type: application/json');
        echo json_encode($passResult);
    }
}
