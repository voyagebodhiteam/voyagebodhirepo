<?php
require("../config/config.inc.php");

// Connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$delResult = [];
$delID = $_REQUEST['delID'];
$apiKey = $_REQUEST['apiKey'];
if ($apiKey == "api123")
{
    try
    {
        $delQry = "delete from `".TABLE_AIRLINE_PASSENGER."` where ID = ".$delID;
        $delRes = mysqli_query($connection, $delQry);
        $delResult['status'] = "ok";
        header('Content-type: application/json');
        echo json_encode($delResult);
    }
    catch (Exception $e)
    {
        $delResult['status'] = "failure";
        $delResult['error'] = "You can't delete. Because this data is used some where else";
        header('Content-type: application/json');
        echo json_encode($delResult);
    }
}
else
{
    $delResult['status'] = "failure";
    $delResult['error'] = "Api Key is not valid";
    header('Content-type: application/json');
    echo json_encode($delResult);
}