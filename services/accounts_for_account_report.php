<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
if (isset($_REQUEST['user_type'])) {
    // Connection
    $connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
    header("Content-type: Application/json");
    $userType = $_REQUEST['user_type'];
    
    $cond1= "1";
    if($userType == 'Branch' || $userType == 'Staff')
    {
		$cond1 = $cond1." and proId = ".$_SESSION['proId']."";
	}
    else if($userType == 'Admin'){
		if(isset($_REQUEST['company'])) {
			$cond1 = $cond1." and proId = ".$_REQUEST['company']."";
		}
		else{
			$cond1="1";
		}
	}
	
    if (isset($_REQUEST['agent'])) {
        $agent = $_REQUEST['agent'];
        $acSelect = "SELECT ID, accountName FROM ".TABLE_ACCOUNTS." WHERE proId=0 || ($cond1 AND agentId = ".$agent.")";
    } else {
        $acSelect = "SELECT ID, accountName FROM ".TABLE_ACCOUNTS." WHERE (proId=0 || $cond1)";
    }
    $acQry = mysqli_query($connection, $acSelect);
    if (mysqli_num_rows($acQry) > 0) {
        $accList = array();
        while ($acRow = mysqli_fetch_array($acQry)) {
            $tempArray = array("id" => $acRow['ID'], "acc_name" => $acRow['accountName']);
            array_push($accList, $tempArray);
        }
        $result = array("status" => "ok", "acc_list" => $accList);
    } else {
        $result = array("status" => "failure", "error" => "No data found");
    }
    echo json_encode($result);
}