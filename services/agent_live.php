<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$check 	= 	"proId='$proId'";
}
// Connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
header("Content-type: Application/json");
if (isset($_REQUEST['keyWord'])) {
    $keyWord = $_REQUEST['keyWord'];
    $agentQry = "SELECT ID, accountName FROM `".TABLE_ACCOUNTS."` WHERE accountGroupId = '48' AND (ID LIKE '".$keyWord."%' OR accountName LIKE '%".$keyWord."%') AND $check and finYear=$finYear";
    $agentRes = mysqli_query($connection, $agentQry);
    if (mysqli_num_rows($agentRes) > 0) {
        $agentList = array();
        while ($agentRow = mysqli_fetch_array($agentRes)) {
            array_push($agentList, array("id" => $agentRow['ID'], "name" => $agentRow['accountName']));
        }
        $result = array("status" => "ok", "agent_list" => $agentList);
    } else {
        $result = array("status" => "failure", "error" => "No data found!");
    }
} else {
    $result = array("status" => "failure", "error" => "No keyword found!");
}
echo json_encode($result);