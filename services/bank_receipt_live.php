<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
if($_SESSION['travelType']=='Admin')
{
	$check	=	"finYear='$finYear'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$finYear	=	$_SESSION['finYear'];
	$check 	= 	"proId='$proId' and finYear='$finYear'";
}
// Connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
header("Content-type: application/json");
if (isset($_REQUEST["apiKey"]) && isset($_REQUEST["custID"])) {
    $apiKey = $_REQUEST["apiKey"];
	$custID = $_REQUEST["custID"];
	$bill_list = array();
	if ($apiKey == "api123") {
		$billDetQry = "SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'airline' as invType FROM `".TABLE_INVOICE_AIRLINE."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_receipt` where `invoiceNo`= a.`invoiceNo` and voucherType='airline' AND $check)) AND $check and `customerId`= ".$custID."
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'hotel' as invType FROM `".TABLE_INVOICE_HOTEL."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_receipt` where `invoiceNo`= a.`invoiceNo` and voucherType='hotel' AND $check)) AND $check  and `customerId`= ".$custID."
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'miscellaneous' as invType FROM `".TABLE_INVOICE_MISC."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_receipt` where `invoiceNo`= a.`invoiceNo` and voucherType='miscellaneous' AND $check)) AND $check  and `customerId`= ".$custID."
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'transportation' as invType FROM `".TABLE_INVOICE_TRANSPORT."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_receipt` where `invoiceNo`= a.`invoiceNo` and voucherType='transportation' AND $check)) AND $check  and `customerId`= ".$custID."
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'visa' as invType FROM `".TABLE_INVOICE_VISA."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_receipt` where `invoiceNo`= a.`invoiceNo` and voucherType='visa' AND $check)) AND $check  and `customerId`= ".$custID."		
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'package' as invType FROM `".TABLE_INVOICE_PACKAGE."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_receipt` where `invoiceNo`= a.`invoiceNo` and voucherType='package' AND $check)) AND $check  and `customerId`= ".$custID;
		$billDetRes = mysqli_query($connection, $billDetQry);
		if (mysqli_num_rows($billDetRes) > 0) {
			$cashReceiptResult["status"] = "ok";
			$cashReceiptResult["bill_count"] = mysqli_num_rows($billDetRes);
			while ($billDetRow = mysqli_fetch_array($billDetRes)) {
				switch ($billDetRow['invType']) {
					case "airline" :
						$invPrefix = "AI";
						$voucherType = "Airline Receivable";
						break;
					case "hotel" :
						$invPrefix = "HI";
						$voucherType = "Hotel Receivable";
						break;
					case "miscellaneous" :
						$invPrefix = "MI";
						$voucherType = "Misc Receivable";
						break;
					case "transportation" :
						$invPrefix = "TI";
						$voucherType = "Transport Receivable";
						break;
					case "visa" :
						$invPrefix = "VI";
						$voucherType = "Visa Receivable";
						break;
					case "package" :
						$invPrefix = "PI";
						$voucherType = "Package Receivable";
						break;
				}
				$invDateArray = explode('-', $billDetRow['invoiceDate']);
				$tempArray = array("invoice_no" => $billDetRow['invoiceNo'], "invoice_date" => $invDateArray[2].'-'.$invDateArray[1].'-'.$invDateArray[0], "net_amount" => round($billDetRow['netAmount']), "paid_amount" => 0, "inv_type" => $billDetRow['invType'], "inv_prefix" => $invPrefix);
				$billSumQry = "SELECT SUM(payingAmount) as paidAmount FROM `".TABLE_CASH_RECEIPT."` WHERE voucherType = '".$billDetRow['invType']."' AND invoiceNo = ".$billDetRow['invoiceNo']."  AND $check  GROUP BY invoiceNo"; 
				$billSumRes = mysqli_query($connection, $billSumQry);
				if (mysqli_num_rows($billSumRes) > 0) {
					//$tempArray["paid_amount"] = mysqli_fetch_array($billSumRes)['paidAmount'];	
					$paidAmount =   mysqli_fetch_array($billSumRes)['paidAmount'];	
					
					
				//advance
				$advance=0;
				$advSumQry = "SELECT SUM(netAmount) as advAmount FROM `".TABLE_ADVANCE_RECEIPT."` WHERE billType = '".$billDetRow['invType']."' AND billNo = ".$billDetRow['invoiceNo']."  AND $check GROUP BY billNo";//echo $billSumQry;
				$advSumRes = mysqli_query($connection, $advSumQry);
				if (mysqli_num_rows($advSumRes) > 0) {
					$advance = mysqli_fetch_array($advSumRes)['advAmount'];		
					
				} 
					
					$paidAmount2=$paidAmount+$advance;
													
					$diff		=	$billDetRow['netAmount']-$paidAmount2;//echo $diff;die;
					if($diff > 0)
					{
						$tempArray["paid_amount"] = $paidAmount2;
					}
					else
					{
						continue;
					}					
				}
				
				//details
				$detailsQry = "SELECT DISTINCT(details) FROM `".TABLE_TRANSACTION."` WHERE voucherType = '$voucherType' AND voucherNo = ".$billDetRow['invoiceNo']."  AND $check ";//GROUP BY invoiceNo;
				$detailsRes = mysqli_query($connection, $detailsQry);
				if (mysqli_num_rows($detailsRes) > 0) {
					$tempArray["details"] = mysqli_fetch_array($detailsRes)['details'];				
				}
				array_push($bill_list, $tempArray);
			}				
			$cashReceiptResult['bill_list'] = $bill_list;
			echo json_encode($cashReceiptResult);			
		} else {
			$cashReceiptResult = array("status" => "ok", "bill_count" => 0);
			echo json_encode($cashReceiptResult);	
		}
	} else {
		$cashReceiptResult = array("status" => "failure", "error" => "API Key invalid");
		echo json_encode($cashReceiptResult);		
	}
}