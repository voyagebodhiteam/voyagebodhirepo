<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
if (isset($_REQUEST['company'])) {
    $connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
    header("Content-type: Application/json");
    $compId = $_REQUEST['company'];
    $agSelect = "SELECT * FROM ".TABLE_ACCOUNTS." where accountGroupId='48' and proId=$compId and finYear=$finYear";
    $agQry = mysqli_query($connection, $agSelect);
    if (mysqli_num_rows($agQry) > 0) {
        $agentArray = array();
        while ($agRow = mysqli_fetch_array($agQry)) {
            $tempArray = array("id" => $agRow['ID'], "accName" => $agRow['accountName']);
            array_push($agentArray, $tempArray);
        }
        $result = array("status" => "ok", "agents" => $agentArray);
    } else {
        $result = array("status" => "failure", "error" => "No data found");
    }
    echo json_encode($result);
}