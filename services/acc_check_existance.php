<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
if($_SESSION['travelType']=='Admin')
{
    $check	=	1;
}
else
{
    $check	=	'';
    $logId	=	$_SESSION['travelId'];
    $proId	=	$_SESSION['proId'];
    $finYear	=	$_SESSION['finYear'];
    $check 	= 	"proId='$proId'";
}
// Connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
header("Content-type: application/json");
if (isset($_REQUEST['keyWord'])) {
    $searchKey = $_REQUEST['keyWord'];
    $acToQry = "SELECT accountName FROM `".TABLE_ACCOUNTS."` WHERE accountName LIKE '%".$searchKey."%' AND $check AND finYear=$finYear";
    $acToRes = mysqli_query($connection, $acToQry);
    if (mysqli_num_rows($acToRes) > 0) {
        $acToResult["status"] = "ok";
        $acToArray = array();
        while ($acToRow = mysqli_fetch_array($acToRes)) {
            //$tempArray = array("id" => $acToRow['ID'], "acc_name" => $acToRow['accountName']);
            array_push($acToArray, $acToRow['accountName']);
        }
        //$acToResult['acc_details'] = $acToArray;
        $acToResult = array("status" => "ok", "acc_list" => $acToArray);
    } else {
        $acToResult = array("status" => "failure", "error" => "No data found");
    }
} else {
    $acToResult = array("status" => "failure", "error" => "No data found");
}
echo json_encode($acToResult);
