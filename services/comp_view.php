<?php
require ("../config/config.inc.php");

$connection	=	mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_DATABASE);

$acResult	=	[];
$acDetails	=	[];
$staticID	=	$_REQUEST['accID'];
$apiKey		=	$_REQUEST['apiKey'];

if($apiKey 	==	'api123')
{
	$acSelect	=	"select * from ".TABLE_COMPANY." where ID='$staticID'";
	$acQry		=	mysqli_query($connection,$acSelect);
	if(mysqli_num_rows($acQry)>0)
	{
		$acRow	=	mysqli_fetch_array($acQry);
		
		$acResult['status'] = "ok";
		
        if($acRow['companyName'] != null)
        {
            $acDetails['companyName'] = ['title' => 'Company Name', 'dataValue' => $acRow['companyName']];
        }
        if($acRow['address'] != null)
        {
            $acDetails['address'] = ['title' => 'Address', 'dataValue' => $acRow['address']];
        }
        if($acRow['pin'] != null)
        {
            $acDetails['pin'] = ['title' => 'Pin', 'dataValue' => $acRow['pin']];
        }
        if($acRow['city'] != null)
        {
            $acDetails['city'] = ['title' => 'City', 'dataValue' => $acRow['city']];
        }
        if($acRow['phone'] != null)
        {
            $acDetails['phone'] = ['title' => 'Phone', 'dataValue' => $acRow['phone']];
        }
        if($acRow['mobile'] != null)
        {
            $acDetails['mobile'] = ['title' => 'Mobile', 'dataValue' => $acRow['mobile']];
        }
        if($acRow['email'] != null)
        {
            $acDetails['email'] = ['title' => 'Email', 'dataValue' => $acRow['email']];
        }
        if($acRow['website'] != null)
        {
            $acDetails['website'] = ['title' => 'Website', 'dataValue' => $acRow['website']];
        }
        if($acRow['tds'] != null)
        {
            $acDetails['tds'] = ['title' => 'tds', 'dataValue' => $acRow['tds']];
        }
        if($acRow['circle'] != null)
        {
            $acDetails['circle'] = ['title' => 'Circle', 'dataValue' => $acRow['circle']];
        }
        if($acRow['resPerson'] != null)
        {
            $acDetails['resPerson'] = ['title' => 'Responsible Person ', 'dataValue' => $acRow['resPerson']];
        }
        if($acRow['designation'] != null)
        {
            $acDetails['designation'] = ['title' => 'Designation', 'dataValue' => $acRow['designation']];
        }
        if($acRow['remark'] != null)
        {
            $acDetails['remark'] = ['title' => 'Remark', 'dataValue' => $acRow['remark']];
        }
        
        //print_r($acDetails);
        $acResult['details'] = $acDetails;
        //print_r($acResult);
        header('Content-type: application/json');
        echo json_encode($acResult);
	}
	else
    {
        $acResult['status'] = "failure";
        $acResult['error'] = "No data found";
        header('Content-type: application/json');
        echo json_encode($acResult);
    }
}
else
{
    $acResult['status'] = "failure";
    $acResult['error'] = "apiKey not valid";
    header('Content-type: application/json');
    echo json_encode($acResult);
}

?>