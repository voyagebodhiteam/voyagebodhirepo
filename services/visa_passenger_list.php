<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$finYear	=	$_SESSION['finYear'];
	$check 	= 	"proId='$proId'";
}
// Connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
header("Content-type: application/json");
if (isset($_REQUEST['apiKey']) && isset($_REQUEST['parentID'])) {
    $apiKey = $_REQUEST['apiKey'];
    $parentID = $_REQUEST['parentID'];
    if ($apiKey == "api123") {
        $passQry = "SELECT ID, passengerName, dob, gender, passportNo FROM `".TABLE_PASSENGER."` WHERE $check and finYear=$finYear and customerId = ".$parentID;
        $passRes = mysqli_query($connection, $passQry);
        if (mysqli_num_rows($passRes) > 0) {
            $passengerList = array();
            while ($passRow = mysqli_fetch_array($passRes)) {
            	$dob  = $App->dbformat_date($passRow['dob']);
                $tempArray = array("id" => $passRow['ID'], "name" => $passRow['passengerName'], "dob" => $dob, "gender" => $passRow['gender'], "passport_no" => $passRow['passportNo']);
                array_push($passengerList, $tempArray);
            }
            $passResult = array("status" => "ok", "passenger_details" => $passengerList);
        } else {
            $passResult = array("status" => "failure", "error" => "No passenger found under this customer");
        }
    } else {
        $passResult = array("status" => "failure", "error" => "API Key invalid");
    }
    echo json_encode($passResult);
}