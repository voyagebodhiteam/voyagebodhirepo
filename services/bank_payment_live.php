<?php
require("../config/config.inc.php");
require("../config/Application.class.php");
if($_SESSION['travelType']=='Admin')
{
	$check	=	"finYear='$finYear'";
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$finYear	=	$_SESSION['finYear'];
	$check 	= 	"proId='$proId' and finYear='$finYear'";
}
// Connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
header('Content-type: application/json');
if (isset($_REQUEST["apiKey"]) && isset($_REQUEST["supID"])) {
	$apiKey = $_REQUEST["apiKey"];
	$supID = $_REQUEST["supID"];
	$bill_list = array();
	if ($apiKey == "api123") {
		$billDetQry = "SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'airline' as invType FROM `".TABLE_PURCHASE_AIRLINE."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`invoiceNo` and voucherType='airline' AND $check)) AND $check  and `supplierId`= ".$supID."
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'hotel' as invType FROM `".TABLE_PURCHASE_HOTEL."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`invoiceNo` and voucherType='hotel' AND $check)) AND $check  and `supplierId`= ".$supID."
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'miscellaneous' as invType FROM `".TABLE_PURCHASE_MISC."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`invoiceNo` and voucherType='miscellaneous' AND $check)) AND $check  and `supplierId`= ".$supID."
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'transportation' as invType FROM `".TABLE_PURCHASE_TRANSPORT."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`invoiceNo` and voucherType='transportation' AND $check)) AND $check  and `supplierId`= ".$supID."
		UNION
		SELECT a.`invoiceNo`,a.`invoiceDate`,a.`netAmount`, 'visa' as invType FROM `".TABLE_PURCHASE_VISA."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`invoiceNo` and voucherType='visa' AND $check)) AND $check  and `supplierId`= ".$supID."
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Airline' as invType FROM `".TABLE_PURCHASE_PACKAGE_AIRLINE."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`voucherNo` and voucherType='Package Airline' AND $check)) AND $check  and `supplierId`= ".$supID."
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Hotel' as invType FROM `".TABLE_PURCHASE_PACKAGE_HOTEL."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`voucherNo` and voucherType='Package Hotel' AND $check)) AND $check  and `supplierId`= ".$supID."
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Transport' as invType FROM `".TABLE_PURCHASE_PACKAGE_TRANSPORT."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`voucherNo` and voucherType='Package Transport' AND $check)) AND $check  and `supplierId`= ".$supID."
		UNION
		SELECT a.`voucherNo` as invoiceNo,a.`purchaseDate` as invoiceDate,a.`netAmount`, 'Package Visa' as invType FROM `".TABLE_PURCHASE_PACKAGE_VISA."` a WHERE (`netAmount`> (select COALESCE(sum(`payingAmount`),0) as amt from `cash_payment` where `invoiceNo`= a.`voucherNo` and voucherType='Package Visa' AND $check)) AND $check  and `supplierId`= ".$supID;
		
		$billDetRes = mysqli_query($connection, $billDetQry);
		if (mysqli_num_rows($billDetRes) > 0) {
			$cashPaymentResult["status"] = "ok";
			$cashPaymentResult["bill_count"] = mysqli_num_rows($billDetRes);
			while ($billDetRow = mysqli_fetch_array($billDetRes)) {
				switch ($billDetRow['invType']) {
					case "airline" :
						$invPrefix = "AP";
						$voucherType = "Airline Payable";
						break;
					case "hotel" :
						$invPrefix = "HP";
						$voucherType = "Hotel Payable";
						break;
					case "miscellaneous" :
						$invPrefix = "MP";
						$voucherType = "Misc Payable";
						break;
					case "transportation" :
						$invPrefix = "TP";
						$voucherType = "Transport Payable";
						break;
					case "visa" :
						$invPrefix = "VP";
						$voucherType = "Visa Payable";
						break;						
					case "Package Airline" :
						$invPrefix = "PAP";
						$voucherType = "Package Payable";
						break;
					case "Package Hotel" :
						$invPrefix = "PHP";
						$voucherType = "Package Payable";
						break;					
					case "Package Transport" :
						$invPrefix = "PTP";
						$voucherType = "Package Payable";
						break;
					case "Package Visa" :
						$invPrefix = "PVP";
						$voucherType = "Package Payable";
						break;
				}
				$invDateArray = explode('-', $billDetRow['invoiceDate']);
				$tempArray = array("invoice_no" => $billDetRow['invoiceNo'], "invoice_date" => $invDateArray[2].'-'.$invDateArray[1].'-'.$invDateArray[0], "net_amount" => round($billDetRow['netAmount']), "paid_amount" => 0, "inv_type" => $billDetRow['invType'], "inv_prefix" => $invPrefix);
				$billSumQry = "SELECT SUM(payingAmount) as paidAmount FROM `".TABLE_CASH_PAYMENT."` WHERE voucherType = '".$billDetRow['invType']."' AND invoiceNo = ".$billDetRow['invoiceNo']." AND $check  GROUP BY invoiceNo";
				$billSumRes = mysqli_query($connection, $billSumQry);
				if (mysqli_num_rows($billSumRes) > 0) {
					$paidAmount = mysqli_fetch_array($billSumRes)['paidAmount'];											$diff=$billDetRow['netAmount']-$paidAmount;
					if($diff > 0)
					{
						$tempArray["paid_amount"] = $paidAmount;
					}	
				}
				//array_push($bill_list, $tempArray);
				
				//details
				$detailsQry = "SELECT DISTINCT(details) FROM `".TABLE_TRANSACTION."` WHERE voucherType = '$voucherType' AND voucherNo = ".$billDetRow['invoiceNo']."  AND $check ";//GROUP BY invoiceNo;
				$detailsRes = mysqli_query($connection, $detailsQry);
				if (mysqli_num_rows($detailsRes) > 0) {
					$tempArray["details"] = mysqli_fetch_array($detailsRes)['details'];				
				}
				array_push($bill_list, $tempArray);
			}				
			$cashPaymentResult['bill_list'] = $bill_list;
			echo json_encode($cashPaymentResult);			
		} else {
			$cashPaymentResult = array("status" => "ok", "bill_count" => 0);
			echo json_encode($cashPaymentResult);	
		}
	} else {
		$cashPaymentResult = array("status" => "failure", "error" => "API Key invalid");
		echo json_encode($cashPaymentResult);		
	}
}