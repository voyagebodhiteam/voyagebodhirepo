<?php
require ("../config/config.inc.php");
require("../config/Application.class.php");
if($_SESSION['travelType']=='Admin')
{
	$check	=	1;
}
else
{
	$check	=	'';
	$logId	=	$_SESSION['travelId'];
	$proId	=	$_SESSION['proId'];
	$finYear	=	$_SESSION['finYear'];
	$check 	= 	"a.proId='$proId'";
}

$connection	=	mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_DATABASE);

$acResult	=	[];
$acDetails	=	[];
$staticID	=	$_REQUEST['accID'];
$apiKey		=	$_REQUEST['apiKey'];

if($apiKey 	==	'api123')
{
	$acSelect	=	"select a.passengerName,
							a.dob,
                                                        a.prefix,
							a.age,
							a.gender,
							a.placeOfBirth,
							a.relationshipId,
							a.mobile,
							a.nationality,
							a.passportNo,
							a.dateOfIssue,
							a.validUpTo,
							a.issuePlace,
							a.mofa,
							a.emofa,
							b.accountName,
							b.ID as acId,
							b.mobile as aMobile,
							c.relationship
					   from ".TABLE_PASSENGER." as a  
				  left join	".TABLE_ACCOUNTS." as b on b.ID = a.customerId
				  left join ".TABLE_RELATIONSHIP." as c on c.ID = a.relationshipId
					  where a.ID='$staticID' and $check ";
	$acQry		=	mysqli_query($connection,$acSelect);
	if(mysqli_num_rows($acQry)>0)
	{
		$acRow	=	mysqli_fetch_array($acQry);

		$prefix= $acRow['prefix'];
		/*$aId = $acRow['ID'];
		
		$relQuery = "SELECT relationship FROM ".TABLE_RELATIONSHIP."
		             WHERE ".TABLE_RELATIONSHIP.".ID = '$aId'";
		$relResult		=	mysqli_query($connection,$relQuery);
		
		$relRow	=	mysqli_fetch_array($relResult);*/
		
		$acResult['status'] = "ok";
		
		if($acRow['accountName'] != null)
        {
            $acDetails['accountName'] = ['title' => 'Customer Name', 'dataValue' => $acRow['accountName']];
        }
        
        if($acRow['acId'] != null)
        {
            $acDetails['acId'] = ['title' => 'Customer ID', 'dataValue' => $acRow['acId']];
        }
        
        if($acRow['aMobile'] != null)
        {
            $acDetails['aMobile'] = ['title' => 'Customer Mobile', 'dataValue' => $acRow['aMobile']];
        }
        if($acRow['passengerName'] != null && $prefix!='')
        {
            $acDetails['passengerName'] = ['title' => 'Passenger Name', 'dataValue' => $prefix.'.'.$acRow['passengerName']];
        }
        
        if($prefix=='') 
        {
			$acDetails['passengerName'] = ['title' => 'Passenger Name', 'dataValue' =>$acRow['passengerName']];
		}
       
        if($acRow['dob'] != null)
        {
            $acDetails['dob'] = ['title' => 'DOB', 'dataValue' => $App->dbformat_date($acRow['dob'])];
        }
        if($acRow['age'] != null)
        {
            $acDetails['age'] = ['title' => 'Age', 'dataValue' => $acRow['age']];
        }
        if($acRow['gender'] != null)
        {
            $acDetails['gender'] = ['title' => 'Gender', 'dataValue' => $acRow['gender']];
        }
        if($acRow['placeOfBirth'] != null)
        {
            $acDetails['placeOfBirth'] = ['title' => 'Place Of Birth', 'dataValue' => $acRow['placeOfBirth']];
        }
       
        if($acRow['nationality'] != null)
        {
            $acDetails['nationality'] = ['title' => 'Nationality', 'dataValue' => $acRow['nationality']];
        }
        if($acRow['passportNo'] != null)
        {
            $acDetails['passportNo'] = ['title' => 'Passport No', 'dataValue' => $acRow['passportNo']];
        }
        if($acRow['dateOfIssue'] != null)
        {
            $acDetails['dateOfIssue'] = ['title' => 'Date Of Issue', 'dataValue' => $App->dbformat_date($acRow['dateOfIssue'])];
        }
        if($acRow['validUpTo'] != null)
        {
            $acDetails['validUpTo'] = ['title' => 'Date of Expiry', 'dataValue' => $App->dbformat_date($acRow['validUpTo'])];
        }
        if($acRow['issuePlace'] != null)
        {
            $acDetails['issuePlace'] = ['title' => 'Place of Issue', 'dataValue' => $acRow['issuePlace']];
        }
        if($acRow['mobile'] != null)
        {
            $acDetails['mobile'] = ['title' => 'Mahram', 'dataValue' => $acRow['mobile']];
        }
        if($acRow['mofa'] != null)
        {
            $acDetails['mofa'] = ['title' => 'MOFA', 'dataValue' => $acRow['mofa']];
        }
         if($acRow['emofa'] != null)
        {
            $acDetails['emofa'] = ['title' => 'EMOFA', 'dataValue' => $acRow['emofa']];
        }
        if($acRow['relationship'] != null)
        {
        	$aId=$acRow['relationshipId']; 
			$editSelect2 = 	"select  relationship                          			
			from ".TABLE_RELATIONSHIP." 
			where ID='$aId'";
			$editResult2 = 	mysqli_query($connection, $editSelect2);
			$editRow2	 = 	mysqli_fetch_array($editResult2);
        	
            $acDetails['relationship'] = ['title' => 'Relationship', 'dataValue' => $editRow2['relationship']];
        }
        //print_r($acDetails);
        $acResult['details'] = $acDetails;
        //print_r($acResult);
        header('Content-type: application/json');
        echo json_encode($acResult);
	}
	else
    {
        $acResult['status'] = "failure";
        $acResult['error'] = "No data found";
        header('Content-type: application/json');
        echo json_encode($acResult);
    }
}
else
{
    $acResult['status'] = "failure";
    $acResult['error'] = "apiKey not valid";
    header('Content-type: application/json');
    echo json_encode($acResult);
}

?>